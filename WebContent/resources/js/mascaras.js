$(function() {
	$(".tpData").datepicker();
});
$( document ).ready( function () {
	
   	$( ".validaCartao" ).mask( "9999.9999.999999.99-?9" );
   	$( ".validaCnpj" ).mask( "99.999.999/9999-99" );
   	$( ".validaCpf" ).mask("999.999.999-99");
   	$( ".validaRg" ).mask("9999999999?9999");
   	$( ".validaNumero" ).mask("9?9999");
   	$( ".validaLotacao" ).mask("9?9999");
   	$( ".validaValor" ).mask( "999.999.999.999,99" );
   	$( ".validaTelefone" ).mask( "(99) 99999-99999" );
   	$( ".validaCep" ).mask("99999-999");  
    $( ".validaData" ).mask("99/99/9999");  
    $( ".valores" ).mask("99999,99"); 
    $( ".validaCompetencia" ).mask("99/9999"); 
    $( ".validaPeriodo" ).mask("9");
    $( ".validaLote" ).mask("9?999");
    $( ".validaCdPrestador" ).mask("9?9999999");
    $( ".validaCdProcedimento" ).mask("99999999");
    $( ".validaUnimed" ).mask("9999");
    $( ".validaEmpresa" ).mask("9999");
    $( ".validaFamilia" ).mask("999999");
    $( ".validaDependencia" ).mask("99");
    $( ".validaDigito" ).mask("9");
    $( ".validaSomenteNumero" ).mask("9?9999999999999");
    $( ".validaTpData" ).mask("99/99/9999");
    $( ".validaHora" ).mask("99:99");
    
    //protocolos de atendimento
    $(".codArea").mask("(99)");
	$(".fone").mask("9999-9999?9");
	$(".cpf").mask("999.999.999-99");
	$(".codBenef").mask("9999 9999999999 99 9");
} 
);
function carregando(prm) {
	$('#' + prm).modal({
		keyboard : false
	});
}
