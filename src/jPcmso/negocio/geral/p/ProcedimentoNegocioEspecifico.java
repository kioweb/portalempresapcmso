package jPcmso.negocio.geral.p;

import jPcmso.persistencia.geral.p.Procedimento;
import jPcmso.persistencia.geral.p.ProcedimentoLocalizador;
import quatro.negocio.ErroDeNegocio;
import quatro.negocio.NegocioEspecificoImpl;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;

public class ProcedimentoNegocioEspecifico extends NegocioEspecificoImpl {
	
	public void antesDeInserir(Conexao cnx, TabelaBasica registro) throws QtSQLException, ErroDeNegocio {
		Procedimento t = (Procedimento) registro;
		
		if( ProcedimentoLocalizador.existe( cnx, t ) ){
			throw new ErroDeNegocio( "J� existe na base de dados um Procedimento " + t.getCdProcedimento() );
		}
		
	}
	
}