package jPcmso.negocio.geral.p;

import quatro.negocio.NegocioBasico;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import jPcmso.persistencia.geral.p.PreCadastroCbo;
import jPcmso.persistencia.geral.p.PreCadastroCboPersistor;

/**
 *
 * Classe de Neg�cio para PCMSO.PRE_CADASTRO_CBO
 *
 * @author Lucio Nascimento Teixeira
 * @see quatro.negocio.NegocioBasico
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 23/06/2020 23:58 - importado por Lucio Nascimento Teixeira
 *
 */
public class PreCadastroCboNegocio extends NegocioBasico {

	/**
	 * M�todo Construtor b�sico.
	 *
	 */
	public PreCadastroCboNegocio() throws QtSQLException {

		PreCadastroCboPersistor persistor = new PreCadastroCboPersistor();
		setPersistor( persistor );
	}

	/**
	 * M�todo Construtor que recebe uma conex�o.
	 *
	 */
	public PreCadastroCboNegocio( Conexao cnx ) throws QtSQLException {

		PreCadastroCboPersistor persistor = new PreCadastroCboPersistor();
		setPersistor( persistor );
		setConexao( cnx );
	}

	/**
	 * M�todo Construtor que recebe uma Tabela B�sica de PCMSO.PRE_CADASTRO_CBO
	 *
	 */
	public PreCadastroCboNegocio( PreCadastroCbo preCadastroCbo ) throws QtSQLException {

		PreCadastroCboPersistor persistor = new PreCadastroCboPersistor( preCadastroCbo );
		setPersistor( persistor );
	}

	/**
	 * M�todo Construtor que recebe uma conex�o e uma Tabela B�sica de PCMSO.PRE_CADASTRO_CBO
	 *
	 */
	public PreCadastroCboNegocio( Conexao cnx, PreCadastroCbo preCadastroCbo ) throws QtSQLException {

		PreCadastroCboPersistor persistor = new PreCadastroCboPersistor( preCadastroCbo );
		setPersistor( persistor );
		setConexao( cnx );
	}

	/**
	 * M�todo que retorna uma nova Tabela B�sica de PCMSO.PRE_CADASTRO_CBO
	 *
	 */
	public TabelaBasica geraNovaTabelaBasica() {

		return new PreCadastroCbo();

	}

	public boolean antesDeInserir() throws QtSQLException {

		testaPersistor();
		PreCadastroCbo preCadastroCbo = (PreCadastroCbo) getPersistor().getTabelaBasica();
		PreCadastroCboPersistor persistor = (PreCadastroCboPersistor) getPersistor();

		if ( preCadastroCbo.getIdPreCadastro() == null ) {

			try {
				preCadastroCbo.setIdPreCadastro( PreCadastroCboPersistor.geraNovoIdPreCadastro() );
			} catch (QtSQLException e) {
				setMsgErro( "PreCadastroCboNegocio - Antes de Inserir: Erro na Gera��o do Identificador: " + e.getMessage() );
				return false;
			}
		}
		if( testaRegrasBasicas() ) {
			return testaReferencias();
		}
		return false;
	}

	public boolean antesDeAlterar() throws QtSQLException {

		if( testaRegrasBasicas() ) {
			return testaReferencias();
		}
		return false;
	}

	private boolean testaReferencias() throws QtSQLException {

		
		return true;
	}

	public boolean antesDeExcluir() {

		return true;
	}

	public boolean testaRegrasBasicas() {

		PreCadastroCbo preCadastroCbo = (PreCadastroCbo) getPersistor().getTabelaBasica();

		if( preCadastroCbo.getIdPreCadastro() == null  ) {
			setMsgErro( "SEQUENCIA de BENEFICARIOS necessita estar preenchido.", "ID_PRE_CADASTRO" );
			return false;
		}

		if( preCadastroCbo.getTpSolicitacao() != null && !preCadastroCbo.getTpSolicitacao().trim().equals( "" ) && !preCadastroCbo.getTpSolicitacao().matches( "I|A|E|N" ) ) {
			setMsgErro( "tipo de solicita��o inv�lido (" + preCadastroCbo.getTpSolicitacao() + "): precisa ser I-Inclusao, A-Altera��o, E-Exclus�o ou N-Nenhuma Solicita��o", "TP_SOLICITACAO" );
			return false;
		}

		if( preCadastroCbo.getTpSexo() != null && !preCadastroCbo.getTpSexo().trim().equals( "" ) && !preCadastroCbo.getTpSexo().matches( "M|F" ) ) {
			setMsgErro( "tipo de Sexo inv�lido (" + preCadastroCbo.getTpSexo() + "): precisa ser M-Masculino ou F-feminino", "TP_SEXO" );
			return false;
		}

		if( preCadastroCbo.getTpBrPdh() != null && !preCadastroCbo.getTpBrPdh().trim().equals( "" ) && !preCadastroCbo.getTpBrPdh().matches( "N|B|P" ) ) {
			setMsgErro( "Tipo de BR PDH inv�lido (" + preCadastroCbo.getTpBrPdh() + "): precisa ser N-N�o aplicavel (NA), B-Beneficiario Reabilitado (BR) ou P-Portador de Deficiencia Habilitado (PDH)", "TP_BR_PDH" );
			return false;
		}

		if( preCadastroCbo.getTpEstadoCivil() != null && !preCadastroCbo.getTpEstadoCivil().trim().equals( "" ) && !preCadastroCbo.getTpEstadoCivil().matches( "S|M|W|D|A|U" ) ) {
			setMsgErro( "Tipo de Estado Civil (char(1)) inv�lido (" + preCadastroCbo.getTpEstadoCivil() + "): precisa ser S-Solteiro, M-Casado (M-Married), W-Viuvo (W-Widow), D-Divorciado, A-Apartado (Separado) ou U-Uni�o Est�vel", "TP_ESTADO_CIVIL" );
			return false;
		}

		if( preCadastroCbo.getTpDeficiencia() != null && !preCadastroCbo.getTpDeficiencia().trim().equals( "" ) && !preCadastroCbo.getTpDeficiencia().matches( "I|F|A|V" ) ) {
			setMsgErro( "Tipo de deficiencia do benefici�rio: inv�lido (" + preCadastroCbo.getTpDeficiencia() + "): precisa ser I-Intelectual, F-Fisica, A-Auditiva ou V-Visual", "TP_DEFICIENCIA" );
			return false;
		}

		return true;
	}
}