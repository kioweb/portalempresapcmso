package jPcmso.negocio.geral.p;

import jPcmso.biblioteca.negocio.BibliotecaPreCadastro;
import jPcmso.negocio.geral.m.MensagemEnviadaNegocio;
import jPcmso.persistencia.geral.c.Cargo;
import jPcmso.persistencia.geral.c.CargoLocalizador;
import jPcmso.persistencia.geral.c.Cbo;
import jPcmso.persistencia.geral.c.CboLocalizador;
import jPcmso.persistencia.geral.c.Cidade;
import jPcmso.persistencia.geral.c.CidadeLocalizador;
import jPcmso.persistencia.geral.e.Empresa;
import jPcmso.persistencia.geral.e.EmpresaLocalizador;
import jPcmso.persistencia.geral.f.FiliaisEmpresa;
import jPcmso.persistencia.geral.f.FiliaisEmpresaLocalizador;
import jPcmso.persistencia.geral.f.Funcao;
import jPcmso.persistencia.geral.f.FuncaoLocalizador;
import jPcmso.persistencia.geral.l.Lotacao;
import jPcmso.persistencia.geral.l.LotacaoLocalizador;
import jPcmso.persistencia.geral.m.MensagemEnviada;
import jPcmso.persistencia.geral.m.MensagemParametro;
import jPcmso.persistencia.geral.m.MensagemParametroLocalizador;
import jPcmso.persistencia.geral.m.MotivoExclusaoBeneficiario;
import jPcmso.persistencia.geral.m.MotivoExclusaoBeneficiarioLocalizador;
import jPcmso.persistencia.geral.p.PreCadastroCbo;
import jPcmso.persistencia.geral.p.PreCadastroCboLocalizador;
import jPcmso.persistencia.geral.s.Setor;
import jPcmso.persistencia.geral.s.SetorLocalizador;
import jUni.biblioteca.conversao.Inteiro;
import jUni.biblioteca.mensageiro.Email;
import jUni.biblioteca.mensageiro.Mailer;
import jUni.biblioteca.parametros.ParametrosCliente;
import jUni.biblioteca.util.Util;
import jUni.persistencia.geral.u.Usuario;
import jUni.persistencia.geral.u.UsuarioLocalizador;
import quatro.negocio.ErroDeNegocio;
import quatro.negocio.NegocioEspecificoImpl;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.util.QtData;

public class PreCadastroCboNegocioEspecifico extends NegocioEspecificoImpl {

	private static MensagemParametro mensagemParametro;
	private static String dsPortaEnvio = "465";
	private static String dsHostEnvio = "smtp.gmail.com";
	private static String dsEmailEnvio = "unimeduau@gmail.com";
	private static String dsSenhaEnvio = "Rj@0022659";

	private MensagemEnviada mensagemEnviada;
	
	public void depoisDeAlterar(Conexao cnx, TabelaBasica registro) throws QtSQLException, ErroDeNegocio {
		try {
			mensagemEnviada = new MensagemEnviada();
			mensagemEnviada.setDtMensagem(new QtData());
			initMensagemParametro(cnx);

			PreCadastroCbo pre = (PreCadastroCbo) registro;
			
			enviaEmail(pre, pre.getTpSolicitacao());
			
//			if( mensagemEnviada.getDsMensagem() != null ) {
				MensagemEnviadaNegocio mn = new MensagemEnviadaNegocio(cnx, mensagemEnviada);
				mn.insere();
//			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new ErroDeNegocio( e );
		} 
	}

	public void depoisDeInserir(Conexao cnx, TabelaBasica registro) throws QtSQLException, ErroDeNegocio {
		try {
			mensagemEnviada = new MensagemEnviada();
			mensagemEnviada.setDtMensagem(new QtData());
			initMensagemParametro(cnx);

			PreCadastroCbo pre = (PreCadastroCbo) registro;
			pre.setDtSolicitacao(new QtData());
			pre.setDtUltimoMovimento( new QtData() );
			
			enviaEmail(pre, pre.getTpSolicitacao());
			
			MensagemEnviadaNegocio mn = new MensagemEnviadaNegocio(cnx, mensagemEnviada);
			mn.insere();
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new ErroDeNegocio( e );
		} 
	}

	public void antesDeAlterar(Conexao cnx, TabelaBasica registro) throws QtSQLException, ErroDeNegocio {
		PreCadastroCbo pre = (PreCadastroCbo) registro;
		if (pre != null) {
			pre.setDtSolicitacao(new QtData());
			pre.setDtUltimoMovimento( new QtData() );
			
		}
		if( pre.isSnPossuiDeficiencia() ) {
			if( pre.getTpDeficiencia() == null ) {
				throw new ErroDeNegocio("Se possuir deficiencia, selecione um tipo.");
			}
		}
	}

	public void antesDeInserir(Conexao cnx, TabelaBasica registro) throws QtSQLException, ErroDeNegocio {
		PreCadastroCbo pre = (PreCadastroCbo) registro;
		if (pre != null) {
			pre.setDtSolicitacao(new QtData());
			pre.setDtUltimoMovimento( new QtData() );
		}
		if( pre.isSnPossuiDeficiencia() ) {
			if( pre.getTpDeficiencia() == null ) {
				throw new ErroDeNegocio("Se possuir deficiencia, selecione um tipo.");
			}
		}
	}

	public void enviaEmail(PreCadastroCbo pre, String tpSolicitacao) {
		
		try {
			
			if (tpSolicitacao != null && !tpSolicitacao.equals("N")) {
				PreCadastroCbo preCadastroCboAnterior = null;

				boolean inclusao = false;

				Email email = new Email();
				email.setConfirmacaoLeitura(true);
				email.setPara(mensagemParametro.getDsDestinatarios());
				email.setDe("NoReply@quatro.com.br");

				email.setAssunto("PCMSO Unimed - Solicitacao de Pre-Cadastro de Beneficiario");
				StringBuffer mensagem = new StringBuffer();
				StringBuffer mensagemAnterior = new StringBuffer();

				if (pre.getIdUsuarioDigitou() != null) {
					Usuario u = UsuarioLocalizador.buscaUsuario(pre.getIdUsuarioDigitou());
					if (u != null) {
						mensagem.append("<b>");
						mensagem.append("Usuario " + u.getDsLogin() + " solicitou a ");
					} else {
						mensagem.append("<b>");
						mensagem.append("a uma solicitacao de ");
					}
				} else {
					mensagem.append("<b>");
					mensagem.append("Ha uma solicitacao de ");
				}
				String tipo = null;
				switch ( tpSolicitacao.charAt(0) ) {
				case 'I':
					mensagem.append("Inclusao ");
					tipo = "I";
					inclusao = true;
					break;
				case 'E':
					tipo = "E";
					String motivo = "";
					if (pre.getIdMotivoSolicitacaoExclusao() != null) {
						MotivoExclusaoBeneficiario motivoExclusaoBeneficiario = MotivoExclusaoBeneficiarioLocalizador
								.buscaMotivoExclusaoBeneficiario(pre.getIdMotivoSolicitacaoExclusao());
						if (motivoExclusaoBeneficiario != null) {
							motivo += motivoExclusaoBeneficiario.getIdMotivoExclusao();
							motivo += " - ";
							motivo += motivoExclusaoBeneficiario.getDsMotivoExclusao();
						}
					}
					mensagem.append(
							"Exclusao para a Data (" + pre.getDtSolicitacaoExclusao().toString() + ") - Motivo: " + motivo);
					break;
				case 'A':
					tipo = "A";
					mensagem.append("Alteracao ");
					break;

				}
				if (!inclusao) {
					preCadastroCboAnterior = PreCadastroCboLocalizador.buscaPreCadastroCbo(pre.getIdPreCadastro());
				}

				if (preCadastroCboAnterior == null) {
					inclusao = true;
				}
				mensagem.append(" do Beneficiario:");
				if (pre.getCdBeneficiarioCartao() != null) {
					mensagem.append(pre.getCdBeneficiarioCartao());
				}

				mensagem.append("</b>");

				mensagem.append("<br/>");
				mensagem.append("<b>data solicitacao: </b>");
				mensagem.append(pre.getDtSolicitacao().dataEHora());
				mensagem.append("<br/>");
				mensagem.append("<br/>");
				mensagem.append("<br/>");
				mensagem.append("<br/>");
				Empresa e = EmpresaLocalizador.buscaEmpresa(pre.getCdEmpresa());
				if (e != null) {
					mensagem.append(getCampoHtml("EMPRESA", pre.getCdEmpresa() + " - " + e.getDsEmpresa()));
				}

				mensagem.append(getCampoHtml("NOME BENEFICIARIO", pre.getDsNome()));

				mensagem.append("<br/>");
				mensagem.append("<br/>");
				mensagem.append("<br/>");
				if( tipo.matches("A") ) {
					mensagem.append("<b>Dados Atuais:</b><br/><br/>");
				}
				
				if (pre.getDtAdmissao() != null) {
					if (inclusao || preCadastroCboAnterior.getDtAdmissao() == null || (!preCadastroCboAnterior.getDtAdmissao().eIgual(pre.getDtAdmissao()))) {
						if( preCadastroCboAnterior != null && preCadastroCboAnterior.getDtAdmissao() != null ) {
							mensagemAnterior.append(getCampoHtml("DATA DE ADMISAO", preCadastroCboAnterior.getDtAdmissao().toString()));
						} else {
							mensagemAnterior.append(getCampoHtml("DATA DE ADMISAO","" ) );
						}
						mensagem.append(getCampoHtml("DATA DE ADMISAO", pre.getDtAdmissao().toString()));
					}
				}
				if( pre.getIdFilial() != null ) {
					if( inclusao || compara( preCadastroCboAnterior.getIdFilial(), pre.getIdFilial() )  ) {
						if(  preCadastroCboAnterior != null && preCadastroCboAnterior.getIdFilial() != null ) {
							FiliaisEmpresa filial = FiliaisEmpresaLocalizador.buscaFiliaisEmpresa( preCadastroCboAnterior.getCdEmpresa(), preCadastroCboAnterior.getIdFilial() );
							if( filial != null ) {
								mensagemAnterior.append(getCampoHtml("FILIAL", preCadastroCboAnterior.getIdFilial() + " - " + filial.getDsFilial() ) );
							} else {
								mensagemAnterior.append(getCampoHtml("FILIAL", "" ) );
							}
						} else {
							mensagemAnterior.append(getCampoHtml("FILIAL", "" ) );
						}
						if( pre.getIdFilial() != null ) {
							FiliaisEmpresa filial = FiliaisEmpresaLocalizador.buscaFiliaisEmpresa( pre.getCdEmpresa(), pre.getIdFilial() );
							if( filial != null ) {
								mensagem.append(getCampoHtml("FILIAL", pre.getIdFilial() + " - " + filial.getDsFilial() ) );
							}
						}
					}
				}
				
				if (inclusao || compara(preCadastroCboAnterior.getTpSexo(), pre.getTpSexo())) {
					if(  preCadastroCboAnterior != null && preCadastroCboAnterior.getTpSexo() != null) {
						mensagemAnterior.append(getCampoHtml("SEXO", preCadastroCboAnterior.getTpSexo().equals("M") ? "Masculino" : "Feminino"));
					} else {
						mensagemAnterior.append(getCampoHtml("SEXO", "" ) );
					}
					if (pre.getTpSexo() != null) {
						mensagem.append(getCampoHtml("SEXO", pre.getTpSexo().equals("M") ? "Masculino" : "Feminino"));
					}
				}

				if (pre.getDtNascimento() != null) {
					if (inclusao || preCadastroCboAnterior.getDtNascimento() == null || (!preCadastroCboAnterior.getDtNascimento().eIgual(pre.getDtNascimento()))) {
						if(  preCadastroCboAnterior != null && preCadastroCboAnterior.getDtNascimento() != null ) {
							mensagemAnterior.append(getCampoHtml("DATA DE NASCIMENTO", preCadastroCboAnterior.getDtNascimento().toString()));
						} else {
							mensagemAnterior.append(getCampoHtml("DATA DE NASCIMENTO", "" ));
						}
						mensagem.append(getCampoHtml("DATA DE NASCIMENTO", pre.getDtNascimento().toString()));
					}
				}

				if (inclusao || compara(preCadastroCboAnterior.getNrCpf(), pre.getNrCpf())) {
					if(  preCadastroCboAnterior != null && preCadastroCboAnterior.getNrCpf() != null ) {
						mensagemAnterior.append(getCampoHtml("CPF", preCadastroCboAnterior.getNrCpf()));
					} else {
						mensagemAnterior.append(getCampoHtml("CPF", "" ) );
					}
					if (pre.getNrCpf() != null) {
						mensagem.append(getCampoHtml("CPF", pre.getNrCpf()));
					}
				}

				if (inclusao || compara(preCadastroCboAnterior.getCdRg(), pre.getCdRg())) {
					if(  preCadastroCboAnterior != null && preCadastroCboAnterior.getCdRg() != null ) {
						mensagemAnterior.append(getCampoHtml("RG", preCadastroCboAnterior.getCdRg() ) );
					} else {
						mensagemAnterior.append(getCampoHtml("RG", "" ) );
					}
					if (pre.getCdRg() != null) {
						mensagem.append(getCampoHtml("RG", pre.getCdRg()));
					}
				}
				if (inclusao || compara(preCadastroCboAnterior.getDsOrgaoEmissorRg(), pre.getDsOrgaoEmissorRg())) {
					if(  preCadastroCboAnterior != null && preCadastroCboAnterior.getDsOrgaoEmissorRg() != null ) {
						mensagemAnterior.append(getCampoHtml("ORGAO RG", preCadastroCboAnterior.getDsOrgaoEmissorRg()));
					} else {
						mensagemAnterior.append(getCampoHtml("ORGAO RG", "" ) );
					}
					if (pre.getDsOrgaoEmissorRg() != null) {
						mensagem.append(getCampoHtml("ORGAO RG", pre.getDsOrgaoEmissorRg()));
					}
				}

				if (inclusao || compara(preCadastroCboAnterior.getDsNomeDaMae(), pre.getDsNomeDaMae())) {
					if( preCadastroCboAnterior.getDsNomeDaMae() != null ) {
						mensagemAnterior.append(getCampoHtml("NOME DA MAE", preCadastroCboAnterior.getDsNomeDaMae()));
					} else {
						mensagemAnterior.append(getCampoHtml("NOME DA MAE", "" ) );
					}
					if (pre.getDsNomeDaMae() != null) {
						mensagem.append(getCampoHtml("NOME DA MAE", pre.getDsNomeDaMae()));
					}
				}

				if (inclusao || compara(preCadastroCboAnterior.getDsLogradouro(), pre.getDsLogradouro())) {
					if( preCadastroCboAnterior != null && preCadastroCboAnterior.getDsLogradouro() != null ) {
						mensagemAnterior.append(getCampoHtml("LOGRADOURO", preCadastroCboAnterior.getDsLogradouro()));
					} else {
						mensagemAnterior.append(getCampoHtml("LOGRADOURO", "" ) );
					}
					if (pre.getDsLogradouro() != null) {
						mensagem.append(getCampoHtml("LOGRADOURO", pre.getDsLogradouro()));
					}
				}

				if (inclusao || compara(preCadastroCboAnterior.getDsNumeroLogradouro(), pre.getDsNumeroLogradouro())) {
					if(  preCadastroCboAnterior != null && preCadastroCboAnterior.getDsNumeroLogradouro() != null ) {
						mensagemAnterior.append(getCampoHtml("NUMERO", preCadastroCboAnterior.getDsNumeroLogradouro()));
					} else {
						mensagemAnterior.append(getCampoHtml("NUMERO", "" ) );
					}
					if (pre.getDsNumeroLogradouro() != null) {
						mensagem.append(getCampoHtml("NUMERO", pre.getDsNumeroLogradouro()));
					}
				}
				if (inclusao || compara(preCadastroCboAnterior.getDsComplementoLogradouro(), pre.getDsComplementoLogradouro())) {
					if(  preCadastroCboAnterior != null && preCadastroCboAnterior.getDsComplementoLogradouro() != null ) {
						mensagemAnterior.append(getCampoHtml("COMPLEMENTO", preCadastroCboAnterior.getDsComplementoLogradouro()));
					} else {
						mensagemAnterior.append(getCampoHtml("COMPLEMENTO", "" ) );
					}
					if (pre.getDsComplementoLogradouro() != null) {
						mensagem.append(getCampoHtml("COMPLEMENTO", pre.getDsComplementoLogradouro()));
					}
				}
				if (inclusao || compara(preCadastroCboAnterior.getDsBairro(), pre.getDsBairro())) {
					if(  preCadastroCboAnterior != null && preCadastroCboAnterior.getDsBairro() != null ) {
						mensagemAnterior.append(getCampoHtml("BAIRRO", preCadastroCboAnterior.getDsBairro()));
					} else {
						mensagemAnterior.append(getCampoHtml("BAIRRO", "" ) );
					}
					if (pre.getDsBairro() != null) {
						mensagem.append(getCampoHtml("BAIRRO", pre.getDsBairro()));
					}
				}

				if (inclusao || compara(preCadastroCboAnterior.getIdCidade() + "", pre.getIdCidade() + "")) {
					if (pre.getIdCidade() != null) {
						if(  preCadastroCboAnterior != null ) {
							Cidade cAnterior = CidadeLocalizador.buscaCidade(preCadastroCboAnterior.getCdEstado(), preCadastroCboAnterior.getIdCidade());
							if( cAnterior != null ) {
								mensagemAnterior.append(getCampoHtml("ESTADO", preCadastroCboAnterior.getCdEstado()));
								mensagemAnterior.append(getCampoHtml("CIDADE", cAnterior.getDsCidade()));
							} else {
								mensagemAnterior.append(getCampoHtml("ESTADO", "" ) );
								mensagemAnterior.append(getCampoHtml("CIDADE", "" ) );
							}
						} else {
							mensagemAnterior.append(getCampoHtml("ESTADO", "" ) );
							mensagemAnterior.append(getCampoHtml("CIDADE", "" ) );
						}
						Cidade c = CidadeLocalizador.buscaCidade(pre.getCdEstado(), pre.getIdCidade());
						if (c != null) {
							mensagem.append(getCampoHtml("ESTADO", pre.getCdEstado()));
							mensagem.append(getCampoHtml("CIDADE", c.getDsCidade()));
						}
					}
				}
				if (inclusao || compara(preCadastroCboAnterior.getTpEstadoCivil(), pre.getTpEstadoCivil())) {
					
					mensagemAnterior.append("<br/>");
					mensagemAnterior.append("<b>ESTADO CIVIL:&nbsp;</b>");
					if ( preCadastroCboAnterior != null && preCadastroCboAnterior.getTpEstadoCivil() != null) {
						if (preCadastroCboAnterior.getTpEstadoCivil().equals("S")) {
							mensagemAnterior.append("Solteiro");
						} else if (preCadastroCboAnterior.getTpEstadoCivil().equals("M")) {
							mensagemAnterior.append("Casado (M-Married)");
						} else if (preCadastroCboAnterior.getTpEstadoCivil().equals("W")) {
							mensagemAnterior.append("Viuvo (W-Widow)");
						} else if (preCadastroCboAnterior.getTpEstadoCivil().equals("D")) {
							mensagemAnterior.append("Divorciado");
						} else if (preCadastroCboAnterior.getTpEstadoCivil().equals("A")) {
							mensagemAnterior.append("Apartado (Separado)");
						} else if (preCadastroCboAnterior.getTpEstadoCivil().equals("U")) {
							mensagemAnterior.append("Uniao Estavel");
						}
					}
					
					if (pre.getTpEstadoCivil() != null) {
						mensagem.append("<br/>");
						mensagem.append("<b>ESTADO CIVIL:&nbsp;</b>");
						if (pre.getTpEstadoCivil().equals("S")) {
							mensagem.append("Solteiro");
						} else if (pre.getTpEstadoCivil().equals("M")) {
							mensagem.append("Casado (M-Married)");
						} else if (pre.getTpEstadoCivil().equals("W")) {
							mensagem.append("Viuvo (W-Widow)");
						} else if (pre.getTpEstadoCivil().equals("D")) {
							mensagem.append("Divorciado");
						} else if (pre.getTpEstadoCivil().equals("A")) {
							mensagem.append("Apartado (Separado)");
						} else if (pre.getTpEstadoCivil().equals("U")) {
							mensagem.append("Uniao Estavel");
						}

					}
				}

				if (inclusao || compara(preCadastroCboAnterior.getDsTelefoneContato(), pre.getDsTelefoneContato())) {
					if( preCadastroCboAnterior != null && preCadastroCboAnterior.getDsTelefoneContato() != null ) {
						mensagemAnterior.append(getCampoHtml("TELEFONE DE CONTATO", preCadastroCboAnterior.getDsTelefoneContato()));
					} else {
						mensagemAnterior.append(getCampoHtml("TELEFONE DE CONTATO", "" ) );
					}
					if (pre.getDsTelefoneContato() != null) {
						mensagem.append(getCampoHtml("TELEFONE DE CONTATO", pre.getDsTelefoneContato()));
					}
				}

				if (inclusao || compara(preCadastroCboAnterior.getDsAtividade(), pre.getDsAtividade())) {
					if( preCadastroCboAnterior != null && preCadastroCboAnterior.getDsAtividade() != null ) {
						mensagemAnterior.append(getCampoHtml("ATIVIDADE", preCadastroCboAnterior.getDsAtividade()));
					} else {
						mensagemAnterior.append(getCampoHtml("ATIVIDADE", "" ) );
					}
					if (pre.getDsAtividade() != null) {
						mensagem.append(getCampoHtml("ATIVIDADE", pre.getDsAtividade()));
					}
				}

				if (inclusao || compara(preCadastroCboAnterior.getNrPis(), pre.getNrPis())) {
					if( preCadastroCboAnterior != null && preCadastroCboAnterior.getNrPis() != null ) {
						mensagemAnterior.append(getCampoHtml("PIS/Pasep", preCadastroCboAnterior.getNrPis()));
					} else {
						mensagemAnterior.append(getCampoHtml("PIS/Pasep", "" ) );
					}
					if (pre.getNrPis() != null) {
						mensagem.append(getCampoHtml("PIS/Pasep", pre.getNrPis()));
					}
				}
				if (inclusao || compara(preCadastroCboAnterior.getNrCtps(), pre.getNrCtps())) {
					if( preCadastroCboAnterior != null && preCadastroCboAnterior.getNrCtps() != null ) {
						mensagemAnterior.append(getCampoHtml("CTPS", preCadastroCboAnterior.getNrCtps() ));
					} else {
						mensagemAnterior.append(getCampoHtml("CTPS", "" ) );
					}
					if (pre.getNrCtps() != null) {
						mensagem.append(getCampoHtml("CTPS", pre.getNrCtps()));
					}
				}

				if (inclusao || compara(preCadastroCboAnterior.getNrSerieUf(), pre.getNrSerieUf())) {
					if( preCadastroCboAnterior != null && preCadastroCboAnterior.getNrSerieUf() != null ) {
						mensagemAnterior.append(getCampoHtml("SERIE (UF)", preCadastroCboAnterior.getNrSerieUf()));
					} else {
						mensagemAnterior.append(getCampoHtml("SERIE (UF)", "" ) );
					}
					if (pre.getNrSerieUf() != null) {
						mensagem.append(getCampoHtml("SERIE (UF)", pre.getNrSerieUf()));
					}
				}

				if (inclusao || compara(preCadastroCboAnterior.getCdCbo(), pre.getCdCbo())) {
					mensagemAnterior.append("<br/>");
					mensagemAnterior.append("<b>CBO:&nbsp;</b>");
					if (preCadastroCboAnterior != null && preCadastroCboAnterior.getCdCbo() != null) {
						Cbo cboAnterior = CboLocalizador.buscaCbo(preCadastroCboAnterior.getCdCbo());
						if (cboAnterior != null) {
							mensagemAnterior.append(cboAnterior.getNmCbo());
						} else {
							mensagemAnterior.append(preCadastroCboAnterior.getCdCbo());
						}
					}
					if (pre.getCdCbo() != null) {
						mensagem.append("<br/>");
						mensagem.append("<b>CBO:&nbsp;</b>");
						Cbo cbo = CboLocalizador.buscaCbo(pre.getCdCbo());
						if (cbo != null) {
							mensagem.append(cbo.getNmCbo());
						} else {
							mensagem.append(pre.getCdCbo());
						}
					}
				}
				
				if (inclusao || compara(preCadastroCboAnterior.getIdFuncao(), pre.getIdFuncao() )) {
					
					if( preCadastroCboAnterior != null && preCadastroCboAnterior.getIdFuncao() != null ) {
						Funcao f = FuncaoLocalizador.buscaFuncao(preCadastroCboAnterior.getIdFuncao());
						if (f != null) {
							mensagemAnterior.append(getCampoHtml("FUNCAO", f.getDsFuncao()));
						} else {
							mensagemAnterior.append(getCampoHtml("FUNCAO", "" ) );
						}
					} else {
						mensagemAnterior.append(getCampoHtml("FUNCAO", "" ) );
					}
					
					if( pre.getIdFuncao() != null ) {
						Funcao f = FuncaoLocalizador.buscaFuncao(pre.getIdFuncao());
						if (f != null) {
							mensagem.append(getCampoHtml("FUNCAO", f.getDsFuncao()));
						}
					}
				}
				
				if (inclusao || compara(preCadastroCboAnterior.getIdSetor(), pre.getIdSetor() )) {	
					if (preCadastroCboAnterior != null && preCadastroCboAnterior.getIdSetor() != null) {
						Setor s = SetorLocalizador.buscaSetor(preCadastroCboAnterior.getIdSetor());
						if (s != null) {
							mensagemAnterior.append(getCampoHtml("SETOR", s.getDsSetor()));
						} else {
							mensagemAnterior.append(getCampoHtml("SETOR", "" ) );
						}
					} else {
						mensagemAnterior.append(getCampoHtml("SETOR", "" ) );
					}
					if (pre.getIdSetor() != null) {
						Setor s = SetorLocalizador.buscaSetor(pre.getIdSetor());
						if (s != null) {
							mensagem.append(getCampoHtml("SETOR", s.getDsSetor()));
						}
					}
				}
				if (inclusao || compara(preCadastroCboAnterior.getIdCargo(), pre.getIdCargo() )) {	
					if( preCadastroCboAnterior != null && preCadastroCboAnterior.getIdCargo() != null ) {
						Cargo c = CargoLocalizador.buscaCargo(preCadastroCboAnterior.getIdCargo());
						if (c != null) {
							mensagemAnterior.append(getCampoHtml("CARGO", c.getDsCargo()));
						} else {
							mensagemAnterior.append(getCampoHtml("CARGO", "" ) );
						}
					}
					if( pre.getIdCargo() != null ) {
						Cargo c = CargoLocalizador.buscaCargo(pre.getIdCargo());
						if (c != null) {
							mensagem.append(getCampoHtml("CARGO", c.getDsCargo()));
						}
					}
				}
				if (inclusao || compara(preCadastroCboAnterior.getIdLotacao(), pre.getIdLotacao() )) {
					if (preCadastroCboAnterior != null && preCadastroCboAnterior.getIdLotacao() != null) {
						Lotacao lotacao = LotacaoLocalizador.buscaLotacao(ParametrosCliente.getCodigoCliente(), Inteiro.stoi(preCadastroCboAnterior.getCdEmpresa()), preCadastroCboAnterior.getIdLotacao());
						if (lotacao != null) {
							mensagemAnterior.append( getCampoHtml("LOTACAO", lotacao.getIdLotacao() + ": " + lotacao.getDsLotacao()));
						} else {
							mensagemAnterior.append(getCampoHtml("LOTACAO", "" ) );
						}
					} else {
						mensagemAnterior.append(getCampoHtml("LOTACAO", "" ) );
					}
					if (pre.getIdLotacao() != null) {
						Lotacao lotacao = LotacaoLocalizador.buscaLotacao(ParametrosCliente.getCodigoCliente(),
								Inteiro.stoi(pre.getCdEmpresa()), pre.getIdLotacao());
						if (lotacao != null) {
							mensagem.append( getCampoHtml("LOTACAO", lotacao.getIdLotacao() + ": " + lotacao.getDsLotacao()));
						}
					}
				}

				if (inclusao || compara(Util.mascaraFloat(preCadastroCboAnterior.getVlPeso()),
						Util.mascaraFloat(pre.getVlPeso()))) {
					if (preCadastroCboAnterior != null && preCadastroCboAnterior.getVlPeso() != null) {
						mensagemAnterior.append(getCampoHtml("PESO", Util.mascaraFloat(preCadastroCboAnterior.getVlPeso())));
					} else {
						mensagemAnterior.append(getCampoHtml("PESO", "" ) );
					}
					if (pre.getVlPeso() != null) {
						mensagem.append(getCampoHtml("PESO", Util.mascaraFloat(pre.getVlPeso())));
					}
				}

				if (inclusao || compara(Util.mascaraFloat(preCadastroCboAnterior.getVlAltura()),
						Util.mascaraFloat(pre.getVlAltura()))) {
					if (preCadastroCboAnterior != null && preCadastroCboAnterior.getVlPeso() != null) {
						mensagemAnterior.append(getCampoHtml("ALTURA", Util.mascaraFloat(preCadastroCboAnterior.getVlAltura())));
					} else {
						mensagemAnterior.append(getCampoHtml("ALTURA", "" ) );
					}
					if (pre.getVlPeso() != null) {
						mensagem.append(getCampoHtml("ALTURA", Util.mascaraFloat(pre.getVlAltura())));
					}
				}
				if (inclusao || compara(preCadastroCboAnterior.isSnPossuiDeficiencia(), pre.isSnPossuiDeficiencia())) {
					if( preCadastroCboAnterior != null ) {
						mensagemAnterior.append(getCampoHtml("POSSUI ALGUMA DEFICIENCIA", preCadastroCboAnterior.isSnPossuiDeficiencia() ? "SIM" : "NAO"));
					} else {
						mensagemAnterior.append(getCampoHtml("POSSUI ALGUMA DEFICIENCIA","" ) );
					}
					mensagem.append(getCampoHtml("POSSUI ALGUMA DEFICIENCIA", pre.isSnPossuiDeficiencia() ? "SIM" : "NAO"));
				}
				
				if (inclusao || compara(preCadastroCboAnterior.getTpDeficiencia(), pre.getTpDeficiencia())) {
					mensagemAnterior.append("<br/>");
					mensagemAnterior.append("<b>TIPO DE DEFICIENCIA:&nbsp;</b>");
					if (preCadastroCboAnterior != null && preCadastroCboAnterior.getTpDeficiencia() != null) {
						if (preCadastroCboAnterior.getTpDeficiencia().equals("I")) {
							mensagemAnterior.append("Intelectual");
						} else if (preCadastroCboAnterior.getTpDeficiencia().equals("F")) {
							mensagemAnterior.append("F�sica");
						} else if (preCadastroCboAnterior.getTpDeficiencia().equals("A")) {
							mensagemAnterior.append("Auditiva");
						} else if (preCadastroCboAnterior.getTpDeficiencia().equals("V")) {
							mensagemAnterior.append("Visual");
						} else {
							mensagemAnterior.append("Nenhuma");
						}
					}
					
					if (pre.getTpDeficiencia() != null) {
						mensagem.append("<br/>");
						mensagem.append("<b>TIPO DE DEFICIENCIA:&nbsp;</b>");
						if (pre.getTpDeficiencia().equals("I")) {
							mensagem.append("Intelectual");
						} else if (pre.getTpDeficiencia().equals("F")) {
							mensagem.append("Física");
						} else if (pre.getTpDeficiencia().equals("A")) {
							mensagem.append("Auditiva");
						} else if (pre.getTpDeficiencia().equals("V")) {
							mensagem.append("Visual");
						} else {
							mensagem.append("Nenhuma");
						}
					}
				}
				if ( inclusao || compara(preCadastroCboAnterior.isSnTrabalhoEmAltura(), pre.isSnTrabalhoEmAltura())) {
					if( preCadastroCboAnterior != null ) {
						mensagemAnterior.append(getCampoHtml("TRABALHA EM ALTURA", preCadastroCboAnterior.isSnTrabalhoEmAltura() ? "SIM" : "NAO"));
					} else {
						mensagemAnterior.append(getCampoHtml("TRABALHA EM ALTURA", "" ) );
					}
					mensagem.append(getCampoHtml("TRABALHA EM ALTURA", pre.isSnTrabalhoEmAltura() ? "SIM" : "NAO"));
				}
				
				if ( inclusao || compara(preCadastroCboAnterior.isSnTrabalhoConfinado(), pre.isSnTrabalhoConfinado())) {
					if( preCadastroCboAnterior != null ) {
						mensagemAnterior.append(getCampoHtml("TRABALHA EM ESPACO CONFINADO", preCadastroCboAnterior.isSnTrabalhoConfinado() ? "SIM" : "NAO"));
					} else {
						mensagemAnterior.append(getCampoHtml("TRABALHA EM ESPACO CONFINADO", "" ) );
					}
					mensagem.append(getCampoHtml("TRABALHA EM ESPACO CONFINADO", pre.isSnTrabalhoConfinado() ? "SIM" : "NAO"));
				}
				
				mensagem.append("<br/>");
				mensagem.append("<br/>");
				mensagem.append("<br/>");
				if ( preCadastroCboAnterior != null ) {
					if( tipo.matches("A") ) {
						mensagem.append("<b>Dados Anteriores:</b><br/><br/>");
						mensagem.append( mensagemAnterior );
					}
				}
				email.setMensagem( mensagem);
				email.setConteudo(Email.TEXT_HTML);
				mensagemEnviada.setDsMensagem(BibliotecaPreCadastro.getFormatoHTML(mensagem.toString()));
				
				
				Mailer mailer = new Mailer( email, dsHostEnvio, dsPortaEnvio, dsEmailEnvio, dsSenhaEnvio );
				mailer.send();
				
				if (pre.getIdUsuarioDigitou() != null) {
					Usuario u = UsuarioLocalizador.buscaUsuario(pre.getIdUsuarioDigitou());
					
					email.setAssunto("PCMSO Unimed - Confirma��o de Pre-Cadastro de Beneficiario");
					email.setPara( u.getDsEmail() );
					Mailer mailer2 = new Mailer( email, dsHostEnvio, dsPortaEnvio, dsEmailEnvio, dsSenhaEnvio );
					mailer2.send();
				}
				
			}

			
		} catch (Exception e) {
			e.printStackTrace();
			mensagemEnviada.setDsErroEmail(Util.gravaTrace(e));
		}
	}

	private String getCampoHtml(String nomeCampo, String dsCampo) {
		StringBuilder sb = new StringBuilder();
		sb.append("<br/>");
		sb.append("<b>");
		sb.append(nomeCampo);
		sb.append(":&nbsp;</b>");
		sb.append(dsCampo);

		return sb.toString();
	}

	private boolean compara(boolean atual, boolean novo) {
		return compara(atual ? "S" : "N", novo ? "S" : "N");
	}

	private boolean compara(Integer atual, Integer novo) {
		
		if( atual == null ) {
			atual = 0;
		}
		if( novo == null ) {
			novo = 0;
		}
		
		if( !atual.equals( novo ) ) {
			return true;
		}
		
		return false;
	}
	
	private boolean compara(String atual, String novo) {

		if (atual == null) {
			atual = "";
		}

		if (novo == null) {
			novo = "";
		}

		if (!atual.trim().equals(novo.trim())) {
			return true;
		}
		return false;
	}

	private static void initMensagemParametro(Conexao cnx) throws QtSQLException {
		if (mensagemParametro == null) {
			mensagemParametro = MensagemParametroLocalizador.buscaMensagemParametro(cnx, 1);
		}
	}

}