package jPcmso.negocio.geral.p;

import quatro.negocio.NegocioBasico;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.sql.Query;
import jPcmso.persistencia.geral.p.Procedimento;
import jPcmso.persistencia.geral.p.ProcedimentoPersistor;

/**
 *
 * Classe de Neg�cio para PCMSO.PROCEDIMENTOS
 *
 * @author Samuel Antonio Klein
 * @see quatro.negocio.NegocioBasico
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 20/12/2015 22:02 - importado por Samuel Antonio Klein
 *
 */
public class ProcedimentoNegocio extends NegocioBasico {

	/**
	 * M�todo Construtor b�sico.
	 *
	 */
	public ProcedimentoNegocio() throws QtSQLException {

		ProcedimentoPersistor persistor = new ProcedimentoPersistor();
		setPersistor( persistor );
	}

	/**
	 * M�todo Construtor que recebe uma conex�o.
	 *
	 */
	public ProcedimentoNegocio( Conexao cnx ) throws QtSQLException {

		ProcedimentoPersistor persistor = new ProcedimentoPersistor();
		setPersistor( persistor );
		setConexao( cnx );
	}

	/**
	 * M�todo Construtor que recebe uma Tabela B�sica de PCMSO.PROCEDIMENTOS
	 *
	 */
	public ProcedimentoNegocio( Procedimento procedimento ) throws QtSQLException {

		ProcedimentoPersistor persistor = new ProcedimentoPersistor( procedimento );
		setPersistor( persistor );
	}

	/**
	 * M�todo Construtor que recebe uma conex�o e uma Tabela B�sica de PCMSO.PROCEDIMENTOS
	 *
	 */
	public ProcedimentoNegocio( Conexao cnx, Procedimento procedimento ) throws QtSQLException {

		ProcedimentoPersistor persistor = new ProcedimentoPersistor( procedimento );
		setPersistor( persistor );
		setConexao( cnx );
	}

	/**
	 * M�todo que retorna uma nova Tabela B�sica de PCMSO.PROCEDIMENTOS
	 *
	 */
	public TabelaBasica geraNovaTabelaBasica() {

		return new Procedimento();

	}

	public boolean antesDeInserir() throws QtSQLException {

		if( testaRegrasBasicas() ) {
			return testaReferencias();
		}
		return false;
	}

	public boolean antesDeAlterar() throws QtSQLException {

		if( testaRegrasBasicas() ) {
			return testaReferencias();
		}
		return false;
	}

	private boolean testaReferencias() throws QtSQLException {

		
		return true;
	}

	public boolean antesDeExcluir() {

		try {
			Procedimento procedimento = (Procedimento) getPersistor().getTabelaBasica();

			Conexao cnx = getConexao();
			Query query = new Query( cnx );

			try {
				query.setSQL( "select count(*) from PCMSO.EXAMES_OCUPACIONAIS " );
				query.addSQL( " where CD_PROCEDIMENTO like :prm8CdProcedimento" );
				query.setParameter( "prm8CdProcedimento", procedimento.getCdProcedimento() );
				query.executeQuery();

				if( query.getInt( 1 ) > 0 ) {
					setMsgErro( "N�o posso excluir registro em PCMSO.PROCEDIMENTOS( " + procedimento.getCdProcedimento()+ " ) pois o mesmo � referenciado em PCMSO.EXAMES_OCUPACIONAIS" );
					return false;
				}
			
				query.setSQL( "select count(*) from PCMSO.EXAMES_COMPLEMENTARES " );
				query.addSQL( " where CD_PROCEDIMENTO like :prm3CdProcedimento" );
				query.setParameter( "prm3CdProcedimento", procedimento.getCdProcedimento() );
				query.executeQuery();

				if( query.getInt( 1 ) > 0 ) {
					setMsgErro( "N�o posso excluir registro em PCMSO.PROCEDIMENTOS( " + procedimento.getCdProcedimento()+ " ) pois o mesmo � referenciado em PCMSO.EXAMES_COMPLEMENTARES" );
					return false;
				}
			
				query.setSQL( "select count(*) from PCMSO.EXAMES_BENEFICIARIO " );
				query.addSQL( " where CD_PROCEDIMENTO like :prm2CdProcedimento" );
				query.setParameter( "prm2CdProcedimento", procedimento.getCdProcedimento() );
				query.executeQuery();

				if( query.getInt( 1 ) > 0 ) {
					setMsgErro( "N�o posso excluir registro em PCMSO.PROCEDIMENTOS( " + procedimento.getCdProcedimento()+ " ) pois o mesmo � referenciado em PCMSO.EXAMES_BENEFICIARIO" );
					return false;
				}
			
				query.setSQL( "select count(*) from PCMSO.EXAMES_COMPLEMENTARES_POR_CBO " );
				query.addSQL( " where CD_PROCEDIMENTO like :prm1CdProcedimento" );
				query.setParameter( "prm1CdProcedimento", procedimento.getCdProcedimento() );
				query.executeQuery();

				if( query.getInt( 1 ) > 0 ) {
					setMsgErro( "N�o posso excluir registro em PCMSO.PROCEDIMENTOS( " + procedimento.getCdProcedimento()+ " ) pois o mesmo � referenciado em PCMSO.EXAMES_COMPLEMENTARES_POR_CBO" );
					return false;
				}
			} finally {
				query.liberaRecursos();
			}
		} catch ( Exception e ) {
			this.setMsgErro( e.getMessage() );
			return false;
		}
		return true;
	}

	public boolean testaRegrasBasicas() {

		Procedimento procedimento = (Procedimento) getPersistor().getTabelaBasica();

		if( procedimento.getCdProcedimento() == null  || procedimento.getCdProcedimento().trim().equals( "" )  ) {
			setMsgErro( "Codigo do procedimento necessita estar preenchido.", "CD_PROCEDIMENTO" );
			return false;
		}

		return true;
	}
}