package jPcmso.negocio.geral.p;

import quatro.negocio.NegocioBasico;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.sql.Query;
import jPcmso.persistencia.geral.p.PerfilProfissiografico;
import jPcmso.persistencia.geral.p.PerfilProfissiograficoPersistor;

/**
 *
 * Classe de Negãcio para PCMSO.PERFIL_PROFISSIOGRAFICO
 *
 * @author Samuel Antonio Klein
 * @see quatro.negocio.NegocioBasico
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:31 - importado por Samuel Antonio Klein
 *
 */
public class PerfilProfissiograficoNegocio extends NegocioBasico {

	/**
	 * Mãtodo Construtor bãsico.
	 *
	 */
	public PerfilProfissiograficoNegocio() throws QtSQLException {

		PerfilProfissiograficoPersistor persistor = new PerfilProfissiograficoPersistor();
		setPersistor( persistor );
	}

	/**
	 * Mãtodo Construtor que recebe uma conexão.
	 *
	 */
	public PerfilProfissiograficoNegocio( Conexao cnx ) throws QtSQLException {

		PerfilProfissiograficoPersistor persistor = new PerfilProfissiograficoPersistor();
		setPersistor( persistor );
		setConexao( cnx );
	}

	/**
	 * Mãtodo Construtor que recebe uma Tabela Bãsica de PCMSO.PERFIL_PROFISSIOGRAFICO
	 *
	 */
	public PerfilProfissiograficoNegocio( PerfilProfissiografico perfilProfissiografico ) throws QtSQLException {

		PerfilProfissiograficoPersistor persistor = new PerfilProfissiograficoPersistor( perfilProfissiografico );
		setPersistor( persistor );
	}

	/**
	 * Mãtodo Construtor que recebe uma conexão e uma Tabela Bãsica de PCMSO.PERFIL_PROFISSIOGRAFICO
	 *
	 */
	public PerfilProfissiograficoNegocio( Conexao cnx, PerfilProfissiografico perfilProfissiografico ) throws QtSQLException {

		PerfilProfissiograficoPersistor persistor = new PerfilProfissiograficoPersistor( perfilProfissiografico );
		setPersistor( persistor );
		setConexao( cnx );
	}

	/**
	 * Mãtodo que retorna uma nova Tabela Bãsica de PCMSO.PERFIL_PROFISSIOGRAFICO
	 *
	 */
	public TabelaBasica geraNovaTabelaBasica() {

		return new PerfilProfissiografico();

	}

	public boolean antesDeInserir() throws QtSQLException {

		if( testaRegrasBasicas() ) {
			return testaReferencias();
		}
		return false;
	}

	public boolean antesDeAlterar() throws QtSQLException {

		if( testaRegrasBasicas() ) {
			return testaReferencias();
		}
		return false;
	}

	private boolean testaReferencias() throws QtSQLException {

		
		return true;
	}

	public boolean antesDeExcluir() {

		try {
			PerfilProfissiografico perfilProfissiografico = (PerfilProfissiografico) getPersistor().getTabelaBasica();

			Conexao cnx = getConexao();
			Query query = new Query( cnx );

			try {
				query.setSQL( "select count(*) from PCMSO.COMUNICACAO_ACIDENTE_TRABALHO " );
				query.addSQL( " where CD_BENEFICIARIO_CARTAO like :prm1CdBeneficiarioCartao and SQ_PERFIL = :prm2SqPerfil" );
				query.setParameter( "prm1CdBeneficiarioCartao", perfilProfissiografico.getCdBeneficiarioCartao() );
				query.setParameter( "prm2SqPerfil", perfilProfissiografico.getSqPerfil() );
				query.executeQuery();

				if( query.getInt( 1 ) > 0 ) {
					setMsgErro( "Não posso excluir registro em PCMSO.PERFIL_PROFISSIOGRAFICO( " + perfilProfissiografico.getCdBeneficiarioCartao()+ ", " + perfilProfissiografico.getSqPerfil()+ " ) pois o mesmo ã referenciado em PCMSO.COMUNICACAO_ACIDENTE_TRABALHO" );
					return false;
				}
			
				query.setSQL( "select count(*) from PCMSO.LOTACAO_ATRIBUICAO " );
				query.addSQL( " where CD_BENEFICIARIO_CARTAO like :prm1CdBeneficiarioCartao and SQ_PERFIL = :prm2SqPerfil" );
				query.setParameter( "prm1CdBeneficiarioCartao", perfilProfissiografico.getCdBeneficiarioCartao() );
				query.setParameter( "prm2SqPerfil", perfilProfissiografico.getSqPerfil() );
				query.executeQuery();

				if( query.getInt( 1 ) > 0 ) {
					setMsgErro( "Não posso excluir registro em PCMSO.PERFIL_PROFISSIOGRAFICO( " + perfilProfissiografico.getCdBeneficiarioCartao()+ ", " + perfilProfissiografico.getSqPerfil()+ " ) pois o mesmo ã referenciado em PCMSO.LOTACAO_ATRIBUICAO" );
					return false;
				}
			
				query.setSQL( "select count(*) from PCMSO.FATORES_RISCOS " );
				query.addSQL( " where CD_BENEFICIARIO_CARTAO like :prm1CdBeneficiarioCartao and SQ_PERFIL = :prm2SqPerfil" );
				query.setParameter( "prm1CdBeneficiarioCartao", perfilProfissiografico.getCdBeneficiarioCartao() );
				query.setParameter( "prm2SqPerfil", perfilProfissiografico.getSqPerfil() );
				query.executeQuery();

				if( query.getInt( 1 ) > 0 ) {
					setMsgErro( "Não posso excluir registro em PCMSO.PERFIL_PROFISSIOGRAFICO( " + perfilProfissiografico.getCdBeneficiarioCartao()+ ", " + perfilProfissiografico.getSqPerfil()+ " ) pois o mesmo ã referenciado em PCMSO.FATORES_RISCOS" );
					return false;
				}
			
				query.setSQL( "select count(*) from PCMSO.RESPONSAVEL_REGISTRO_AMBIENTAL " );
				query.addSQL( " where CD_BENEFICIARIO_CARTAO like :prm1CdBeneficiarioCartao and SQ_PERFIL = :prm2SqPerfil" );
				query.setParameter( "prm1CdBeneficiarioCartao", perfilProfissiografico.getCdBeneficiarioCartao() );
				query.setParameter( "prm2SqPerfil", perfilProfissiografico.getSqPerfil() );
				query.executeQuery();

				if( query.getInt( 1 ) > 0 ) {
					setMsgErro( "Não posso excluir registro em PCMSO.PERFIL_PROFISSIOGRAFICO( " + perfilProfissiografico.getCdBeneficiarioCartao()+ ", " + perfilProfissiografico.getSqPerfil()+ " ) pois o mesmo ã referenciado em PCMSO.RESPONSAVEL_REGISTRO_AMBIENTAL" );
					return false;
				}
			} finally {
				query.liberaRecursos();
			}
		} catch ( Exception e ) {
			this.setMsgErro( e.getMessage() );
			return false;
		}
		return true;
	}

	public boolean testaRegrasBasicas() {

		PerfilProfissiografico perfilProfissiografico = (PerfilProfissiografico) getPersistor().getTabelaBasica();

		if( perfilProfissiografico.getCdBeneficiarioCartao() == null  || perfilProfissiografico.getCdBeneficiarioCartao().trim().equals( "" )  ) {
			setMsgErro( "Codigo de Beneficiario  necessita estar preenchido.", "CD_BENEFICIARIO_CARTAO" );
			return false;
		}

		if( perfilProfissiografico.getSqPerfil() == null  ) {
			setMsgErro( "sequencia perfil necessita estar preenchido.", "SQ_PERFIL" );
			return false;
		}

		if( perfilProfissiografico.getDtGeracao() == null  ) {
			setMsgErro( " necessita estar preenchido.", "DT_GERACAO" );
			return false;
		}

		if( perfilProfissiografico.getTpBrPdh() != null && !perfilProfissiografico.getTpBrPdh().trim().equals( "" ) && !perfilProfissiografico.getTpBrPdh().matches( "N|B|P" ) ) {
			setMsgErro( "Tipo de BR PDH invãlido (" + perfilProfissiografico.getTpBrPdh() + "): precisa ser N-Não aplicavel (NA), B-Beneficiario Reabilitado (BR) ou P-Portador de Deficiencia Habilitado (PDH)", "TP_BR_PDH" );
			return false;
		}

		if( perfilProfissiografico.getTpSexo() != null && !perfilProfissiografico.getTpSexo().trim().equals( "" ) && !perfilProfissiografico.getTpSexo().matches( "M|F" ) ) {
			setMsgErro( "Tipo de sexo invãlido (" + perfilProfissiografico.getTpSexo() + "): precisa ser M-Masculino ou F-feminino", "TP_SEXO" );
			return false;
		}

		return true;
	}
}