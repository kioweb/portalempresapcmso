package jPcmso.negocio.geral.t;

import quatro.negocio.NegocioBasico;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.sql.Query;
import jPcmso.persistencia.geral.t.TipoDeRisco;
import jPcmso.persistencia.geral.t.TipoDeRiscoPersistor;

/**
 *
 * Classe de Neg�cio para PCMSO.TIPOS_RISCOS
 *
 * @author Samuel Antonio Klein
 * @see quatro.negocio.NegocioBasico
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
public class TipoDeRiscoNegocio extends NegocioBasico {

	/**
	 * M�todo Construtor b�sico.
	 *
	 */
	public TipoDeRiscoNegocio() throws QtSQLException {

		TipoDeRiscoPersistor persistor = new TipoDeRiscoPersistor();
		setPersistor( persistor );
	}

	/**
	 * M�todo Construtor que recebe uma conex�o.
	 *
	 */
	public TipoDeRiscoNegocio( Conexao cnx ) throws QtSQLException {

		TipoDeRiscoPersistor persistor = new TipoDeRiscoPersistor();
		setPersistor( persistor );
		setConexao( cnx );
	}

	/**
	 * M�todo Construtor que recebe uma Tabela B�sica de PCMSO.TIPOS_RISCOS
	 *
	 */
	public TipoDeRiscoNegocio( TipoDeRisco tipoDeRisco ) throws QtSQLException {

		TipoDeRiscoPersistor persistor = new TipoDeRiscoPersistor( tipoDeRisco );
		setPersistor( persistor );
	}

	/**
	 * M�todo Construtor que recebe uma conex�o e uma Tabela B�sica de PCMSO.TIPOS_RISCOS
	 *
	 */
	public TipoDeRiscoNegocio( Conexao cnx, TipoDeRisco tipoDeRisco ) throws QtSQLException {

		TipoDeRiscoPersistor persistor = new TipoDeRiscoPersistor( tipoDeRisco );
		setPersistor( persistor );
		setConexao( cnx );
	}

	/**
	 * M�todo que retorna uma nova Tabela B�sica de PCMSO.TIPOS_RISCOS
	 *
	 */
	public TabelaBasica geraNovaTabelaBasica() {

		return new TipoDeRisco();

	}

	public boolean antesDeInserir() throws QtSQLException {

		if( testaRegrasBasicas() ) {
			return testaReferencias();
		}
		return false;
	}

	public boolean antesDeAlterar() throws QtSQLException {

		if( testaRegrasBasicas() ) {
			return testaReferencias();
		}
		return false;
	}

	private boolean testaReferencias() throws QtSQLException {

		
		return true;
	}

	public boolean antesDeExcluir() {

		try {
			TipoDeRisco tipoDeRisco = (TipoDeRisco) getPersistor().getTabelaBasica();

			Conexao cnx = getConexao();
			Query query = new Query( cnx );

			try {
				query.setSQL( "select count(*) from PCMSO.RISCOS " );
				query.addSQL( " where TP_RISCO like :prm2TpRisco" );
				query.setParameter( "prm2TpRisco", tipoDeRisco.getTpRisco() );
				query.executeQuery();

				if( query.getInt( 1 ) > 0 ) {
					setMsgErro( "N�o posso excluir registro em PCMSO.TIPOS_RISCOS( " + tipoDeRisco.getTpRisco()+ " ) pois o mesmo � referenciado em PCMSO.RISCOS" );
					return false;
				}
			} finally {
				query.liberaRecursos();
			}
		} catch ( Exception e ) {
			this.setMsgErro( e.getMessage() );
			return false;
		}
		return true;
	}

	public boolean testaRegrasBasicas() {

		TipoDeRisco tipoDeRisco = (TipoDeRisco) getPersistor().getTabelaBasica();

		if( tipoDeRisco.getTpRisco() == null  || tipoDeRisco.getTpRisco().trim().equals( "" )  ) {
			setMsgErro( "Tipo de risco necessita estar preenchido.", "TP_RISCO" );
			return false;
		}

		return true;
	}
}