package jPcmso.negocio.geral.t;

import jPcmso.persistencia.geral.t.TipoDeRisco;
import jPcmso.persistencia.geral.t.TipoDeRiscoLocalizador;
import quatro.negocio.ErroDeNegocio;
import quatro.negocio.NegocioEspecificoImpl;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;

public class TipoDeRiscoNegocioEspecifico extends NegocioEspecificoImpl {
	
	public void antesDeInserir(Conexao cnx, TabelaBasica registro) throws QtSQLException, ErroDeNegocio {
		TipoDeRisco t = (TipoDeRisco) registro;
		
		if( TipoDeRiscoLocalizador.existe( cnx, t ) ){
			throw new ErroDeNegocio( "J� existe na base de dados um TipoDeRisco " + t.getTpRisco() );
		}
		
	}
	
}