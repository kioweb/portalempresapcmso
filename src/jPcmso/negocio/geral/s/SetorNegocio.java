package jPcmso.negocio.geral.s;

import quatro.negocio.NegocioBasico;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.sql.Query;
import jPcmso.persistencia.geral.s.Setor;
import jPcmso.persistencia.geral.s.SetorPersistor;

/**
 *
 * Classe de Negócio para PCMSO.SETORES
 *
 * @author ronei@unimed-uau.com.br
 * @see quatro.negocio.NegocioBasico
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 14/12/2017 15:26 - importado por ronei@unimed-uau.com.br
 *
 */
public class SetorNegocio extends NegocioBasico {

	/**
	 * Método Construtor básico.
	 *
	 */
	public SetorNegocio() throws QtSQLException {

		SetorPersistor persistor = new SetorPersistor();
		setPersistor( persistor );
	}

	/**
	 * Método Construtor que recebe uma conexão.
	 *
	 */
	public SetorNegocio( Conexao cnx ) throws QtSQLException {

		SetorPersistor persistor = new SetorPersistor();
		setPersistor( persistor );
		setConexao( cnx );
	}

	/**
	 * Método Construtor que recebe uma Tabela Básica de PCMSO.SETORES
	 *
	 */
	public SetorNegocio( Setor setor ) throws QtSQLException {

		SetorPersistor persistor = new SetorPersistor( setor );
		setPersistor( persistor );
	}

	/**
	 * Método Construtor que recebe uma conexão e uma Tabela Básica de PCMSO.SETORES
	 *
	 */
	public SetorNegocio( Conexao cnx, Setor setor ) throws QtSQLException {

		SetorPersistor persistor = new SetorPersistor( setor );
		setPersistor( persistor );
		setConexao( cnx );
	}

	/**
	 * Método que retorna uma nova Tabela Básica de PCMSO.SETORES
	 *
	 */
	public TabelaBasica geraNovaTabelaBasica() {

		return new Setor();

	}

	public boolean antesDeInserir() throws QtSQLException {

		testaPersistor();
		Setor setor = (Setor) getPersistor().getTabelaBasica();
		SetorPersistor persistor = (SetorPersistor) getPersistor();

		if ( setor.getIdSetor() == null ) {

			try {
				setor.setIdSetor( SetorPersistor.geraNovoIdSetor() );
			} catch (QtSQLException e) {
				setMsgErro( "SetorNegocio - Antes de Inserir: Erro na Geração do Identificador: " + e.getMessage() );
				return false;
			}
		}
		if( testaRegrasBasicas() ) {
			return testaReferencias();
		}
		return false;
	}

	public boolean antesDeAlterar() throws QtSQLException {

		if( testaRegrasBasicas() ) {
			return testaReferencias();
		}
		return false;
	}

	private boolean testaReferencias() throws QtSQLException {

		
		return true;
	}

	public boolean antesDeExcluir() {

		try {
			Setor setor = (Setor) getPersistor().getTabelaBasica();

			Conexao cnx = getConexao();
			Query query = new Query( cnx );

			try {
				query.setSQL( "select count(*) from PCMSO.BENEFICIARIOS_CBO " );
				query.addSQL( " where ID_SETOR = :prm9IdSetor" );
				query.setParameter( "prm9IdSetor", setor.getIdSetor() );
				query.executeQuery();

				if( query.getInt( 1 ) > 0 ) {
					setMsgErro( "Não posso excluir registro em PCMSO.SETORES( " + setor.getIdSetor()+ " ) pois o mesmo é referenciado em PCMSO.BENEFICIARIOS_CBO" );
					return false;
				}
			
				query.setSQL( "select count(*) from PCMSO.HISTORICO_BENEFICIARIOS_CBO " );
				query.addSQL( " where ID_SETOR = :prm4IdSetor" );
				query.setParameter( "prm4IdSetor", setor.getIdSetor() );
				query.executeQuery();

				if( query.getInt( 1 ) > 0 ) {
					setMsgErro( "Não posso excluir registro em PCMSO.SETORES( " + setor.getIdSetor()+ " ) pois o mesmo é referenciado em PCMSO.HISTORICO_BENEFICIARIOS_CBO" );
					return false;
				}
			
				query.setSQL( "select count(*) from PCMSO.EXAMES_OCUPACIONAIS " );
				query.addSQL( " where ID_SETOR = :prm2IdSetor" );
				query.setParameter( "prm2IdSetor", setor.getIdSetor() );
				query.executeQuery();

				if( query.getInt( 1 ) > 0 ) {
					setMsgErro( "Não posso excluir registro em PCMSO.SETORES( " + setor.getIdSetor()+ " ) pois o mesmo é referenciado em PCMSO.EXAMES_OCUPACIONAIS" );
					return false;
				}
			
				query.setSQL( "select count(*) from PCMSO.LOTACAO_ATRIBUICAO " );
				query.addSQL( " where ID_SETOR = :prm8IdSetor" );
				query.setParameter( "prm8IdSetor", setor.getIdSetor() );
				query.executeQuery();

				if( query.getInt( 1 ) > 0 ) {
					setMsgErro( "Não posso excluir registro em PCMSO.SETORES( " + setor.getIdSetor()+ " ) pois o mesmo é referenciado em PCMSO.LOTACAO_ATRIBUICAO" );
					return false;
				}
			} finally {
				query.liberaRecursos();
			}
		} catch ( Exception e ) {
			this.setMsgErro( e.getMessage() );
			return false;
		}
		return true;
	}

	public boolean testaRegrasBasicas() {

		Setor setor = (Setor) getPersistor().getTabelaBasica();

		if( setor.getIdSetor() == null  ) {
			setMsgErro( "Codigo do setor necessita estar preenchido.", "ID_SETOR" );
			return false;
		}

		return true;
	}
}