package jPcmso.negocio.geral.s;

import jPcmso.persistencia.geral.s.Setor;
import quatro.negocio.ErroDeNegocio;
import quatro.negocio.NegocioEspecificoImpl;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;

public class SetorNergocioEspecifico extends NegocioEspecificoImpl {

	@Override
	public void antesDeAlterar( Conexao cnx, TabelaBasica registro ) throws QtSQLException, ErroDeNegocio {
		validaDescricao( registro );
	}

	@Override
	public void antesDeInserir( Conexao cnx, TabelaBasica registro ) throws QtSQLException, ErroDeNegocio {
		validaDescricao( registro );
	}
	
	private void validaDescricao( TabelaBasica registro ) throws ErroDeNegocio {
		Setor setor = (Setor) registro;
		if( setor != null && setor.getDsSetor() != null && !setor.getDsSetor().trim().equals( "" ) ) {
			if( setor.getDsSetor().length() > 70 ) {
				throw new ErroDeNegocio( "N�o � possivel gravar mais de 70 caracteres para o descri��o" );
			}
		}
	}

}
