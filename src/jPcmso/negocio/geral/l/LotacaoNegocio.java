package jPcmso.negocio.geral.l;

import quatro.negocio.NegocioBasico;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.sql.Query;
import jPcmso.persistencia.geral.l.Lotacao;
import jPcmso.persistencia.geral.l.LotacaoPersistor;

/**
 *
 * Classe de Neg�cio para LOTACOES
 *
 * @author Samuel Antonio Klein
 * @see quatro.negocio.NegocioBasico
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:34 - importado por Samuel Antonio Klein
 *
 */
public class LotacaoNegocio extends NegocioBasico {

	/**
	 * M�todo Construtor b�sico.
	 *
	 */
	public LotacaoNegocio() throws QtSQLException {

		LotacaoPersistor persistor = new LotacaoPersistor();
		setPersistor( persistor );
	}

	/**
	 * M�todo Construtor que recebe uma conex�o.
	 *
	 */
	public LotacaoNegocio( Conexao cnx ) throws QtSQLException {

		LotacaoPersistor persistor = new LotacaoPersistor();
		setPersistor( persistor );
		setConexao( cnx );
	}

	/**
	 * M�todo Construtor que recebe uma Tabela B�sica de LOTACOES
	 *
	 */
	public LotacaoNegocio( Lotacao lotacao ) throws QtSQLException {

		LotacaoPersistor persistor = new LotacaoPersistor( lotacao );
		setPersistor( persistor );
	}

	/**
	 * M�todo Construtor que recebe uma conex�o e uma Tabela B�sica de LOTACOES
	 *
	 */
	public LotacaoNegocio( Conexao cnx, Lotacao lotacao ) throws QtSQLException {

		LotacaoPersistor persistor = new LotacaoPersistor( lotacao );
		setPersistor( persistor );
		setConexao( cnx );
	}

	/**
	 * M�todo que retorna uma nova Tabela B�sica de LOTACOES
	 *
	 */
	public TabelaBasica geraNovaTabelaBasica() {

		return new Lotacao();

	}

	public boolean antesDeInserir() throws QtSQLException {

		if( testaRegrasBasicas() ) {
			return testaReferencias();
		}
		return false;
	}

	public boolean antesDeAlterar() throws QtSQLException {

		if( testaRegrasBasicas() ) {
			return testaReferencias();
		}
		return false;
	}

	private boolean testaReferencias() throws QtSQLException {

		
		return true;
	}

	public boolean antesDeExcluir() {

		try {
			Lotacao lotacao = (Lotacao) getPersistor().getTabelaBasica();

			Conexao cnx = getConexao();
			Query query = new Query( cnx );

			try {
				query.setSQL( "select count(*) from PCMSO.BENEFICIARIOS_CBO " );
				query.addSQL( " where ID_LOTACAO = :prm4IdLotacao" );
				query.setParameter( "prm4IdLotacao", lotacao.getIdLotacao() );
				query.executeQuery();

				if( query.getInt( 1 ) > 0 ) {
					setMsgErro( "N�o posso excluir registro em LOTACOES( " + lotacao.getIdLotacao()+ " ) pois o mesmo � referenciado em PCMSO.BENEFICIARIOS_CBO" );
					return false;
				}
			} finally {
				query.liberaRecursos();
			}
		} catch ( Exception e ) {
			this.setMsgErro( e.getMessage() );
			return false;
		}
		return true;
	}

	public boolean testaRegrasBasicas() {

		Lotacao lotacao = (Lotacao) getPersistor().getTabelaBasica();

		if( lotacao.getCdUnimed() == null  || lotacao.getCdUnimed().trim().equals( "" )  ) {
			setMsgErro( "C�digo da Unimed necessita estar preenchido.", "CD_UNIMED" );
			return false;
		}

		if( lotacao.getIdEmpresa() == null  ) {
			setMsgErro( "Identificador da Empresa necessita estar preenchido.", "ID_EMPRESA" );
			return false;
		}

		if( lotacao.getIdLotacao() == null  ) {
			setMsgErro( "Identificador da Lota��o necessita estar preenchido.", "ID_LOTACAO" );
			return false;
		}

		if( lotacao.getDsLotacao() == null  || lotacao.getDsLotacao().trim().equals( "" )  ) {
			setMsgErro( "Nome da Lota��o necessita estar preenchido.", "DS_LOTACAO" );
			return false;
		}

		return true;
	}
}