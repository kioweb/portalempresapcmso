package jPcmso.negocio.geral.l;

import quatro.negocio.NegocioBasico;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import jPcmso.persistencia.geral.l.LotacaoAtribuicao;
import jPcmso.persistencia.geral.l.LotacaoAtribuicaoPersistor;

/**
 *
 * Classe de Neg�cio para PCMSO.LOTACAO_ATRIBUICAO
 *
 * @author Samuel Antonio Klein
 * @see quatro.negocio.NegocioBasico
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:31 - importado por Samuel Antonio Klein
 *
 */
public class LotacaoAtribuicaoNegocio extends NegocioBasico {

	/**
	 * M�todo Construtor b�sico.
	 *
	 */
	public LotacaoAtribuicaoNegocio() throws QtSQLException {

		LotacaoAtribuicaoPersistor persistor = new LotacaoAtribuicaoPersistor();
		setPersistor( persistor );
	}

	/**
	 * M�todo Construtor que recebe uma conex�o.
	 *
	 */
	public LotacaoAtribuicaoNegocio( Conexao cnx ) throws QtSQLException {

		LotacaoAtribuicaoPersistor persistor = new LotacaoAtribuicaoPersistor();
		setPersistor( persistor );
		setConexao( cnx );
	}

	/**
	 * M�todo Construtor que recebe uma Tabela B�sica de PCMSO.LOTACAO_ATRIBUICAO
	 *
	 */
	public LotacaoAtribuicaoNegocio( LotacaoAtribuicao lotacaoAtribuicao ) throws QtSQLException {

		LotacaoAtribuicaoPersistor persistor = new LotacaoAtribuicaoPersistor( lotacaoAtribuicao );
		setPersistor( persistor );
	}

	/**
	 * M�todo Construtor que recebe uma conex�o e uma Tabela B�sica de PCMSO.LOTACAO_ATRIBUICAO
	 *
	 */
	public LotacaoAtribuicaoNegocio( Conexao cnx, LotacaoAtribuicao lotacaoAtribuicao ) throws QtSQLException {

		LotacaoAtribuicaoPersistor persistor = new LotacaoAtribuicaoPersistor( lotacaoAtribuicao );
		setPersistor( persistor );
		setConexao( cnx );
	}

	/**
	 * M�todo que retorna uma nova Tabela B�sica de PCMSO.LOTACAO_ATRIBUICAO
	 *
	 */
	public TabelaBasica geraNovaTabelaBasica() {

		return new LotacaoAtribuicao();

	}

	public boolean antesDeInserir() throws QtSQLException {

		if( testaRegrasBasicas() ) {
			return testaReferencias();
		}
		return false;
	}

	public boolean antesDeAlterar() throws QtSQLException {

		if( testaRegrasBasicas() ) {
			return testaReferencias();
		}
		return false;
	}

	private boolean testaReferencias() throws QtSQLException {

		
		return true;
	}

	public boolean antesDeExcluir() {

		return true;
	}

	public boolean testaRegrasBasicas() {

		LotacaoAtribuicao lotacaoAtribuicao = (LotacaoAtribuicao) getPersistor().getTabelaBasica();

		if( lotacaoAtribuicao.getCdBeneficiarioCartao() == null  || lotacaoAtribuicao.getCdBeneficiarioCartao().trim().equals( "" )  ) {
			setMsgErro( "Codigo de Beneficiario  necessita estar preenchido.", "CD_BENEFICIARIO_CARTAO" );
			return false;
		}

		if( lotacaoAtribuicao.getSqPerfil() == null  ) {
			setMsgErro( "sequencia perfil necessita estar preenchido.", "SQ_PERFIL" );
			return false;
		}

		if( lotacaoAtribuicao.getSqLotacao() == null  || lotacaoAtribuicao.getSqLotacao().trim().equals( "" )  ) {
			setMsgErro( "sequencia lotacao necessita estar preenchido.", "SQ_LOTACAO" );
			return false;
		}

		if( lotacaoAtribuicao.getIdSetor() == null  ) {
			setMsgErro( "Codigo do setor necessita estar preenchido.", "ID_SETOR" );
			return false;
		}

		if( lotacaoAtribuicao.getIdCargo() == null  ) {
			setMsgErro( "Codigo do cargo necessita estar preenchido.", "ID_CARGO" );
			return false;
		}

		if( lotacaoAtribuicao.getIdFuncao() == null  ) {
			setMsgErro( "Codigo do fun��o necessita estar preenchido.", "ID_FUNCAO" );
			return false;
		}

		if( lotacaoAtribuicao.getCdCbo() == null  || lotacaoAtribuicao.getCdCbo().trim().equals( "" )  ) {
			setMsgErro( "Codigo do CBO necessita estar preenchido.", "CD_CBO" );
			return false;
		}

		return true;
	}
}