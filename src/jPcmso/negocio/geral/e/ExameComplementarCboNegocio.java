package jPcmso.negocio.geral.e;

import quatro.negocio.NegocioBasico;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import jPcmso.persistencia.geral.e.ExameComplementarCbo;
import jPcmso.persistencia.geral.e.ExameComplementarCboPersistor;

/**
 *
 * Classe de Neg�cio para PCMSO.EXAMES_COMPLEMENTARES_POR_CBO
 *
 * @author Samuel Antonio Klein
 * @see quatro.negocio.NegocioBasico
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
public class ExameComplementarCboNegocio extends NegocioBasico {

	/**
	 * M�todo Construtor b�sico.
	 *
	 */
	public ExameComplementarCboNegocio() throws QtSQLException {

		ExameComplementarCboPersistor persistor = new ExameComplementarCboPersistor();
		setPersistor( persistor );
	}

	/**
	 * M�todo Construtor que recebe uma conex�o.
	 *
	 */
	public ExameComplementarCboNegocio( Conexao cnx ) throws QtSQLException {

		ExameComplementarCboPersistor persistor = new ExameComplementarCboPersistor();
		setPersistor( persistor );
		setConexao( cnx );
	}

	/**
	 * M�todo Construtor que recebe uma Tabela B�sica de PCMSO.EXAMES_COMPLEMENTARES_POR_CBO
	 *
	 */
	public ExameComplementarCboNegocio( ExameComplementarCbo exameComplementarCbo ) throws QtSQLException {

		ExameComplementarCboPersistor persistor = new ExameComplementarCboPersistor( exameComplementarCbo );
		setPersistor( persistor );
	}

	/**
	 * M�todo Construtor que recebe uma conex�o e uma Tabela B�sica de PCMSO.EXAMES_COMPLEMENTARES_POR_CBO
	 *
	 */
	public ExameComplementarCboNegocio( Conexao cnx, ExameComplementarCbo exameComplementarCbo ) throws QtSQLException {

		ExameComplementarCboPersistor persistor = new ExameComplementarCboPersistor( exameComplementarCbo );
		setPersistor( persistor );
		setConexao( cnx );
	}

	/**
	 * M�todo que retorna uma nova Tabela B�sica de PCMSO.EXAMES_COMPLEMENTARES_POR_CBO
	 *
	 */
	public TabelaBasica geraNovaTabelaBasica() {

		return new ExameComplementarCbo();

	}

	public boolean antesDeInserir() throws QtSQLException {

		if( testaRegrasBasicas() ) {
			return testaReferencias();
		}
		return false;
	}

	public boolean antesDeAlterar() throws QtSQLException {

		if( testaRegrasBasicas() ) {
			return testaReferencias();
		}
		return false;
	}

	private boolean testaReferencias() throws QtSQLException {

		
		return true;
	}

	public boolean antesDeExcluir() {

		return true;
	}

	public boolean testaRegrasBasicas() {

		ExameComplementarCbo exameComplementarCbo = (ExameComplementarCbo) getPersistor().getTabelaBasica();

		if( exameComplementarCbo.getCdProcedimento() == null  || exameComplementarCbo.getCdProcedimento().trim().equals( "" )  ) {
			setMsgErro( "Codigo do procedimento necessita estar preenchido.", "CD_PROCEDIMENTO" );
			return false;
		}

		if( exameComplementarCbo.getCdCbo() == null  || exameComplementarCbo.getCdCbo().trim().equals( "" )  ) {
			setMsgErro( "Codigo de CBO necessita estar preenchido.", "CD_CBO" );
			return false;
		}

		return true;
	}
}