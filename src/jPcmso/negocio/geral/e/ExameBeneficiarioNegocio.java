package jPcmso.negocio.geral.e;

import quatro.negocio.NegocioBasico;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import jPcmso.persistencia.geral.e.ExameBeneficiario;
import jPcmso.persistencia.geral.e.ExameBeneficiarioPersistor;

/**
 *
 * Classe de Neg�cio para PCMSO.EXAMES_BENEFICIARIO
 *
 * @author Samuel Antonio Klein
 * @see quatro.negocio.NegocioBasico
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
public class ExameBeneficiarioNegocio extends NegocioBasico {

	/**
	 * M�todo Construtor b�sico.
	 *
	 */
	public ExameBeneficiarioNegocio() throws QtSQLException {

		ExameBeneficiarioPersistor persistor = new ExameBeneficiarioPersistor();
		setPersistor( persistor );
	}

	/**
	 * M�todo Construtor que recebe uma conex�o.
	 *
	 */
	public ExameBeneficiarioNegocio( Conexao cnx ) throws QtSQLException {

		ExameBeneficiarioPersistor persistor = new ExameBeneficiarioPersistor();
		setPersistor( persistor );
		setConexao( cnx );
	}

	/**
	 * M�todo Construtor que recebe uma Tabela B�sica de PCMSO.EXAMES_BENEFICIARIO
	 *
	 */
	public ExameBeneficiarioNegocio( ExameBeneficiario exameBeneficiario ) throws QtSQLException {

		ExameBeneficiarioPersistor persistor = new ExameBeneficiarioPersistor( exameBeneficiario );
		setPersistor( persistor );
	}

	/**
	 * M�todo Construtor que recebe uma conex�o e uma Tabela B�sica de PCMSO.EXAMES_BENEFICIARIO
	 *
	 */
	public ExameBeneficiarioNegocio( Conexao cnx, ExameBeneficiario exameBeneficiario ) throws QtSQLException {

		ExameBeneficiarioPersistor persistor = new ExameBeneficiarioPersistor( exameBeneficiario );
		setPersistor( persistor );
		setConexao( cnx );
	}

	/**
	 * M�todo que retorna uma nova Tabela B�sica de PCMSO.EXAMES_BENEFICIARIO
	 *
	 */
	public TabelaBasica geraNovaTabelaBasica() {

		return new ExameBeneficiario();

	}

	public boolean antesDeInserir() throws QtSQLException {

		if( testaRegrasBasicas() ) {
			return testaReferencias();
		}
		return false;
	}

	public boolean antesDeAlterar() throws QtSQLException {

		if( testaRegrasBasicas() ) {
			return testaReferencias();
		}
		return false;
	}

	private boolean testaReferencias() throws QtSQLException {

		
		return true;
	}

	public boolean antesDeExcluir() {

		return true;
	}

	public boolean testaRegrasBasicas() {

		ExameBeneficiario exameBeneficiario = (ExameBeneficiario) getPersistor().getTabelaBasica();

		if( exameBeneficiario.getCdBeneficiarioCartao() == null  || exameBeneficiario.getCdBeneficiarioCartao().trim().equals( "" )  ) {
			setMsgErro( "Codigo de beneficiario  necessita estar preenchido.", "CD_BENEFICIARIO_CARTAO" );
			return false;
		}

		if( exameBeneficiario.getCdProcedimento() == null  || exameBeneficiario.getCdProcedimento().trim().equals( "" )  ) {
			setMsgErro( "Codigo do procedimento necessita estar preenchido.", "CD_PROCEDIMENTO" );
			return false;
		}

		return true;
	}
}