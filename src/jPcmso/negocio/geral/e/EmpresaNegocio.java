package jPcmso.negocio.geral.e;

import quatro.negocio.NegocioBasico;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.sql.Query;
import jPcmso.persistencia.geral.e.Empresa;
import jPcmso.persistencia.geral.e.EmpresaPersistor;

/**
 *
 * Classe de Neg�cio para PCMSO.EMPRESAS
 *
 * @author Samuel Antonio Klein
 * @see quatro.negocio.NegocioBasico
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
public class EmpresaNegocio extends NegocioBasico {

	/**
	 * M�todo Construtor b�sico.
	 *
	 */
	public EmpresaNegocio() throws QtSQLException {

		EmpresaPersistor persistor = new EmpresaPersistor();
		setPersistor( persistor );
	}

	/**
	 * M�todo Construtor que recebe uma conex�o.
	 *
	 */
	public EmpresaNegocio( Conexao cnx ) throws QtSQLException {

		EmpresaPersistor persistor = new EmpresaPersistor();
		setPersistor( persistor );
		setConexao( cnx );
	}

	/**
	 * M�todo Construtor que recebe uma Tabela B�sica de PCMSO.EMPRESAS
	 *
	 */
	public EmpresaNegocio( Empresa empresa ) throws QtSQLException {

		EmpresaPersistor persistor = new EmpresaPersistor( empresa );
		setPersistor( persistor );
	}

	/**
	 * M�todo Construtor que recebe uma conex�o e uma Tabela B�sica de PCMSO.EMPRESAS
	 *
	 */
	public EmpresaNegocio( Conexao cnx, Empresa empresa ) throws QtSQLException {

		EmpresaPersistor persistor = new EmpresaPersistor( empresa );
		setPersistor( persistor );
		setConexao( cnx );
	}

	/**
	 * M�todo que retorna uma nova Tabela B�sica de PCMSO.EMPRESAS
	 *
	 */
	public TabelaBasica geraNovaTabelaBasica() {

		return new Empresa();

	}

	public boolean antesDeInserir() throws QtSQLException {

		if( testaRegrasBasicas() ) {
			return testaReferencias();
		}
		return false;
	}

	public boolean antesDeAlterar() throws QtSQLException {

		if( testaRegrasBasicas() ) {
			return testaReferencias();
		}
		return false;
	}

	private boolean testaReferencias() throws QtSQLException {

		
		return true;
	}

	public boolean antesDeExcluir() {

		try {
			Empresa empresa = (Empresa) getPersistor().getTabelaBasica();

			Conexao cnx = getConexao();
			Query query = new Query( cnx );

			try {
				query.setSQL( "select count(*) from PCMSO.RISCOS_EMPRESA " );
				query.addSQL( " where CD_EMPRESA like :prm2CdEmpresa" );
				query.setParameter( "prm2CdEmpresa", empresa.getCdEmpresa() );
				query.executeQuery();

				if( query.getInt( 1 ) > 0 ) {
					setMsgErro( "Não posso excluir registro em PCMSO.EMPRESAS( " + empresa.getCdEmpresa()+ " ) pois o mesmo � referenciado em PCMSO.RISCOS_EMPRESA" );
					return false;
				}
			
				query.setSQL( "select count(*) from PCMSO.FILIAIS_EMPRESAS " );
				query.addSQL( " where CD_EMPRESA like :prm1CdEmpresa" );
				query.setParameter( "prm1CdEmpresa", empresa.getCdEmpresa() );
				query.executeQuery();

				if( query.getInt( 1 ) > 0 ) {
					setMsgErro( "N�o posso excluir registro em PCMSO.EMPRESAS( " + empresa.getCdEmpresa()+ " ) pois o mesmo � referenciado em PCMSO.FILIAIS_EMPRESAS" );
					return false;
				}
			
//				query.setSQL( "select count(*) from PCMSO.PERFIL_PROFISSIOGRAFICO " );
//				query.addSQL( " where CD_EMPRESA like :prm3CdEmpresa" );
//				query.setParameter( "prm3CdEmpresa", empresa.getCdEmpresa() );
//				query.executeQuery();
//
//				if( query.getInt( 1 ) > 0 ) {
//					setMsgErro( "N�o posso excluir registro em PCMSO.EMPRESAS( " + empresa.getCdEmpresa()+ " ) pois o mesmo � referenciado em PCMSO.PERFIL_PROFISSIOGRAFICO" );
//					return false;
//				}
			
//				query.setSQL( "select count(*) from PCMSO.LOTACAO_ATRIBUICAO " );
//				query.addSQL( " where CD_EMPRESA like :prm6CdEmpresa" );
//				query.setParameter( "prm6CdEmpresa", empresa.getCdEmpresa() );
//				query.executeQuery();
//
//				if( query.getInt( 1 ) > 0 ) {
//					setMsgErro( "N�o posso excluir registro em PCMSO.EMPRESAS( " + empresa.getCdEmpresa()+ " ) pois o mesmo � referenciado em PCMSO.LOTACAO_ATRIBUICAO" );
//					return false;
//				}
			} finally {
				query.liberaRecursos();
			}
		} catch ( Exception e ) {
			this.setMsgErro( e.getMessage() );
			return false;
		}
		return true;
	}

	public boolean testaRegrasBasicas() {

		Empresa empresa = (Empresa) getPersistor().getTabelaBasica();

		if( empresa.getCdEmpresa() == null  || empresa.getCdEmpresa().trim().equals( "" )  ) {
			setMsgErro( "Codigo da Empresa necessita estar preenchido.", "CD_EMPRESA" );
			return false;
		}

		return true;
	}
}