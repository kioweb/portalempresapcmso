package jPcmso.negocio.geral.e;

import quatro.negocio.NegocioBasico;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.sql.Query;
import jPcmso.persistencia.geral.e.ExameOcupacional;
import jPcmso.persistencia.geral.e.ExameOcupacionalPersistor;

/**
 *
 * Classe de Neg�cio para PCMSO.EXAMES_OCUPACIONAIS
 *
 * @author Samuel Antonio Klein
 * @see quatro.negocio.NegocioBasico
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
public class ExameOcupacionalNegocio extends NegocioBasico {

	/**
	 * M�todo Construtor b�sico.
	 *
	 */
	public ExameOcupacionalNegocio() throws QtSQLException {

		ExameOcupacionalPersistor persistor = new ExameOcupacionalPersistor();
		setPersistor( persistor );
	}

	/**
	 * M�todo Construtor que recebe uma conex�o.
	 *
	 */
	public ExameOcupacionalNegocio( Conexao cnx ) throws QtSQLException {

		ExameOcupacionalPersistor persistor = new ExameOcupacionalPersistor();
		setPersistor( persistor );
		setConexao( cnx );
	}

	/**
	 * M�todo Construtor que recebe uma Tabela B�sica de PCMSO.EXAMES_OCUPACIONAIS
	 *
	 */
	public ExameOcupacionalNegocio( ExameOcupacional exameOcupacional ) throws QtSQLException {

		ExameOcupacionalPersistor persistor = new ExameOcupacionalPersistor( exameOcupacional );
		setPersistor( persistor );
	}

	/**
	 * M�todo Construtor que recebe uma conex�o e uma Tabela B�sica de PCMSO.EXAMES_OCUPACIONAIS
	 *
	 */
	public ExameOcupacionalNegocio( Conexao cnx, ExameOcupacional exameOcupacional ) throws QtSQLException {

		ExameOcupacionalPersistor persistor = new ExameOcupacionalPersistor( exameOcupacional );
		setPersistor( persistor );
		setConexao( cnx );
	}

	/**
	 * M�todo que retorna uma nova Tabela B�sica de PCMSO.EXAMES_OCUPACIONAIS
	 *
	 */
	public TabelaBasica geraNovaTabelaBasica() {

		return new ExameOcupacional();

	}

	public boolean antesDeInserir() throws QtSQLException {

		if( testaRegrasBasicas() ) {
			return testaReferencias();
		}
		return false;
	}

	public boolean antesDeAlterar() throws QtSQLException {

		if( testaRegrasBasicas() ) {
			return testaReferencias();
		}
		return false;
	}

	private boolean testaReferencias() throws QtSQLException {

		
		return true;
	}

	public boolean antesDeExcluir() {

		try {
			ExameOcupacional exameOcupacional = (ExameOcupacional) getPersistor().getTabelaBasica();

			Conexao cnx = getConexao();
			Query query = new Query( cnx );

			try {
				query.setSQL( "select count(*) from PCMSO.EXAMES_COMPLEMENTARES " );
				query.addSQL( " where NR_EXAME like :prm1NrExame" );
				query.setParameter( "prm1NrExame", exameOcupacional.getNrExame() );
				query.executeQuery();

				if( query.getInt( 1 ) > 0 ) {
					setMsgErro( "N�o posso excluir registro em PCMSO.EXAMES_OCUPACIONAIS( " + exameOcupacional.getNrExame()+ " ) pois o mesmo � referenciado em PCMSO.EXAMES_COMPLEMENTARES" );
					return false;
				}
			} finally {
				query.liberaRecursos();
			}
		} catch ( Exception e ) {
			this.setMsgErro( e.getMessage() );
			return false;
		}
		return true;
	}

	public boolean testaRegrasBasicas() {

		ExameOcupacional exameOcupacional = (ExameOcupacional) getPersistor().getTabelaBasica();

		if( exameOcupacional.getNrExame() == null  || exameOcupacional.getNrExame().trim().equals( "" )  ) {
			setMsgErro( "Numero do exame necessita estar preenchido.", "NR_EXAME" );
			return false;
		}

		if( exameOcupacional.getCdBeneficiarioCartao() == null  || exameOcupacional.getCdBeneficiarioCartao().trim().equals( "" )  ) {
			setMsgErro( "Codigo do benenficiario necessita estar preenchido.", "CD_BENEFICIARIO_CARTAO" );
			return false;
		}

		if( exameOcupacional.getCdProcedimento() == null  || exameOcupacional.getCdProcedimento().trim().equals( "" )  ) {
			setMsgErro( "Codigo do procedimento necessita estar preenchido.", "CD_PROCEDIMENTO" );
			return false;
		}

		if( exameOcupacional.getTpExame() == null  || exameOcupacional.getTpExame().trim().equals( "" )  ) {
			setMsgErro( "Tipo de Exame necessita estar preenchido.", "TP_EXAME" );
			return false;
		}

		if( !exameOcupacional.getTpExame().matches( "A|P|R|M|D|E" ) ) {
			setMsgErro( "Tipo de Exame inv�lido (" + exameOcupacional.getTpExame() + "): precisa ser A-Admissional, P-Peri�dico, R-Retorno ao Trabalho, M-Mudan�a de Fun��o, D-Demissional ou E-Referencial Peri�dico", "TP_EXAME" );
			return false;
		}

		if( exameOcupacional.getTpConciderado() != null && !exameOcupacional.getTpConciderado().trim().equals( "" ) && !exameOcupacional.getTpConciderado().matches( "A|I" ) ) {
			setMsgErro( "Tipo de concidera��es inv�lido (" + exameOcupacional.getTpConciderado() + "): precisa ser A-apto ou I-Inapto", "TP_CONCIDERADO" );
			return false;
		}

		if( exameOcupacional.getTpFrequenciaUsoMedicamentos() != null && !exameOcupacional.getTpFrequenciaUsoMedicamentos().trim().equals( "" ) && !exameOcupacional.getTpFrequenciaUsoMedicamentos().matches( "D|S|M" ) ) {
			setMsgErro( "Antecedentes Pessoal inv�lido (" + exameOcupacional.getTpFrequenciaUsoMedicamentos() + "): precisa ser D-Diariamente, S-Semanalmente ou M-Mensalmente", "TP_FREQUENCIA_USO_MEDICAMENTOS" );
			return false;
		}

		return true;
	}
}