package jPcmso.negocio.geral.e;

import quatro.negocio.NegocioBasico;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import jPcmso.persistencia.geral.e.ExameComplementar;
import jPcmso.persistencia.geral.e.ExameComplementarPersistor;

/**
 *
 * Classe de Neg�cio para PCMSO.EXAMES_COMPLEMENTARES
 *
 * @author Samuel Antonio Klein
 * @see quatro.negocio.NegocioBasico
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
public class ExameComplementarNegocio extends NegocioBasico {

	/**
	 * M�todo Construtor b�sico.
	 *
	 */
	public ExameComplementarNegocio() throws QtSQLException {

		ExameComplementarPersistor persistor = new ExameComplementarPersistor();
		setPersistor( persistor );
	}

	/**
	 * M�todo Construtor que recebe uma conex�o.
	 *
	 */
	public ExameComplementarNegocio( Conexao cnx ) throws QtSQLException {

		ExameComplementarPersistor persistor = new ExameComplementarPersistor();
		setPersistor( persistor );
		setConexao( cnx );
	}

	/**
	 * M�todo Construtor que recebe uma Tabela B�sica de PCMSO.EXAMES_COMPLEMENTARES
	 *
	 */
	public ExameComplementarNegocio( ExameComplementar exameComplementar ) throws QtSQLException {

		ExameComplementarPersistor persistor = new ExameComplementarPersistor( exameComplementar );
		setPersistor( persistor );
	}

	/**
	 * M�todo Construtor que recebe uma conex�o e uma Tabela B�sica de PCMSO.EXAMES_COMPLEMENTARES
	 *
	 */
	public ExameComplementarNegocio( Conexao cnx, ExameComplementar exameComplementar ) throws QtSQLException {

		ExameComplementarPersistor persistor = new ExameComplementarPersistor( exameComplementar );
		setPersistor( persistor );
		setConexao( cnx );
	}

	/**
	 * M�todo que retorna uma nova Tabela B�sica de PCMSO.EXAMES_COMPLEMENTARES
	 *
	 */
	public TabelaBasica geraNovaTabelaBasica() {

		return new ExameComplementar();

	}

	public boolean antesDeInserir() throws QtSQLException {

		testaPersistor();
		ExameComplementar exameComplementar = (ExameComplementar) getPersistor().getTabelaBasica();
		ExameComplementarPersistor persistor = (ExameComplementarPersistor) getPersistor();

		if ( exameComplementar.getSqExame() == null ) {

			try {
				exameComplementar.setSqExame( ExameComplementarPersistor.geraNovoSqExame(exameComplementar.getNrExame()) );
			} catch (QtSQLException e) {
				setMsgErro( "ExameComplementarNegocio - Antes de Inserir: Erro na Gera��o do Identificador: " + e.getMessage() );
				return false;
			}
		}
		if( testaRegrasBasicas() ) {
			return testaReferencias();
		}
		return false;
	}

	public boolean antesDeAlterar() throws QtSQLException {

		if( testaRegrasBasicas() ) {
			return testaReferencias();
		}
		return false;
	}

	private boolean testaReferencias() throws QtSQLException {

		
		return true;
	}

	public boolean antesDeExcluir() {

		return true;
	}

	public boolean testaRegrasBasicas() {

		ExameComplementar exameComplementar = (ExameComplementar) getPersistor().getTabelaBasica();

		if( exameComplementar.getNrExame() == null  || exameComplementar.getNrExame().trim().equals( "" )  ) {
			setMsgErro( "Numero do exame necessita estar preenchido.", "NR_EXAME" );
			return false;
		}

		if( exameComplementar.getSqExame() == null  ) {
			setMsgErro( "Sequencia do exame necessita estar preenchido.", "SQ_EXAME" );
			return false;
		}

		if( exameComplementar.getCdProcedimento() == null  || exameComplementar.getCdProcedimento().trim().equals( "" )  ) {
			setMsgErro( "Codigo do procedimento necessita estar preenchido.", "CD_PROCEDIMENTO" );
			return false;
		}

		if( exameComplementar.getTpResultado() != null && !exameComplementar.getTpResultado().trim().equals( "" ) && !exameComplementar.getTpResultado().matches( "N|A" ) ) {
			setMsgErro( "Tipo de Resultado inv�lido (" + exameComplementar.getTpResultado() + "): precisa ser N-Normal ou A-alterado", "TP_RESULTADO" );
			return false;
		}

		return true;
	}
}