package jPcmso.negocio.geral.e;

import jPcmso.persistencia.geral.e.ExameOcupacional;
import quatro.negocio.ErroDeNegocio;
import quatro.negocio.NegocioEspecificoImpl;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.sql.Query;
import quatro.util.QtData;
import quatro.util.QtString;

public class ExameOcupacionalNegocioEspecifico extends NegocioEspecificoImpl {

	public void antesDeInserir(Conexao cnx, TabelaBasica tab )	throws QtSQLException, ErroDeNegocio {
		ExameOcupacional exameOcupacional = (ExameOcupacional) tab;
		exameOcupacional.setDtEmissaoNota( new QtData() );
		Query q = new Query( cnx );
		
		try {
			q.setSQL( "SELECT NEXTVAL( 'PCMSO.SQ_EXAME' ); " );
			q.executeQuery();
			exameOcupacional.setNrExame( QtString.strzero( q.getInteger( 1 ), 7 ) );
		} finally {
			q.liberaRecursos();
		}
	}
	
	public void antesDeExcluir(Conexao cnx, TabelaBasica tabela)	throws QtSQLException, ErroDeNegocio {
		ExameOcupacional e = (ExameOcupacional) tabela;
		
		if( e.getDtExecucaoExame() != null || e.getDtRetornoNota() != null ){
			throw new ErroDeNegocio( "n�o posso Excluir pois tem Data de Execu��o ou de Retorno" );
		}
		
		Query qry = new Query( cnx );
		
		qry.setSQL( " delete from pcmso.exames_complementares where nr_exame = " + e.getNrExame() );
		qry.executeUpdate();
		
	}
	public void antesDeAlterar(Conexao arg0, TabelaBasica tab)	throws QtSQLException, ErroDeNegocio {
		ExameOcupacional exameOcupacional = (ExameOcupacional) tab;
		if( exameOcupacional.getDtExecucaoExame() != null && exameOcupacional.getDtRetornoNota() == null ) {
			exameOcupacional.setDtRetornoNota( new QtData() );
		}
	}

}