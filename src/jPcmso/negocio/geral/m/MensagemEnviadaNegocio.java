package jPcmso.negocio.geral.m;

import quatro.negocio.NegocioBasico;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import jPcmso.persistencia.geral.m.MensagemEnviada;
import jPcmso.persistencia.geral.m.MensagemEnviadaPersistor;

/**
 *
 * Classe de Neg�cio para PCMSO.MENSAGENS_ENVIADAS
 *
 * @author Samuel Antonio Klein
 * @see quatro.negocio.NegocioBasico
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
public class MensagemEnviadaNegocio extends NegocioBasico {

	/**
	 * M�todo Construtor b�sico.
	 *
	 */
	public MensagemEnviadaNegocio() throws QtSQLException {

		MensagemEnviadaPersistor persistor = new MensagemEnviadaPersistor();
		setPersistor( persistor );
	}

	/**
	 * M�todo Construtor que recebe uma conex�o.
	 *
	 */
	public MensagemEnviadaNegocio( Conexao cnx ) throws QtSQLException {

		MensagemEnviadaPersistor persistor = new MensagemEnviadaPersistor();
		setPersistor( persistor );
		setConexao( cnx );
	}

	/**
	 * M�todo Construtor que recebe uma Tabela B�sica de PCMSO.MENSAGENS_ENVIADAS
	 *
	 */
	public MensagemEnviadaNegocio( MensagemEnviada mensagemEnviada ) throws QtSQLException {

		MensagemEnviadaPersistor persistor = new MensagemEnviadaPersistor( mensagemEnviada );
		setPersistor( persistor );
	}

	/**
	 * M�todo Construtor que recebe uma conex�o e uma Tabela B�sica de PCMSO.MENSAGENS_ENVIADAS
	 *
	 */
	public MensagemEnviadaNegocio( Conexao cnx, MensagemEnviada mensagemEnviada ) throws QtSQLException {
		System.out.println("iniciando persistor da mesnagem 1");
		MensagemEnviadaPersistor persistor = new MensagemEnviadaPersistor( mensagemEnviada );
		System.out.println("iniciando persistor da mesnagem 2");
		setPersistor( persistor );
		System.out.println("iniciando persistor da mesnagem 3");
		setConexao( cnx );
		System.out.println("iniciando persistor da mesnagem 4");
	}

	/**
	 * M�todo que retorna uma nova Tabela B�sica de PCMSO.MENSAGENS_ENVIADAS
	 *
	 */
	public TabelaBasica geraNovaTabelaBasica() {

		return new MensagemEnviada();

	}

	public boolean antesDeInserir() throws QtSQLException {
		System.out.println("mensagem antes de inserir 1");
		testaPersistor();
		System.out.println("mensagem antes de inserir 2");
		MensagemEnviada mensagemEnviada = (MensagemEnviada) getPersistor().getTabelaBasica();
		System.out.println("mensagem antes de inserir 3");
		if ( mensagemEnviada.getSqMensagem() == null ) {
			System.out.println("mensagem antes de inserir 4");
			try {
				System.out.println("mensagem antes de inserir 5");
				mensagemEnviada.setSqMensagem( MensagemEnviadaPersistor.geraNovoSqMensagem() );
				System.out.println("mensagem antes de inserir 6");
			} catch (QtSQLException e) {
				e.printStackTrace();
				setMsgErro( "MensagemEnviadaNegocio - Antes de Inserir: Erro na Geracao do Identificador: " + e.getMessage() );
				return false;
			}
		}
		System.out.println("mensagem antes de inserir 7");
		if( testaRegrasBasicas() ) {
			System.out.println("mensagem antes de inserir 8");
			return testaReferencias();
		}
		System.out.println("mensagem antes de inserir 9");
		return false;
	}

	public boolean antesDeAlterar() throws QtSQLException {

		if( testaRegrasBasicas() ) {
			return testaReferencias();
		}
		return false;
	}

	private boolean testaReferencias() throws QtSQLException {

		
		return true;
	}

	public boolean antesDeExcluir() {

		return true;
	}

	public boolean testaRegrasBasicas() {

		MensagemEnviada mensagemEnviada = (MensagemEnviada) getPersistor().getTabelaBasica();

		if( mensagemEnviada.getSqMensagem() == null  ) {
			setMsgErro( " necessita estar preenchido.", "SQ_MENSAGEM" );
			return false;
		}

		return true;
	}
}