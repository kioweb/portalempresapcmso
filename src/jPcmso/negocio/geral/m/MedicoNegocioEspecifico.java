package jPcmso.negocio.geral.m;

import jPcmso.persistencia.geral.m.Medico;
import jPcmso.persistencia.geral.m.MedicoLocalizador;
import quatro.negocio.ErroDeNegocio;
import quatro.negocio.NegocioEspecificoImpl;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;

public class MedicoNegocioEspecifico extends NegocioEspecificoImpl {
	
	public void antesDeInserir(Conexao cnx, TabelaBasica registro) throws QtSQLException, ErroDeNegocio {
		Medico t = (Medico) registro;
		
		if( MedicoLocalizador.existe( cnx, t ) ){
			throw new ErroDeNegocio( "J� existe na base de dados um Medico " + t.getCdMedico() );
		}
		
	}
	
}