package jPcmso.negocio.geral.m;

import quatro.negocio.NegocioBasico;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import jPcmso.persistencia.geral.m.MotivoExclusaoBeneficiario;
import jPcmso.persistencia.geral.m.MotivoExclusaoBeneficiarioPersistor;

/**
 *
 * Classe de Neg�cio para MOTIVOS_EXCLUSAO_BENEFICIARIO
 *
 * @author Samuel Antonio Klein
 * @see quatro.negocio.NegocioBasico
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
public class MotivoExclusaoBeneficiarioNegocio extends NegocioBasico {

	/**
	 * M�todo Construtor b�sico.
	 *
	 */
	public MotivoExclusaoBeneficiarioNegocio() throws QtSQLException {

		MotivoExclusaoBeneficiarioPersistor persistor = new MotivoExclusaoBeneficiarioPersistor();
		setPersistor( persistor );
	}

	/**
	 * M�todo Construtor que recebe uma conex�o.
	 *
	 */
	public MotivoExclusaoBeneficiarioNegocio( Conexao cnx ) throws QtSQLException {

		MotivoExclusaoBeneficiarioPersistor persistor = new MotivoExclusaoBeneficiarioPersistor();
		setPersistor( persistor );
		setConexao( cnx );
	}

	/**
	 * M�todo Construtor que recebe uma Tabela B�sica de MOTIVOS_EXCLUSAO_BENEFICIARIO
	 *
	 */
	public MotivoExclusaoBeneficiarioNegocio( MotivoExclusaoBeneficiario motivoExclusaoBeneficiario ) throws QtSQLException {

		MotivoExclusaoBeneficiarioPersistor persistor = new MotivoExclusaoBeneficiarioPersistor( motivoExclusaoBeneficiario );
		setPersistor( persistor );
	}

	/**
	 * M�todo Construtor que recebe uma conex�o e uma Tabela B�sica de MOTIVOS_EXCLUSAO_BENEFICIARIO
	 *
	 */
	public MotivoExclusaoBeneficiarioNegocio( Conexao cnx, MotivoExclusaoBeneficiario motivoExclusaoBeneficiario ) throws QtSQLException {

		MotivoExclusaoBeneficiarioPersistor persistor = new MotivoExclusaoBeneficiarioPersistor( motivoExclusaoBeneficiario );
		setPersistor( persistor );
		setConexao( cnx );
	}

	/**
	 * M�todo que retorna uma nova Tabela B�sica de MOTIVOS_EXCLUSAO_BENEFICIARIO
	 *
	 */
	public TabelaBasica geraNovaTabelaBasica() {

		return new MotivoExclusaoBeneficiario();

	}

	public boolean antesDeInserir() throws QtSQLException {

		if( testaRegrasBasicas() ) {
			return testaReferencias();
		}
		return false;
	}

	public boolean antesDeAlterar() throws QtSQLException {

		if( testaRegrasBasicas() ) {
			return testaReferencias();
		}
		return false;
	}

	private boolean testaReferencias() throws QtSQLException {

		
		return true;
	}

	public boolean antesDeExcluir() {

		return true;
	}

	public boolean testaRegrasBasicas() {

		MotivoExclusaoBeneficiario motivoExclusaoBeneficiario = (MotivoExclusaoBeneficiario) getPersistor().getTabelaBasica();

		if( motivoExclusaoBeneficiario.getIdMotivoExclusao() == null  ) {
			setMsgErro( "Motivo de Exclus�o necessita estar preenchido.", "ID_MOTIVO_EXCLUSAO" );
			return false;
		}

		if( motivoExclusaoBeneficiario.getTpMotivoExclusao() == null  || motivoExclusaoBeneficiario.getTpMotivoExclusao().trim().equals( "" )  ) {
			setMsgErro( "Descri��o do Motivo de Exclus�o necessita estar preenchido.", "TP_MOTIVO_EXCLUSAO" );
			return false;
		}

		if( !motivoExclusaoBeneficiario.getTpMotivoExclusao().matches( "01|02|03|04|05|06|07|08|09|11|12|13|99" ) ) {
			setMsgErro( "Descri��o do Motivo de Exclus�o inv�lido (" + motivoExclusaoBeneficiario.getTpMotivoExclusao() + "): precisa ser 01-Rompimento do contrato por iniciativa do Benefici�rio, 02-T�rmino da rela��o de vinculado a um benefici�rio, 03-Desligamento da Empresa (planos coletivos), 04-Inadimpl�ncia, 05-�bito, 06-Mudan�a de plano, 07-Exclus�o decorrente de mudan�a do c�digo do benefici�rio, motivada por adapta��o de sistema da operadora, 08-Transfer�ncia de carteira, 09-Altera��o do c�digo do benefici�rio., 11-Plano antigo migrado, 12-Plano antigo adaptado, 13-Inclus�o indevida de benefici�rios ou 99-Outros ", "TP_MOTIVO_EXCLUSAO" );
			return false;
		}

		return true;
	}
}