package jPcmso.negocio.geral.m;

import quatro.negocio.NegocioBasico;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.sql.Query;
import jPcmso.persistencia.geral.m.Medico;
import jPcmso.persistencia.geral.m.MedicoPersistor;

/**
 *
 * Classe de Neg�cio para PCMSO.MEDICOS
 *
 * @author Samuel Antonio Klein
 * @see quatro.negocio.NegocioBasico
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
public class MedicoNegocio extends NegocioBasico {

	/**
	 * M�todo Construtor b�sico.
	 *
	 */
	public MedicoNegocio() throws QtSQLException {

		MedicoPersistor persistor = new MedicoPersistor();
		setPersistor( persistor );
	}

	/**
	 * M�todo Construtor que recebe uma conex�o.
	 *
	 */
	public MedicoNegocio( Conexao cnx ) throws QtSQLException {

		MedicoPersistor persistor = new MedicoPersistor();
		setPersistor( persistor );
		setConexao( cnx );
	}

	/**
	 * M�todo Construtor que recebe uma Tabela B�sica de PCMSO.MEDICOS
	 *
	 */
	public MedicoNegocio( Medico medico ) throws QtSQLException {

		MedicoPersistor persistor = new MedicoPersistor( medico );
		setPersistor( persistor );
	}

	/**
	 * M�todo Construtor que recebe uma conex�o e uma Tabela B�sica de PCMSO.MEDICOS
	 *
	 */
	public MedicoNegocio( Conexao cnx, Medico medico ) throws QtSQLException {

		MedicoPersistor persistor = new MedicoPersistor( medico );
		setPersistor( persistor );
		setConexao( cnx );
	}

	/**
	 * M�todo que retorna uma nova Tabela B�sica de PCMSO.MEDICOS
	 *
	 */
	public TabelaBasica geraNovaTabelaBasica() {

		return new Medico();

	}

	public boolean antesDeInserir() throws QtSQLException {

		if( testaRegrasBasicas() ) {
			return testaReferencias();
		}
		return false;
	}

	public boolean antesDeAlterar() throws QtSQLException {

		if( testaRegrasBasicas() ) {
			return testaReferencias();
		}
		return false;
	}

	private boolean testaReferencias() throws QtSQLException {

		
		return true;
	}

	public boolean antesDeExcluir() {

		try {
			Medico medico = (Medico) getPersistor().getTabelaBasica();

			Conexao cnx = getConexao();
			Query query = new Query( cnx );

			try {
				query.setSQL( "select count(*) from PCMSO.EXAMES_OCUPACIONAIS " );
				query.addSQL( " where CD_MEDICO_EXAMINADOR like :prm6CdMedicoExaminador" );
				query.setParameter( "prm6CdMedicoExaminador", medico.getCdMedico() );
				query.executeQuery();

				if( query.getInt( 1 ) > 0 ) {
					setMsgErro( "N�o posso excluir registro em PCMSO.MEDICOS( " + medico.getCdMedico()+ " ) pois o mesmo � referenciado em PCMSO.EXAMES_OCUPACIONAIS" );
					return false;
				}
			
				query.setSQL( "select count(*) from PCMSO.EMPRESAS " );
				query.addSQL( " where CD_MEDICO_COORDENADOR like :prm9CdMedicoCoordenador" );
				query.setParameter( "prm9CdMedicoCoordenador", medico.getCdMedico() );
				query.executeQuery();

				if( query.getInt( 1 ) > 0 ) {
					setMsgErro( "N�o posso excluir registro em PCMSO.MEDICOS( " + medico.getCdMedico()+ " ) pois o mesmo � referenciado em PCMSO.EMPRESAS" );
					return false;
				}
			} finally {
				query.liberaRecursos();
			}
		} catch ( Exception e ) {
			this.setMsgErro( e.getMessage() );
			return false;
		}
		return true;
	}

	public boolean testaRegrasBasicas() {

		Medico medico = (Medico) getPersistor().getTabelaBasica();

		if( medico.getCdMedico() == null  || medico.getCdMedico().trim().equals( "" )  ) {
			setMsgErro( "Codigo do medico necessita estar preenchido.", "CD_MEDICO" );
			return false;
		}

		if( medico.getNrCrm() == null  ) {
			setMsgErro( "Numero do CRM necessita estar preenchido.", "NR_CRM" );
			return false;
		}

		return true;
	}
}