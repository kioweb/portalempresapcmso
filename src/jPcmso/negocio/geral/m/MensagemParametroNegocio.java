package jPcmso.negocio.geral.m;

import quatro.negocio.NegocioBasico;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import jPcmso.persistencia.geral.m.MensagemParametro;
import jPcmso.persistencia.geral.m.MensagemParametroPersistor;

/**
 *
 * Classe de Neg�cio para PCMSO.MENSAGEM_PARAMETRO
 *
 * @author Samuel Antonio Klein
 * @see quatro.negocio.NegocioBasico
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
public class MensagemParametroNegocio extends NegocioBasico {

	/**
	 * M�todo Construtor b�sico.
	 *
	 */
	public MensagemParametroNegocio() throws QtSQLException {

		MensagemParametroPersistor persistor = new MensagemParametroPersistor();
		setPersistor( persistor );
	}

	/**
	 * M�todo Construtor que recebe uma conex�o.
	 *
	 */
	public MensagemParametroNegocio( Conexao cnx ) throws QtSQLException {

		MensagemParametroPersistor persistor = new MensagemParametroPersistor();
		setPersistor( persistor );
		setConexao( cnx );
	}

	/**
	 * M�todo Construtor que recebe uma Tabela B�sica de PCMSO.MENSAGEM_PARAMETRO
	 *
	 */
	public MensagemParametroNegocio( MensagemParametro mensagemParametro ) throws QtSQLException {

		MensagemParametroPersistor persistor = new MensagemParametroPersistor( mensagemParametro );
		setPersistor( persistor );
	}

	/**
	 * M�todo Construtor que recebe uma conex�o e uma Tabela B�sica de PCMSO.MENSAGEM_PARAMETRO
	 *
	 */
	public MensagemParametroNegocio( Conexao cnx, MensagemParametro mensagemParametro ) throws QtSQLException {

		MensagemParametroPersistor persistor = new MensagemParametroPersistor( mensagemParametro );
		setPersistor( persistor );
		setConexao( cnx );
	}

	/**
	 * M�todo que retorna uma nova Tabela B�sica de PCMSO.MENSAGEM_PARAMETRO
	 *
	 */
	public TabelaBasica geraNovaTabelaBasica() {

		return new MensagemParametro();

	}

	public boolean antesDeInserir() throws QtSQLException {

		testaPersistor();
		MensagemParametro mensagemParametro = (MensagemParametro) getPersistor().getTabelaBasica();
		MensagemParametroPersistor persistor = (MensagemParametroPersistor) getPersistor();

		if ( mensagemParametro.getIdParametro() == null ) {

			try {
				mensagemParametro.setIdParametro( MensagemParametroPersistor.geraNovoIdParametro() );
			} catch (QtSQLException e) {
				setMsgErro( "MensagemParametroNegocio - Antes de Inserir: Erro na Gera��o do Identificador: " + e.getMessage() );
				return false;
			}
		}
		if( testaRegrasBasicas() ) {
			return testaReferencias();
		}
		return false;
	}

	public boolean antesDeAlterar() throws QtSQLException {

		if( testaRegrasBasicas() ) {
			return testaReferencias();
		}
		return false;
	}

	private boolean testaReferencias() throws QtSQLException {

		
		return true;
	}

	public boolean antesDeExcluir() {

		return true;
	}

	public boolean testaRegrasBasicas() {

		MensagemParametro mensagemParametro = (MensagemParametro) getPersistor().getTabelaBasica();

		if( mensagemParametro.getIdParametro() == null  ) {
			setMsgErro( " necessita estar preenchido.", "ID_PARAMETRO" );
			return false;
		}

		return true;
	}
}