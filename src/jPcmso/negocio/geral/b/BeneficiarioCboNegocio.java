package jPcmso.negocio.geral.b;

import quatro.negocio.NegocioBasico;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.sql.Query;
import jPcmso.persistencia.geral.b.BeneficiarioCbo;
import jPcmso.persistencia.geral.b.BeneficiarioCboPersistor;

/**
 *
 * Classe de Neg�cio para PCMSO.BENEFICIARIOS_CBO
 *
 * @author Lucio Nascimento Teixeira
 * @see quatro.negocio.NegocioBasico
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 23/06/2020 23:51 - importado por Lucio Nascimento Teixeira
 *
 */
public class BeneficiarioCboNegocio extends NegocioBasico {

	/**
	 * M�todo Construtor b�sico.
	 *
	 */
	public BeneficiarioCboNegocio() throws QtSQLException {

		BeneficiarioCboPersistor persistor = new BeneficiarioCboPersistor();
		setPersistor( persistor );
	}

	/**
	 * M�todo Construtor que recebe uma conex�o.
	 *
	 */
	public BeneficiarioCboNegocio( Conexao cnx ) throws QtSQLException {

		BeneficiarioCboPersistor persistor = new BeneficiarioCboPersistor();
		setPersistor( persistor );
		setConexao( cnx );
	}

	/**
	 * M�todo Construtor que recebe uma Tabela B�sica de PCMSO.BENEFICIARIOS_CBO
	 *
	 */
	public BeneficiarioCboNegocio( BeneficiarioCbo beneficiarioCbo ) throws QtSQLException {

		BeneficiarioCboPersistor persistor = new BeneficiarioCboPersistor( beneficiarioCbo );
		setPersistor( persistor );
	}

	/**
	 * M�todo Construtor que recebe uma conex�o e uma Tabela B�sica de PCMSO.BENEFICIARIOS_CBO
	 *
	 */
	public BeneficiarioCboNegocio( Conexao cnx, BeneficiarioCbo beneficiarioCbo ) throws QtSQLException {

		BeneficiarioCboPersistor persistor = new BeneficiarioCboPersistor( beneficiarioCbo );
		setPersistor( persistor );
		setConexao( cnx );
	}

	/**
	 * M�todo que retorna uma nova Tabela B�sica de PCMSO.BENEFICIARIOS_CBO
	 *
	 */
	public TabelaBasica geraNovaTabelaBasica() {

		return new BeneficiarioCbo();

	}

	public boolean antesDeInserir() throws QtSQLException {

		if( testaRegrasBasicas() ) {
			return testaReferencias();
		}
		return false;
	}

	public boolean antesDeAlterar() throws QtSQLException {

		if( testaRegrasBasicas() ) {
			return testaReferencias();
		}
		return false;
	}

	private boolean testaReferencias() throws QtSQLException {

		
		return true;
	}

	public boolean antesDeExcluir() {

		try {
			BeneficiarioCbo beneficiarioCbo = (BeneficiarioCbo) getPersistor().getTabelaBasica();

			Conexao cnx = getConexao();
			Query query = new Query( cnx );

			try {
				query.setSQL( "select count(*) from PCMSO.EXAMES_OCUPACIONAIS " );
				query.addSQL( " where CD_BENEFICIARIO_CARTAO like :prm7CdBeneficiarioCartao" );
				query.setParameter( "prm7CdBeneficiarioCartao", beneficiarioCbo.getCdBeneficiarioCartao() );
				query.executeQuery();

				if( query.getInt( 1 ) > 0 ) {
					setMsgErro( "N�o posso excluir registro em PCMSO.BENEFICIARIOS_CBO( " + beneficiarioCbo.getCdBeneficiarioCartao()+ " ) pois o mesmo � referenciado em PCMSO.EXAMES_OCUPACIONAIS" );
					return false;
				}
			
				query.setSQL( "select count(*) from PCMSO.RISCOS_BENEFICIARIO " );
				query.addSQL( " where CD_BENEFICIARIO_CARTAO like :prm1CdBeneficiarioCartao" );
				query.setParameter( "prm1CdBeneficiarioCartao", beneficiarioCbo.getCdBeneficiarioCartao() );
				query.executeQuery();

				if( query.getInt( 1 ) > 0 ) {
					setMsgErro( "N�o posso excluir registro em PCMSO.BENEFICIARIOS_CBO( " + beneficiarioCbo.getCdBeneficiarioCartao()+ " ) pois o mesmo � referenciado em PCMSO.RISCOS_BENEFICIARIO" );
					return false;
				}
			
				query.setSQL( "select count(*) from PCMSO.HISTORICO_BENEFICIARIOS_CBO " );
				query.addSQL( " where CD_BENEFICIARIO_CARTAO like :prm1CdBeneficiarioCartao" );
				query.setParameter( "prm1CdBeneficiarioCartao", beneficiarioCbo.getCdBeneficiarioCartao() );
				query.executeQuery();

				if( query.getInt( 1 ) > 0 ) {
					setMsgErro( "N�o posso excluir registro em PCMSO.BENEFICIARIOS_CBO( " + beneficiarioCbo.getCdBeneficiarioCartao()+ " ) pois o mesmo � referenciado em PCMSO.HISTORICO_BENEFICIARIOS_CBO" );
					return false;
				}
			
				query.setSQL( "select count(*) from PCMSO.EXAMES_BENEFICIARIO " );
				query.addSQL( " where CD_BENEFICIARIO_CARTAO like :prm1CdBeneficiarioCartao" );
				query.setParameter( "prm1CdBeneficiarioCartao", beneficiarioCbo.getCdBeneficiarioCartao() );
				query.executeQuery();

				if( query.getInt( 1 ) > 0 ) {
					setMsgErro( "N�o posso excluir registro em PCMSO.BENEFICIARIOS_CBO( " + beneficiarioCbo.getCdBeneficiarioCartao()+ " ) pois o mesmo � referenciado em PCMSO.EXAMES_BENEFICIARIO" );
					return false;
				}
			
				query.setSQL( "select count(*) from PCMSO.PERFIL_PROFISSIOGRAFICO " );
				query.addSQL( " where CD_BENEFICIARIO_CARTAO like :prm1CdBeneficiarioCartao" );
				query.setParameter( "prm1CdBeneficiarioCartao", beneficiarioCbo.getCdBeneficiarioCartao() );
				query.executeQuery();

				if( query.getInt( 1 ) > 0 ) {
					setMsgErro( "N�o posso excluir registro em PCMSO.BENEFICIARIOS_CBO( " + beneficiarioCbo.getCdBeneficiarioCartao()+ " ) pois o mesmo � referenciado em PCMSO.PERFIL_PROFISSIOGRAFICO" );
					return false;
				}
			} finally {
				query.liberaRecursos();
			}
		} catch ( Exception e ) {
			this.setMsgErro( e.getMessage() );
			return false;
		}
		return true;
	}

	public boolean testaRegrasBasicas() {

		BeneficiarioCbo beneficiarioCbo = (BeneficiarioCbo) getPersistor().getTabelaBasica();

		if( beneficiarioCbo.getCdBeneficiarioCartao() == null  || beneficiarioCbo.getCdBeneficiarioCartao().trim().equals( "" )  ) {
			setMsgErro( "Codigo de Beneficiario  necessita estar preenchido.", "CD_BENEFICIARIO_CARTAO" );
			return false;
		}

		if( beneficiarioCbo.getTpSexo() != null && !beneficiarioCbo.getTpSexo().trim().equals( "" ) && !beneficiarioCbo.getTpSexo().matches( "M|F" ) ) {
			setMsgErro( "Tipo de sexo inv�lido (" + beneficiarioCbo.getTpSexo() + "): precisa ser M-Masculino ou F-feminino", "TP_SEXO" );
			return false;
		}

		if( beneficiarioCbo.getTpEstadoCivil() != null && !beneficiarioCbo.getTpEstadoCivil().trim().equals( "" ) && !beneficiarioCbo.getTpEstadoCivil().matches( "S|M|W|D|A|U" ) ) {
			setMsgErro( "Tipo de Estado Civil (char(1)) inv�lido (" + beneficiarioCbo.getTpEstadoCivil() + "): precisa ser S-Solteiro, M-Casado (M-Married), W-Viuvo (W-Widow), D-Divorciado, A-Apartado (Separado) ou U-Uni�o Est�vel", "TP_ESTADO_CIVIL" );
			return false;
		}

		if( beneficiarioCbo.getTpBrPdh() != null && !beneficiarioCbo.getTpBrPdh().trim().equals( "" ) && !beneficiarioCbo.getTpBrPdh().matches( "N|B|P" ) ) {
			setMsgErro( "Tipo de BR PDH inv�lido (" + beneficiarioCbo.getTpBrPdh() + "): precisa ser N-N�o aplicavel (NA), B-Beneficiario Reabilitado (BR) ou P-Portador de Deficiencia Habilitado (PDH)", "TP_BR_PDH" );
			return false;
		}

		return true;
	}
}