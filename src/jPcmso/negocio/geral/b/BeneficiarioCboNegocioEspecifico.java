package jPcmso.negocio.geral.b;

import jPcmso.negocio.geral.h.HistoricoBeneficiarioCboNegocio;
import jPcmso.negocio.geral.p.PreCadastroCboNegocio;
import jPcmso.persistencia.geral.b.BeneficiarioCbo;
import jPcmso.persistencia.geral.b.BeneficiarioCboLocalizador;
import jPcmso.persistencia.geral.h.HistoricoBeneficiarioCbo;
import jPcmso.persistencia.geral.p.PreCadastroCbo;
import jPcmso.persistencia.geral.p.PreCadastroCboLocalizador;
import jUni.biblioteca.util.Util;
import quatro.negocio.ErroDeNegocio;
import quatro.negocio.NegocioEspecificoImpl;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.util.QtData;

public class BeneficiarioCboNegocioEspecifico extends NegocioEspecificoImpl {
	
	public void depoisDeInserir( Conexao cnx, TabelaBasica registro ) throws QtSQLException, ErroDeNegocio {
		gravaHistorico( cnx, registro );
		// gravaExames( cnx, registro ); //TODO Exame do CBO
	}

	public void depoisDeAlterar( Conexao cnx, TabelaBasica registro ) throws QtSQLException, ErroDeNegocio {
		gravaHistorico( cnx, registro );
	}

	@Override
	public void antesDeInserir( Conexao cnx, TabelaBasica registro ) throws QtSQLException, ErroDeNegocio {
		testaRegrasDeNegocio( cnx, registro );
	}

	public void antesDeAlterar( Conexao cnx, TabelaBasica registro ) throws QtSQLException, ErroDeNegocio {
		testaRegrasDeNegocio( cnx, registro );
		gravaFuncaoAnterior( cnx, registro );
	}

	public void testaRegrasDeNegocio( Conexao cnx, TabelaBasica registro ) throws QtSQLException, ErroDeNegocio {
		BeneficiarioCbo beneficiarioCbo = (BeneficiarioCbo) registro;
		if( beneficiarioCbo.getDtExclusao() == null ) {
			if( beneficiarioCbo.getCdCbo() == null ) {
				throw new ErroDeNegocio( "Código CBO Obrigatorio!" );
			}
			if( beneficiarioCbo.getIdFuncao() == null ) {
				throw new ErroDeNegocio( "Código Função Obrigatorio!" );
			}
			if( beneficiarioCbo.getIdCargo() == null ) {
				throw new ErroDeNegocio( "Código Cargo Obrigatorio!" );
			}
			if( beneficiarioCbo.getIdSetor() == null ) {
				throw new ErroDeNegocio( "Código Setor Obrigatorio!" );
			}
			if( beneficiarioCbo.getNrCtps() == null ) {
				throw new ErroDeNegocio( "Código CTPS Obrigatorio!" );
			}
			if( beneficiarioCbo.getNrSerieUf() == null ) {
				throw new ErroDeNegocio( "Código Serie Obrigatoria!" );
			}
		}
	}

	private void gravaFuncaoAnterior( Conexao cnx, TabelaBasica registro ) throws QtSQLException {
		
		BeneficiarioCbo b = (BeneficiarioCbo) registro;
		BeneficiarioCbo bFuncao = BeneficiarioCboLocalizador.buscaBeneficiarioCbo( cnx, b.getCdBeneficiarioCartao() );
		if( bFuncao != null ) {
			if( Util.zeroCast( bFuncao.getIdFuncao() ) != 0 && Util.zeroCast( b.getIdFuncao() ) != 0 ) {
				if( Util.zeroCast( bFuncao.getIdFuncao() ) != Util.zeroCast( b.getIdFuncao() ) ) {
					b.setIdFuncaoAnterior( bFuncao.getIdFuncao() );
				}
			}
		}
	}

	private void gravaHistorico( Conexao cnx, TabelaBasica registro ) throws QtSQLException, ErroDeNegocio {
		if( registro != null ) {
			BeneficiarioCbo b = (BeneficiarioCbo) registro;
			HistoricoBeneficiarioCbo hi = new HistoricoBeneficiarioCbo();
			hi.setCdBeneficiarioCartao( b.getCdBeneficiarioCartao() );
			hi.setCdCbo( b.getCdCbo() );
			hi.setDsAtividade( b.getDsAtividade() );
			hi.setDtAlteracao( new QtData() );
			hi.setIdCargo( b.getIdCargo() );
			hi.setIdFuncao( b.getIdFuncao() );
			hi.setIdSetor( b.getIdSetor() );
			HistoricoBeneficiarioCboNegocio hb = new HistoricoBeneficiarioCboNegocio( hi );
			hb.insere();
			PreCadastroCbo pre = PreCadastroCboLocalizador.buscaporBeneficiario( b.getCdBeneficiarioCartao() );
			if( pre != null ) {
				pre.setDtExclusao( b.getDtExclusao() );
				pre.setTpSolicitacao( "N" );

				PreCadastroCboNegocio pn = new PreCadastroCboNegocio( pre );
				pn.altera();
			}
		}

	}
}