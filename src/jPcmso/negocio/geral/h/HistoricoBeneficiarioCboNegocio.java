package jPcmso.negocio.geral.h;

import quatro.negocio.NegocioBasico;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import jPcmso.persistencia.geral.h.HistoricoBeneficiarioCbo;
import jPcmso.persistencia.geral.h.HistoricoBeneficiarioCboPersistor;

/**
 *
 * Classe de Neg�cio para PCMSO.HISTORICO_BENEFICIARIOS_CBO
 *
 * @author Samuel Antonio Klein
 * @see quatro.negocio.NegocioBasico
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
public class HistoricoBeneficiarioCboNegocio extends NegocioBasico {

	/**
	 * M�todo Construtor b�sico.
	 *
	 */
	public HistoricoBeneficiarioCboNegocio() throws QtSQLException {

		HistoricoBeneficiarioCboPersistor persistor = new HistoricoBeneficiarioCboPersistor();
		setPersistor( persistor );
	}

	/**
	 * M�todo Construtor que recebe uma conex�o.
	 *
	 */
	public HistoricoBeneficiarioCboNegocio( Conexao cnx ) throws QtSQLException {

		HistoricoBeneficiarioCboPersistor persistor = new HistoricoBeneficiarioCboPersistor();
		setPersistor( persistor );
		setConexao( cnx );
	}

	/**
	 * M�todo Construtor que recebe uma Tabela B�sica de PCMSO.HISTORICO_BENEFICIARIOS_CBO
	 *
	 */
	public HistoricoBeneficiarioCboNegocio( HistoricoBeneficiarioCbo historicoBeneficiarioCbo ) throws QtSQLException {

		HistoricoBeneficiarioCboPersistor persistor = new HistoricoBeneficiarioCboPersistor( historicoBeneficiarioCbo );
		setPersistor( persistor );
	}

	/**
	 * M�todo Construtor que recebe uma conex�o e uma Tabela B�sica de PCMSO.HISTORICO_BENEFICIARIOS_CBO
	 *
	 */
	public HistoricoBeneficiarioCboNegocio( Conexao cnx, HistoricoBeneficiarioCbo historicoBeneficiarioCbo ) throws QtSQLException {

		HistoricoBeneficiarioCboPersistor persistor = new HistoricoBeneficiarioCboPersistor( historicoBeneficiarioCbo );
		setPersistor( persistor );
		setConexao( cnx );
	}

	/**
	 * M�todo que retorna uma nova Tabela B�sica de PCMSO.HISTORICO_BENEFICIARIOS_CBO
	 *
	 */
	public TabelaBasica geraNovaTabelaBasica() {

		return new HistoricoBeneficiarioCbo();

	}

	public boolean antesDeInserir() throws QtSQLException {

		testaPersistor();
		HistoricoBeneficiarioCbo historicoBeneficiarioCbo = (HistoricoBeneficiarioCbo) getPersistor().getTabelaBasica();
		HistoricoBeneficiarioCboPersistor persistor = (HistoricoBeneficiarioCboPersistor) getPersistor();

		if ( historicoBeneficiarioCbo.getSqAlteracao() == null ) {

			try {
				historicoBeneficiarioCbo.setSqAlteracao( HistoricoBeneficiarioCboPersistor.geraNovoSqAlteracao(historicoBeneficiarioCbo.getCdBeneficiarioCartao()) );
			} catch (QtSQLException e) {
				setMsgErro( "HistoricoBeneficiarioCboNegocio - Antes de Inserir: Erro na Gera��o do Identificador: " + e.getMessage() );
				return false;
			}
		}
		if( testaRegrasBasicas() ) {
			return testaReferencias();
		}
		return false;
	}

	public boolean antesDeAlterar() throws QtSQLException {

		if( testaRegrasBasicas() ) {
			return testaReferencias();
		}
		return false;
	}

	private boolean testaReferencias() throws QtSQLException {

		
		return true;
	}

	public boolean antesDeExcluir() {

		return true;
	}

	public boolean testaRegrasBasicas() {

		HistoricoBeneficiarioCbo historicoBeneficiarioCbo = (HistoricoBeneficiarioCbo) getPersistor().getTabelaBasica();

		if( historicoBeneficiarioCbo.getCdBeneficiarioCartao() == null  || historicoBeneficiarioCbo.getCdBeneficiarioCartao().trim().equals( "" )  ) {
			setMsgErro( "Codigo de Beneficiario  necessita estar preenchido.", "CD_BENEFICIARIO_CARTAO" );
			return false;
		}

		if( historicoBeneficiarioCbo.getSqAlteracao() == null  ) {
			setMsgErro( "Sequecial de ateracoes necessita estar preenchido.", "SQ_ALTERACAO" );
			return false;
		}

		return true;
	}
}