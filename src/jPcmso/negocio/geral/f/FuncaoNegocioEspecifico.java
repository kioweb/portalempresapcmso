package jPcmso.negocio.geral.f;

import jPcmso.persistencia.geral.f.Funcao;
import quatro.negocio.ErroDeNegocio;
import quatro.negocio.NegocioEspecificoImpl;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;

public class FuncaoNegocioEspecifico extends NegocioEspecificoImpl {
	
	@Override
	public void antesDeAlterar( Conexao cnx, TabelaBasica registro ) throws QtSQLException, ErroDeNegocio {
		validaDescricao( registro );
	}

	@Override
	public void antesDeInserir( Conexao cnx, TabelaBasica registro ) throws QtSQLException, ErroDeNegocio {
		validaDescricao( registro );
	}
	
	private void validaDescricao( TabelaBasica registro ) throws ErroDeNegocio {
		Funcao funcao = (Funcao) registro;
		if( funcao != null && funcao.getDsFuncao() != null && !funcao.getDsFuncao().trim().equals( "" ) ) {
			if( funcao.getDsFuncao().length() > 70 ) {
				throw new ErroDeNegocio( "N�o � possivel gravar mais de 70 caracteres para o descri��o" );
			}
		}
	}
	
}