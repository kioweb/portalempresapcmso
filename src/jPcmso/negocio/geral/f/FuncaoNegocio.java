package jPcmso.negocio.geral.f;

import quatro.negocio.NegocioBasico;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.sql.Query;
import jPcmso.persistencia.geral.f.Funcao;
import jPcmso.persistencia.geral.f.FuncaoPersistor;

/**
 *
 * Classe de Neg�cio para PCMSO.FUNCOES
 *
 * @author Samuel Antonio Klein
 * @see quatro.negocio.NegocioBasico
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
public class FuncaoNegocio extends NegocioBasico {

	/**
	 * M�todo Construtor b�sico.
	 *
	 */
	public FuncaoNegocio() throws QtSQLException {

		FuncaoPersistor persistor = new FuncaoPersistor();
		setPersistor( persistor );
	}

	/**
	 * M�todo Construtor que recebe uma conex�o.
	 *
	 */
	public FuncaoNegocio( Conexao cnx ) throws QtSQLException {

		FuncaoPersistor persistor = new FuncaoPersistor();
		setPersistor( persistor );
		setConexao( cnx );
	}

	/**
	 * M�todo Construtor que recebe uma Tabela B�sica de PCMSO.FUNCOES
	 *
	 */
	public FuncaoNegocio( Funcao funcao ) throws QtSQLException {

		FuncaoPersistor persistor = new FuncaoPersistor( funcao );
		setPersistor( persistor );
	}

	/**
	 * M�todo Construtor que recebe uma conex�o e uma Tabela B�sica de PCMSO.FUNCOES
	 *
	 */
	public FuncaoNegocio( Conexao cnx, Funcao funcao ) throws QtSQLException {

		FuncaoPersistor persistor = new FuncaoPersistor( funcao );
		setPersistor( persistor );
		setConexao( cnx );
	}

	/**
	 * M�todo que retorna uma nova Tabela B�sica de PCMSO.FUNCOES
	 *
	 */
	public TabelaBasica geraNovaTabelaBasica() {

		return new Funcao();

	}

	public boolean antesDeInserir() throws QtSQLException {

		testaPersistor();
		Funcao funcao = (Funcao) getPersistor().getTabelaBasica();
		FuncaoPersistor persistor = (FuncaoPersistor) getPersistor();

		if ( funcao.getIdFuncao() == null ) {

			try {
				funcao.setIdFuncao( FuncaoPersistor.geraNovoIdFuncao() );
			} catch (QtSQLException e) {
				setMsgErro( "FuncaoNegocio - Antes de Inserir: Erro na Gera��o do Identificador: " + e.getMessage() );
				return false;
			}
		}
		if( testaRegrasBasicas() ) {
			return testaReferencias();
		}
		return false;
	}

	public boolean antesDeAlterar() throws QtSQLException {

		if( testaRegrasBasicas() ) {
			return testaReferencias();
		}
		return false;
	}

	private boolean testaReferencias() throws QtSQLException {

		
		return true;
	}

	public boolean antesDeExcluir() {

		try {
			Funcao funcao = (Funcao) getPersistor().getTabelaBasica();

			Conexao cnx = getConexao();
			Query query = new Query( cnx );

			try {
				query.setSQL( "select count(*) from PCMSO.BENEFICIARIOS_CBO " );
				query.addSQL( " where ID_FUNCAO = :prm8IdFuncao" );
				query.setParameter( "prm8IdFuncao", funcao.getIdFuncao() );
				query.executeQuery();

				if( query.getInt( 1 ) > 0 ) {
					setMsgErro( "N�o posso excluir registro em PCMSO.FUNCOES( " + funcao.getIdFuncao()+ " ) pois o mesmo � referenciado em PCMSO.BENEFICIARIOS_CBO" );
					return false;
				}
			
				query.setSQL( "select count(*) from PCMSO.HISTORICO_BENEFICIARIOS_CBO " );
				query.addSQL( " where ID_FUNCAO = :prm5IdFuncao" );
				query.setParameter( "prm5IdFuncao", funcao.getIdFuncao() );
				query.executeQuery();

				if( query.getInt( 1 ) > 0 ) {
					setMsgErro( "N�o posso excluir registro em PCMSO.FUNCOES( " + funcao.getIdFuncao()+ " ) pois o mesmo � referenciado em PCMSO.HISTORICO_BENEFICIARIOS_CBO" );
					return false;
				}
			
				query.setSQL( "select count(*) from PCMSO.EXAMES_OCUPACIONAIS " );
				query.addSQL( " where ID_FUNCAO = :prm3IdFuncao" );
				query.setParameter( "prm3IdFuncao", funcao.getIdFuncao() );
				query.executeQuery();

				if( query.getInt( 1 ) > 0 ) {
					setMsgErro( "N�o posso excluir registro em PCMSO.FUNCOES( " + funcao.getIdFuncao()+ " ) pois o mesmo � referenciado em PCMSO.EXAMES_OCUPACIONAIS" );
					return false;
				}
			
//				query.setSQL( "select count(*) from PCMSO.LOTACAO_ATRIBUICAO " );
//				query.addSQL( " where ID_FUNCAO = :prm10IdFuncao" );
//				query.setParameter( "prm10IdFuncao", funcao.getIdFuncao() );
//				query.executeQuery();
//
//				if( query.getInt( 1 ) > 0 ) {
//					setMsgErro( "N�o posso excluir registro em PCMSO.FUNCOES( " + funcao.getIdFuncao()+ " ) pois o mesmo � referenciado em PCMSO.LOTACAO_ATRIBUICAO" );
//					return false;
//				}
			
				query.setSQL( "select count(*) from PCMSO.BENEFICIARIOS_CBO " );
				query.addSQL( " where ID_FUNCAO_ANTERIOR = :prm28IdFuncaoAnterior" );
				query.setParameter( "prm28IdFuncaoAnterior", funcao.getIdFuncao() );
				query.executeQuery();

				if( query.getInt( 1 ) > 0 ) {
					setMsgErro( "N�o posso excluir registro em PCMSO.FUNCOES( " + funcao.getIdFuncao()+ " ) pois o mesmo � referenciado em PCMSO.BENEFICIARIOS_CBO" );
					return false;
				}
			} finally {
				query.liberaRecursos();
			}
		} catch ( Exception e ) {
			this.setMsgErro( e.getMessage() );
			return false;
		}
		return true;
	}

	public boolean testaRegrasBasicas() {

		Funcao funcao = (Funcao) getPersistor().getTabelaBasica();

		if( funcao.getIdFuncao() == null  ) {
			setMsgErro( "Codigo da funcao necessita estar preenchido.", "ID_FUNCAO" );
			return false;
		}

		return true;
	}
}