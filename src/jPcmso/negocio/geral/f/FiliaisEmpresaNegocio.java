package jPcmso.negocio.geral.f;

import quatro.negocio.NegocioBasico;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.sql.Query;
import jPcmso.persistencia.geral.f.FiliaisEmpresa;
import jPcmso.persistencia.geral.f.FiliaisEmpresaPersistor;

/**
 *
 * Classe de Negócio para PCMSO.FILIAIS_EMPRESAS
 *
 * @author ronei@unimed-uau.com.br
 * @see quatro.negocio.NegocioBasico
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 21/12/2017 07:46 - importado por ronei@unimed-uau.com.br
 *
 */
public class FiliaisEmpresaNegocio extends NegocioBasico {

	/**
	 * Método Construtor básico.
	 *
	 */
	public FiliaisEmpresaNegocio() throws QtSQLException {

		FiliaisEmpresaPersistor persistor = new FiliaisEmpresaPersistor();
		setPersistor( persistor );
	}

	/**
	 * Método Construtor que recebe uma conexão.
	 *
	 */
	public FiliaisEmpresaNegocio( Conexao cnx ) throws QtSQLException {

		FiliaisEmpresaPersistor persistor = new FiliaisEmpresaPersistor();
		setPersistor( persistor );
		setConexao( cnx );
	}

	/**
	 * Método Construtor que recebe uma Tabela Básica de PCMSO.FILIAIS_EMPRESAS
	 *
	 */
	public FiliaisEmpresaNegocio( FiliaisEmpresa filiaisEmpresa ) throws QtSQLException {

		FiliaisEmpresaPersistor persistor = new FiliaisEmpresaPersistor( filiaisEmpresa );
		setPersistor( persistor );
	}

	/**
	 * Método Construtor que recebe uma conexão e uma Tabela Básica de PCMSO.FILIAIS_EMPRESAS
	 *
	 */
	public FiliaisEmpresaNegocio( Conexao cnx, FiliaisEmpresa filiaisEmpresa ) throws QtSQLException {

		FiliaisEmpresaPersistor persistor = new FiliaisEmpresaPersistor( filiaisEmpresa );
		setPersistor( persistor );
		setConexao( cnx );
	}

	/**
	 * Método que retorna uma nova Tabela Básica de PCMSO.FILIAIS_EMPRESAS
	 *
	 */
	public TabelaBasica geraNovaTabelaBasica() {

		return new FiliaisEmpresa();

	}

	public boolean antesDeInserir() throws QtSQLException {

		if( testaRegrasBasicas() ) {
			return testaReferencias();
		}
		return false;
	}

	public boolean antesDeAlterar() throws QtSQLException {

		if( testaRegrasBasicas() ) {
			return testaReferencias();
		}
		return false;
	}

	private boolean testaReferencias() throws QtSQLException {

		
		return true;
	}

	public boolean antesDeExcluir() {

		try {
			FiliaisEmpresa filiaisEmpresa = (FiliaisEmpresa) getPersistor().getTabelaBasica();

			Conexao cnx = getConexao();
			Query query = new Query( cnx );

			try {
				query.setSQL( "select count(*) from PCMSO.BENEFICIARIOS_CBO " );
				query.addSQL( " where CD_EMPRESA like :prm6CdEmpresa and ID_FILIAL = :prm7IdFilial" );
				query.setParameter( "prm6CdEmpresa", filiaisEmpresa.getCdEmpresa() );
				query.setParameter( "prm7IdFilial", filiaisEmpresa.getIdFilial() );
				query.executeQuery();

				if( query.getInt( 1 ) > 0 ) {
					setMsgErro( "Não posso excluir registro em PCMSO.FILIAIS_EMPRESAS( " + filiaisEmpresa.getCdEmpresa()+ ", " + filiaisEmpresa.getIdFilial()+ " ) pois o mesmo é referenciado em PCMSO.BENEFICIARIOS_CBO" );
					return false;
				}
			} finally {
				query.liberaRecursos();
			}
		} catch ( Exception e ) {
			this.setMsgErro( e.getMessage() );
			return false;
		}
		return true;
	}

	public boolean testaRegrasBasicas() {

		FiliaisEmpresa filiaisEmpresa = (FiliaisEmpresa) getPersistor().getTabelaBasica();

		if( filiaisEmpresa.getCdEmpresa() == null  || filiaisEmpresa.getCdEmpresa().trim().equals( "" )  ) {
			setMsgErro( "Codigo da Empresa necessita estar preenchido.", "CD_EMPRESA" );
			return false;
		}

		if( filiaisEmpresa.getIdFilial() == null  ) {
			setMsgErro( "Identificador da Filial necessita estar preenchido.", "ID_FILIAL" );
			return false;
		}

		if( filiaisEmpresa.getDsFilial() == null  || filiaisEmpresa.getDsFilial().trim().equals( "" )  ) {
			setMsgErro( "Nome da Filial necessita estar preenchido.", "DS_FILIAL" );
			return false;
		}

		if( filiaisEmpresa.getNrCnpj() == null  || filiaisEmpresa.getNrCnpj().trim().equals( "" )  ) {
			setMsgErro( "Número do CNPJ da filial necessita estar preenchido.", "NR_CNPJ" );
			return false;
		}

		return true;
	}
}