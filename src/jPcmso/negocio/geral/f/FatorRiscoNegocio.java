package jPcmso.negocio.geral.f;

import quatro.negocio.NegocioBasico;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import jPcmso.persistencia.geral.f.FatorRisco;
import jPcmso.persistencia.geral.f.FatorRiscoPersistor;

/**
 *
 * Classe de Neg�cio para PCMSO.FATORES_RISCOS
 *
 * @author Samuel Antonio Klein
 * @see quatro.negocio.NegocioBasico
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:31 - importado por Samuel Antonio Klein
 *
 */
public class FatorRiscoNegocio extends NegocioBasico {

	/**
	 * M�todo Construtor b�sico.
	 *
	 */
	public FatorRiscoNegocio() throws QtSQLException {

		FatorRiscoPersistor persistor = new FatorRiscoPersistor();
		setPersistor( persistor );
	}

	/**
	 * M�todo Construtor que recebe uma conex�o.
	 *
	 */
	public FatorRiscoNegocio( Conexao cnx ) throws QtSQLException {

		FatorRiscoPersistor persistor = new FatorRiscoPersistor();
		setPersistor( persistor );
		setConexao( cnx );
	}

	/**
	 * M�todo Construtor que recebe uma Tabela B�sica de PCMSO.FATORES_RISCOS
	 *
	 */
	public FatorRiscoNegocio( FatorRisco fatorRisco ) throws QtSQLException {

		FatorRiscoPersistor persistor = new FatorRiscoPersistor( fatorRisco );
		setPersistor( persistor );
	}

	/**
	 * M�todo Construtor que recebe uma conex�o e uma Tabela B�sica de PCMSO.FATORES_RISCOS
	 *
	 */
	public FatorRiscoNegocio( Conexao cnx, FatorRisco fatorRisco ) throws QtSQLException {

		FatorRiscoPersistor persistor = new FatorRiscoPersistor( fatorRisco );
		setPersistor( persistor );
		setConexao( cnx );
	}

	/**
	 * M�todo que retorna uma nova Tabela B�sica de PCMSO.FATORES_RISCOS
	 *
	 */
	public TabelaBasica geraNovaTabelaBasica() {

		return new FatorRisco();

	}

	public boolean antesDeInserir() throws QtSQLException {

		if( testaRegrasBasicas() ) {
			return testaReferencias();
		}
		return false;
	}

	public boolean antesDeAlterar() throws QtSQLException {

		if( testaRegrasBasicas() ) {
			return testaReferencias();
		}
		return false;
	}

	private boolean testaReferencias() throws QtSQLException {

		
		return true;
	}

	public boolean antesDeExcluir() {

		return true;
	}

	public boolean testaRegrasBasicas() {

		FatorRisco fatorRisco = (FatorRisco) getPersistor().getTabelaBasica();

		if( fatorRisco.getCdBeneficiarioCartao() == null  || fatorRisco.getCdBeneficiarioCartao().trim().equals( "" )  ) {
			setMsgErro( "Codigo de Beneficiario  necessita estar preenchido.", "CD_BENEFICIARIO_CARTAO" );
			return false;
		}

		if( fatorRisco.getSqPerfil() == null  ) {
			setMsgErro( "sequencia perfil necessita estar preenchido.", "SQ_PERFIL" );
			return false;
		}

		if( fatorRisco.getSqFator() == null  ) {
			setMsgErro( "sequencia fator necessita estar preenchido.", "SQ_FATOR" );
			return false;
		}

		if( fatorRisco.getIdRisco() == null  ) {
			setMsgErro( "identificador de Risco necessita estar preenchido.", "ID_RISCO" );
			return false;
		}

		return true;
	}
}