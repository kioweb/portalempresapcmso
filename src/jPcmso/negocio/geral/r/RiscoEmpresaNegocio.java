package jPcmso.negocio.geral.r;

import quatro.negocio.NegocioBasico;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import jPcmso.persistencia.geral.r.RiscoEmpresa;
import jPcmso.persistencia.geral.r.RiscoEmpresaPersistor;

/**
 *
 * Classe de Neg�cio para PCMSO.RISCOS_EMPRESA
 *
 * @author Samuel Antonio Klein
 * @see quatro.negocio.NegocioBasico
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
public class RiscoEmpresaNegocio extends NegocioBasico {

	/**
	 * M�todo Construtor b�sico.
	 *
	 */
	public RiscoEmpresaNegocio() throws QtSQLException {

		RiscoEmpresaPersistor persistor = new RiscoEmpresaPersistor();
		setPersistor( persistor );
	}

	/**
	 * M�todo Construtor que recebe uma conex�o.
	 *
	 */
	public RiscoEmpresaNegocio( Conexao cnx ) throws QtSQLException {

		RiscoEmpresaPersistor persistor = new RiscoEmpresaPersistor();
		setPersistor( persistor );
		setConexao( cnx );
	}

	/**
	 * M�todo Construtor que recebe uma Tabela B�sica de PCMSO.RISCOS_EMPRESA
	 *
	 */
	public RiscoEmpresaNegocio( RiscoEmpresa riscoEmpresa ) throws QtSQLException {

		RiscoEmpresaPersistor persistor = new RiscoEmpresaPersistor( riscoEmpresa );
		setPersistor( persistor );
	}

	/**
	 * M�todo Construtor que recebe uma conex�o e uma Tabela B�sica de PCMSO.RISCOS_EMPRESA
	 *
	 */
	public RiscoEmpresaNegocio( Conexao cnx, RiscoEmpresa riscoEmpresa ) throws QtSQLException {

		RiscoEmpresaPersistor persistor = new RiscoEmpresaPersistor( riscoEmpresa );
		setPersistor( persistor );
		setConexao( cnx );
	}

	/**
	 * M�todo que retorna uma nova Tabela B�sica de PCMSO.RISCOS_EMPRESA
	 *
	 */
	public TabelaBasica geraNovaTabelaBasica() {

		return new RiscoEmpresa();

	}

	public boolean antesDeInserir() throws QtSQLException {

		if( testaRegrasBasicas() ) {
			return testaReferencias();
		}
		return false;
	}

	public boolean antesDeAlterar() throws QtSQLException {

		if( testaRegrasBasicas() ) {
			return testaReferencias();
		}
		return false;
	}

	private boolean testaReferencias() throws QtSQLException {

		
		return true;
	}

	public boolean antesDeExcluir() {

		return true;
	}

	public boolean testaRegrasBasicas() {

		RiscoEmpresa riscoEmpresa = (RiscoEmpresa) getPersistor().getTabelaBasica();

		if( riscoEmpresa.getIdRisco() == null  ) {
			setMsgErro( "Identificador do Risco necessita estar preenchido.", "ID_RISCO" );
			return false;
		}

		if( riscoEmpresa.getCdEmpresa() == null  || riscoEmpresa.getCdEmpresa().trim().equals( "" )  ) {
			setMsgErro( "Codigo da empresa necessita estar preenchido.", "CD_EMPRESA" );
			return false;
		}

		return true;
	}
}