package jPcmso.negocio.geral.r;

import quatro.negocio.NegocioBasico;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import jPcmso.persistencia.geral.r.RiscoBeneficiario;
import jPcmso.persistencia.geral.r.RiscoBeneficiarioPersistor;

/**
 *
 * Classe de Neg�cio para PCMSO.RISCOS_BENEFICIARIO
 *
 * @author Samuel Antonio Klein
 * @see quatro.negocio.NegocioBasico
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
public class RiscoBeneficiarioNegocio extends NegocioBasico {

	/**
	 * M�todo Construtor b�sico.
	 *
	 */
	public RiscoBeneficiarioNegocio() throws QtSQLException {

		RiscoBeneficiarioPersistor persistor = new RiscoBeneficiarioPersistor();
		setPersistor( persistor );
	}

	/**
	 * M�todo Construtor que recebe uma conex�o.
	 *
	 */
	public RiscoBeneficiarioNegocio( Conexao cnx ) throws QtSQLException {

		RiscoBeneficiarioPersistor persistor = new RiscoBeneficiarioPersistor();
		setPersistor( persistor );
		setConexao( cnx );
	}

	/**
	 * M�todo Construtor que recebe uma Tabela B�sica de PCMSO.RISCOS_BENEFICIARIO
	 *
	 */
	public RiscoBeneficiarioNegocio( RiscoBeneficiario riscoBeneficiario ) throws QtSQLException {

		RiscoBeneficiarioPersistor persistor = new RiscoBeneficiarioPersistor( riscoBeneficiario );
		setPersistor( persistor );
	}

	/**
	 * M�todo Construtor que recebe uma conex�o e uma Tabela B�sica de PCMSO.RISCOS_BENEFICIARIO
	 *
	 */
	public RiscoBeneficiarioNegocio( Conexao cnx, RiscoBeneficiario riscoBeneficiario ) throws QtSQLException {

		RiscoBeneficiarioPersistor persistor = new RiscoBeneficiarioPersistor( riscoBeneficiario );
		setPersistor( persistor );
		setConexao( cnx );
	}

	/**
	 * M�todo que retorna uma nova Tabela B�sica de PCMSO.RISCOS_BENEFICIARIO
	 *
	 */
	public TabelaBasica geraNovaTabelaBasica() {

		return new RiscoBeneficiario();

	}

	public boolean antesDeInserir() throws QtSQLException {

		if( testaRegrasBasicas() ) {
			return testaReferencias();
		}
		return false;
	}

	public boolean antesDeAlterar() throws QtSQLException {

		if( testaRegrasBasicas() ) {
			return testaReferencias();
		}
		return false;
	}

	private boolean testaReferencias() throws QtSQLException {

		
		return true;
	}

	public boolean antesDeExcluir() {

		return true;
	}

	public boolean testaRegrasBasicas() {

		RiscoBeneficiario riscoBeneficiario = (RiscoBeneficiario) getPersistor().getTabelaBasica();

		if( riscoBeneficiario.getCdBeneficiarioCartao() == null  || riscoBeneficiario.getCdBeneficiarioCartao().trim().equals( "" )  ) {
			setMsgErro( "Codigo do beneficiario necessita estar preenchido.", "CD_BENEFICIARIO_CARTAO" );
			return false;
		}

		if( riscoBeneficiario.getIdRisco() == null  ) {
			setMsgErro( "Identificador do Risco necessita estar preenchido.", "ID_RISCO" );
			return false;
		}

		return true;
	}
}