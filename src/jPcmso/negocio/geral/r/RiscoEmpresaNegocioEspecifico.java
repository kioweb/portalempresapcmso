package jPcmso.negocio.geral.r;

import jPcmso.persistencia.geral.r.RiscoEmpresa;
import jPcmso.persistencia.geral.r.RiscoEmpresaLocalizador;
import quatro.negocio.ErroDeNegocio;
import quatro.negocio.NegocioEspecificoImpl;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;

public class RiscoEmpresaNegocioEspecifico extends NegocioEspecificoImpl {
	
	public void antesDeInserir(Conexao cnx, TabelaBasica registro) throws QtSQLException, ErroDeNegocio {
		RiscoEmpresa t = (RiscoEmpresa) registro;
		
		if( RiscoEmpresaLocalizador.existe( cnx, t ) ){
			throw new ErroDeNegocio( "J� existe na base de dados um Risco:" + t.getIdRisco() + " Para empresa:" + t.getCdEmpresa() );
		}
		
	}
	
}