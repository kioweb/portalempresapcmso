package jPcmso.negocio.geral.r;

import quatro.negocio.NegocioBasico;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import jPcmso.persistencia.geral.r.ResponsavelRegistroAmbiental;
import jPcmso.persistencia.geral.r.ResponsavelRegistroAmbientalPersistor;

/**
 *
 * Classe de Neg�cio para PCMSO.RESPONSAVEL_REGISTRO_AMBIENTAL
 *
 * @author Samuel Antonio Klein
 * @see quatro.negocio.NegocioBasico
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:31 - importado por Samuel Antonio Klein
 *
 */
public class ResponsavelRegistroAmbientalNegocio extends NegocioBasico {

	/**
	 * M�todo Construtor b�sico.
	 *
	 */
	public ResponsavelRegistroAmbientalNegocio() throws QtSQLException {

		ResponsavelRegistroAmbientalPersistor persistor = new ResponsavelRegistroAmbientalPersistor();
		setPersistor( persistor );
	}

	/**
	 * M�todo Construtor que recebe uma conex�o.
	 *
	 */
	public ResponsavelRegistroAmbientalNegocio( Conexao cnx ) throws QtSQLException {

		ResponsavelRegistroAmbientalPersistor persistor = new ResponsavelRegistroAmbientalPersistor();
		setPersistor( persistor );
		setConexao( cnx );
	}

	/**
	 * M�todo Construtor que recebe uma Tabela B�sica de PCMSO.RESPONSAVEL_REGISTRO_AMBIENTAL
	 *
	 */
	public ResponsavelRegistroAmbientalNegocio( ResponsavelRegistroAmbiental responsavelRegistroAmbiental ) throws QtSQLException {

		ResponsavelRegistroAmbientalPersistor persistor = new ResponsavelRegistroAmbientalPersistor( responsavelRegistroAmbiental );
		setPersistor( persistor );
	}

	/**
	 * M�todo Construtor que recebe uma conex�o e uma Tabela B�sica de PCMSO.RESPONSAVEL_REGISTRO_AMBIENTAL
	 *
	 */
	public ResponsavelRegistroAmbientalNegocio( Conexao cnx, ResponsavelRegistroAmbiental responsavelRegistroAmbiental ) throws QtSQLException {

		ResponsavelRegistroAmbientalPersistor persistor = new ResponsavelRegistroAmbientalPersistor( responsavelRegistroAmbiental );
		setPersistor( persistor );
		setConexao( cnx );
	}

	/**
	 * M�todo que retorna uma nova Tabela B�sica de PCMSO.RESPONSAVEL_REGISTRO_AMBIENTAL
	 *
	 */
	public TabelaBasica geraNovaTabelaBasica() {

		return new ResponsavelRegistroAmbiental();

	}

	public boolean antesDeInserir() throws QtSQLException {

		if( testaRegrasBasicas() ) {
			return testaReferencias();
		}
		return false;
	}

	public boolean antesDeAlterar() throws QtSQLException {

		if( testaRegrasBasicas() ) {
			return testaReferencias();
		}
		return false;
	}

	private boolean testaReferencias() throws QtSQLException {

		
		return true;
	}

	public boolean antesDeExcluir() {

		return true;
	}

	public boolean testaRegrasBasicas() {

		ResponsavelRegistroAmbiental responsavelRegistroAmbiental = (ResponsavelRegistroAmbiental) getPersistor().getTabelaBasica();

		if( responsavelRegistroAmbiental.getCdBeneficiarioCartao() == null  || responsavelRegistroAmbiental.getCdBeneficiarioCartao().trim().equals( "" )  ) {
			setMsgErro( "Codigo de Beneficiario  necessita estar preenchido.", "CD_BENEFICIARIO_CARTAO" );
			return false;
		}

		if( responsavelRegistroAmbiental.getSqPerfil() == null  ) {
			setMsgErro( "sequencia perfil necessita estar preenchido.", "SQ_PERFIL" );
			return false;
		}

		if( responsavelRegistroAmbiental.getSqResponsavel() == null  ) {
			setMsgErro( "sequencia Responsavel necessita estar preenchido.", "SQ_RESPONSAVEL" );
			return false;
		}

		return true;
	}
}