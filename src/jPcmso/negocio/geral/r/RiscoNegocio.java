package jPcmso.negocio.geral.r;

import quatro.negocio.NegocioBasico;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.sql.Query;
import jPcmso.persistencia.geral.r.Risco;
import jPcmso.persistencia.geral.r.RiscoPersistor;

/**
 *
 * Classe de Neg�cio para PCMSO.RISCOS
 *
 * @author Samuel Antonio Klein
 * @see quatro.negocio.NegocioBasico
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
public class RiscoNegocio extends NegocioBasico {

	/**
	 * M�todo Construtor b�sico.
	 *
	 */
	public RiscoNegocio() throws QtSQLException {

		RiscoPersistor persistor = new RiscoPersistor();
		setPersistor( persistor );
	}

	/**
	 * M�todo Construtor que recebe uma conex�o.
	 *
	 */
	public RiscoNegocio( Conexao cnx ) throws QtSQLException {

		RiscoPersistor persistor = new RiscoPersistor();
		setPersistor( persistor );
		setConexao( cnx );
	}

	/**
	 * M�todo Construtor que recebe uma Tabela B�sica de PCMSO.RISCOS
	 *
	 */
	public RiscoNegocio( Risco risco ) throws QtSQLException {

		RiscoPersistor persistor = new RiscoPersistor( risco );
		setPersistor( persistor );
	}

	/**
	 * M�todo Construtor que recebe uma conex�o e uma Tabela B�sica de PCMSO.RISCOS
	 *
	 */
	public RiscoNegocio( Conexao cnx, Risco risco ) throws QtSQLException {

		RiscoPersistor persistor = new RiscoPersistor( risco );
		setPersistor( persistor );
		setConexao( cnx );
	}

	/**
	 * M�todo que retorna uma nova Tabela B�sica de PCMSO.RISCOS
	 *
	 */
	public TabelaBasica geraNovaTabelaBasica() {

		return new Risco();

	}

	public boolean antesDeInserir() throws QtSQLException {

		testaPersistor();
		Risco risco = (Risco) getPersistor().getTabelaBasica();
		RiscoPersistor persistor = (RiscoPersistor) getPersistor();

		if ( risco.getIdRisco() == null ) {

			try {
				risco.setIdRisco( RiscoPersistor.geraNovoIdRisco() );
			} catch (QtSQLException e) {
				setMsgErro( "RiscoNegocio - Antes de Inserir: Erro na Gera��o do Identificador: " + e.getMessage() );
				return false;
			}
		}
		if( testaRegrasBasicas() ) {
			return testaReferencias();
		}
		return false;
	}

	public boolean antesDeAlterar() throws QtSQLException {

		if( testaRegrasBasicas() ) {
			return testaReferencias();
		}
		return false;
	}

	private boolean testaReferencias() throws QtSQLException {

		
		return true;
	}

	public boolean antesDeExcluir() {

		try {
			Risco risco = (Risco) getPersistor().getTabelaBasica();

			Conexao cnx = getConexao();
			Query query = new Query( cnx );

			try {
				query.setSQL( "select count(*) from PCMSO.RISCOS_BENEFICIARIO " );
				query.addSQL( " where ID_RISCO = :prm2IdRisco" );
				query.setParameter( "prm2IdRisco", risco.getIdRisco() );
				query.executeQuery();

				if( query.getInt( 1 ) > 0 ) {
					setMsgErro( "N�o posso excluir registro em PCMSO.RISCOS( " + risco.getIdRisco()+ " ) pois o mesmo � referenciado em PCMSO.RISCOS_BENEFICIARIO" );
					return false;
				}
			
				query.setSQL( "select count(*) from PCMSO.RISCOS_EMPRESA " );
				query.addSQL( " where ID_RISCO = :prm1IdRisco" );
				query.setParameter( "prm1IdRisco", risco.getIdRisco() );
				query.executeQuery();

				if( query.getInt( 1 ) > 0 ) {
					setMsgErro( "N�o posso excluir registro em PCMSO.RISCOS( " + risco.getIdRisco()+ " ) pois o mesmo � referenciado em PCMSO.RISCOS_EMPRESA" );
					return false;
				}
			
//				query.setSQL( "select count(*) from PCMSO.FATORES_RISCOS " );
//				query.addSQL( " where ID_RISCO = :prm6IdRisco" );
//				query.setParameter( "prm6IdRisco", risco.getIdRisco() );
//				query.executeQuery();
//
//				if( query.getInt( 1 ) > 0 ) {
//					setMsgErro( "N�o posso excluir registro em PCMSO.RISCOS( " + risco.getIdRisco()+ " ) pois o mesmo � referenciado em PCMSO.FATORES_RISCOS" );
//					return false;
//				}
			} finally {
				query.liberaRecursos();
			}
		} catch ( Exception e ) {
			this.setMsgErro( e.getMessage() );
			return false;
		}
		return true;
	}

	public boolean testaRegrasBasicas() {

		Risco risco = (Risco) getPersistor().getTabelaBasica();

		if( risco.getIdRisco() == null  ) {
			setMsgErro( "Identificador do Risco necessita estar preenchido.", "ID_RISCO" );
			return false;
		}

		return true;
	}
}