package jPcmso.negocio.geral.r;

import jPcmso.persistencia.geral.r.RiscoBeneficiario;
import jPcmso.persistencia.geral.r.RiscoBeneficiarioLocalizador;
import quatro.negocio.ErroDeNegocio;
import quatro.negocio.NegocioEspecificoImpl;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;

public class RiscoBeneficiarioNegocioEspecifico extends NegocioEspecificoImpl {
	
	public void antesDeInserir(Conexao cnx, TabelaBasica registro) throws QtSQLException, ErroDeNegocio {
		RiscoBeneficiario t = (RiscoBeneficiario) registro;
		
		if( RiscoBeneficiarioLocalizador.existe( cnx, t ) ){
			throw new ErroDeNegocio( "J� existe na base de dados um Risco:" + t.getIdRisco() + " Para beneficiario:" + t.getCdBeneficiarioCartao() );
		}
		
	}
	
}