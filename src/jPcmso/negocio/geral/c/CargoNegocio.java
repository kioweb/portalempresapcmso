package jPcmso.negocio.geral.c;

import quatro.negocio.NegocioBasico;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.sql.Query;
import jPcmso.persistencia.geral.c.Cargo;
import jPcmso.persistencia.geral.c.CargoPersistor;

/**
 *
 * Classe de Neg�cio para PCMSO.CARGOS
 *
 * @author Samuel Antonio Klein
 * @see quatro.negocio.NegocioBasico
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
public class CargoNegocio extends NegocioBasico {

	/**
	 * M�todo Construtor b�sico.
	 *
	 */
	public CargoNegocio() throws QtSQLException {

		CargoPersistor persistor = new CargoPersistor();
		setPersistor( persistor );
	}

	/**
	 * M�todo Construtor que recebe uma conex�o.
	 *
	 */
	public CargoNegocio( Conexao cnx ) throws QtSQLException {

		CargoPersistor persistor = new CargoPersistor();
		setPersistor( persistor );
		setConexao( cnx );
	}

	/**
	 * M�todo Construtor que recebe uma Tabela B�sica de PCMSO.CARGOS
	 *
	 */
	public CargoNegocio( Cargo cargo ) throws QtSQLException {

		CargoPersistor persistor = new CargoPersistor( cargo );
		setPersistor( persistor );
	}

	/**
	 * M�todo Construtor que recebe uma conex�o e uma Tabela B�sica de PCMSO.CARGOS
	 *
	 */
	public CargoNegocio( Conexao cnx, Cargo cargo ) throws QtSQLException {

		CargoPersistor persistor = new CargoPersistor( cargo );
		setPersistor( persistor );
		setConexao( cnx );
	}

	/**
	 * M�todo que retorna uma nova Tabela B�sica de PCMSO.CARGOS
	 *
	 */
	public TabelaBasica geraNovaTabelaBasica() {

		return new Cargo();

	}

	public boolean antesDeInserir() throws QtSQLException {

		testaPersistor();
		Cargo cargo = (Cargo) getPersistor().getTabelaBasica();
		CargoPersistor persistor = (CargoPersistor) getPersistor();

		if ( cargo.getIdCargo() == null ) {

			try {
				cargo.setIdCargo( CargoPersistor.geraNovoIdCargo() );
			} catch (QtSQLException e) {
				setMsgErro( "CargoNegocio - Antes de Inserir: Erro na Geração do Identificador: " + e.getMessage() );
				return false;
			}
		}
		if( testaRegrasBasicas() ) {
			return testaReferencias();
		}
		return false;
	}

	public boolean antesDeAlterar() throws QtSQLException {

		if( testaRegrasBasicas() ) {
			return testaReferencias();
		}
		return false;
	}

	private boolean testaReferencias() throws QtSQLException {

		
		return true;
	}

	public boolean antesDeExcluir() {

		try {
			Cargo cargo = (Cargo) getPersistor().getTabelaBasica();

			Conexao cnx = getConexao();
			Query query = new Query( cnx );

			try {
				query.setSQL( "select count(*) from PCMSO.BENEFICIARIOS_CBO " );
				query.addSQL( " where ID_CARGO = :prm10IdCargo" );
				query.setParameter( "prm10IdCargo", cargo.getIdCargo() );
				query.executeQuery();

				if( query.getInt( 1 ) > 0 ) {
					setMsgErro( "Não posso excluir registro em PCMSO.CARGOS( " + cargo.getIdCargo()+ " ) pois o mesmo é referenciado em PCMSO.BENEFICIARIOS_CBO" );
					return false;
				}
			
				query.setSQL( "select count(*) from PCMSO.HISTORICO_BENEFICIARIOS_CBO " );
				query.addSQL( " where ID_CARGO = :prm6IdCargo" );
				query.setParameter( "prm6IdCargo", cargo.getIdCargo() );
				query.executeQuery();

				if( query.getInt( 1 ) > 0 ) {
					setMsgErro( "Não posso excluir registro em PCMSO.CARGOS( " + cargo.getIdCargo()+ " ) pois o mesmo é referenciado em PCMSO.HISTORICO_BENEFICIARIOS_CBO" );
					return false;
				}
			
				query.setSQL( "select count(*) from PCMSO.EXAMES_OCUPACIONAIS " );
				query.addSQL( " where ID_CARGO = :prm4IdCargo" );
				query.setParameter( "prm4IdCargo", cargo.getIdCargo() );
				query.executeQuery();

				if( query.getInt( 1 ) > 0 ) {
					setMsgErro( "Não posso excluir registro em PCMSO.CARGOS( " + cargo.getIdCargo()+ " ) pois o mesmo é referenciado em PCMSO.EXAMES_OCUPACIONAIS" );
					return false;
				}
			
//				query.setSQL( "select count(*) from PCMSO.LOTACAO_ATRIBUICAO " );
//				query.addSQL( " where ID_CARGO = :prm9IdCargo" );
//				query.setParameter( "prm9IdCargo", cargo.getIdCargo() );
//				query.executeQuery();
//
//				if( query.getInt( 1 ) > 0 ) {
//					setMsgErro( "Não posso excluir registro em PCMSO.CARGOS( " + cargo.getIdCargo()+ " ) pois o mesmo � referenciado em PCMSO.LOTACAO_ATRIBUICAO" );
//					return false;
//				}
			} finally {
				query.liberaRecursos();
			}
		} catch ( Exception e ) {
			this.setMsgErro( e.getMessage() );
			return false;
		}
		return true;
	}

	public boolean testaRegrasBasicas() {

		Cargo cargo = (Cargo) getPersistor().getTabelaBasica();

		if( cargo.getIdCargo() == null  ) {
			setMsgErro( "Codigo do cargo necessita estar preenchido.", "ID_CARGO" );
			return false;
		}

		return true;
	}
}