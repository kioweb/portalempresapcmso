package jPcmso.negocio.geral.c;

import quatro.negocio.NegocioBasico;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.sql.Query;
import jPcmso.persistencia.geral.c.CodigoConselhoProfissional;
import jPcmso.persistencia.geral.c.CodigoConselhoProfissionalPersistor;

/**
 *
 * Classe de Neg�cio para CODIGOS_CONSELHO_PROFISSIONAL
 *
 * @author Samuel Antonio Klein
 * @see quatro.negocio.NegocioBasico
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
public class CodigoConselhoProfissionalNegocio extends NegocioBasico {

	/**
	 * M�todo Construtor b�sico.
	 *
	 */
	public CodigoConselhoProfissionalNegocio() throws QtSQLException {

		CodigoConselhoProfissionalPersistor persistor = new CodigoConselhoProfissionalPersistor();
		setPersistor( persistor );
	}

	/**
	 * M�todo Construtor que recebe uma conex�o.
	 *
	 */
	public CodigoConselhoProfissionalNegocio( Conexao cnx ) throws QtSQLException {

		CodigoConselhoProfissionalPersistor persistor = new CodigoConselhoProfissionalPersistor();
		setPersistor( persistor );
		setConexao( cnx );
	}

	/**
	 * M�todo Construtor que recebe uma Tabela B�sica de CODIGOS_CONSELHO_PROFISSIONAL
	 *
	 */
	public CodigoConselhoProfissionalNegocio( CodigoConselhoProfissional codigoConselhoProfissional ) throws QtSQLException {

		CodigoConselhoProfissionalPersistor persistor = new CodigoConselhoProfissionalPersistor( codigoConselhoProfissional );
		setPersistor( persistor );
	}

	/**
	 * M�todo Construtor que recebe uma conex�o e uma Tabela B�sica de CODIGOS_CONSELHO_PROFISSIONAL
	 *
	 */
	public CodigoConselhoProfissionalNegocio( Conexao cnx, CodigoConselhoProfissional codigoConselhoProfissional ) throws QtSQLException {

		CodigoConselhoProfissionalPersistor persistor = new CodigoConselhoProfissionalPersistor( codigoConselhoProfissional );
		setPersistor( persistor );
		setConexao( cnx );
	}

	/**
	 * M�todo que retorna uma nova Tabela B�sica de CODIGOS_CONSELHO_PROFISSIONAL
	 *
	 */
	public TabelaBasica geraNovaTabelaBasica() {

		return new CodigoConselhoProfissional();

	}

	public boolean antesDeInserir() throws QtSQLException {

		if( testaRegrasBasicas() ) {
			return testaReferencias();
		}
		return false;
	}

	public boolean antesDeAlterar() throws QtSQLException {

		if( testaRegrasBasicas() ) {
			return testaReferencias();
		}
		return false;
	}

	private boolean testaReferencias() throws QtSQLException {

		
		return true;
	}

	public boolean antesDeExcluir() {

		try {
			CodigoConselhoProfissional codigoConselhoProfissional = (CodigoConselhoProfissional) getPersistor().getTabelaBasica();

			Conexao cnx = getConexao();
			Query query = new Query( cnx );

			try {
				query.setSQL( "select count(*) from PCMSO.MEDICOS " );
				query.addSQL( " where CD_SIGLA like :prm2CdSigla" );
				query.setParameter( "prm2CdSigla", codigoConselhoProfissional.getCdSigla() );
				query.executeQuery();

				if( query.getInt( 1 ) > 0 ) {
					setMsgErro( "Não posso excluir registro em CODIGOS_CONSELHO_PROFISSIONAL( " + codigoConselhoProfissional.getCdSigla()+ " ) pois o mesmo é referenciado em PCMSO.MEDICOS" );
					return false;
				}
			} finally {
				query.liberaRecursos();
			}
		} catch ( Exception e ) {
			this.setMsgErro( e.getMessage() );
			return false;
		}
		return true;
	}

	public boolean testaRegrasBasicas() {

		CodigoConselhoProfissional codigoConselhoProfissional = (CodigoConselhoProfissional) getPersistor().getTabelaBasica();

		if( codigoConselhoProfissional.getCdSigla() == null  || codigoConselhoProfissional.getCdSigla().trim().equals( "" )  ) {
			setMsgErro( "Sigla do Conselho Profissional necessita estar preenchido.", "CD_SIGLA" );
			return false;
		}

		if( codigoConselhoProfissional.getDsConselho() == null  || codigoConselhoProfissional.getDsConselho().trim().equals( "" )  ) {
			setMsgErro( "Descrição do Conselho necessita estar preenchido.", "DS_CONSELHO" );
			return false;
		}

		return true;
	}
}