package jPcmso.negocio.geral.c;

import quatro.negocio.NegocioBasico;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import jPcmso.persistencia.geral.c.ComunicacaoAcidenteTrabalho;
import jPcmso.persistencia.geral.c.ComunicacaoAcidenteTrabalhoPersistor;

/**
 *
 * Classe de Neg�cio para PCMSO.COMUNICACAO_ACIDENTE_TRABALHO
 *
 * @author Samuel Antonio Klein
 * @see quatro.negocio.NegocioBasico
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:31 - importado por Samuel Antonio Klein
 *
 */
public class ComunicacaoAcidenteTrabalhoNegocio extends NegocioBasico {

	/**
	 * M�todo Construtor b�sico.
	 *
	 */
	public ComunicacaoAcidenteTrabalhoNegocio() throws QtSQLException {

		ComunicacaoAcidenteTrabalhoPersistor persistor = new ComunicacaoAcidenteTrabalhoPersistor();
		setPersistor( persistor );
	}

	/**
	 * M�todo Construtor que recebe uma conex�o.
	 *
	 */
	public ComunicacaoAcidenteTrabalhoNegocio( Conexao cnx ) throws QtSQLException {

		ComunicacaoAcidenteTrabalhoPersistor persistor = new ComunicacaoAcidenteTrabalhoPersistor();
		setPersistor( persistor );
		setConexao( cnx );
	}

	/**
	 * M�todo Construtor que recebe uma Tabela B�sica de PCMSO.COMUNICACAO_ACIDENTE_TRABALHO
	 *
	 */
	public ComunicacaoAcidenteTrabalhoNegocio( ComunicacaoAcidenteTrabalho comunicacaoAcidenteTrabalho ) throws QtSQLException {

		ComunicacaoAcidenteTrabalhoPersistor persistor = new ComunicacaoAcidenteTrabalhoPersistor( comunicacaoAcidenteTrabalho );
		setPersistor( persistor );
	}

	/**
	 * M�todo Construtor que recebe uma conex�o e uma Tabela B�sica de PCMSO.COMUNICACAO_ACIDENTE_TRABALHO
	 *
	 */
	public ComunicacaoAcidenteTrabalhoNegocio( Conexao cnx, ComunicacaoAcidenteTrabalho comunicacaoAcidenteTrabalho ) throws QtSQLException {

		ComunicacaoAcidenteTrabalhoPersistor persistor = new ComunicacaoAcidenteTrabalhoPersistor( comunicacaoAcidenteTrabalho );
		setPersistor( persistor );
		setConexao( cnx );
	}

	/**
	 * M�todo que retorna uma nova Tabela B�sica de PCMSO.COMUNICACAO_ACIDENTE_TRABALHO
	 *
	 */
	public TabelaBasica geraNovaTabelaBasica() {

		return new ComunicacaoAcidenteTrabalho();

	}

	public boolean antesDeInserir() throws QtSQLException {

		if( testaRegrasBasicas() ) {
			return testaReferencias();
		}
		return false;
	}

	public boolean antesDeAlterar() throws QtSQLException {

		if( testaRegrasBasicas() ) {
			return testaReferencias();
		}
		return false;
	}

	private boolean testaReferencias() throws QtSQLException {

		
		return true;
	}

	public boolean antesDeExcluir() {

		return true;
	}

	public boolean testaRegrasBasicas() {

		ComunicacaoAcidenteTrabalho comunicacaoAcidenteTrabalho = (ComunicacaoAcidenteTrabalho) getPersistor().getTabelaBasica();

		if( comunicacaoAcidenteTrabalho.getCdBeneficiarioCartao() == null  || comunicacaoAcidenteTrabalho.getCdBeneficiarioCartao().trim().equals( "" )  ) {
			setMsgErro( "Codigo de Beneficiario  necessita estar preenchido.", "CD_BENEFICIARIO_CARTAO" );
			return false;
		}

		if( comunicacaoAcidenteTrabalho.getSqPerfil() == null  ) {
			setMsgErro( "sequencia perfil necessita estar preenchido.", "SQ_PERFIL" );
			return false;
		}

		if( comunicacaoAcidenteTrabalho.getSqComunicacao() == null  ) {
			setMsgErro( "sequencia Comunicação necessita estar preenchido.", "SQ_COMUNICACAO" );
			return false;
		}

		return true;
	}
}