package jPcmso.negocio.geral.c;

import jPcmso.persistencia.geral.c.Cbo;
import jPcmso.persistencia.geral.c.CboLocalizador;
import quatro.negocio.ErroDeNegocio;
import quatro.negocio.NegocioEspecificoImpl;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;

public class CboNegocioEspecifico extends NegocioEspecificoImpl {

	public void antesDeInserir(Conexao arg0, TabelaBasica arg1) throws QtSQLException, ErroDeNegocio {
		if( arg1 != null ){
			Cbo c = (Cbo) arg1;
			if( CboLocalizador.existe( c.getCdCbo() ) ){
				throw new ErroDeNegocio( "já consta na base de Dados", "ID_CARGO" );
			}
		}
		
	}
}