package jPcmso.negocio.geral.c;

import quatro.negocio.NegocioBasico;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import jPcmso.persistencia.geral.c.Cidade;
import jPcmso.persistencia.geral.c.CidadePersistor;

/**
 *
 * Classe de Neg�cio para CIDADES
 *
 * @author Samuel Antonio Klein
 * @see quatro.negocio.NegocioBasico
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
public class CidadeNegocio extends NegocioBasico {

	/**
	 * M�todo Construtor b�sico.
	 *
	 */
	public CidadeNegocio() throws QtSQLException {

		CidadePersistor persistor = new CidadePersistor();
		setPersistor( persistor );
	}

	/**
	 * M�todo Construtor que recebe uma conex�o.
	 *
	 */
	public CidadeNegocio( Conexao cnx ) throws QtSQLException {

		CidadePersistor persistor = new CidadePersistor();
		setPersistor( persistor );
		setConexao( cnx );
	}

	/**
	 * M�todo Construtor que recebe uma Tabela B�sica de CIDADES
	 *
	 */
	public CidadeNegocio( Cidade cidade ) throws QtSQLException {

		CidadePersistor persistor = new CidadePersistor( cidade );
		setPersistor( persistor );
	}

	/**
	 * M�todo Construtor que recebe uma conex�o e uma Tabela B�sica de CIDADES
	 *
	 */
	public CidadeNegocio( Conexao cnx, Cidade cidade ) throws QtSQLException {

		CidadePersistor persistor = new CidadePersistor( cidade );
		setPersistor( persistor );
		setConexao( cnx );
	}

	/**
	 * M�todo que retorna uma nova Tabela B�sica de CIDADES
	 *
	 */
	public TabelaBasica geraNovaTabelaBasica() {

		return new Cidade();

	}

	public boolean antesDeInserir() throws QtSQLException {

		testaPersistor();
		Cidade cidade = (Cidade) getPersistor().getTabelaBasica();
		CidadePersistor persistor = (CidadePersistor) getPersistor();

		if ( cidade.getIdCidade() == null ) {

			try {
				cidade.setIdCidade( CidadePersistor.geraNovoIdCidade() );
			} catch (QtSQLException e) {
				setMsgErro( "CidadeNegocio - Antes de Inserir: Erro na Geração do Identificador: " + e.getMessage() );
				return false;
			}
		}
		if( testaRegrasBasicas() ) {
			return testaReferencias();
		}
		return false;
	}

	public boolean antesDeAlterar() throws QtSQLException {

		if( testaRegrasBasicas() ) {
			return testaReferencias();
		}
		return false;
	}

	private boolean testaReferencias() throws QtSQLException {

		
		return true;
	}

	public boolean antesDeExcluir() {

		return true;
	}

	public boolean testaRegrasBasicas() {

		Cidade cidade = (Cidade) getPersistor().getTabelaBasica();

		if( cidade.getCdEstado() == null  || cidade.getCdEstado().trim().equals( "" )  ) {
			setMsgErro( "Código do Estado necessita estar preenchido.", "CD_ESTADO" );
			return false;
		}

		if( cidade.getIdCidade() == null  ) {
			setMsgErro( "Identificador da Cidade necessita estar preenchido.", "ID_CIDADE" );
			return false;
		}

		if( cidade.getDsCidade() == null  || cidade.getDsCidade().trim().equals( "" )  ) {
			setMsgErro( "Descrição da Cidade (nome) necessita estar preenchido.", "DS_CIDADE" );
			return false;
		}

		return true;
	}
}