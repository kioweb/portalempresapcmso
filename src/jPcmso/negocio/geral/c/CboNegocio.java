package jPcmso.negocio.geral.c;

import quatro.negocio.NegocioBasico;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.sql.Query;
import jPcmso.persistencia.geral.c.Cbo;
import jPcmso.persistencia.geral.c.CboPersistor;

/**
 *
 * Classe de Negécio para PCMSO.CBOS
 *
 * @author ronei@unimed-uau.com.br
 * @see quatro.negocio.NegocioBasico
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 24/01/2018 11:18 - importado por ronei@unimed-uau.com.br
 *
 */
public class CboNegocio extends NegocioBasico {

	/**
	 * Método Construtor bésico.
	 *
	 */
	public CboNegocio() throws QtSQLException {

		CboPersistor persistor = new CboPersistor();
		setPersistor( persistor );
	}

	/**
	 * Método Construtor que recebe uma conexéo.
	 *
	 */
	public CboNegocio( Conexao cnx ) throws QtSQLException {

		CboPersistor persistor = new CboPersistor();
		setPersistor( persistor );
		setConexao( cnx );
	}

	/**
	 * Método Construtor que recebe uma Tabela Bésica de PCMSO.CBOS
	 *
	 */
	public CboNegocio( Cbo cbo ) throws QtSQLException {

		CboPersistor persistor = new CboPersistor( cbo );
		setPersistor( persistor );
	}

	/**
	 * Método Construtor que recebe uma conexéo e uma Tabela Bésica de PCMSO.CBOS
	 *
	 */
	public CboNegocio( Conexao cnx, Cbo cbo ) throws QtSQLException {

		CboPersistor persistor = new CboPersistor( cbo );
		setPersistor( persistor );
		setConexao( cnx );
	}

	/**
	 * Método que retorna uma nova Tabela Bésica de PCMSO.CBOS
	 *
	 */
	public TabelaBasica geraNovaTabelaBasica() {

		return new Cbo();

	}

	public boolean antesDeInserir() throws QtSQLException {

		if( testaRegrasBasicas() ) {
			return testaReferencias();
		}
		return false;
	}

	public boolean antesDeAlterar() throws QtSQLException {

		if( testaRegrasBasicas() ) {
			return testaReferencias();
		}
		return false;
	}

	private boolean testaReferencias() throws QtSQLException {

		
		return true;
	}

	public boolean antesDeExcluir() {

		try {
			Cbo cbo = (Cbo) getPersistor().getTabelaBasica();

			Conexao cnx = getConexao();
			Query query = new Query( cnx );

			try {
				query.setSQL( "select count(*) from PCMSO.BENEFICIARIOS_CBO " );
				query.addSQL( " where CD_CBO like :prm11CdCbo" );
				query.setParameter( "prm11CdCbo", cbo.getCdCbo() );
				query.executeQuery();

				if( query.getInt( 1 ) > 0 ) {
					setMsgErro( "Não posso excluir registro em PCMSO.CBOS( " + cbo.getCdCbo()+ " ) pois o mesmo é referenciado em PCMSO.BENEFICIARIOS_CBO" );
					return false;
				}
			
				query.setSQL( "select count(*) from PCMSO.HISTORICO_BENEFICIARIOS_CBO " );
				query.addSQL( " where CD_CBO like :prm7CdCbo" );
				query.setParameter( "prm7CdCbo", cbo.getCdCbo() );
				query.executeQuery();

				if( query.getInt( 1 ) > 0 ) {
					setMsgErro( "Não posso excluir registro em PCMSO.CBOS( " + cbo.getCdCbo()+ " ) pois o mesmo é referenciado em PCMSO.HISTORICO_BENEFICIARIOS_CBO" );
					return false;
				}
			
				query.setSQL( "select count(*) from PCMSO.EXAMES_COMPLEMENTARES_POR_CBO " );
				query.addSQL( " where CD_CBO like :prm2CdCbo" );
				query.setParameter( "prm2CdCbo", cbo.getCdCbo() );
				query.executeQuery();

				if( query.getInt( 1 ) > 0 ) {
					setMsgErro( "Não posso excluir registro em PCMSO.CBOS( " + cbo.getCdCbo()+ " ) pois o mesmo é referenciado em PCMSO.EXAMES_COMPLEMENTARES_POR_CBO" );
					return false;
				}
			
				query.setSQL( "select count(*) from PCMSO.EXAMES_OCUPACIONAIS " );
				query.addSQL( " where CD_CBO like :prm5CdCbo" );
				query.setParameter( "prm5CdCbo", cbo.getCdCbo() );
				query.executeQuery();

				if( query.getInt( 1 ) > 0 ) {
					setMsgErro( "Não posso excluir registro em PCMSO.CBOS( " + cbo.getCdCbo()+ " ) pois o mesmo é referenciado em PCMSO.EXAMES_OCUPACIONAIS" );
					return false;
				}
			
				query.setSQL( "select count(*) from PCMSO.LOTACAO_ATRIBUICAO " );
				query.addSQL( " where CD_CBO like :prm11CdCbo" );
				query.setParameter( "prm11CdCbo", cbo.getCdCbo() );
				query.executeQuery();

				if( query.getInt( 1 ) > 0 ) {
					setMsgErro( "Não posso excluir registro em PCMSO.CBOS( " + cbo.getCdCbo()+ " ) pois o mesmo é referenciado em PCMSO.LOTACAO_ATRIBUICAO" );
					return false;
				}
			} finally {
				query.liberaRecursos();
			}
		} catch ( Exception e ) {
			this.setMsgErro( e.getMessage() );
			return false;
		}
		return true;
	}

	public boolean testaRegrasBasicas() {

		Cbo cbo = (Cbo) getPersistor().getTabelaBasica();

		if( cbo.getCdCbo() == null  || cbo.getCdCbo().trim().equals( "" )  ) {
			setMsgErro( "Codigo de CBO necessita estar preenchido.", "CD_CBO" );
			return false;
		}

		return true;
	}
}