package jPcmso.negocio.visoes;

import quatro.negocio.NegocioBasico;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import jPcmso.persistencia.visoes.BuscaRiscoEmpresa;
import jPcmso.persistencia.visoes.BuscaRiscoEmpresaPersistor;

/**
 *
 * Classe de Neg�cio para PCMSO.BUSCA_RISCOS_EMPRESA
 *
 * @author Samuel Antonio Klein
 * @see quatro.negocio.NegocioBasico
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
public class BuscaRiscoEmpresaNegocio extends NegocioBasico {

	/**
	 * M�todo Construtor b�sico.
	 *
	 */
	public BuscaRiscoEmpresaNegocio() throws QtSQLException {

		BuscaRiscoEmpresaPersistor persistor = new BuscaRiscoEmpresaPersistor();
		setPersistor( persistor );
	}

	/**
	 * M�todo Construtor que recebe uma conex�o.
	 *
	 */
	public BuscaRiscoEmpresaNegocio( Conexao cnx ) throws QtSQLException {

		BuscaRiscoEmpresaPersistor persistor = new BuscaRiscoEmpresaPersistor();
		setPersistor( persistor );
		setConexao( cnx );
	}

	/**
	 * M�todo Construtor que recebe uma Tabela B�sica de PCMSO.BUSCA_RISCOS_EMPRESA
	 *
	 */
	public BuscaRiscoEmpresaNegocio( BuscaRiscoEmpresa buscaRiscoEmpresa ) throws QtSQLException {

		BuscaRiscoEmpresaPersistor persistor = new BuscaRiscoEmpresaPersistor( buscaRiscoEmpresa );
		setPersistor( persistor );
	}

	/**
	 * M�todo Construtor que recebe uma conex�o e uma Tabela B�sica de PCMSO.BUSCA_RISCOS_EMPRESA
	 *
	 */
	public BuscaRiscoEmpresaNegocio( Conexao cnx, BuscaRiscoEmpresa buscaRiscoEmpresa ) throws QtSQLException {

		BuscaRiscoEmpresaPersistor persistor = new BuscaRiscoEmpresaPersistor( buscaRiscoEmpresa );
		setPersistor( persistor );
		setConexao( cnx );
	}

	/**
	 * M�todo que retorna uma nova Tabela B�sica de PCMSO.BUSCA_RISCOS_EMPRESA
	 *
	 */
	public TabelaBasica geraNovaTabelaBasica() {

		return new BuscaRiscoEmpresa();

	}

	public boolean antesDeInserir() throws QtSQLException {

		if( testaRegrasBasicas() ) {
			return testaReferencias();
		}
		return false;
	}

	public boolean antesDeAlterar() throws QtSQLException {

		if( testaRegrasBasicas() ) {
			return testaReferencias();
		}
		return false;
	}

	private boolean testaReferencias() throws QtSQLException {

		
		return true;
	}

	public boolean antesDeExcluir() {

		return true;
	}

	public boolean testaRegrasBasicas() {

		BuscaRiscoEmpresa buscaRiscoEmpresa = (BuscaRiscoEmpresa) getPersistor().getTabelaBasica();

		if( buscaRiscoEmpresa.getIdRisco() == null  ) {
			setMsgErro( "Idetificador do Risco necessita estar preenchido.", "ID_RISCO" );
			return false;
		}

		if( buscaRiscoEmpresa.getCdEmpresa() == null  || buscaRiscoEmpresa.getCdEmpresa().trim().equals( "" )  ) {
			setMsgErro( " necessita estar preenchido.", "CD_EMPRESA" );
			return false;
		}

		return true;
	}
}