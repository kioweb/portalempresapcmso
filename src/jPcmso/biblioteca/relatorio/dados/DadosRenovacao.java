package jPcmso.biblioteca.relatorio.dados;

import quatro.util.QtData;
import jPcmso.persistencia.geral.e.Empresa;
import jPcmso.persistencia.geral.m.Medico;

public class DadosRenovacao {
	
	private String cdBeneficiarioCartao;
	private String dsNome;
	private String cdProcedimento;
	private String dsProcedimento;
	private QtData dtExecucaoExame;
	private Medico medicos;
	private Empresa empresas;
	private String tpExame;
	private String tpResultado;
	
	public String getCdBeneficiarioCartao() {
		return cdBeneficiarioCartao;
	}
	public void setCdBeneficiarioCartao(String cdBeneficiarioCartao) {
		this.cdBeneficiarioCartao = cdBeneficiarioCartao;
	}
	public String getDsNome() {
		return dsNome;
	}
	public void setDsNome(String dsNome) {
		this.dsNome = dsNome;
	}
	public String getCdProcedimento() {
		return cdProcedimento;
	}
	public void setCdProcedimento(String cdProcedimento) {
		this.cdProcedimento = cdProcedimento;
	}
	public String getDsProcedimento() {
		return dsProcedimento;
	}
	
	public void setDsProcedimento(String dsProcedimento) {
		this.dsProcedimento = dsProcedimento;
	}
	public QtData getDtExecucaoExame() {
		return dtExecucaoExame;
	}
	public void setDtExecucaoExame(QtData dtExecucaoExame) {
		this.dtExecucaoExame = dtExecucaoExame;
	}
	public Medico getMedicos() {
		return medicos;
	}
	public void setMedicos(Medico medicos) {
		this.medicos = medicos;
	}
	public Empresa getEmpresas() {
		return empresas;
	}
	public void setEmpresas(Empresa empresas) {
		this.empresas = empresas;
	}
	public void setTpResultado(String tpResultado) {
		this.tpResultado = tpResultado;
	}
	public String getTpResultado() {
		return tpResultado;
	}
	public void setTpExame(String tpExame) {
		this.tpExame = tpExame;
	}
	public String getTpExame() {
		return tpExame;
	}
}