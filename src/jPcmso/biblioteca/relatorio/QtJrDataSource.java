package jPcmso.biblioteca.relatorio;

import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.Vector;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import quatro.persistencia.CaminhadorBasico;
import quatro.sql.QtSQLException;

public class QtJrDataSource implements JRDataSource{
	
	private int totalReg = 0;
	private int indiceReg = 0;
	private CaminhadorBasico caminhador = null;
	
	private Iterator<?> iterator;
	private Object cursor;
	
	public QtJrDataSource( Vector<?> dados ) {
		super();
		totalReg = dados.size();
		iterator = dados.iterator();
	}
	
	public QtJrDataSource( CaminhadorBasico caminhador ) throws QtSQLException{
		this.caminhador = caminhador;
		totalReg = caminhador.getRecordCount();
		caminhador.ativaCaminhador();
		
	}
	
	public Object getFieldValue( JRField campo ) throws JRException {
		
		if ( getMtdGet( campo.getName() ) != null ) {
			try {
				Object o = getMtdGet( campo.getName() ).invoke( cursor, (Object[]) null  ); 
				mtdGet = null;
				return o;
			} catch ( Exception e ) {
				e.printStackTrace();
			}
		}
		return null;
	}
	
	private Method getMtdGet( String campo ) throws JRException {

		if( mtdGet == null ) {
			procuraMetodosGetSet( campo );
		}
		return mtdGet;
	}

	public boolean next() throws JRException {
		boolean retorno = false;
		
		if( iterator != null ){
			retorno = iterator.hasNext();
			if( retorno ) {
				cursor = iterator.next();
			}
		} else {
			retorno = caminhador.isEof();
			if( retorno ){
				cursor = caminhador.getTabelaBasica();
				try {
					caminhador.proximo();
				} catch ( QtSQLException e ) {
					throw new JRException( e );
				}
			}
		}
		indiceReg++;
		
		return retorno;
	}
	
	
	private Method mtdGet;
	
	private void procuraMetodosGetSet( String campo ) throws JRException {

		Method[] mt = cursor.getClass().getDeclaredMethods();
		String nmMetodoGet = getNmMetodoGet( campo );
		String nmMetodoIs  = getNmMetodoIs( campo );
		
		for( int i = 0; i < mt.length; i++ ) {
			if( mtdGet == null ) {
				if( mt[ i ].getName().equals( nmMetodoGet ) ) {
					mtdGet = mt[ i ];
				} else {
					if( mt[ i ].getName().equals( nmMetodoIs ) ) {
						mtdGet = mt[ i ];
					}
				}
			}

			if( mtdGet != null ) return;
		}
		
		mt = cursor.getClass().getSuperclass().getDeclaredMethods();
		nmMetodoGet = getNmMetodoGet( campo );
		nmMetodoIs  = getNmMetodoIs( campo );
		
		for( int i = 0; i < mt.length; i++ ) {
			if( mtdGet == null ) {
				if( mt[ i ].getName().equals( nmMetodoGet ) ) {
					mtdGet = mt[ i ];
				} else {
					if( mt[ i ].getName().equals( nmMetodoIs ) ) {
						mtdGet = mt[ i ];
					}
				}
			}

			if( mtdGet != null ) break;
		}
		
	}
	
	private String getNmMetodoGet( String campo ) {
		return "get" + campo.substring( 0, 1 ).toUpperCase() + campo.substring( 1 );
	}

	private String getNmMetodoIs( String campo ) {
		return "is" + campo.substring( 0, 1 ).toUpperCase() + campo.substring( 1 );
	}
	
	public int getTotalReg() {
		return totalReg;
	}
	
	public int getIndiceReg() {
		return indiceReg;
	}
}