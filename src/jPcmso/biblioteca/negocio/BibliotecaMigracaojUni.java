package jPcmso.biblioteca.negocio;

import jFace.JFaceException;
import jPcmso.negocio.geral.b.BeneficiarioCboNegocio;
import jPcmso.negocio.geral.e.EmpresaNegocio;
import jPcmso.negocio.geral.f.FiliaisEmpresaNegocio;
import jPcmso.persistencia.geral.b.BeneficiarioCbo;
import jPcmso.persistencia.geral.b.BeneficiarioCboLocalizador;
import jPcmso.persistencia.geral.e.Empresa;
import jPcmso.persistencia.geral.e.EmpresaLocalizador;
import jPcmso.persistencia.geral.f.FiliaisEmpresa;
import jUni.biblioteca.util.Util;
import jUni.persistencia.geral.b.Beneficiario;
import jUni.persistencia.geral.f.FilialEmpresa;
import jUni.persistencia.geral.f.FilialEmpresaLocalizador;
import quatro.sql.QtSQLException;
import quatro.util.Inteiro;

public class BibliotecaMigracaojUni {
	
	public static String buscaEmpresa( String cdUnimed, Integer idEmpresa ) throws JFaceException{
		try{
			Empresa empresa = EmpresaLocalizador.buscaEmpresa( Util.strZero( idEmpresa, 4 ) );
			
			if( empresa == null ){
				jUni.persistencia.geral.e.Empresa empresaJuni = jUni.persistencia.geral.e.EmpresaLocalizador.buscaEmpresa( cdUnimed, idEmpresa );
				
				if( empresaJuni != null ){
					empresa = new Empresa();
					empresa.setCdEmpresa( empresaJuni.getCdEmpresa() );
					empresa.setDsEmpresa( empresaJuni.getDsRazaoSocial() );
					empresa.setDtInclusao( empresa.getDtInclusao() );
					empresa.setNrCnae( empresaJuni.getCdCnae() );
					empresa.setNrCnpj( empresaJuni.getNrCnpj() );
					empresa.setNrCpfRepresentante( empresaJuni.getNrCpfResponsavel() );
					empresa.setDsNomeRepresentante( empresaJuni.getDsNomeResponsavel() );
					EmpresaNegocio en = new EmpresaNegocio( empresa );
					en.insere();
				}
			} else {
				jUni.persistencia.geral.e.Empresa empresaJuni = jUni.persistencia.geral.e.EmpresaLocalizador.buscaEmpresa( cdUnimed, idEmpresa );
				
				if( empresaJuni != null ){
					empresa.setDsEmpresa( empresaJuni.getDsNomeAbreviado() );
					empresa.setDtInclusao( empresaJuni.getDtInclusao() );
					empresa.setNrCnae( empresaJuni.getCdCnae() );
					empresa.setNrCnpj( empresaJuni.getNrCnpj() );
					empresa.setNrCpfRepresentante( empresaJuni.getNrCpfResponsavel() );
					empresa.setDsNomeRepresentante( empresa.getDsNomeRepresentante() );
					EmpresaNegocio en = new EmpresaNegocio( empresa );
					en.altera();
				}
			}
			return Util.strZero( idEmpresa, 4 );
		} catch (Exception e) {
			e.printStackTrace();
			throw new JFaceException( e );
		}
	}
	
	public static void buscaFilial( String cdEmpresa, int idFilial ) throws QtSQLException{
		if( !jPcmso.persistencia.geral.f.FiliaisEmpresaLocalizador.existe( cdEmpresa, idFilial ) ){
			FilialEmpresa fe = FilialEmpresaLocalizador.buscaFilialEmpresa( "0270", Inteiro.parseInt( cdEmpresa, 0 ), idFilial );
			if( fe != null ){
				FiliaisEmpresa fie = new FiliaisEmpresa();
				fie.setCdEmpresa( fe.getIdEmpresa() + "" );
				fie.setDsFilial( fe.getDsFilial() );
				fie.setIdFilial( fe.getIdFilial() );
				
				try{
					FiliaisEmpresaNegocio fn = new FiliaisEmpresaNegocio( fie );
					fn.insere();
				} catch (Exception e) {	}
				
			}
		}
	}
	
	public static void copiarBenficiarioParaPcsmo( Beneficiario beneficiario ){
		
		if( beneficiario.getIdEmpresa() == null ){
			beneficiario.setIdEmpresa( Inteiro.parseInt( beneficiario.getCdEmpresa(), 0 ) ); 
		}
		
		BeneficiarioCbo beneficiarioCbo = null;
		try{
			beneficiarioCbo = BeneficiarioCboLocalizador.buscaBeneficiarioCbo( beneficiario.getCdBeneficiarioCartao() );
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		boolean existe = true;
		if( beneficiarioCbo == null ){
			existe = false;
			beneficiarioCbo = new BeneficiarioCbo();
		}
		beneficiarioCbo.setDsAtividade( null );
		beneficiarioCbo.setCdCbo( null );
		beneficiarioCbo.setIdFuncao( null );
		beneficiarioCbo.setIdCargo( null );
		beneficiarioCbo.setIdSetor( null );
		try{
			beneficiarioCbo.setCdEmpresa( buscaEmpresa( beneficiario.getCdUnimed(), beneficiario.getIdEmpresa() ) );
		} catch (Exception e) {
			e.printStackTrace();
		}
		beneficiarioCbo.setCdRg(beneficiario.getCdRg() );
		beneficiarioCbo.setDtNascimento( beneficiario.getDtNascimento() );
		beneficiarioCbo.setTpSexo( beneficiario.getTpSexo() );
		beneficiarioCbo.setVlAltura( 0.00 );
		beneficiarioCbo.setVlPeso( 0.00 );
		beneficiarioCbo.setCdBeneficiarioCartao( beneficiario.getCdBeneficiarioCartao() );
		beneficiarioCbo.setDsNome( beneficiario.getDsNome() );
		beneficiarioCbo.setDtInclusao( beneficiario.getDtInclusao() );
		beneficiarioCbo.setDtExclusao( beneficiarioCbo.getDtExclusao() );
		beneficiarioCbo.setNrCpf( beneficiario.getCdCpf() );
		beneficiarioCbo.setNrPis( beneficiario.getCdPisPasep() );
		beneficiarioCbo.setIdFilial( beneficiario.getIdFilial() );
		beneficiarioCbo.setCdDigito( beneficiario.getCdDigito() );
		
		if( existe ){
			return;
		}
		
		try{
			try{
				BibliotecaMigracaojUni.buscaFilial( beneficiario.getCdEmpresa(), beneficiario.getIdFilial() );	
			} catch (Exception e) {	}
			
			BeneficiarioCboNegocio bn = new BeneficiarioCboNegocio( beneficiarioCbo );
			if( existe ){
				bn.altera();
			} else {
				bn.insere();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}