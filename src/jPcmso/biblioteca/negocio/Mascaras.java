package jPcmso.biblioteca.negocio;


import java.sql.Timestamp;

import quatro.util.QtData;

public class Mascaras {

	/** 
	 * M�todo para aplicar a mascara "##.###-###" no cep
	 * @param cep  - Cep a formatar (00000000)
	 * @return cep - Cep formatado  (00.000-000)
	 */
	public static String formatCep(String cep) {
		if( cep != null && !cep.trim().equals( "" ) ){
			cep = cep.replace("-", "");
			cep = cep.replace(".", "");
			return cep.substring( 0, 2 ) + "." + cep.substring( 2, 5 ) + "-" + cep.substring( 5 );
		}
		
		return cep;
	}
	
	/** 
	 * M�todo para aplicar a mascara "(##) ####-####" no telefone
	 * @param fone  - Telefone a formatar (00000000)
	 * @return fone - Telefone formatado  ((00) 0000-0000)
	 */
	public static String formatTelefone(String fone) {
		if( fone != null && fone.length() >= 9 )
			return "(" + fone.substring( 0, 2 ) + ") " + fone.substring( 2, 6 ) + "-" + fone.substring( 6 );
		else
			return fone;
	}
	
	/** 
	 * M�todo para aplicar a mascara "###.####.###-##" no cpf
	 * @param cpf  - Cpf a formatar (00000000)
	 * @return cpf - Cpf formatado  (000.000.000-00)
	 */
	public static String formatCPF(String cpf) {
		if( cpf != null && !cpf.trim().equals( "" ) )
			return  cpf.substring( 0, 3 ) + "." + cpf.substring( 3, 6 ) + "." + cpf.substring( 6, 9 ) + "-" + cpf.substring( 9 );
		else
			return cpf;
	}
	
	/** 
	 * M�todo para aplicar a mascara "dd/mm/aaaa" em uma data
	 * @param data  - Timestamp a formatar
	 * @return data - Data formatada (dd/mm/aaaa)
	 */
	public static String formatData( Timestamp data ){
		return new QtData( data ).toString();
	}

	/** 
	 * M�todo para aplicar a mascara "####-##" no c�digo do CBO
	 * @param cbo - Codigo a formatar (000000)
	 * @return cbo - CBO formatado  (0000-00)
	 */
	public static String formatCBO( String cbo ) {
		try{
			if( cbo.length() > 4 )
				return cbo.substring( 0, 4 ) + "-" + cbo.substring( 4 );
		} catch (Exception e) {}
		
		return cbo;
	}
	/** 
	 * M�todo para aplicar a mascara "##.###.###/####-##" no c�digo do CNPJ
	 * @param cbo - Codigo a formatar (00000000000000)
	 * @return cbo - CBO formatado  (00.000.000/0000-00)
	 */
	public static String formatCNPJ( String cnpj ){
		return aplicaMascara( "##.###.###/####-##", cnpj );
	}
	
	public static String aplicaMascara( String marcara, String valor ){
		
		int j = 0;
		StringBuilder sb = new StringBuilder();
		
		for ( int i = 0; i < marcara.length(); i++ ) {
			if ( marcara.charAt( i ) != '#' ) {
				sb.append( marcara.charAt( i ) );
			} else {
				try{
					sb.append( valor.charAt( j ) );
				} catch (IndexOutOfBoundsException e) {
					return valor;
				}
				j++;
			}
		}

		return sb.toString();
		
	}
}