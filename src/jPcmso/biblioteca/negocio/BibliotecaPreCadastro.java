package jPcmso.biblioteca.negocio;

import quatro.sql.ConexaoParaPoolDeConexoes;
import quatro.sql.PoolDeConexoes;
import quatro.sql.Query;
import jPcmso.biblioteca.manutencao.Util;
import jPcmso.negocio.geral.p.PreCadastroCboNegocio;
import jPcmso.persistencia.geral.b.BeneficiarioCbo;
import jPcmso.persistencia.geral.b.BeneficiarioCboLocalizador;
import jPcmso.persistencia.geral.p.PreCadastroCbo;
import jPcmso.persistencia.geral.p.PreCadastroCboLocalizador;
import jUni.persistencia.geral.b.Beneficiario;
import jUni.persistencia.geral.b.BeneficiarioLocalizador;

public class BibliotecaPreCadastro {
	
	
	public static void roda(){
		try {
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
			try {
				
				Query qry = new Query( cnx );
				
				qry.setSQL( " select * from beneficiarios where id_empresa >= 8149 and dt_exclusao is null" );
				int i = 0;
				qry.executeQuery();
				
				if( !qry.isEmpty() ){
					while( !qry.isAfterLast() ){
						Beneficiario beneficiario = new Beneficiario();
						BeneficiarioLocalizador.buscaCampos( beneficiario, qry );
						
						buscaBeneficario( beneficiario );
						
						System.out.println( "... " + i++ );
						qry.next();
					}
				}
				
				
			} finally {
				cnx.libera();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	public static PreCadastroCbo buscaBeneficario( Beneficiario beneficiario ) {
		PreCadastroCbo pre = null;

		try{

			if( beneficiario != null && !PreCadastroCboLocalizador.existeParaBeneficiario( beneficiario.getCdBeneficiarioCartao() ) ){

				pre = new PreCadastroCbo();

				Util.copiaTabela( beneficiario, pre );

				pre.setIdCidade( beneficiario.getIdCidadeResidencia() );
				pre.setCdEstado( beneficiario.getCdEstadoResidencia() );
				pre.setDsTelefoneContato( beneficiario.getDsTelefone() );
				pre.setNrCpf( beneficiario.getCdCpf() );
				pre.setNrPis( beneficiario.getCdPisPasep() );
				pre.setDsNomeDaMae( beneficiario.getDsNomeDaMae() );

				pre.setCdBeneficiarioCartao( beneficiario.getCdBeneficiarioCartao() );

				BeneficiarioCbo bCbo = BeneficiarioCboLocalizador.buscaBeneficiarioCbo( beneficiario.getCdBeneficiarioCartao() );

				if( bCbo != null ){

					pre.setCdCbo( bCbo.getCdCbo() );
					pre.setDsAtividade( bCbo.getDsAtividade() );
					pre.setIdFuncao( bCbo.getIdFuncao() );
					pre.setIdSetor( bCbo.getIdSetor() );
					pre.setIdCargo( bCbo.getIdCargo() );
					pre.setVlAltura( bCbo.getVlAltura() );
					pre.setVlPeso( bCbo.getVlPeso() );
					pre.setNrCtps( bCbo.getNrCtps() );
					pre.setNrSerieUf( bCbo.getNrSerieUf() );
				}

				PreCadastroCboNegocio pn = new PreCadastroCboNegocio( pre );
				pn.insere();

			}
		} catch (Exception e) {}

		return pre;
	}
	
	public static void roda2(){

		System.out.println( "asdfsdfsadsdaffdsa" );
		try {
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
			try {
				
				Query qry = new Query( cnx );
				
				qry.setSQL( " select b.* " );
				qry.addSQL( "		from beneficiarios b left join pcmso.beneficiarios_cbo cbo on (" );
				qry.addSQL( " 						b.cd_beneficiario_cartao = cbo.cd_beneficiario_cartao )" );
				qry.addSQL( " where id_empresa >= 8000 and cbo.cd_beneficiario_cartao is null and b.dt_exclusao is null" );
				
				qry.executeQuery();
				
				System.out.println( "rodando ..." );
				
				qry.last();
				int i = qry.getRow();
				qry.first();
				
				if( !qry.isEmpty() ){
					while( !qry.isAfterLast() ){
						Beneficiario beneficiario = new Beneficiario();
						BeneficiarioLocalizador.buscaCampos( beneficiario, qry );
						
						BibliotecaMigracaojUni.copiarBenficiarioParaPcsmo( beneficiario );
						System.out.print( i-- + ". ");
						qry.next();
					}
				}
				
				
			} finally {
				cnx.libera();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static String getFormatoHTML( String string ) {
		
		if( string != null && !string.trim().isEmpty() ) {
			string = string.replaceAll( "\n", "<br />" );
			string = string.replaceAll( "�", "&aacute;" );
			string = string.replaceAll( "�", "&Aacute;" );
			string = string.replaceAll( "�", "&eacute;" );
			string = string.replaceAll( "�", "&Eacute;" );
			string = string.replaceAll( "�", "&iacute;" );
			string = string.replaceAll( "�", "&Iacute;" );
			string = string.replaceAll( "�", "&oacute;" );
			string = string.replaceAll( "�", "&Oacute;" );
			string = string.replaceAll( "�", "&uacute;" );
			string = string.replaceAll( "�", "&Uacute;" );
			string = string.replaceAll( "�", "&atilde;" );
			string = string.replaceAll( "�", "&Atilde;" );
			string = string.replaceAll( "�", "&otilde;" );
			string = string.replaceAll( "�", "&Otilde;" );
			string = string.replaceAll( "�", "&acirc;" );
			string = string.replaceAll( "�", "&Acirc;" );
			string = string.replaceAll( "�", "&ecirc;" );
			string = string.replaceAll( "�", "&Ecirc;" );
			string = string.replaceAll( "�", "&ocirc;" );
			string = string.replaceAll( "�", "&Ocirc;" );
			string = string.replaceAll( "�", "&agrave;" );
			string = string.replaceAll( "�", "&Agrave;" );
			string = string.replaceAll( "�", "&ccedil;" );
			string = string.replaceAll( "�", "&Ccedil;" );
			string = string.replaceAll( "�", "&uuml;" );
			string = string.replaceAll( "�", "&Uuml;" );
			
			return string;
		}
		
		return "&nbsp;";
	}
}