package jPcmso.biblioteca.negocio;

import jPcmso.biblioteca.relatorio.dados.DadosRenovacao;
import jPcmso.negocio.geral.e.ExameBeneficiarioNegocio;
import jPcmso.negocio.geral.e.ExameComplementarNegocio;
import jPcmso.persistencia.geral.e.Empresa;
import jPcmso.persistencia.geral.e.ExameBeneficiario;
import jPcmso.persistencia.geral.e.ExameBeneficiarioCaminhador;
import jPcmso.persistencia.geral.e.ExameBeneficiarioLocalizador;
import jPcmso.persistencia.geral.e.ExameComplementar;
import jPcmso.persistencia.geral.e.ExameComplementarLocalizador;
import jPcmso.persistencia.geral.e.ExameOcupacional;
import jPcmso.persistencia.geral.e.ExameOcupacionalLocalizador;
import jPcmso.persistencia.geral.m.Medico;
import jPcmso.persistencia.geral.r.Risco;
import jPcmso.persistencia.geral.r.RiscoLocalizador;
import jPcmso.tipos.TipoExame;
import quatro.negocio.ErroDeNegocio;
import quatro.persistencia.CondicaoDeFiltro;
import quatro.persistencia.FiltroDeNavegador;
import quatro.sql.Conexao;
import quatro.sql.ConexaoParaPoolDeConexoes;
import quatro.sql.PoolDeConexoes;
import quatro.sql.QtSQLException;
import quatro.sql.Query;
import quatro.util.QtData;

public class BibliotecaExame {

	public static void geraExamesComplementares( ExameOcupacional exameOcupacional ) throws QtSQLException, ErroDeNegocio {
		FiltroDeNavegador filtro = new FiltroDeNavegador();
		filtro.adicionaCondicao( "CD_BENEFICIARIO_CARTAO", CondicaoDeFiltro.IGUAL, exameOcupacional.getCdBeneficiarioCartao() );
		filtro.setOrdemDeNavegacao( ExameBeneficiarioCaminhador.POR_ORDEM_DEFAULT );
		ExameBeneficiarioCaminhador ebc = new ExameBeneficiarioCaminhador( filtro );
		ebc.ativaCaminhador();
		try {
			if( !ebc.isEmpty() ) {
				while( !ebc.isEof() ) {
					ExameBeneficiario exameBeneficiario = ebc.getExameBeneficiario();
					ExameComplementar ec = new ExameComplementar();

					ec.setNrExame( exameOcupacional.getNrExame() );
					ec.setCdProcedimento( exameBeneficiario.getCdProcedimento() );

					try {
						ExameComplementarNegocio ecn = new ExameComplementarNegocio( ec );
						ecn.insere();

					} catch( Exception e ) {
						e.printStackTrace();
						throw new ErroDeNegocio( e );
					}

					ebc.proximo();
				}
			}
		} finally {
			ebc.liberaRecursos();
		}
	}

	public static void geraExamesComplementares( Conexao cnx, ExameOcupacional exameOcupacional ) throws QtSQLException, ErroDeNegocio {
		FiltroDeNavegador filtro = new FiltroDeNavegador();
		filtro.adicionaCondicao( "CD_BENEFICIARIO_CARTAO", CondicaoDeFiltro.IGUAL, exameOcupacional.getCdBeneficiarioCartao() );
		filtro.setOrdemDeNavegacao( ExameBeneficiarioCaminhador.POR_ORDEM_DEFAULT );
		ExameBeneficiarioCaminhador ebc = new ExameBeneficiarioCaminhador( cnx, filtro );
		ebc.ativaCaminhador();
		try {
			if( !ebc.isEmpty() ) {
				while( !ebc.isEof() ) {
					ExameBeneficiario exameBeneficiario = ebc.getExameBeneficiario();
					ExameComplementar ec = new ExameComplementar();

					ec.setNrExame( exameOcupacional.getNrExame() );
					ec.setCdProcedimento( exameBeneficiario.getCdProcedimento() );

					try {
						ExameComplementarNegocio ecn = new ExameComplementarNegocio( cnx, ec );
						ecn.insere();

					} catch( Exception e ) {
						e.printStackTrace();
						throw new ErroDeNegocio( e );
					}

					ebc.proximo();
				}
			}
		} finally {
			ebc.liberaRecursos();
		}
	}

	public static String getListaRiscos( String cdBeneficiarioCartao ) {
		StringBuilder sb = new StringBuilder();
		sb.append( "" );
		try {
			boolean primero = true;
			ConexaoParaPoolDeConexoes cnx = (ConexaoParaPoolDeConexoes) PoolDeConexoes.getConexao();
			try {
				Query qry = new Query( cnx );
				qry.setSQL( "select r.*, tp.* from pcmso.riscos_beneficiario rb, pcmso.riscos r, PCMSO.TIPOS_RISCOS tp where r.id_risco = rb.id_risco  and tp.tp_Risco = r.tp_Risco " );
				qry.addSQL( " and CD_BENEFICIARIO_CARTAO = '" + cdBeneficiarioCartao + "' order by r.tp_risco" );
				qry.executeQuery();
				String dsTipoRisco = "";
				if( !qry.isEmpty() ) {
					while( !qry.isAfterLast() ) {
						String ds = qry.getString( "ds_Tipo_Risco" );
						if( !dsTipoRisco.equals( ds ) ) {
							dsTipoRisco = ds;
							if( !primero ) {
								sb.append( "\n" );
							}
							sb.append( "<style Bold=\"true\" pdfFontName=\"Helvetica-Bold\">" );
							sb.append( ds.trim() );
							sb.append( "</style> - " );
						} else {
							if( !primero ) {
								sb.append( ", " );
							}
						}
						Risco risco = new Risco();
						RiscoLocalizador.buscaCampos( risco, qry );
						sb.append( risco.getDsRisco() );
						primero = false;
						qry.next();
					}
				}
			} finally {
				cnx.libera();
			}
		} catch( Exception e ) {
		}
		System.out.println( sb.toString() );
		return sb.toString();
	}

	public static DadosRenovacao buscaUltimaExame( Conexao cnx, String cdBeneficiarioCartao, String dsNome, QtData dtInicial, QtData dtFinal, Empresa empresa, Medico medico, String tpDataReferente ) throws QtSQLException {
		Query q = new Query( cnx );
		q.setSQL( " select * from pcmso.exames_ocupacionais eo " );
		q.addSQL( "		where eo.CD_BENEFICIARIO_CARTAO = :prmsCdBeneficiarioCartao " );
		q.addSQL( "   order by dt_execucao_exame desc limit 1; " );
		q.setParameter( "prmsCdBeneficiarioCartao", cdBeneficiarioCartao );

		q.executeQuery();
		if( !q.isEmpty() ) {
			ExameOcupacional eo = new ExameOcupacional();
			ExameOcupacionalLocalizador.buscaCampos( eo, q );
			QtData data = null;

			char tp = tpDataReferente.charAt( 0 );
			switch( tp ) {
				case 'A':
					data = eo.getDtEmissaoNota();
				case 'B':
					data = eo.getDtExecucaoExame();
				case 'C':
					data = eo.getDtRetornoNota();
				default:
					eo.getDtExecucaoExame();
			}
			if( data != null ) {
				data.setNrAno( data.getNrAno() + 1 );

				if( data.eMaiorOuIgual( dtInicial ) && data.eMenorOuIgual( dtFinal ) ) {
					DadosRenovacao d = new DadosRenovacao();
					d.setCdBeneficiarioCartao( cdBeneficiarioCartao );
					d.setCdProcedimento( eo.getCdProcedimento() );
					d.setDsProcedimento( "Consulta Ocupacional Peri�dico" );
					d.setDtExecucaoExame( data );
					d.setDsNome( dsNome );
					d.setEmpresas( empresa );
					d.setMedicos( medico );

					return d;
				}
			}
		}

		return null;
	}

	public static DadosRenovacao buscaUltimaExameComplementares( Conexao cnx, ExameBeneficiario exameBeneficiario, QtData dtInicial, QtData dtFinal ) throws QtSQLException {
		Query q = new Query( cnx );
		q.setSQL( " select eo.dt_execucao_exame, ec.* from pcmso.exames_ocupacionais eo, pcmso.exames_complementares ec " );
		q.addSQL( "		where eo.CD_BENEFICIARIO_CARTAO = :prmsCdBeneficiarioCartao " );
		q.addSQL( "		  and ec.CD_PROCEDIMENTO = :prmsCdProcedimento " );
		q.addSQL( "       and eo.nr_exame = ec.nr_exame " );
		q.addSQL( "   order by eo.dt_execucao_exame desc; " );
		q.setParameter( "prmsCdBeneficiarioCartao", exameBeneficiario.getCdBeneficiarioCartao() );
		q.setParameter( "prmsCdProcedimento", exameBeneficiario.getCdProcedimento() );
		q.executeQuery();
		System.out.println( q.preparaSQL() );
		if( !q.isEmpty() ) {
			while( !q.isAfterLast() ) {
				ExameComplementar eo = new ExameComplementar();
				ExameComplementarLocalizador.buscaCampos( eo, q );

				QtData data = QtData.criaQtData( q.getTimestamp( 1 ) );

				if( data != null ) {
					if( exameBeneficiario.getNrMesExame() == null ) {
						// TODO erro tem Exame sem mes de removacao
						exameBeneficiario.setNrMesExame( 12 );
					}

					data.setNrMes( data.getNrMes() + exameBeneficiario.getNrMesExame() );

					if( data.eMaiorOuIgual( dtInicial ) && data.eMenorOuIgual( dtFinal ) ) {
						DadosRenovacao d = new DadosRenovacao();
						d.setCdBeneficiarioCartao( exameBeneficiario.getCdBeneficiarioCartao() );
						d.setCdProcedimento( eo.getCdProcedimento() );
						d.setDtExecucaoExame( data );
						return d;
					}
				}
				q.next();
			}
		}

		return null;

	}

	public static DadosRenovacao buscaExameComplementaresSemestre( Conexao cnx, ExameBeneficiario exameBeneficiario, QtData dtInicial, QtData dtFinal ) throws QtSQLException {
		Query q = new Query( cnx );
		q.setSQL( " select eo.dt_execucao_exame, ec.* from pcmso.exames_ocupacionais eo, pcmso.exames_complementares ec " );
		q.addSQL( "		where eo.CD_BENEFICIARIO_CARTAO = :prmsCdBeneficiarioCartao " );
		q.addSQL( "		  and ec.CD_PROCEDIMENTO = :prmsCdProcedimento " );
		q.addSQL( "       and eo.nr_exame = ec.nr_exame " );
		q.addSQL( "   order by dt_exame_complementar desc; " );
		q.setParameter( "prmsCdBeneficiarioCartao", exameBeneficiario.getCdBeneficiarioCartao() );
		q.setParameter( "prmsCdProcedimento", exameBeneficiario.getCdProcedimento() );
		q.executeQuery();
		System.out.println( q.preparaSQL() );
		if( !q.isEmpty() ) {
			while( !q.isAfterLast() ) {
				ExameComplementar eo = new ExameComplementar();
				ExameComplementarLocalizador.buscaCampos( eo, q );

				// QtData data = QtData.criaQtData( q.getTimestamp( 1 ) );
				QtData data = eo.getDtExameComplementar();

				if( data != null ) {

					data.setNrMes( data.getNrMes() + 6 );

					if( data.eMaiorOuIgual( dtInicial ) && data.eMenorOuIgual( dtFinal ) ) {
						DadosRenovacao d = new DadosRenovacao();
						d.setCdBeneficiarioCartao( exameBeneficiario.getCdBeneficiarioCartao() );
						d.setCdProcedimento( eo.getCdProcedimento() );
						d.setDtExecucaoExame( data );
						return d;
					}
				}
				q.next();
			}
		}

		return null;

	}

	public static DadosRenovacao buscaExameComplementaresApartirInclusao( Conexao cnx, ExameBeneficiario exameBeneficiario, QtData dtInicial, QtData dtFinal ) throws QtSQLException {
		Query q = new Query( cnx );
		q.setSQL( " select eo.tp_exame, ec.*, be.dt_inclusao as dt_inclusao_beneficiario from pcmso.exames_ocupacionais eo, pcmso.exames_complementares ec, pcmso.beneficiarios_cbo be " );
		q.addSQL( "		where eo.CD_BENEFICIARIO_CARTAO = :prmsCdBeneficiarioCartao " );
		q.addSQL( "		  and ec.CD_PROCEDIMENTO = :prmsCdProcedimento " );
		q.addSQL( "		  and eo.CD_BENEFICIARIO_CARTAO = be.CD_BENEFICIARIO_CARTAO " );
		q.addSQL( "       and eo.nr_exame = ec.nr_exame " );
		q.addSQL( "   order by dt_exame_complementar desc; " );
		q.setParameter( "prmsCdBeneficiarioCartao", exameBeneficiario.getCdBeneficiarioCartao() );
		q.setParameter( "prmsCdProcedimento", exameBeneficiario.getCdProcedimento() );
		q.executeQuery();

		if( !q.isEmpty() ) {
			while( !q.isAfterLast() ) {
				ExameComplementar eo = new ExameComplementar();
				ExameComplementarLocalizador.buscaCampos( eo, q );

				QtData data = QtData.criaQtData( q.getTimestamp( "dt_inclusao_beneficiario" ) );

				if( data != null ) {
					if( exameBeneficiario.getNrMesAposInclusao() != null && exameBeneficiario.getNrMesAposInclusao().intValue() != 0 ) {
						data.setNrMes( data.getNrMes() + exameBeneficiario.getNrMesAposInclusao() );
						if( data.eMaiorOuIgual( dtInicial ) && data.eMenorOuIgual( dtFinal ) ) {
							DadosRenovacao d = new DadosRenovacao();
							d.setCdBeneficiarioCartao( exameBeneficiario.getCdBeneficiarioCartao() );
							d.setCdProcedimento( eo.getCdProcedimento() );
							d.setTpExame( TipoExame.getDsExame( q.getString( 1 ) ) );
							d.setDtExecucaoExame( data );
							return d;
						}
					}
				}
				q.next();
			}
		}

		return null;

	}

	public static DadosRenovacao buscaUltimaExameComplementaresExame( Conexao cnx, ExameBeneficiario exameBeneficiario, QtData dtInicial, QtData dtFinal ) throws QtSQLException {
		Query q = new Query( cnx );
		q.setSQL( " select eo.tp_exame, eo.dt_execucao_exame, ec.* from pcmso.exames_ocupacionais eo, pcmso.exames_complementares ec " );
		q.addSQL( "		where eo.CD_BENEFICIARIO_CARTAO = :prmsCdBeneficiarioCartao " );
		q.addSQL( "		  and ec.CD_PROCEDIMENTO = :prmsCdProcedimento " );
		q.addSQL( "       and eo.nr_exame = ec.nr_exame " );
		q.addSQL( "   order by eo.dt_execucao_exame desc; " );
		q.setParameter( "prmsCdBeneficiarioCartao", exameBeneficiario.getCdBeneficiarioCartao() );
		q.setParameter( "prmsCdProcedimento", exameBeneficiario.getCdProcedimento() );
		q.executeQuery();

		if( !q.isEmpty() ) {
			while( !q.isAfterLast() ) {
				ExameComplementar eo = new ExameComplementar();
				ExameComplementarLocalizador.buscaCampos( eo, q );

				QtData data = QtData.criaQtData( q.getTimestamp( 2 ) );
				if( data != null ) {
					if( data.eMaiorOuIgual( dtInicial ) && data.eMenorOuIgual( dtFinal ) ) {
						DadosRenovacao d = new DadosRenovacao();
						d.setCdBeneficiarioCartao( exameBeneficiario.getCdBeneficiarioCartao() );
						d.setCdProcedimento( eo.getCdProcedimento() );
						d.setTpResultado( eo.getTpResultado() );
						d.setTpExame( TipoExame.getDsExame( q.getString( 1 ) ) );
						d.setDtExecucaoExame( data );
						return d;
					}
				}
				q.next();
			}
		}

		return null;

	}

	public static void main( String[] args ) {

		// QtData q = new QtData();
		// q.setNrAno( 2011 );
		//
		// QtData d = new QtData();
		// d.setNrAno( 1999 );
		//
		// System.out.println( new QtData().eMaiorOuIgual( d ) );
		// System.out.println( new QtData().eMaiorOuIgual( q ) );

		System.out.println( System.getProperty( "user.home" ) );

	}

	public static String alterarMesExame( Conexao cnx, String cdEmpresaInicialProcessoAltera, String cdEmpresaFinalProcessoAltera, String cdProcedimentoInicialProcessoAltera, String cdProcedimentoFinalProcessoAltera, Integer nrMesExameProcessoAltera, Integer nrDiasAlteraInicialProcessoAltera, Integer nrDiasAlteraInicialFinalProcessoAltera ) throws QtSQLException {

		StringBuffer sb = new StringBuffer();

		Query q = new Query( cnx );

		q.setSQL( " select exame.* from pcmso.beneficiarios_cbo b, pcmso.exames_beneficiario exame " );
		q.addSQL( "	 where b.cd_beneficiario_cartao = exame.cd_beneficiario_cartao " );
		q.addSQL( "	   and dt_exclusao is null " );
		q.addSQL( "	   and b.cd_empresa >= '" + cdEmpresaInicialProcessoAltera + "'" );
		q.addSQL( "	   and b.cd_empresa <= '" + cdEmpresaFinalProcessoAltera + "' " );
		q.addSQL( "	   and cd_procedimento >= '" + cdProcedimentoInicialProcessoAltera + "' " );
		q.addSQL( "	   and cd_procedimento <= '" + cdProcedimentoFinalProcessoAltera + "' " );
		q.addSQL( "	   and cast( extract(day from now() - b.dt_inclusao) as int) >=  " + nrDiasAlteraInicialProcessoAltera );
		q.addSQL( "    and cast( extract(day from now() - b.dt_inclusao) as int) <= " + nrDiasAlteraInicialFinalProcessoAltera );

		q.executeQuery();

		if( !q.isEmpty() ) {
			sb.append( "Beneficiarios Alterados:\n" );
			sb.append( "Codigo\t\tprocedimento\n" );
			while( !q.isAfterLast() ) {
				ExameBeneficiario exameBeneficiario = new ExameBeneficiario();
				ExameBeneficiarioLocalizador.buscaCampos( exameBeneficiario, q );
				
				exameBeneficiario.setNrMesExame( nrMesExameProcessoAltera ); 
			
				ExameBeneficiarioNegocio en = new ExameBeneficiarioNegocio(exameBeneficiario);
				try {
					en.altera();
				} catch( ErroDeNegocio e ) {
					e.printStackTrace();
				}

				sb.append( exameBeneficiario.getCdBeneficiarioCartao() );
				sb.append( "\t\t" );
				sb.append( exameBeneficiario.getCdProcedimento() );
				sb.append( "\n" );

				q.next();
			}
		}

		return sb.toString();
	}

}