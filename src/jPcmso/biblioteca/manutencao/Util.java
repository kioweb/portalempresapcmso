package jPcmso.biblioteca.manutencao;

import jFace.JFaceException;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Method;

import net.sf.jasperreports.engine.JRException;
import quatro.persistencia.TabelaBasica;

public class Util {
	public static String gravaTrace( Exception e ) {

		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter( sw );
		
		e.printStackTrace( pw );
		
		return sw.toString();
	}
	
	public static Method getMtdGet( Object object, String campo ) throws JRException {
		Method mtdGet = null;
		 
		Method[] mt = object.getClass().getDeclaredMethods();
		String nmMetodoGet = getNmMetodoGet( campo );
		String nmMetodoIs  = getNmMetodoIs( campo );
		
		for( int i = 0; i < mt.length; i++ ) {
			if( mtdGet == null ) {
				if( mt[ i ].getName().equals( nmMetodoGet ) ) {
					mtdGet = mt[ i ];
				} else {
					if( mt[ i ].getName().equals( nmMetodoIs ) ) {
						mtdGet = mt[ i ];
					}
				}
			}

			if( mtdGet != null ) return mtdGet;
		}
		
		mt = object.getClass().getSuperclass().getDeclaredMethods();
		nmMetodoGet = getNmMetodoGet( campo );
		nmMetodoIs  = getNmMetodoIs( campo );
		
		for( int i = 0; i < mt.length; i++ ) {
			if( mtdGet == null ) {
				if( mt[ i ].getName().equals( nmMetodoGet ) ) {
					mtdGet = mt[ i ];
				} else {
					if( mt[ i ].getName().equals( nmMetodoIs ) ) {
						mtdGet = mt[ i ];
					}
				}
			}

			if( mtdGet != null ) return mtdGet;
		}
		
		return mtdGet;
	}
	
	public static Method getMtdGet( Class<?> classe, String campo ) throws JRException {
		Method mtdGet = null;
		 
		Method[] mt = classe.getDeclaredMethods();
		String nmMetodoGet = getNmMetodoGet( campo );
		String nmMetodoIs  = getNmMetodoIs( campo );
		
		for( int i = 0; i < mt.length; i++ ) {
			if( mtdGet == null ) {
				if( mt[ i ].getName().equals( nmMetodoGet ) ) {
					mtdGet = mt[ i ];
				} else {
					if( mt[ i ].getName().equals( nmMetodoIs ) ) {
						mtdGet = mt[ i ];
					}
				}
			}

			if( mtdGet != null ) return mtdGet;
		}
		
		mt = classe.getSuperclass().getDeclaredMethods();
		nmMetodoGet = getNmMetodoGet( campo );
		nmMetodoIs  = getNmMetodoIs( campo );
		
		for( int i = 0; i < mt.length; i++ ) {
			if( mtdGet == null ) {
				if( mt[ i ].getName().equals( nmMetodoGet ) ) {
					mtdGet = mt[ i ];
				} else {
					if( mt[ i ].getName().equals( nmMetodoIs ) ) {
						mtdGet = mt[ i ];
					}
				}
			}

			if( mtdGet != null ) return mtdGet;
		}
		
		return mtdGet;
	}
	
	private static String getNmMetodoGet( String campo ) {
		return "get" + campo.substring( 0, 1 ).toUpperCase() + campo.substring( 1 );
	}

	private static String getNmMetodoIs( String campo ) {
		return "is" + campo.substring( 0, 1 ).toUpperCase() + campo.substring( 1 );
	}
	
	public static boolean copiaTabela( TabelaBasica tabelaOrigem, TabelaBasica tabelaDestino ) throws JFaceException{
		try{
			try{
				Method[] mt = tabelaDestino.getClass().getDeclaredMethods();
				
				for (int i = 0; i < mt.length; i++) {
					Method met = mt[ i ];
					if( met.getName().startsWith( "set" ) ){
						Method method = Util.getMtdGet( tabelaOrigem, met.getName().substring( 3 ) );
						if( method != null ){
							Object vlCampo = method.invoke(tabelaOrigem, (Object[]) null  );
							met.invoke(tabelaDestino, vlCampo );
						}
					}
				}
			}catch (Exception e) {}
		} catch (Exception e) {
			e.printStackTrace();
			throw new JFaceException( e );
		}
		return true;
	}
}