package jPcmso.biblioteca.mensageiro;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import quatro.util.ParametrosSistema;

public class Mailer {
    
	/** Mensagem a ser enviada */
	private Email mensagem;
	/** Servidor de e-mail responsãvel por enviar as mensagens (host) */
	private String smtpHost;
	/** Servidor de e-mail responsãvel por enviar as mensagens (porta) */
	private String smtpPort;
	/** Conta de e-mail */
	private String contaEMail;
	/** Senha do e-mail */
	private String senhaEMail;
	
	/**
	 * Identifica se hã a necessidade de o sistema se autenticar no MTA para 
	 * enviar a mensagem
	 */
	private final boolean PRECISA_AUTENTICAR = true;
    
	/**
	 * @param mensagem E-mail
	 * @throws Exception 
	 */
	public Mailer( Email mensagem ) throws Exception {
		
		this.mensagem = mensagem;
		
		this.smtpHost = (String) ParametrosSistema.getInstance().getParametro( "SMTP-Host" );
		this.smtpPort = (String) ParametrosSistema.getInstance().getParametro( "SMTP-Port" ); 
		this.contaEMail = (String) ParametrosSistema.getInstance().getParametro( "contaEMail" ); 
		this.senhaEMail = (String) ParametrosSistema.getInstance().getParametro( "senhaEMail" ); 
		
		if( smtpHost == null ) {
			throw new Exception( "Erro. Não foi possível encontrar o host do servidor de e-mail nos parametros do sistema! (SMTP-Host)" );
		}
		if( smtpPort == null ) {
			throw new Exception( "Erro. Não foi possível encontrar a porta do servidor de e-mail nos parametros do sistema! (SMTP-Port)" );
		}
		if( contaEMail == null ) {
			throw new Exception( "Erro. Não foi possível encontrar a conta de e-mail nos parametros do sistema! (contaEMail)" );
		}
		if( senhaEMail == null ) {
			throw new Exception( "Erro. Não foi possível encontrar a senha do e-mail nos parametros do sistema! (senhaEMail)" );
		}
	}
	
	/**
	 * @param mensagem E-mail
	 * @param smtpHost Host do servidor de e-mail responsãvel por enviar as mensagens
	 * @param smtpPort Porta do servidor de e-mail responsãvel por enviar as mensagens
	 * @param contaEMail Conta do e-mail
	 * @param senhaEMail Senha do e-mail
	 */
	public Mailer(Email mensagem, String smtpHost, String smtpPort, String contaEMail, String senhaEMail ) {
    	
		this.mensagem = mensagem;
		
		this.smtpPort = smtpPort;
		this.smtpHost = smtpHost;
		this.contaEMail = contaEMail;
		this.senhaEMail = senhaEMail;
	}
	
	/**
	 * Mãtodo que recebe as propriedades de uma mensagem e tenta enviar a mesma
	 * para seu destinatãrio;
	 */
	public void send() throws Exception {

		try {
			Properties mailProps = new Properties();
			mailProps.put("mail.smtp.host", smtpHost );
			mailProps.put( "mail.smtp.port", smtpPort );

			SimpleAuth auth = null;
			
			if( smtpHost.equals("smtp.gmail.com") ) {
				mailProps.put( "mail.smtp.socketFactory.port", smtpPort );
				mailProps.put( "mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory" );
			}

			if ( this.PRECISA_AUTENTICAR ) {
				auth = new SimpleAuth( contaEMail, senhaEMail );
				
				mailProps.put( "mail.smtp.auth", "true" );
				mailProps.put( "mail.user", auth.username );
				mailProps.put( "mail.from", mensagem.getDe() );
				mailProps.put( "mail.to", mensagem.getPara() );
			} else {
				mailProps.put("mail.smtp.auth", "false");
			}
            
			Session mailSession = Session.getInstance( mailProps, auth );
			mailSession.setDebug( true );
			
			Message email = new MimeMessage(mailSession);
			
			String[] para = mensagem.getPara().split( "," );
			
			InternetAddress[] iPara = new InternetAddress[ para.length ];
			for (int i = 0; i < para.length; i++) {
				iPara[ i ] = new InternetAddress( para[ i ] );
			}
			email.setRecipients( Message.RecipientType.TO, iPara );
			
			if( mensagem.getCc() != null && !mensagem.getCc().trim().equals( "" ) ) {
				para = mensagem.getCc().split( "," );
				iPara = new InternetAddress[ para.length ];
				for (int i = 0; i < para.length; i++) {
					iPara[ i ] = new InternetAddress( para[ i ] );
				}
				email.setRecipients( Message.RecipientType.CC, iPara );
			}

			if( mensagem.getBcc() != null && !mensagem.getBcc().trim().equals( "" ) ) {
				para = mensagem.getBcc().split( "," );
				iPara = new InternetAddress[ para.length ];
				for (int i = 0; i < para.length; i++) {
					iPara[ i ] = new InternetAddress( para[ i ] );
				}
				email.setRecipients( Message.RecipientType.BCC, iPara );

			}

			email.setFrom( new InternetAddress( mensagem.getDe() ) );
			email.setSubject( mensagem.getAssunto() );

			Multipart multipart = new MimeMultipart();
			
			MimeBodyPart conteudo = new MimeBodyPart();
			conteudo.setContent( mensagem.getMensagem(), mensagem.getConteudo() );
			
			multipart.addBodyPart( conteudo );
			
			if( mensagem.getAnexos() != null ) {
				
				ArrayList<Anexo> anexos = mensagem.getAnexos();
				Iterator<Anexo> i = anexos.iterator();
				
				while( i.hasNext() ) {
					
					Anexo arquivoAnexo = (Anexo) i.next();
					
					BodyPart anexo = new MimeBodyPart();
					FileDataSource source = new FileDataSource( arquivoAnexo.getDsCaminhoArquivo() );
					anexo.setDataHandler( new DataHandler( source ) );
					anexo.setFileName( arquivoAnexo.getDsNomeArquivo() );
					
					multipart.addBodyPart( anexo );
				}
			}

			email.setContent( multipart );

			Transport.send( email );
            
		} catch ( MessagingException e ) {
			e.printStackTrace();
			throw new Exception("Erro ao processar mensagem: " + e.getMessage());
		}
	}
    
	/**
	 * Classe para autenticaãão de usuãrio/senha
	 */
	class SimpleAuth extends Authenticator {
        
		/** Nome de Usuãrio */
		public String username = null;
		/** Senha de Acesso */
		public String password = null;
        
		/**
		 * Traduz Strings de usuario/senha para um SimpleAuth
		 * @param user Nome do Usuãrio
		 * @param pwd Senha de Acesso
		 */
		public SimpleAuth( String user, String pwd ) {
			username = user;
			password = pwd;
		}
        
		/**
		 * Informa um PasswordAuthentication baseado nos atributos username e password 
		 */
		protected PasswordAuthentication getPasswordAuthentication() {
			
			return new PasswordAuthentication( username,password );
		}
	}    
}