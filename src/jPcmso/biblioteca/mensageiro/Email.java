package jPcmso.biblioteca.mensageiro;

import java.util.ArrayList;


public class Email {
    
    private String de;
    private String para;
    private String cc;
    private String bcc;
    private String assunto;
    private String mensagem;
    private ArrayList<Anexo> anexos = new ArrayList<Anexo>();
    
    private int conteudo;
    
    public static final int TEXT_PLAIN = 0;

    public static final int TEXT_HTML  = 1;
    
    public String getAssunto() {
    	
        return assunto;
    }

    public void setAssunto(String assunto) {
        this.assunto = assunto;
    }
    
    public String getBcc() {
        return bcc;
    }
    
    public void setBcc(String bcc) {
        this.bcc = bcc;
    }
   
    public String getCc() {
        return cc;
    }
    
    public void setCc(String cc) {
        this.cc = cc;
    }
    
    public String getConteudo() {
        switch(this.conteudo) {
        	case TEXT_HTML:
        	    return "text/html";

        	case TEXT_PLAIN:
        	
        	default:
        	    return "text/plain";
        }
    }

    public void setConteudo(int conteudo) {
        this.conteudo = conteudo;
    }

    public String getDe() {
        return de;
    }
    
    public void setDe(String de) {
        this.de = de;
    }
    
    public String getMensagem() {
        return mensagem;
    }
    
    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }
    
    public String getPara() {
        return para;
    }
    
    public void setPara(String para) {
        this.para = para;
    }
    
    public void anexarArquivo( String dsNomeArquivo, String dsCaminhoArquivo ) {
    	
    	Anexo anexo = new Anexo();
    	
    	anexo.setDsNomeArquivo( dsNomeArquivo );
    	anexo.setDsCaminhoArquivo( dsCaminhoArquivo );
    	
    	anexos.add( anexo );
    }
    
    public ArrayList<Anexo> getAnexos() {

    	return anexos;
    }
}

class Anexo {
	
	private String dsNomeArquivo;
	private String dsCaminhoArquivo;
	
	public String getDsNomeArquivo() {
		
		return dsNomeArquivo;
	}
	
	public void setDsNomeArquivo(String dsNomeArquivo) {
		
		this.dsNomeArquivo = dsNomeArquivo;
	}
	
	public String getDsCaminhoArquivo() {
		
		return dsCaminhoArquivo;
	}
	
	public void setDsCaminhoArquivo(String dsCaminhoArquivo) {
		
		this.dsCaminhoArquivo = dsCaminhoArquivo;
	}
}