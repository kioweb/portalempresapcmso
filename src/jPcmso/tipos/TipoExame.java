package jPcmso.tipos;

public class TipoExame {
	public static final char ADMISSIONAL = 'A';
	public static final char PERIODICO = 'P';
	public static final char RETORNO_AO_TRABALHO = 'R';
	public static final char MUDANCA_DE_FUNCAO = 'M';
	public static final char DEMISSIONAL = 'D';
	public static final char REFERENCIAL_PERIODICO = 'E';
	
	public static String getDsExame( String tipo ){
		switch( tipo.charAt( 0 ) ){
			case ADMISSIONAL : return "Admissional";
			case PERIODICO : return "Peri�dico";
			case RETORNO_AO_TRABALHO : return "Retorno ao Trabalho";
			case MUDANCA_DE_FUNCAO : return "Mudan�a de Fun��o";
			case DEMISSIONAL : return "Demissional";
			case REFERENCIAL_PERIODICO : return "Referencial Peri�dico";
		}
		return "";
	}
	
}