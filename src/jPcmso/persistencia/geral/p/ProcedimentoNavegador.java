package jPcmso.persistencia.geral.p;

import quatro.persistencia.FiltroDeNavegador;
import quatro.persistencia.Navegador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.sql.Query;

/**
 *
 * Classe navegador para PCMSO.PROCEDIMENTOS
 *
 * @author Samuel Antonio Klein
 * @see Navegador
 * @see Procedimento
 * @see ProcedimentoPersistor
 * @see ProcedimentoLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 20/12/2015 22:02 - importado por Samuel Antonio Klein
 *
 */
 public class ProcedimentoNavegador extends Navegador {

	
	/** Inst�ncia de Procedimento para manipula��o pelo navegador */
	private Procedimento procedimento;

	/** Construtor b�sico */
	public ProcedimentoNavegador()
	  throws QtSQLException { 

		procedimento = new Procedimento();
		primeiro();
	}

	/** Construtor b�sico que recebe uma conex�o */
	public ProcedimentoNavegador( Conexao conexao )
	  throws QtSQLException { 

		super.setConexao( conexao ); 
		procedimento = new Procedimento();
		primeiro();
	}

	/**
	 * Construtor que recebe uma inst�ncia de ProcedimentoFiltro e posiciona
	 * no primeiro registro que satisfaz suas condi��es 
	 */
	public ProcedimentoNavegador( FiltroDeNavegador filtro ) throws QtSQLException  {
		super.setFiltro( filtro );
		this.procedimento = new Procedimento();
		primeiro();
	}

	/** Construtor que recebe a chave da tabela PCMSO.PROCEDIMENTOS
	 * @param cdProcedimento Codigo do procedimento
	 */
	public ProcedimentoNavegador( String cdProcedimento )
	  throws QtSQLException { 

		procedimento = new Procedimento();
		this.procedimento.setCdProcedimento( cdProcedimento );
		
		busca();
	}

	/** Construtor que recebe  uma conex�o e a chave da tabela PCMSO.PROCEDIMENTOS
	 * @param cdProcedimento Codigo do procedimento
	 * @param conexao Conexao com o banco de dados
	 */
	public ProcedimentoNavegador( Conexao conexao, String cdProcedimento )
	  throws QtSQLException { 

		super.setConexao( conexao ); 
		procedimento = new Procedimento();
		this.procedimento.setCdProcedimento( cdProcedimento );
		
		busca();
	}

	/** M�todo que retorna uma tabela b�sica a que se refere o navegador */
	public TabelaBasica getTabelaBasica() {

		return this.procedimento;
	}

	/** M�todo que retorna a classe Procedimento relacionada ao navegador */
	public Procedimento getProcedimento() {

		return this.procedimento;
	}

	 /** M�todo que move para o primeiro registro da tabela */
	 public boolean primeiro() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select min( CD_PROCEDIMENTO ) from PCMSO.PROCEDIMENTOS" );
			query.addSQL( super.getCondicaoDeFiltro( FiltroDeNavegador.COM_WHERE ) 	);

			query.executeQuery();

			
			if( query.getString( 1 ) == null ) {

				super.setQueryStates( false, true, true );
			} else {
				this.procedimento = new Procedimento();

				procedimento.setCdProcedimento( query.getString( 1 ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o registro anterior na tabela */
	 public boolean anterior() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select max( CD_PROCEDIMENTO ) from PCMSO.PROCEDIMENTOS" );
			query.addSQL( " where CD_PROCEDIMENTO < :prm1CdProcedimento" );
			query.addSQL( super.getCondicaoDeFiltro() );

			query.setParameter( "prm1CdProcedimento", procedimento.getCdProcedimento() );
			
			query.executeQuery();

			
			if( query.getString( 1 ) == null ) {

				super.setFound( false );
				super.setBof( true );
			} else {
				this.procedimento = new Procedimento();

				procedimento.setCdProcedimento( query.getString( 1 ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o proximo registro da tabela */
	 public boolean proximo() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select min( CD_PROCEDIMENTO ) from PCMSO.PROCEDIMENTOS" );
			query.addSQL( " where CD_PROCEDIMENTO > :prm1CdProcedimento" );
			query.addSQL( super.getCondicaoDeFiltro() );

			query.setParameter( "prm1CdProcedimento", procedimento.getCdProcedimento() );

			query.executeQuery();

			
			if( query.getString( 1 ) == null ) {

				super.setFound( false );
				super.setEof( true );
			} else {
				this.procedimento = new Procedimento();

				procedimento.setCdProcedimento( query.getString( 1 ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o �ltimo registro da tabela */
	 public boolean ultimo() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select max( CD_PROCEDIMENTO ) from PCMSO.PROCEDIMENTOS" );
			query.addSQL( super.getCondicaoDeFiltro( FiltroDeNavegador.COM_WHERE ) 	);

			query.executeQuery();

			
			if( query.getString( 1 ) == null ) {

				super.setQueryStates( false, true, true );
			} else {
				this.procedimento = new Procedimento();

				procedimento.setCdProcedimento( query.getString( 1 ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	/**
	 * M�todo para buscar e preencher uma tupla de uma tabela
	 * @param tabelaBasica Recebe uma tabela basica com os atributos chave preenchidos
	 */
	public boolean busca( TabelaBasica tabelaBasica ) throws QtSQLException {

		if( !( tabelaBasica instanceof Procedimento ) )
			throw new QtSQLException( "Esperava um objeto Procedimento!" );

		this.procedimento = (Procedimento) tabelaBasica;
		return busca();

	}

	/**
	 * m�todo interno para buscar e preencher uma tupla de uma tabela
	 * Utiliza a Inst�ncia de Procedimento da classe para fazer a busca
	 */
	private boolean busca() throws QtSQLException {

		ProcedimentoLocalizador localizador = new ProcedimentoLocalizador( getConexao(), this.procedimento );

		if( localizador.isEmpty() ) {

			super.setQueryStates( false, true, true );
			return false;

		} else {

			super.setQueryStates( true, false, false );
			return true;
		}
	}
}