package jPcmso.persistencia.geral.p;

import quatro.persistencia.Localizador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.QtSQLException;
import quatro.sql.Query;
import quatro.sql.Conexao;
import quatro.sql.PoolDeConexoes;
import quatro.sql.ConexaoParaPoolDeConexoes;
import quatro.util.QtData;

/**
 *
 * Classe localizador para PCMSO.PRE_CADASTRO_CBO
 *
 * @author Lucio Nascimento Teixeira
 * @see Localizador
 * @see PreCadastroCbo
 * @see PreCadastroCboNavegador
 * @see PreCadastroCboPersistor
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 23/06/2020 23:58 - importado por Lucio Nascimento Teixeira
 *
 */
 public class PreCadastroCboLocalizador extends Localizador {

	/** Inst�ncia de PreCadastroCbo para manipula��o pelo localizador */
	private PreCadastroCbo preCadastroCbo;

	/** Construtor que recebe uma conex�o e um objeto PreCadastroCbo completo */
	public PreCadastroCboLocalizador( Conexao conexao, PreCadastroCbo preCadastroCbo ) throws QtSQLException {

		super.setConexao( conexao );

		this.preCadastroCbo = preCadastroCbo;
		busca();
	}

	/** Construtor que recebe um objeto PreCadastroCbo completo */
	public PreCadastroCboLocalizador( PreCadastroCbo preCadastroCbo ) throws QtSQLException {

		this.preCadastroCbo = preCadastroCbo;
		busca();
	}

	/** 
	 * Construtor que recebe uma conex�o e a chave da tabela PCMSO.PRE_CADASTRO_CBO
	 * @param idPreCadastro SEQUENCIA de BENEFICARIOS
	 * @param conexao Conexao com o banco de dados
	 */
	public PreCadastroCboLocalizador( Conexao conexao, int idPreCadastro ) throws QtSQLException { 

		super.setConexao( conexao ); 
		preCadastroCbo = new PreCadastroCbo();
		this.preCadastroCbo.setIdPreCadastro( new Integer( idPreCadastro ) );
		
		busca();
	}

	/** 
	 * Construtor que recebe a chave da tabela PCMSO.PRE_CADASTRO_CBO
	 * @param idPreCadastro SEQUENCIA de BENEFICARIOS
	 */
	public PreCadastroCboLocalizador( int idPreCadastro ) throws QtSQLException { 

		preCadastroCbo = new PreCadastroCbo();
		this.preCadastroCbo.setIdPreCadastro( new Integer( idPreCadastro ) );
		
		busca();
	}

	public static boolean existe( Conexao cnx, int idPreCadastro ) throws QtSQLException {

		Query q = new Query( cnx );

		try {
			q.setSQL( "select count(*) from PCMSO.PRE_CADASTRO_CBO" );
			q.addSQL( " where ID_PRE_CADASTRO = :prm1IdPreCadastro" );

			q.setParameter( "prm1IdPreCadastro", idPreCadastro );
			q.executeQuery();

			return q.getInt( 1 ) > 0;
		} finally {
			q.liberaRecursos();
		}
	}

	public static boolean existe( int idPreCadastro ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return existe( cnx, idPreCadastro );
		} finally {
			cnx.libera();
		}
	}

	public static boolean existe( Conexao cnx, PreCadastroCbo preCadastroCbo ) throws QtSQLException {

		return existe( cnx, preCadastroCbo.getIdPreCadastro() );
	}

	public static boolean existe( PreCadastroCbo preCadastroCbo ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return existe( cnx, preCadastroCbo.getIdPreCadastro() );
		} finally {
			cnx.libera();
		}
	}

	public static PreCadastroCbo buscaPreCadastroCbo( Conexao cnx, int idPreCadastro ) throws QtSQLException {

		Query q = new Query( cnx );

		try {
			q.setSQL( "select * from PCMSO.PRE_CADASTRO_CBO" );
			q.addSQL( " where ID_PRE_CADASTRO = :prm1IdPreCadastro" );

			q.setParameter( "prm1IdPreCadastro", idPreCadastro );
			q.executeQuery();

			if( q.isEmpty() ) {
				return null;
			} else {
				PreCadastroCbo preCadastroCbo = new PreCadastroCbo();
				buscaCampos( preCadastroCbo, q );
				return preCadastroCbo;
			}
		} finally {
			q.liberaRecursos();
		}
	}

	public static PreCadastroCbo buscaPreCadastroCbo( int idPreCadastro ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return buscaPreCadastroCbo( cnx, idPreCadastro );
		} finally { 
			cnx.libera();
		}
	}

	/** 
	 * Construtor que recebe uma conex�o e a chave da tabela PCMSO.PRE_CADASTRO_CBO
	 * @param idPreCadastro SEQUENCIA de BENEFICARIOS
	 * @param conexao Conexao com o banco de dados
	 */
	public PreCadastroCboLocalizador( Conexao conexao, Integer idPreCadastro ) throws QtSQLException {

		super.setConexao( conexao ); 
		preCadastroCbo = new PreCadastroCbo();
		this.preCadastroCbo.setIdPreCadastro( idPreCadastro );
		
		busca();
	}

	/** 
	 * Construtor que recebe a chave da tabela PCMSO.PRE_CADASTRO_CBO
	 * @param idPreCadastro SEQUENCIA de BENEFICARIOS
	 */
	public PreCadastroCboLocalizador( Integer idPreCadastro ) throws QtSQLException {

		preCadastroCbo = new PreCadastroCbo();
		this.preCadastroCbo.setIdPreCadastro( idPreCadastro );
		
		busca();
	}

	public static PreCadastroCbo buscaporBeneficiario( Conexao cnx, String cdBeneficiarioCartao ) throws QtSQLException {

		Query query = new Query( cnx );

		try {
			query.setSQL( "select * from PCMSO.PRE_CADASTRO_CBO" );
			query.addSQL( " where CD_BENEFICIARIO_CARTAO = :prm3CdBeneficiarioCartao" );

			query.setParameter( "prm3CdBeneficiarioCartao", cdBeneficiarioCartao );
			query.executeQuery();

			if( query.isEmpty() ) {
				return null;
			} else {
				PreCadastroCbo preCadastroCbo = new PreCadastroCbo();
				buscaCampos( preCadastroCbo, query );
				return preCadastroCbo;
			}
		} finally {
			query.liberaRecursos();
		}
	}

	public static PreCadastroCbo buscaporBeneficiario( String cdBeneficiarioCartao ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
		try {
			return buscaporBeneficiario( cnx, cdBeneficiarioCartao );
		} finally { 
			cnx.libera();
		}

	}

	public static boolean existeParaBeneficiario( Conexao cnx, String cdBeneficiarioCartao ) throws QtSQLException {

		Query query = new Query( cnx );

		try {
			query.setSQL( "select count(*) from PCMSO.PRE_CADASTRO_CBO" );
			query.addSQL( " where CD_BENEFICIARIO_CARTAO = :prm3CdBeneficiarioCartao" );

			query.setParameter( "prm3CdBeneficiarioCartao", cdBeneficiarioCartao );
			query.executeQuery();

			return query.getInt( 1 ) > 0;
		} finally {
			query.liberaRecursos();
		}
	}

	public static boolean existeParaBeneficiario( String cdBeneficiarioCartao ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
		try {
			return existeParaBeneficiario( cnx, cdBeneficiarioCartao );
		} finally {
			cnx.libera();
		}
	}

	public static void buscaCampos( PreCadastroCbo preCadastroCbo, Query query ) throws QtSQLException {

		preCadastroCbo.setIdPreCadastro( query.getInteger( "ID_PRE_CADASTRO" ) );
		preCadastroCbo.setCdUnimed( query.getString( "CD_UNIMED" ) );
		preCadastroCbo.setCdBeneficiarioCartao( query.getString( "CD_BENEFICIARIO_CARTAO" ) );
		preCadastroCbo.setTpSolicitacao( query.getString( "TP_SOLICITACAO" ) );
		preCadastroCbo.setCdNaEmpresa( query.getString( "CD_NA_EMPRESA" ) );
		preCadastroCbo.setCdEmpresa( query.getString( "CD_EMPRESA" ) );
		preCadastroCbo.setIdLotacao( query.getInteger( "ID_LOTACAO" ) );
		preCadastroCbo.setIdFuncao( query.getInteger( "ID_FUNCAO" ) );
		preCadastroCbo.setIdFilial( query.getInteger( "ID_FILIAL" ) );
		preCadastroCbo.setIdSetor( query.getInteger( "ID_SETOR" ) );
		preCadastroCbo.setIdCargo( query.getInteger( "ID_CARGO" ) );
		preCadastroCbo.setCdCbo( query.getString( "CD_CBO" ) );
		preCadastroCbo.setCdRg( query.getString( "CD_RG" ) );
		preCadastroCbo.setDsOrgaoEmissorRg( query.getString( "DS_ORGAO_EMISSOR_RG" ) );
		preCadastroCbo.setNrCpf( query.getString( "NR_CPF" ) );
		preCadastroCbo.setDtNascimento( QtData.criaQtData( query.getTimestamp( "DT_NASCIMENTO" ) ) );
		preCadastroCbo.setTpSexo( query.getString( "TP_SEXO" ) );
		preCadastroCbo.setVlPeso( query.getDoubleObject( "VL_PESO" ) );
		preCadastroCbo.setVlAltura( query.getDoubleObject( "VL_ALTURA" ) );
		preCadastroCbo.setDsAtividade( query.getString( "DS_ATIVIDADE" ) );
		preCadastroCbo.setDsNome( query.getString( "DS_NOME" ) );
		preCadastroCbo.setTpBrPdh( query.getString( "TP_BR_PDH" ) );
		preCadastroCbo.setNrCtps( query.getString( "NR_CTPS" ) );
		preCadastroCbo.setNrSerieUf( query.getString( "NR_SERIE_UF" ) );
		preCadastroCbo.setNrPis( query.getString( "NR_PIS" ) );
		preCadastroCbo.setDsLogradouro( query.getString( "DS_LOGRADOURO" ) );
		preCadastroCbo.setDsNumeroLogradouro( query.getString( "DS_NUMERO_LOGRADOURO" ) );
		preCadastroCbo.setDsComplementoLogradouro( query.getString( "DS_COMPLEMENTO_LOGRADOURO" ) );
		preCadastroCbo.setDsBairro( query.getString( "DS_BAIRRO" ) );
		preCadastroCbo.setIdCidade( query.getInteger( "ID_CIDADE" ) );
		preCadastroCbo.setCdEstado( query.getString( "CD_ESTADO" ) );
		preCadastroCbo.setDtInclusao( QtData.criaQtData( query.getTimestamp( "DT_INCLUSAO" ) ) );
		preCadastroCbo.setDtExclusao( QtData.criaQtData( query.getTimestamp( "DT_EXCLUSAO" ) ) );
		preCadastroCbo.setDtSolicitacaoExclusao( QtData.criaQtData( query.getTimestamp( "DT_SOLICITACAO_EXCLUSAO" ) ) );
		preCadastroCbo.setDtDemissao( QtData.criaQtData( query.getTimestamp( "DT_DEMISSAO" ) ) );
		preCadastroCbo.setDtSolicitacao( QtData.criaQtData( query.getTimestamp( "DT_SOLICITACAO" ) ) );
		preCadastroCbo.setDtUltimoMovimento( QtData.criaQtData( query.getTimestamp( "DT_ULTIMO_MOVIMENTO" ) ) );
		preCadastroCbo.setIdMotivoSolicitacaoExclusao( query.getInteger( "ID_MOTIVO_SOLICITACAO_EXCLUSAO" ) );
		preCadastroCbo.setDsNomeDaMae( query.getString( "DS_NOME_DA_MAE" ) );
		preCadastroCbo.setIdUsuarioDigitou( query.getInteger( "ID_USUARIO_DIGITOU" ) );
		preCadastroCbo.setDtAdmissao( QtData.criaQtData( query.getTimestamp( "DT_ADMISSAO" ) ) );
		preCadastroCbo.setTpEstadoCivil( query.getString( "TP_ESTADO_CIVIL" ) );
		preCadastroCbo.setDsTelefoneContato( query.getString( "DS_TELEFONE_CONTATO" ) );
		preCadastroCbo.setTpDeficiencia( query.getString( "TP_DEFICIENCIA" ) );
		preCadastroCbo.setSnPossuiDeficiencia( query.getString( "SN_POSSUI_DEFICIENCIA" ) );
		preCadastroCbo.setSnTrabalhoEmAltura( query.getString( "SN_TRABALHO_EM_ALTURA" ) );
		preCadastroCbo.setSnTrabalhoConfinado( query.getString( "SN_TRABALHO_CONFINADO" ) );
		preCadastroCbo.setSnFitossanitarios( query.getString( "SN_FITOSSANITARIOS" ) );
	}

	/** M�todo que retorna uma tabela b�sica a que se refere o localizador */
	public TabelaBasica getTabelaBasica() {

		return this.preCadastroCbo;
	}

	/** M�todo que retorna a classe PreCadastroCbo relacionada ao localizador */
	public PreCadastroCbo getPreCadastroCbo() {

		return this.preCadastroCbo;
	}

	/**
	 * M�todo para buscar e preencher uma tupla de uma tabela
	 * @param tabelaBasica Recebe uma tabela basica com os atributos chave preenchidos
	 */
	public boolean busca( TabelaBasica tabelaBasica ) throws QtSQLException {

		if( !( tabelaBasica instanceof PreCadastroCbo ) )
			throw new QtSQLException( "Esperava um objeto PreCadastroCbo!" );

		this.preCadastroCbo = (PreCadastroCbo) tabelaBasica;
		return busca();

	}

	/**
	 * M�todo privado para buscar e preencher uma tupla de uma tabela
	 * Este m�todo busca os dados da inst�ncia de PreCadastroCbo atual
	 */
	private boolean busca() throws QtSQLException {

		boolean ret = false;

		super.estabeleceConexao();
		Query query = super.criaNovaQuery();

		try {

			query.setSQL( "select * from PCMSO.PRE_CADASTRO_CBO" );
			query.addSQL( " where ID_PRE_CADASTRO = :prm1IdPreCadastro" );

			query.setParameter( "prm1IdPreCadastro", preCadastroCbo.getIdPreCadastro() );
			query.executeQuery();

			if( query.isEmpty() ) {
				super.setEmpty( true );
				preCadastroCbo = null;
			} else {
				super.setEmpty( false );
				
				preCadastroCbo.setIdPreCadastro( query.getInteger( "ID_PRE_CADASTRO" ) );
				preCadastroCbo.setCdUnimed( query.getString( "CD_UNIMED" ) );
				preCadastroCbo.setCdBeneficiarioCartao( query.getString( "CD_BENEFICIARIO_CARTAO" ) );
				preCadastroCbo.setTpSolicitacao( query.getString( "TP_SOLICITACAO" ) );
				preCadastroCbo.setCdNaEmpresa( query.getString( "CD_NA_EMPRESA" ) );
				preCadastroCbo.setCdEmpresa( query.getString( "CD_EMPRESA" ) );
				preCadastroCbo.setIdLotacao( query.getInteger( "ID_LOTACAO" ) );
				preCadastroCbo.setIdFuncao( query.getInteger( "ID_FUNCAO" ) );
				preCadastroCbo.setIdFilial( query.getInteger( "ID_FILIAL" ) );
				preCadastroCbo.setIdSetor( query.getInteger( "ID_SETOR" ) );
				preCadastroCbo.setIdCargo( query.getInteger( "ID_CARGO" ) );
				preCadastroCbo.setCdCbo( query.getString( "CD_CBO" ) );
				preCadastroCbo.setCdRg( query.getString( "CD_RG" ) );
				preCadastroCbo.setDsOrgaoEmissorRg( query.getString( "DS_ORGAO_EMISSOR_RG" ) );
				preCadastroCbo.setNrCpf( query.getString( "NR_CPF" ) );
				preCadastroCbo.setDtNascimento( QtData.criaQtData( query.getTimestamp( "DT_NASCIMENTO" ) ) );
				preCadastroCbo.setTpSexo( query.getString( "TP_SEXO" ) );
				preCadastroCbo.setVlPeso( query.getDoubleObject( "VL_PESO" ) );
				preCadastroCbo.setVlAltura( query.getDoubleObject( "VL_ALTURA" ) );
				preCadastroCbo.setDsAtividade( query.getString( "DS_ATIVIDADE" ) );
				preCadastroCbo.setDsNome( query.getString( "DS_NOME" ) );
				preCadastroCbo.setTpBrPdh( query.getString( "TP_BR_PDH" ) );
				preCadastroCbo.setNrCtps( query.getString( "NR_CTPS" ) );
				preCadastroCbo.setNrSerieUf( query.getString( "NR_SERIE_UF" ) );
				preCadastroCbo.setNrPis( query.getString( "NR_PIS" ) );
				preCadastroCbo.setDsLogradouro( query.getString( "DS_LOGRADOURO" ) );
				preCadastroCbo.setDsNumeroLogradouro( query.getString( "DS_NUMERO_LOGRADOURO" ) );
				preCadastroCbo.setDsComplementoLogradouro( query.getString( "DS_COMPLEMENTO_LOGRADOURO" ) );
				preCadastroCbo.setDsBairro( query.getString( "DS_BAIRRO" ) );
				preCadastroCbo.setIdCidade( query.getInteger( "ID_CIDADE" ) );
				preCadastroCbo.setCdEstado( query.getString( "CD_ESTADO" ) );
				preCadastroCbo.setDtInclusao( QtData.criaQtData( query.getTimestamp( "DT_INCLUSAO" ) ) );
				preCadastroCbo.setDtExclusao( QtData.criaQtData( query.getTimestamp( "DT_EXCLUSAO" ) ) );
				preCadastroCbo.setDtSolicitacaoExclusao( QtData.criaQtData( query.getTimestamp( "DT_SOLICITACAO_EXCLUSAO" ) ) );
				preCadastroCbo.setDtDemissao( QtData.criaQtData( query.getTimestamp( "DT_DEMISSAO" ) ) );
				preCadastroCbo.setDtSolicitacao( QtData.criaQtData( query.getTimestamp( "DT_SOLICITACAO" ) ) );
				preCadastroCbo.setDtUltimoMovimento( QtData.criaQtData( query.getTimestamp( "DT_ULTIMO_MOVIMENTO" ) ) );
				preCadastroCbo.setIdMotivoSolicitacaoExclusao( query.getInteger( "ID_MOTIVO_SOLICITACAO_EXCLUSAO" ) );
				preCadastroCbo.setDsNomeDaMae( query.getString( "DS_NOME_DA_MAE" ) );
				preCadastroCbo.setIdUsuarioDigitou( query.getInteger( "ID_USUARIO_DIGITOU" ) );
				preCadastroCbo.setDtAdmissao( QtData.criaQtData( query.getTimestamp( "DT_ADMISSAO" ) ) );
				preCadastroCbo.setTpEstadoCivil( query.getString( "TP_ESTADO_CIVIL" ) );
				preCadastroCbo.setDsTelefoneContato( query.getString( "DS_TELEFONE_CONTATO" ) );
				preCadastroCbo.setTpDeficiencia( query.getString( "TP_DEFICIENCIA" ) );
				preCadastroCbo.setSnPossuiDeficiencia( query.getString( "SN_POSSUI_DEFICIENCIA" ) );
				preCadastroCbo.setSnTrabalhoEmAltura( query.getString( "SN_TRABALHO_EM_ALTURA" ) );
				preCadastroCbo.setSnTrabalhoConfinado( query.getString( "SN_TRABALHO_CONFINADO" ) );
				preCadastroCbo.setSnFitossanitarios( query.getString( "SN_FITOSSANITARIOS" ) );
				ret = true;
			}
		} finally {
			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}
 }