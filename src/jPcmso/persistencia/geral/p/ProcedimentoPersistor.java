package jPcmso.persistencia.geral.p;

import java.sql.PreparedStatement;

import quatro.persistencia.Persistor;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.util.QtException;

/**
 *
 * Classe persistor para PCMSO.PROCEDIMENTOS
 *
 * @author Samuel Antonio Klein
 * @see Persistor
 * @see Procedimento
 * @see ProcedimentoNavegador
 * @see ProcedimentoLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 20/12/2015 22:02 - importado por Samuel Antonio Klein
 *
 */
 public class ProcedimentoPersistor extends Persistor {

	/** Inst�ncia de Procedimento para manipula��o pelo persistor */
	private Procedimento procedimento;

	/** Construtor gen�rico */
	public ProcedimentoPersistor() {

		this.procedimento = null;
	}

	/** Construtor gen�rico */
	public ProcedimentoPersistor( Conexao cnx ) throws QtSQLException {

		this.procedimento = null;
		super.setConexao( cnx );
	}

	/** Construtor que recebe uma inst�ncia de Procedimento */
	public ProcedimentoPersistor( Procedimento procedimento ) throws QtSQLException {

		if( procedimento == null ) 
			throw new QtSQLException( "Imposs�vel instanciar ProcedimentoPersistor porque est� apontando para um nulo!" );

		this.procedimento = procedimento;
	}

	/** Construtor que recebe uma conex�o e uma inst�ncia de Procedimento */
	public ProcedimentoPersistor( Conexao conexao, Procedimento procedimento ) throws QtSQLException {

		if( procedimento == null ) 
			throw new QtSQLException( "Imposs�vel instanciar ProcedimentoPersistor porque est� apontando para um nulo!" );

		setConexao( conexao );
		this.procedimento = procedimento;
	}

	/**
	 * Construtor que recebe a chave da tabela PCMSO.PROCEDIMENTOS
	 * @param cdProcedimento Codigo do procedimento
	 */
	public ProcedimentoPersistor( String cdProcedimento )
	  throws QtSQLException { 

		busca( cdProcedimento );
	}

	/**
	 * Construtor que recebe uma conex�o e a chave da tabela PCMSO.PROCEDIMENTOS
	 * @param cdProcedimento Codigo do procedimento
	 * @param conexao Conexao com o banco de dados
	 */
	public ProcedimentoPersistor( Conexao conexao, String cdProcedimento )
	  throws QtSQLException { 

		super.setConexao( conexao );
		busca( cdProcedimento );
	}

	/**
	 * M�todo interno para buscar e preencher uma tupla de uma tabela
	 * Utiliza a Inst�ncia de Procedimento para fazer a busca
	 * @param cdProcedimento Codigo do procedimento
	 */
	private void busca( String cdProcedimento ) throws QtSQLException { 

		ProcedimentoLocalizador procedimentoLocalizador = new ProcedimentoLocalizador( getConexao(), cdProcedimento );

		if( procedimentoLocalizador.isEmpty() ) {
			throw new QtSQLException( "Chaves da Tabela Procedimento n�o encontradas - cdProcedimento: " + cdProcedimento );
		}

		procedimento = (Procedimento ) procedimentoLocalizador.getTabelaBasica();
	}

	/**
	 * Permite alimentar o Persistor com um novo registro
	 */
	public void setTabelaBasica( TabelaBasica procedimento ) throws QtException {

		if( procedimento instanceof Procedimento ) {
			this.procedimento = (Procedimento) procedimento;
		} else {
			throw new QtException( "Tabela B�sica fornecida tem que ser uma inst�ncia de Procedimento" );
		}
	}

	/**
	 * Permite acessar o registro corrente do persistor
	 */
	public TabelaBasica getTabelaBasica() throws QtException {

		return procedimento;
	}

	/**
	 * M�todo para inserir tupla na tabela
	 */
	public void insere() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "insert into PCMSO.PROCEDIMENTOS" );
			cmd.append( "( CD_PROCEDIMENTO, DS_PROCEDIMENTO, SN_RENOVACAO_SEMESTRAL )" );
			cmd.append( "values " );
			cmd.append( "( ?, ?, ?)" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			ps.setObject( 1, procedimento.getCdProcedimento() );
			ps.setObject( 2, procedimento.getDsProcedimento() );
			ps.setObject( 3, procedimento.isSnRenovacaoSemestral() ? "S": "N"  );
			
			ps.execute();

		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}

	/**
	 * M�todo para alterar tupla na tabela
	 */
	public void altera() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "update PCMSO.PROCEDIMENTOS" );
			cmd.append( "  set CD_PROCEDIMENTO = ?, " );
			cmd.append( " DS_PROCEDIMENTO = ?, " );
			cmd.append( " SN_RENOVACAO_SEMESTRAL = ?" );
			cmd.append( " where CD_PROCEDIMENTO like ?" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			ps.setObject( 1, procedimento.getCdProcedimento() );
			ps.setObject( 2, procedimento.getDsProcedimento() );
			ps.setObject( 3, procedimento.isSnRenovacaoSemestral() ? "S": "N"  );
			
			ps.setObject( 4, procedimento.getCdProcedimento() );
			
			ps.execute();

		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}

	/**
	 * M�todo para excluir tupla de uma tabela
	 */
	public void exclui() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "delete from PCMSO.PROCEDIMENTOS" );
			cmd.append( " where CD_PROCEDIMENTO like ?" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			ps.setObject( 1, procedimento.getCdProcedimento() );
			
			ps.execute();

		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}
}