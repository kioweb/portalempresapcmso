package jPcmso.persistencia.geral.p;

import quatro.persistencia.Localizador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.QtSQLException;
import quatro.sql.Query;
import quatro.sql.Conexao;
import quatro.sql.PoolDeConexoes;
import quatro.sql.ConexaoParaPoolDeConexoes;

/**
 *
 * Classe localizador para PCMSO.PROCEDIMENTOS
 *
 * @author Samuel Antonio Klein
 * @see Localizador
 * @see Procedimento
 * @see ProcedimentoNavegador
 * @see ProcedimentoPersistor
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 20/12/2015 22:02 - importado por Samuel Antonio Klein
 *
 */
 public class ProcedimentoLocalizador extends Localizador {

	/** Inst�ncia de Procedimento para manipula��o pelo localizador */
	private Procedimento procedimento;

	/** Construtor que recebe uma conex�o e um objeto Procedimento completo */
	public ProcedimentoLocalizador( Conexao conexao, Procedimento procedimento ) throws QtSQLException {

		super.setConexao( conexao );

		this.procedimento = procedimento;
		busca();
	}

	/** Construtor que recebe um objeto Procedimento completo */
	public ProcedimentoLocalizador( Procedimento procedimento ) throws QtSQLException {

		this.procedimento = procedimento;
		busca();
	}

	/** 
	 * Construtor que recebe uma conex�o e a chave da tabela PCMSO.PROCEDIMENTOS
	 * @param cdProcedimento Codigo do procedimento
	 * @param conexao Conexao com o banco de dados
	 */
	public ProcedimentoLocalizador( Conexao conexao, String cdProcedimento ) throws QtSQLException { 

		super.setConexao( conexao ); 
		procedimento = new Procedimento();
		this.procedimento.setCdProcedimento( cdProcedimento );
		
		busca();
	}

	/** 
	 * Construtor que recebe a chave da tabela PCMSO.PROCEDIMENTOS
	 * @param cdProcedimento Codigo do procedimento
	 */
	public ProcedimentoLocalizador( String cdProcedimento ) throws QtSQLException { 

		procedimento = new Procedimento();
		this.procedimento.setCdProcedimento( cdProcedimento );
		
		busca();
	}

	public static boolean existe( Conexao cnx, String cdProcedimento ) throws QtSQLException {

		Query q = new Query( cnx );

		try {
			q.setSQL( "select count(*) from PCMSO.PROCEDIMENTOS" );
			q.addSQL( " where CD_PROCEDIMENTO = :prm1CdProcedimento" );

			q.setParameter( "prm1CdProcedimento", cdProcedimento );
			q.executeQuery();

			return q.getInt( 1 ) > 0;
		} finally {
			q.liberaRecursos();
		}
	}

	public static boolean existe( String cdProcedimento ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return existe( cnx, cdProcedimento );
		} finally {
			cnx.libera();
		}
	}

	public static boolean existe( Conexao cnx, Procedimento procedimento ) throws QtSQLException {

		return existe( cnx, procedimento.getCdProcedimento() );
	}

	public static boolean existe( Procedimento procedimento ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return existe( cnx, procedimento.getCdProcedimento() );
		} finally {
			cnx.libera();
		}
	}

	public static Procedimento buscaProcedimento( Conexao cnx, String cdProcedimento ) throws QtSQLException {

		Query q = new Query( cnx );

		try {
			q.setSQL( "select * from PCMSO.PROCEDIMENTOS" );
			q.addSQL( " where CD_PROCEDIMENTO = :prm1CdProcedimento" );

			q.setParameter( "prm1CdProcedimento", cdProcedimento );
			q.executeQuery();

			if( q.isEmpty() ) {
				return null;
			} else {
				Procedimento procedimento = new Procedimento();
				buscaCampos( procedimento, q );
				return procedimento;
			}
		} finally {
			q.liberaRecursos();
		}
	}

	public static Procedimento buscaProcedimento( String cdProcedimento ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return buscaProcedimento( cnx, cdProcedimento );
		} finally { 
			cnx.libera();
		}
	}

	public static void buscaCampos( Procedimento procedimento, Query query ) throws QtSQLException {

		procedimento.setCdProcedimento( query.getString( "CD_PROCEDIMENTO" ) );
		procedimento.setDsProcedimento( query.getString( "DS_PROCEDIMENTO" ) );
		procedimento.setSnRenovacaoSemestral( query.getString( "SN_RENOVACAO_SEMESTRAL" ) );
	}

	/** M�todo que retorna uma tabela b�sica a que se refere o localizador */
	public TabelaBasica getTabelaBasica() {

		return this.procedimento;
	}

	/** M�todo que retorna a classe Procedimento relacionada ao localizador */
	public Procedimento getProcedimento() {

		return this.procedimento;
	}

	/**
	 * M�todo para buscar e preencher uma tupla de uma tabela
	 * @param tabelaBasica Recebe uma tabela basica com os atributos chave preenchidos
	 */
	public boolean busca( TabelaBasica tabelaBasica ) throws QtSQLException {

		if( !( tabelaBasica instanceof Procedimento ) )
			throw new QtSQLException( "Esperava um objeto Procedimento!" );

		this.procedimento = (Procedimento) tabelaBasica;
		return busca();

	}

	/**
	 * M�todo privado para buscar e preencher uma tupla de uma tabela
	 * Este m�todo busca os dados da inst�ncia de Procedimento atual
	 */
	private boolean busca() throws QtSQLException {

		boolean ret = false;

		super.estabeleceConexao();
		Query query = super.criaNovaQuery();

		try {

			query.setSQL( "select * from PCMSO.PROCEDIMENTOS" );
			query.addSQL( " where CD_PROCEDIMENTO like :prm1CdProcedimento" );

			query.setParameter( "prm1CdProcedimento", procedimento.getCdProcedimento() );
			query.executeQuery();

			if( query.isEmpty() ) {
				super.setEmpty( true );
				procedimento = null;
			} else {
				super.setEmpty( false );
				
				procedimento.setCdProcedimento( query.getString( "CD_PROCEDIMENTO" ) );
				procedimento.setDsProcedimento( query.getString( "DS_PROCEDIMENTO" ) );
				procedimento.setSnRenovacaoSemestral( query.getString( "SN_RENOVACAO_SEMESTRAL" ) );
				ret = true;
			}
		} finally {
			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}
 }