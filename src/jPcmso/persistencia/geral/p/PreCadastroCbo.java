package jPcmso.persistencia.geral.p;

import quatro.persistencia.TabelaBasica;
import quatro.smartDB.IgnoredField;
import quatro.smartDB.PrimaryKey;
import quatro.smartDB.Table;
import quatro.util.QtData;
import quatro.util.QtString;

/**
 *
 * Classe b�sica para PCMSO.PRE_CADASTRO_CBO
 *
 * Tabela de Beneficiarios CBO
 * 
 * @author Lucio Nascimento Teixeira
 * @see PreCadastroCboNavegador
 * @see PreCadastroCboPersistor
 * @see PreCadastroCboLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 23/06/2020 23:58 - importado por Lucio Nascimento Teixeira
 *
 */
 @Table(name="PCMSO.PRE_CADASTRO_CBO")
 public class PreCadastroCbo implements TabelaBasica {

	@IgnoredField
	private static final long serialVersionUID = -846807571500647442L;

	/** SEQUENCIA de BENEFICARIOS (chave) */
	@PrimaryKey
	private Integer idPreCadastro;
	/** codigo da Unimed (char(4)) */
	private String cdUnimed;
	/** Codigo de Beneficiario  (char(12)) */
	private String cdBeneficiarioCartao;
	/** 
	 * tipo de solicita��o (char(1))
	 * 
	 * I - Inclusao
	 * A - Altera��o
	 * E - Exclus�o
	 * N - Nenhuma Solicita��o
	 */
	private String tpSolicitacao;
	/** C�digo do benefici�rio na empresa. (varchar(5)) */
	private String cdNaEmpresa;
	/** Codigo da empresa (char(4)) */
	private String cdEmpresa;
	/** identificador da Lota��o */
	private Integer idLotacao;
	/** identifidor da fun��o */
	private Integer idFuncao;
	/** Codigo da Filial */
	private Integer idFilial;
	/** codigo do setor */
	private Integer idSetor;
	/** codigo do cargo */
	private Integer idCargo;
	/** Codigo do CBO (char(6)) */
	private String cdCbo;
	/** Codigo do RG (char(10)) */
	private String cdRg;
	/** descri��o do org�o de emiss�o (varchar(250)) */
	private String dsOrgaoEmissorRg;
	/** codigo do cnpj (char(12)) */
	private String nrCpf;
	/** Data do nascimento */
	private QtData dtNascimento;
	/** 
	 * tipo de Sexo (char(1))
	 * M - Masculino
	 * F - feminino
	 */
	private String tpSexo;
	/** valor peso */
	private Double vlPeso;
	/** valor altura */
	private Double vlAltura;
	/** descricao da atividade */
	private String dsAtividade;
	/** Nome beneficiario (varchar(250)) */
	private String dsNome;
	/** 
	 * Tipo de BR PDH (char(1))
	 * 
	 * N-N�o aplicavel (NA)
	 * B-Beneficiario Reabilitado (BR)
	 * P-Portador de Deficiencia Habilitado (PDH)
	 */
	private String tpBrPdh;
	/** NR_CTPS (char(7)) */
	private String nrCtps;
	/** NR_SERIE_UF (char(5)) */
	private String nrSerieUf;
	/** NR_PIS (char(11)) */
	private String nrPis;
	/** Descricao do lougradouro (varchar(250)) */
	private String dsLogradouro;
	/** numero do l.ogradouro (varchar(10)) */
	private String dsNumeroLogradouro;
	/** descricao do complemento (varchar(250)) */
	private String dsComplementoLogradouro;
	/** descricao do bairro (varchar(250)) */
	private String dsBairro;
	/** identificador de cidade */
	private Integer idCidade;
	/** codigo do estado (char(2)) */
	private String cdEstado;
	/** data de inclusao */
	private QtData dtInclusao;
	/** data de Exclus�o */
	private QtData dtExclusao;
	/** Data de solicita��o de Excluis�o */
	private QtData dtSolicitacaoExclusao;
	/** Data da demiss�o do Pr� Cadastro CBO */
	private QtData dtDemissao;
	/** Data da solicita��o da movimenta��o */
	private QtData dtSolicitacao;
	/** Data do �ltimo movimento realizado */
	private QtData dtUltimoMovimento;
	/** motivo da solicitacao da exclusao */
	private Integer idMotivoSolicitacaoExclusao;
	/** descricao do nome da mae (varchar(250)) */
	private String dsNomeDaMae;
	/** usuario q digitou */
	private Integer idUsuarioDigitou;
	/** datqa da admissao */
	private QtData dtAdmissao;
	/** 
	 * Tipo de Estado Civil (char(1)) (char(1))
	 *  
	 * S-Solteiro
	 * M-Casado (M-Married)
	 * W-Viuvo (W-Widow)
	 * D-Divorciado
	 * A-Apartado (Separado)
	 * U-Uni�o Est�vel
	 */
	private String tpEstadoCivil;
	/** descricao do telefone de contato (varchar(20)) */
	private String dsTelefoneContato;
	/** 
	 * Tipo de deficiencia do benefici�rio: (char(1))
	 * I-Intelectual
	 * F-Fisica
	 * A-Auditiva
	 * V-Visual
	 */
	private String tpDeficiencia;
	/** Indica se benefici�rio possui deficiencia (char(1)) */
	private boolean snPossuiDeficiencia;
	/** Indica se benefici�rio trabalha em altura. (char(1)) */
	private boolean snTrabalhoEmAltura;
	/** Indica se benefici�rio trabalha em espa�o confinado (char(1)) */
	private boolean snTrabalhoConfinado;
	/** Indica se benefici�rio utiliza defensivos agr�colas (char(1)) */
	private boolean snFitossanitarios;

	/**
	 * Alimenta SEQUENCIA de BENEFICARIOS
	 * @param idPreCadastro SEQUENCIA de BENEFICARIOS
	 */ 
	public void setIdPreCadastro( Integer idPreCadastro ) {
		this.idPreCadastro = idPreCadastro;
	}

	/**
	 * Retorna SEQUENCIA de BENEFICARIOS
	 * @return SEQUENCIA de BENEFICARIOS
	 */
	public Integer getIdPreCadastro() {
		return this.idPreCadastro;
	}

	/**
	 * Alimenta codigo da Unimed
	 * @param cdUnimed codigo da Unimed
	 */ 
	public void setCdUnimed( String cdUnimed ) {
		this.cdUnimed = cdUnimed;
	}

	/**
	 * Retorna codigo da Unimed
	 * @return codigo da Unimed
	 */
	public String getCdUnimed() {
		return this.cdUnimed;
	}

	/**
	 * Alimenta Codigo de Beneficiario 
	 * @param cdBeneficiarioCartao Codigo de Beneficiario 
	 */ 
	public void setCdBeneficiarioCartao( String cdBeneficiarioCartao ) {
		this.cdBeneficiarioCartao = cdBeneficiarioCartao;
	}

	/**
	 * Retorna Codigo de Beneficiario 
	 * @return Codigo de Beneficiario 
	 */
	public String getCdBeneficiarioCartao() {
		return this.cdBeneficiarioCartao;
	}

	/**
	 * Alimenta tipo de solicita��o
	 * @param tpSolicitacao tipo de solicita��o
	 */ 
	public void setTpSolicitacao( String tpSolicitacao ) {
		this.tpSolicitacao = tpSolicitacao;
	}

	/**
	 * Retorna tipo de solicita��o
	 * @return tipo de solicita��o
	 */
	public String getTpSolicitacao() {
		return this.tpSolicitacao;
	}

	/**
	 * Alimenta C�digo do benefici�rio na empresa.
	 * @param cdNaEmpresa C�digo do benefici�rio na empresa.
	 */ 
	public void setCdNaEmpresa( String cdNaEmpresa ) {
		this.cdNaEmpresa = cdNaEmpresa;
	}

	/**
	 * Retorna C�digo do benefici�rio na empresa.
	 * @return C�digo do benefici�rio na empresa.
	 */
	public String getCdNaEmpresa() {
		return this.cdNaEmpresa;
	}

	/**
	 * Alimenta Codigo da empresa
	 * @param cdEmpresa Codigo da empresa
	 */ 
	public void setCdEmpresa( String cdEmpresa ) {
		this.cdEmpresa = cdEmpresa;
	}

	/**
	 * Retorna Codigo da empresa
	 * @return Codigo da empresa
	 */
	public String getCdEmpresa() {
		return this.cdEmpresa;
	}

	/**
	 * Alimenta identificador da Lota��o
	 * @param idLotacao identificador da Lota��o
	 */ 
	public void setIdLotacao( Integer idLotacao ) {
		this.idLotacao = idLotacao;
	}

	/**
	 * Retorna identificador da Lota��o
	 * @return identificador da Lota��o
	 */
	public Integer getIdLotacao() {
		return this.idLotacao;
	}

	/**
	 * Alimenta identifidor da fun��o
	 * @param idFuncao identifidor da fun��o
	 */ 
	public void setIdFuncao( Integer idFuncao ) {
		this.idFuncao = idFuncao;
	}

	/**
	 * Retorna identifidor da fun��o
	 * @return identifidor da fun��o
	 */
	public Integer getIdFuncao() {
		return this.idFuncao;
	}

	/**
	 * Alimenta Codigo da Filial
	 * @param idFilial Codigo da Filial
	 */ 
	public void setIdFilial( Integer idFilial ) {
		this.idFilial = idFilial;
	}

	/**
	 * Retorna Codigo da Filial
	 * @return Codigo da Filial
	 */
	public Integer getIdFilial() {
		return this.idFilial;
	}

	/**
	 * Alimenta codigo do setor
	 * @param idSetor codigo do setor
	 */ 
	public void setIdSetor( Integer idSetor ) {
		this.idSetor = idSetor;
	}

	/**
	 * Retorna codigo do setor
	 * @return codigo do setor
	 */
	public Integer getIdSetor() {
		return this.idSetor;
	}

	/**
	 * Alimenta codigo do cargo
	 * @param idCargo codigo do cargo
	 */ 
	public void setIdCargo( Integer idCargo ) {
		this.idCargo = idCargo;
	}

	/**
	 * Retorna codigo do cargo
	 * @return codigo do cargo
	 */
	public Integer getIdCargo() {
		return this.idCargo;
	}

	/**
	 * Alimenta Codigo do CBO
	 * @param cdCbo Codigo do CBO
	 */ 
	public void setCdCbo( String cdCbo ) {
		this.cdCbo = cdCbo;
	}

	/**
	 * Retorna Codigo do CBO
	 * @return Codigo do CBO
	 */
	public String getCdCbo() {
		return this.cdCbo;
	}

	/**
	 * Alimenta Codigo do RG
	 * @param cdRg Codigo do RG
	 */ 
	public void setCdRg( String cdRg ) {
		this.cdRg = cdRg;
	}

	/**
	 * Retorna Codigo do RG
	 * @return Codigo do RG
	 */
	public String getCdRg() {
		return this.cdRg;
	}

	/**
	 * Alimenta descri��o do org�o de emiss�o
	 * @param dsOrgaoEmissorRg descri��o do org�o de emiss�o
	 */ 
	public void setDsOrgaoEmissorRg( String dsOrgaoEmissorRg ) {
		this.dsOrgaoEmissorRg = dsOrgaoEmissorRg;
	}

	/**
	 * Retorna descri��o do org�o de emiss�o
	 * @return descri��o do org�o de emiss�o
	 */
	public String getDsOrgaoEmissorRg() {
		return this.dsOrgaoEmissorRg;
	}

	/**
	 * Alimenta codigo do cnpj
	 * @param nrCpf codigo do cnpj
	 */ 
	public void setNrCpf( String nrCpf ) {
		this.nrCpf = nrCpf;
	}

	/**
	 * Retorna codigo do cnpj
	 * @return codigo do cnpj
	 */
	public String getNrCpf() {
		return this.nrCpf;
	}

	/**
	 * Alimenta Data do nascimento
	 * @param dtNascimento Data do nascimento
	 */ 
	public void setDtNascimento( QtData dtNascimento ) {
		this.dtNascimento = dtNascimento;
	}

	/**
	 * Retorna Data do nascimento
	 * @return Data do nascimento
	 */
	public QtData getDtNascimento() {
		return this.dtNascimento;
	}

	/**
	 * Alimenta tipo de Sexo
	 * @param tpSexo tipo de Sexo
	 */ 
	public void setTpSexo( String tpSexo ) {
		this.tpSexo = tpSexo;
	}

	/**
	 * Retorna tipo de Sexo
	 * @return tipo de Sexo
	 */
	public String getTpSexo() {
		return this.tpSexo;
	}

	/**
	 * Alimenta valor peso
	 * @param vlPeso valor peso
	 */ 
	public void setVlPeso( Double vlPeso ) {
		this.vlPeso = vlPeso;
	}

	/**
	 * Retorna valor peso
	 * @return valor peso
	 */
	public Double getVlPeso() {
		return this.vlPeso;
	}

	/**
	 * Alimenta valor altura
	 * @param vlAltura valor altura
	 */ 
	public void setVlAltura( Double vlAltura ) {
		this.vlAltura = vlAltura;
	}

	/**
	 * Retorna valor altura
	 * @return valor altura
	 */
	public Double getVlAltura() {
		return this.vlAltura;
	}

	/**
	 * Alimenta descricao da atividade
	 * @param dsAtividade descricao da atividade
	 */ 
	public void setDsAtividade( String dsAtividade ) {
		this.dsAtividade = dsAtividade;
	}

	/**
	 * Retorna descricao da atividade
	 * @return descricao da atividade
	 */
	public String getDsAtividade() {
		return this.dsAtividade;
	}

	/**
	 * Alimenta Nome beneficiario
	 * @param dsNome Nome beneficiario
	 */ 
	public void setDsNome( String dsNome ) {
		this.dsNome = dsNome;
	}

	/**
	 * Retorna Nome beneficiario
	 * @return Nome beneficiario
	 */
	public String getDsNome() {
		return this.dsNome;
	}

	/**
	 * Alimenta Tipo de BR PDH
	 * @param tpBrPdh Tipo de BR PDH
	 */ 
	public void setTpBrPdh( String tpBrPdh ) {
		this.tpBrPdh = tpBrPdh;
	}

	/**
	 * Retorna Tipo de BR PDH
	 * @return Tipo de BR PDH
	 */
	public String getTpBrPdh() {
		return this.tpBrPdh;
	}

	/**
	 * Alimenta NR_CTPS
	 * @param nrCtps NR_CTPS
	 */ 
	public void setNrCtps( String nrCtps ) {
		this.nrCtps = nrCtps;
	}

	/**
	 * Retorna NR_CTPS
	 * @return NR_CTPS
	 */
	public String getNrCtps() {
		return this.nrCtps;
	}

	/**
	 * Alimenta NR_SERIE_UF
	 * @param nrSerieUf NR_SERIE_UF
	 */ 
	public void setNrSerieUf( String nrSerieUf ) {
		this.nrSerieUf = nrSerieUf;
	}

	/**
	 * Retorna NR_SERIE_UF
	 * @return NR_SERIE_UF
	 */
	public String getNrSerieUf() {
		return this.nrSerieUf;
	}

	/**
	 * Alimenta NR_PIS
	 * @param nrPis NR_PIS
	 */ 
	public void setNrPis( String nrPis ) {
		this.nrPis = nrPis;
	}

	/**
	 * Retorna NR_PIS
	 * @return NR_PIS
	 */
	public String getNrPis() {
		return this.nrPis;
	}

	/**
	 * Alimenta Descricao do lougradouro
	 * @param dsLogradouro Descricao do lougradouro
	 */ 
	public void setDsLogradouro( String dsLogradouro ) {
		this.dsLogradouro = dsLogradouro;
	}

	/**
	 * Retorna Descricao do lougradouro
	 * @return Descricao do lougradouro
	 */
	public String getDsLogradouro() {
		return this.dsLogradouro;
	}

	/**
	 * Alimenta numero do l.ogradouro
	 * @param dsNumeroLogradouro numero do l.ogradouro
	 */ 
	public void setDsNumeroLogradouro( String dsNumeroLogradouro ) {
		this.dsNumeroLogradouro = dsNumeroLogradouro;
	}

	/**
	 * Retorna numero do l.ogradouro
	 * @return numero do l.ogradouro
	 */
	public String getDsNumeroLogradouro() {
		return this.dsNumeroLogradouro;
	}

	/**
	 * Alimenta descricao do complemento
	 * @param dsComplementoLogradouro descricao do complemento
	 */ 
	public void setDsComplementoLogradouro( String dsComplementoLogradouro ) {
		this.dsComplementoLogradouro = dsComplementoLogradouro;
	}

	/**
	 * Retorna descricao do complemento
	 * @return descricao do complemento
	 */
	public String getDsComplementoLogradouro() {
		return this.dsComplementoLogradouro;
	}

	/**
	 * Alimenta descricao do bairro
	 * @param dsBairro descricao do bairro
	 */ 
	public void setDsBairro( String dsBairro ) {
		this.dsBairro = dsBairro;
	}

	/**
	 * Retorna descricao do bairro
	 * @return descricao do bairro
	 */
	public String getDsBairro() {
		return this.dsBairro;
	}

	/**
	 * Alimenta identificador de cidade
	 * @param idCidade identificador de cidade
	 */ 
	public void setIdCidade( Integer idCidade ) {
		this.idCidade = idCidade;
	}

	/**
	 * Retorna identificador de cidade
	 * @return identificador de cidade
	 */
	public Integer getIdCidade() {
		return this.idCidade;
	}

	/**
	 * Alimenta codigo do estado
	 * @param cdEstado codigo do estado
	 */ 
	public void setCdEstado( String cdEstado ) {
		this.cdEstado = cdEstado;
	}

	/**
	 * Retorna codigo do estado
	 * @return codigo do estado
	 */
	public String getCdEstado() {
		return this.cdEstado;
	}

	/**
	 * Alimenta data de inclusao
	 * @param dtInclusao data de inclusao
	 */ 
	public void setDtInclusao( QtData dtInclusao ) {
		this.dtInclusao = dtInclusao;
	}

	/**
	 * Retorna data de inclusao
	 * @return data de inclusao
	 */
	public QtData getDtInclusao() {
		return this.dtInclusao;
	}

	/**
	 * Alimenta data de Exclus�o
	 * @param dtExclusao data de Exclus�o
	 */ 
	public void setDtExclusao( QtData dtExclusao ) {
		this.dtExclusao = dtExclusao;
	}

	/**
	 * Retorna data de Exclus�o
	 * @return data de Exclus�o
	 */
	public QtData getDtExclusao() {
		return this.dtExclusao;
	}

	/**
	 * Alimenta Data de solicita��o de Excluis�o
	 * @param dtSolicitacaoExclusao Data de solicita��o de Excluis�o
	 */ 
	public void setDtSolicitacaoExclusao( QtData dtSolicitacaoExclusao ) {
		this.dtSolicitacaoExclusao = dtSolicitacaoExclusao;
	}

	/**
	 * Retorna Data de solicita��o de Excluis�o
	 * @return Data de solicita��o de Excluis�o
	 */
	public QtData getDtSolicitacaoExclusao() {
		return this.dtSolicitacaoExclusao;
	}

	/**
	 * Alimenta Data da demiss�o do Pr� Cadastro CBO
	 * @param dtDemissao Data da demiss�o do Pr� Cadastro CBO
	 */ 
	public void setDtDemissao( QtData dtDemissao ) {
		this.dtDemissao = dtDemissao;
	}

	/**
	 * Retorna Data da demiss�o do Pr� Cadastro CBO
	 * @return Data da demiss�o do Pr� Cadastro CBO
	 */
	public QtData getDtDemissao() {
		return this.dtDemissao;
	}

	/**
	 * Alimenta Data da solicita��o da movimenta��o
	 * @param dtSolicitacao Data da solicita��o da movimenta��o
	 */ 
	public void setDtSolicitacao( QtData dtSolicitacao ) {
		this.dtSolicitacao = dtSolicitacao;
	}

	/**
	 * Retorna Data da solicita��o da movimenta��o
	 * @return Data da solicita��o da movimenta��o
	 */
	public QtData getDtSolicitacao() {
		return this.dtSolicitacao;
	}

	/**
	 * Alimenta Data do �ltimo movimento realizado
	 * @param dtUltimoMovimento Data do �ltimo movimento realizado
	 */ 
	public void setDtUltimoMovimento( QtData dtUltimoMovimento ) {
		this.dtUltimoMovimento = dtUltimoMovimento;
	}

	/**
	 * Retorna Data do �ltimo movimento realizado
	 * @return Data do �ltimo movimento realizado
	 */
	public QtData getDtUltimoMovimento() {
		return this.dtUltimoMovimento;
	}

	/**
	 * Alimenta motivo da solicitacao da exclusao
	 * @param idMotivoSolicitacaoExclusao motivo da solicitacao da exclusao
	 */ 
	public void setIdMotivoSolicitacaoExclusao( Integer idMotivoSolicitacaoExclusao ) {
		this.idMotivoSolicitacaoExclusao = idMotivoSolicitacaoExclusao;
	}

	/**
	 * Retorna motivo da solicitacao da exclusao
	 * @return motivo da solicitacao da exclusao
	 */
	public Integer getIdMotivoSolicitacaoExclusao() {
		return this.idMotivoSolicitacaoExclusao;
	}

	/**
	 * Alimenta descricao do nome da mae
	 * @param dsNomeDaMae descricao do nome da mae
	 */ 
	public void setDsNomeDaMae( String dsNomeDaMae ) {
		this.dsNomeDaMae = dsNomeDaMae;
	}

	/**
	 * Retorna descricao do nome da mae
	 * @return descricao do nome da mae
	 */
	public String getDsNomeDaMae() {
		return this.dsNomeDaMae;
	}

	/**
	 * Alimenta usuario q digitou
	 * @param idUsuarioDigitou usuario q digitou
	 */ 
	public void setIdUsuarioDigitou( Integer idUsuarioDigitou ) {
		this.idUsuarioDigitou = idUsuarioDigitou;
	}

	/**
	 * Retorna usuario q digitou
	 * @return usuario q digitou
	 */
	public Integer getIdUsuarioDigitou() {
		return this.idUsuarioDigitou;
	}

	/**
	 * Alimenta datqa da admissao
	 * @param dtAdmissao datqa da admissao
	 */ 
	public void setDtAdmissao( QtData dtAdmissao ) {
		this.dtAdmissao = dtAdmissao;
	}

	/**
	 * Retorna datqa da admissao
	 * @return datqa da admissao
	 */
	public QtData getDtAdmissao() {
		return this.dtAdmissao;
	}

	/**
	 * Alimenta Tipo de Estado Civil (char(1))
	 * @param tpEstadoCivil Tipo de Estado Civil (char(1))
	 */ 
	public void setTpEstadoCivil( String tpEstadoCivil ) {
		this.tpEstadoCivil = tpEstadoCivil;
	}

	/**
	 * Retorna Tipo de Estado Civil (char(1))
	 * @return Tipo de Estado Civil (char(1))
	 */
	public String getTpEstadoCivil() {
		return this.tpEstadoCivil;
	}

	/**
	 * Alimenta descricao do telefone de contato
	 * @param dsTelefoneContato descricao do telefone de contato
	 */ 
	public void setDsTelefoneContato( String dsTelefoneContato ) {
		this.dsTelefoneContato = dsTelefoneContato;
	}

	/**
	 * Retorna descricao do telefone de contato
	 * @return descricao do telefone de contato
	 */
	public String getDsTelefoneContato() {
		return this.dsTelefoneContato;
	}

	/**
	 * Alimenta Tipo de deficiencia do benefici�rio:
	 * @param tpDeficiencia Tipo de deficiencia do benefici�rio:
	 */ 
	public void setTpDeficiencia( String tpDeficiencia ) {
		this.tpDeficiencia = tpDeficiencia;
	}

	/**
	 * Retorna Tipo de deficiencia do benefici�rio:
	 * @return Tipo de deficiencia do benefici�rio:
	 */
	public String getTpDeficiencia() {
		return this.tpDeficiencia;
	}

	/**
	 * Alimenta Indica se benefici�rio possui deficiencia
	 * @param snPossuiDeficiencia Indica se benefici�rio possui deficiencia
	 */ 
	public void setSnPossuiDeficiencia( String snPossuiDeficiencia ) {
		this.snPossuiDeficiencia = QtString.toBoolean( snPossuiDeficiencia );
	}

	/**
	 * Retorna Indica se benefici�rio possui deficiencia
	 * @return Indica se benefici�rio possui deficiencia
	 */
	public boolean isSnPossuiDeficiencia() {
		return this.snPossuiDeficiencia;
	}

	/**
	 * Alimenta Indica se benefici�rio trabalha em altura.
	 * @param snTrabalhoEmAltura Indica se benefici�rio trabalha em altura.
	 */ 
	public void setSnTrabalhoEmAltura( String snTrabalhoEmAltura ) {
		this.snTrabalhoEmAltura = QtString.toBoolean( snTrabalhoEmAltura );
	}

	/**
	 * Retorna Indica se benefici�rio trabalha em altura.
	 * @return Indica se benefici�rio trabalha em altura.
	 */
	public boolean isSnTrabalhoEmAltura() {
		return this.snTrabalhoEmAltura;
	}

	/**
	 * Alimenta Indica se benefici�rio trabalha em espa�o confinado
	 * @param snTrabalhoConfinado Indica se benefici�rio trabalha em espa�o confinado
	 */ 
	public void setSnTrabalhoConfinado( String snTrabalhoConfinado ) {
		this.snTrabalhoConfinado = QtString.toBoolean( snTrabalhoConfinado );
	}

	/**
	 * Retorna Indica se benefici�rio trabalha em espa�o confinado
	 * @return Indica se benefici�rio trabalha em espa�o confinado
	 */
	public boolean isSnTrabalhoConfinado() {
		return this.snTrabalhoConfinado;
	}

	/**
	 * Alimenta Indica se benefici�rio utiliza defensivos agr�colas
	 * @param snFitossanitarios Indica se benefici�rio utiliza defensivos agr�colas
	 */ 
	public void setSnFitossanitarios( String snFitossanitarios ) {
		this.snFitossanitarios = QtString.toBoolean( snFitossanitarios );
	}

	/**
	 * Retorna Indica se benefici�rio utiliza defensivos agr�colas
	 * @return Indica se benefici�rio utiliza defensivos agr�colas
	 */
	public boolean isSnFitossanitarios() {
		return this.snFitossanitarios;
	}

}