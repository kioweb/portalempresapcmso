package jPcmso.persistencia.geral.p;

import java.sql.PreparedStatement;

import quatro.persistencia.Persistor;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.ConexaoParaPoolDeConexoes;
import quatro.sql.PoolDeConexoes;
import quatro.sql.QtSQLException;
import quatro.sql.Query;
import quatro.util.QtData;
import quatro.util.QtException;

/**
 *
 * Classe persistor para PCMSO.PRE_CADASTRO_CBO
 *
 * @author Lucio Nascimento Teixeira
 * @see Persistor
 * @see PreCadastroCbo
 * @see PreCadastroCboNavegador
 * @see PreCadastroCboLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 23/06/2020 23:58 - importado por Lucio Nascimento Teixeira
 *
 */
 public class PreCadastroCboPersistor extends Persistor {

	/** Inst�ncia de PreCadastroCbo para manipula��o pelo persistor */
	private PreCadastroCbo preCadastroCbo;

	/** Construtor gen�rico */
	public PreCadastroCboPersistor() {

		this.preCadastroCbo = null;
	}

	/** Construtor gen�rico */
	public PreCadastroCboPersistor( Conexao cnx ) throws QtSQLException {

		this.preCadastroCbo = null;
		super.setConexao( cnx );
	}

	/** Construtor que recebe uma inst�ncia de PreCadastroCbo */
	public PreCadastroCboPersistor( PreCadastroCbo preCadastroCbo ) throws QtSQLException {

		if( preCadastroCbo == null ) 
			throw new QtSQLException( "Imposs�vel instanciar PreCadastroCboPersistor porque est� apontando para um nulo!" );

		this.preCadastroCbo = preCadastroCbo;
	}

	/** Construtor que recebe uma conex�o e uma inst�ncia de PreCadastroCbo */
	public PreCadastroCboPersistor( Conexao conexao, PreCadastroCbo preCadastroCbo ) throws QtSQLException {

		if( preCadastroCbo == null ) 
			throw new QtSQLException( "Imposs�vel instanciar PreCadastroCboPersistor porque est� apontando para um nulo!" );

		setConexao( conexao );
		this.preCadastroCbo = preCadastroCbo;
	}

	/**
	 * Construtor que recebe a chave da tabela PCMSO.PRE_CADASTRO_CBO
	 * @param idPreCadastro SEQUENCIA de BENEFICARIOS
	 */
	public PreCadastroCboPersistor( int idPreCadastro )
	  throws QtSQLException { 

		busca( idPreCadastro );
	}

	/**
	 * Construtor que recebe uma conex�o e a chave da tabela PCMSO.PRE_CADASTRO_CBO
	 * @param idPreCadastro SEQUENCIA de BENEFICARIOS
	 * @param conexao Conexao com o banco de dados
	 */
	public PreCadastroCboPersistor( Conexao conexao, int idPreCadastro )
	  throws QtSQLException { 

		super.setConexao( conexao );
		busca( idPreCadastro );
	}

	/**
	 * M�todo interno para buscar e preencher uma tupla de uma tabela
	 * Utiliza a Inst�ncia de PreCadastroCbo para fazer a busca
	 * @param idPreCadastro SEQUENCIA de BENEFICARIOS
	 */
	private void busca( int idPreCadastro ) throws QtSQLException { 

		PreCadastroCboLocalizador preCadastroCboLocalizador = new PreCadastroCboLocalizador( getConexao(), idPreCadastro );

		if( preCadastroCboLocalizador.isEmpty() ) {
			throw new QtSQLException( "Chaves da Tabela PreCadastroCbo n�o encontradas - idPreCadastro: " + idPreCadastro );
		}

		preCadastroCbo = (PreCadastroCbo ) preCadastroCboLocalizador.getTabelaBasica();
	}

	/**
	 * Permite alimentar o Persistor com um novo registro
	 */
	public void setTabelaBasica( TabelaBasica preCadastroCbo ) throws QtException {

		if( preCadastroCbo instanceof PreCadastroCbo ) {
			this.preCadastroCbo = (PreCadastroCbo) preCadastroCbo;
		} else {
			throw new QtException( "Tabela B�sica fornecida tem que ser uma inst�ncia de PreCadastroCbo" );
		}
	}

	/**
	 * Permite acessar o registro corrente do persistor
	 */
	public TabelaBasica getTabelaBasica() throws QtException {

		return preCadastroCbo;
	}

	/**
	 * M�todo para gerar automaticamente uma nova chave
	 */
	public static Integer geraNovoIdPreCadastro( Conexao cnx ) throws QtSQLException {

		Integer result = null;

		Query query = new Query( cnx );

		try{

			query.setSQL( "select max( ID_PRE_CADASTRO ) from PCMSO.PRE_CADASTRO_CBO" );

			query.executeQuery();
			result = new Integer( query.getInt( 1 ) + 1 );
		} catch( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			query.liberaRecursos();
		}
		return result;
	}

	/**
	 * M�todo para gerar automaticamente uma nova chave
	 */
	public static Integer geraNovoIdPreCadastro(  ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return geraNovoIdPreCadastro(  cnx  );
		} finally {
			cnx.libera();
		}
	}


	/**
	 * M�todo para inserir tupla na tabela
	 */
	public void insere() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "insert into PCMSO.PRE_CADASTRO_CBO" );
			cmd.append( "( ID_PRE_CADASTRO, CD_UNIMED, CD_BENEFICIARIO_CARTAO, TP_SOLICITACAO, CD_NA_EMPRESA, CD_EMPRESA, ID_LOTACAO, ID_FUNCAO, ID_FILIAL, ID_SETOR, ID_CARGO, CD_CBO, CD_RG, DS_ORGAO_EMISSOR_RG, NR_CPF, DT_NASCIMENTO, TP_SEXO, VL_PESO, VL_ALTURA, DS_ATIVIDADE, DS_NOME, TP_BR_PDH, NR_CTPS, NR_SERIE_UF, NR_PIS, DS_LOGRADOURO, DS_NUMERO_LOGRADOURO, DS_COMPLEMENTO_LOGRADOURO, DS_BAIRRO, ID_CIDADE, CD_ESTADO, DT_INCLUSAO, DT_EXCLUSAO, DT_SOLICITACAO_EXCLUSAO, DT_DEMISSAO, DT_SOLICITACAO, DT_ULTIMO_MOVIMENTO, ID_MOTIVO_SOLICITACAO_EXCLUSAO, DS_NOME_DA_MAE, ID_USUARIO_DIGITOU, DT_ADMISSAO, TP_ESTADO_CIVIL, DS_TELEFONE_CONTATO, TP_DEFICIENCIA, SN_POSSUI_DEFICIENCIA, SN_TRABALHO_EM_ALTURA, SN_TRABALHO_CONFINADO, SN_FITOSSANITARIOS )" );
			cmd.append( "values " );
			cmd.append( "( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			try {

				ps.setObject( 1, preCadastroCbo.getIdPreCadastro() );
				ps.setObject( 2, preCadastroCbo.getCdUnimed() );
				ps.setObject( 3, preCadastroCbo.getCdBeneficiarioCartao() );
				ps.setObject( 4, preCadastroCbo.getTpSolicitacao() );
				ps.setObject( 5, preCadastroCbo.getCdNaEmpresa() );
				ps.setObject( 6, preCadastroCbo.getCdEmpresa() );
				ps.setObject( 7, preCadastroCbo.getIdLotacao() );
				ps.setObject( 8, preCadastroCbo.getIdFuncao() );
				ps.setObject( 9, preCadastroCbo.getIdFilial() );
				ps.setObject( 10, preCadastroCbo.getIdSetor() );
				ps.setObject( 11, preCadastroCbo.getIdCargo() );
				ps.setObject( 12, preCadastroCbo.getCdCbo() );
				ps.setObject( 13, preCadastroCbo.getCdRg() );
				ps.setObject( 14, preCadastroCbo.getDsOrgaoEmissorRg() );
				ps.setObject( 15, preCadastroCbo.getNrCpf() );
				ps.setObject( 16, QtData.getQtDataAsTimestamp( preCadastroCbo.getDtNascimento() ) );
				ps.setObject( 17, preCadastroCbo.getTpSexo() );
				ps.setObject( 18, preCadastroCbo.getVlPeso() );
				ps.setObject( 19, preCadastroCbo.getVlAltura() );
				ps.setObject( 20, preCadastroCbo.getDsAtividade() );
				ps.setObject( 21, preCadastroCbo.getDsNome() );
				ps.setObject( 22, preCadastroCbo.getTpBrPdh() );
				ps.setObject( 23, preCadastroCbo.getNrCtps() );
				ps.setObject( 24, preCadastroCbo.getNrSerieUf() );
				ps.setObject( 25, preCadastroCbo.getNrPis() );
				ps.setObject( 26, preCadastroCbo.getDsLogradouro() );
				ps.setObject( 27, preCadastroCbo.getDsNumeroLogradouro() );
				ps.setObject( 28, preCadastroCbo.getDsComplementoLogradouro() );
				ps.setObject( 29, preCadastroCbo.getDsBairro() );
				ps.setObject( 30, preCadastroCbo.getIdCidade() );
				ps.setObject( 31, preCadastroCbo.getCdEstado() );
				ps.setObject( 32, QtData.getQtDataAsTimestamp( preCadastroCbo.getDtInclusao() ) );
				ps.setObject( 33, QtData.getQtDataAsTimestamp( preCadastroCbo.getDtExclusao() ) );
				ps.setObject( 34, QtData.getQtDataAsTimestamp( preCadastroCbo.getDtSolicitacaoExclusao() ) );
				ps.setObject( 35, QtData.getQtDataAsTimestamp( preCadastroCbo.getDtDemissao() ) );
				ps.setObject( 36, QtData.getQtDataAsTimestamp( preCadastroCbo.getDtSolicitacao() ) );
				ps.setObject( 37, QtData.getQtDataAsTimestamp( preCadastroCbo.getDtUltimoMovimento() ) );
				ps.setObject( 38, preCadastroCbo.getIdMotivoSolicitacaoExclusao() );
				ps.setObject( 39, preCadastroCbo.getDsNomeDaMae() );
				ps.setObject( 40, preCadastroCbo.getIdUsuarioDigitou() );
				ps.setObject( 41, QtData.getQtDataAsTimestamp( preCadastroCbo.getDtAdmissao() ) );
				ps.setObject( 42, preCadastroCbo.getTpEstadoCivil() );
				ps.setObject( 43, preCadastroCbo.getDsTelefoneContato() );
				ps.setObject( 44, preCadastroCbo.getTpDeficiencia() );
				ps.setObject( 45, preCadastroCbo.isSnPossuiDeficiencia() ? "S": "N"  );
				ps.setObject( 46, preCadastroCbo.isSnTrabalhoEmAltura() ? "S": "N"  );
				ps.setObject( 47, preCadastroCbo.isSnTrabalhoConfinado() ? "S": "N"  );
				ps.setObject( 48, preCadastroCbo.isSnFitossanitarios() ? "S": "N"  );
				
				ps.execute();
			} finally { 
				ps.close();
			}
		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}

	/**
	 * M�todo para alterar tupla na tabela
	 */
	public void altera() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "update PCMSO.PRE_CADASTRO_CBO" );
			cmd.append( "  set ID_PRE_CADASTRO = ?, " );
			cmd.append( " CD_UNIMED = ?, " );
			cmd.append( " CD_BENEFICIARIO_CARTAO = ?, " );
			cmd.append( " TP_SOLICITACAO = ?, " );
			cmd.append( " CD_NA_EMPRESA = ?, " );
			cmd.append( " CD_EMPRESA = ?, " );
			cmd.append( " ID_LOTACAO = ?, " );
			cmd.append( " ID_FUNCAO = ?, " );
			cmd.append( " ID_FILIAL = ?, " );
			cmd.append( " ID_SETOR = ?, " );
			cmd.append( " ID_CARGO = ?, " );
			cmd.append( " CD_CBO = ?, " );
			cmd.append( " CD_RG = ?, " );
			cmd.append( " DS_ORGAO_EMISSOR_RG = ?, " );
			cmd.append( " NR_CPF = ?, " );
			cmd.append( " DT_NASCIMENTO = ?, " );
			cmd.append( " TP_SEXO = ?, " );
			cmd.append( " VL_PESO = ?, " );
			cmd.append( " VL_ALTURA = ?, " );
			cmd.append( " DS_ATIVIDADE = ?, " );
			cmd.append( " DS_NOME = ?, " );
			cmd.append( " TP_BR_PDH = ?, " );
			cmd.append( " NR_CTPS = ?, " );
			cmd.append( " NR_SERIE_UF = ?, " );
			cmd.append( " NR_PIS = ?, " );
			cmd.append( " DS_LOGRADOURO = ?, " );
			cmd.append( " DS_NUMERO_LOGRADOURO = ?, " );
			cmd.append( " DS_COMPLEMENTO_LOGRADOURO = ?, " );
			cmd.append( " DS_BAIRRO = ?, " );
			cmd.append( " ID_CIDADE = ?, " );
			cmd.append( " CD_ESTADO = ?, " );
			cmd.append( " DT_INCLUSAO = ?, " );
			cmd.append( " DT_EXCLUSAO = ?, " );
			cmd.append( " DT_SOLICITACAO_EXCLUSAO = ?, " );
			cmd.append( " DT_DEMISSAO = ?, " );
			cmd.append( " DT_SOLICITACAO = ?, " );
			cmd.append( " DT_ULTIMO_MOVIMENTO = ?, " );
			cmd.append( " ID_MOTIVO_SOLICITACAO_EXCLUSAO = ?, " );
			cmd.append( " DS_NOME_DA_MAE = ?, " );
			cmd.append( " ID_USUARIO_DIGITOU = ?, " );
			cmd.append( " DT_ADMISSAO = ?, " );
			cmd.append( " TP_ESTADO_CIVIL = ?, " );
			cmd.append( " DS_TELEFONE_CONTATO = ?, " );
			cmd.append( " TP_DEFICIENCIA = ?, " );
			cmd.append( " SN_POSSUI_DEFICIENCIA = ?, " );
			cmd.append( " SN_TRABALHO_EM_ALTURA = ?, " );
			cmd.append( " SN_TRABALHO_CONFINADO = ?, " );
			cmd.append( " SN_FITOSSANITARIOS = ?" );
			cmd.append( " where ID_PRE_CADASTRO = ?" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			try {

				ps.setObject( 1, preCadastroCbo.getIdPreCadastro() );
				ps.setObject( 2, preCadastroCbo.getCdUnimed() );
				ps.setObject( 3, preCadastroCbo.getCdBeneficiarioCartao() );
				ps.setObject( 4, preCadastroCbo.getTpSolicitacao() );
				ps.setObject( 5, preCadastroCbo.getCdNaEmpresa() );
				ps.setObject( 6, preCadastroCbo.getCdEmpresa() );
				ps.setObject( 7, preCadastroCbo.getIdLotacao() );
				ps.setObject( 8, preCadastroCbo.getIdFuncao() );
				ps.setObject( 9, preCadastroCbo.getIdFilial() );
				ps.setObject( 10, preCadastroCbo.getIdSetor() );
				ps.setObject( 11, preCadastroCbo.getIdCargo() );
				ps.setObject( 12, preCadastroCbo.getCdCbo() );
				ps.setObject( 13, preCadastroCbo.getCdRg() );
				ps.setObject( 14, preCadastroCbo.getDsOrgaoEmissorRg() );
				ps.setObject( 15, preCadastroCbo.getNrCpf() );
				ps.setObject( 16, QtData.getQtDataAsTimestamp( preCadastroCbo.getDtNascimento() ) );
				ps.setObject( 17, preCadastroCbo.getTpSexo() );
				ps.setObject( 18, preCadastroCbo.getVlPeso() );
				ps.setObject( 19, preCadastroCbo.getVlAltura() );
				ps.setObject( 20, preCadastroCbo.getDsAtividade() );
				ps.setObject( 21, preCadastroCbo.getDsNome() );
				ps.setObject( 22, preCadastroCbo.getTpBrPdh() );
				ps.setObject( 23, preCadastroCbo.getNrCtps() );
				ps.setObject( 24, preCadastroCbo.getNrSerieUf() );
				ps.setObject( 25, preCadastroCbo.getNrPis() );
				ps.setObject( 26, preCadastroCbo.getDsLogradouro() );
				ps.setObject( 27, preCadastroCbo.getDsNumeroLogradouro() );
				ps.setObject( 28, preCadastroCbo.getDsComplementoLogradouro() );
				ps.setObject( 29, preCadastroCbo.getDsBairro() );
				ps.setObject( 30, preCadastroCbo.getIdCidade() );
				ps.setObject( 31, preCadastroCbo.getCdEstado() );
				ps.setObject( 32, QtData.getQtDataAsTimestamp( preCadastroCbo.getDtInclusao() ) );
				ps.setObject( 33, QtData.getQtDataAsTimestamp( preCadastroCbo.getDtExclusao() ) );
				ps.setObject( 34, QtData.getQtDataAsTimestamp( preCadastroCbo.getDtSolicitacaoExclusao() ) );
				ps.setObject( 35, QtData.getQtDataAsTimestamp( preCadastroCbo.getDtDemissao() ) );
				ps.setObject( 36, QtData.getQtDataAsTimestamp( preCadastroCbo.getDtSolicitacao() ) );
				ps.setObject( 37, QtData.getQtDataAsTimestamp( preCadastroCbo.getDtUltimoMovimento() ) );
				ps.setObject( 38, preCadastroCbo.getIdMotivoSolicitacaoExclusao() );
				ps.setObject( 39, preCadastroCbo.getDsNomeDaMae() );
				ps.setObject( 40, preCadastroCbo.getIdUsuarioDigitou() );
				ps.setObject( 41, QtData.getQtDataAsTimestamp( preCadastroCbo.getDtAdmissao() ) );
				ps.setObject( 42, preCadastroCbo.getTpEstadoCivil() );
				ps.setObject( 43, preCadastroCbo.getDsTelefoneContato() );
				ps.setObject( 44, preCadastroCbo.getTpDeficiencia() );
				ps.setObject( 45, preCadastroCbo.isSnPossuiDeficiencia() ? "S": "N"  );
				ps.setObject( 46, preCadastroCbo.isSnTrabalhoEmAltura() ? "S": "N"  );
				ps.setObject( 47, preCadastroCbo.isSnTrabalhoConfinado() ? "S": "N"  );
				ps.setObject( 48, preCadastroCbo.isSnFitossanitarios() ? "S": "N"  );
				ps.setObject( 49, preCadastroCbo.getIdPreCadastro() );
				
				ps.execute();
			} finally {
				ps.close();
			}
		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}

	/**
	 * M�todo para excluir tupla de uma tabela
	 */
	public void exclui() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "delete from PCMSO.PRE_CADASTRO_CBO" );
			cmd.append( " where ID_PRE_CADASTRO = ?" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			try {

				ps.setObject( 1, preCadastroCbo.getIdPreCadastro() );
				
				ps.execute();
			} finally {
				ps.close();
			}
		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}
}