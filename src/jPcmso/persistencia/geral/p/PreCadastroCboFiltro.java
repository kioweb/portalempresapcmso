package jPcmso.persistencia.geral.p;

import quatro.persistencia.FiltroDeNavegador;

/**
 *
 * Classe filtro para PCMSO.PRE_CADASTRO_CBO
 *
 * @author Lucio Nascimento Teixeira
 * @see FiltroDeTabela
 * @see PreCadastroCbo
 * @see PreCadastroCboNavegador
 * @see PreCadastroCboPersistor
 * @see PreCadastroCboLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 23/06/2020 23:58 - importado por Lucio Nascimento Teixeira
 *
 */
 public class PreCadastroCboFiltro extends FiltroDeNavegador {

	/** Determina a ordem de navega��o que ser� passada para o navegador */
	public void setOrdemDeNavegacao( int idOrdem ) {

		super.setOrdemDeNavegacao( idOrdem );
	}
}