package jPcmso.persistencia.geral.p;

import quatro.persistencia.TabelaBasica;
import quatro.smartDB.IgnoredField;
import quatro.smartDB.PrimaryKey;
import quatro.smartDB.Table;
import quatro.util.QtData;
import quatro.util.QtString;

/**
 *
 * Classe b�sica para PCMSO.PERFIL_PROFISSIOGRAFICO
 *
 * Tabela de Beneficiarios CBO
 * 
 * @author Samuel Antonio Klein
 * @see PerfilProfissiograficoNavegador
 * @see PerfilProfissiograficoPersistor
 * @see PerfilProfissiograficoLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:31 - importado por Samuel Antonio Klein
 *
 */
 @Table(name="PCMSO.PERFIL_PROFISSIOGRAFICO")
 public class PerfilProfissiografico implements TabelaBasica {

	@IgnoredField
	private static final long serialVersionUID = -155013161246236849L;

	/** Codigo de Beneficiario  (chave-char(12)) */
	@PrimaryKey
	private String cdBeneficiarioCartao;
	/** sequencia perfil (chave) */
	@PrimaryKey
	private Integer sqPerfil;
	/** Codigo da Empresa (char(4)) */
	private String cdEmpresa;
	/** CNPJ do Domic�lio Tribut�rio/CEI: (char(14)) */
	private String nrCnpjCei;
	/** Nome Empresarial:  (varchar(35)) */
	private String dsNomeEmpresa;
	/** CNAE: (char(10)) */
	private String nrCnae;
	/** Nome do Trabalhador (varchar(30)) */
	private String dsNomeTrabalhador;
	/** 
	 * Tipo de BR PDH (char(1))
	 * 
	 * N-N�o aplicavel (NA)
	 * B-Beneficiario Reabilitado (BR)
	 * P-Portador de Deficiencia Habilitado (PDH)
	 */
	private String tpBrPdh;
	/** N�mero do NIT (PIS) do Representante (char(11)) */
	private String nrNitRespresentante;
	/** Data do Nascimento */
	private QtData dtNascimento;
	/** 
	 * Tipo de sexo (char(1))
	 * 
	 * M - Masculino
	 * F - feminino
	 */
	private String tpSexo;
	/** N�mero da carteira profissional (char(10)) */
	private String nrCtps;
	/**  (char(10)) */
	private String nrSerieUf;
	/**  (char(1)) */
	private String cdEstadoCtps;
	/** Data de Admiss�o */
	private QtData dtAdmissao;
	/** Regime Revezamento (varchar(10)) */
	private String dsRegimeRevezamento;
	/** 1- da hierarquia estabelecida no item 9.3.5.4 da NR-09 do MTE (medidas de prote��o coletiva, medidas de car�ter administrativo ou de organiza��o do trabalho e utiliza��o de EPI, nesta ordem, admitindo-se a utiliza��o de EPI somente em situa��es de inviabilidade t�cnica, insufici�ncia ou interinidade � implementa��o do EPC, ou ainda em car�ter complementar ou emergencial); (char(1)) */
	private boolean snAtendeRequisito1;
	/** 2 - das condi��es de funcionamento do EPI ao longo do tempo, conforme especifica��o t�cnica do fabricante ajustada �s condi��es de campo;  (char(1)) */
	private boolean snAtendeRequisito2;
	/** 3- do prazo de validade, conforme Certificado de Aprova��o do MTE; (char(1)) */
	private boolean snAtendeRequisito3;
	/** 4- da periodicidade de troca definida pelos programas ambientais, devendo esta ser comprovada mediante recibo; (char(1)) */
	private boolean snAtendeRequisito4;
	/** 5- dos meios de higieniza��o. (char(1)) */
	private boolean snAtendeRequisito5;
	/** REPRESENTANTE LEGAL DA EMPRESA: (varchar(40)) */
	private String dsRepresentanteEmpresa;
	/**  */
	private String dsObservacao;
	/**  */
	private QtData dtGeracao;
	/**  */
	private QtData dtUltimaEmissao;

	/**
	 * Alimenta Codigo de Beneficiario 
	 * @param cdBeneficiarioCartao Codigo de Beneficiario 
	 */ 
	public void setCdBeneficiarioCartao( String cdBeneficiarioCartao ) {
		this.cdBeneficiarioCartao = cdBeneficiarioCartao;
	}

	/**
	 * Retorna Codigo de Beneficiario 
	 * @return Codigo de Beneficiario 
	 */
	public String getCdBeneficiarioCartao() {
		return this.cdBeneficiarioCartao;
	}

	/**
	 * Alimenta sequencia perfil
	 * @param sqPerfil sequencia perfil
	 */ 
	public void setSqPerfil( Integer sqPerfil ) {
		this.sqPerfil = sqPerfil;
	}

	/**
	 * Retorna sequencia perfil
	 * @return sequencia perfil
	 */
	public Integer getSqPerfil() {
		return this.sqPerfil;
	}

	/**
	 * Alimenta Codigo da Empresa
	 * @param cdEmpresa Codigo da Empresa
	 */ 
	public void setCdEmpresa( String cdEmpresa ) {
		this.cdEmpresa = cdEmpresa;
	}

	/**
	 * Retorna Codigo da Empresa
	 * @return Codigo da Empresa
	 */
	public String getCdEmpresa() {
		return this.cdEmpresa;
	}

	/**
	 * Alimenta CNPJ do Domic�lio Tribut�rio/CEI:
	 * @param nrCnpjCei CNPJ do Domic�lio Tribut�rio/CEI:
	 */ 
	public void setNrCnpjCei( String nrCnpjCei ) {
		this.nrCnpjCei = nrCnpjCei;
	}

	/**
	 * Retorna CNPJ do Domic�lio Tribut�rio/CEI:
	 * @return CNPJ do Domic�lio Tribut�rio/CEI:
	 */
	public String getNrCnpjCei() {
		return this.nrCnpjCei;
	}

	/**
	 * Alimenta Nome Empresarial: 
	 * @param dsNomeEmpresa Nome Empresarial: 
	 */ 
	public void setDsNomeEmpresa( String dsNomeEmpresa ) {
		this.dsNomeEmpresa = dsNomeEmpresa;
	}

	/**
	 * Retorna Nome Empresarial: 
	 * @return Nome Empresarial: 
	 */
	public String getDsNomeEmpresa() {
		return this.dsNomeEmpresa;
	}

	/**
	 * Alimenta CNAE:
	 * @param nrCnae CNAE:
	 */ 
	public void setNrCnae( String nrCnae ) {
		this.nrCnae = nrCnae;
	}

	/**
	 * Retorna CNAE:
	 * @return CNAE:
	 */
	public String getNrCnae() {
		return this.nrCnae;
	}

	/**
	 * Alimenta Nome do Trabalhador
	 * @param dsNomeTrabalhador Nome do Trabalhador
	 */ 
	public void setDsNomeTrabalhador( String dsNomeTrabalhador ) {
		this.dsNomeTrabalhador = dsNomeTrabalhador;
	}

	/**
	 * Retorna Nome do Trabalhador
	 * @return Nome do Trabalhador
	 */
	public String getDsNomeTrabalhador() {
		return this.dsNomeTrabalhador;
	}

	/**
	 * Alimenta Tipo de BR PDH
	 * @param tpBrPdh Tipo de BR PDH
	 */ 
	public void setTpBrPdh( String tpBrPdh ) {
		this.tpBrPdh = tpBrPdh;
	}

	/**
	 * Retorna Tipo de BR PDH
	 * @return Tipo de BR PDH
	 */
	public String getTpBrPdh() {
		return this.tpBrPdh;
	}

	/**
	 * Alimenta N�mero do NIT (PIS) do Representante
	 * @param nrNitRespresentante N�mero do NIT (PIS) do Representante
	 */ 
	public void setNrNitRespresentante( String nrNitRespresentante ) {
		this.nrNitRespresentante = nrNitRespresentante;
	}

	/**
	 * Retorna N�mero do NIT (PIS) do Representante
	 * @return N�mero do NIT (PIS) do Representante
	 */
	public String getNrNitRespresentante() {
		return this.nrNitRespresentante;
	}

	/**
	 * Alimenta Data do Nascimento
	 * @param dtNascimento Data do Nascimento
	 */ 
	public void setDtNascimento( QtData dtNascimento ) {
		this.dtNascimento = dtNascimento;
	}

	/**
	 * Retorna Data do Nascimento
	 * @return Data do Nascimento
	 */
	public QtData getDtNascimento() {
		return this.dtNascimento;
	}

	/**
	 * Alimenta Tipo de sexo
	 * @param tpSexo Tipo de sexo
	 */ 
	public void setTpSexo( String tpSexo ) {
		this.tpSexo = tpSexo;
	}

	/**
	 * Retorna Tipo de sexo
	 * @return Tipo de sexo
	 */
	public String getTpSexo() {
		return this.tpSexo;
	}

	/**
	 * Alimenta N�mero da carteira profissional
	 * @param nrCtps N�mero da carteira profissional
	 */ 
	public void setNrCtps( String nrCtps ) {
		this.nrCtps = nrCtps;
	}

	/**
	 * Retorna N�mero da carteira profissional
	 * @return N�mero da carteira profissional
	 */
	public String getNrCtps() {
		return this.nrCtps;
	}

	/**
	 * Alimenta 
	 * @param nrSerieUf 
	 */ 
	public void setNrSerieUf( String nrSerieUf ) {
		this.nrSerieUf = nrSerieUf;
	}

	/**
	 * Retorna 
	 * @return 
	 */
	public String getNrSerieUf() {
		return this.nrSerieUf;
	}

	/**
	 * Alimenta 
	 * @param cdEstadoCtps 
	 */ 
	public void setCdEstadoCtps( String cdEstadoCtps ) {
		this.cdEstadoCtps = cdEstadoCtps;
	}

	/**
	 * Retorna 
	 * @return 
	 */
	public String getCdEstadoCtps() {
		return this.cdEstadoCtps;
	}

	/**
	 * Alimenta Data de Admiss�o
	 * @param dtAdmissao Data de Admiss�o
	 */ 
	public void setDtAdmissao( QtData dtAdmissao ) {
		this.dtAdmissao = dtAdmissao;
	}

	/**
	 * Retorna Data de Admiss�o
	 * @return Data de Admiss�o
	 */
	public QtData getDtAdmissao() {
		return this.dtAdmissao;
	}

	/**
	 * Alimenta Regime Revezamento
	 * @param dsRegimeRevezamento Regime Revezamento
	 */ 
	public void setDsRegimeRevezamento( String dsRegimeRevezamento ) {
		this.dsRegimeRevezamento = dsRegimeRevezamento;
	}

	/**
	 * Retorna Regime Revezamento
	 * @return Regime Revezamento
	 */
	public String getDsRegimeRevezamento() {
		return this.dsRegimeRevezamento;
	}

	/**
	 * Alimenta 1- da hierarquia estabelecida no item 9.3.5.4 da NR-09 do MTE (medidas de prote��o coletiva, medidas de car�ter administrativo ou de organiza��o do trabalho e utiliza��o de EPI, nesta ordem, admitindo-se a utiliza��o de EPI somente em situa��es de inviabilidade t�cnica, insufici�ncia ou interinidade � implementa��o do EPC, ou ainda em car�ter complementar ou emergencial);
	 * @param snAtendeRequisito1 1- da hierarquia estabelecida no item 9.3.5.4 da NR-09 do MTE (medidas de prote��o coletiva, medidas de car�ter administrativo ou de organiza��o do trabalho e utiliza��o de EPI, nesta ordem, admitindo-se a utiliza��o de EPI somente em situa��es de inviabilidade t�cnica, insufici�ncia ou interinidade � implementa��o do EPC, ou ainda em car�ter complementar ou emergencial);
	 */ 
	public void setSnAtendeRequisito1( String snAtendeRequisito1 ) {
		this.snAtendeRequisito1 = QtString.toBoolean( snAtendeRequisito1 );
	}

	/**
	 * Retorna 1- da hierarquia estabelecida no item 9.3.5.4 da NR-09 do MTE (medidas de prote��o coletiva, medidas de car�ter administrativo ou de organiza��o do trabalho e utiliza��o de EPI, nesta ordem, admitindo-se a utiliza��o de EPI somente em situa��es de inviabilidade t�cnica, insufici�ncia ou interinidade � implementa��o do EPC, ou ainda em car�ter complementar ou emergencial);
	 * @return 1- da hierarquia estabelecida no item 9.3.5.4 da NR-09 do MTE (medidas de prote��o coletiva, medidas de car�ter administrativo ou de organiza��o do trabalho e utiliza��o de EPI, nesta ordem, admitindo-se a utiliza��o de EPI somente em situa��es de inviabilidade t�cnica, insufici�ncia ou interinidade � implementa��o do EPC, ou ainda em car�ter complementar ou emergencial);
	 */
	public boolean isSnAtendeRequisito1() {
		return this.snAtendeRequisito1;
	}

	/**
	 * Alimenta 2 - das condi��es de funcionamento do EPI ao longo do tempo, conforme especifica��o t�cnica do fabricante ajustada �s condi��es de campo; 
	 * @param snAtendeRequisito2 2 - das condi��es de funcionamento do EPI ao longo do tempo, conforme especifica��o t�cnica do fabricante ajustada �s condi��es de campo; 
	 */ 
	public void setSnAtendeRequisito2( String snAtendeRequisito2 ) {
		this.snAtendeRequisito2 = QtString.toBoolean( snAtendeRequisito2 );
	}

	/**
	 * Retorna 2 - das condi��es de funcionamento do EPI ao longo do tempo, conforme especifica��o t�cnica do fabricante ajustada �s condi��es de campo; 
	 * @return 2 - das condi��es de funcionamento do EPI ao longo do tempo, conforme especifica��o t�cnica do fabricante ajustada �s condi��es de campo; 
	 */
	public boolean isSnAtendeRequisito2() {
		return this.snAtendeRequisito2;
	}

	/**
	 * Alimenta 3- do prazo de validade, conforme Certificado de Aprova��o do MTE;
	 * @param snAtendeRequisito3 3- do prazo de validade, conforme Certificado de Aprova��o do MTE;
	 */ 
	public void setSnAtendeRequisito3( String snAtendeRequisito3 ) {
		this.snAtendeRequisito3 = QtString.toBoolean( snAtendeRequisito3 );
	}

	/**
	 * Retorna 3- do prazo de validade, conforme Certificado de Aprova��o do MTE;
	 * @return 3- do prazo de validade, conforme Certificado de Aprova��o do MTE;
	 */
	public boolean isSnAtendeRequisito3() {
		return this.snAtendeRequisito3;
	}

	/**
	 * Alimenta 4- da periodicidade de troca definida pelos programas ambientais, devendo esta ser comprovada mediante recibo;
	 * @param snAtendeRequisito4 4- da periodicidade de troca definida pelos programas ambientais, devendo esta ser comprovada mediante recibo;
	 */ 
	public void setSnAtendeRequisito4( String snAtendeRequisito4 ) {
		this.snAtendeRequisito4 = QtString.toBoolean( snAtendeRequisito4 );
	}

	/**
	 * Retorna 4- da periodicidade de troca definida pelos programas ambientais, devendo esta ser comprovada mediante recibo;
	 * @return 4- da periodicidade de troca definida pelos programas ambientais, devendo esta ser comprovada mediante recibo;
	 */
	public boolean isSnAtendeRequisito4() {
		return this.snAtendeRequisito4;
	}

	/**
	 * Alimenta 5- dos meios de higieniza��o.
	 * @param snAtendeRequisito5 5- dos meios de higieniza��o.
	 */ 
	public void setSnAtendeRequisito5( String snAtendeRequisito5 ) {
		this.snAtendeRequisito5 = QtString.toBoolean( snAtendeRequisito5 );
	}

	/**
	 * Retorna 5- dos meios de higieniza��o.
	 * @return 5- dos meios de higieniza��o.
	 */
	public boolean isSnAtendeRequisito5() {
		return this.snAtendeRequisito5;
	}

	/**
	 * Alimenta REPRESENTANTE LEGAL DA EMPRESA:
	 * @param dsRepresentanteEmpresa REPRESENTANTE LEGAL DA EMPRESA:
	 */ 
	public void setDsRepresentanteEmpresa( String dsRepresentanteEmpresa ) {
		this.dsRepresentanteEmpresa = dsRepresentanteEmpresa;
	}

	/**
	 * Retorna REPRESENTANTE LEGAL DA EMPRESA:
	 * @return REPRESENTANTE LEGAL DA EMPRESA:
	 */
	public String getDsRepresentanteEmpresa() {
		return this.dsRepresentanteEmpresa;
	}

	/**
	 * Alimenta 
	 * @param dsObservacao 
	 */ 
	public void setDsObservacao( String dsObservacao ) {
		this.dsObservacao = dsObservacao;
	}

	/**
	 * Retorna 
	 * @return 
	 */
	public String getDsObservacao() {
		return this.dsObservacao;
	}

	/**
	 * Alimenta 
	 * @param dtGeracao 
	 */ 
	public void setDtGeracao( QtData dtGeracao ) {
		this.dtGeracao = dtGeracao;
	}

	/**
	 * Retorna 
	 * @return 
	 */
	public QtData getDtGeracao() {
		return this.dtGeracao;
	}

	/**
	 * Alimenta 
	 * @param dtUltimaEmissao 
	 */ 
	public void setDtUltimaEmissao( QtData dtUltimaEmissao ) {
		this.dtUltimaEmissao = dtUltimaEmissao;
	}

	/**
	 * Retorna 
	 * @return 
	 */
	public QtData getDtUltimaEmissao() {
		return this.dtUltimaEmissao;
	}

}