package jPcmso.persistencia.geral.p;

import quatro.persistencia.FiltroDeNavegador;
import quatro.persistencia.Navegador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.sql.Query;
import quatro.util.QtData;

/**
 *
 * Classe navegador para PCMSO.PERFIL_PROFISSIOGRAFICO
 *
 * @author Samuel Antonio Klein
 * @see Navegador
 * @see PerfilProfissiografico
 * @see PerfilProfissiograficoPersistor
 * @see PerfilProfissiograficoLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:31 - importado por Samuel Antonio Klein
 *
 */
 public class PerfilProfissiograficoNavegador extends Navegador {

	
	/** Inst�ncia de PerfilProfissiografico para manipula��o pelo navegador */
	private PerfilProfissiografico perfilProfissiografico;

	/** Construtor b�sico */
	public PerfilProfissiograficoNavegador()
	  throws QtSQLException { 

		perfilProfissiografico = new PerfilProfissiografico();
		primeiro();
	}

	/** Construtor b�sico que recebe uma conex�o */
	public PerfilProfissiograficoNavegador( Conexao conexao )
	  throws QtSQLException { 

		super.setConexao( conexao ); 
		perfilProfissiografico = new PerfilProfissiografico();
		primeiro();
	}

	/**
	 * Construtor que recebe uma inst�ncia de PerfilProfissiograficoFiltro e posiciona
	 * no primeiro registro que satisfaz suas condi��es 
	 */
	public PerfilProfissiograficoNavegador( FiltroDeNavegador filtro ) throws QtSQLException  {
		super.setFiltro( filtro );
		this.perfilProfissiografico = new PerfilProfissiografico();
		primeiro();
	}

	/** Construtor que recebe as chaves da tabela PCMSO.PERFIL_PROFISSIOGRAFICO
	 * @param cdBeneficiarioCartao Codigo de Beneficiario 
	 * @param sqPerfil sequencia perfil
	 */
	public PerfilProfissiograficoNavegador( String cdBeneficiarioCartao, int sqPerfil )
	  throws QtSQLException { 

		perfilProfissiografico = new PerfilProfissiografico();
		this.perfilProfissiografico.setCdBeneficiarioCartao( cdBeneficiarioCartao );
		this.perfilProfissiografico.setSqPerfil( new Integer( sqPerfil ) );
		
		busca();
	}

	/** Construtor que recebe  uma conex�o e as chaves da tabela PCMSO.PERFIL_PROFISSIOGRAFICO
	 * @param cdBeneficiarioCartao Codigo de Beneficiario 
	 * @param sqPerfil sequencia perfil
	 * @param conexao Conexao com o banco de dados
	 */
	public PerfilProfissiograficoNavegador( Conexao conexao, String cdBeneficiarioCartao, int sqPerfil )
	  throws QtSQLException { 

		super.setConexao( conexao ); 
		perfilProfissiografico = new PerfilProfissiografico();
		this.perfilProfissiografico.setCdBeneficiarioCartao( cdBeneficiarioCartao );
		this.perfilProfissiografico.setSqPerfil( new Integer( sqPerfil ) );
		
		busca();
	}

	/** M�todo que retorna uma tabela b�sica a que se refere o navegador */
	public TabelaBasica getTabelaBasica() {

		return this.perfilProfissiografico;
	}

	/** M�todo que retorna a classe PerfilProfissiografico relacionada ao navegador */
	public PerfilProfissiografico getPerfilProfissiografico() {

		return this.perfilProfissiografico;
	}

	 /** M�todo que move para o primeiro registro da tabela */
	 public boolean primeiro() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select CD_BENEFICIARIO_CARTAO, SQ_PERFIL" );
			query.addSQL( "  from PCMSO.PERFIL_PROFISSIOGRAFICO" );
			query.addSQL( super.getCondicaoDeFiltro( FiltroDeNavegador.COM_WHERE ) );
			query.addSQL( "  order by CD_BENEFICIARIO_CARTAO, SQ_PERFIL" );
			query.addSQL( "  limit 1 " );
			
			query.executeQuery();

			
			if( query.isEmpty() ) {

				super.setQueryStates( false, true, true );
			} else {
				this.perfilProfissiografico = new PerfilProfissiografico();

				perfilProfissiografico.setCdBeneficiarioCartao( query.getString( "CD_BENEFICIARIO_CARTAO" ) );
				perfilProfissiografico.setSqPerfil( query.getInteger( "SQ_PERFIL" ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o registro anterior na tabela */
	 public boolean anterior() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select CD_BENEFICIARIO_CARTAO, SQ_PERFIL" );
			query.addSQL( "  from PCMSO.PERFIL_PROFISSIOGRAFICO" );
			query.addSQL( "  where ( ( CD_BENEFICIARIO_CARTAO = :prm1CdBeneficiarioCartao and  SQ_PERFIL < :prm2SqPerfil )" );
			query.addSQL( "  or (  CD_BENEFICIARIO_CARTAO < :prm1CdBeneficiarioCartao ) )" );
			query.addSQL( super.getCondicaoDeFiltro() );
			query.addSQL( "  order by CD_BENEFICIARIO_CARTAO desc, SQ_PERFIL desc" );
			query.addSQL( "  limit 1 " );

			query.setParameter( "prm1CdBeneficiarioCartao", perfilProfissiografico.getCdBeneficiarioCartao() );
			query.setParameter( "prm2SqPerfil", perfilProfissiografico.getSqPerfil() );
		
			query.executeQuery();

			
			if( query.isEmpty() ) {

				super.setFound( false );
				super.setBof( true );
			} else {
				this.perfilProfissiografico = new PerfilProfissiografico();

				perfilProfissiografico.setCdBeneficiarioCartao( query.getString( "CD_BENEFICIARIO_CARTAO" ) );
				perfilProfissiografico.setSqPerfil( query.getInteger( "SQ_PERFIL" ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o proximo registro da tabela */
	 public boolean proximo() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select CD_BENEFICIARIO_CARTAO, SQ_PERFIL" );
			query.addSQL( "  from PCMSO.PERFIL_PROFISSIOGRAFICO" );
			query.addSQL( "  where ( (  CD_BENEFICIARIO_CARTAO = :prm1CdBeneficiarioCartao and  SQ_PERFIL > :prm2SqPerfil )" );
			query.addSQL( "  or (  CD_BENEFICIARIO_CARTAO > :prm1CdBeneficiarioCartao ) )" );
			query.addSQL( super.getCondicaoDeFiltro() );
			query.addSQL( "  order by CD_BENEFICIARIO_CARTAO, SQ_PERFIL" );
			query.addSQL( "  limit 1 " );

			query.setParameter( "prm1CdBeneficiarioCartao", perfilProfissiografico.getCdBeneficiarioCartao() );
			query.setParameter( "prm2SqPerfil", perfilProfissiografico.getSqPerfil() );
		
			query.executeQuery();

			
			if( query.isEmpty() ) {

				super.setFound( false );
				super.setEof( true );
			} else {
				this.perfilProfissiografico = new PerfilProfissiografico();

				perfilProfissiografico.setCdBeneficiarioCartao( query.getString( "CD_BENEFICIARIO_CARTAO" ) );
				perfilProfissiografico.setSqPerfil( query.getInteger( "SQ_PERFIL" ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o �ltimo registro da tabela */
	 public boolean ultimo() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select CD_BENEFICIARIO_CARTAO, SQ_PERFIL" );
			query.addSQL( "  from PCMSO.PERFIL_PROFISSIOGRAFICO" );
			query.addSQL( super.getCondicaoDeFiltro( FiltroDeNavegador.COM_WHERE ) 	);
			query.addSQL( "  order by CD_BENEFICIARIO_CARTAO desc, SQ_PERFIL desc" );
			query.addSQL( "  limit 1 " );

			query.executeQuery();

			
			if( query.isEmpty() ) {

				super.setQueryStates( false, true, true );
			} else {
				this.perfilProfissiografico = new PerfilProfissiografico();

				perfilProfissiografico.setCdBeneficiarioCartao( query.getString( "CD_BENEFICIARIO_CARTAO" ) );
				perfilProfissiografico.setSqPerfil( query.getInteger( "SQ_PERFIL" ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	/**
	 * M�todo para buscar e preencher uma tupla de uma tabela
	 * @param tabelaBasica Recebe uma tabela basica com os atributos chave preenchidos
	 */
	public boolean busca( TabelaBasica tabelaBasica ) throws QtSQLException {

		if( !( tabelaBasica instanceof PerfilProfissiografico ) )
			throw new QtSQLException( "Esperava um objeto PerfilProfissiografico!" );

		this.perfilProfissiografico = (PerfilProfissiografico) tabelaBasica;
		return busca();

	}

	/**
	 * m�todo interno para buscar e preencher uma tupla de uma tabela
	 * Utiliza a Inst�ncia de PerfilProfissiografico da classe para fazer a busca
	 */
	private boolean busca() throws QtSQLException {

		PerfilProfissiograficoLocalizador localizador = new PerfilProfissiograficoLocalizador( getConexao(), this.perfilProfissiografico );

		if( localizador.isEmpty() ) {

			super.setQueryStates( false, true, true );
			return false;

		} else {

			super.setQueryStates( true, false, false );
			return true;
		}
	}
}