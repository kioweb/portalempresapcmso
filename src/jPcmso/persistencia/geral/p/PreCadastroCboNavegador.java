package jPcmso.persistencia.geral.p;

import quatro.persistencia.FiltroDeNavegador;
import quatro.persistencia.Navegador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.sql.Query;
import quatro.util.QtData;

/**
 *
 * Classe navegador para PCMSO.PRE_CADASTRO_CBO
 *
 * @author Lucio Nascimento Teixeira
 * @see Navegador
 * @see PreCadastroCbo
 * @see PreCadastroCboPersistor
 * @see PreCadastroCboLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 23/06/2020 23:58 - importado por Lucio Nascimento Teixeira
 *
 */
 public class PreCadastroCboNavegador extends Navegador {

	
	/** Inst�ncia de PreCadastroCbo para manipula��o pelo navegador */
	private PreCadastroCbo preCadastroCbo;

	/** Construtor b�sico */
	public PreCadastroCboNavegador()
	  throws QtSQLException { 

		preCadastroCbo = new PreCadastroCbo();
		primeiro();
	}

	/** Construtor b�sico que recebe uma conex�o */
	public PreCadastroCboNavegador( Conexao conexao )
	  throws QtSQLException { 

		super.setConexao( conexao ); 
		preCadastroCbo = new PreCadastroCbo();
		primeiro();
	}

	/**
	 * Construtor que recebe uma inst�ncia de PreCadastroCboFiltro e posiciona
	 * no primeiro registro que satisfaz suas condi��es 
	 */
	public PreCadastroCboNavegador( FiltroDeNavegador filtro ) throws QtSQLException  {
		super.setFiltro( filtro );
		this.preCadastroCbo = new PreCadastroCbo();
		primeiro();
	}

	/** Construtor que recebe a chave da tabela PCMSO.PRE_CADASTRO_CBO
	 * @param idPreCadastro SEQUENCIA de BENEFICARIOS
	 */
	public PreCadastroCboNavegador( int idPreCadastro )
	  throws QtSQLException { 

		preCadastroCbo = new PreCadastroCbo();
		this.preCadastroCbo.setIdPreCadastro( new Integer( idPreCadastro ) );
		
		busca();
	}

	/** Construtor que recebe  uma conex�o e a chave da tabela PCMSO.PRE_CADASTRO_CBO
	 * @param idPreCadastro SEQUENCIA de BENEFICARIOS
	 * @param conexao Conexao com o banco de dados
	 */
	public PreCadastroCboNavegador( Conexao conexao, int idPreCadastro )
	  throws QtSQLException { 

		super.setConexao( conexao ); 
		preCadastroCbo = new PreCadastroCbo();
		this.preCadastroCbo.setIdPreCadastro( new Integer( idPreCadastro ) );
		
		busca();
	}

	/** M�todo que retorna uma tabela b�sica a que se refere o navegador */
	public TabelaBasica getTabelaBasica() {

		return this.preCadastroCbo;
	}

	/** M�todo que retorna a classe PreCadastroCbo relacionada ao navegador */
	public PreCadastroCbo getPreCadastroCbo() {

		return this.preCadastroCbo;
	}

	 /** M�todo que move para o primeiro registro da tabela */
	 public boolean primeiro() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select min( ID_PRE_CADASTRO ) from PCMSO.PRE_CADASTRO_CBO" );
			query.addSQL( super.getCondicaoDeFiltro( FiltroDeNavegador.COM_WHERE ) 	);

			query.executeQuery();

			
			if( query.getString( 1 ) == null ) {

				super.setQueryStates( false, true, true );
			} else {
				this.preCadastroCbo = new PreCadastroCbo();

				preCadastroCbo.setIdPreCadastro( query.getInteger( 1 ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o registro anterior na tabela */
	 public boolean anterior() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select max( ID_PRE_CADASTRO ) from PCMSO.PRE_CADASTRO_CBO" );
			query.addSQL( " where ID_PRE_CADASTRO < :prm1IdPreCadastro" );
			query.addSQL( super.getCondicaoDeFiltro() );

			query.setParameter( "prm1IdPreCadastro", preCadastroCbo.getIdPreCadastro() );
			
			query.executeQuery();

			
			if( query.getString( 1 ) == null ) {

				super.setFound( false );
				super.setBof( true );
			} else {
				this.preCadastroCbo = new PreCadastroCbo();

				preCadastroCbo.setIdPreCadastro( query.getInteger( 1 ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o proximo registro da tabela */
	 public boolean proximo() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select min( ID_PRE_CADASTRO ) from PCMSO.PRE_CADASTRO_CBO" );
			query.addSQL( " where ID_PRE_CADASTRO > :prm1IdPreCadastro" );
			query.addSQL( super.getCondicaoDeFiltro() );

			query.setParameter( "prm1IdPreCadastro", preCadastroCbo.getIdPreCadastro() );

			query.executeQuery();

			
			if( query.getString( 1 ) == null ) {

				super.setFound( false );
				super.setEof( true );
			} else {
				this.preCadastroCbo = new PreCadastroCbo();

				preCadastroCbo.setIdPreCadastro( query.getInteger( 1 ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o �ltimo registro da tabela */
	 public boolean ultimo() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select max( ID_PRE_CADASTRO ) from PCMSO.PRE_CADASTRO_CBO" );
			query.addSQL( super.getCondicaoDeFiltro( FiltroDeNavegador.COM_WHERE ) 	);

			query.executeQuery();

			
			if( query.getString( 1 ) == null ) {

				super.setQueryStates( false, true, true );
			} else {
				this.preCadastroCbo = new PreCadastroCbo();

				preCadastroCbo.setIdPreCadastro( query.getInteger( 1 ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	/**
	 * M�todo para buscar e preencher uma tupla de uma tabela
	 * @param tabelaBasica Recebe uma tabela basica com os atributos chave preenchidos
	 */
	public boolean busca( TabelaBasica tabelaBasica ) throws QtSQLException {

		if( !( tabelaBasica instanceof PreCadastroCbo ) )
			throw new QtSQLException( "Esperava um objeto PreCadastroCbo!" );

		this.preCadastroCbo = (PreCadastroCbo) tabelaBasica;
		return busca();

	}

	/**
	 * m�todo interno para buscar e preencher uma tupla de uma tabela
	 * Utiliza a Inst�ncia de PreCadastroCbo da classe para fazer a busca
	 */
	private boolean busca() throws QtSQLException {

		PreCadastroCboLocalizador localizador = new PreCadastroCboLocalizador( getConexao(), this.preCadastroCbo );

		if( localizador.isEmpty() ) {

			super.setQueryStates( false, true, true );
			return false;

		} else {

			super.setQueryStates( true, false, false );
			return true;
		}
	}
}