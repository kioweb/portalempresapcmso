package jPcmso.persistencia.geral.p;

import quatro.persistencia.Localizador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.QtSQLException;
import quatro.sql.Query;
import quatro.sql.Conexao;
import quatro.sql.PoolDeConexoes;
import quatro.sql.ConexaoParaPoolDeConexoes;
import quatro.util.QtData;

/**
 *
 * Classe localizador para PCMSO.PERFIL_PROFISSIOGRAFICO
 *
 * @author Samuel Antonio Klein
 * @see Localizador
 * @see PerfilProfissiografico
 * @see PerfilProfissiograficoNavegador
 * @see PerfilProfissiograficoPersistor
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:31 - importado por Samuel Antonio Klein
 *
 */
 public class PerfilProfissiograficoLocalizador extends Localizador {

	/** Inst�ncia de PerfilProfissiografico para manipula��o pelo localizador */
	private PerfilProfissiografico perfilProfissiografico;

	/** Construtor que recebe uma conex�o e um objeto PerfilProfissiografico completo */
	public PerfilProfissiograficoLocalizador( Conexao conexao, PerfilProfissiografico perfilProfissiografico ) throws QtSQLException {

		super.setConexao( conexao );

		this.perfilProfissiografico = perfilProfissiografico;
		busca();
	}

	/** Construtor que recebe um objeto PerfilProfissiografico completo */
	public PerfilProfissiograficoLocalizador( PerfilProfissiografico perfilProfissiografico ) throws QtSQLException {

		this.perfilProfissiografico = perfilProfissiografico;
		busca();
	}

	/** 
	 * Construtor que recebe uma conex�o e as chaves da tabela PCMSO.PERFIL_PROFISSIOGRAFICO
	 * @param cdBeneficiarioCartao Codigo de Beneficiario 
	 * @param sqPerfil sequencia perfil
	 * @param conexao Conexao com o banco de dados
	 */
	public PerfilProfissiograficoLocalizador( Conexao conexao, String cdBeneficiarioCartao, int sqPerfil ) throws QtSQLException { 

		super.setConexao( conexao ); 
		perfilProfissiografico = new PerfilProfissiografico();
		this.perfilProfissiografico.setCdBeneficiarioCartao( cdBeneficiarioCartao );
		this.perfilProfissiografico.setSqPerfil( new Integer( sqPerfil ) );
		
		busca();
	}

	/** 
	 * Construtor que recebe as chaves da tabela PCMSO.PERFIL_PROFISSIOGRAFICO
	 * @param cdBeneficiarioCartao Codigo de Beneficiario 
	 * @param sqPerfil sequencia perfil
	 */
	public PerfilProfissiograficoLocalizador( String cdBeneficiarioCartao, int sqPerfil ) throws QtSQLException { 

		perfilProfissiografico = new PerfilProfissiografico();
		this.perfilProfissiografico.setCdBeneficiarioCartao( cdBeneficiarioCartao );
		this.perfilProfissiografico.setSqPerfil( new Integer( sqPerfil ) );
		
		busca();
	}

	public static boolean existe( Conexao cnx, String cdBeneficiarioCartao, int sqPerfil ) throws QtSQLException {

		Query q = new Query( cnx );

		try {
			q.setSQL( "select count(*) from PCMSO.PERFIL_PROFISSIOGRAFICO" );
			q.addSQL( " where CD_BENEFICIARIO_CARTAO = :prm1CdBeneficiarioCartao and SQ_PERFIL = :prm2SqPerfil" );

			q.setParameter( "prm1CdBeneficiarioCartao", cdBeneficiarioCartao );
			q.setParameter( "prm2SqPerfil", sqPerfil );
			q.executeQuery();

			return q.getInt( 1 ) > 0;
		} finally {
			q.liberaRecursos();
		}
	}

	public static boolean existe( String cdBeneficiarioCartao, int sqPerfil ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return existe( cnx, cdBeneficiarioCartao, sqPerfil );
		} finally {
			cnx.libera();
		}
	}

	public static boolean existe( Conexao cnx, PerfilProfissiografico perfilProfissiografico ) throws QtSQLException {

		return existe( cnx, perfilProfissiografico.getCdBeneficiarioCartao(), perfilProfissiografico.getSqPerfil() );
	}

	public static boolean existe( PerfilProfissiografico perfilProfissiografico ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return existe( cnx, perfilProfissiografico.getCdBeneficiarioCartao(), perfilProfissiografico.getSqPerfil() );
		} finally {
			cnx.libera();
		}
	}

	public static PerfilProfissiografico buscaPerfilProfissiografico( Conexao cnx, String cdBeneficiarioCartao, int sqPerfil ) throws QtSQLException {

		Query q = new Query( cnx );

		try {
			q.setSQL( "select * from PCMSO.PERFIL_PROFISSIOGRAFICO" );
			q.addSQL( " where CD_BENEFICIARIO_CARTAO = :prm1CdBeneficiarioCartao and SQ_PERFIL = :prm2SqPerfil" );

			q.setParameter( "prm1CdBeneficiarioCartao", cdBeneficiarioCartao );
			q.setParameter( "prm2SqPerfil", sqPerfil );
			q.executeQuery();

			if( q.isEmpty() ) {
				return null;
			} else {
				PerfilProfissiografico perfilProfissiografico = new PerfilProfissiografico();
				buscaCampos( perfilProfissiografico, q );
				return perfilProfissiografico;
			}
		} finally {
			q.liberaRecursos();
		}
	}

	public static PerfilProfissiografico buscaPerfilProfissiografico( String cdBeneficiarioCartao, int sqPerfil ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return buscaPerfilProfissiografico( cnx, cdBeneficiarioCartao, sqPerfil );
		} finally { 
			cnx.libera();
		}
	}

	/** 
	 * Construtor que recebe uma conex�o e as chaves da tabela PCMSO.PERFIL_PROFISSIOGRAFICO
	 * @param cdBeneficiarioCartao Codigo de Beneficiario 
	 * @param sqPerfil sequencia perfil
	 * @param conexao Conexao com o banco de dados
	 */
	public PerfilProfissiograficoLocalizador( Conexao conexao, String cdBeneficiarioCartao, Integer sqPerfil ) throws QtSQLException {

		super.setConexao( conexao ); 
		perfilProfissiografico = new PerfilProfissiografico();
		this.perfilProfissiografico.setCdBeneficiarioCartao( cdBeneficiarioCartao );
		this.perfilProfissiografico.setSqPerfil( sqPerfil );
		
		busca();
	}

	/** 
	 * Construtor que recebe as chaves da tabela PCMSO.PERFIL_PROFISSIOGRAFICO
	 * @param cdBeneficiarioCartao Codigo de Beneficiario 
	 * @param sqPerfil sequencia perfil
	 */
	public PerfilProfissiograficoLocalizador( String cdBeneficiarioCartao, Integer sqPerfil ) throws QtSQLException {

		perfilProfissiografico = new PerfilProfissiografico();
		this.perfilProfissiografico.setCdBeneficiarioCartao( cdBeneficiarioCartao );
		this.perfilProfissiografico.setSqPerfil( sqPerfil );
		
		busca();
	}

	public static void buscaCampos( PerfilProfissiografico perfilProfissiografico, Query query ) throws QtSQLException {

		perfilProfissiografico.setCdBeneficiarioCartao( query.getString( "CD_BENEFICIARIO_CARTAO" ) );
		perfilProfissiografico.setSqPerfil( query.getInteger( "SQ_PERFIL" ) );
		perfilProfissiografico.setCdEmpresa( query.getString( "CD_EMPRESA" ) );
		perfilProfissiografico.setNrCnpjCei( query.getString( "NR_CNPJ_CEI" ) );
		perfilProfissiografico.setDsNomeEmpresa( query.getString( "DS_NOME_EMPRESA" ) );
		perfilProfissiografico.setNrCnae( query.getString( "NR_CNAE" ) );
		perfilProfissiografico.setDsNomeTrabalhador( query.getString( "DS_NOME_TRABALHADOR" ) );
		perfilProfissiografico.setTpBrPdh( query.getString( "TP_BR_PDH" ) );
		perfilProfissiografico.setNrNitRespresentante( query.getString( "NR_NIT_RESPRESENTANTE" ) );
		perfilProfissiografico.setDtNascimento( QtData.criaQtData( query.getTimestamp( "DT_NASCIMENTO" ) ) );
		perfilProfissiografico.setTpSexo( query.getString( "TP_SEXO" ) );
		perfilProfissiografico.setNrCtps( query.getString( "NR_CTPS" ) );
		perfilProfissiografico.setNrSerieUf( query.getString( "NR_SERIE_UF" ) );
		perfilProfissiografico.setCdEstadoCtps( query.getString( "CD_ESTADO_CTPS" ) );
		perfilProfissiografico.setDtAdmissao( QtData.criaQtData( query.getTimestamp( "DT_ADMISSAO" ) ) );
		perfilProfissiografico.setDsRegimeRevezamento( query.getString( "DS_REGIME_REVEZAMENTO" ) );
		perfilProfissiografico.setSnAtendeRequisito1( query.getString( "SN_ATENDE_REQUISITO_1" ) );
		perfilProfissiografico.setSnAtendeRequisito2( query.getString( "SN_ATENDE_REQUISITO_2" ) );
		perfilProfissiografico.setSnAtendeRequisito3( query.getString( "SN_ATENDE_REQUISITO_3" ) );
		perfilProfissiografico.setSnAtendeRequisito4( query.getString( "SN_ATENDE_REQUISITO_4" ) );
		perfilProfissiografico.setSnAtendeRequisito5( query.getString( "SN_ATENDE_REQUISITO_5" ) );
		perfilProfissiografico.setDsRepresentanteEmpresa( query.getString( "DS_REPRESENTANTE_EMPRESA" ) );
		perfilProfissiografico.setDsObservacao( query.getString( "DS_OBSERVACAO" ) );
		perfilProfissiografico.setDtGeracao( QtData.criaQtData( query.getTimestamp( "DT_GERACAO" ) ) );
		perfilProfissiografico.setDtUltimaEmissao( QtData.criaQtData( query.getTimestamp( "DT_ULTIMA_EMISSAO" ) ) );
	}

	/** M�todo que retorna uma tabela b�sica a que se refere o localizador */
	public TabelaBasica getTabelaBasica() {

		return this.perfilProfissiografico;
	}

	/** M�todo que retorna a classe PerfilProfissiografico relacionada ao localizador */
	public PerfilProfissiografico getPerfilProfissiografico() {

		return this.perfilProfissiografico;
	}

	/**
	 * M�todo para buscar e preencher uma tupla de uma tabela
	 * @param tabelaBasica Recebe uma tabela basica com os atributos chave preenchidos
	 */
	public boolean busca( TabelaBasica tabelaBasica ) throws QtSQLException {

		if( !( tabelaBasica instanceof PerfilProfissiografico ) )
			throw new QtSQLException( "Esperava um objeto PerfilProfissiografico!" );

		this.perfilProfissiografico = (PerfilProfissiografico) tabelaBasica;
		return busca();

	}

	/**
	 * M�todo privado para buscar e preencher uma tupla de uma tabela
	 * Este m�todo busca os dados da inst�ncia de PerfilProfissiografico atual
	 */
	private boolean busca() throws QtSQLException {

		boolean ret = false;

		super.estabeleceConexao();
		Query query = super.criaNovaQuery();

		try {

			query.setSQL( "select * from PCMSO.PERFIL_PROFISSIOGRAFICO" );
			query.addSQL( " where CD_BENEFICIARIO_CARTAO like :prm1CdBeneficiarioCartao and SQ_PERFIL = :prm2SqPerfil" );

			query.setParameter( "prm1CdBeneficiarioCartao", perfilProfissiografico.getCdBeneficiarioCartao() );
			query.setParameter( "prm2SqPerfil", perfilProfissiografico.getSqPerfil() );
			query.executeQuery();

			if( query.isEmpty() ) {
				super.setEmpty( true );
				perfilProfissiografico = null;
			} else {
				super.setEmpty( false );
				
				perfilProfissiografico.setCdBeneficiarioCartao( query.getString( "CD_BENEFICIARIO_CARTAO" ) );
				perfilProfissiografico.setSqPerfil( query.getInteger( "SQ_PERFIL" ) );
				perfilProfissiografico.setCdEmpresa( query.getString( "CD_EMPRESA" ) );
				perfilProfissiografico.setNrCnpjCei( query.getString( "NR_CNPJ_CEI" ) );
				perfilProfissiografico.setDsNomeEmpresa( query.getString( "DS_NOME_EMPRESA" ) );
				perfilProfissiografico.setNrCnae( query.getString( "NR_CNAE" ) );
				perfilProfissiografico.setDsNomeTrabalhador( query.getString( "DS_NOME_TRABALHADOR" ) );
				perfilProfissiografico.setTpBrPdh( query.getString( "TP_BR_PDH" ) );
				perfilProfissiografico.setNrNitRespresentante( query.getString( "NR_NIT_RESPRESENTANTE" ) );
				perfilProfissiografico.setDtNascimento( QtData.criaQtData( query.getTimestamp( "DT_NASCIMENTO" ) ) );
				perfilProfissiografico.setTpSexo( query.getString( "TP_SEXO" ) );
				perfilProfissiografico.setNrCtps( query.getString( "NR_CTPS" ) );
				perfilProfissiografico.setNrSerieUf( query.getString( "NR_SERIE_UF" ) );
				perfilProfissiografico.setCdEstadoCtps( query.getString( "CD_ESTADO_CTPS" ) );
				perfilProfissiografico.setDtAdmissao( QtData.criaQtData( query.getTimestamp( "DT_ADMISSAO" ) ) );
				perfilProfissiografico.setDsRegimeRevezamento( query.getString( "DS_REGIME_REVEZAMENTO" ) );
				perfilProfissiografico.setSnAtendeRequisito1( query.getString( "SN_ATENDE_REQUISITO_1" ) );
				perfilProfissiografico.setSnAtendeRequisito2( query.getString( "SN_ATENDE_REQUISITO_2" ) );
				perfilProfissiografico.setSnAtendeRequisito3( query.getString( "SN_ATENDE_REQUISITO_3" ) );
				perfilProfissiografico.setSnAtendeRequisito4( query.getString( "SN_ATENDE_REQUISITO_4" ) );
				perfilProfissiografico.setSnAtendeRequisito5( query.getString( "SN_ATENDE_REQUISITO_5" ) );
				perfilProfissiografico.setDsRepresentanteEmpresa( query.getString( "DS_REPRESENTANTE_EMPRESA" ) );
				perfilProfissiografico.setDsObservacao( query.getString( "DS_OBSERVACAO" ) );
				perfilProfissiografico.setDtGeracao( QtData.criaQtData( query.getTimestamp( "DT_GERACAO" ) ) );
				perfilProfissiografico.setDtUltimaEmissao( QtData.criaQtData( query.getTimestamp( "DT_ULTIMA_EMISSAO" ) ) );
				ret = true;
			}
		} finally {
			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}
 }