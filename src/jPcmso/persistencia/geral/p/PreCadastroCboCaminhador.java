package jPcmso.persistencia.geral.p;

import quatro.persistencia.Caminhador;
import quatro.persistencia.FiltroDeNavegador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.util.QtData;

 /**
 *
 * Classe Caminhador para PreCadastroCbo
 *
 * @author Lucio Nascimento Teixeira
 * @see Caminhador
 * @see FiltroDeNavegador
 * @see PreCadastroCboNavegador
 * @see PreCadastroCboPersistor
 * @see PreCadastroCboLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 23/06/2020 23:58 - importado por Lucio Nascimento Teixeira
 *
 */
 public class PreCadastroCboCaminhador extends Caminhador {

	public static final int POR_ORDEM_DEFAULT = -1;
	
	private PreCadastroCbo preCadastroCbo;

	public PreCadastroCboCaminhador( FiltroDeNavegador filtro ) {

		setFiltro( filtro );
		inicializaVisao();
	}

	public PreCadastroCboCaminhador( Conexao conexao, FiltroDeNavegador filtro ) throws QtSQLException {

		super.setConexao( conexao );
		setFiltro( filtro );
		inicializaVisao();
	}

	private void inicializaVisao() {

		setNmVisao( "PCMSO.PRE_CADASTRO_CBO" );
	}

	protected void carregaCampos() throws QtSQLException {

		preCadastroCbo = new PreCadastroCbo();

		if( isRecordAvailable() ) {
			preCadastroCbo.setIdPreCadastro( query.getInteger( "ID_PRE_CADASTRO" ) );
			preCadastroCbo.setCdUnimed( query.getString( "CD_UNIMED" ) );
			preCadastroCbo.setCdBeneficiarioCartao( query.getString( "CD_BENEFICIARIO_CARTAO" ) );
			preCadastroCbo.setTpSolicitacao( query.getString( "TP_SOLICITACAO" ) );
			preCadastroCbo.setCdNaEmpresa( query.getString( "CD_NA_EMPRESA" ) );
			preCadastroCbo.setCdEmpresa( query.getString( "CD_EMPRESA" ) );
			preCadastroCbo.setIdLotacao( query.getInteger( "ID_LOTACAO" ) );
			preCadastroCbo.setIdFuncao( query.getInteger( "ID_FUNCAO" ) );
			preCadastroCbo.setIdFilial( query.getInteger( "ID_FILIAL" ) );
			preCadastroCbo.setIdSetor( query.getInteger( "ID_SETOR" ) );
			preCadastroCbo.setIdCargo( query.getInteger( "ID_CARGO" ) );
			preCadastroCbo.setCdCbo( query.getString( "CD_CBO" ) );
			preCadastroCbo.setCdRg( query.getString( "CD_RG" ) );
			preCadastroCbo.setDsOrgaoEmissorRg( query.getString( "DS_ORGAO_EMISSOR_RG" ) );
			preCadastroCbo.setNrCpf( query.getString( "NR_CPF" ) );
			preCadastroCbo.setDtNascimento(  QtData.criaQtData( query.getTimestamp( "DT_NASCIMENTO" ) ) );
			preCadastroCbo.setTpSexo( query.getString( "TP_SEXO" ) );
			preCadastroCbo.setVlPeso( query.getDoubleObject( "VL_PESO" ) );
			preCadastroCbo.setVlAltura( query.getDoubleObject( "VL_ALTURA" ) );
			preCadastroCbo.setDsAtividade( query.getString( "DS_ATIVIDADE" ) );
			preCadastroCbo.setDsNome( query.getString( "DS_NOME" ) );
			preCadastroCbo.setTpBrPdh( query.getString( "TP_BR_PDH" ) );
			preCadastroCbo.setNrCtps( query.getString( "NR_CTPS" ) );
			preCadastroCbo.setNrSerieUf( query.getString( "NR_SERIE_UF" ) );
			preCadastroCbo.setNrPis( query.getString( "NR_PIS" ) );
			preCadastroCbo.setDsLogradouro( query.getString( "DS_LOGRADOURO" ) );
			preCadastroCbo.setDsNumeroLogradouro( query.getString( "DS_NUMERO_LOGRADOURO" ) );
			preCadastroCbo.setDsComplementoLogradouro( query.getString( "DS_COMPLEMENTO_LOGRADOURO" ) );
			preCadastroCbo.setDsBairro( query.getString( "DS_BAIRRO" ) );
			preCadastroCbo.setIdCidade( query.getInteger( "ID_CIDADE" ) );
			preCadastroCbo.setCdEstado( query.getString( "CD_ESTADO" ) );
			preCadastroCbo.setDtInclusao(  QtData.criaQtData( query.getTimestamp( "DT_INCLUSAO" ) ) );
			preCadastroCbo.setDtExclusao(  QtData.criaQtData( query.getTimestamp( "DT_EXCLUSAO" ) ) );
			preCadastroCbo.setDtSolicitacaoExclusao(  QtData.criaQtData( query.getTimestamp( "DT_SOLICITACAO_EXCLUSAO" ) ) );
			preCadastroCbo.setDtDemissao(  QtData.criaQtData( query.getTimestamp( "DT_DEMISSAO" ) ) );
			preCadastroCbo.setDtSolicitacao(  QtData.criaQtData( query.getTimestamp( "DT_SOLICITACAO" ) ) );
			preCadastroCbo.setDtUltimoMovimento(  QtData.criaQtData( query.getTimestamp( "DT_ULTIMO_MOVIMENTO" ) ) );
			preCadastroCbo.setIdMotivoSolicitacaoExclusao( query.getInteger( "ID_MOTIVO_SOLICITACAO_EXCLUSAO" ) );
			preCadastroCbo.setDsNomeDaMae( query.getString( "DS_NOME_DA_MAE" ) );
			preCadastroCbo.setIdUsuarioDigitou( query.getInteger( "ID_USUARIO_DIGITOU" ) );
			preCadastroCbo.setDtAdmissao(  QtData.criaQtData( query.getTimestamp( "DT_ADMISSAO" ) ) );
			preCadastroCbo.setTpEstadoCivil( query.getString( "TP_ESTADO_CIVIL" ) );
			preCadastroCbo.setDsTelefoneContato( query.getString( "DS_TELEFONE_CONTATO" ) );
			preCadastroCbo.setTpDeficiencia( query.getString( "TP_DEFICIENCIA" ) );
			preCadastroCbo.setSnPossuiDeficiencia( query.getString( "SN_POSSUI_DEFICIENCIA" ) );
			preCadastroCbo.setSnTrabalhoEmAltura( query.getString( "SN_TRABALHO_EM_ALTURA" ) );
			preCadastroCbo.setSnTrabalhoConfinado( query.getString( "SN_TRABALHO_CONFINADO" ) );
			preCadastroCbo.setSnFitossanitarios( query.getString( "SN_FITOSSANITARIOS" ) );
		}
	}

	/** M�todo que retorna uma tabela b�sica a que se refere o caminhador */
	public TabelaBasica getTabelaBasica() {

		return this.preCadastroCbo;
	}

	/** M�todo que retorna a classe PreCadastroCbo relacionada ao caminhador */
	public PreCadastroCbo getPreCadastroCbo() {

		return this.preCadastroCbo;
	}

	public void ativaVisao() throws QtSQLException {

		if(  super.getOrdemDeNavegacao() == PreCadastroCboCaminhador.POR_ORDEM_DEFAULT ) {
			super.ativaCaminhador( "order by ID_PRE_CADASTRO" );
	
		} else {
			throw new QtSQLException( "PreCadastroCboCaminhador - Ordem de Navega��o n�o definida!" );
		}
	}

	public String getCamposIndice( int qualIndice ) {
		return getDsIndice( qualIndice );
	}

	public String getCamposChave() {
		return getDsChave();
	}

	/**
	 * M�todo est�tico que devolve os campos que comp�e a chave da tabela;
	 * @return campos que comp�e a chave.
	 */
	public static String getDsChave() {
		return "ID_PRE_CADASTRO";
	}

	/**
	 * M�todo est�tico que devolve os campos que fazem parte de uma dada ordem
	 * de navega��o.
	 * @param qualIndice indica a ordem de navega��o desejada.
	 * @return campos que comp�e a ordem de navega��o.
	 */
	public static String getDsIndice( int qualIndice ) {

		switch( qualIndice ) {

			case POR_ORDEM_DEFAULT: return "ID_PRE_CADASTRO";
		}
		return null;
	}
}