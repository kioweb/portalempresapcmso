package jPcmso.persistencia.geral.p;

import quatro.persistencia.FiltroDeNavegador;

/**
 *
 * Classe filtro para PCMSO.PROCEDIMENTOS
 *
 * @author Samuel Antonio Klein
 * @see FiltroDeTabela
 * @see Procedimento
 * @see ProcedimentoNavegador
 * @see ProcedimentoPersistor
 * @see ProcedimentoLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 20/12/2015 22:02 - importado por Samuel Antonio Klein
 *
 */
 public class ProcedimentoFiltro extends FiltroDeNavegador {

	/** Determina a ordem de navega��o que ser� passada para o navegador */
	public void setOrdemDeNavegacao( int idOrdem ) {

		super.setOrdemDeNavegacao( idOrdem );
	}
}