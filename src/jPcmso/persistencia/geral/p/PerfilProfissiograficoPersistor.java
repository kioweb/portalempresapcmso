package jPcmso.persistencia.geral.p;

import java.sql.PreparedStatement;

import quatro.persistencia.Persistor;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.util.QtException;
import quatro.util.QtData;

/**
 *
 * Classe persistor para PCMSO.PERFIL_PROFISSIOGRAFICO
 *
 * @author Samuel Antonio Klein
 * @see Persistor
 * @see PerfilProfissiografico
 * @see PerfilProfissiograficoNavegador
 * @see PerfilProfissiograficoLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:31 - importado por Samuel Antonio Klein
 *
 */
 public class PerfilProfissiograficoPersistor extends Persistor {

	/** Inst�ncia de PerfilProfissiografico para manipula��o pelo persistor */
	private PerfilProfissiografico perfilProfissiografico;

	/** Construtor gen�rico */
	public PerfilProfissiograficoPersistor() {

		this.perfilProfissiografico = null;
	}

	/** Construtor gen�rico */
	public PerfilProfissiograficoPersistor( Conexao cnx ) throws QtSQLException {

		this.perfilProfissiografico = null;
		super.setConexao( cnx );
	}

	/** Construtor que recebe uma inst�ncia de PerfilProfissiografico */
	public PerfilProfissiograficoPersistor( PerfilProfissiografico perfilProfissiografico ) throws QtSQLException {

		if( perfilProfissiografico == null ) 
			throw new QtSQLException( "Imposs�vel instanciar PerfilProfissiograficoPersistor porque est� apontando para um nulo!" );

		this.perfilProfissiografico = perfilProfissiografico;
	}

	/** Construtor que recebe uma conex�o e uma inst�ncia de PerfilProfissiografico */
	public PerfilProfissiograficoPersistor( Conexao conexao, PerfilProfissiografico perfilProfissiografico ) throws QtSQLException {

		if( perfilProfissiografico == null ) 
			throw new QtSQLException( "Imposs�vel instanciar PerfilProfissiograficoPersistor porque est� apontando para um nulo!" );

		setConexao( conexao );
		this.perfilProfissiografico = perfilProfissiografico;
	}

	/**
	 * Construtor que recebe as chaves da tabela PCMSO.PERFIL_PROFISSIOGRAFICO
	 * @param cdBeneficiarioCartao Codigo de Beneficiario 
	 * @param sqPerfil sequencia perfil
	 */
	public PerfilProfissiograficoPersistor( String cdBeneficiarioCartao, int sqPerfil )
	  throws QtSQLException { 

		busca( cdBeneficiarioCartao, sqPerfil );
	}

	/**
	 * Construtor que recebe uma conex�o e as chaves da tabela PCMSO.PERFIL_PROFISSIOGRAFICO
	 * @param cdBeneficiarioCartao Codigo de Beneficiario 
	 * @param sqPerfil sequencia perfil
	 * @param conexao Conexao com o banco de dados
	 */
	public PerfilProfissiograficoPersistor( Conexao conexao, String cdBeneficiarioCartao, int sqPerfil )
	  throws QtSQLException { 

		super.setConexao( conexao );
		busca( cdBeneficiarioCartao, sqPerfil );
	}

	/**
	 * M�todo interno para buscar e preencher uma tupla de uma tabela
	 * Utiliza a Inst�ncia de PerfilProfissiografico para fazer a busca
	 * @param cdBeneficiarioCartao Codigo de Beneficiario 
	 * @param sqPerfil sequencia perfil
	 */
	private void busca( String cdBeneficiarioCartao, int sqPerfil ) throws QtSQLException { 

		PerfilProfissiograficoLocalizador perfilProfissiograficoLocalizador = new PerfilProfissiograficoLocalizador( getConexao(), cdBeneficiarioCartao, sqPerfil );

		if( perfilProfissiograficoLocalizador.isEmpty() ) {
			throw new QtSQLException( "Chaves da Tabela PerfilProfissiografico n�o encontradas - cdBeneficiarioCartao: " + cdBeneficiarioCartao + " / sqPerfil: " + sqPerfil );
		}

		perfilProfissiografico = (PerfilProfissiografico ) perfilProfissiograficoLocalizador.getTabelaBasica();
	}

	/**
	 * Permite alimentar o Persistor com um novo registro
	 */
	public void setTabelaBasica( TabelaBasica perfilProfissiografico ) throws QtException {

		if( perfilProfissiografico instanceof PerfilProfissiografico ) {
			this.perfilProfissiografico = (PerfilProfissiografico) perfilProfissiografico;
		} else {
			throw new QtException( "Tabela B�sica fornecida tem que ser uma inst�ncia de PerfilProfissiografico" );
		}
	}

	/**
	 * Permite acessar o registro corrente do persistor
	 */
	public TabelaBasica getTabelaBasica() throws QtException {

		return perfilProfissiografico;
	}

	/**
	 * M�todo para inserir tupla na tabela
	 */
	public void insere() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "insert into PCMSO.PERFIL_PROFISSIOGRAFICO" );
			cmd.append( "( CD_BENEFICIARIO_CARTAO, SQ_PERFIL, CD_EMPRESA, NR_CNPJ_CEI, DS_NOME_EMPRESA, NR_CNAE, DS_NOME_TRABALHADOR, TP_BR_PDH, NR_NIT_RESPRESENTANTE, DT_NASCIMENTO, TP_SEXO, NR_CTPS, NR_SERIE_UF, CD_ESTADO_CTPS, DT_ADMISSAO, DS_REGIME_REVEZAMENTO, SN_ATENDE_REQUISITO_1, SN_ATENDE_REQUISITO_2, SN_ATENDE_REQUISITO_3, SN_ATENDE_REQUISITO_4, SN_ATENDE_REQUISITO_5, DS_REPRESENTANTE_EMPRESA, DS_OBSERVACAO, DT_GERACAO, DT_ULTIMA_EMISSAO )" );
			cmd.append( "values " );
			cmd.append( "( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			ps.setObject( 1, perfilProfissiografico.getCdBeneficiarioCartao() );
			ps.setObject( 2, perfilProfissiografico.getSqPerfil() );
			ps.setObject( 3, perfilProfissiografico.getCdEmpresa() );
			ps.setObject( 4, perfilProfissiografico.getNrCnpjCei() );
			ps.setObject( 5, perfilProfissiografico.getDsNomeEmpresa() );
			ps.setObject( 6, perfilProfissiografico.getNrCnae() );
			ps.setObject( 7, perfilProfissiografico.getDsNomeTrabalhador() );
			ps.setObject( 8, perfilProfissiografico.getTpBrPdh() );
			ps.setObject( 9, perfilProfissiografico.getNrNitRespresentante() );
			ps.setObject( 10, QtData.getQtDataAsTimestamp( perfilProfissiografico.getDtNascimento() ) );
			ps.setObject( 11, perfilProfissiografico.getTpSexo() );
			ps.setObject( 12, perfilProfissiografico.getNrCtps() );
			ps.setObject( 13, perfilProfissiografico.getNrSerieUf() );
			ps.setObject( 14, perfilProfissiografico.getCdEstadoCtps() );
			ps.setObject( 15, QtData.getQtDataAsTimestamp( perfilProfissiografico.getDtAdmissao() ) );
			ps.setObject( 16, perfilProfissiografico.getDsRegimeRevezamento() );
			ps.setObject( 17, perfilProfissiografico.isSnAtendeRequisito1() ? "S": "N"  );
			ps.setObject( 18, perfilProfissiografico.isSnAtendeRequisito2() ? "S": "N"  );
			ps.setObject( 19, perfilProfissiografico.isSnAtendeRequisito3() ? "S": "N"  );
			ps.setObject( 20, perfilProfissiografico.isSnAtendeRequisito4() ? "S": "N"  );
			ps.setObject( 21, perfilProfissiografico.isSnAtendeRequisito5() ? "S": "N"  );
			ps.setObject( 22, perfilProfissiografico.getDsRepresentanteEmpresa() );
			ps.setObject( 23, perfilProfissiografico.getDsObservacao() );
			ps.setObject( 24, QtData.getQtDataAsTimestamp( perfilProfissiografico.getDtGeracao() ) );
			ps.setObject( 25, QtData.getQtDataAsTimestamp( perfilProfissiografico.getDtUltimaEmissao() ) );
			
			ps.execute();

		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}

	/**
	 * M�todo para alterar tupla na tabela
	 */
	public void altera() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "update PCMSO.PERFIL_PROFISSIOGRAFICO" );
			cmd.append( "  set CD_BENEFICIARIO_CARTAO = ?, " );
			cmd.append( " SQ_PERFIL = ?, " );
			cmd.append( " CD_EMPRESA = ?, " );
			cmd.append( " NR_CNPJ_CEI = ?, " );
			cmd.append( " DS_NOME_EMPRESA = ?, " );
			cmd.append( " NR_CNAE = ?, " );
			cmd.append( " DS_NOME_TRABALHADOR = ?, " );
			cmd.append( " TP_BR_PDH = ?, " );
			cmd.append( " NR_NIT_RESPRESENTANTE = ?, " );
			cmd.append( " DT_NASCIMENTO = ?, " );
			cmd.append( " TP_SEXO = ?, " );
			cmd.append( " NR_CTPS = ?, " );
			cmd.append( " NR_SERIE_UF = ?, " );
			cmd.append( " CD_ESTADO_CTPS = ?, " );
			cmd.append( " DT_ADMISSAO = ?, " );
			cmd.append( " DS_REGIME_REVEZAMENTO = ?, " );
			cmd.append( " SN_ATENDE_REQUISITO_1 = ?, " );
			cmd.append( " SN_ATENDE_REQUISITO_2 = ?, " );
			cmd.append( " SN_ATENDE_REQUISITO_3 = ?, " );
			cmd.append( " SN_ATENDE_REQUISITO_4 = ?, " );
			cmd.append( " SN_ATENDE_REQUISITO_5 = ?, " );
			cmd.append( " DS_REPRESENTANTE_EMPRESA = ?, " );
			cmd.append( " DS_OBSERVACAO = ?, " );
			cmd.append( " DT_GERACAO = ?, " );
			cmd.append( " DT_ULTIMA_EMISSAO = ?" );
			cmd.append( " where CD_BENEFICIARIO_CARTAO like ? and SQ_PERFIL = ?" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			ps.setObject( 1, perfilProfissiografico.getCdBeneficiarioCartao() );
			ps.setObject( 2, perfilProfissiografico.getSqPerfil() );
			ps.setObject( 3, perfilProfissiografico.getCdEmpresa() );
			ps.setObject( 4, perfilProfissiografico.getNrCnpjCei() );
			ps.setObject( 5, perfilProfissiografico.getDsNomeEmpresa() );
			ps.setObject( 6, perfilProfissiografico.getNrCnae() );
			ps.setObject( 7, perfilProfissiografico.getDsNomeTrabalhador() );
			ps.setObject( 8, perfilProfissiografico.getTpBrPdh() );
			ps.setObject( 9, perfilProfissiografico.getNrNitRespresentante() );
			ps.setObject( 10, QtData.getQtDataAsTimestamp( perfilProfissiografico.getDtNascimento() ) );
			ps.setObject( 11, perfilProfissiografico.getTpSexo() );
			ps.setObject( 12, perfilProfissiografico.getNrCtps() );
			ps.setObject( 13, perfilProfissiografico.getNrSerieUf() );
			ps.setObject( 14, perfilProfissiografico.getCdEstadoCtps() );
			ps.setObject( 15, QtData.getQtDataAsTimestamp( perfilProfissiografico.getDtAdmissao() ) );
			ps.setObject( 16, perfilProfissiografico.getDsRegimeRevezamento() );
			ps.setObject( 17, perfilProfissiografico.isSnAtendeRequisito1() ? "S": "N"  );
			ps.setObject( 18, perfilProfissiografico.isSnAtendeRequisito2() ? "S": "N"  );
			ps.setObject( 19, perfilProfissiografico.isSnAtendeRequisito3() ? "S": "N"  );
			ps.setObject( 20, perfilProfissiografico.isSnAtendeRequisito4() ? "S": "N"  );
			ps.setObject( 21, perfilProfissiografico.isSnAtendeRequisito5() ? "S": "N"  );
			ps.setObject( 22, perfilProfissiografico.getDsRepresentanteEmpresa() );
			ps.setObject( 23, perfilProfissiografico.getDsObservacao() );
			ps.setObject( 24, QtData.getQtDataAsTimestamp( perfilProfissiografico.getDtGeracao() ) );
			ps.setObject( 25, QtData.getQtDataAsTimestamp( perfilProfissiografico.getDtUltimaEmissao() ) );
			
			ps.setObject( 26, perfilProfissiografico.getCdBeneficiarioCartao() );
			ps.setObject( 27, perfilProfissiografico.getSqPerfil() );
			
			ps.execute();

		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}

	/**
	 * M�todo para excluir tupla de uma tabela
	 */
	public void exclui() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "delete from PCMSO.PERFIL_PROFISSIOGRAFICO" );
			cmd.append( " where CD_BENEFICIARIO_CARTAO like ? and SQ_PERFIL = ?" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			ps.setObject( 1, perfilProfissiografico.getCdBeneficiarioCartao() );
			ps.setObject( 2, perfilProfissiografico.getSqPerfil() );
			
			ps.execute();

		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}
}