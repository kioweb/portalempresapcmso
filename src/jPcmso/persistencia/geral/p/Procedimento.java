package jPcmso.persistencia.geral.p;

import quatro.persistencia.TabelaBasica;
import quatro.smartDB.IgnoredField;
import quatro.smartDB.PrimaryKey;
import quatro.smartDB.Table;
import quatro.util.QtString;

/**
 *
 * Classe b�sica para PCMSO.PROCEDIMENTOS
 *
 * Tabela de procedimentos
 * 
 * @author Samuel Antonio Klein
 * @see ProcedimentoNavegador
 * @see ProcedimentoPersistor
 * @see ProcedimentoLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 20/12/2015 22:02 - importado por Samuel Antonio Klein
 *
 */
 @Table(name="PCMSO.PROCEDIMENTOS")
 public class Procedimento implements TabelaBasica {

	@IgnoredField
	private static final long serialVersionUID = -672401972003778510L;

	/** Codigo do procedimento (chave-char(8)) */
	@PrimaryKey
	private String cdProcedimento;
	/** Descri��o do procedimento (varchar(250)) */
	private String dsProcedimento;
	/** Indica se o procedimento tem renovacao semestral (char(1)) */
	private boolean snRenovacaoSemestral;

	/**
	 * Alimenta Codigo do procedimento
	 * @param cdProcedimento Codigo do procedimento
	 */ 
	public void setCdProcedimento( String cdProcedimento ) {
		this.cdProcedimento = cdProcedimento;
	}

	/**
	 * Retorna Codigo do procedimento
	 * @return Codigo do procedimento
	 */
	public String getCdProcedimento() {
		return this.cdProcedimento;
	}

	/**
	 * Alimenta Descri��o do procedimento
	 * @param dsProcedimento Descri��o do procedimento
	 */ 
	public void setDsProcedimento( String dsProcedimento ) {
		this.dsProcedimento = dsProcedimento;
	}

	/**
	 * Retorna Descri��o do procedimento
	 * @return Descri��o do procedimento
	 */
	public String getDsProcedimento() {
		return this.dsProcedimento;
	}

	/**
	 * Alimenta Indica se o procedimento tem renovacao semestral
	 * @param snRenovacaoSemestral Indica se o procedimento tem renovacao semestral
	 */ 
	public void setSnRenovacaoSemestral( String snRenovacaoSemestral ) {
		this.snRenovacaoSemestral = QtString.toBoolean( snRenovacaoSemestral );
	}

	/**
	 * Retorna Indica se o procedimento tem renovacao semestral
	 * @return Indica se o procedimento tem renovacao semestral
	 */
	public boolean isSnRenovacaoSemestral() {
		return this.snRenovacaoSemestral;
	}

}