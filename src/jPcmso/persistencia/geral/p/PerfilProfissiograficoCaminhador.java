package jPcmso.persistencia.geral.p;

import quatro.persistencia.Caminhador;
import quatro.persistencia.FiltroDeNavegador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.util.QtData;

 /**
 *
 * Classe Caminhador para PerfilProfissiografico
 *
 * @author Samuel Antonio Klein
 * @see Caminhador
 * @see FiltroDeNavegador
 * @see PerfilProfissiograficoNavegador
 * @see PerfilProfissiograficoPersistor
 * @see PerfilProfissiograficoLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:31 - importado por Samuel Antonio Klein
 *
 */
 public class PerfilProfissiograficoCaminhador extends Caminhador {

	public static final int POR_ORDEM_DEFAULT = -1;
	
	private PerfilProfissiografico perfilProfissiografico;

	public PerfilProfissiograficoCaminhador( FiltroDeNavegador filtro ) {

		setFiltro( filtro );
		inicializaVisao();
	}

	public PerfilProfissiograficoCaminhador( Conexao conexao, FiltroDeNavegador filtro ) throws QtSQLException {

		super.setConexao( conexao );
		setFiltro( filtro );
		inicializaVisao();
	}

	private void inicializaVisao() {

		setNmVisao( "PCMSO.PERFIL_PROFISSIOGRAFICO" );
	}

	protected void carregaCampos() throws QtSQLException {

		perfilProfissiografico = new PerfilProfissiografico();

		if( isRecordAvailable() ) {
			perfilProfissiografico.setCdBeneficiarioCartao( query.getString( "CD_BENEFICIARIO_CARTAO" ) );
			perfilProfissiografico.setSqPerfil( query.getInteger( "SQ_PERFIL" ) );
			perfilProfissiografico.setCdEmpresa( query.getString( "CD_EMPRESA" ) );
			perfilProfissiografico.setNrCnpjCei( query.getString( "NR_CNPJ_CEI" ) );
			perfilProfissiografico.setDsNomeEmpresa( query.getString( "DS_NOME_EMPRESA" ) );
			perfilProfissiografico.setNrCnae( query.getString( "NR_CNAE" ) );
			perfilProfissiografico.setDsNomeTrabalhador( query.getString( "DS_NOME_TRABALHADOR" ) );
			perfilProfissiografico.setTpBrPdh( query.getString( "TP_BR_PDH" ) );
			perfilProfissiografico.setNrNitRespresentante( query.getString( "NR_NIT_RESPRESENTANTE" ) );
			perfilProfissiografico.setDtNascimento(  QtData.criaQtData( query.getTimestamp( "DT_NASCIMENTO" ) ) );
			perfilProfissiografico.setTpSexo( query.getString( "TP_SEXO" ) );
			perfilProfissiografico.setNrCtps( query.getString( "NR_CTPS" ) );
			perfilProfissiografico.setNrSerieUf( query.getString( "NR_SERIE_UF" ) );
			perfilProfissiografico.setCdEstadoCtps( query.getString( "CD_ESTADO_CTPS" ) );
			perfilProfissiografico.setDtAdmissao(  QtData.criaQtData( query.getTimestamp( "DT_ADMISSAO" ) ) );
			perfilProfissiografico.setDsRegimeRevezamento( query.getString( "DS_REGIME_REVEZAMENTO" ) );
			perfilProfissiografico.setSnAtendeRequisito1( query.getString( "SN_ATENDE_REQUISITO_1" ) );
			perfilProfissiografico.setSnAtendeRequisito2( query.getString( "SN_ATENDE_REQUISITO_2" ) );
			perfilProfissiografico.setSnAtendeRequisito3( query.getString( "SN_ATENDE_REQUISITO_3" ) );
			perfilProfissiografico.setSnAtendeRequisito4( query.getString( "SN_ATENDE_REQUISITO_4" ) );
			perfilProfissiografico.setSnAtendeRequisito5( query.getString( "SN_ATENDE_REQUISITO_5" ) );
			perfilProfissiografico.setDsRepresentanteEmpresa( query.getString( "DS_REPRESENTANTE_EMPRESA" ) );
			perfilProfissiografico.setDsObservacao( query.getString( "DS_OBSERVACAO" ) );
			perfilProfissiografico.setDtGeracao(  QtData.criaQtData( query.getTimestamp( "DT_GERACAO" ) ) );
			perfilProfissiografico.setDtUltimaEmissao(  QtData.criaQtData( query.getTimestamp( "DT_ULTIMA_EMISSAO" ) ) );
		}
	}

	/** M�todo que retorna uma tabela b�sica a que se refere o caminhador */
	public TabelaBasica getTabelaBasica() {

		return this.perfilProfissiografico;
	}

	/** M�todo que retorna a classe PerfilProfissiografico relacionada ao caminhador */
	public PerfilProfissiografico getPerfilProfissiografico() {

		return this.perfilProfissiografico;
	}

	public void ativaVisao() throws QtSQLException {

		if(  super.getOrdemDeNavegacao() == PerfilProfissiograficoCaminhador.POR_ORDEM_DEFAULT ) {
			super.ativaCaminhador( "order by CD_BENEFICIARIO_CARTAO, SQ_PERFIL" );
	
		} else {
			throw new QtSQLException( "PerfilProfissiograficoCaminhador - Ordem de Navega��o n�o definida!" );
		}
	}

	public String getCamposIndice( int qualIndice ) {
		return getDsIndice( qualIndice );
	}

	public String getCamposChave() {
		return getDsChave();
	}

	/**
	 * M�todo est�tico que devolve os campos que comp�e a chave da tabela;
	 * @return campos que comp�e a chave.
	 */
	public static String getDsChave() {
		return "CD_BENEFICIARIO_CARTAO, SQ_PERFIL";
	}

	/**
	 * M�todo est�tico que devolve os campos que fazem parte de uma dada ordem
	 * de navega��o.
	 * @param qualIndice indica a ordem de navega��o desejada.
	 * @return campos que comp�e a ordem de navega��o.
	 */
	public static String getDsIndice( int qualIndice ) {

		switch( qualIndice ) {

			case POR_ORDEM_DEFAULT: return "CD_BENEFICIARIO_CARTAO, SQ_PERFIL";
		}
		return null;
	}
}