package jPcmso.persistencia.geral.p;

import quatro.persistencia.FiltroDeNavegador;

/**
 *
 * Classe filtro para PCMSO.PERFIL_PROFISSIOGRAFICO
 *
 * @author Samuel Antonio Klein
 * @see FiltroDeTabela
 * @see PerfilProfissiografico
 * @see PerfilProfissiograficoNavegador
 * @see PerfilProfissiograficoPersistor
 * @see PerfilProfissiograficoLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:31 - importado por Samuel Antonio Klein
 *
 */
 public class PerfilProfissiograficoFiltro extends FiltroDeNavegador {

	/** Determina a ordem de navega��o que ser� passada para o navegador */
	public void setOrdemDeNavegacao( int idOrdem ) {

		super.setOrdemDeNavegacao( idOrdem );
	}
}