package jPcmso.persistencia.geral.p;

import quatro.persistencia.Caminhador;
import quatro.persistencia.FiltroDeNavegador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;

 /**
 *
 * Classe Caminhador para Procedimento
 *
 * @author Samuel Antonio Klein
 * @see Caminhador
 * @see FiltroDeNavegador
 * @see ProcedimentoNavegador
 * @see ProcedimentoPersistor
 * @see ProcedimentoLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 20/12/2015 22:02 - importado por Samuel Antonio Klein
 *
 */
 public class ProcedimentoCaminhador extends Caminhador {

	public static final int POR_ORDEM_DEFAULT = -1;
	
	private Procedimento procedimento;

	public ProcedimentoCaminhador( FiltroDeNavegador filtro ) {

		setFiltro( filtro );
		inicializaVisao();
	}

	public ProcedimentoCaminhador( Conexao conexao, FiltroDeNavegador filtro ) throws QtSQLException {

		super.setConexao( conexao );
		setFiltro( filtro );
		inicializaVisao();
	}

	private void inicializaVisao() {

		setNmVisao( "PCMSO.PROCEDIMENTOS" );
	}

	protected void carregaCampos() throws QtSQLException {

		procedimento = new Procedimento();

		if( isRecordAvailable() ) {
			procedimento.setCdProcedimento( query.getString( "CD_PROCEDIMENTO" ) );
			procedimento.setDsProcedimento( query.getString( "DS_PROCEDIMENTO" ) );
			procedimento.setSnRenovacaoSemestral( query.getString( "SN_RENOVACAO_SEMESTRAL" ) );
		}
	}

	/** M�todo que retorna uma tabela b�sica a que se refere o caminhador */
	public TabelaBasica getTabelaBasica() {

		return this.procedimento;
	}

	/** M�todo que retorna a classe Procedimento relacionada ao caminhador */
	public Procedimento getProcedimento() {

		return this.procedimento;
	}

	public void ativaVisao() throws QtSQLException {

		if(  super.getOrdemDeNavegacao() == ProcedimentoCaminhador.POR_ORDEM_DEFAULT ) {
			super.ativaCaminhador( "order by CD_PROCEDIMENTO" );
	
		} else {
			throw new QtSQLException( "ProcedimentoCaminhador - Ordem de Navega��o n�o definida!" );
		}
	}

	public String getCamposIndice( int qualIndice ) {
		return getDsIndice( qualIndice );
	}

	public String getCamposChave() {
		return getDsChave();
	}

	/**
	 * M�todo est�tico que devolve os campos que comp�e a chave da tabela;
	 * @return campos que comp�e a chave.
	 */
	public static String getDsChave() {
		return "CD_PROCEDIMENTO";
	}

	/**
	 * M�todo est�tico que devolve os campos que fazem parte de uma dada ordem
	 * de navega��o.
	 * @param qualIndice indica a ordem de navega��o desejada.
	 * @return campos que comp�e a ordem de navega��o.
	 */
	public static String getDsIndice( int qualIndice ) {

		switch( qualIndice ) {

			case POR_ORDEM_DEFAULT: return "CD_PROCEDIMENTO";
		}
		return null;
	}
}