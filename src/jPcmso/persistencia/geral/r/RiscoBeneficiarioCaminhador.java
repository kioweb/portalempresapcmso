package jPcmso.persistencia.geral.r;

import quatro.persistencia.Caminhador;
import quatro.persistencia.FiltroDeNavegador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.util.QtData;

 /**
 *
 * Classe Caminhador para RiscoBeneficiario
 *
 * @author Samuel Antonio Klein
 * @see Caminhador
 * @see FiltroDeNavegador
 * @see RiscoBeneficiarioNavegador
 * @see RiscoBeneficiarioPersistor
 * @see RiscoBeneficiarioLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 public class RiscoBeneficiarioCaminhador extends Caminhador {

	public static final int POR_ORDEM_DEFAULT = -1;
	
	private RiscoBeneficiario riscoBeneficiario;

	public RiscoBeneficiarioCaminhador( FiltroDeNavegador filtro ) {

		setFiltro( filtro );
		inicializaVisao();
	}

	public RiscoBeneficiarioCaminhador( Conexao conexao, FiltroDeNavegador filtro ) throws QtSQLException {

		super.setConexao( conexao );
		setFiltro( filtro );
		inicializaVisao();
	}

	private void inicializaVisao() {

		setNmVisao( "PCMSO.RISCOS_BENEFICIARIO" );
	}

	protected void carregaCampos() throws QtSQLException {

		riscoBeneficiario = new RiscoBeneficiario();

		if( isRecordAvailable() ) {
			riscoBeneficiario.setCdBeneficiarioCartao( query.getString( "CD_BENEFICIARIO_CARTAO" ) );
			riscoBeneficiario.setIdRisco( query.getInteger( "ID_RISCO" ) );
			riscoBeneficiario.setDtInclusao(  QtData.criaQtData( query.getTimestamp( "DT_INCLUSAO" ) ) );
			riscoBeneficiario.setDtExclusao(  QtData.criaQtData( query.getTimestamp( "DT_EXCLUSAO" ) ) );
		}
	}

	/** M�todo que retorna uma tabela b�sica a que se refere o caminhador */
	public TabelaBasica getTabelaBasica() {

		return this.riscoBeneficiario;
	}

	/** M�todo que retorna a classe RiscoBeneficiario relacionada ao caminhador */
	public RiscoBeneficiario getRiscoBeneficiario() {

		return this.riscoBeneficiario;
	}

	public void ativaVisao() throws QtSQLException {

		if(  super.getOrdemDeNavegacao() == RiscoBeneficiarioCaminhador.POR_ORDEM_DEFAULT ) {
			super.ativaCaminhador( "order by CD_BENEFICIARIO_CARTAO, ID_RISCO" );
	
		} else {
			throw new QtSQLException( "RiscoBeneficiarioCaminhador - Ordem de Navega��o n�o definida!" );
		}
	}

	public String getCamposIndice( int qualIndice ) {
		return getDsIndice( qualIndice );
	}

	public String getCamposChave() {
		return getDsChave();
	}

	/**
	 * M�todo est�tico que devolve os campos que comp�e a chave da tabela;
	 * @return campos que comp�e a chave.
	 */
	public static String getDsChave() {
		return "CD_BENEFICIARIO_CARTAO, ID_RISCO";
	}

	/**
	 * M�todo est�tico que devolve os campos que fazem parte de uma dada ordem
	 * de navega��o.
	 * @param qualIndice indica a ordem de navega��o desejada.
	 * @return campos que comp�e a ordem de navega��o.
	 */
	public static String getDsIndice( int qualIndice ) {

		switch( qualIndice ) {

			case POR_ORDEM_DEFAULT: return "CD_BENEFICIARIO_CARTAO, ID_RISCO";
		}
		return null;
	}
}