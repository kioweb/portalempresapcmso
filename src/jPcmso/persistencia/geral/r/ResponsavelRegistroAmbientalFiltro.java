package jPcmso.persistencia.geral.r;

import quatro.persistencia.FiltroDeNavegador;

/**
 *
 * Classe filtro para PCMSO.RESPONSAVEL_REGISTRO_AMBIENTAL
 *
 * @author Samuel Antonio Klein
 * @see FiltroDeTabela
 * @see ResponsavelRegistroAmbiental
 * @see ResponsavelRegistroAmbientalNavegador
 * @see ResponsavelRegistroAmbientalPersistor
 * @see ResponsavelRegistroAmbientalLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:31 - importado por Samuel Antonio Klein
 *
 */
 public class ResponsavelRegistroAmbientalFiltro extends FiltroDeNavegador {

	/** Determina a ordem de navega��o que ser� passada para o navegador */
	public void setOrdemDeNavegacao( int idOrdem ) {

		super.setOrdemDeNavegacao( idOrdem );
	}
}