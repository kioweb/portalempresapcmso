package jPcmso.persistencia.geral.r;

import java.sql.PreparedStatement;

import quatro.persistencia.Persistor;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.util.QtException;
import quatro.util.QtData;

/**
 *
 * Classe persistor para PCMSO.RESPONSAVEL_REGISTRO_AMBIENTAL
 *
 * @author Samuel Antonio Klein
 * @see Persistor
 * @see ResponsavelRegistroAmbiental
 * @see ResponsavelRegistroAmbientalNavegador
 * @see ResponsavelRegistroAmbientalLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:31 - importado por Samuel Antonio Klein
 *
 */
 public class ResponsavelRegistroAmbientalPersistor extends Persistor {

	/** Inst�ncia de ResponsavelRegistroAmbiental para manipula��o pelo persistor */
	private ResponsavelRegistroAmbiental responsavelRegistroAmbiental;

	/** Construtor gen�rico */
	public ResponsavelRegistroAmbientalPersistor() {

		this.responsavelRegistroAmbiental = null;
	}

	/** Construtor gen�rico */
	public ResponsavelRegistroAmbientalPersistor( Conexao cnx ) throws QtSQLException {

		this.responsavelRegistroAmbiental = null;
		super.setConexao( cnx );
	}

	/** Construtor que recebe uma inst�ncia de ResponsavelRegistroAmbiental */
	public ResponsavelRegistroAmbientalPersistor( ResponsavelRegistroAmbiental responsavelRegistroAmbiental ) throws QtSQLException {

		if( responsavelRegistroAmbiental == null ) 
			throw new QtSQLException( "Imposs�vel instanciar ResponsavelRegistroAmbientalPersistor porque est� apontando para um nulo!" );

		this.responsavelRegistroAmbiental = responsavelRegistroAmbiental;
	}

	/** Construtor que recebe uma conex�o e uma inst�ncia de ResponsavelRegistroAmbiental */
	public ResponsavelRegistroAmbientalPersistor( Conexao conexao, ResponsavelRegistroAmbiental responsavelRegistroAmbiental ) throws QtSQLException {

		if( responsavelRegistroAmbiental == null ) 
			throw new QtSQLException( "Imposs�vel instanciar ResponsavelRegistroAmbientalPersistor porque est� apontando para um nulo!" );

		setConexao( conexao );
		this.responsavelRegistroAmbiental = responsavelRegistroAmbiental;
	}

	/**
	 * Construtor que recebe as chaves da tabela PCMSO.RESPONSAVEL_REGISTRO_AMBIENTAL
	 * @param cdBeneficiarioCartao Codigo de Beneficiario 
	 * @param sqPerfil sequencia perfil
	 * @param sqResponsavel sequencia Responsavel
	 */
	public ResponsavelRegistroAmbientalPersistor( String cdBeneficiarioCartao, int sqPerfil, int sqResponsavel )
	  throws QtSQLException { 

		busca( cdBeneficiarioCartao, sqPerfil, sqResponsavel );
	}

	/**
	 * Construtor que recebe uma conex�o e as chaves da tabela PCMSO.RESPONSAVEL_REGISTRO_AMBIENTAL
	 * @param cdBeneficiarioCartao Codigo de Beneficiario 
	 * @param sqPerfil sequencia perfil
	 * @param sqResponsavel sequencia Responsavel
	 * @param conexao Conexao com o banco de dados
	 */
	public ResponsavelRegistroAmbientalPersistor( Conexao conexao, String cdBeneficiarioCartao, int sqPerfil, int sqResponsavel )
	  throws QtSQLException { 

		super.setConexao( conexao );
		busca( cdBeneficiarioCartao, sqPerfil, sqResponsavel );
	}

	/**
	 * M�todo interno para buscar e preencher uma tupla de uma tabela
	 * Utiliza a Inst�ncia de ResponsavelRegistroAmbiental para fazer a busca
	 * @param cdBeneficiarioCartao Codigo de Beneficiario 
	 * @param sqPerfil sequencia perfil
	 * @param sqResponsavel sequencia Responsavel
	 */
	private void busca( String cdBeneficiarioCartao, int sqPerfil, int sqResponsavel ) throws QtSQLException { 

		ResponsavelRegistroAmbientalLocalizador responsavelRegistroAmbientalLocalizador = new ResponsavelRegistroAmbientalLocalizador( getConexao(), cdBeneficiarioCartao, sqPerfil, sqResponsavel );

		if( responsavelRegistroAmbientalLocalizador.isEmpty() ) {
			throw new QtSQLException( "Chaves da Tabela ResponsavelRegistroAmbiental n�o encontradas - cdBeneficiarioCartao: " + cdBeneficiarioCartao + " / sqPerfil: " + sqPerfil + " / sqResponsavel: " + sqResponsavel );
		}

		responsavelRegistroAmbiental = (ResponsavelRegistroAmbiental ) responsavelRegistroAmbientalLocalizador.getTabelaBasica();
	}

	/**
	 * Permite alimentar o Persistor com um novo registro
	 */
	public void setTabelaBasica( TabelaBasica responsavelRegistroAmbiental ) throws QtException {

		if( responsavelRegistroAmbiental instanceof ResponsavelRegistroAmbiental ) {
			this.responsavelRegistroAmbiental = (ResponsavelRegistroAmbiental) responsavelRegistroAmbiental;
		} else {
			throw new QtException( "Tabela B�sica fornecida tem que ser uma inst�ncia de ResponsavelRegistroAmbiental" );
		}
	}

	/**
	 * Permite acessar o registro corrente do persistor
	 */
	public TabelaBasica getTabelaBasica() throws QtException {

		return responsavelRegistroAmbiental;
	}

	/**
	 * M�todo para inserir tupla na tabela
	 */
	public void insere() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "insert into PCMSO.RESPONSAVEL_REGISTRO_AMBIENTAL" );
			cmd.append( "( CD_BENEFICIARIO_CARTAO, SQ_PERFIL, SQ_RESPONSAVEL, DT_INICIO, DT_FINAL, NR_NIT_RESPRESENTANTE, CD_REGISTRO_CONSELHO_CLASSE, DS_NOME_PROFISSIONAL )" );
			cmd.append( "values " );
			cmd.append( "( ?, ?, ?, ?, ?, ?, ?, ?)" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			ps.setObject( 1, responsavelRegistroAmbiental.getCdBeneficiarioCartao() );
			ps.setObject( 2, responsavelRegistroAmbiental.getSqPerfil() );
			ps.setObject( 3, responsavelRegistroAmbiental.getSqResponsavel() );
			ps.setObject( 4, QtData.getQtDataAsTimestamp( responsavelRegistroAmbiental.getDtInicio() ) );
			ps.setObject( 5, QtData.getQtDataAsTimestamp( responsavelRegistroAmbiental.getDtFinal() ) );
			ps.setObject( 6, responsavelRegistroAmbiental.getNrNitRespresentante() );
			ps.setObject( 7, responsavelRegistroAmbiental.getCdRegistroConselhoClasse() );
			ps.setObject( 8, responsavelRegistroAmbiental.getDsNomeProfissional() );
			
			ps.execute();

		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}

	/**
	 * M�todo para alterar tupla na tabela
	 */
	public void altera() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "update PCMSO.RESPONSAVEL_REGISTRO_AMBIENTAL" );
			cmd.append( "  set CD_BENEFICIARIO_CARTAO = ?, " );
			cmd.append( " SQ_PERFIL = ?, " );
			cmd.append( " SQ_RESPONSAVEL = ?, " );
			cmd.append( " DT_INICIO = ?, " );
			cmd.append( " DT_FINAL = ?, " );
			cmd.append( " NR_NIT_RESPRESENTANTE = ?, " );
			cmd.append( " CD_REGISTRO_CONSELHO_CLASSE = ?, " );
			cmd.append( " DS_NOME_PROFISSIONAL = ?" );
			cmd.append( " where CD_BENEFICIARIO_CARTAO like ? and SQ_PERFIL = ? and SQ_RESPONSAVEL = ?" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			ps.setObject( 1, responsavelRegistroAmbiental.getCdBeneficiarioCartao() );
			ps.setObject( 2, responsavelRegistroAmbiental.getSqPerfil() );
			ps.setObject( 3, responsavelRegistroAmbiental.getSqResponsavel() );
			ps.setObject( 4, QtData.getQtDataAsTimestamp( responsavelRegistroAmbiental.getDtInicio() ) );
			ps.setObject( 5, QtData.getQtDataAsTimestamp( responsavelRegistroAmbiental.getDtFinal() ) );
			ps.setObject( 6, responsavelRegistroAmbiental.getNrNitRespresentante() );
			ps.setObject( 7, responsavelRegistroAmbiental.getCdRegistroConselhoClasse() );
			ps.setObject( 8, responsavelRegistroAmbiental.getDsNomeProfissional() );
			
			ps.setObject( 9, responsavelRegistroAmbiental.getCdBeneficiarioCartao() );
			ps.setObject( 10, responsavelRegistroAmbiental.getSqPerfil() );
			ps.setObject( 11, responsavelRegistroAmbiental.getSqResponsavel() );
			
			ps.execute();

		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}

	/**
	 * M�todo para excluir tupla de uma tabela
	 */
	public void exclui() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "delete from PCMSO.RESPONSAVEL_REGISTRO_AMBIENTAL" );
			cmd.append( " where CD_BENEFICIARIO_CARTAO like ? and SQ_PERFIL = ? and SQ_RESPONSAVEL = ?" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			ps.setObject( 1, responsavelRegistroAmbiental.getCdBeneficiarioCartao() );
			ps.setObject( 2, responsavelRegistroAmbiental.getSqPerfil() );
			ps.setObject( 3, responsavelRegistroAmbiental.getSqResponsavel() );
			
			ps.execute();

		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}
}