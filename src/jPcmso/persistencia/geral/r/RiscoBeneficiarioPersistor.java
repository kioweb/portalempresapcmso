package jPcmso.persistencia.geral.r;

import java.sql.PreparedStatement;

import quatro.persistencia.Persistor;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.util.QtException;
import quatro.util.QtData;

/**
 *
 * Classe persistor para PCMSO.RISCOS_BENEFICIARIO
 *
 * @author Samuel Antonio Klein
 * @see Persistor
 * @see RiscoBeneficiario
 * @see RiscoBeneficiarioNavegador
 * @see RiscoBeneficiarioLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 public class RiscoBeneficiarioPersistor extends Persistor {

	/** Inst�ncia de RiscoBeneficiario para manipula��o pelo persistor */
	private RiscoBeneficiario riscoBeneficiario;

	/** Construtor gen�rico */
	public RiscoBeneficiarioPersistor() {

		this.riscoBeneficiario = null;
	}

	/** Construtor gen�rico */
	public RiscoBeneficiarioPersistor( Conexao cnx ) throws QtSQLException {

		this.riscoBeneficiario = null;
		super.setConexao( cnx );
	}

	/** Construtor que recebe uma inst�ncia de RiscoBeneficiario */
	public RiscoBeneficiarioPersistor( RiscoBeneficiario riscoBeneficiario ) throws QtSQLException {

		if( riscoBeneficiario == null ) 
			throw new QtSQLException( "Imposs�vel instanciar RiscoBeneficiarioPersistor porque est� apontando para um nulo!" );

		this.riscoBeneficiario = riscoBeneficiario;
	}

	/** Construtor que recebe uma conex�o e uma inst�ncia de RiscoBeneficiario */
	public RiscoBeneficiarioPersistor( Conexao conexao, RiscoBeneficiario riscoBeneficiario ) throws QtSQLException {

		if( riscoBeneficiario == null ) 
			throw new QtSQLException( "Imposs�vel instanciar RiscoBeneficiarioPersistor porque est� apontando para um nulo!" );

		setConexao( conexao );
		this.riscoBeneficiario = riscoBeneficiario;
	}

	/**
	 * Construtor que recebe as chaves da tabela PCMSO.RISCOS_BENEFICIARIO
	 * @param cdBeneficiarioCartao Codigo do beneficiario
	 * @param idRisco Identificador do Risco
	 */
	public RiscoBeneficiarioPersistor( String cdBeneficiarioCartao, int idRisco )
	  throws QtSQLException { 

		busca( cdBeneficiarioCartao, idRisco );
	}

	/**
	 * Construtor que recebe uma conex�o e as chaves da tabela PCMSO.RISCOS_BENEFICIARIO
	 * @param cdBeneficiarioCartao Codigo do beneficiario
	 * @param idRisco Identificador do Risco
	 * @param conexao Conexao com o banco de dados
	 */
	public RiscoBeneficiarioPersistor( Conexao conexao, String cdBeneficiarioCartao, int idRisco )
	  throws QtSQLException { 

		super.setConexao( conexao );
		busca( cdBeneficiarioCartao, idRisco );
	}

	/**
	 * M�todo interno para buscar e preencher uma tupla de uma tabela
	 * Utiliza a Inst�ncia de RiscoBeneficiario para fazer a busca
	 * @param cdBeneficiarioCartao Codigo do beneficiario
	 * @param idRisco Identificador do Risco
	 */
	private void busca( String cdBeneficiarioCartao, int idRisco ) throws QtSQLException { 

		RiscoBeneficiarioLocalizador riscoBeneficiarioLocalizador = new RiscoBeneficiarioLocalizador( getConexao(), cdBeneficiarioCartao, idRisco );

		if( riscoBeneficiarioLocalizador.isEmpty() ) {
			throw new QtSQLException( "Chaves da Tabela RiscoBeneficiario n�o encontradas - cdBeneficiarioCartao: " + cdBeneficiarioCartao + " / idRisco: " + idRisco );
		}

		riscoBeneficiario = (RiscoBeneficiario ) riscoBeneficiarioLocalizador.getTabelaBasica();
	}

	/**
	 * Permite alimentar o Persistor com um novo registro
	 */
	public void setTabelaBasica( TabelaBasica riscoBeneficiario ) throws QtException {

		if( riscoBeneficiario instanceof RiscoBeneficiario ) {
			this.riscoBeneficiario = (RiscoBeneficiario) riscoBeneficiario;
		} else {
			throw new QtException( "Tabela B�sica fornecida tem que ser uma inst�ncia de RiscoBeneficiario" );
		}
	}

	/**
	 * Permite acessar o registro corrente do persistor
	 */
	public TabelaBasica getTabelaBasica() throws QtException {

		return riscoBeneficiario;
	}

	/**
	 * M�todo para inserir tupla na tabela
	 */
	public void insere() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "insert into PCMSO.RISCOS_BENEFICIARIO" );
			cmd.append( "( CD_BENEFICIARIO_CARTAO, ID_RISCO, DT_INCLUSAO, DT_EXCLUSAO )" );
			cmd.append( "values " );
			cmd.append( "( ?, ?, ?, ?)" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			ps.setObject( 1, riscoBeneficiario.getCdBeneficiarioCartao() );
			ps.setObject( 2, riscoBeneficiario.getIdRisco() );
			ps.setObject( 3, QtData.getQtDataAsTimestamp( riscoBeneficiario.getDtInclusao() ) );
			ps.setObject( 4, QtData.getQtDataAsTimestamp( riscoBeneficiario.getDtExclusao() ) );
			
			ps.execute();

		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}

	/**
	 * M�todo para alterar tupla na tabela
	 */
	public void altera() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "update PCMSO.RISCOS_BENEFICIARIO" );
			cmd.append( "  set CD_BENEFICIARIO_CARTAO = ?, " );
			cmd.append( " ID_RISCO = ?, " );
			cmd.append( " DT_INCLUSAO = ?, " );
			cmd.append( " DT_EXCLUSAO = ?" );
			cmd.append( " where CD_BENEFICIARIO_CARTAO like ? and ID_RISCO = ?" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			ps.setObject( 1, riscoBeneficiario.getCdBeneficiarioCartao() );
			ps.setObject( 2, riscoBeneficiario.getIdRisco() );
			ps.setObject( 3, QtData.getQtDataAsTimestamp( riscoBeneficiario.getDtInclusao() ) );
			ps.setObject( 4, QtData.getQtDataAsTimestamp( riscoBeneficiario.getDtExclusao() ) );
			
			ps.setObject( 5, riscoBeneficiario.getCdBeneficiarioCartao() );
			ps.setObject( 6, riscoBeneficiario.getIdRisco() );
			
			ps.execute();

		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}

	/**
	 * M�todo para excluir tupla de uma tabela
	 */
	public void exclui() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "delete from PCMSO.RISCOS_BENEFICIARIO" );
			cmd.append( " where CD_BENEFICIARIO_CARTAO like ? and ID_RISCO = ?" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			ps.setObject( 1, riscoBeneficiario.getCdBeneficiarioCartao() );
			ps.setObject( 2, riscoBeneficiario.getIdRisco() );
			
			ps.execute();

		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}
}