package jPcmso.persistencia.geral.r;

import quatro.persistencia.TabelaBasica;
import quatro.smartDB.IgnoredField;
import quatro.smartDB.PrimaryKey;
import quatro.smartDB.Table;
import quatro.util.QtData;

/**
 *
 * Classe b�sica para PCMSO.RISCOS_BENEFICIARIO
 *
 * Tabela Riscos dp beneficiario
 * 
 * @author Samuel Antonio Klein
 * @see RiscoBeneficiarioNavegador
 * @see RiscoBeneficiarioPersistor
 * @see RiscoBeneficiarioLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 @Table(name="PCMSO.RISCOS_BENEFICIARIO")
 public class RiscoBeneficiario implements TabelaBasica {

	@IgnoredField
	private static final long serialVersionUID = -6729428953215266L;

	/** Codigo do beneficiario (chave-char(12)) */
	@PrimaryKey
	private String cdBeneficiarioCartao;
	/** Identificador do Risco (chave) */
	@PrimaryKey
	private Integer idRisco;
	/** Data de inclusao */
	private QtData dtInclusao;
	/** Data exclusao */
	private QtData dtExclusao;

	/**
	 * Alimenta Codigo do beneficiario
	 * @param cdBeneficiarioCartao Codigo do beneficiario
	 */ 
	public void setCdBeneficiarioCartao( String cdBeneficiarioCartao ) {
		this.cdBeneficiarioCartao = cdBeneficiarioCartao;
	}

	/**
	 * Retorna Codigo do beneficiario
	 * @return Codigo do beneficiario
	 */
	public String getCdBeneficiarioCartao() {
		return this.cdBeneficiarioCartao;
	}

	/**
	 * Alimenta Identificador do Risco
	 * @param idRisco Identificador do Risco
	 */ 
	public void setIdRisco( Integer idRisco ) {
		this.idRisco = idRisco;
	}

	/**
	 * Retorna Identificador do Risco
	 * @return Identificador do Risco
	 */
	public Integer getIdRisco() {
		return this.idRisco;
	}

	/**
	 * Alimenta Data de inclusao
	 * @param dtInclusao Data de inclusao
	 */ 
	public void setDtInclusao( QtData dtInclusao ) {
		this.dtInclusao = dtInclusao;
	}

	/**
	 * Retorna Data de inclusao
	 * @return Data de inclusao
	 */
	public QtData getDtInclusao() {
		return this.dtInclusao;
	}

	/**
	 * Alimenta Data exclusao
	 * @param dtExclusao Data exclusao
	 */ 
	public void setDtExclusao( QtData dtExclusao ) {
		this.dtExclusao = dtExclusao;
	}

	/**
	 * Retorna Data exclusao
	 * @return Data exclusao
	 */
	public QtData getDtExclusao() {
		return this.dtExclusao;
	}

}