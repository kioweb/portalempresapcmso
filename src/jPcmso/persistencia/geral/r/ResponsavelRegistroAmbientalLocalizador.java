package jPcmso.persistencia.geral.r;

import quatro.persistencia.Localizador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.QtSQLException;
import quatro.sql.Query;
import quatro.sql.Conexao;
import quatro.sql.PoolDeConexoes;
import quatro.sql.ConexaoParaPoolDeConexoes;
import quatro.util.QtData;

/**
 *
 * Classe localizador para PCMSO.RESPONSAVEL_REGISTRO_AMBIENTAL
 *
 * @author Samuel Antonio Klein
 * @see Localizador
 * @see ResponsavelRegistroAmbiental
 * @see ResponsavelRegistroAmbientalNavegador
 * @see ResponsavelRegistroAmbientalPersistor
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:31 - importado por Samuel Antonio Klein
 *
 */
 public class ResponsavelRegistroAmbientalLocalizador extends Localizador {

	/** Inst�ncia de ResponsavelRegistroAmbiental para manipula��o pelo localizador */
	private ResponsavelRegistroAmbiental responsavelRegistroAmbiental;

	/** Construtor que recebe uma conex�o e um objeto ResponsavelRegistroAmbiental completo */
	public ResponsavelRegistroAmbientalLocalizador( Conexao conexao, ResponsavelRegistroAmbiental responsavelRegistroAmbiental ) throws QtSQLException {

		super.setConexao( conexao );

		this.responsavelRegistroAmbiental = responsavelRegistroAmbiental;
		busca();
	}

	/** Construtor que recebe um objeto ResponsavelRegistroAmbiental completo */
	public ResponsavelRegistroAmbientalLocalizador( ResponsavelRegistroAmbiental responsavelRegistroAmbiental ) throws QtSQLException {

		this.responsavelRegistroAmbiental = responsavelRegistroAmbiental;
		busca();
	}

	/** 
	 * Construtor que recebe uma conex�o e as chaves da tabela PCMSO.RESPONSAVEL_REGISTRO_AMBIENTAL
	 * @param cdBeneficiarioCartao Codigo de Beneficiario 
	 * @param sqPerfil sequencia perfil
	 * @param sqResponsavel sequencia Responsavel
	 * @param conexao Conexao com o banco de dados
	 */
	public ResponsavelRegistroAmbientalLocalizador( Conexao conexao, String cdBeneficiarioCartao, int sqPerfil, int sqResponsavel ) throws QtSQLException { 

		super.setConexao( conexao ); 
		responsavelRegistroAmbiental = new ResponsavelRegistroAmbiental();
		this.responsavelRegistroAmbiental.setCdBeneficiarioCartao( cdBeneficiarioCartao );
		this.responsavelRegistroAmbiental.setSqPerfil( new Integer( sqPerfil ) );
		this.responsavelRegistroAmbiental.setSqResponsavel( new Integer( sqResponsavel ) );
		
		busca();
	}

	/** 
	 * Construtor que recebe as chaves da tabela PCMSO.RESPONSAVEL_REGISTRO_AMBIENTAL
	 * @param cdBeneficiarioCartao Codigo de Beneficiario 
	 * @param sqPerfil sequencia perfil
	 * @param sqResponsavel sequencia Responsavel
	 */
	public ResponsavelRegistroAmbientalLocalizador( String cdBeneficiarioCartao, int sqPerfil, int sqResponsavel ) throws QtSQLException { 

		responsavelRegistroAmbiental = new ResponsavelRegistroAmbiental();
		this.responsavelRegistroAmbiental.setCdBeneficiarioCartao( cdBeneficiarioCartao );
		this.responsavelRegistroAmbiental.setSqPerfil( new Integer( sqPerfil ) );
		this.responsavelRegistroAmbiental.setSqResponsavel( new Integer( sqResponsavel ) );
		
		busca();
	}

	public static boolean existe( Conexao cnx, String cdBeneficiarioCartao, int sqPerfil, int sqResponsavel ) throws QtSQLException {

		Query q = new Query( cnx );

		try {
			q.setSQL( "select count(*) from PCMSO.RESPONSAVEL_REGISTRO_AMBIENTAL" );
			q.addSQL( " where CD_BENEFICIARIO_CARTAO = :prm1CdBeneficiarioCartao and SQ_PERFIL = :prm2SqPerfil and SQ_RESPONSAVEL = :prm3SqResponsavel" );

			q.setParameter( "prm1CdBeneficiarioCartao", cdBeneficiarioCartao );
			q.setParameter( "prm2SqPerfil", sqPerfil );
			q.setParameter( "prm3SqResponsavel", sqResponsavel );
			q.executeQuery();

			return q.getInt( 1 ) > 0;
		} finally {
			q.liberaRecursos();
		}
	}

	public static boolean existe( String cdBeneficiarioCartao, int sqPerfil, int sqResponsavel ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return existe( cnx, cdBeneficiarioCartao, sqPerfil, sqResponsavel );
		} finally {
			cnx.libera();
		}
	}

	public static boolean existe( Conexao cnx, ResponsavelRegistroAmbiental responsavelRegistroAmbiental ) throws QtSQLException {

		return existe( cnx, responsavelRegistroAmbiental.getCdBeneficiarioCartao(), responsavelRegistroAmbiental.getSqPerfil(), responsavelRegistroAmbiental.getSqResponsavel() );
	}

	public static boolean existe( ResponsavelRegistroAmbiental responsavelRegistroAmbiental ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return existe( cnx, responsavelRegistroAmbiental.getCdBeneficiarioCartao(), responsavelRegistroAmbiental.getSqPerfil(), responsavelRegistroAmbiental.getSqResponsavel() );
		} finally {
			cnx.libera();
		}
	}

	public static ResponsavelRegistroAmbiental buscaResponsavelRegistroAmbiental( Conexao cnx, String cdBeneficiarioCartao, int sqPerfil, int sqResponsavel ) throws QtSQLException {

		Query q = new Query( cnx );

		try {
			q.setSQL( "select * from PCMSO.RESPONSAVEL_REGISTRO_AMBIENTAL" );
			q.addSQL( " where CD_BENEFICIARIO_CARTAO = :prm1CdBeneficiarioCartao and SQ_PERFIL = :prm2SqPerfil and SQ_RESPONSAVEL = :prm3SqResponsavel" );

			q.setParameter( "prm1CdBeneficiarioCartao", cdBeneficiarioCartao );
			q.setParameter( "prm2SqPerfil", sqPerfil );
			q.setParameter( "prm3SqResponsavel", sqResponsavel );
			q.executeQuery();

			if( q.isEmpty() ) {
				return null;
			} else {
				ResponsavelRegistroAmbiental responsavelRegistroAmbiental = new ResponsavelRegistroAmbiental();
				buscaCampos( responsavelRegistroAmbiental, q );
				return responsavelRegistroAmbiental;
			}
		} finally {
			q.liberaRecursos();
		}
	}

	public static ResponsavelRegistroAmbiental buscaResponsavelRegistroAmbiental( String cdBeneficiarioCartao, int sqPerfil, int sqResponsavel ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return buscaResponsavelRegistroAmbiental( cnx, cdBeneficiarioCartao, sqPerfil, sqResponsavel );
		} finally { 
			cnx.libera();
		}
	}

	/** 
	 * Construtor que recebe uma conex�o e as chaves da tabela PCMSO.RESPONSAVEL_REGISTRO_AMBIENTAL
	 * @param cdBeneficiarioCartao Codigo de Beneficiario 
	 * @param sqPerfil sequencia perfil
	 * @param sqResponsavel sequencia Responsavel
	 * @param conexao Conexao com o banco de dados
	 */
	public ResponsavelRegistroAmbientalLocalizador( Conexao conexao, String cdBeneficiarioCartao, Integer sqPerfil, Integer sqResponsavel ) throws QtSQLException {

		super.setConexao( conexao ); 
		responsavelRegistroAmbiental = new ResponsavelRegistroAmbiental();
		this.responsavelRegistroAmbiental.setCdBeneficiarioCartao( cdBeneficiarioCartao );
		this.responsavelRegistroAmbiental.setSqPerfil( sqPerfil );
		this.responsavelRegistroAmbiental.setSqResponsavel( sqResponsavel );
		
		busca();
	}

	/** 
	 * Construtor que recebe as chaves da tabela PCMSO.RESPONSAVEL_REGISTRO_AMBIENTAL
	 * @param cdBeneficiarioCartao Codigo de Beneficiario 
	 * @param sqPerfil sequencia perfil
	 * @param sqResponsavel sequencia Responsavel
	 */
	public ResponsavelRegistroAmbientalLocalizador( String cdBeneficiarioCartao, Integer sqPerfil, Integer sqResponsavel ) throws QtSQLException {

		responsavelRegistroAmbiental = new ResponsavelRegistroAmbiental();
		this.responsavelRegistroAmbiental.setCdBeneficiarioCartao( cdBeneficiarioCartao );
		this.responsavelRegistroAmbiental.setSqPerfil( sqPerfil );
		this.responsavelRegistroAmbiental.setSqResponsavel( sqResponsavel );
		
		busca();
	}

	public static void buscaCampos( ResponsavelRegistroAmbiental responsavelRegistroAmbiental, Query query ) throws QtSQLException {

		responsavelRegistroAmbiental.setCdBeneficiarioCartao( query.getString( "CD_BENEFICIARIO_CARTAO" ) );
		responsavelRegistroAmbiental.setSqPerfil( query.getInteger( "SQ_PERFIL" ) );
		responsavelRegistroAmbiental.setSqResponsavel( query.getInteger( "SQ_RESPONSAVEL" ) );
		responsavelRegistroAmbiental.setDtInicio( QtData.criaQtData( query.getTimestamp( "DT_INICIO" ) ) );
		responsavelRegistroAmbiental.setDtFinal( QtData.criaQtData( query.getTimestamp( "DT_FINAL" ) ) );
		responsavelRegistroAmbiental.setNrNitRespresentante( query.getInteger( "NR_NIT_RESPRESENTANTE" ) );
		responsavelRegistroAmbiental.setCdRegistroConselhoClasse( query.getString( "CD_REGISTRO_CONSELHO_CLASSE" ) );
		responsavelRegistroAmbiental.setDsNomeProfissional( query.getString( "DS_NOME_PROFISSIONAL" ) );
	}

	/** M�todo que retorna uma tabela b�sica a que se refere o localizador */
	public TabelaBasica getTabelaBasica() {

		return this.responsavelRegistroAmbiental;
	}

	/** M�todo que retorna a classe ResponsavelRegistroAmbiental relacionada ao localizador */
	public ResponsavelRegistroAmbiental getResponsavelRegistroAmbiental() {

		return this.responsavelRegistroAmbiental;
	}

	/**
	 * M�todo para buscar e preencher uma tupla de uma tabela
	 * @param tabelaBasica Recebe uma tabela basica com os atributos chave preenchidos
	 */
	public boolean busca( TabelaBasica tabelaBasica ) throws QtSQLException {

		if( !( tabelaBasica instanceof ResponsavelRegistroAmbiental ) )
			throw new QtSQLException( "Esperava um objeto ResponsavelRegistroAmbiental!" );

		this.responsavelRegistroAmbiental = (ResponsavelRegistroAmbiental) tabelaBasica;
		return busca();

	}

	/**
	 * M�todo privado para buscar e preencher uma tupla de uma tabela
	 * Este m�todo busca os dados da inst�ncia de ResponsavelRegistroAmbiental atual
	 */
	private boolean busca() throws QtSQLException {

		boolean ret = false;

		super.estabeleceConexao();
		Query query = super.criaNovaQuery();

		try {

			query.setSQL( "select * from PCMSO.RESPONSAVEL_REGISTRO_AMBIENTAL" );
			query.addSQL( " where CD_BENEFICIARIO_CARTAO like :prm1CdBeneficiarioCartao and SQ_PERFIL = :prm2SqPerfil and SQ_RESPONSAVEL = :prm3SqResponsavel" );

			query.setParameter( "prm1CdBeneficiarioCartao", responsavelRegistroAmbiental.getCdBeneficiarioCartao() );
			query.setParameter( "prm2SqPerfil", responsavelRegistroAmbiental.getSqPerfil() );
			query.setParameter( "prm3SqResponsavel", responsavelRegistroAmbiental.getSqResponsavel() );
			query.executeQuery();

			if( query.isEmpty() ) {
				super.setEmpty( true );
				responsavelRegistroAmbiental = null;
			} else {
				super.setEmpty( false );
				
				responsavelRegistroAmbiental.setCdBeneficiarioCartao( query.getString( "CD_BENEFICIARIO_CARTAO" ) );
				responsavelRegistroAmbiental.setSqPerfil( query.getInteger( "SQ_PERFIL" ) );
				responsavelRegistroAmbiental.setSqResponsavel( query.getInteger( "SQ_RESPONSAVEL" ) );
				responsavelRegistroAmbiental.setDtInicio( QtData.criaQtData( query.getTimestamp( "DT_INICIO" ) ) );
				responsavelRegistroAmbiental.setDtFinal( QtData.criaQtData( query.getTimestamp( "DT_FINAL" ) ) );
				responsavelRegistroAmbiental.setNrNitRespresentante( query.getInteger( "NR_NIT_RESPRESENTANTE" ) );
				responsavelRegistroAmbiental.setCdRegistroConselhoClasse( query.getString( "CD_REGISTRO_CONSELHO_CLASSE" ) );
				responsavelRegistroAmbiental.setDsNomeProfissional( query.getString( "DS_NOME_PROFISSIONAL" ) );
				ret = true;
			}
		} finally {
			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}
 }