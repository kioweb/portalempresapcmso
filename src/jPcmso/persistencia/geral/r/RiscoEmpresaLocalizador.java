package jPcmso.persistencia.geral.r;

import quatro.persistencia.Localizador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.QtSQLException;
import quatro.sql.Query;
import quatro.sql.Conexao;
import quatro.sql.PoolDeConexoes;
import quatro.sql.ConexaoParaPoolDeConexoes;
import quatro.util.QtData;

/**
 *
 * Classe localizador para PCMSO.RISCOS_EMPRESA
 *
 * @author Samuel Antonio Klein
 * @see Localizador
 * @see RiscoEmpresa
 * @see RiscoEmpresaNavegador
 * @see RiscoEmpresaPersistor
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 public class RiscoEmpresaLocalizador extends Localizador {

	/** Inst�ncia de RiscoEmpresa para manipula��o pelo localizador */
	private RiscoEmpresa riscoEmpresa;

	/** Construtor que recebe uma conex�o e um objeto RiscoEmpresa completo */
	public RiscoEmpresaLocalizador( Conexao conexao, RiscoEmpresa riscoEmpresa ) throws QtSQLException {

		super.setConexao( conexao );

		this.riscoEmpresa = riscoEmpresa;
		busca();
	}

	/** Construtor que recebe um objeto RiscoEmpresa completo */
	public RiscoEmpresaLocalizador( RiscoEmpresa riscoEmpresa ) throws QtSQLException {

		this.riscoEmpresa = riscoEmpresa;
		busca();
	}

	/** 
	 * Construtor que recebe uma conex�o e as chaves da tabela PCMSO.RISCOS_EMPRESA
	 * @param idRisco Identificador do Risco
	 * @param cdEmpresa Codigo da empresa
	 * @param conexao Conexao com o banco de dados
	 */
	public RiscoEmpresaLocalizador( Conexao conexao, int idRisco, String cdEmpresa ) throws QtSQLException { 

		super.setConexao( conexao ); 
		riscoEmpresa = new RiscoEmpresa();
		this.riscoEmpresa.setIdRisco( new Integer( idRisco ) );
		this.riscoEmpresa.setCdEmpresa( cdEmpresa );
		
		busca();
	}

	/** 
	 * Construtor que recebe as chaves da tabela PCMSO.RISCOS_EMPRESA
	 * @param idRisco Identificador do Risco
	 * @param cdEmpresa Codigo da empresa
	 */
	public RiscoEmpresaLocalizador( int idRisco, String cdEmpresa ) throws QtSQLException { 

		riscoEmpresa = new RiscoEmpresa();
		this.riscoEmpresa.setIdRisco( new Integer( idRisco ) );
		this.riscoEmpresa.setCdEmpresa( cdEmpresa );
		
		busca();
	}

	public static boolean existe( Conexao cnx, int idRisco, String cdEmpresa ) throws QtSQLException {

		Query q = new Query( cnx );

		try {
			q.setSQL( "select count(*) from PCMSO.RISCOS_EMPRESA" );
			q.addSQL( " where ID_RISCO = :prm1IdRisco and CD_EMPRESA = :prm2CdEmpresa" );

			q.setParameter( "prm1IdRisco", idRisco );
			q.setParameter( "prm2CdEmpresa", cdEmpresa );
			q.executeQuery();

			return q.getInt( 1 ) > 0;
		} finally {
			q.liberaRecursos();
		}
	}

	public static boolean existe( int idRisco, String cdEmpresa ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return existe( cnx, idRisco, cdEmpresa );
		} finally {
			cnx.libera();
		}
	}

	public static boolean existe( Conexao cnx, RiscoEmpresa riscoEmpresa ) throws QtSQLException {

		return existe( cnx, riscoEmpresa.getIdRisco(), riscoEmpresa.getCdEmpresa() );
	}

	public static boolean existe( RiscoEmpresa riscoEmpresa ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return existe( cnx, riscoEmpresa.getIdRisco(), riscoEmpresa.getCdEmpresa() );
		} finally {
			cnx.libera();
		}
	}

	public static RiscoEmpresa buscaRiscoEmpresa( Conexao cnx, int idRisco, String cdEmpresa ) throws QtSQLException {

		Query q = new Query( cnx );

		try {
			q.setSQL( "select * from PCMSO.RISCOS_EMPRESA" );
			q.addSQL( " where ID_RISCO = :prm1IdRisco and CD_EMPRESA = :prm2CdEmpresa" );

			q.setParameter( "prm1IdRisco", idRisco );
			q.setParameter( "prm2CdEmpresa", cdEmpresa );
			q.executeQuery();

			if( q.isEmpty() ) {
				return null;
			} else {
				RiscoEmpresa riscoEmpresa = new RiscoEmpresa();
				buscaCampos( riscoEmpresa, q );
				return riscoEmpresa;
			}
		} finally {
			q.liberaRecursos();
		}
	}

	public static RiscoEmpresa buscaRiscoEmpresa( int idRisco, String cdEmpresa ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return buscaRiscoEmpresa( cnx, idRisco, cdEmpresa );
		} finally { 
			cnx.libera();
		}
	}

	/** 
	 * Construtor que recebe uma conex�o e as chaves da tabela PCMSO.RISCOS_EMPRESA
	 * @param idRisco Identificador do Risco
	 * @param cdEmpresa Codigo da empresa
	 * @param conexao Conexao com o banco de dados
	 */
	public RiscoEmpresaLocalizador( Conexao conexao, Integer idRisco, String cdEmpresa ) throws QtSQLException {

		super.setConexao( conexao ); 
		riscoEmpresa = new RiscoEmpresa();
		this.riscoEmpresa.setIdRisco( idRisco );
		this.riscoEmpresa.setCdEmpresa( cdEmpresa );
		
		busca();
	}

	/** 
	 * Construtor que recebe as chaves da tabela PCMSO.RISCOS_EMPRESA
	 * @param idRisco Identificador do Risco
	 * @param cdEmpresa Codigo da empresa
	 */
	public RiscoEmpresaLocalizador( Integer idRisco, String cdEmpresa ) throws QtSQLException {

		riscoEmpresa = new RiscoEmpresa();
		this.riscoEmpresa.setIdRisco( idRisco );
		this.riscoEmpresa.setCdEmpresa( cdEmpresa );
		
		busca();
	}

	public static void buscaCampos( RiscoEmpresa riscoEmpresa, Query query ) throws QtSQLException {

		riscoEmpresa.setIdRisco( query.getInteger( "ID_RISCO" ) );
		riscoEmpresa.setCdEmpresa( query.getString( "CD_EMPRESA" ) );
		riscoEmpresa.setDtInclusao( QtData.criaQtData( query.getTimestamp( "DT_INCLUSAO" ) ) );
		riscoEmpresa.setDtExclusao( QtData.criaQtData( query.getTimestamp( "DT_EXCLUSAO" ) ) );
	}

	/** M�todo que retorna uma tabela b�sica a que se refere o localizador */
	public TabelaBasica getTabelaBasica() {

		return this.riscoEmpresa;
	}

	/** M�todo que retorna a classe RiscoEmpresa relacionada ao localizador */
	public RiscoEmpresa getRiscoEmpresa() {

		return this.riscoEmpresa;
	}

	/**
	 * M�todo para buscar e preencher uma tupla de uma tabela
	 * @param tabelaBasica Recebe uma tabela basica com os atributos chave preenchidos
	 */
	public boolean busca( TabelaBasica tabelaBasica ) throws QtSQLException {

		if( !( tabelaBasica instanceof RiscoEmpresa ) )
			throw new QtSQLException( "Esperava um objeto RiscoEmpresa!" );

		this.riscoEmpresa = (RiscoEmpresa) tabelaBasica;
		return busca();

	}

	/**
	 * M�todo privado para buscar e preencher uma tupla de uma tabela
	 * Este m�todo busca os dados da inst�ncia de RiscoEmpresa atual
	 */
	private boolean busca() throws QtSQLException {

		boolean ret = false;

		super.estabeleceConexao();
		Query query = super.criaNovaQuery();

		try {

			query.setSQL( "select * from PCMSO.RISCOS_EMPRESA" );
			query.addSQL( " where ID_RISCO = :prm1IdRisco and CD_EMPRESA like :prm2CdEmpresa" );

			query.setParameter( "prm1IdRisco", riscoEmpresa.getIdRisco() );
			query.setParameter( "prm2CdEmpresa", riscoEmpresa.getCdEmpresa() );
			query.executeQuery();

			if( query.isEmpty() ) {
				super.setEmpty( true );
				riscoEmpresa = null;
			} else {
				super.setEmpty( false );
				
				riscoEmpresa.setIdRisco( query.getInteger( "ID_RISCO" ) );
				riscoEmpresa.setCdEmpresa( query.getString( "CD_EMPRESA" ) );
				riscoEmpresa.setDtInclusao( QtData.criaQtData( query.getTimestamp( "DT_INCLUSAO" ) ) );
				riscoEmpresa.setDtExclusao( QtData.criaQtData( query.getTimestamp( "DT_EXCLUSAO" ) ) );
				ret = true;
			}
		} finally {
			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}
 }