package jPcmso.persistencia.geral.r;

import quatro.persistencia.TabelaBasica;
import quatro.smartDB.IgnoredField;
import quatro.smartDB.PrimaryKey;
import quatro.smartDB.Table;

/**
 *
 * Classe básica para PCMSO.RISCOS
 *
 * Tabela Riscos
 * 
 * @author ronei@unimed-uau.com.br
 * @see RiscoNavegador
 * @see RiscoPersistor
 * @see RiscoLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 14/12/2017 15:26 - importado por ronei@unimed-uau.com.br
 *
 */
 @Table(name="PCMSO.RISCOS")
 public class Risco implements TabelaBasica {

	@IgnoredField
	private static final long serialVersionUID = -1409417474114111385L;

	/** Identificador do Risco (chave) */
	@PrimaryKey
	private Integer idRisco;
	/** tipo de Risco (char(1)) */
	private String tpRisco;
	/** Descrição de Risco (varchar(250)) */
	private String dsRisco;

	/**
	 * Alimenta Identificador do Risco
	 * @param idRisco Identificador do Risco
	 */ 
	public void setIdRisco( Integer idRisco ) {
		this.idRisco = idRisco;
	}

	/**
	 * Retorna Identificador do Risco
	 * @return Identificador do Risco
	 */
	public Integer getIdRisco() {
		return this.idRisco;
	}

	/**
	 * Alimenta tipo de Risco
	 * @param tpRisco tipo de Risco
	 */ 
	public void setTpRisco( String tpRisco ) {
		this.tpRisco = tpRisco;
	}

	/**
	 * Retorna tipo de Risco
	 * @return tipo de Risco
	 */
	public String getTpRisco() {
		return this.tpRisco;
	}

	/**
	 * Alimenta Descrição de Risco
	 * @param dsRisco Descrição de Risco
	 */ 
	public void setDsRisco( String dsRisco ) {
		this.dsRisco = dsRisco;
	}

	/**
	 * Retorna Descrição de Risco
	 * @return Descrição de Risco
	 */
	public String getDsRisco() {
		return this.dsRisco;
	}

}