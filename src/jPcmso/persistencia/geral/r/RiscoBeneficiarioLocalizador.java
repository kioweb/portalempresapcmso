package jPcmso.persistencia.geral.r;

import quatro.persistencia.Localizador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.QtSQLException;
import quatro.sql.Query;
import quatro.sql.Conexao;
import quatro.sql.PoolDeConexoes;
import quatro.sql.ConexaoParaPoolDeConexoes;
import quatro.util.QtData;

/**
 *
 * Classe localizador para PCMSO.RISCOS_BENEFICIARIO
 *
 * @author Samuel Antonio Klein
 * @see Localizador
 * @see RiscoBeneficiario
 * @see RiscoBeneficiarioNavegador
 * @see RiscoBeneficiarioPersistor
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 public class RiscoBeneficiarioLocalizador extends Localizador {

	/** Inst�ncia de RiscoBeneficiario para manipula��o pelo localizador */
	private RiscoBeneficiario riscoBeneficiario;

	/** Construtor que recebe uma conex�o e um objeto RiscoBeneficiario completo */
	public RiscoBeneficiarioLocalizador( Conexao conexao, RiscoBeneficiario riscoBeneficiario ) throws QtSQLException {

		super.setConexao( conexao );

		this.riscoBeneficiario = riscoBeneficiario;
		busca();
	}

	/** Construtor que recebe um objeto RiscoBeneficiario completo */
	public RiscoBeneficiarioLocalizador( RiscoBeneficiario riscoBeneficiario ) throws QtSQLException {

		this.riscoBeneficiario = riscoBeneficiario;
		busca();
	}

	/** 
	 * Construtor que recebe uma conex�o e as chaves da tabela PCMSO.RISCOS_BENEFICIARIO
	 * @param cdBeneficiarioCartao Codigo do beneficiario
	 * @param idRisco Identificador do Risco
	 * @param conexao Conexao com o banco de dados
	 */
	public RiscoBeneficiarioLocalizador( Conexao conexao, String cdBeneficiarioCartao, int idRisco ) throws QtSQLException { 

		super.setConexao( conexao ); 
		riscoBeneficiario = new RiscoBeneficiario();
		this.riscoBeneficiario.setCdBeneficiarioCartao( cdBeneficiarioCartao );
		this.riscoBeneficiario.setIdRisco( new Integer( idRisco ) );
		
		busca();
	}

	/** 
	 * Construtor que recebe as chaves da tabela PCMSO.RISCOS_BENEFICIARIO
	 * @param cdBeneficiarioCartao Codigo do beneficiario
	 * @param idRisco Identificador do Risco
	 */
	public RiscoBeneficiarioLocalizador( String cdBeneficiarioCartao, int idRisco ) throws QtSQLException { 

		riscoBeneficiario = new RiscoBeneficiario();
		this.riscoBeneficiario.setCdBeneficiarioCartao( cdBeneficiarioCartao );
		this.riscoBeneficiario.setIdRisco( new Integer( idRisco ) );
		
		busca();
	}

	public static boolean existe( Conexao cnx, String cdBeneficiarioCartao, int idRisco ) throws QtSQLException {

		Query q = new Query( cnx );

		try {
			q.setSQL( "select count(*) from PCMSO.RISCOS_BENEFICIARIO" );
			q.addSQL( " where CD_BENEFICIARIO_CARTAO = :prm1CdBeneficiarioCartao and ID_RISCO = :prm2IdRisco" );

			q.setParameter( "prm1CdBeneficiarioCartao", cdBeneficiarioCartao );
			q.setParameter( "prm2IdRisco", idRisco );
			q.executeQuery();

			return q.getInt( 1 ) > 0;
		} finally {
			q.liberaRecursos();
		}
	}

	public static boolean existe( String cdBeneficiarioCartao, int idRisco ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return existe( cnx, cdBeneficiarioCartao, idRisco );
		} finally {
			cnx.libera();
		}
	}

	public static boolean existe( Conexao cnx, RiscoBeneficiario riscoBeneficiario ) throws QtSQLException {

		return existe( cnx, riscoBeneficiario.getCdBeneficiarioCartao(), riscoBeneficiario.getIdRisco() );
	}

	public static boolean existe( RiscoBeneficiario riscoBeneficiario ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return existe( cnx, riscoBeneficiario.getCdBeneficiarioCartao(), riscoBeneficiario.getIdRisco() );
		} finally {
			cnx.libera();
		}
	}

	public static RiscoBeneficiario buscaRiscoBeneficiario( Conexao cnx, String cdBeneficiarioCartao, int idRisco ) throws QtSQLException {

		Query q = new Query( cnx );

		try {
			q.setSQL( "select * from PCMSO.RISCOS_BENEFICIARIO" );
			q.addSQL( " where CD_BENEFICIARIO_CARTAO = :prm1CdBeneficiarioCartao and ID_RISCO = :prm2IdRisco" );

			q.setParameter( "prm1CdBeneficiarioCartao", cdBeneficiarioCartao );
			q.setParameter( "prm2IdRisco", idRisco );
			q.executeQuery();

			if( q.isEmpty() ) {
				return null;
			} else {
				RiscoBeneficiario riscoBeneficiario = new RiscoBeneficiario();
				buscaCampos( riscoBeneficiario, q );
				return riscoBeneficiario;
			}
		} finally {
			q.liberaRecursos();
		}
	}

	public static RiscoBeneficiario buscaRiscoBeneficiario( String cdBeneficiarioCartao, int idRisco ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return buscaRiscoBeneficiario( cnx, cdBeneficiarioCartao, idRisco );
		} finally { 
			cnx.libera();
		}
	}

	/** 
	 * Construtor que recebe uma conex�o e as chaves da tabela PCMSO.RISCOS_BENEFICIARIO
	 * @param cdBeneficiarioCartao Codigo do beneficiario
	 * @param idRisco Identificador do Risco
	 * @param conexao Conexao com o banco de dados
	 */
	public RiscoBeneficiarioLocalizador( Conexao conexao, String cdBeneficiarioCartao, Integer idRisco ) throws QtSQLException {

		super.setConexao( conexao ); 
		riscoBeneficiario = new RiscoBeneficiario();
		this.riscoBeneficiario.setCdBeneficiarioCartao( cdBeneficiarioCartao );
		this.riscoBeneficiario.setIdRisco( idRisco );
		
		busca();
	}

	/** 
	 * Construtor que recebe as chaves da tabela PCMSO.RISCOS_BENEFICIARIO
	 * @param cdBeneficiarioCartao Codigo do beneficiario
	 * @param idRisco Identificador do Risco
	 */
	public RiscoBeneficiarioLocalizador( String cdBeneficiarioCartao, Integer idRisco ) throws QtSQLException {

		riscoBeneficiario = new RiscoBeneficiario();
		this.riscoBeneficiario.setCdBeneficiarioCartao( cdBeneficiarioCartao );
		this.riscoBeneficiario.setIdRisco( idRisco );
		
		busca();
	}

	public static void buscaCampos( RiscoBeneficiario riscoBeneficiario, Query query ) throws QtSQLException {

		riscoBeneficiario.setCdBeneficiarioCartao( query.getString( "CD_BENEFICIARIO_CARTAO" ) );
		riscoBeneficiario.setIdRisco( query.getInteger( "ID_RISCO" ) );
		riscoBeneficiario.setDtInclusao( QtData.criaQtData( query.getTimestamp( "DT_INCLUSAO" ) ) );
		riscoBeneficiario.setDtExclusao( QtData.criaQtData( query.getTimestamp( "DT_EXCLUSAO" ) ) );
	}

	/** M�todo que retorna uma tabela b�sica a que se refere o localizador */
	public TabelaBasica getTabelaBasica() {

		return this.riscoBeneficiario;
	}

	/** M�todo que retorna a classe RiscoBeneficiario relacionada ao localizador */
	public RiscoBeneficiario getRiscoBeneficiario() {

		return this.riscoBeneficiario;
	}

	/**
	 * M�todo para buscar e preencher uma tupla de uma tabela
	 * @param tabelaBasica Recebe uma tabela basica com os atributos chave preenchidos
	 */
	public boolean busca( TabelaBasica tabelaBasica ) throws QtSQLException {

		if( !( tabelaBasica instanceof RiscoBeneficiario ) )
			throw new QtSQLException( "Esperava um objeto RiscoBeneficiario!" );

		this.riscoBeneficiario = (RiscoBeneficiario) tabelaBasica;
		return busca();

	}

	/**
	 * M�todo privado para buscar e preencher uma tupla de uma tabela
	 * Este m�todo busca os dados da inst�ncia de RiscoBeneficiario atual
	 */
	private boolean busca() throws QtSQLException {

		boolean ret = false;

		super.estabeleceConexao();
		Query query = super.criaNovaQuery();

		try {

			query.setSQL( "select * from PCMSO.RISCOS_BENEFICIARIO" );
			query.addSQL( " where CD_BENEFICIARIO_CARTAO like :prm1CdBeneficiarioCartao and ID_RISCO = :prm2IdRisco" );

			query.setParameter( "prm1CdBeneficiarioCartao", riscoBeneficiario.getCdBeneficiarioCartao() );
			query.setParameter( "prm2IdRisco", riscoBeneficiario.getIdRisco() );
			query.executeQuery();

			if( query.isEmpty() ) {
				super.setEmpty( true );
				riscoBeneficiario = null;
			} else {
				super.setEmpty( false );
				
				riscoBeneficiario.setCdBeneficiarioCartao( query.getString( "CD_BENEFICIARIO_CARTAO" ) );
				riscoBeneficiario.setIdRisco( query.getInteger( "ID_RISCO" ) );
				riscoBeneficiario.setDtInclusao( QtData.criaQtData( query.getTimestamp( "DT_INCLUSAO" ) ) );
				riscoBeneficiario.setDtExclusao( QtData.criaQtData( query.getTimestamp( "DT_EXCLUSAO" ) ) );
				ret = true;
			}
		} finally {
			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}
 }