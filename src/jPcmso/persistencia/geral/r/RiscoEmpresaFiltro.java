package jPcmso.persistencia.geral.r;

import quatro.persistencia.FiltroDeNavegador;

/**
 *
 * Classe filtro para PCMSO.RISCOS_EMPRESA
 *
 * @author Samuel Antonio Klein
 * @see FiltroDeTabela
 * @see RiscoEmpresa
 * @see RiscoEmpresaNavegador
 * @see RiscoEmpresaPersistor
 * @see RiscoEmpresaLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 public class RiscoEmpresaFiltro extends FiltroDeNavegador {

	/** Determina a ordem de navega��o que ser� passada para o navegador */
	public void setOrdemDeNavegacao( int idOrdem ) {

		super.setOrdemDeNavegacao( idOrdem );
	}
}