package jPcmso.persistencia.geral.r;

import quatro.persistencia.Caminhador;
import quatro.persistencia.FiltroDeNavegador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;

 /**
 *
 * Classe Caminhador para Risco
 *
 * @author ronei@unimed-uau.com.br
 * @see Caminhador
 * @see FiltroDeNavegador
 * @see RiscoNavegador
 * @see RiscoPersistor
 * @see RiscoLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 14/12/2017 15:26 - importado por ronei@unimed-uau.com.br
 *
 */
 public class RiscoCaminhador extends Caminhador {

	public static final int POR_ORDEM_DEFAULT = -1;
	public static final int POR_CODIGO = 0;
	public static final int POR_DESCRICAO = 1;
	public static final int POR_TIPO_RISCO = 2;
	
	private Risco risco;

	public RiscoCaminhador( FiltroDeNavegador filtro ) {

		setFiltro( filtro );
		inicializaVisao();
	}

	public RiscoCaminhador( Conexao conexao, FiltroDeNavegador filtro ) throws QtSQLException {

		super.setConexao( conexao );
		setFiltro( filtro );
		inicializaVisao();
	}

	private void inicializaVisao() {

		setNmVisao( "PCMSO.RISCOS" );
	}

	protected void carregaCampos() throws QtSQLException {

		risco = new Risco();

		if( isRecordAvailable() ) {
			risco.setIdRisco( query.getInteger( "ID_RISCO" ) );
			risco.setTpRisco( query.getString( "TP_RISCO" ) );
			risco.setDsRisco( query.getString( "DS_RISCO" ) );
		}
	}

	/** Método que retorna uma tabela básica a que se refere o caminhador */
	public TabelaBasica getTabelaBasica() {

		return this.risco;
	}

	/** Método que retorna a classe Risco relacionada ao caminhador */
	public Risco getRisco() {

		return this.risco;
	}

	public void ativaVisao() throws QtSQLException {

		if(  super.getOrdemDeNavegacao() == RiscoCaminhador.POR_ORDEM_DEFAULT ) {
			super.ativaCaminhador( "order by ID_RISCO" );
		} else if(  super.getOrdemDeNavegacao() == RiscoCaminhador.POR_CODIGO ) {
			super.ativaCaminhador( "order by ID_RISCO" );
		} else if(  super.getOrdemDeNavegacao() == RiscoCaminhador.POR_DESCRICAO ) {
			super.ativaCaminhador( "order by DS_RISCO" );
		} else if(  super.getOrdemDeNavegacao() == RiscoCaminhador.POR_TIPO_RISCO ) {
			super.ativaCaminhador( "order by TP_RISCO, DS_RISCO" );
	
		} else {
			throw new QtSQLException( "RiscoCaminhador - Ordem de Navegação não definida!" );
		}
	}

	public String getCamposIndice( int qualIndice ) {
		return getDsIndice( qualIndice );
	}

	public String getCamposChave() {
		return getDsChave();
	}

	/**
	 * Método estático que devolve os campos que compõe a chave da tabela;
	 * @return campos que compõe a chave.
	 */
	public static String getDsChave() {
		return "ID_RISCO";
	}

	/**
	 * Método estático que devolve os campos que fazem parte de uma dada ordem
	 * de navegação.
	 * @param qualIndice indica a ordem de navegação desejada.
	 * @return campos que compõe a ordem de navegação.
	 */
	public static String getDsIndice( int qualIndice ) {

		switch( qualIndice ) {

			case POR_ORDEM_DEFAULT: return "ID_RISCO";
			case POR_CODIGO: return "ID_RISCO";
			case POR_DESCRICAO: return "DS_RISCO";
			case POR_TIPO_RISCO: return "TP_RISCO, DS_RISCO";
		}
		return null;
	}
}