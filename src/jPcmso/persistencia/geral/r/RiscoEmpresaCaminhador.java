package jPcmso.persistencia.geral.r;

import quatro.persistencia.Caminhador;
import quatro.persistencia.FiltroDeNavegador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.util.QtData;

 /**
 *
 * Classe Caminhador para RiscoEmpresa
 *
 * @author Samuel Antonio Klein
 * @see Caminhador
 * @see FiltroDeNavegador
 * @see RiscoEmpresaNavegador
 * @see RiscoEmpresaPersistor
 * @see RiscoEmpresaLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 public class RiscoEmpresaCaminhador extends Caminhador {

	public static final int POR_ORDEM_DEFAULT = -1;
	
	private RiscoEmpresa riscoEmpresa;

	public RiscoEmpresaCaminhador( FiltroDeNavegador filtro ) {

		setFiltro( filtro );
		inicializaVisao();
	}

	public RiscoEmpresaCaminhador( Conexao conexao, FiltroDeNavegador filtro ) throws QtSQLException {

		super.setConexao( conexao );
		setFiltro( filtro );
		inicializaVisao();
	}

	private void inicializaVisao() {

		setNmVisao( "PCMSO.RISCOS_EMPRESA" );
	}

	protected void carregaCampos() throws QtSQLException {

		riscoEmpresa = new RiscoEmpresa();

		if( isRecordAvailable() ) {
			riscoEmpresa.setIdRisco( query.getInteger( "ID_RISCO" ) );
			riscoEmpresa.setCdEmpresa( query.getString( "CD_EMPRESA" ) );
			riscoEmpresa.setDtInclusao(  QtData.criaQtData( query.getTimestamp( "DT_INCLUSAO" ) ) );
			riscoEmpresa.setDtExclusao(  QtData.criaQtData( query.getTimestamp( "DT_EXCLUSAO" ) ) );
		}
	}

	/** M�todo que retorna uma tabela b�sica a que se refere o caminhador */
	public TabelaBasica getTabelaBasica() {

		return this.riscoEmpresa;
	}

	/** M�todo que retorna a classe RiscoEmpresa relacionada ao caminhador */
	public RiscoEmpresa getRiscoEmpresa() {

		return this.riscoEmpresa;
	}

	public void ativaVisao() throws QtSQLException {

		if(  super.getOrdemDeNavegacao() == RiscoEmpresaCaminhador.POR_ORDEM_DEFAULT ) {
			super.ativaCaminhador( "order by ID_RISCO, CD_EMPRESA" );
	
		} else {
			throw new QtSQLException( "RiscoEmpresaCaminhador - Ordem de Navega��o n�o definida!" );
		}
	}

	public String getCamposIndice( int qualIndice ) {
		return getDsIndice( qualIndice );
	}

	public String getCamposChave() {
		return getDsChave();
	}

	/**
	 * M�todo est�tico que devolve os campos que comp�e a chave da tabela;
	 * @return campos que comp�e a chave.
	 */
	public static String getDsChave() {
		return "ID_RISCO, CD_EMPRESA";
	}

	/**
	 * M�todo est�tico que devolve os campos que fazem parte de uma dada ordem
	 * de navega��o.
	 * @param qualIndice indica a ordem de navega��o desejada.
	 * @return campos que comp�e a ordem de navega��o.
	 */
	public static String getDsIndice( int qualIndice ) {

		switch( qualIndice ) {

			case POR_ORDEM_DEFAULT: return "ID_RISCO, CD_EMPRESA";
		}
		return null;
	}
}