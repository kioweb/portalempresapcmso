package jPcmso.persistencia.geral.r;

import quatro.persistencia.FiltroDeNavegador;
import quatro.persistencia.Navegador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.sql.Query;
import quatro.util.QtData;

/**
 *
 * Classe navegador para PCMSO.RESPONSAVEL_REGISTRO_AMBIENTAL
 *
 * @author Samuel Antonio Klein
 * @see Navegador
 * @see ResponsavelRegistroAmbiental
 * @see ResponsavelRegistroAmbientalPersistor
 * @see ResponsavelRegistroAmbientalLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:31 - importado por Samuel Antonio Klein
 *
 */
 public class ResponsavelRegistroAmbientalNavegador extends Navegador {

	
	/** Inst�ncia de ResponsavelRegistroAmbiental para manipula��o pelo navegador */
	private ResponsavelRegistroAmbiental responsavelRegistroAmbiental;

	/** Construtor b�sico */
	public ResponsavelRegistroAmbientalNavegador()
	  throws QtSQLException { 

		responsavelRegistroAmbiental = new ResponsavelRegistroAmbiental();
		primeiro();
	}

	/** Construtor b�sico que recebe uma conex�o */
	public ResponsavelRegistroAmbientalNavegador( Conexao conexao )
	  throws QtSQLException { 

		super.setConexao( conexao ); 
		responsavelRegistroAmbiental = new ResponsavelRegistroAmbiental();
		primeiro();
	}

	/**
	 * Construtor que recebe uma inst�ncia de ResponsavelRegistroAmbientalFiltro e posiciona
	 * no primeiro registro que satisfaz suas condi��es 
	 */
	public ResponsavelRegistroAmbientalNavegador( FiltroDeNavegador filtro ) throws QtSQLException  {
		super.setFiltro( filtro );
		this.responsavelRegistroAmbiental = new ResponsavelRegistroAmbiental();
		primeiro();
	}

	/** Construtor que recebe as chaves da tabela PCMSO.RESPONSAVEL_REGISTRO_AMBIENTAL
	 * @param cdBeneficiarioCartao Codigo de Beneficiario 
	 * @param sqPerfil sequencia perfil
	 * @param sqResponsavel sequencia Responsavel
	 */
	public ResponsavelRegistroAmbientalNavegador( String cdBeneficiarioCartao, int sqPerfil, int sqResponsavel )
	  throws QtSQLException { 

		responsavelRegistroAmbiental = new ResponsavelRegistroAmbiental();
		this.responsavelRegistroAmbiental.setCdBeneficiarioCartao( cdBeneficiarioCartao );
		this.responsavelRegistroAmbiental.setSqPerfil( new Integer( sqPerfil ) );
		this.responsavelRegistroAmbiental.setSqResponsavel( new Integer( sqResponsavel ) );
		
		busca();
	}

	/** Construtor que recebe  uma conex�o e as chaves da tabela PCMSO.RESPONSAVEL_REGISTRO_AMBIENTAL
	 * @param cdBeneficiarioCartao Codigo de Beneficiario 
	 * @param sqPerfil sequencia perfil
	 * @param sqResponsavel sequencia Responsavel
	 * @param conexao Conexao com o banco de dados
	 */
	public ResponsavelRegistroAmbientalNavegador( Conexao conexao, String cdBeneficiarioCartao, int sqPerfil, int sqResponsavel )
	  throws QtSQLException { 

		super.setConexao( conexao ); 
		responsavelRegistroAmbiental = new ResponsavelRegistroAmbiental();
		this.responsavelRegistroAmbiental.setCdBeneficiarioCartao( cdBeneficiarioCartao );
		this.responsavelRegistroAmbiental.setSqPerfil( new Integer( sqPerfil ) );
		this.responsavelRegistroAmbiental.setSqResponsavel( new Integer( sqResponsavel ) );
		
		busca();
	}

	/** M�todo que retorna uma tabela b�sica a que se refere o navegador */
	public TabelaBasica getTabelaBasica() {

		return this.responsavelRegistroAmbiental;
	}

	/** M�todo que retorna a classe ResponsavelRegistroAmbiental relacionada ao navegador */
	public ResponsavelRegistroAmbiental getResponsavelRegistroAmbiental() {

		return this.responsavelRegistroAmbiental;
	}

	 /** M�todo que move para o primeiro registro da tabela */
	 public boolean primeiro() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select CD_BENEFICIARIO_CARTAO, SQ_PERFIL, SQ_RESPONSAVEL" );
			query.addSQL( "  from PCMSO.RESPONSAVEL_REGISTRO_AMBIENTAL" );
			query.addSQL( super.getCondicaoDeFiltro( FiltroDeNavegador.COM_WHERE ) );
			query.addSQL( "  order by CD_BENEFICIARIO_CARTAO, SQ_PERFIL, SQ_RESPONSAVEL" );
			query.addSQL( "  limit 1 " );
			
			query.executeQuery();

			
			if( query.isEmpty() ) {

				super.setQueryStates( false, true, true );
			} else {
				this.responsavelRegistroAmbiental = new ResponsavelRegistroAmbiental();

				responsavelRegistroAmbiental.setCdBeneficiarioCartao( query.getString( "CD_BENEFICIARIO_CARTAO" ) );
				responsavelRegistroAmbiental.setSqPerfil( query.getInteger( "SQ_PERFIL" ) );
				responsavelRegistroAmbiental.setSqResponsavel( query.getInteger( "SQ_RESPONSAVEL" ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o registro anterior na tabela */
	 public boolean anterior() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select CD_BENEFICIARIO_CARTAO, SQ_PERFIL, SQ_RESPONSAVEL" );
			query.addSQL( "  from PCMSO.RESPONSAVEL_REGISTRO_AMBIENTAL" );
			query.addSQL( "  where ( ( CD_BENEFICIARIO_CARTAO = :prm1CdBeneficiarioCartao and  SQ_PERFIL = :prm2SqPerfil and  SQ_RESPONSAVEL < :prm3SqResponsavel )" );
			query.addSQL( "  or (  CD_BENEFICIARIO_CARTAO = :prm1CdBeneficiarioCartao and  SQ_PERFIL < :prm2SqPerfil )" );
			query.addSQL( "  or (  CD_BENEFICIARIO_CARTAO < :prm1CdBeneficiarioCartao ) )" );
			query.addSQL( super.getCondicaoDeFiltro() );
			query.addSQL( "  order by CD_BENEFICIARIO_CARTAO desc, SQ_PERFIL desc, SQ_RESPONSAVEL desc" );
			query.addSQL( "  limit 1 " );

			query.setParameter( "prm1CdBeneficiarioCartao", responsavelRegistroAmbiental.getCdBeneficiarioCartao() );
			query.setParameter( "prm2SqPerfil", responsavelRegistroAmbiental.getSqPerfil() );
			query.setParameter( "prm3SqResponsavel", responsavelRegistroAmbiental.getSqResponsavel() );
		
			query.executeQuery();

			
			if( query.isEmpty() ) {

				super.setFound( false );
				super.setBof( true );
			} else {
				this.responsavelRegistroAmbiental = new ResponsavelRegistroAmbiental();

				responsavelRegistroAmbiental.setCdBeneficiarioCartao( query.getString( "CD_BENEFICIARIO_CARTAO" ) );
				responsavelRegistroAmbiental.setSqPerfil( query.getInteger( "SQ_PERFIL" ) );
				responsavelRegistroAmbiental.setSqResponsavel( query.getInteger( "SQ_RESPONSAVEL" ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o proximo registro da tabela */
	 public boolean proximo() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select CD_BENEFICIARIO_CARTAO, SQ_PERFIL, SQ_RESPONSAVEL" );
			query.addSQL( "  from PCMSO.RESPONSAVEL_REGISTRO_AMBIENTAL" );
			query.addSQL( "  where ( (  CD_BENEFICIARIO_CARTAO = :prm1CdBeneficiarioCartao and  SQ_PERFIL = :prm2SqPerfil and  SQ_RESPONSAVEL > :prm3SqResponsavel )" );
			query.addSQL( "  or (  CD_BENEFICIARIO_CARTAO = :prm1CdBeneficiarioCartao and  SQ_PERFIL > :prm2SqPerfil )" );
			query.addSQL( "  or (  CD_BENEFICIARIO_CARTAO > :prm1CdBeneficiarioCartao ) )" );
			query.addSQL( super.getCondicaoDeFiltro() );
			query.addSQL( "  order by CD_BENEFICIARIO_CARTAO, SQ_PERFIL, SQ_RESPONSAVEL" );
			query.addSQL( "  limit 1 " );

			query.setParameter( "prm1CdBeneficiarioCartao", responsavelRegistroAmbiental.getCdBeneficiarioCartao() );
			query.setParameter( "prm2SqPerfil", responsavelRegistroAmbiental.getSqPerfil() );
			query.setParameter( "prm3SqResponsavel", responsavelRegistroAmbiental.getSqResponsavel() );
		
			query.executeQuery();

			
			if( query.isEmpty() ) {

				super.setFound( false );
				super.setEof( true );
			} else {
				this.responsavelRegistroAmbiental = new ResponsavelRegistroAmbiental();

				responsavelRegistroAmbiental.setCdBeneficiarioCartao( query.getString( "CD_BENEFICIARIO_CARTAO" ) );
				responsavelRegistroAmbiental.setSqPerfil( query.getInteger( "SQ_PERFIL" ) );
				responsavelRegistroAmbiental.setSqResponsavel( query.getInteger( "SQ_RESPONSAVEL" ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o �ltimo registro da tabela */
	 public boolean ultimo() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select CD_BENEFICIARIO_CARTAO, SQ_PERFIL, SQ_RESPONSAVEL" );
			query.addSQL( "  from PCMSO.RESPONSAVEL_REGISTRO_AMBIENTAL" );
			query.addSQL( super.getCondicaoDeFiltro( FiltroDeNavegador.COM_WHERE ) 	);
			query.addSQL( "  order by CD_BENEFICIARIO_CARTAO desc, SQ_PERFIL desc, SQ_RESPONSAVEL desc" );
			query.addSQL( "  limit 1 " );

			query.executeQuery();

			
			if( query.isEmpty() ) {

				super.setQueryStates( false, true, true );
			} else {
				this.responsavelRegistroAmbiental = new ResponsavelRegistroAmbiental();

				responsavelRegistroAmbiental.setCdBeneficiarioCartao( query.getString( "CD_BENEFICIARIO_CARTAO" ) );
				responsavelRegistroAmbiental.setSqPerfil( query.getInteger( "SQ_PERFIL" ) );
				responsavelRegistroAmbiental.setSqResponsavel( query.getInteger( "SQ_RESPONSAVEL" ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	/**
	 * M�todo para buscar e preencher uma tupla de uma tabela
	 * @param tabelaBasica Recebe uma tabela basica com os atributos chave preenchidos
	 */
	public boolean busca( TabelaBasica tabelaBasica ) throws QtSQLException {

		if( !( tabelaBasica instanceof ResponsavelRegistroAmbiental ) )
			throw new QtSQLException( "Esperava um objeto ResponsavelRegistroAmbiental!" );

		this.responsavelRegistroAmbiental = (ResponsavelRegistroAmbiental) tabelaBasica;
		return busca();

	}

	/**
	 * m�todo interno para buscar e preencher uma tupla de uma tabela
	 * Utiliza a Inst�ncia de ResponsavelRegistroAmbiental da classe para fazer a busca
	 */
	private boolean busca() throws QtSQLException {

		ResponsavelRegistroAmbientalLocalizador localizador = new ResponsavelRegistroAmbientalLocalizador( getConexao(), this.responsavelRegistroAmbiental );

		if( localizador.isEmpty() ) {

			super.setQueryStates( false, true, true );
			return false;

		} else {

			super.setQueryStates( true, false, false );
			return true;
		}
	}
}