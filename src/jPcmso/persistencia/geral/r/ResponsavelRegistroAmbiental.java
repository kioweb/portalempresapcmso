package jPcmso.persistencia.geral.r;

import quatro.persistencia.TabelaBasica;
import quatro.smartDB.IgnoredField;
import quatro.smartDB.PrimaryKey;
import quatro.smartDB.Table;
import quatro.util.QtData;

/**
 *
 * Classe b�sica para PCMSO.RESPONSAVEL_REGISTRO_AMBIENTAL
 *
 * Tabela de Beneficiarios CBO
 * 
 * @author Samuel Antonio Klein
 * @see ResponsavelRegistroAmbientalNavegador
 * @see ResponsavelRegistroAmbientalPersistor
 * @see ResponsavelRegistroAmbientalLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:31 - importado por Samuel Antonio Klein
 *
 */
 @Table(name="PCMSO.RESPONSAVEL_REGISTRO_AMBIENTAL")
 public class ResponsavelRegistroAmbiental implements TabelaBasica {

	@IgnoredField
	private static final long serialVersionUID = 18706186065510211L;

	/** Codigo de Beneficiario  (chave-char(12)) */
	@PrimaryKey
	private String cdBeneficiarioCartao;
	/** sequencia perfil (chave) */
	@PrimaryKey
	private Integer sqPerfil;
	/** sequencia Responsavel (chave) */
	@PrimaryKey
	private Integer sqResponsavel;
	/** data de Inicio */
	private QtData dtInicio;
	/** data Final */
	private QtData dtFinal;
	/** N�mero de Identifica��o do Trabalhador com 11 (onze) caracteres num�ricos, no formato XXX.XXXXX.XX-X.  O NIT corresponde ao n�mero do PIS/PASEP/CI sendo que, no caso de Contribuinte Individual (CI), pode ser utilizado o n�mero de inscri��o no Sistema �nico de Sa�de (SUS) ou na Previd�ncia Social. */
	private Integer nrNitRespresentante;
	/** Registro Conselho de Classe (char(2)) */
	private String cdRegistroConselhoClasse;
	/** Nome do Profissional Legalmente Habilitado (varchar(40)) */
	private String dsNomeProfissional;

	/**
	 * Alimenta Codigo de Beneficiario 
	 * @param cdBeneficiarioCartao Codigo de Beneficiario 
	 */ 
	public void setCdBeneficiarioCartao( String cdBeneficiarioCartao ) {
		this.cdBeneficiarioCartao = cdBeneficiarioCartao;
	}

	/**
	 * Retorna Codigo de Beneficiario 
	 * @return Codigo de Beneficiario 
	 */
	public String getCdBeneficiarioCartao() {
		return this.cdBeneficiarioCartao;
	}

	/**
	 * Alimenta sequencia perfil
	 * @param sqPerfil sequencia perfil
	 */ 
	public void setSqPerfil( Integer sqPerfil ) {
		this.sqPerfil = sqPerfil;
	}

	/**
	 * Retorna sequencia perfil
	 * @return sequencia perfil
	 */
	public Integer getSqPerfil() {
		return this.sqPerfil;
	}

	/**
	 * Alimenta sequencia Responsavel
	 * @param sqResponsavel sequencia Responsavel
	 */ 
	public void setSqResponsavel( Integer sqResponsavel ) {
		this.sqResponsavel = sqResponsavel;
	}

	/**
	 * Retorna sequencia Responsavel
	 * @return sequencia Responsavel
	 */
	public Integer getSqResponsavel() {
		return this.sqResponsavel;
	}

	/**
	 * Alimenta data de Inicio
	 * @param dtInicio data de Inicio
	 */ 
	public void setDtInicio( QtData dtInicio ) {
		this.dtInicio = dtInicio;
	}

	/**
	 * Retorna data de Inicio
	 * @return data de Inicio
	 */
	public QtData getDtInicio() {
		return this.dtInicio;
	}

	/**
	 * Alimenta data Final
	 * @param dtFinal data Final
	 */ 
	public void setDtFinal( QtData dtFinal ) {
		this.dtFinal = dtFinal;
	}

	/**
	 * Retorna data Final
	 * @return data Final
	 */
	public QtData getDtFinal() {
		return this.dtFinal;
	}

	/**
	 * Alimenta N�mero de Identifica��o do Trabalhador com 11 (onze) caracteres num�ricos, no formato XXX.XXXXX.XX-X.  O NIT corresponde ao n�mero do PIS/PASEP/CI sendo que, no caso de Contribuinte Individual (CI), pode ser utilizado o n�mero de inscri��o no Sistema �nico de Sa�de (SUS) ou na Previd�ncia Social.
	 * @param nrNitRespresentante N�mero de Identifica��o do Trabalhador com 11 (onze) caracteres num�ricos, no formato XXX.XXXXX.XX-X.  O NIT corresponde ao n�mero do PIS/PASEP/CI sendo que, no caso de Contribuinte Individual (CI), pode ser utilizado o n�mero de inscri��o no Sistema �nico de Sa�de (SUS) ou na Previd�ncia Social.
	 */ 
	public void setNrNitRespresentante( Integer nrNitRespresentante ) {
		this.nrNitRespresentante = nrNitRespresentante;
	}

	/**
	 * Retorna N�mero de Identifica��o do Trabalhador com 11 (onze) caracteres num�ricos, no formato XXX.XXXXX.XX-X.  O NIT corresponde ao n�mero do PIS/PASEP/CI sendo que, no caso de Contribuinte Individual (CI), pode ser utilizado o n�mero de inscri��o no Sistema �nico de Sa�de (SUS) ou na Previd�ncia Social.
	 * @return N�mero de Identifica��o do Trabalhador com 11 (onze) caracteres num�ricos, no formato XXX.XXXXX.XX-X.  O NIT corresponde ao n�mero do PIS/PASEP/CI sendo que, no caso de Contribuinte Individual (CI), pode ser utilizado o n�mero de inscri��o no Sistema �nico de Sa�de (SUS) ou na Previd�ncia Social.
	 */
	public Integer getNrNitRespresentante() {
		return this.nrNitRespresentante;
	}

	/**
	 * Alimenta Registro Conselho de Classe
	 * @param cdRegistroConselhoClasse Registro Conselho de Classe
	 */ 
	public void setCdRegistroConselhoClasse( String cdRegistroConselhoClasse ) {
		this.cdRegistroConselhoClasse = cdRegistroConselhoClasse;
	}

	/**
	 * Retorna Registro Conselho de Classe
	 * @return Registro Conselho de Classe
	 */
	public String getCdRegistroConselhoClasse() {
		return this.cdRegistroConselhoClasse;
	}

	/**
	 * Alimenta Nome do Profissional Legalmente Habilitado
	 * @param dsNomeProfissional Nome do Profissional Legalmente Habilitado
	 */ 
	public void setDsNomeProfissional( String dsNomeProfissional ) {
		this.dsNomeProfissional = dsNomeProfissional;
	}

	/**
	 * Retorna Nome do Profissional Legalmente Habilitado
	 * @return Nome do Profissional Legalmente Habilitado
	 */
	public String getDsNomeProfissional() {
		return this.dsNomeProfissional;
	}

}