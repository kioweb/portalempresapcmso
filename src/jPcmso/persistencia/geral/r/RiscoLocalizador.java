package jPcmso.persistencia.geral.r;

import quatro.persistencia.Localizador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.QtSQLException;
import quatro.sql.Query;
import quatro.sql.Conexao;
import quatro.sql.PoolDeConexoes;
import quatro.sql.ConexaoParaPoolDeConexoes;

/**
 *
 * Classe localizador para PCMSO.RISCOS
 *
 * @author ronei@unimed-uau.com.br
 * @see Localizador
 * @see Risco
 * @see RiscoNavegador
 * @see RiscoPersistor
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 14/12/2017 15:26 - importado por ronei@unimed-uau.com.br
 *
 */
 public class RiscoLocalizador extends Localizador {

	/** Instância de Risco para manipulação pelo localizador */
	private Risco risco;

	/** Construtor que recebe uma conexão e um objeto Risco completo */
	public RiscoLocalizador( Conexao conexao, Risco risco ) throws QtSQLException {

		super.setConexao( conexao );

		this.risco = risco;
		busca();
	}

	/** Construtor que recebe um objeto Risco completo */
	public RiscoLocalizador( Risco risco ) throws QtSQLException {

		this.risco = risco;
		busca();
	}

	/** 
	 * Construtor que recebe uma conexão e a chave da tabela PCMSO.RISCOS
	 * @param idRisco Identificador do Risco
	 * @param conexao Conexao com o banco de dados
	 */
	public RiscoLocalizador( Conexao conexao, int idRisco ) throws QtSQLException { 

		super.setConexao( conexao ); 
		risco = new Risco();
		this.risco.setIdRisco( new Integer( idRisco ) );
		
		busca();
	}

	/** 
	 * Construtor que recebe a chave da tabela PCMSO.RISCOS
	 * @param idRisco Identificador do Risco
	 */
	public RiscoLocalizador( int idRisco ) throws QtSQLException { 

		risco = new Risco();
		this.risco.setIdRisco( new Integer( idRisco ) );
		
		busca();
	}

	public static boolean existe( Conexao cnx, int idRisco ) throws QtSQLException {

		Query q = new Query( cnx );

		try {
			q.setSQL( "select count(*) from PCMSO.RISCOS" );
			q.addSQL( " where ID_RISCO = :prm1IdRisco" );

			q.setParameter( "prm1IdRisco", idRisco );
			q.executeQuery();

			return q.getInt( 1 ) > 0;
		} finally {
			q.liberaRecursos();
		}
	}

	public static boolean existe( int idRisco ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return existe( cnx, idRisco );
		} finally {
			cnx.libera();
		}
	}

	public static boolean existe( Conexao cnx, Risco risco ) throws QtSQLException {

		return existe( cnx, risco.getIdRisco() );
	}

	public static boolean existe( Risco risco ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return existe( cnx, risco.getIdRisco() );
		} finally {
			cnx.libera();
		}
	}

	public static Risco buscaRisco( Conexao cnx, int idRisco ) throws QtSQLException {

		Query q = new Query( cnx );

		try {
			q.setSQL( "select * from PCMSO.RISCOS" );
			q.addSQL( " where ID_RISCO = :prm1IdRisco" );

			q.setParameter( "prm1IdRisco", idRisco );
			q.executeQuery();

			if( q.isEmpty() ) {
				return null;
			} else {
				Risco risco = new Risco();
				buscaCampos( risco, q );
				return risco;
			}
		} finally {
			q.liberaRecursos();
		}
	}

	public static Risco buscaRisco( int idRisco ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return buscaRisco( cnx, idRisco );
		} finally { 
			cnx.libera();
		}
	}

	/** 
	 * Construtor que recebe uma conexão e a chave da tabela PCMSO.RISCOS
	 * @param idRisco Identificador do Risco
	 * @param conexao Conexao com o banco de dados
	 */
	public RiscoLocalizador( Conexao conexao, Integer idRisco ) throws QtSQLException {

		super.setConexao( conexao ); 
		risco = new Risco();
		this.risco.setIdRisco( idRisco );
		
		busca();
	}

	/** 
	 * Construtor que recebe a chave da tabela PCMSO.RISCOS
	 * @param idRisco Identificador do Risco
	 */
	public RiscoLocalizador( Integer idRisco ) throws QtSQLException {

		risco = new Risco();
		this.risco.setIdRisco( idRisco );
		
		busca();
	}

	public static void buscaCampos( Risco risco, Query query ) throws QtSQLException {

		risco.setIdRisco( query.getInteger( "ID_RISCO" ) );
		risco.setTpRisco( query.getString( "TP_RISCO" ) );
		risco.setDsRisco( query.getString( "DS_RISCO" ) );
	}

	/** Método que retorna uma tabela básica a que se refere o localizador */
	public TabelaBasica getTabelaBasica() {

		return this.risco;
	}

	/** Método que retorna a classe Risco relacionada ao localizador */
	public Risco getRisco() {

		return this.risco;
	}

	/**
	 * Método para buscar e preencher uma tupla de uma tabela
	 * @param tabelaBasica Recebe uma tabela basica com os atributos chave preenchidos
	 */
	public boolean busca( TabelaBasica tabelaBasica ) throws QtSQLException {

		if( !( tabelaBasica instanceof Risco ) )
			throw new QtSQLException( "Esperava um objeto Risco!" );

		this.risco = (Risco) tabelaBasica;
		return busca();

	}

	/**
	 * Método privado para buscar e preencher uma tupla de uma tabela
	 * Este método busca os dados da instância de Risco atual
	 */
	private boolean busca() throws QtSQLException {

		boolean ret = false;

		super.estabeleceConexao();
		Query query = super.criaNovaQuery();

		try {

			query.setSQL( "select * from PCMSO.RISCOS" );
			query.addSQL( " where ID_RISCO = :prm1IdRisco" );

			query.setParameter( "prm1IdRisco", risco.getIdRisco() );
			query.executeQuery();

			if( query.isEmpty() ) {
				super.setEmpty( true );
				risco = null;
			} else {
				super.setEmpty( false );
				
				risco.setIdRisco( query.getInteger( "ID_RISCO" ) );
				risco.setTpRisco( query.getString( "TP_RISCO" ) );
				risco.setDsRisco( query.getString( "DS_RISCO" ) );
				ret = true;
			}
		} finally {
			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}
 }