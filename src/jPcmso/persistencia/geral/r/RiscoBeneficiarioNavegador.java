package jPcmso.persistencia.geral.r;

import quatro.persistencia.FiltroDeNavegador;
import quatro.persistencia.Navegador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.sql.Query;
import quatro.util.QtData;

/**
 *
 * Classe navegador para PCMSO.RISCOS_BENEFICIARIO
 *
 * @author Samuel Antonio Klein
 * @see Navegador
 * @see RiscoBeneficiario
 * @see RiscoBeneficiarioPersistor
 * @see RiscoBeneficiarioLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 public class RiscoBeneficiarioNavegador extends Navegador {

	
	/** Inst�ncia de RiscoBeneficiario para manipula��o pelo navegador */
	private RiscoBeneficiario riscoBeneficiario;

	/** Construtor b�sico */
	public RiscoBeneficiarioNavegador()
	  throws QtSQLException { 

		riscoBeneficiario = new RiscoBeneficiario();
		primeiro();
	}

	/** Construtor b�sico que recebe uma conex�o */
	public RiscoBeneficiarioNavegador( Conexao conexao )
	  throws QtSQLException { 

		super.setConexao( conexao ); 
		riscoBeneficiario = new RiscoBeneficiario();
		primeiro();
	}

	/**
	 * Construtor que recebe uma inst�ncia de RiscoBeneficiarioFiltro e posiciona
	 * no primeiro registro que satisfaz suas condi��es 
	 */
	public RiscoBeneficiarioNavegador( FiltroDeNavegador filtro ) throws QtSQLException  {
		super.setFiltro( filtro );
		this.riscoBeneficiario = new RiscoBeneficiario();
		primeiro();
	}

	/** Construtor que recebe as chaves da tabela PCMSO.RISCOS_BENEFICIARIO
	 * @param cdBeneficiarioCartao Codigo do beneficiario
	 * @param idRisco Identificador do Risco
	 */
	public RiscoBeneficiarioNavegador( String cdBeneficiarioCartao, int idRisco )
	  throws QtSQLException { 

		riscoBeneficiario = new RiscoBeneficiario();
		this.riscoBeneficiario.setCdBeneficiarioCartao( cdBeneficiarioCartao );
		this.riscoBeneficiario.setIdRisco( new Integer( idRisco ) );
		
		busca();
	}

	/** Construtor que recebe  uma conex�o e as chaves da tabela PCMSO.RISCOS_BENEFICIARIO
	 * @param cdBeneficiarioCartao Codigo do beneficiario
	 * @param idRisco Identificador do Risco
	 * @param conexao Conexao com o banco de dados
	 */
	public RiscoBeneficiarioNavegador( Conexao conexao, String cdBeneficiarioCartao, int idRisco )
	  throws QtSQLException { 

		super.setConexao( conexao ); 
		riscoBeneficiario = new RiscoBeneficiario();
		this.riscoBeneficiario.setCdBeneficiarioCartao( cdBeneficiarioCartao );
		this.riscoBeneficiario.setIdRisco( new Integer( idRisco ) );
		
		busca();
	}

	/** M�todo que retorna uma tabela b�sica a que se refere o navegador */
	public TabelaBasica getTabelaBasica() {

		return this.riscoBeneficiario;
	}

	/** M�todo que retorna a classe RiscoBeneficiario relacionada ao navegador */
	public RiscoBeneficiario getRiscoBeneficiario() {

		return this.riscoBeneficiario;
	}

	 /** M�todo que move para o primeiro registro da tabela */
	 public boolean primeiro() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select CD_BENEFICIARIO_CARTAO, ID_RISCO" );
			query.addSQL( "  from PCMSO.RISCOS_BENEFICIARIO" );
			query.addSQL( super.getCondicaoDeFiltro( FiltroDeNavegador.COM_WHERE ) );
			query.addSQL( "  order by CD_BENEFICIARIO_CARTAO, ID_RISCO" );
			query.addSQL( "  limit 1 " );
			
			query.executeQuery();

			
			if( query.isEmpty() ) {

				super.setQueryStates( false, true, true );
			} else {
				this.riscoBeneficiario = new RiscoBeneficiario();

				riscoBeneficiario.setCdBeneficiarioCartao( query.getString( "CD_BENEFICIARIO_CARTAO" ) );
				riscoBeneficiario.setIdRisco( query.getInteger( "ID_RISCO" ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o registro anterior na tabela */
	 public boolean anterior() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select CD_BENEFICIARIO_CARTAO, ID_RISCO" );
			query.addSQL( "  from PCMSO.RISCOS_BENEFICIARIO" );
			query.addSQL( "  where ( ( CD_BENEFICIARIO_CARTAO = :prm1CdBeneficiarioCartao and  ID_RISCO < :prm2IdRisco )" );
			query.addSQL( "  or (  CD_BENEFICIARIO_CARTAO < :prm1CdBeneficiarioCartao ) )" );
			query.addSQL( super.getCondicaoDeFiltro() );
			query.addSQL( "  order by CD_BENEFICIARIO_CARTAO desc, ID_RISCO desc" );
			query.addSQL( "  limit 1 " );

			query.setParameter( "prm1CdBeneficiarioCartao", riscoBeneficiario.getCdBeneficiarioCartao() );
			query.setParameter( "prm2IdRisco", riscoBeneficiario.getIdRisco() );
		
			query.executeQuery();

			
			if( query.isEmpty() ) {

				super.setFound( false );
				super.setBof( true );
			} else {
				this.riscoBeneficiario = new RiscoBeneficiario();

				riscoBeneficiario.setCdBeneficiarioCartao( query.getString( "CD_BENEFICIARIO_CARTAO" ) );
				riscoBeneficiario.setIdRisco( query.getInteger( "ID_RISCO" ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o proximo registro da tabela */
	 public boolean proximo() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select CD_BENEFICIARIO_CARTAO, ID_RISCO" );
			query.addSQL( "  from PCMSO.RISCOS_BENEFICIARIO" );
			query.addSQL( "  where ( (  CD_BENEFICIARIO_CARTAO = :prm1CdBeneficiarioCartao and  ID_RISCO > :prm2IdRisco )" );
			query.addSQL( "  or (  CD_BENEFICIARIO_CARTAO > :prm1CdBeneficiarioCartao ) )" );
			query.addSQL( super.getCondicaoDeFiltro() );
			query.addSQL( "  order by CD_BENEFICIARIO_CARTAO, ID_RISCO" );
			query.addSQL( "  limit 1 " );

			query.setParameter( "prm1CdBeneficiarioCartao", riscoBeneficiario.getCdBeneficiarioCartao() );
			query.setParameter( "prm2IdRisco", riscoBeneficiario.getIdRisco() );
		
			query.executeQuery();

			
			if( query.isEmpty() ) {

				super.setFound( false );
				super.setEof( true );
			} else {
				this.riscoBeneficiario = new RiscoBeneficiario();

				riscoBeneficiario.setCdBeneficiarioCartao( query.getString( "CD_BENEFICIARIO_CARTAO" ) );
				riscoBeneficiario.setIdRisco( query.getInteger( "ID_RISCO" ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o �ltimo registro da tabela */
	 public boolean ultimo() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select CD_BENEFICIARIO_CARTAO, ID_RISCO" );
			query.addSQL( "  from PCMSO.RISCOS_BENEFICIARIO" );
			query.addSQL( super.getCondicaoDeFiltro( FiltroDeNavegador.COM_WHERE ) 	);
			query.addSQL( "  order by CD_BENEFICIARIO_CARTAO desc, ID_RISCO desc" );
			query.addSQL( "  limit 1 " );

			query.executeQuery();

			
			if( query.isEmpty() ) {

				super.setQueryStates( false, true, true );
			} else {
				this.riscoBeneficiario = new RiscoBeneficiario();

				riscoBeneficiario.setCdBeneficiarioCartao( query.getString( "CD_BENEFICIARIO_CARTAO" ) );
				riscoBeneficiario.setIdRisco( query.getInteger( "ID_RISCO" ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	/**
	 * M�todo para buscar e preencher uma tupla de uma tabela
	 * @param tabelaBasica Recebe uma tabela basica com os atributos chave preenchidos
	 */
	public boolean busca( TabelaBasica tabelaBasica ) throws QtSQLException {

		if( !( tabelaBasica instanceof RiscoBeneficiario ) )
			throw new QtSQLException( "Esperava um objeto RiscoBeneficiario!" );

		this.riscoBeneficiario = (RiscoBeneficiario) tabelaBasica;
		return busca();

	}

	/**
	 * m�todo interno para buscar e preencher uma tupla de uma tabela
	 * Utiliza a Inst�ncia de RiscoBeneficiario da classe para fazer a busca
	 */
	private boolean busca() throws QtSQLException {

		RiscoBeneficiarioLocalizador localizador = new RiscoBeneficiarioLocalizador( getConexao(), this.riscoBeneficiario );

		if( localizador.isEmpty() ) {

			super.setQueryStates( false, true, true );
			return false;

		} else {

			super.setQueryStates( true, false, false );
			return true;
		}
	}
}