package jPcmso.persistencia.geral.r;

import java.sql.PreparedStatement;

import quatro.persistencia.Persistor;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.util.QtException;
import quatro.util.QtData;

/**
 *
 * Classe persistor para PCMSO.RISCOS_EMPRESA
 *
 * @author Samuel Antonio Klein
 * @see Persistor
 * @see RiscoEmpresa
 * @see RiscoEmpresaNavegador
 * @see RiscoEmpresaLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 public class RiscoEmpresaPersistor extends Persistor {

	/** Inst�ncia de RiscoEmpresa para manipula��o pelo persistor */
	private RiscoEmpresa riscoEmpresa;

	/** Construtor gen�rico */
	public RiscoEmpresaPersistor() {

		this.riscoEmpresa = null;
	}

	/** Construtor gen�rico */
	public RiscoEmpresaPersistor( Conexao cnx ) throws QtSQLException {

		this.riscoEmpresa = null;
		super.setConexao( cnx );
	}

	/** Construtor que recebe uma inst�ncia de RiscoEmpresa */
	public RiscoEmpresaPersistor( RiscoEmpresa riscoEmpresa ) throws QtSQLException {

		if( riscoEmpresa == null ) 
			throw new QtSQLException( "Imposs�vel instanciar RiscoEmpresaPersistor porque est� apontando para um nulo!" );

		this.riscoEmpresa = riscoEmpresa;
	}

	/** Construtor que recebe uma conex�o e uma inst�ncia de RiscoEmpresa */
	public RiscoEmpresaPersistor( Conexao conexao, RiscoEmpresa riscoEmpresa ) throws QtSQLException {

		if( riscoEmpresa == null ) 
			throw new QtSQLException( "Imposs�vel instanciar RiscoEmpresaPersistor porque est� apontando para um nulo!" );

		setConexao( conexao );
		this.riscoEmpresa = riscoEmpresa;
	}

	/**
	 * Construtor que recebe as chaves da tabela PCMSO.RISCOS_EMPRESA
	 * @param idRisco Identificador do Risco
	 * @param cdEmpresa Codigo da empresa
	 */
	public RiscoEmpresaPersistor( int idRisco, String cdEmpresa )
	  throws QtSQLException { 

		busca( idRisco, cdEmpresa );
	}

	/**
	 * Construtor que recebe uma conex�o e as chaves da tabela PCMSO.RISCOS_EMPRESA
	 * @param idRisco Identificador do Risco
	 * @param cdEmpresa Codigo da empresa
	 * @param conexao Conexao com o banco de dados
	 */
	public RiscoEmpresaPersistor( Conexao conexao, int idRisco, String cdEmpresa )
	  throws QtSQLException { 

		super.setConexao( conexao );
		busca( idRisco, cdEmpresa );
	}

	/**
	 * M�todo interno para buscar e preencher uma tupla de uma tabela
	 * Utiliza a Inst�ncia de RiscoEmpresa para fazer a busca
	 * @param idRisco Identificador do Risco
	 * @param cdEmpresa Codigo da empresa
	 */
	private void busca( int idRisco, String cdEmpresa ) throws QtSQLException { 

		RiscoEmpresaLocalizador riscoEmpresaLocalizador = new RiscoEmpresaLocalizador( getConexao(), idRisco, cdEmpresa );

		if( riscoEmpresaLocalizador.isEmpty() ) {
			throw new QtSQLException( "Chaves da Tabela RiscoEmpresa n�o encontradas - idRisco: " + idRisco + " / cdEmpresa: " + cdEmpresa );
		}

		riscoEmpresa = (RiscoEmpresa ) riscoEmpresaLocalizador.getTabelaBasica();
	}

	/**
	 * Permite alimentar o Persistor com um novo registro
	 */
	public void setTabelaBasica( TabelaBasica riscoEmpresa ) throws QtException {

		if( riscoEmpresa instanceof RiscoEmpresa ) {
			this.riscoEmpresa = (RiscoEmpresa) riscoEmpresa;
		} else {
			throw new QtException( "Tabela B�sica fornecida tem que ser uma inst�ncia de RiscoEmpresa" );
		}
	}

	/**
	 * Permite acessar o registro corrente do persistor
	 */
	public TabelaBasica getTabelaBasica() throws QtException {

		return riscoEmpresa;
	}

	/**
	 * M�todo para inserir tupla na tabela
	 */
	public void insere() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "insert into PCMSO.RISCOS_EMPRESA" );
			cmd.append( "( ID_RISCO, CD_EMPRESA, DT_INCLUSAO, DT_EXCLUSAO )" );
			cmd.append( "values " );
			cmd.append( "( ?, ?, ?, ?)" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			ps.setObject( 1, riscoEmpresa.getIdRisco() );
			ps.setObject( 2, riscoEmpresa.getCdEmpresa() );
			ps.setObject( 3, QtData.getQtDataAsTimestamp( riscoEmpresa.getDtInclusao() ) );
			ps.setObject( 4, QtData.getQtDataAsTimestamp( riscoEmpresa.getDtExclusao() ) );
			
			ps.execute();

		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}

	/**
	 * M�todo para alterar tupla na tabela
	 */
	public void altera() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "update PCMSO.RISCOS_EMPRESA" );
			cmd.append( "  set ID_RISCO = ?, " );
			cmd.append( " CD_EMPRESA = ?, " );
			cmd.append( " DT_INCLUSAO = ?, " );
			cmd.append( " DT_EXCLUSAO = ?" );
			cmd.append( " where ID_RISCO = ? and CD_EMPRESA like ?" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			ps.setObject( 1, riscoEmpresa.getIdRisco() );
			ps.setObject( 2, riscoEmpresa.getCdEmpresa() );
			ps.setObject( 3, QtData.getQtDataAsTimestamp( riscoEmpresa.getDtInclusao() ) );
			ps.setObject( 4, QtData.getQtDataAsTimestamp( riscoEmpresa.getDtExclusao() ) );
			
			ps.setObject( 5, riscoEmpresa.getIdRisco() );
			ps.setObject( 6, riscoEmpresa.getCdEmpresa() );
			
			ps.execute();

		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}

	/**
	 * M�todo para excluir tupla de uma tabela
	 */
	public void exclui() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "delete from PCMSO.RISCOS_EMPRESA" );
			cmd.append( " where ID_RISCO = ? and CD_EMPRESA like ?" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			ps.setObject( 1, riscoEmpresa.getIdRisco() );
			ps.setObject( 2, riscoEmpresa.getCdEmpresa() );
			
			ps.execute();

		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}
}