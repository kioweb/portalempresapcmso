package jPcmso.persistencia.geral.r;

import java.sql.PreparedStatement;

import quatro.persistencia.Persistor;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.ConexaoParaPoolDeConexoes;
import quatro.sql.PoolDeConexoes;
import quatro.sql.QtSQLException;
import quatro.sql.Query;
import quatro.util.QtException;

/**
 *
 * Classe persistor para PCMSO.RISCOS
 *
 * @author ronei@unimed-uau.com.br
 * @see Persistor
 * @see Risco
 * @see RiscoNavegador
 * @see RiscoLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 14/12/2017 15:26 - importado por ronei@unimed-uau.com.br
 *
 */
 public class RiscoPersistor extends Persistor {

	/** Instância de Risco para manipulação pelo persistor */
	private Risco risco;

	/** Construtor genérico */
	public RiscoPersistor() {

		this.risco = null;
	}

	/** Construtor genérico */
	public RiscoPersistor( Conexao cnx ) throws QtSQLException {

		this.risco = null;
		super.setConexao( cnx );
	}

	/** Construtor que recebe uma instância de Risco */
	public RiscoPersistor( Risco risco ) throws QtSQLException {

		if( risco == null ) 
			throw new QtSQLException( "Impossível instanciar RiscoPersistor porque está apontando para um nulo!" );

		this.risco = risco;
	}

	/** Construtor que recebe uma conexão e uma instância de Risco */
	public RiscoPersistor( Conexao conexao, Risco risco ) throws QtSQLException {

		if( risco == null ) 
			throw new QtSQLException( "Impossível instanciar RiscoPersistor porque está apontando para um nulo!" );

		setConexao( conexao );
		this.risco = risco;
	}

	/**
	 * Construtor que recebe a chave da tabela PCMSO.RISCOS
	 * @param idRisco Identificador do Risco
	 */
	public RiscoPersistor( int idRisco )
	  throws QtSQLException { 

		busca( idRisco );
	}

	/**
	 * Construtor que recebe uma conexão e a chave da tabela PCMSO.RISCOS
	 * @param idRisco Identificador do Risco
	 * @param conexao Conexao com o banco de dados
	 */
	public RiscoPersistor( Conexao conexao, int idRisco )
	  throws QtSQLException { 

		super.setConexao( conexao );
		busca( idRisco );
	}

	/**
	 * Método interno para buscar e preencher uma tupla de uma tabela
	 * Utiliza a Instância de Risco para fazer a busca
	 * @param idRisco Identificador do Risco
	 */
	private void busca( int idRisco ) throws QtSQLException { 

		RiscoLocalizador riscoLocalizador = new RiscoLocalizador( getConexao(), idRisco );

		if( riscoLocalizador.isEmpty() ) {
			throw new QtSQLException( "Chaves da Tabela Risco não encontradas - idRisco: " + idRisco );
		}

		risco = (Risco ) riscoLocalizador.getTabelaBasica();
	}

	/**
	 * Permite alimentar o Persistor com um novo registro
	 */
	public void setTabelaBasica( TabelaBasica risco ) throws QtException {

		if( risco instanceof Risco ) {
			this.risco = (Risco) risco;
		} else {
			throw new QtException( "Tabela Básica fornecida tem que ser uma instância de Risco" );
		}
	}

	/**
	 * Permite acessar o registro corrente do persistor
	 */
	public TabelaBasica getTabelaBasica() throws QtException {

		return risco;
	}

	/**
	 * Método para gerar automaticamente uma nova chave
	 */
	public static Integer geraNovoIdRisco( Conexao cnx ) throws QtSQLException {

		Integer result = null;

		Query query = new Query( cnx );

		try{

			query.setSQL( "select max( ID_RISCO ) from PCMSO.RISCOS" );

			query.executeQuery();
			result = new Integer( query.getInt( 1 ) + 1 );
		} catch( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			query.liberaRecursos();
		}
		return result;
	}

	/**
	 * Método para gerar automaticamente uma nova chave
	 */
	public static Integer geraNovoIdRisco(  ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return geraNovoIdRisco(  cnx  );
		} finally {
			cnx.libera();
		}
	}


	/**
	 * Método para inserir tupla na tabela
	 */
	public void insere() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "insert into PCMSO.RISCOS" );
			cmd.append( "( ID_RISCO, TP_RISCO, DS_RISCO )" );
			cmd.append( "values " );
			cmd.append( "( ?, ?, ?)" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			try {

				ps.setObject( 1, risco.getIdRisco() );
				ps.setObject( 2, risco.getTpRisco() );
				ps.setObject( 3, risco.getDsRisco() );
				
				ps.execute();
			} finally { 
				ps.close();
			}
		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}

	/**
	 * Método para alterar tupla na tabela
	 */
	public void altera() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "update PCMSO.RISCOS" );
			cmd.append( "  set ID_RISCO = ?, " );
			cmd.append( " TP_RISCO = ?, " );
			cmd.append( " DS_RISCO = ?" );
			cmd.append( " where ID_RISCO = ?" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			try {

				ps.setObject( 1, risco.getIdRisco() );
				ps.setObject( 2, risco.getTpRisco() );
				ps.setObject( 3, risco.getDsRisco() );
				ps.setObject( 4, risco.getIdRisco() );
				
				ps.execute();
			} finally {
				ps.close();
			}
		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}

	/**
	 * Método para excluir tupla de uma tabela
	 */
	public void exclui() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "delete from PCMSO.RISCOS" );
			cmd.append( " where ID_RISCO = ?" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			try {

				ps.setObject( 1, risco.getIdRisco() );
				
				ps.execute();
			} finally {
				ps.close();
			}
		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}
}