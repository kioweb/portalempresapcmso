package jPcmso.persistencia.geral.r;

import quatro.persistencia.FiltroDeNavegador;
import quatro.persistencia.Navegador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.sql.Query;
import quatro.util.QtData;

/**
 *
 * Classe navegador para PCMSO.RISCOS_EMPRESA
 *
 * @author Samuel Antonio Klein
 * @see Navegador
 * @see RiscoEmpresa
 * @see RiscoEmpresaPersistor
 * @see RiscoEmpresaLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 public class RiscoEmpresaNavegador extends Navegador {

	
	/** Inst�ncia de RiscoEmpresa para manipula��o pelo navegador */
	private RiscoEmpresa riscoEmpresa;

	/** Construtor b�sico */
	public RiscoEmpresaNavegador()
	  throws QtSQLException { 

		riscoEmpresa = new RiscoEmpresa();
		primeiro();
	}

	/** Construtor b�sico que recebe uma conex�o */
	public RiscoEmpresaNavegador( Conexao conexao )
	  throws QtSQLException { 

		super.setConexao( conexao ); 
		riscoEmpresa = new RiscoEmpresa();
		primeiro();
	}

	/**
	 * Construtor que recebe uma inst�ncia de RiscoEmpresaFiltro e posiciona
	 * no primeiro registro que satisfaz suas condi��es 
	 */
	public RiscoEmpresaNavegador( FiltroDeNavegador filtro ) throws QtSQLException  {
		super.setFiltro( filtro );
		this.riscoEmpresa = new RiscoEmpresa();
		primeiro();
	}

	/** Construtor que recebe as chaves da tabela PCMSO.RISCOS_EMPRESA
	 * @param idRisco Identificador do Risco
	 * @param cdEmpresa Codigo da empresa
	 */
	public RiscoEmpresaNavegador( int idRisco, String cdEmpresa )
	  throws QtSQLException { 

		riscoEmpresa = new RiscoEmpresa();
		this.riscoEmpresa.setIdRisco( new Integer( idRisco ) );
		this.riscoEmpresa.setCdEmpresa( cdEmpresa );
		
		busca();
	}

	/** Construtor que recebe  uma conex�o e as chaves da tabela PCMSO.RISCOS_EMPRESA
	 * @param idRisco Identificador do Risco
	 * @param cdEmpresa Codigo da empresa
	 * @param conexao Conexao com o banco de dados
	 */
	public RiscoEmpresaNavegador( Conexao conexao, int idRisco, String cdEmpresa )
	  throws QtSQLException { 

		super.setConexao( conexao ); 
		riscoEmpresa = new RiscoEmpresa();
		this.riscoEmpresa.setIdRisco( new Integer( idRisco ) );
		this.riscoEmpresa.setCdEmpresa( cdEmpresa );
		
		busca();
	}

	/** M�todo que retorna uma tabela b�sica a que se refere o navegador */
	public TabelaBasica getTabelaBasica() {

		return this.riscoEmpresa;
	}

	/** M�todo que retorna a classe RiscoEmpresa relacionada ao navegador */
	public RiscoEmpresa getRiscoEmpresa() {

		return this.riscoEmpresa;
	}

	 /** M�todo que move para o primeiro registro da tabela */
	 public boolean primeiro() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select ID_RISCO, CD_EMPRESA" );
			query.addSQL( "  from PCMSO.RISCOS_EMPRESA" );
			query.addSQL( super.getCondicaoDeFiltro( FiltroDeNavegador.COM_WHERE ) );
			query.addSQL( "  order by ID_RISCO, CD_EMPRESA" );
			query.addSQL( "  limit 1 " );
			
			query.executeQuery();

			
			if( query.isEmpty() ) {

				super.setQueryStates( false, true, true );
			} else {
				this.riscoEmpresa = new RiscoEmpresa();

				riscoEmpresa.setIdRisco( query.getInteger( "ID_RISCO" ) );
				riscoEmpresa.setCdEmpresa( query.getString( "CD_EMPRESA" ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o registro anterior na tabela */
	 public boolean anterior() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select ID_RISCO, CD_EMPRESA" );
			query.addSQL( "  from PCMSO.RISCOS_EMPRESA" );
			query.addSQL( "  where ( ( ID_RISCO = :prm1IdRisco and  CD_EMPRESA < :prm2CdEmpresa )" );
			query.addSQL( "  or (  ID_RISCO < :prm1IdRisco ) )" );
			query.addSQL( super.getCondicaoDeFiltro() );
			query.addSQL( "  order by ID_RISCO desc, CD_EMPRESA desc" );
			query.addSQL( "  limit 1 " );

			query.setParameter( "prm1IdRisco", riscoEmpresa.getIdRisco() );
			query.setParameter( "prm2CdEmpresa", riscoEmpresa.getCdEmpresa() );
		
			query.executeQuery();

			
			if( query.isEmpty() ) {

				super.setFound( false );
				super.setBof( true );
			} else {
				this.riscoEmpresa = new RiscoEmpresa();

				riscoEmpresa.setIdRisco( query.getInteger( "ID_RISCO" ) );
				riscoEmpresa.setCdEmpresa( query.getString( "CD_EMPRESA" ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o proximo registro da tabela */
	 public boolean proximo() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select ID_RISCO, CD_EMPRESA" );
			query.addSQL( "  from PCMSO.RISCOS_EMPRESA" );
			query.addSQL( "  where ( (  ID_RISCO = :prm1IdRisco and  CD_EMPRESA > :prm2CdEmpresa )" );
			query.addSQL( "  or (  ID_RISCO > :prm1IdRisco ) )" );
			query.addSQL( super.getCondicaoDeFiltro() );
			query.addSQL( "  order by ID_RISCO, CD_EMPRESA" );
			query.addSQL( "  limit 1 " );

			query.setParameter( "prm1IdRisco", riscoEmpresa.getIdRisco() );
			query.setParameter( "prm2CdEmpresa", riscoEmpresa.getCdEmpresa() );
		
			query.executeQuery();

			
			if( query.isEmpty() ) {

				super.setFound( false );
				super.setEof( true );
			} else {
				this.riscoEmpresa = new RiscoEmpresa();

				riscoEmpresa.setIdRisco( query.getInteger( "ID_RISCO" ) );
				riscoEmpresa.setCdEmpresa( query.getString( "CD_EMPRESA" ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o �ltimo registro da tabela */
	 public boolean ultimo() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select ID_RISCO, CD_EMPRESA" );
			query.addSQL( "  from PCMSO.RISCOS_EMPRESA" );
			query.addSQL( super.getCondicaoDeFiltro( FiltroDeNavegador.COM_WHERE ) 	);
			query.addSQL( "  order by ID_RISCO desc, CD_EMPRESA desc" );
			query.addSQL( "  limit 1 " );

			query.executeQuery();

			
			if( query.isEmpty() ) {

				super.setQueryStates( false, true, true );
			} else {
				this.riscoEmpresa = new RiscoEmpresa();

				riscoEmpresa.setIdRisco( query.getInteger( "ID_RISCO" ) );
				riscoEmpresa.setCdEmpresa( query.getString( "CD_EMPRESA" ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	/**
	 * M�todo para buscar e preencher uma tupla de uma tabela
	 * @param tabelaBasica Recebe uma tabela basica com os atributos chave preenchidos
	 */
	public boolean busca( TabelaBasica tabelaBasica ) throws QtSQLException {

		if( !( tabelaBasica instanceof RiscoEmpresa ) )
			throw new QtSQLException( "Esperava um objeto RiscoEmpresa!" );

		this.riscoEmpresa = (RiscoEmpresa) tabelaBasica;
		return busca();

	}

	/**
	 * m�todo interno para buscar e preencher uma tupla de uma tabela
	 * Utiliza a Inst�ncia de RiscoEmpresa da classe para fazer a busca
	 */
	private boolean busca() throws QtSQLException {

		RiscoEmpresaLocalizador localizador = new RiscoEmpresaLocalizador( getConexao(), this.riscoEmpresa );

		if( localizador.isEmpty() ) {

			super.setQueryStates( false, true, true );
			return false;

		} else {

			super.setQueryStates( true, false, false );
			return true;
		}
	}
}