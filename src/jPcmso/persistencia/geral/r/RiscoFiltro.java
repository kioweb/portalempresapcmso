package jPcmso.persistencia.geral.r;

import quatro.persistencia.FiltroDeNavegador;

/**
 *
 * Classe filtro para PCMSO.RISCOS
 *
 * @author ronei@unimed-uau.com.br
 * @see FiltroDeTabela
 * @see Risco
 * @see RiscoNavegador
 * @see RiscoPersistor
 * @see RiscoLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 14/12/2017 15:26 - importado por ronei@unimed-uau.com.br
 *
 */
 public class RiscoFiltro extends FiltroDeNavegador {

	/** Determina a ordem de navegação que será passada para o navegador */
	public void setOrdemDeNavegacao( int idOrdem ) {

		super.setOrdemDeNavegacao( idOrdem );
	}
}