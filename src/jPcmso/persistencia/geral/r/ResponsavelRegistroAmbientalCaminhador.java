package jPcmso.persistencia.geral.r;

import quatro.persistencia.Caminhador;
import quatro.persistencia.FiltroDeNavegador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.util.QtData;

 /**
 *
 * Classe Caminhador para ResponsavelRegistroAmbiental
 *
 * @author Samuel Antonio Klein
 * @see Caminhador
 * @see FiltroDeNavegador
 * @see ResponsavelRegistroAmbientalNavegador
 * @see ResponsavelRegistroAmbientalPersistor
 * @see ResponsavelRegistroAmbientalLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:31 - importado por Samuel Antonio Klein
 *
 */
 public class ResponsavelRegistroAmbientalCaminhador extends Caminhador {

	public static final int POR_ORDEM_DEFAULT = -1;
	
	private ResponsavelRegistroAmbiental responsavelRegistroAmbiental;

	public ResponsavelRegistroAmbientalCaminhador( FiltroDeNavegador filtro ) {

		setFiltro( filtro );
		inicializaVisao();
	}

	public ResponsavelRegistroAmbientalCaminhador( Conexao conexao, FiltroDeNavegador filtro ) throws QtSQLException {

		super.setConexao( conexao );
		setFiltro( filtro );
		inicializaVisao();
	}

	private void inicializaVisao() {

		setNmVisao( "PCMSO.RESPONSAVEL_REGISTRO_AMBIENTAL" );
	}

	protected void carregaCampos() throws QtSQLException {

		responsavelRegistroAmbiental = new ResponsavelRegistroAmbiental();

		if( isRecordAvailable() ) {
			responsavelRegistroAmbiental.setCdBeneficiarioCartao( query.getString( "CD_BENEFICIARIO_CARTAO" ) );
			responsavelRegistroAmbiental.setSqPerfil( query.getInteger( "SQ_PERFIL" ) );
			responsavelRegistroAmbiental.setSqResponsavel( query.getInteger( "SQ_RESPONSAVEL" ) );
			responsavelRegistroAmbiental.setDtInicio(  QtData.criaQtData( query.getTimestamp( "DT_INICIO" ) ) );
			responsavelRegistroAmbiental.setDtFinal(  QtData.criaQtData( query.getTimestamp( "DT_FINAL" ) ) );
			responsavelRegistroAmbiental.setNrNitRespresentante( query.getInteger( "NR_NIT_RESPRESENTANTE" ) );
			responsavelRegistroAmbiental.setCdRegistroConselhoClasse( query.getString( "CD_REGISTRO_CONSELHO_CLASSE" ) );
			responsavelRegistroAmbiental.setDsNomeProfissional( query.getString( "DS_NOME_PROFISSIONAL" ) );
		}
	}

	/** M�todo que retorna uma tabela b�sica a que se refere o caminhador */
	public TabelaBasica getTabelaBasica() {

		return this.responsavelRegistroAmbiental;
	}

	/** M�todo que retorna a classe ResponsavelRegistroAmbiental relacionada ao caminhador */
	public ResponsavelRegistroAmbiental getResponsavelRegistroAmbiental() {

		return this.responsavelRegistroAmbiental;
	}

	public void ativaVisao() throws QtSQLException {

		if(  super.getOrdemDeNavegacao() == ResponsavelRegistroAmbientalCaminhador.POR_ORDEM_DEFAULT ) {
			super.ativaCaminhador( "order by CD_BENEFICIARIO_CARTAO, SQ_PERFIL, SQ_RESPONSAVEL" );
	
		} else {
			throw new QtSQLException( "ResponsavelRegistroAmbientalCaminhador - Ordem de Navega��o n�o definida!" );
		}
	}

	public String getCamposIndice( int qualIndice ) {
		return getDsIndice( qualIndice );
	}

	public String getCamposChave() {
		return getDsChave();
	}

	/**
	 * M�todo est�tico que devolve os campos que comp�e a chave da tabela;
	 * @return campos que comp�e a chave.
	 */
	public static String getDsChave() {
		return "CD_BENEFICIARIO_CARTAO, SQ_PERFIL, SQ_RESPONSAVEL";
	}

	/**
	 * M�todo est�tico que devolve os campos que fazem parte de uma dada ordem
	 * de navega��o.
	 * @param qualIndice indica a ordem de navega��o desejada.
	 * @return campos que comp�e a ordem de navega��o.
	 */
	public static String getDsIndice( int qualIndice ) {

		switch( qualIndice ) {

			case POR_ORDEM_DEFAULT: return "CD_BENEFICIARIO_CARTAO, SQ_PERFIL, SQ_RESPONSAVEL";
		}
		return null;
	}
}