package jPcmso.persistencia.geral.r;

import quatro.persistencia.FiltroDeNavegador;
import quatro.persistencia.Navegador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.sql.Query;

/**
 *
 * Classe navegador para PCMSO.RISCOS
 *
 * @author ronei@unimed-uau.com.br
 * @see Navegador
 * @see Risco
 * @see RiscoPersistor
 * @see RiscoLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 14/12/2017 15:26 - importado por ronei@unimed-uau.com.br
 *
 */
 public class RiscoNavegador extends Navegador {

	
	/** Instância de Risco para manipulação pelo navegador */
	private Risco risco;

	/** Construtor básico */
	public RiscoNavegador()
	  throws QtSQLException { 

		risco = new Risco();
		primeiro();
	}

	/** Construtor básico que recebe uma conexão */
	public RiscoNavegador( Conexao conexao )
	  throws QtSQLException { 

		super.setConexao( conexao ); 
		risco = new Risco();
		primeiro();
	}

	/**
	 * Construtor que recebe uma instância de RiscoFiltro e posiciona
	 * no primeiro registro que satisfaz suas condições 
	 */
	public RiscoNavegador( FiltroDeNavegador filtro ) throws QtSQLException  {
		super.setFiltro( filtro );
		this.risco = new Risco();
		primeiro();
	}

	/** Construtor que recebe a chave da tabela PCMSO.RISCOS
	 * @param idRisco Identificador do Risco
	 */
	public RiscoNavegador( int idRisco )
	  throws QtSQLException { 

		risco = new Risco();
		this.risco.setIdRisco( new Integer( idRisco ) );
		
		busca();
	}

	/** Construtor que recebe  uma conexão e a chave da tabela PCMSO.RISCOS
	 * @param idRisco Identificador do Risco
	 * @param conexao Conexao com o banco de dados
	 */
	public RiscoNavegador( Conexao conexao, int idRisco )
	  throws QtSQLException { 

		super.setConexao( conexao ); 
		risco = new Risco();
		this.risco.setIdRisco( new Integer( idRisco ) );
		
		busca();
	}

	/** Método que retorna uma tabela básica a que se refere o navegador */
	public TabelaBasica getTabelaBasica() {

		return this.risco;
	}

	/** Método que retorna a classe Risco relacionada ao navegador */
	public Risco getRisco() {

		return this.risco;
	}

	 /** Método que move para o primeiro registro da tabela */
	 public boolean primeiro() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select min( ID_RISCO ) from PCMSO.RISCOS" );
			query.addSQL( super.getCondicaoDeFiltro( FiltroDeNavegador.COM_WHERE ) 	);

			query.executeQuery();

			
			if( query.getString( 1 ) == null ) {

				super.setQueryStates( false, true, true );
			} else {
				this.risco = new Risco();

				risco.setIdRisco( query.getInteger( 1 ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** Método que move para o registro anterior na tabela */
	 public boolean anterior() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select max( ID_RISCO ) from PCMSO.RISCOS" );
			query.addSQL( " where ID_RISCO < :prm1IdRisco" );
			query.addSQL( super.getCondicaoDeFiltro() );

			query.setParameter( "prm1IdRisco", risco.getIdRisco() );
			
			query.executeQuery();

			
			if( query.getString( 1 ) == null ) {

				super.setFound( false );
				super.setBof( true );
			} else {
				this.risco = new Risco();

				risco.setIdRisco( query.getInteger( 1 ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** Método que move para o proximo registro da tabela */
	 public boolean proximo() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select min( ID_RISCO ) from PCMSO.RISCOS" );
			query.addSQL( " where ID_RISCO > :prm1IdRisco" );
			query.addSQL( super.getCondicaoDeFiltro() );

			query.setParameter( "prm1IdRisco", risco.getIdRisco() );

			query.executeQuery();

			
			if( query.getString( 1 ) == null ) {

				super.setFound( false );
				super.setEof( true );
			} else {
				this.risco = new Risco();

				risco.setIdRisco( query.getInteger( 1 ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** Método que move para o último registro da tabela */
	 public boolean ultimo() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select max( ID_RISCO ) from PCMSO.RISCOS" );
			query.addSQL( super.getCondicaoDeFiltro( FiltroDeNavegador.COM_WHERE ) 	);

			query.executeQuery();

			
			if( query.getString( 1 ) == null ) {

				super.setQueryStates( false, true, true );
			} else {
				this.risco = new Risco();

				risco.setIdRisco( query.getInteger( 1 ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	/**
	 * Método para buscar e preencher uma tupla de uma tabela
	 * @param tabelaBasica Recebe uma tabela basica com os atributos chave preenchidos
	 */
	public boolean busca( TabelaBasica tabelaBasica ) throws QtSQLException {

		if( !( tabelaBasica instanceof Risco ) )
			throw new QtSQLException( "Esperava um objeto Risco!" );

		this.risco = (Risco) tabelaBasica;
		return busca();

	}

	/**
	 * método interno para buscar e preencher uma tupla de uma tabela
	 * Utiliza a Instância de Risco da classe para fazer a busca
	 */
	private boolean busca() throws QtSQLException {

		RiscoLocalizador localizador = new RiscoLocalizador( getConexao(), this.risco );

		if( localizador.isEmpty() ) {

			super.setQueryStates( false, true, true );
			return false;

		} else {

			super.setQueryStates( true, false, false );
			return true;
		}
	}
}