package jPcmso.persistencia.geral.r;

import quatro.persistencia.TabelaBasica;
import quatro.smartDB.IgnoredField;
import quatro.smartDB.PrimaryKey;
import quatro.smartDB.Table;
import quatro.util.QtData;

/**
 *
 * Classe b�sica para PCMSO.RISCOS_EMPRESA
 *
 * Tabela Riscos da empresa
 * 
 * @author Samuel Antonio Klein
 * @see RiscoEmpresaNavegador
 * @see RiscoEmpresaPersistor
 * @see RiscoEmpresaLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 @Table(name="PCMSO.RISCOS_EMPRESA")
 public class RiscoEmpresa implements TabelaBasica {

	@IgnoredField
	private static final long serialVersionUID = -3019981302006181887L;

	/** Identificador do Risco (chave) */
	@PrimaryKey
	private Integer idRisco;
	/** Codigo da empresa (chave-char(4)) */
	@PrimaryKey
	private String cdEmpresa;
	/** Data de inclusao */
	private QtData dtInclusao;
	/** Data de exclusao */
	private QtData dtExclusao;

	/**
	 * Alimenta Identificador do Risco
	 * @param idRisco Identificador do Risco
	 */ 
	public void setIdRisco( Integer idRisco ) {
		this.idRisco = idRisco;
	}

	/**
	 * Retorna Identificador do Risco
	 * @return Identificador do Risco
	 */
	public Integer getIdRisco() {
		return this.idRisco;
	}

	/**
	 * Alimenta Codigo da empresa
	 * @param cdEmpresa Codigo da empresa
	 */ 
	public void setCdEmpresa( String cdEmpresa ) {
		this.cdEmpresa = cdEmpresa;
	}

	/**
	 * Retorna Codigo da empresa
	 * @return Codigo da empresa
	 */
	public String getCdEmpresa() {
		return this.cdEmpresa;
	}

	/**
	 * Alimenta Data de inclusao
	 * @param dtInclusao Data de inclusao
	 */ 
	public void setDtInclusao( QtData dtInclusao ) {
		this.dtInclusao = dtInclusao;
	}

	/**
	 * Retorna Data de inclusao
	 * @return Data de inclusao
	 */
	public QtData getDtInclusao() {
		return this.dtInclusao;
	}

	/**
	 * Alimenta Data de exclusao
	 * @param dtExclusao Data de exclusao
	 */ 
	public void setDtExclusao( QtData dtExclusao ) {
		this.dtExclusao = dtExclusao;
	}

	/**
	 * Retorna Data de exclusao
	 * @return Data de exclusao
	 */
	public QtData getDtExclusao() {
		return this.dtExclusao;
	}

}