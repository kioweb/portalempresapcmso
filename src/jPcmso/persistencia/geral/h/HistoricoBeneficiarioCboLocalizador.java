package jPcmso.persistencia.geral.h;

import quatro.persistencia.Localizador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.QtSQLException;
import quatro.sql.Query;
import quatro.sql.Conexao;
import quatro.sql.PoolDeConexoes;
import quatro.sql.ConexaoParaPoolDeConexoes;
import quatro.util.QtData;

/**
 *
 * Classe localizador para PCMSO.HISTORICO_BENEFICIARIOS_CBO
 *
 * @author Samuel Antonio Klein
 * @see Localizador
 * @see HistoricoBeneficiarioCbo
 * @see HistoricoBeneficiarioCboNavegador
 * @see HistoricoBeneficiarioCboPersistor
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 public class HistoricoBeneficiarioCboLocalizador extends Localizador {

	/** Inst�ncia de HistoricoBeneficiarioCbo para manipula��o pelo localizador */
	private HistoricoBeneficiarioCbo historicoBeneficiarioCbo;

	/** Construtor que recebe uma conex�o e um objeto HistoricoBeneficiarioCbo completo */
	public HistoricoBeneficiarioCboLocalizador( Conexao conexao, HistoricoBeneficiarioCbo historicoBeneficiarioCbo ) throws QtSQLException {

		super.setConexao( conexao );

		this.historicoBeneficiarioCbo = historicoBeneficiarioCbo;
		busca();
	}

	/** Construtor que recebe um objeto HistoricoBeneficiarioCbo completo */
	public HistoricoBeneficiarioCboLocalizador( HistoricoBeneficiarioCbo historicoBeneficiarioCbo ) throws QtSQLException {

		this.historicoBeneficiarioCbo = historicoBeneficiarioCbo;
		busca();
	}

	/** 
	 * Construtor que recebe uma conex�o e as chaves da tabela PCMSO.HISTORICO_BENEFICIARIOS_CBO
	 * @param cdBeneficiarioCartao Codigo de Beneficiario 
	 * @param sqAlteracao Sequecial de ateracoes
	 * @param conexao Conexao com o banco de dados
	 */
	public HistoricoBeneficiarioCboLocalizador( Conexao conexao, String cdBeneficiarioCartao, int sqAlteracao ) throws QtSQLException { 

		super.setConexao( conexao ); 
		historicoBeneficiarioCbo = new HistoricoBeneficiarioCbo();
		this.historicoBeneficiarioCbo.setCdBeneficiarioCartao( cdBeneficiarioCartao );
		this.historicoBeneficiarioCbo.setSqAlteracao( new Integer( sqAlteracao ) );
		
		busca();
	}

	/** 
	 * Construtor que recebe as chaves da tabela PCMSO.HISTORICO_BENEFICIARIOS_CBO
	 * @param cdBeneficiarioCartao Codigo de Beneficiario 
	 * @param sqAlteracao Sequecial de ateracoes
	 */
	public HistoricoBeneficiarioCboLocalizador( String cdBeneficiarioCartao, int sqAlteracao ) throws QtSQLException { 

		historicoBeneficiarioCbo = new HistoricoBeneficiarioCbo();
		this.historicoBeneficiarioCbo.setCdBeneficiarioCartao( cdBeneficiarioCartao );
		this.historicoBeneficiarioCbo.setSqAlteracao( new Integer( sqAlteracao ) );
		
		busca();
	}

	public static boolean existe( Conexao cnx, String cdBeneficiarioCartao, int sqAlteracao ) throws QtSQLException {

		Query q = new Query( cnx );

		try {
			q.setSQL( "select count(*) from PCMSO.HISTORICO_BENEFICIARIOS_CBO" );
			q.addSQL( " where CD_BENEFICIARIO_CARTAO = :prm1CdBeneficiarioCartao and SQ_ALTERACAO = :prm2SqAlteracao" );

			q.setParameter( "prm1CdBeneficiarioCartao", cdBeneficiarioCartao );
			q.setParameter( "prm2SqAlteracao", sqAlteracao );
			q.executeQuery();

			return q.getInt( 1 ) > 0;
		} finally {
			q.liberaRecursos();
		}
	}

	public static boolean existe( String cdBeneficiarioCartao, int sqAlteracao ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return existe( cnx, cdBeneficiarioCartao, sqAlteracao );
		} finally {
			cnx.libera();
		}
	}

	public static boolean existe( Conexao cnx, HistoricoBeneficiarioCbo historicoBeneficiarioCbo ) throws QtSQLException {

		return existe( cnx, historicoBeneficiarioCbo.getCdBeneficiarioCartao(), historicoBeneficiarioCbo.getSqAlteracao() );
	}

	public static boolean existe( HistoricoBeneficiarioCbo historicoBeneficiarioCbo ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return existe( cnx, historicoBeneficiarioCbo.getCdBeneficiarioCartao(), historicoBeneficiarioCbo.getSqAlteracao() );
		} finally {
			cnx.libera();
		}
	}

	public static HistoricoBeneficiarioCbo buscaHistoricoBeneficiarioCbo( Conexao cnx, String cdBeneficiarioCartao, int sqAlteracao ) throws QtSQLException {

		Query q = new Query( cnx );

		try {
			q.setSQL( "select * from PCMSO.HISTORICO_BENEFICIARIOS_CBO" );
			q.addSQL( " where CD_BENEFICIARIO_CARTAO = :prm1CdBeneficiarioCartao and SQ_ALTERACAO = :prm2SqAlteracao" );

			q.setParameter( "prm1CdBeneficiarioCartao", cdBeneficiarioCartao );
			q.setParameter( "prm2SqAlteracao", sqAlteracao );
			q.executeQuery();

			if( q.isEmpty() ) {
				return null;
			} else {
				HistoricoBeneficiarioCbo historicoBeneficiarioCbo = new HistoricoBeneficiarioCbo();
				buscaCampos( historicoBeneficiarioCbo, q );
				return historicoBeneficiarioCbo;
			}
		} finally {
			q.liberaRecursos();
		}
	}

	public static HistoricoBeneficiarioCbo buscaHistoricoBeneficiarioCbo( String cdBeneficiarioCartao, int sqAlteracao ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return buscaHistoricoBeneficiarioCbo( cnx, cdBeneficiarioCartao, sqAlteracao );
		} finally { 
			cnx.libera();
		}
	}

	/** 
	 * Construtor que recebe uma conex�o e as chaves da tabela PCMSO.HISTORICO_BENEFICIARIOS_CBO
	 * @param cdBeneficiarioCartao Codigo de Beneficiario 
	 * @param sqAlteracao Sequecial de ateracoes
	 * @param conexao Conexao com o banco de dados
	 */
	public HistoricoBeneficiarioCboLocalizador( Conexao conexao, String cdBeneficiarioCartao, Integer sqAlteracao ) throws QtSQLException {

		super.setConexao( conexao ); 
		historicoBeneficiarioCbo = new HistoricoBeneficiarioCbo();
		this.historicoBeneficiarioCbo.setCdBeneficiarioCartao( cdBeneficiarioCartao );
		this.historicoBeneficiarioCbo.setSqAlteracao( sqAlteracao );
		
		busca();
	}

	/** 
	 * Construtor que recebe as chaves da tabela PCMSO.HISTORICO_BENEFICIARIOS_CBO
	 * @param cdBeneficiarioCartao Codigo de Beneficiario 
	 * @param sqAlteracao Sequecial de ateracoes
	 */
	public HistoricoBeneficiarioCboLocalizador( String cdBeneficiarioCartao, Integer sqAlteracao ) throws QtSQLException {

		historicoBeneficiarioCbo = new HistoricoBeneficiarioCbo();
		this.historicoBeneficiarioCbo.setCdBeneficiarioCartao( cdBeneficiarioCartao );
		this.historicoBeneficiarioCbo.setSqAlteracao( sqAlteracao );
		
		busca();
	}

	public static void buscaCampos( HistoricoBeneficiarioCbo historicoBeneficiarioCbo, Query query ) throws QtSQLException {

		historicoBeneficiarioCbo.setCdBeneficiarioCartao( query.getString( "CD_BENEFICIARIO_CARTAO" ) );
		historicoBeneficiarioCbo.setSqAlteracao( query.getInteger( "SQ_ALTERACAO" ) );
		historicoBeneficiarioCbo.setDtAlteracao( QtData.criaQtData( query.getTimestamp( "DT_ALTERACAO" ) ) );
		historicoBeneficiarioCbo.setIdSetor( query.getInteger( "ID_SETOR" ) );
		historicoBeneficiarioCbo.setIdFuncao( query.getInteger( "ID_FUNCAO" ) );
		historicoBeneficiarioCbo.setIdCargo( query.getInteger( "ID_CARGO" ) );
		historicoBeneficiarioCbo.setCdCbo( query.getString( "CD_CBO" ) );
		historicoBeneficiarioCbo.setDsAtividade( query.getString( "DS_ATIVIDADE" ) );
	}

	/** M�todo que retorna uma tabela b�sica a que se refere o localizador */
	public TabelaBasica getTabelaBasica() {

		return this.historicoBeneficiarioCbo;
	}

	/** M�todo que retorna a classe HistoricoBeneficiarioCbo relacionada ao localizador */
	public HistoricoBeneficiarioCbo getHistoricoBeneficiarioCbo() {

		return this.historicoBeneficiarioCbo;
	}

	/**
	 * M�todo para buscar e preencher uma tupla de uma tabela
	 * @param tabelaBasica Recebe uma tabela basica com os atributos chave preenchidos
	 */
	public boolean busca( TabelaBasica tabelaBasica ) throws QtSQLException {

		if( !( tabelaBasica instanceof HistoricoBeneficiarioCbo ) )
			throw new QtSQLException( "Esperava um objeto HistoricoBeneficiarioCbo!" );

		this.historicoBeneficiarioCbo = (HistoricoBeneficiarioCbo) tabelaBasica;
		return busca();

	}

	/**
	 * M�todo privado para buscar e preencher uma tupla de uma tabela
	 * Este m�todo busca os dados da inst�ncia de HistoricoBeneficiarioCbo atual
	 */
	private boolean busca() throws QtSQLException {

		boolean ret = false;

		super.estabeleceConexao();
		Query query = super.criaNovaQuery();

		try {

			query.setSQL( "select * from PCMSO.HISTORICO_BENEFICIARIOS_CBO" );
			query.addSQL( " where CD_BENEFICIARIO_CARTAO like :prm1CdBeneficiarioCartao and SQ_ALTERACAO = :prm2SqAlteracao" );

			query.setParameter( "prm1CdBeneficiarioCartao", historicoBeneficiarioCbo.getCdBeneficiarioCartao() );
			query.setParameter( "prm2SqAlteracao", historicoBeneficiarioCbo.getSqAlteracao() );
			query.executeQuery();

			if( query.isEmpty() ) {
				super.setEmpty( true );
				historicoBeneficiarioCbo = null;
			} else {
				super.setEmpty( false );
				
				historicoBeneficiarioCbo.setCdBeneficiarioCartao( query.getString( "CD_BENEFICIARIO_CARTAO" ) );
				historicoBeneficiarioCbo.setSqAlteracao( query.getInteger( "SQ_ALTERACAO" ) );
				historicoBeneficiarioCbo.setDtAlteracao( QtData.criaQtData( query.getTimestamp( "DT_ALTERACAO" ) ) );
				historicoBeneficiarioCbo.setIdSetor( query.getInteger( "ID_SETOR" ) );
				historicoBeneficiarioCbo.setIdFuncao( query.getInteger( "ID_FUNCAO" ) );
				historicoBeneficiarioCbo.setIdCargo( query.getInteger( "ID_CARGO" ) );
				historicoBeneficiarioCbo.setCdCbo( query.getString( "CD_CBO" ) );
				historicoBeneficiarioCbo.setDsAtividade( query.getString( "DS_ATIVIDADE" ) );
				ret = true;
			}
		} finally {
			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}
 }