package jPcmso.persistencia.geral.h;

import quatro.persistencia.FiltroDeNavegador;

/**
 *
 * Classe filtro para PCMSO.HISTORICO_BENEFICIARIOS_CBO
 *
 * @author Samuel Antonio Klein
 * @see FiltroDeTabela
 * @see HistoricoBeneficiarioCbo
 * @see HistoricoBeneficiarioCboNavegador
 * @see HistoricoBeneficiarioCboPersistor
 * @see HistoricoBeneficiarioCboLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 public class HistoricoBeneficiarioCboFiltro extends FiltroDeNavegador {

	/** Determina a ordem de navega��o que ser� passada para o navegador */
	public void setOrdemDeNavegacao( int idOrdem ) {

		super.setOrdemDeNavegacao( idOrdem );
	}
}