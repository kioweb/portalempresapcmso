package jPcmso.persistencia.geral.h;

import quatro.persistencia.TabelaBasica;
import quatro.smartDB.IgnoredField;
import quatro.smartDB.PrimaryKey;
import quatro.smartDB.Table;
import quatro.util.QtData;

/**
 *
 * Classe b�sica para PCMSO.HISTORICO_BENEFICIARIOS_CBO
 *
 * Tabela de historico do Beneficiarios CBO
 * 
 * @author Samuel Antonio Klein
 * @see HistoricoBeneficiarioCboNavegador
 * @see HistoricoBeneficiarioCboPersistor
 * @see HistoricoBeneficiarioCboLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 @Table(name="PCMSO.HISTORICO_BENEFICIARIOS_CBO")
 public class HistoricoBeneficiarioCbo implements TabelaBasica {

	@IgnoredField
	private static final long serialVersionUID = -1084524809570962013L;

	/** Codigo de Beneficiario  (chave-char(12)) */
	@PrimaryKey
	private String cdBeneficiarioCartao;
	/** Sequecial de ateracoes (chave) */
	@PrimaryKey
	private Integer sqAlteracao;
	/** data alteracao */
	private QtData dtAlteracao;
	/** Setor */
	private Integer idSetor;
	/** funcao */
	private Integer idFuncao;
	/** cargo */
	private Integer idCargo;
	/** Codigo do CBO (char(6)) */
	private String cdCbo;
	/** Descricao da atividade */
	private String dsAtividade;

	/**
	 * Alimenta Codigo de Beneficiario 
	 * @param cdBeneficiarioCartao Codigo de Beneficiario 
	 */ 
	public void setCdBeneficiarioCartao( String cdBeneficiarioCartao ) {
		this.cdBeneficiarioCartao = cdBeneficiarioCartao;
	}

	/**
	 * Retorna Codigo de Beneficiario 
	 * @return Codigo de Beneficiario 
	 */
	public String getCdBeneficiarioCartao() {
		return this.cdBeneficiarioCartao;
	}

	/**
	 * Alimenta Sequecial de ateracoes
	 * @param sqAlteracao Sequecial de ateracoes
	 */ 
	public void setSqAlteracao( Integer sqAlteracao ) {
		this.sqAlteracao = sqAlteracao;
	}

	/**
	 * Retorna Sequecial de ateracoes
	 * @return Sequecial de ateracoes
	 */
	public Integer getSqAlteracao() {
		return this.sqAlteracao;
	}

	/**
	 * Alimenta data alteracao
	 * @param dtAlteracao data alteracao
	 */ 
	public void setDtAlteracao( QtData dtAlteracao ) {
		this.dtAlteracao = dtAlteracao;
	}

	/**
	 * Retorna data alteracao
	 * @return data alteracao
	 */
	public QtData getDtAlteracao() {
		return this.dtAlteracao;
	}

	/**
	 * Alimenta Setor
	 * @param idSetor Setor
	 */ 
	public void setIdSetor( Integer idSetor ) {
		this.idSetor = idSetor;
	}

	/**
	 * Retorna Setor
	 * @return Setor
	 */
	public Integer getIdSetor() {
		return this.idSetor;
	}

	/**
	 * Alimenta funcao
	 * @param idFuncao funcao
	 */ 
	public void setIdFuncao( Integer idFuncao ) {
		this.idFuncao = idFuncao;
	}

	/**
	 * Retorna funcao
	 * @return funcao
	 */
	public Integer getIdFuncao() {
		return this.idFuncao;
	}

	/**
	 * Alimenta cargo
	 * @param idCargo cargo
	 */ 
	public void setIdCargo( Integer idCargo ) {
		this.idCargo = idCargo;
	}

	/**
	 * Retorna cargo
	 * @return cargo
	 */
	public Integer getIdCargo() {
		return this.idCargo;
	}

	/**
	 * Alimenta Codigo do CBO
	 * @param cdCbo Codigo do CBO
	 */ 
	public void setCdCbo( String cdCbo ) {
		this.cdCbo = cdCbo;
	}

	/**
	 * Retorna Codigo do CBO
	 * @return Codigo do CBO
	 */
	public String getCdCbo() {
		return this.cdCbo;
	}

	/**
	 * Alimenta Descricao da atividade
	 * @param dsAtividade Descricao da atividade
	 */ 
	public void setDsAtividade( String dsAtividade ) {
		this.dsAtividade = dsAtividade;
	}

	/**
	 * Retorna Descricao da atividade
	 * @return Descricao da atividade
	 */
	public String getDsAtividade() {
		return this.dsAtividade;
	}

}