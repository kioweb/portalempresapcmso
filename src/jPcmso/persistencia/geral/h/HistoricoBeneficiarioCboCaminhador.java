package jPcmso.persistencia.geral.h;

import quatro.persistencia.Caminhador;
import quatro.persistencia.FiltroDeNavegador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.util.QtData;

 /**
 *
 * Classe Caminhador para HistoricoBeneficiarioCbo
 *
 * @author Samuel Antonio Klein
 * @see Caminhador
 * @see FiltroDeNavegador
 * @see HistoricoBeneficiarioCboNavegador
 * @see HistoricoBeneficiarioCboPersistor
 * @see HistoricoBeneficiarioCboLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 public class HistoricoBeneficiarioCboCaminhador extends Caminhador {

	public static final int POR_ORDEM_DEFAULT = -1;
	
	private HistoricoBeneficiarioCbo historicoBeneficiarioCbo;

	public HistoricoBeneficiarioCboCaminhador( FiltroDeNavegador filtro ) {

		setFiltro( filtro );
		inicializaVisao();
	}

	public HistoricoBeneficiarioCboCaminhador( Conexao conexao, FiltroDeNavegador filtro ) throws QtSQLException {

		super.setConexao( conexao );
		setFiltro( filtro );
		inicializaVisao();
	}

	private void inicializaVisao() {

		setNmVisao( "PCMSO.HISTORICO_BENEFICIARIOS_CBO" );
	}

	protected void carregaCampos() throws QtSQLException {

		historicoBeneficiarioCbo = new HistoricoBeneficiarioCbo();

		if( isRecordAvailable() ) {
			historicoBeneficiarioCbo.setCdBeneficiarioCartao( query.getString( "CD_BENEFICIARIO_CARTAO" ) );
			historicoBeneficiarioCbo.setSqAlteracao( query.getInteger( "SQ_ALTERACAO" ) );
			historicoBeneficiarioCbo.setDtAlteracao(  QtData.criaQtData( query.getTimestamp( "DT_ALTERACAO" ) ) );
			historicoBeneficiarioCbo.setIdSetor( query.getInteger( "ID_SETOR" ) );
			historicoBeneficiarioCbo.setIdFuncao( query.getInteger( "ID_FUNCAO" ) );
			historicoBeneficiarioCbo.setIdCargo( query.getInteger( "ID_CARGO" ) );
			historicoBeneficiarioCbo.setCdCbo( query.getString( "CD_CBO" ) );
			historicoBeneficiarioCbo.setDsAtividade( query.getString( "DS_ATIVIDADE" ) );
		}
	}

	/** M�todo que retorna uma tabela b�sica a que se refere o caminhador */
	public TabelaBasica getTabelaBasica() {

		return this.historicoBeneficiarioCbo;
	}

	/** M�todo que retorna a classe HistoricoBeneficiarioCbo relacionada ao caminhador */
	public HistoricoBeneficiarioCbo getHistoricoBeneficiarioCbo() {

		return this.historicoBeneficiarioCbo;
	}

	public void ativaVisao() throws QtSQLException {

		if(  super.getOrdemDeNavegacao() == HistoricoBeneficiarioCboCaminhador.POR_ORDEM_DEFAULT ) {
			super.ativaCaminhador( "order by CD_BENEFICIARIO_CARTAO, SQ_ALTERACAO" );
	
		} else {
			throw new QtSQLException( "HistoricoBeneficiarioCboCaminhador - Ordem de Navega��o n�o definida!" );
		}
	}

	public String getCamposIndice( int qualIndice ) {
		return getDsIndice( qualIndice );
	}

	public String getCamposChave() {
		return getDsChave();
	}

	/**
	 * M�todo est�tico que devolve os campos que comp�e a chave da tabela;
	 * @return campos que comp�e a chave.
	 */
	public static String getDsChave() {
		return "CD_BENEFICIARIO_CARTAO, SQ_ALTERACAO";
	}

	/**
	 * M�todo est�tico que devolve os campos que fazem parte de uma dada ordem
	 * de navega��o.
	 * @param qualIndice indica a ordem de navega��o desejada.
	 * @return campos que comp�e a ordem de navega��o.
	 */
	public static String getDsIndice( int qualIndice ) {

		switch( qualIndice ) {

			case POR_ORDEM_DEFAULT: return "CD_BENEFICIARIO_CARTAO, SQ_ALTERACAO";
		}
		return null;
	}
}