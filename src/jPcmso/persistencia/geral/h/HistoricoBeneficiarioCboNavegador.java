package jPcmso.persistencia.geral.h;

import quatro.persistencia.FiltroDeNavegador;
import quatro.persistencia.Navegador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.sql.Query;
import quatro.util.QtData;

/**
 *
 * Classe navegador para PCMSO.HISTORICO_BENEFICIARIOS_CBO
 *
 * @author Samuel Antonio Klein
 * @see Navegador
 * @see HistoricoBeneficiarioCbo
 * @see HistoricoBeneficiarioCboPersistor
 * @see HistoricoBeneficiarioCboLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 public class HistoricoBeneficiarioCboNavegador extends Navegador {

	
	/** Inst�ncia de HistoricoBeneficiarioCbo para manipula��o pelo navegador */
	private HistoricoBeneficiarioCbo historicoBeneficiarioCbo;

	/** Construtor b�sico */
	public HistoricoBeneficiarioCboNavegador()
	  throws QtSQLException { 

		historicoBeneficiarioCbo = new HistoricoBeneficiarioCbo();
		primeiro();
	}

	/** Construtor b�sico que recebe uma conex�o */
	public HistoricoBeneficiarioCboNavegador( Conexao conexao )
	  throws QtSQLException { 

		super.setConexao( conexao ); 
		historicoBeneficiarioCbo = new HistoricoBeneficiarioCbo();
		primeiro();
	}

	/**
	 * Construtor que recebe uma inst�ncia de HistoricoBeneficiarioCboFiltro e posiciona
	 * no primeiro registro que satisfaz suas condi��es 
	 */
	public HistoricoBeneficiarioCboNavegador( FiltroDeNavegador filtro ) throws QtSQLException  {
		super.setFiltro( filtro );
		this.historicoBeneficiarioCbo = new HistoricoBeneficiarioCbo();
		primeiro();
	}

	/** Construtor que recebe as chaves da tabela PCMSO.HISTORICO_BENEFICIARIOS_CBO
	 * @param cdBeneficiarioCartao Codigo de Beneficiario 
	 * @param sqAlteracao Sequecial de ateracoes
	 */
	public HistoricoBeneficiarioCboNavegador( String cdBeneficiarioCartao, int sqAlteracao )
	  throws QtSQLException { 

		historicoBeneficiarioCbo = new HistoricoBeneficiarioCbo();
		this.historicoBeneficiarioCbo.setCdBeneficiarioCartao( cdBeneficiarioCartao );
		this.historicoBeneficiarioCbo.setSqAlteracao( new Integer( sqAlteracao ) );
		
		busca();
	}

	/** Construtor que recebe  uma conex�o e as chaves da tabela PCMSO.HISTORICO_BENEFICIARIOS_CBO
	 * @param cdBeneficiarioCartao Codigo de Beneficiario 
	 * @param sqAlteracao Sequecial de ateracoes
	 * @param conexao Conexao com o banco de dados
	 */
	public HistoricoBeneficiarioCboNavegador( Conexao conexao, String cdBeneficiarioCartao, int sqAlteracao )
	  throws QtSQLException { 

		super.setConexao( conexao ); 
		historicoBeneficiarioCbo = new HistoricoBeneficiarioCbo();
		this.historicoBeneficiarioCbo.setCdBeneficiarioCartao( cdBeneficiarioCartao );
		this.historicoBeneficiarioCbo.setSqAlteracao( new Integer( sqAlteracao ) );
		
		busca();
	}

	/** M�todo que retorna uma tabela b�sica a que se refere o navegador */
	public TabelaBasica getTabelaBasica() {

		return this.historicoBeneficiarioCbo;
	}

	/** M�todo que retorna a classe HistoricoBeneficiarioCbo relacionada ao navegador */
	public HistoricoBeneficiarioCbo getHistoricoBeneficiarioCbo() {

		return this.historicoBeneficiarioCbo;
	}

	 /** M�todo que move para o primeiro registro da tabela */
	 public boolean primeiro() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select CD_BENEFICIARIO_CARTAO, SQ_ALTERACAO" );
			query.addSQL( "  from PCMSO.HISTORICO_BENEFICIARIOS_CBO" );
			query.addSQL( super.getCondicaoDeFiltro( FiltroDeNavegador.COM_WHERE ) );
			query.addSQL( "  order by CD_BENEFICIARIO_CARTAO, SQ_ALTERACAO" );
			query.addSQL( "  limit 1 " );
			
			query.executeQuery();

			
			if( query.isEmpty() ) {

				super.setQueryStates( false, true, true );
			} else {
				this.historicoBeneficiarioCbo = new HistoricoBeneficiarioCbo();

				historicoBeneficiarioCbo.setCdBeneficiarioCartao( query.getString( "CD_BENEFICIARIO_CARTAO" ) );
				historicoBeneficiarioCbo.setSqAlteracao( query.getInteger( "SQ_ALTERACAO" ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o registro anterior na tabela */
	 public boolean anterior() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select CD_BENEFICIARIO_CARTAO, SQ_ALTERACAO" );
			query.addSQL( "  from PCMSO.HISTORICO_BENEFICIARIOS_CBO" );
			query.addSQL( "  where ( ( CD_BENEFICIARIO_CARTAO = :prm1CdBeneficiarioCartao and  SQ_ALTERACAO < :prm2SqAlteracao )" );
			query.addSQL( "  or (  CD_BENEFICIARIO_CARTAO < :prm1CdBeneficiarioCartao ) )" );
			query.addSQL( super.getCondicaoDeFiltro() );
			query.addSQL( "  order by CD_BENEFICIARIO_CARTAO desc, SQ_ALTERACAO desc" );
			query.addSQL( "  limit 1 " );

			query.setParameter( "prm1CdBeneficiarioCartao", historicoBeneficiarioCbo.getCdBeneficiarioCartao() );
			query.setParameter( "prm2SqAlteracao", historicoBeneficiarioCbo.getSqAlteracao() );
		
			query.executeQuery();

			
			if( query.isEmpty() ) {

				super.setFound( false );
				super.setBof( true );
			} else {
				this.historicoBeneficiarioCbo = new HistoricoBeneficiarioCbo();

				historicoBeneficiarioCbo.setCdBeneficiarioCartao( query.getString( "CD_BENEFICIARIO_CARTAO" ) );
				historicoBeneficiarioCbo.setSqAlteracao( query.getInteger( "SQ_ALTERACAO" ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o proximo registro da tabela */
	 public boolean proximo() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select CD_BENEFICIARIO_CARTAO, SQ_ALTERACAO" );
			query.addSQL( "  from PCMSO.HISTORICO_BENEFICIARIOS_CBO" );
			query.addSQL( "  where ( (  CD_BENEFICIARIO_CARTAO = :prm1CdBeneficiarioCartao and  SQ_ALTERACAO > :prm2SqAlteracao )" );
			query.addSQL( "  or (  CD_BENEFICIARIO_CARTAO > :prm1CdBeneficiarioCartao ) )" );
			query.addSQL( super.getCondicaoDeFiltro() );
			query.addSQL( "  order by CD_BENEFICIARIO_CARTAO, SQ_ALTERACAO" );
			query.addSQL( "  limit 1 " );

			query.setParameter( "prm1CdBeneficiarioCartao", historicoBeneficiarioCbo.getCdBeneficiarioCartao() );
			query.setParameter( "prm2SqAlteracao", historicoBeneficiarioCbo.getSqAlteracao() );
		
			query.executeQuery();

			
			if( query.isEmpty() ) {

				super.setFound( false );
				super.setEof( true );
			} else {
				this.historicoBeneficiarioCbo = new HistoricoBeneficiarioCbo();

				historicoBeneficiarioCbo.setCdBeneficiarioCartao( query.getString( "CD_BENEFICIARIO_CARTAO" ) );
				historicoBeneficiarioCbo.setSqAlteracao( query.getInteger( "SQ_ALTERACAO" ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o �ltimo registro da tabela */
	 public boolean ultimo() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select CD_BENEFICIARIO_CARTAO, SQ_ALTERACAO" );
			query.addSQL( "  from PCMSO.HISTORICO_BENEFICIARIOS_CBO" );
			query.addSQL( super.getCondicaoDeFiltro( FiltroDeNavegador.COM_WHERE ) 	);
			query.addSQL( "  order by CD_BENEFICIARIO_CARTAO desc, SQ_ALTERACAO desc" );
			query.addSQL( "  limit 1 " );

			query.executeQuery();

			
			if( query.isEmpty() ) {

				super.setQueryStates( false, true, true );
			} else {
				this.historicoBeneficiarioCbo = new HistoricoBeneficiarioCbo();

				historicoBeneficiarioCbo.setCdBeneficiarioCartao( query.getString( "CD_BENEFICIARIO_CARTAO" ) );
				historicoBeneficiarioCbo.setSqAlteracao( query.getInteger( "SQ_ALTERACAO" ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	/**
	 * M�todo para buscar e preencher uma tupla de uma tabela
	 * @param tabelaBasica Recebe uma tabela basica com os atributos chave preenchidos
	 */
	public boolean busca( TabelaBasica tabelaBasica ) throws QtSQLException {

		if( !( tabelaBasica instanceof HistoricoBeneficiarioCbo ) )
			throw new QtSQLException( "Esperava um objeto HistoricoBeneficiarioCbo!" );

		this.historicoBeneficiarioCbo = (HistoricoBeneficiarioCbo) tabelaBasica;
		return busca();

	}

	/**
	 * m�todo interno para buscar e preencher uma tupla de uma tabela
	 * Utiliza a Inst�ncia de HistoricoBeneficiarioCbo da classe para fazer a busca
	 */
	private boolean busca() throws QtSQLException {

		HistoricoBeneficiarioCboLocalizador localizador = new HistoricoBeneficiarioCboLocalizador( getConexao(), this.historicoBeneficiarioCbo );

		if( localizador.isEmpty() ) {

			super.setQueryStates( false, true, true );
			return false;

		} else {

			super.setQueryStates( true, false, false );
			return true;
		}
	}
}