package jPcmso.persistencia.geral.h;

import java.sql.PreparedStatement;

import quatro.persistencia.Persistor;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.ConexaoParaPoolDeConexoes;
import quatro.sql.PoolDeConexoes;
import quatro.sql.QtSQLException;
import quatro.sql.Query;
import quatro.util.QtData;
import quatro.util.QtException;

/**
 *
 * Classe persistor para PCMSO.HISTORICO_BENEFICIARIOS_CBO
 *
 * @author Samuel Antonio Klein
 * @see Persistor
 * @see HistoricoBeneficiarioCbo
 * @see HistoricoBeneficiarioCboNavegador
 * @see HistoricoBeneficiarioCboLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 public class HistoricoBeneficiarioCboPersistor extends Persistor {

	/** Inst�ncia de HistoricoBeneficiarioCbo para manipula��o pelo persistor */
	private HistoricoBeneficiarioCbo historicoBeneficiarioCbo;

	/** Construtor gen�rico */
	public HistoricoBeneficiarioCboPersistor() {

		this.historicoBeneficiarioCbo = null;
	}

	/** Construtor gen�rico */
	public HistoricoBeneficiarioCboPersistor( Conexao cnx ) throws QtSQLException {

		this.historicoBeneficiarioCbo = null;
		super.setConexao( cnx );
	}

	/** Construtor que recebe uma inst�ncia de HistoricoBeneficiarioCbo */
	public HistoricoBeneficiarioCboPersistor( HistoricoBeneficiarioCbo historicoBeneficiarioCbo ) throws QtSQLException {

		if( historicoBeneficiarioCbo == null ) 
			throw new QtSQLException( "Imposs�vel instanciar HistoricoBeneficiarioCboPersistor porque est� apontando para um nulo!" );

		this.historicoBeneficiarioCbo = historicoBeneficiarioCbo;
	}

	/** Construtor que recebe uma conex�o e uma inst�ncia de HistoricoBeneficiarioCbo */
	public HistoricoBeneficiarioCboPersistor( Conexao conexao, HistoricoBeneficiarioCbo historicoBeneficiarioCbo ) throws QtSQLException {

		if( historicoBeneficiarioCbo == null ) 
			throw new QtSQLException( "Imposs�vel instanciar HistoricoBeneficiarioCboPersistor porque est� apontando para um nulo!" );

		setConexao( conexao );
		this.historicoBeneficiarioCbo = historicoBeneficiarioCbo;
	}

	/**
	 * Construtor que recebe as chaves da tabela PCMSO.HISTORICO_BENEFICIARIOS_CBO
	 * @param cdBeneficiarioCartao Codigo de Beneficiario 
	 * @param sqAlteracao Sequecial de ateracoes
	 */
	public HistoricoBeneficiarioCboPersistor( String cdBeneficiarioCartao, int sqAlteracao )
	  throws QtSQLException { 

		busca( cdBeneficiarioCartao, sqAlteracao );
	}

	/**
	 * Construtor que recebe uma conex�o e as chaves da tabela PCMSO.HISTORICO_BENEFICIARIOS_CBO
	 * @param cdBeneficiarioCartao Codigo de Beneficiario 
	 * @param sqAlteracao Sequecial de ateracoes
	 * @param conexao Conexao com o banco de dados
	 */
	public HistoricoBeneficiarioCboPersistor( Conexao conexao, String cdBeneficiarioCartao, int sqAlteracao )
	  throws QtSQLException { 

		super.setConexao( conexao );
		busca( cdBeneficiarioCartao, sqAlteracao );
	}

	/**
	 * M�todo interno para buscar e preencher uma tupla de uma tabela
	 * Utiliza a Inst�ncia de HistoricoBeneficiarioCbo para fazer a busca
	 * @param cdBeneficiarioCartao Codigo de Beneficiario 
	 * @param sqAlteracao Sequecial de ateracoes
	 */
	private void busca( String cdBeneficiarioCartao, int sqAlteracao ) throws QtSQLException { 

		HistoricoBeneficiarioCboLocalizador historicoBeneficiarioCboLocalizador = new HistoricoBeneficiarioCboLocalizador( getConexao(), cdBeneficiarioCartao, sqAlteracao );

		if( historicoBeneficiarioCboLocalizador.isEmpty() ) {
			throw new QtSQLException( "Chaves da Tabela HistoricoBeneficiarioCbo n�o encontradas - cdBeneficiarioCartao: " + cdBeneficiarioCartao + " / sqAlteracao: " + sqAlteracao );
		}

		historicoBeneficiarioCbo = (HistoricoBeneficiarioCbo ) historicoBeneficiarioCboLocalizador.getTabelaBasica();
	}

	/**
	 * Permite alimentar o Persistor com um novo registro
	 */
	public void setTabelaBasica( TabelaBasica historicoBeneficiarioCbo ) throws QtException {

		if( historicoBeneficiarioCbo instanceof HistoricoBeneficiarioCbo ) {
			this.historicoBeneficiarioCbo = (HistoricoBeneficiarioCbo) historicoBeneficiarioCbo;
		} else {
			throw new QtException( "Tabela B�sica fornecida tem que ser uma inst�ncia de HistoricoBeneficiarioCbo" );
		}
	}

	/**
	 * Permite acessar o registro corrente do persistor
	 */
	public TabelaBasica getTabelaBasica() throws QtException {

		return historicoBeneficiarioCbo;
	}

	/**
	 * M�todo para gerar automaticamente uma nova chave
	 */
	public static Integer geraNovoSqAlteracao( Conexao cnx, String cdBeneficiarioCartao ) throws QtSQLException {

		Integer result = null;

		Query query = new Query( cnx );

		try{

			query.setSQL( "select max( SQ_ALTERACAO ) from PCMSO.HISTORICO_BENEFICIARIOS_CBO" );

			query.executeQuery();
			result = new Integer( query.getInt( 1 ) + 1 );
		} catch( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			query.liberaRecursos();
		}
		return result;
	}

	/**
	 * M�todo para gerar automaticamente uma nova chave
	 */
	public static Integer geraNovoSqAlteracao( String cdBeneficiarioCartao ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return geraNovoSqAlteracao(  cnx, cdBeneficiarioCartao );
		} finally {
			cnx.libera();
		}
	}


	/**
	 * M�todo para inserir tupla na tabela
	 */
	public void insere() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "insert into PCMSO.HISTORICO_BENEFICIARIOS_CBO" );
			cmd.append( "( CD_BENEFICIARIO_CARTAO, SQ_ALTERACAO, DT_ALTERACAO, ID_SETOR, ID_FUNCAO, ID_CARGO, CD_CBO, DS_ATIVIDADE )" );
			cmd.append( "values " );
			cmd.append( "( ?, ?, ?, ?, ?, ?, ?, ?)" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			ps.setObject( 1, historicoBeneficiarioCbo.getCdBeneficiarioCartao() );
			ps.setObject( 2, historicoBeneficiarioCbo.getSqAlteracao() );
			ps.setObject( 3, QtData.getQtDataAsTimestamp( historicoBeneficiarioCbo.getDtAlteracao() ) );
			ps.setObject( 4, historicoBeneficiarioCbo.getIdSetor() );
			ps.setObject( 5, historicoBeneficiarioCbo.getIdFuncao() );
			ps.setObject( 6, historicoBeneficiarioCbo.getIdCargo() );
			ps.setObject( 7, historicoBeneficiarioCbo.getCdCbo() );
			ps.setObject( 8, historicoBeneficiarioCbo.getDsAtividade() );
			
			ps.execute();

		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}

	/**
	 * M�todo para alterar tupla na tabela
	 */
	public void altera() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "update PCMSO.HISTORICO_BENEFICIARIOS_CBO" );
			cmd.append( "  set CD_BENEFICIARIO_CARTAO = ?, " );
			cmd.append( " SQ_ALTERACAO = ?, " );
			cmd.append( " DT_ALTERACAO = ?, " );
			cmd.append( " ID_SETOR = ?, " );
			cmd.append( " ID_FUNCAO = ?, " );
			cmd.append( " ID_CARGO = ?, " );
			cmd.append( " CD_CBO = ?, " );
			cmd.append( " DS_ATIVIDADE = ?" );
			cmd.append( " where CD_BENEFICIARIO_CARTAO like ? and SQ_ALTERACAO = ?" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			ps.setObject( 1, historicoBeneficiarioCbo.getCdBeneficiarioCartao() );
			ps.setObject( 2, historicoBeneficiarioCbo.getSqAlteracao() );
			ps.setObject( 3, QtData.getQtDataAsTimestamp( historicoBeneficiarioCbo.getDtAlteracao() ) );
			ps.setObject( 4, historicoBeneficiarioCbo.getIdSetor() );
			ps.setObject( 5, historicoBeneficiarioCbo.getIdFuncao() );
			ps.setObject( 6, historicoBeneficiarioCbo.getIdCargo() );
			ps.setObject( 7, historicoBeneficiarioCbo.getCdCbo() );
			ps.setObject( 8, historicoBeneficiarioCbo.getDsAtividade() );
			
			ps.setObject( 9, historicoBeneficiarioCbo.getCdBeneficiarioCartao() );
			ps.setObject( 10, historicoBeneficiarioCbo.getSqAlteracao() );
			
			ps.execute();

		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}

	/**
	 * M�todo para excluir tupla de uma tabela
	 */
	public void exclui() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "delete from PCMSO.HISTORICO_BENEFICIARIOS_CBO" );
			cmd.append( " where CD_BENEFICIARIO_CARTAO like ? and SQ_ALTERACAO = ?" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			ps.setObject( 1, historicoBeneficiarioCbo.getCdBeneficiarioCartao() );
			ps.setObject( 2, historicoBeneficiarioCbo.getSqAlteracao() );
			
			ps.execute();

		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}
}