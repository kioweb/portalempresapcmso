package jPcmso.persistencia.geral.f;

import quatro.persistencia.Caminhador;
import quatro.persistencia.FiltroDeNavegador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.util.QtData;

 /**
 *
 * Classe Caminhador para FatorRisco
 *
 * @author Samuel Antonio Klein
 * @see Caminhador
 * @see FiltroDeNavegador
 * @see FatorRiscoNavegador
 * @see FatorRiscoPersistor
 * @see FatorRiscoLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:31 - importado por Samuel Antonio Klein
 *
 */
 public class FatorRiscoCaminhador extends Caminhador {

	public static final int POR_ORDEM_DEFAULT = -1;
	
	private FatorRisco fatorRisco;

	public FatorRiscoCaminhador( FiltroDeNavegador filtro ) {

		setFiltro( filtro );
		inicializaVisao();
	}

	public FatorRiscoCaminhador( Conexao conexao, FiltroDeNavegador filtro ) throws QtSQLException {

		super.setConexao( conexao );
		setFiltro( filtro );
		inicializaVisao();
	}

	private void inicializaVisao() {

		setNmVisao( "PCMSO.FATORES_RISCOS" );
	}

	protected void carregaCampos() throws QtSQLException {

		fatorRisco = new FatorRisco();

		if( isRecordAvailable() ) {
			fatorRisco.setCdBeneficiarioCartao( query.getString( "CD_BENEFICIARIO_CARTAO" ) );
			fatorRisco.setSqPerfil( query.getInteger( "SQ_PERFIL" ) );
			fatorRisco.setSqFator( query.getInteger( "SQ_FATOR" ) );
			fatorRisco.setDtInicio(  QtData.criaQtData( query.getTimestamp( "DT_INICIO" ) ) );
			fatorRisco.setDtFinal(  QtData.criaQtData( query.getTimestamp( "DT_FINAL" ) ) );
			fatorRisco.setIdRisco( query.getInteger( "ID_RISCO" ) );
			fatorRisco.setDsIntensidadeConcentracao( query.getString( "DS_INTENSIDADE_CONCENTRACAO" ) );
			fatorRisco.setDsTecnicaUtilizada( query.getString( "DS_TECNICA_UTILIZADA" ) );
			fatorRisco.setSnEpcEficaz( query.getString( "SN_EPC_EFICAZ" ) );
			fatorRisco.setSnEpiEficaz( query.getString( "SN_EPI_EFICAZ" ) );
			fatorRisco.setNrCaEpi( query.getString( "NR_CA_EPI" ) );
		}
	}

	/** M�todo que retorna uma tabela b�sica a que se refere o caminhador */
	public TabelaBasica getTabelaBasica() {

		return this.fatorRisco;
	}

	/** M�todo que retorna a classe FatorRisco relacionada ao caminhador */
	public FatorRisco getFatorRisco() {

		return this.fatorRisco;
	}

	public void ativaVisao() throws QtSQLException {

		if(  super.getOrdemDeNavegacao() == FatorRiscoCaminhador.POR_ORDEM_DEFAULT ) {
			super.ativaCaminhador( "order by CD_BENEFICIARIO_CARTAO, SQ_PERFIL, SQ_FATOR" );
	
		} else {
			throw new QtSQLException( "FatorRiscoCaminhador - Ordem de Navega��o n�o definida!" );
		}
	}

	public String getCamposIndice( int qualIndice ) {
		return getDsIndice( qualIndice );
	}

	public String getCamposChave() {
		return getDsChave();
	}

	/**
	 * M�todo est�tico que devolve os campos que comp�e a chave da tabela;
	 * @return campos que comp�e a chave.
	 */
	public static String getDsChave() {
		return "CD_BENEFICIARIO_CARTAO, SQ_PERFIL, SQ_FATOR";
	}

	/**
	 * M�todo est�tico que devolve os campos que fazem parte de uma dada ordem
	 * de navega��o.
	 * @param qualIndice indica a ordem de navega��o desejada.
	 * @return campos que comp�e a ordem de navega��o.
	 */
	public static String getDsIndice( int qualIndice ) {

		switch( qualIndice ) {

			case POR_ORDEM_DEFAULT: return "CD_BENEFICIARIO_CARTAO, SQ_PERFIL, SQ_FATOR";
		}
		return null;
	}
}