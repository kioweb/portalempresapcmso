package jPcmso.persistencia.geral.f;

import quatro.persistencia.Caminhador;
import quatro.persistencia.FiltroDeNavegador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;

 /**
 *
 * Classe Caminhador para Funcao
 *
 * @author ronei@unimed-uau.com.br
 * @see Caminhador
 * @see FiltroDeNavegador
 * @see FuncaoNavegador
 * @see FuncaoPersistor
 * @see FuncaoLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 14/12/2017 15:26 - importado por ronei@unimed-uau.com.br
 *
 */
 public class FuncaoCaminhador extends Caminhador {

	public static final int POR_ORDEM_DEFAULT = -1;
	public static final int POR_DS_FUNCAO = 0;
	
	private Funcao funcao;

	public FuncaoCaminhador( FiltroDeNavegador filtro ) {

		setFiltro( filtro );
		inicializaVisao();
	}

	public FuncaoCaminhador( Conexao conexao, FiltroDeNavegador filtro ) throws QtSQLException {

		super.setConexao( conexao );
		setFiltro( filtro );
		inicializaVisao();
	}

	private void inicializaVisao() {

		setNmVisao( "PCMSO.FUNCOES" );
	}

	protected void carregaCampos() throws QtSQLException {

		funcao = new Funcao();

		if( isRecordAvailable() ) {
			funcao.setIdFuncao( query.getInteger( "ID_FUNCAO" ) );
			funcao.setDsFuncao( query.getString( "DS_FUNCAO" ) );
		}
	}

	/** Método que retorna uma tabela básica a que se refere o caminhador */
	public TabelaBasica getTabelaBasica() {

		return this.funcao;
	}

	/** Método que retorna a classe Funcao relacionada ao caminhador */
	public Funcao getFuncao() {

		return this.funcao;
	}

	public void ativaVisao() throws QtSQLException {

		if(  super.getOrdemDeNavegacao() == FuncaoCaminhador.POR_ORDEM_DEFAULT ) {
			super.ativaCaminhador( "order by ID_FUNCAO" );
		} else if(  super.getOrdemDeNavegacao() == FuncaoCaminhador.POR_DS_FUNCAO ) {
			super.ativaCaminhador( "order by DS_FUNCAO" );
	
		} else {
			throw new QtSQLException( "FuncaoCaminhador - Ordem de Navegação não definida!" );
		}
	}

	public String getCamposIndice( int qualIndice ) {
		return getDsIndice( qualIndice );
	}

	public String getCamposChave() {
		return getDsChave();
	}

	/**
	 * Método estático que devolve os campos que compõe a chave da tabela;
	 * @return campos que compõe a chave.
	 */
	public static String getDsChave() {
		return "ID_FUNCAO";
	}

	/**
	 * Método estático que devolve os campos que fazem parte de uma dada ordem
	 * de navegação.
	 * @param qualIndice indica a ordem de navegação desejada.
	 * @return campos que compõe a ordem de navegação.
	 */
	public static String getDsIndice( int qualIndice ) {

		switch( qualIndice ) {

			case POR_ORDEM_DEFAULT: return "ID_FUNCAO";
			case POR_DS_FUNCAO: return "DS_FUNCAO";
		}
		return null;
	}
}