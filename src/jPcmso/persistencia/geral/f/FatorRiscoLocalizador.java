package jPcmso.persistencia.geral.f;

import quatro.persistencia.Localizador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.QtSQLException;
import quatro.sql.Query;
import quatro.sql.Conexao;
import quatro.sql.PoolDeConexoes;
import quatro.sql.ConexaoParaPoolDeConexoes;
import quatro.util.QtData;

/**
 *
 * Classe localizador para PCMSO.FATORES_RISCOS
 *
 * @author Samuel Antonio Klein
 * @see Localizador
 * @see FatorRisco
 * @see FatorRiscoNavegador
 * @see FatorRiscoPersistor
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:31 - importado por Samuel Antonio Klein
 *
 */
 public class FatorRiscoLocalizador extends Localizador {

	/** Inst�ncia de FatorRisco para manipula��o pelo localizador */
	private FatorRisco fatorRisco;

	/** Construtor que recebe uma conex�o e um objeto FatorRisco completo */
	public FatorRiscoLocalizador( Conexao conexao, FatorRisco fatorRisco ) throws QtSQLException {

		super.setConexao( conexao );

		this.fatorRisco = fatorRisco;
		busca();
	}

	/** Construtor que recebe um objeto FatorRisco completo */
	public FatorRiscoLocalizador( FatorRisco fatorRisco ) throws QtSQLException {

		this.fatorRisco = fatorRisco;
		busca();
	}

	/** 
	 * Construtor que recebe uma conex�o e as chaves da tabela PCMSO.FATORES_RISCOS
	 * @param cdBeneficiarioCartao Codigo de Beneficiario 
	 * @param sqPerfil sequencia perfil
	 * @param sqFator sequencia fator
	 * @param conexao Conexao com o banco de dados
	 */
	public FatorRiscoLocalizador( Conexao conexao, String cdBeneficiarioCartao, int sqPerfil, int sqFator ) throws QtSQLException { 

		super.setConexao( conexao ); 
		fatorRisco = new FatorRisco();
		this.fatorRisco.setCdBeneficiarioCartao( cdBeneficiarioCartao );
		this.fatorRisco.setSqPerfil( new Integer( sqPerfil ) );
		this.fatorRisco.setSqFator( new Integer( sqFator ) );
		
		busca();
	}

	/** 
	 * Construtor que recebe as chaves da tabela PCMSO.FATORES_RISCOS
	 * @param cdBeneficiarioCartao Codigo de Beneficiario 
	 * @param sqPerfil sequencia perfil
	 * @param sqFator sequencia fator
	 */
	public FatorRiscoLocalizador( String cdBeneficiarioCartao, int sqPerfil, int sqFator ) throws QtSQLException { 

		fatorRisco = new FatorRisco();
		this.fatorRisco.setCdBeneficiarioCartao( cdBeneficiarioCartao );
		this.fatorRisco.setSqPerfil( new Integer( sqPerfil ) );
		this.fatorRisco.setSqFator( new Integer( sqFator ) );
		
		busca();
	}

	public static boolean existe( Conexao cnx, String cdBeneficiarioCartao, int sqPerfil, int sqFator ) throws QtSQLException {

		Query q = new Query( cnx );

		try {
			q.setSQL( "select count(*) from PCMSO.FATORES_RISCOS" );
			q.addSQL( " where CD_BENEFICIARIO_CARTAO = :prm1CdBeneficiarioCartao and SQ_PERFIL = :prm2SqPerfil and SQ_FATOR = :prm3SqFator" );

			q.setParameter( "prm1CdBeneficiarioCartao", cdBeneficiarioCartao );
			q.setParameter( "prm2SqPerfil", sqPerfil );
			q.setParameter( "prm3SqFator", sqFator );
			q.executeQuery();

			return q.getInt( 1 ) > 0;
		} finally {
			q.liberaRecursos();
		}
	}

	public static boolean existe( String cdBeneficiarioCartao, int sqPerfil, int sqFator ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return existe( cnx, cdBeneficiarioCartao, sqPerfil, sqFator );
		} finally {
			cnx.libera();
		}
	}

	public static boolean existe( Conexao cnx, FatorRisco fatorRisco ) throws QtSQLException {

		return existe( cnx, fatorRisco.getCdBeneficiarioCartao(), fatorRisco.getSqPerfil(), fatorRisco.getSqFator() );
	}

	public static boolean existe( FatorRisco fatorRisco ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return existe( cnx, fatorRisco.getCdBeneficiarioCartao(), fatorRisco.getSqPerfil(), fatorRisco.getSqFator() );
		} finally {
			cnx.libera();
		}
	}

	public static FatorRisco buscaFatorRisco( Conexao cnx, String cdBeneficiarioCartao, int sqPerfil, int sqFator ) throws QtSQLException {

		Query q = new Query( cnx );

		try {
			q.setSQL( "select * from PCMSO.FATORES_RISCOS" );
			q.addSQL( " where CD_BENEFICIARIO_CARTAO = :prm1CdBeneficiarioCartao and SQ_PERFIL = :prm2SqPerfil and SQ_FATOR = :prm3SqFator" );

			q.setParameter( "prm1CdBeneficiarioCartao", cdBeneficiarioCartao );
			q.setParameter( "prm2SqPerfil", sqPerfil );
			q.setParameter( "prm3SqFator", sqFator );
			q.executeQuery();

			if( q.isEmpty() ) {
				return null;
			} else {
				FatorRisco fatorRisco = new FatorRisco();
				buscaCampos( fatorRisco, q );
				return fatorRisco;
			}
		} finally {
			q.liberaRecursos();
		}
	}

	public static FatorRisco buscaFatorRisco( String cdBeneficiarioCartao, int sqPerfil, int sqFator ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return buscaFatorRisco( cnx, cdBeneficiarioCartao, sqPerfil, sqFator );
		} finally { 
			cnx.libera();
		}
	}

	/** 
	 * Construtor que recebe uma conex�o e as chaves da tabela PCMSO.FATORES_RISCOS
	 * @param cdBeneficiarioCartao Codigo de Beneficiario 
	 * @param sqPerfil sequencia perfil
	 * @param sqFator sequencia fator
	 * @param conexao Conexao com o banco de dados
	 */
	public FatorRiscoLocalizador( Conexao conexao, String cdBeneficiarioCartao, Integer sqPerfil, Integer sqFator ) throws QtSQLException {

		super.setConexao( conexao ); 
		fatorRisco = new FatorRisco();
		this.fatorRisco.setCdBeneficiarioCartao( cdBeneficiarioCartao );
		this.fatorRisco.setSqPerfil( sqPerfil );
		this.fatorRisco.setSqFator( sqFator );
		
		busca();
	}

	/** 
	 * Construtor que recebe as chaves da tabela PCMSO.FATORES_RISCOS
	 * @param cdBeneficiarioCartao Codigo de Beneficiario 
	 * @param sqPerfil sequencia perfil
	 * @param sqFator sequencia fator
	 */
	public FatorRiscoLocalizador( String cdBeneficiarioCartao, Integer sqPerfil, Integer sqFator ) throws QtSQLException {

		fatorRisco = new FatorRisco();
		this.fatorRisco.setCdBeneficiarioCartao( cdBeneficiarioCartao );
		this.fatorRisco.setSqPerfil( sqPerfil );
		this.fatorRisco.setSqFator( sqFator );
		
		busca();
	}

	public static void buscaCampos( FatorRisco fatorRisco, Query query ) throws QtSQLException {

		fatorRisco.setCdBeneficiarioCartao( query.getString( "CD_BENEFICIARIO_CARTAO" ) );
		fatorRisco.setSqPerfil( query.getInteger( "SQ_PERFIL" ) );
		fatorRisco.setSqFator( query.getInteger( "SQ_FATOR" ) );
		fatorRisco.setDtInicio( QtData.criaQtData( query.getTimestamp( "DT_INICIO" ) ) );
		fatorRisco.setDtFinal( QtData.criaQtData( query.getTimestamp( "DT_FINAL" ) ) );
		fatorRisco.setIdRisco( query.getInteger( "ID_RISCO" ) );
		fatorRisco.setDsIntensidadeConcentracao( query.getString( "DS_INTENSIDADE_CONCENTRACAO" ) );
		fatorRisco.setDsTecnicaUtilizada( query.getString( "DS_TECNICA_UTILIZADA" ) );
		fatorRisco.setSnEpcEficaz( query.getString( "SN_EPC_EFICAZ" ) );
		fatorRisco.setSnEpiEficaz( query.getString( "SN_EPI_EFICAZ" ) );
		fatorRisco.setNrCaEpi( query.getString( "NR_CA_EPI" ) );
	}

	/** M�todo que retorna uma tabela b�sica a que se refere o localizador */
	public TabelaBasica getTabelaBasica() {

		return this.fatorRisco;
	}

	/** M�todo que retorna a classe FatorRisco relacionada ao localizador */
	public FatorRisco getFatorRisco() {

		return this.fatorRisco;
	}

	/**
	 * M�todo para buscar e preencher uma tupla de uma tabela
	 * @param tabelaBasica Recebe uma tabela basica com os atributos chave preenchidos
	 */
	public boolean busca( TabelaBasica tabelaBasica ) throws QtSQLException {

		if( !( tabelaBasica instanceof FatorRisco ) )
			throw new QtSQLException( "Esperava um objeto FatorRisco!" );

		this.fatorRisco = (FatorRisco) tabelaBasica;
		return busca();

	}

	/**
	 * M�todo privado para buscar e preencher uma tupla de uma tabela
	 * Este m�todo busca os dados da inst�ncia de FatorRisco atual
	 */
	private boolean busca() throws QtSQLException {

		boolean ret = false;

		super.estabeleceConexao();
		Query query = super.criaNovaQuery();

		try {

			query.setSQL( "select * from PCMSO.FATORES_RISCOS" );
			query.addSQL( " where CD_BENEFICIARIO_CARTAO like :prm1CdBeneficiarioCartao and SQ_PERFIL = :prm2SqPerfil and SQ_FATOR = :prm3SqFator" );

			query.setParameter( "prm1CdBeneficiarioCartao", fatorRisco.getCdBeneficiarioCartao() );
			query.setParameter( "prm2SqPerfil", fatorRisco.getSqPerfil() );
			query.setParameter( "prm3SqFator", fatorRisco.getSqFator() );
			query.executeQuery();

			if( query.isEmpty() ) {
				super.setEmpty( true );
				fatorRisco = null;
			} else {
				super.setEmpty( false );
				
				fatorRisco.setCdBeneficiarioCartao( query.getString( "CD_BENEFICIARIO_CARTAO" ) );
				fatorRisco.setSqPerfil( query.getInteger( "SQ_PERFIL" ) );
				fatorRisco.setSqFator( query.getInteger( "SQ_FATOR" ) );
				fatorRisco.setDtInicio( QtData.criaQtData( query.getTimestamp( "DT_INICIO" ) ) );
				fatorRisco.setDtFinal( QtData.criaQtData( query.getTimestamp( "DT_FINAL" ) ) );
				fatorRisco.setIdRisco( query.getInteger( "ID_RISCO" ) );
				fatorRisco.setDsIntensidadeConcentracao( query.getString( "DS_INTENSIDADE_CONCENTRACAO" ) );
				fatorRisco.setDsTecnicaUtilizada( query.getString( "DS_TECNICA_UTILIZADA" ) );
				fatorRisco.setSnEpcEficaz( query.getString( "SN_EPC_EFICAZ" ) );
				fatorRisco.setSnEpiEficaz( query.getString( "SN_EPI_EFICAZ" ) );
				fatorRisco.setNrCaEpi( query.getString( "NR_CA_EPI" ) );
				ret = true;
			}
		} finally {
			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}
 }