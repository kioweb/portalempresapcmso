package jPcmso.persistencia.geral.f;

import quatro.persistencia.Localizador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.QtSQLException;
import quatro.sql.Query;
import quatro.sql.Conexao;
import quatro.sql.PoolDeConexoes;
import quatro.sql.ConexaoParaPoolDeConexoes;

/**
 *
 * Classe localizador para PCMSO.FUNCOES
 *
 * @author ronei@unimed-uau.com.br
 * @see Localizador
 * @see Funcao
 * @see FuncaoNavegador
 * @see FuncaoPersistor
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 14/12/2017 15:26 - importado por ronei@unimed-uau.com.br
 *
 */
 public class FuncaoLocalizador extends Localizador {

	/** Instância de Funcao para manipulação pelo localizador */
	private Funcao funcao;

	/** Construtor que recebe uma conexão e um objeto Funcao completo */
	public FuncaoLocalizador( Conexao conexao, Funcao funcao ) throws QtSQLException {

		super.setConexao( conexao );

		this.funcao = funcao;
		busca();
	}

	/** Construtor que recebe um objeto Funcao completo */
	public FuncaoLocalizador( Funcao funcao ) throws QtSQLException {

		this.funcao = funcao;
		busca();
	}

	/** 
	 * Construtor que recebe uma conexão e a chave da tabela PCMSO.FUNCOES
	 * @param idFuncao Codigo da funcao
	 * @param conexao Conexao com o banco de dados
	 */
	public FuncaoLocalizador( Conexao conexao, int idFuncao ) throws QtSQLException { 

		super.setConexao( conexao ); 
		funcao = new Funcao();
		this.funcao.setIdFuncao( new Integer( idFuncao ) );
		
		busca();
	}

	/** 
	 * Construtor que recebe a chave da tabela PCMSO.FUNCOES
	 * @param idFuncao Codigo da funcao
	 */
	public FuncaoLocalizador( int idFuncao ) throws QtSQLException { 

		funcao = new Funcao();
		this.funcao.setIdFuncao( new Integer( idFuncao ) );
		
		busca();
	}

	public static boolean existe( Conexao cnx, int idFuncao ) throws QtSQLException {

		Query q = new Query( cnx );

		try {
			q.setSQL( "select count(*) from PCMSO.FUNCOES" );
			q.addSQL( " where ID_FUNCAO = :prm1IdFuncao" );

			q.setParameter( "prm1IdFuncao", idFuncao );
			q.executeQuery();

			return q.getInt( 1 ) > 0;
		} finally {
			q.liberaRecursos();
		}
	}

	public static boolean existe( int idFuncao ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return existe( cnx, idFuncao );
		} finally {
			cnx.libera();
		}
	}

	public static boolean existe( Conexao cnx, Funcao funcao ) throws QtSQLException {

		return existe( cnx, funcao.getIdFuncao() );
	}

	public static boolean existe( Funcao funcao ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return existe( cnx, funcao.getIdFuncao() );
		} finally {
			cnx.libera();
		}
	}

	public static Funcao buscaFuncao( Conexao cnx, int idFuncao ) throws QtSQLException {

		Query q = new Query( cnx );

		try {
			q.setSQL( "select * from PCMSO.FUNCOES" );
			q.addSQL( " where ID_FUNCAO = :prm1IdFuncao" );

			q.setParameter( "prm1IdFuncao", idFuncao );
			q.executeQuery();

			if( q.isEmpty() ) {
				return null;
			} else {
				Funcao funcao = new Funcao();
				buscaCampos( funcao, q );
				return funcao;
			}
		} finally {
			q.liberaRecursos();
		}
	}

	public static Funcao buscaFuncao( int idFuncao ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return buscaFuncao( cnx, idFuncao );
		} finally { 
			cnx.libera();
		}
	}

	/** 
	 * Construtor que recebe uma conexão e a chave da tabela PCMSO.FUNCOES
	 * @param idFuncao Codigo da funcao
	 * @param conexao Conexao com o banco de dados
	 */
	public FuncaoLocalizador( Conexao conexao, Integer idFuncao ) throws QtSQLException {

		super.setConexao( conexao ); 
		funcao = new Funcao();
		this.funcao.setIdFuncao( idFuncao );
		
		busca();
	}

	/** 
	 * Construtor que recebe a chave da tabela PCMSO.FUNCOES
	 * @param idFuncao Codigo da funcao
	 */
	public FuncaoLocalizador( Integer idFuncao ) throws QtSQLException {

		funcao = new Funcao();
		this.funcao.setIdFuncao( idFuncao );
		
		busca();
	}

	public static void buscaCampos( Funcao funcao, Query query ) throws QtSQLException {

		funcao.setIdFuncao( query.getInteger( "ID_FUNCAO" ) );
		funcao.setDsFuncao( query.getString( "DS_FUNCAO" ) );
	}

	/** Método que retorna uma tabela básica a que se refere o localizador */
	public TabelaBasica getTabelaBasica() {

		return this.funcao;
	}

	/** Método que retorna a classe Funcao relacionada ao localizador */
	public Funcao getFuncao() {

		return this.funcao;
	}

	/**
	 * Método para buscar e preencher uma tupla de uma tabela
	 * @param tabelaBasica Recebe uma tabela basica com os atributos chave preenchidos
	 */
	public boolean busca( TabelaBasica tabelaBasica ) throws QtSQLException {

		if( !( tabelaBasica instanceof Funcao ) )
			throw new QtSQLException( "Esperava um objeto Funcao!" );

		this.funcao = (Funcao) tabelaBasica;
		return busca();

	}

	/**
	 * Método privado para buscar e preencher uma tupla de uma tabela
	 * Este método busca os dados da instância de Funcao atual
	 */
	private boolean busca() throws QtSQLException {

		boolean ret = false;

		super.estabeleceConexao();
		Query query = super.criaNovaQuery();

		try {

			query.setSQL( "select * from PCMSO.FUNCOES" );
			query.addSQL( " where ID_FUNCAO = :prm1IdFuncao" );

			query.setParameter( "prm1IdFuncao", funcao.getIdFuncao() );
			query.executeQuery();

			if( query.isEmpty() ) {
				super.setEmpty( true );
				funcao = null;
			} else {
				super.setEmpty( false );
				
				funcao.setIdFuncao( query.getInteger( "ID_FUNCAO" ) );
				funcao.setDsFuncao( query.getString( "DS_FUNCAO" ) );
				ret = true;
			}
		} finally {
			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}
 }