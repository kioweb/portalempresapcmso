package jPcmso.persistencia.geral.f;

import quatro.persistencia.TabelaBasica;
import quatro.smartDB.IgnoredField;
import quatro.smartDB.PrimaryKey;
import quatro.smartDB.Table;

/**
 *
 * Classe básica para PCMSO.FILIAIS_EMPRESAS
 *
 * Esta tabela conterá as Filiais de Empresas.
 * 
 * @author ronei@unimed-uau.com.br
 * @see FiliaisEmpresaNavegador
 * @see FiliaisEmpresaPersistor
 * @see FiliaisEmpresaLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 21/12/2017 07:46 - importado por ronei@unimed-uau.com.br
 *
 */
 @Table(name="PCMSO.FILIAIS_EMPRESAS")
 public class FiliaisEmpresa implements TabelaBasica {

	@IgnoredField
	private static final long serialVersionUID = -87255555385197933L;

	/** Codigo da Empresa (chave-char(4)) */
	@PrimaryKey
	private String cdEmpresa;
	/** Identificador da Filial (chave) */
	@PrimaryKey
	private Integer idFilial;
	/** Nome da Filial (varchar(100)) */
	private String dsFilial;
	/** Número do CNPJ da filial (varchar(18)) */
	private String nrCnpj;

	/**
	 * Alimenta Codigo da Empresa
	 * @param cdEmpresa Codigo da Empresa
	 */ 
	public void setCdEmpresa( String cdEmpresa ) {
		this.cdEmpresa = cdEmpresa;
	}

	/**
	 * Retorna Codigo da Empresa
	 * @return Codigo da Empresa
	 */
	public String getCdEmpresa() {
		return this.cdEmpresa;
	}

	/**
	 * Alimenta Identificador da Filial
	 * @param idFilial Identificador da Filial
	 */ 
	public void setIdFilial( Integer idFilial ) {
		this.idFilial = idFilial;
	}

	/**
	 * Retorna Identificador da Filial
	 * @return Identificador da Filial
	 */
	public Integer getIdFilial() {
		return this.idFilial;
	}

	/**
	 * Alimenta Nome da Filial
	 * @param dsFilial Nome da Filial
	 */ 
	public void setDsFilial( String dsFilial ) {
		this.dsFilial = dsFilial;
	}

	/**
	 * Retorna Nome da Filial
	 * @return Nome da Filial
	 */
	public String getDsFilial() {
		return this.dsFilial;
	}

	/**
	 * Alimenta Número do CNPJ da filial
	 * @param nrCnpj Número do CNPJ da filial
	 */ 
	public void setNrCnpj( String nrCnpj ) {
		this.nrCnpj = nrCnpj;
	}

	/**
	 * Retorna Número do CNPJ da filial
	 * @return Número do CNPJ da filial
	 */
	public String getNrCnpj() {
		return this.nrCnpj;
	}

}