package jPcmso.persistencia.geral.f;

import quatro.persistencia.TabelaBasica;
import quatro.smartDB.IgnoredField;
import quatro.smartDB.PrimaryKey;
import quatro.smartDB.Table;
import quatro.util.QtData;
import quatro.util.QtString;

/**
 *
 * Classe b�sica para PCMSO.FATORES_RISCOS
 *
 * Tabela de Beneficiarios CBO
 * 
 * @author Samuel Antonio Klein
 * @see FatorRiscoNavegador
 * @see FatorRiscoPersistor
 * @see FatorRiscoLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:31 - importado por Samuel Antonio Klein
 *
 */
 @Table(name="PCMSO.FATORES_RISCOS")
 public class FatorRisco implements TabelaBasica {

	@IgnoredField
	private static final long serialVersionUID = -541526547291637773L;

	/** Codigo de Beneficiario  (chave-char(12)) */
	@PrimaryKey
	private String cdBeneficiarioCartao;
	/** sequencia perfil (chave) */
	@PrimaryKey
	private Integer sqPerfil;
	/** sequencia fator (chave) */
	@PrimaryKey
	private Integer sqFator;
	/** data de Inicio */
	private QtData dtInicio;
	/** data Final */
	private QtData dtFinal;
	/** identificador de Risco */
	private Integer idRisco;
	/** Intensidade / Concentra��o (varchar(15)) */
	private String dsIntensidadeConcentracao;
	/** T�cnica Utilizada (varchar(40)) */
	private String dsTecnicaUtilizada;
	/** considerando se houve ou n�o a elimina��o ou a neutraliza��o, com base no informado nos itens 15.2 a 15.5, assegurada as condi��es de funcionamento do EPC ao longo do tempo, conforme especifica��o t�cnica do fabricante e respectivo plano de manuten��o. (char(1)) */
	private boolean snEpcEficaz;
	/** considerando se houve ou n�o a atenua��o, com base no informado nos itens 15.2 a 15.5. (char(1)) */
	private boolean snEpiEficaz;
	/** N�mero do Certificado de Aprova��o do MTE para o Equipamento de Prote��o Individual referido no campo 154.7, com 5 (cinco) caracteres num�ricos. (char(5)) */
	private String nrCaEpi;

	/**
	 * Alimenta Codigo de Beneficiario 
	 * @param cdBeneficiarioCartao Codigo de Beneficiario 
	 */ 
	public void setCdBeneficiarioCartao( String cdBeneficiarioCartao ) {
		this.cdBeneficiarioCartao = cdBeneficiarioCartao;
	}

	/**
	 * Retorna Codigo de Beneficiario 
	 * @return Codigo de Beneficiario 
	 */
	public String getCdBeneficiarioCartao() {
		return this.cdBeneficiarioCartao;
	}

	/**
	 * Alimenta sequencia perfil
	 * @param sqPerfil sequencia perfil
	 */ 
	public void setSqPerfil( Integer sqPerfil ) {
		this.sqPerfil = sqPerfil;
	}

	/**
	 * Retorna sequencia perfil
	 * @return sequencia perfil
	 */
	public Integer getSqPerfil() {
		return this.sqPerfil;
	}

	/**
	 * Alimenta sequencia fator
	 * @param sqFator sequencia fator
	 */ 
	public void setSqFator( Integer sqFator ) {
		this.sqFator = sqFator;
	}

	/**
	 * Retorna sequencia fator
	 * @return sequencia fator
	 */
	public Integer getSqFator() {
		return this.sqFator;
	}

	/**
	 * Alimenta data de Inicio
	 * @param dtInicio data de Inicio
	 */ 
	public void setDtInicio( QtData dtInicio ) {
		this.dtInicio = dtInicio;
	}

	/**
	 * Retorna data de Inicio
	 * @return data de Inicio
	 */
	public QtData getDtInicio() {
		return this.dtInicio;
	}

	/**
	 * Alimenta data Final
	 * @param dtFinal data Final
	 */ 
	public void setDtFinal( QtData dtFinal ) {
		this.dtFinal = dtFinal;
	}

	/**
	 * Retorna data Final
	 * @return data Final
	 */
	public QtData getDtFinal() {
		return this.dtFinal;
	}

	/**
	 * Alimenta identificador de Risco
	 * @param idRisco identificador de Risco
	 */ 
	public void setIdRisco( Integer idRisco ) {
		this.idRisco = idRisco;
	}

	/**
	 * Retorna identificador de Risco
	 * @return identificador de Risco
	 */
	public Integer getIdRisco() {
		return this.idRisco;
	}

	/**
	 * Alimenta Intensidade / Concentra��o
	 * @param dsIntensidadeConcentracao Intensidade / Concentra��o
	 */ 
	public void setDsIntensidadeConcentracao( String dsIntensidadeConcentracao ) {
		this.dsIntensidadeConcentracao = dsIntensidadeConcentracao;
	}

	/**
	 * Retorna Intensidade / Concentra��o
	 * @return Intensidade / Concentra��o
	 */
	public String getDsIntensidadeConcentracao() {
		return this.dsIntensidadeConcentracao;
	}

	/**
	 * Alimenta T�cnica Utilizada
	 * @param dsTecnicaUtilizada T�cnica Utilizada
	 */ 
	public void setDsTecnicaUtilizada( String dsTecnicaUtilizada ) {
		this.dsTecnicaUtilizada = dsTecnicaUtilizada;
	}

	/**
	 * Retorna T�cnica Utilizada
	 * @return T�cnica Utilizada
	 */
	public String getDsTecnicaUtilizada() {
		return this.dsTecnicaUtilizada;
	}

	/**
	 * Alimenta considerando se houve ou n�o a elimina��o ou a neutraliza��o, com base no informado nos itens 15.2 a 15.5, assegurada as condi��es de funcionamento do EPC ao longo do tempo, conforme especifica��o t�cnica do fabricante e respectivo plano de manuten��o.
	 * @param snEpcEficaz considerando se houve ou n�o a elimina��o ou a neutraliza��o, com base no informado nos itens 15.2 a 15.5, assegurada as condi��es de funcionamento do EPC ao longo do tempo, conforme especifica��o t�cnica do fabricante e respectivo plano de manuten��o.
	 */ 
	public void setSnEpcEficaz( String snEpcEficaz ) {
		this.snEpcEficaz = QtString.toBoolean( snEpcEficaz );
	}

	/**
	 * Retorna considerando se houve ou n�o a elimina��o ou a neutraliza��o, com base no informado nos itens 15.2 a 15.5, assegurada as condi��es de funcionamento do EPC ao longo do tempo, conforme especifica��o t�cnica do fabricante e respectivo plano de manuten��o.
	 * @return considerando se houve ou n�o a elimina��o ou a neutraliza��o, com base no informado nos itens 15.2 a 15.5, assegurada as condi��es de funcionamento do EPC ao longo do tempo, conforme especifica��o t�cnica do fabricante e respectivo plano de manuten��o.
	 */
	public boolean isSnEpcEficaz() {
		return this.snEpcEficaz;
	}

	/**
	 * Alimenta considerando se houve ou n�o a atenua��o, com base no informado nos itens 15.2 a 15.5.
	 * @param snEpiEficaz considerando se houve ou n�o a atenua��o, com base no informado nos itens 15.2 a 15.5.
	 */ 
	public void setSnEpiEficaz( String snEpiEficaz ) {
		this.snEpiEficaz = QtString.toBoolean( snEpiEficaz );
	}

	/**
	 * Retorna considerando se houve ou n�o a atenua��o, com base no informado nos itens 15.2 a 15.5.
	 * @return considerando se houve ou n�o a atenua��o, com base no informado nos itens 15.2 a 15.5.
	 */
	public boolean isSnEpiEficaz() {
		return this.snEpiEficaz;
	}

	/**
	 * Alimenta N�mero do Certificado de Aprova��o do MTE para o Equipamento de Prote��o Individual referido no campo 154.7, com 5 (cinco) caracteres num�ricos.
	 * @param nrCaEpi N�mero do Certificado de Aprova��o do MTE para o Equipamento de Prote��o Individual referido no campo 154.7, com 5 (cinco) caracteres num�ricos.
	 */ 
	public void setNrCaEpi( String nrCaEpi ) {
		this.nrCaEpi = nrCaEpi;
	}

	/**
	 * Retorna N�mero do Certificado de Aprova��o do MTE para o Equipamento de Prote��o Individual referido no campo 154.7, com 5 (cinco) caracteres num�ricos.
	 * @return N�mero do Certificado de Aprova��o do MTE para o Equipamento de Prote��o Individual referido no campo 154.7, com 5 (cinco) caracteres num�ricos.
	 */
	public String getNrCaEpi() {
		return this.nrCaEpi;
	}

}