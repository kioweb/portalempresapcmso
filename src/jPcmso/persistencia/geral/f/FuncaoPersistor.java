package jPcmso.persistencia.geral.f;

import java.sql.PreparedStatement;

import quatro.persistencia.Persistor;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.ConexaoParaPoolDeConexoes;
import quatro.sql.PoolDeConexoes;
import quatro.sql.QtSQLException;
import quatro.sql.Query;
import quatro.util.QtException;

/**
 *
 * Classe persistor para PCMSO.FUNCOES
 *
 * @author ronei@unimed-uau.com.br
 * @see Persistor
 * @see Funcao
 * @see FuncaoNavegador
 * @see FuncaoLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 14/12/2017 15:26 - importado por ronei@unimed-uau.com.br
 *
 */
 public class FuncaoPersistor extends Persistor {

	/** Instância de Funcao para manipulação pelo persistor */
	private Funcao funcao;

	/** Construtor genérico */
	public FuncaoPersistor() {

		this.funcao = null;
	}

	/** Construtor genérico */
	public FuncaoPersistor( Conexao cnx ) throws QtSQLException {

		this.funcao = null;
		super.setConexao( cnx );
	}

	/** Construtor que recebe uma instância de Funcao */
	public FuncaoPersistor( Funcao funcao ) throws QtSQLException {

		if( funcao == null ) 
			throw new QtSQLException( "Impossível instanciar FuncaoPersistor porque está apontando para um nulo!" );

		this.funcao = funcao;
	}

	/** Construtor que recebe uma conexão e uma instância de Funcao */
	public FuncaoPersistor( Conexao conexao, Funcao funcao ) throws QtSQLException {

		if( funcao == null ) 
			throw new QtSQLException( "Impossível instanciar FuncaoPersistor porque está apontando para um nulo!" );

		setConexao( conexao );
		this.funcao = funcao;
	}

	/**
	 * Construtor que recebe a chave da tabela PCMSO.FUNCOES
	 * @param idFuncao Codigo da funcao
	 */
	public FuncaoPersistor( int idFuncao )
	  throws QtSQLException { 

		busca( idFuncao );
	}

	/**
	 * Construtor que recebe uma conexão e a chave da tabela PCMSO.FUNCOES
	 * @param idFuncao Codigo da funcao
	 * @param conexao Conexao com o banco de dados
	 */
	public FuncaoPersistor( Conexao conexao, int idFuncao )
	  throws QtSQLException { 

		super.setConexao( conexao );
		busca( idFuncao );
	}

	/**
	 * Método interno para buscar e preencher uma tupla de uma tabela
	 * Utiliza a Instância de Funcao para fazer a busca
	 * @param idFuncao Codigo da funcao
	 */
	private void busca( int idFuncao ) throws QtSQLException { 

		FuncaoLocalizador funcaoLocalizador = new FuncaoLocalizador( getConexao(), idFuncao );

		if( funcaoLocalizador.isEmpty() ) {
			throw new QtSQLException( "Chaves da Tabela Funcao não encontradas - idFuncao: " + idFuncao );
		}

		funcao = (Funcao ) funcaoLocalizador.getTabelaBasica();
	}

	/**
	 * Permite alimentar o Persistor com um novo registro
	 */
	public void setTabelaBasica( TabelaBasica funcao ) throws QtException {

		if( funcao instanceof Funcao ) {
			this.funcao = (Funcao) funcao;
		} else {
			throw new QtException( "Tabela Básica fornecida tem que ser uma instância de Funcao" );
		}
	}

	/**
	 * Permite acessar o registro corrente do persistor
	 */
	public TabelaBasica getTabelaBasica() throws QtException {

		return funcao;
	}

	/**
	 * Método para gerar automaticamente uma nova chave
	 */
	public static Integer geraNovoIdFuncao( Conexao cnx ) throws QtSQLException {

		Integer result = null;

		Query query = new Query( cnx );

		try{

			query.setSQL( "select max( ID_FUNCAO ) from PCMSO.FUNCOES" );

			query.executeQuery();
			result = new Integer( query.getInt( 1 ) + 1 );
		} catch( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			query.liberaRecursos();
		}
		return result;
	}

	/**
	 * Método para gerar automaticamente uma nova chave
	 */
	public static Integer geraNovoIdFuncao(  ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return geraNovoIdFuncao(  cnx  );
		} finally {
			cnx.libera();
		}
	}


	/**
	 * Método para inserir tupla na tabela
	 */
	public void insere() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "insert into PCMSO.FUNCOES" );
			cmd.append( "( ID_FUNCAO, DS_FUNCAO )" );
			cmd.append( "values " );
			cmd.append( "( ?, ?)" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			try {

				ps.setObject( 1, funcao.getIdFuncao() );
				ps.setObject( 2, funcao.getDsFuncao() );
				
				ps.execute();
			} finally { 
				ps.close();
			}
		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}

	/**
	 * Método para alterar tupla na tabela
	 */
	public void altera() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "update PCMSO.FUNCOES" );
			cmd.append( "  set ID_FUNCAO = ?, " );
			cmd.append( " DS_FUNCAO = ?" );
			cmd.append( " where ID_FUNCAO = ?" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			try {

				ps.setObject( 1, funcao.getIdFuncao() );
				ps.setObject( 2, funcao.getDsFuncao() );
				ps.setObject( 3, funcao.getIdFuncao() );
				
				ps.execute();
			} finally {
				ps.close();
			}
		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}

	/**
	 * Método para excluir tupla de uma tabela
	 */
	public void exclui() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "delete from PCMSO.FUNCOES" );
			cmd.append( " where ID_FUNCAO = ?" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			try {

				ps.setObject( 1, funcao.getIdFuncao() );
				
				ps.execute();
			} finally {
				ps.close();
			}
		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}
}