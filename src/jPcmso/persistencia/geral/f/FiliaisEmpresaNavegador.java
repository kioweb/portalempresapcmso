package jPcmso.persistencia.geral.f;

import quatro.persistencia.FiltroDeNavegador;
import quatro.persistencia.Navegador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.sql.Query;

/**
 *
 * Classe navegador para PCMSO.FILIAIS_EMPRESAS
 *
 * @author Samuel Antonio Klein
 * @see Navegador
 * @see FiliaisEmpresa
 * @see FiliaisEmpresaPersistor
 * @see FiliaisEmpresaLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 public class FiliaisEmpresaNavegador extends Navegador {

	
	/** Inst�ncia de FiliaisEmpresa para manipula��o pelo navegador */
	private FiliaisEmpresa filiaisEmpresa;

	/** Construtor b�sico */
	public FiliaisEmpresaNavegador()
	  throws QtSQLException { 

		filiaisEmpresa = new FiliaisEmpresa();
		primeiro();
	}

	/** Construtor b�sico que recebe uma conex�o */
	public FiliaisEmpresaNavegador( Conexao conexao )
	  throws QtSQLException { 

		super.setConexao( conexao ); 
		filiaisEmpresa = new FiliaisEmpresa();
		primeiro();
	}

	/**
	 * Construtor que recebe uma inst�ncia de FiliaisEmpresaFiltro e posiciona
	 * no primeiro registro que satisfaz suas condi��es 
	 */
	public FiliaisEmpresaNavegador( FiltroDeNavegador filtro ) throws QtSQLException  {
		super.setFiltro( filtro );
		this.filiaisEmpresa = new FiliaisEmpresa();
		primeiro();
	}

	/** Construtor que recebe as chaves da tabela PCMSO.FILIAIS_EMPRESAS
	 * @param cdEmpresa Codigo da Empresa
	 * @param idFilial Identificador da Filial
	 */
	public FiliaisEmpresaNavegador( String cdEmpresa, int idFilial )
	  throws QtSQLException { 

		filiaisEmpresa = new FiliaisEmpresa();
		this.filiaisEmpresa.setCdEmpresa( cdEmpresa );
		this.filiaisEmpresa.setIdFilial( new Integer( idFilial ) );
		
		busca();
	}

	/** Construtor que recebe  uma conex�o e as chaves da tabela PCMSO.FILIAIS_EMPRESAS
	 * @param cdEmpresa Codigo da Empresa
	 * @param idFilial Identificador da Filial
	 * @param conexao Conexao com o banco de dados
	 */
	public FiliaisEmpresaNavegador( Conexao conexao, String cdEmpresa, int idFilial )
	  throws QtSQLException { 

		super.setConexao( conexao ); 
		filiaisEmpresa = new FiliaisEmpresa();
		this.filiaisEmpresa.setCdEmpresa( cdEmpresa );
		this.filiaisEmpresa.setIdFilial( new Integer( idFilial ) );
		
		busca();
	}

	/** M�todo que retorna uma tabela b�sica a que se refere o navegador */
	public TabelaBasica getTabelaBasica() {

		return this.filiaisEmpresa;
	}

	/** M�todo que retorna a classe FiliaisEmpresa relacionada ao navegador */
	public FiliaisEmpresa getFiliaisEmpresa() {

		return this.filiaisEmpresa;
	}

	 /** M�todo que move para o primeiro registro da tabela */
	 public boolean primeiro() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select CD_EMPRESA, ID_FILIAL" );
			query.addSQL( "  from PCMSO.FILIAIS_EMPRESAS" );
			query.addSQL( super.getCondicaoDeFiltro( FiltroDeNavegador.COM_WHERE ) );
			query.addSQL( "  order by CD_EMPRESA, ID_FILIAL" );
			query.addSQL( "  limit 1 " );
			
			query.executeQuery();

			
			if( query.isEmpty() ) {

				super.setQueryStates( false, true, true );
			} else {
				this.filiaisEmpresa = new FiliaisEmpresa();

				filiaisEmpresa.setCdEmpresa( query.getString( "CD_EMPRESA" ) );
				filiaisEmpresa.setIdFilial( query.getInteger( "ID_FILIAL" ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o registro anterior na tabela */
	 public boolean anterior() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select CD_EMPRESA, ID_FILIAL" );
			query.addSQL( "  from PCMSO.FILIAIS_EMPRESAS" );
			query.addSQL( "  where ( ( CD_EMPRESA = :prm1CdEmpresa and  ID_FILIAL < :prm2IdFilial )" );
			query.addSQL( "  or (  CD_EMPRESA < :prm1CdEmpresa ) )" );
			query.addSQL( super.getCondicaoDeFiltro() );
			query.addSQL( "  order by CD_EMPRESA desc, ID_FILIAL desc" );
			query.addSQL( "  limit 1 " );

			query.setParameter( "prm1CdEmpresa", filiaisEmpresa.getCdEmpresa() );
			query.setParameter( "prm2IdFilial", filiaisEmpresa.getIdFilial() );
		
			query.executeQuery();

			
			if( query.isEmpty() ) {

				super.setFound( false );
				super.setBof( true );
			} else {
				this.filiaisEmpresa = new FiliaisEmpresa();

				filiaisEmpresa.setCdEmpresa( query.getString( "CD_EMPRESA" ) );
				filiaisEmpresa.setIdFilial( query.getInteger( "ID_FILIAL" ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o proximo registro da tabela */
	 public boolean proximo() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select CD_EMPRESA, ID_FILIAL" );
			query.addSQL( "  from PCMSO.FILIAIS_EMPRESAS" );
			query.addSQL( "  where ( (  CD_EMPRESA = :prm1CdEmpresa and  ID_FILIAL > :prm2IdFilial )" );
			query.addSQL( "  or (  CD_EMPRESA > :prm1CdEmpresa ) )" );
			query.addSQL( super.getCondicaoDeFiltro() );
			query.addSQL( "  order by CD_EMPRESA, ID_FILIAL" );
			query.addSQL( "  limit 1 " );

			query.setParameter( "prm1CdEmpresa", filiaisEmpresa.getCdEmpresa() );
			query.setParameter( "prm2IdFilial", filiaisEmpresa.getIdFilial() );
		
			query.executeQuery();

			
			if( query.isEmpty() ) {

				super.setFound( false );
				super.setEof( true );
			} else {
				this.filiaisEmpresa = new FiliaisEmpresa();

				filiaisEmpresa.setCdEmpresa( query.getString( "CD_EMPRESA" ) );
				filiaisEmpresa.setIdFilial( query.getInteger( "ID_FILIAL" ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o �ltimo registro da tabela */
	 public boolean ultimo() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select CD_EMPRESA, ID_FILIAL" );
			query.addSQL( "  from PCMSO.FILIAIS_EMPRESAS" );
			query.addSQL( super.getCondicaoDeFiltro( FiltroDeNavegador.COM_WHERE ) 	);
			query.addSQL( "  order by CD_EMPRESA desc, ID_FILIAL desc" );
			query.addSQL( "  limit 1 " );

			query.executeQuery();

			
			if( query.isEmpty() ) {

				super.setQueryStates( false, true, true );
			} else {
				this.filiaisEmpresa = new FiliaisEmpresa();

				filiaisEmpresa.setCdEmpresa( query.getString( "CD_EMPRESA" ) );
				filiaisEmpresa.setIdFilial( query.getInteger( "ID_FILIAL" ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	/**
	 * M�todo para buscar e preencher uma tupla de uma tabela
	 * @param tabelaBasica Recebe uma tabela basica com os atributos chave preenchidos
	 */
	public boolean busca( TabelaBasica tabelaBasica ) throws QtSQLException {

		if( !( tabelaBasica instanceof FiliaisEmpresa ) )
			throw new QtSQLException( "Esperava um objeto FiliaisEmpresa!" );

		this.filiaisEmpresa = (FiliaisEmpresa) tabelaBasica;
		return busca();

	}

	/**
	 * m�todo interno para buscar e preencher uma tupla de uma tabela
	 * Utiliza a Inst�ncia de FiliaisEmpresa da classe para fazer a busca
	 */
	private boolean busca() throws QtSQLException {

		FiliaisEmpresaLocalizador localizador = new FiliaisEmpresaLocalizador( getConexao(), this.filiaisEmpresa );

		if( localizador.isEmpty() ) {

			super.setQueryStates( false, true, true );
			return false;

		} else {

			super.setQueryStates( true, false, false );
			return true;
		}
	}
}