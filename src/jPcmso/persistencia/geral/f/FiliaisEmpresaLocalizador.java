package jPcmso.persistencia.geral.f;

import quatro.persistencia.Localizador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.QtSQLException;
import quatro.sql.Query;
import quatro.sql.Conexao;
import quatro.sql.PoolDeConexoes;
import quatro.sql.ConexaoParaPoolDeConexoes;

/**
 *
 * Classe localizador para PCMSO.FILIAIS_EMPRESAS
 *
 * @author ronei@unimed-uau.com.br
 * @see Localizador
 * @see FiliaisEmpresa
 * @see FiliaisEmpresaNavegador
 * @see FiliaisEmpresaPersistor
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 21/12/2017 07:46 - importado por ronei@unimed-uau.com.br
 *
 */
 public class FiliaisEmpresaLocalizador extends Localizador {

	/** Instância de FiliaisEmpresa para manipulação pelo localizador */
	private FiliaisEmpresa filiaisEmpresa;

	/** Construtor que recebe uma conexão e um objeto FiliaisEmpresa completo */
	public FiliaisEmpresaLocalizador( Conexao conexao, FiliaisEmpresa filiaisEmpresa ) throws QtSQLException {

		super.setConexao( conexao );

		this.filiaisEmpresa = filiaisEmpresa;
		busca();
	}

	/** Construtor que recebe um objeto FiliaisEmpresa completo */
	public FiliaisEmpresaLocalizador( FiliaisEmpresa filiaisEmpresa ) throws QtSQLException {

		this.filiaisEmpresa = filiaisEmpresa;
		busca();
	}

	/** 
	 * Construtor que recebe uma conexão e as chaves da tabela PCMSO.FILIAIS_EMPRESAS
	 * @param cdEmpresa Codigo da Empresa
	 * @param idFilial Identificador da Filial
	 * @param conexao Conexao com o banco de dados
	 */
	public FiliaisEmpresaLocalizador( Conexao conexao, String cdEmpresa, int idFilial ) throws QtSQLException { 

		super.setConexao( conexao ); 
		filiaisEmpresa = new FiliaisEmpresa();
		this.filiaisEmpresa.setCdEmpresa( cdEmpresa );
		this.filiaisEmpresa.setIdFilial( new Integer( idFilial ) );
		
		busca();
	}

	/** 
	 * Construtor que recebe as chaves da tabela PCMSO.FILIAIS_EMPRESAS
	 * @param cdEmpresa Codigo da Empresa
	 * @param idFilial Identificador da Filial
	 */
	public FiliaisEmpresaLocalizador( String cdEmpresa, int idFilial ) throws QtSQLException { 

		filiaisEmpresa = new FiliaisEmpresa();
		this.filiaisEmpresa.setCdEmpresa( cdEmpresa );
		this.filiaisEmpresa.setIdFilial( new Integer( idFilial ) );
		
		busca();
	}

	public static boolean existe( Conexao cnx, String cdEmpresa, int idFilial ) throws QtSQLException {

		Query q = new Query( cnx );

		try {
			q.setSQL( "select count(*) from PCMSO.FILIAIS_EMPRESAS" );
			q.addSQL( " where CD_EMPRESA = :prm1CdEmpresa and ID_FILIAL = :prm2IdFilial" );

			q.setParameter( "prm1CdEmpresa", cdEmpresa );
			q.setParameter( "prm2IdFilial", idFilial );
			q.executeQuery();

			return q.getInt( 1 ) > 0;
		} finally {
			q.liberaRecursos();
		}
	}

	public static boolean existe( String cdEmpresa, int idFilial ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return existe( cnx, cdEmpresa, idFilial );
		} finally {
			cnx.libera();
		}
	}

	public static boolean existe( Conexao cnx, FiliaisEmpresa filiaisEmpresa ) throws QtSQLException {

		return existe( cnx, filiaisEmpresa.getCdEmpresa(), filiaisEmpresa.getIdFilial() );
	}

	public static boolean existe( FiliaisEmpresa filiaisEmpresa ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return existe( cnx, filiaisEmpresa.getCdEmpresa(), filiaisEmpresa.getIdFilial() );
		} finally {
			cnx.libera();
		}
	}

	public static FiliaisEmpresa buscaFiliaisEmpresa( Conexao cnx, String cdEmpresa, int idFilial ) throws QtSQLException {

		Query q = new Query( cnx );

		try {
			q.setSQL( "select * from PCMSO.FILIAIS_EMPRESAS" );
			q.addSQL( " where CD_EMPRESA = :prm1CdEmpresa and ID_FILIAL = :prm2IdFilial" );

			q.setParameter( "prm1CdEmpresa", cdEmpresa );
			q.setParameter( "prm2IdFilial", idFilial );
			q.executeQuery();

			if( q.isEmpty() ) {
				return null;
			} else {
				FiliaisEmpresa filiaisEmpresa = new FiliaisEmpresa();
				buscaCampos( filiaisEmpresa, q );
				return filiaisEmpresa;
			}
		} finally {
			q.liberaRecursos();
		}
	}

	public static FiliaisEmpresa buscaFiliaisEmpresa( String cdEmpresa, int idFilial ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return buscaFiliaisEmpresa( cnx, cdEmpresa, idFilial );
		} finally { 
			cnx.libera();
		}
	}

	/** 
	 * Construtor que recebe uma conexão e as chaves da tabela PCMSO.FILIAIS_EMPRESAS
	 * @param cdEmpresa Codigo da Empresa
	 * @param idFilial Identificador da Filial
	 * @param conexao Conexao com o banco de dados
	 */
	public FiliaisEmpresaLocalizador( Conexao conexao, String cdEmpresa, Integer idFilial ) throws QtSQLException {

		super.setConexao( conexao ); 
		filiaisEmpresa = new FiliaisEmpresa();
		this.filiaisEmpresa.setCdEmpresa( cdEmpresa );
		this.filiaisEmpresa.setIdFilial( idFilial );
		
		busca();
	}

	/** 
	 * Construtor que recebe as chaves da tabela PCMSO.FILIAIS_EMPRESAS
	 * @param cdEmpresa Codigo da Empresa
	 * @param idFilial Identificador da Filial
	 */
	public FiliaisEmpresaLocalizador( String cdEmpresa, Integer idFilial ) throws QtSQLException {

		filiaisEmpresa = new FiliaisEmpresa();
		this.filiaisEmpresa.setCdEmpresa( cdEmpresa );
		this.filiaisEmpresa.setIdFilial( idFilial );
		
		busca();
	}

	public static FiliaisEmpresa buscaPorNumeroDaFilial( Conexao cnx, Integer idFilial ) throws QtSQLException {

		Query query = new Query( cnx );

		try {
			query.setSQL( "select * from PCMSO.FILIAIS_EMPRESAS" );
			query.addSQL( " where ID_FILIAL = :prm2IdFilial" );

			query.setParameter( "prm2IdFilial", idFilial );
			query.executeQuery();

			if( query.isEmpty() ) {
				return null;
			} else {
				FiliaisEmpresa filiaisEmpresa = new FiliaisEmpresa();
				buscaCampos( filiaisEmpresa, query );
				return filiaisEmpresa;
			}
		} finally {
			query.liberaRecursos();
		}
	}

	public static FiliaisEmpresa buscaPorNumeroDaFilial( Integer idFilial ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
		try {
			return buscaPorNumeroDaFilial( cnx, idFilial );
		} finally { 
			cnx.libera();
		}

	}

	public static boolean existeParaNumeroDaFilial( Conexao cnx, Integer idFilial ) throws QtSQLException {

		Query query = new Query( cnx );

		try {
			query.setSQL( "select count(*) from PCMSO.FILIAIS_EMPRESAS" );
			query.addSQL( " where ID_FILIAL = :prm2IdFilial" );

			query.setParameter( "prm2IdFilial", idFilial );
			query.executeQuery();

			return query.getInt( 1 ) > 0;
		} finally {
			query.liberaRecursos();
		}
	}

	public static boolean existeParaNumeroDaFilial( Integer idFilial ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
		try {
			return existeParaNumeroDaFilial( cnx, idFilial );
		} finally {
			cnx.libera();
		}
	}

	public static void buscaCampos( FiliaisEmpresa filiaisEmpresa, Query query ) throws QtSQLException {

		filiaisEmpresa.setCdEmpresa( query.getString( "CD_EMPRESA" ) );
		filiaisEmpresa.setIdFilial( query.getInteger( "ID_FILIAL" ) );
		filiaisEmpresa.setDsFilial( query.getString( "DS_FILIAL" ) );
		filiaisEmpresa.setNrCnpj( query.getString( "NR_CNPJ" ) );
	}

	/** Método que retorna uma tabela básica a que se refere o localizador */
	public TabelaBasica getTabelaBasica() {

		return this.filiaisEmpresa;
	}

	/** Método que retorna a classe FiliaisEmpresa relacionada ao localizador */
	public FiliaisEmpresa getFiliaisEmpresa() {

		return this.filiaisEmpresa;
	}

	/**
	 * Método para buscar e preencher uma tupla de uma tabela
	 * @param tabelaBasica Recebe uma tabela basica com os atributos chave preenchidos
	 */
	public boolean busca( TabelaBasica tabelaBasica ) throws QtSQLException {

		if( !( tabelaBasica instanceof FiliaisEmpresa ) )
			throw new QtSQLException( "Esperava um objeto FiliaisEmpresa!" );

		this.filiaisEmpresa = (FiliaisEmpresa) tabelaBasica;
		return busca();

	}

	/**
	 * Método privado para buscar e preencher uma tupla de uma tabela
	 * Este método busca os dados da instância de FiliaisEmpresa atual
	 */
	private boolean busca() throws QtSQLException {

		boolean ret = false;

		super.estabeleceConexao();
		Query query = super.criaNovaQuery();

		try {

			query.setSQL( "select * from PCMSO.FILIAIS_EMPRESAS" );
			query.addSQL( " where CD_EMPRESA like :prm1CdEmpresa and ID_FILIAL = :prm2IdFilial" );

			query.setParameter( "prm1CdEmpresa", filiaisEmpresa.getCdEmpresa() );
			query.setParameter( "prm2IdFilial", filiaisEmpresa.getIdFilial() );
			query.executeQuery();

			if( query.isEmpty() ) {
				super.setEmpty( true );
				filiaisEmpresa = null;
			} else {
				super.setEmpty( false );
				
				filiaisEmpresa.setCdEmpresa( query.getString( "CD_EMPRESA" ) );
				filiaisEmpresa.setIdFilial( query.getInteger( "ID_FILIAL" ) );
				filiaisEmpresa.setDsFilial( query.getString( "DS_FILIAL" ) );
				filiaisEmpresa.setNrCnpj( query.getString( "NR_CNPJ" ) );
				ret = true;
			}
		} finally {
			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}
 }