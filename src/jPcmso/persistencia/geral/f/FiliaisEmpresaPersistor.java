package jPcmso.persistencia.geral.f;

import java.sql.PreparedStatement;

import quatro.persistencia.Persistor;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.util.QtException;

/**
 *
 * Classe persistor para PCMSO.FILIAIS_EMPRESAS
 *
 * @author ronei@unimed-uau.com.br
 * @see Persistor
 * @see FiliaisEmpresa
 * @see FiliaisEmpresaNavegador
 * @see FiliaisEmpresaLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 21/12/2017 07:46 - importado por ronei@unimed-uau.com.br
 *
 */
 public class FiliaisEmpresaPersistor extends Persistor {

	/** Instância de FiliaisEmpresa para manipulação pelo persistor */
	private FiliaisEmpresa filiaisEmpresa;

	/** Construtor genérico */
	public FiliaisEmpresaPersistor() {

		this.filiaisEmpresa = null;
	}

	/** Construtor genérico */
	public FiliaisEmpresaPersistor( Conexao cnx ) throws QtSQLException {

		this.filiaisEmpresa = null;
		super.setConexao( cnx );
	}

	/** Construtor que recebe uma instância de FiliaisEmpresa */
	public FiliaisEmpresaPersistor( FiliaisEmpresa filiaisEmpresa ) throws QtSQLException {

		if( filiaisEmpresa == null ) 
			throw new QtSQLException( "Impossível instanciar FiliaisEmpresaPersistor porque está apontando para um nulo!" );

		this.filiaisEmpresa = filiaisEmpresa;
	}

	/** Construtor que recebe uma conexão e uma instância de FiliaisEmpresa */
	public FiliaisEmpresaPersistor( Conexao conexao, FiliaisEmpresa filiaisEmpresa ) throws QtSQLException {

		if( filiaisEmpresa == null ) 
			throw new QtSQLException( "Impossível instanciar FiliaisEmpresaPersistor porque está apontando para um nulo!" );

		setConexao( conexao );
		this.filiaisEmpresa = filiaisEmpresa;
	}

	/**
	 * Construtor que recebe as chaves da tabela PCMSO.FILIAIS_EMPRESAS
	 * @param cdEmpresa Codigo da Empresa
	 * @param idFilial Identificador da Filial
	 */
	public FiliaisEmpresaPersistor( String cdEmpresa, int idFilial )
	  throws QtSQLException { 

		busca( cdEmpresa, idFilial );
	}

	/**
	 * Construtor que recebe uma conexão e as chaves da tabela PCMSO.FILIAIS_EMPRESAS
	 * @param cdEmpresa Codigo da Empresa
	 * @param idFilial Identificador da Filial
	 * @param conexao Conexao com o banco de dados
	 */
	public FiliaisEmpresaPersistor( Conexao conexao, String cdEmpresa, int idFilial )
	  throws QtSQLException { 

		super.setConexao( conexao );
		busca( cdEmpresa, idFilial );
	}

	/**
	 * Método interno para buscar e preencher uma tupla de uma tabela
	 * Utiliza a Instância de FiliaisEmpresa para fazer a busca
	 * @param cdEmpresa Codigo da Empresa
	 * @param idFilial Identificador da Filial
	 */
	private void busca( String cdEmpresa, int idFilial ) throws QtSQLException { 

		FiliaisEmpresaLocalizador filiaisEmpresaLocalizador = new FiliaisEmpresaLocalizador( getConexao(), cdEmpresa, idFilial );

		if( filiaisEmpresaLocalizador.isEmpty() ) {
			throw new QtSQLException( "Chaves da Tabela FiliaisEmpresa não encontradas - cdEmpresa: " + cdEmpresa + " / idFilial: " + idFilial );
		}

		filiaisEmpresa = (FiliaisEmpresa ) filiaisEmpresaLocalizador.getTabelaBasica();
	}

	/**
	 * Permite alimentar o Persistor com um novo registro
	 */
	public void setTabelaBasica( TabelaBasica filiaisEmpresa ) throws QtException {

		if( filiaisEmpresa instanceof FiliaisEmpresa ) {
			this.filiaisEmpresa = (FiliaisEmpresa) filiaisEmpresa;
		} else {
			throw new QtException( "Tabela Básica fornecida tem que ser uma instância de FiliaisEmpresa" );
		}
	}

	/**
	 * Permite acessar o registro corrente do persistor
	 */
	public TabelaBasica getTabelaBasica() throws QtException {

		return filiaisEmpresa;
	}

	/**
	 * Método para inserir tupla na tabela
	 */
	public void insere() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "insert into PCMSO.FILIAIS_EMPRESAS" );
			cmd.append( "( CD_EMPRESA, ID_FILIAL, DS_FILIAL, NR_CNPJ )" );
			cmd.append( "values " );
			cmd.append( "( ?, ?, ?, ?)" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			try {

				ps.setObject( 1, filiaisEmpresa.getCdEmpresa() );
				ps.setObject( 2, filiaisEmpresa.getIdFilial() );
				ps.setObject( 3, filiaisEmpresa.getDsFilial() );
				ps.setObject( 4, filiaisEmpresa.getNrCnpj() );
				
				ps.execute();
			} finally { 
				ps.close();
			}
		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}

	/**
	 * Método para alterar tupla na tabela
	 */
	public void altera() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "update PCMSO.FILIAIS_EMPRESAS" );
			cmd.append( "  set CD_EMPRESA = ?, " );
			cmd.append( " ID_FILIAL = ?, " );
			cmd.append( " DS_FILIAL = ?, " );
			cmd.append( " NR_CNPJ = ?" );
			cmd.append( " where CD_EMPRESA like ? and ID_FILIAL = ?" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			try {

				ps.setObject( 1, filiaisEmpresa.getCdEmpresa() );
				ps.setObject( 2, filiaisEmpresa.getIdFilial() );
				ps.setObject( 3, filiaisEmpresa.getDsFilial() );
				ps.setObject( 4, filiaisEmpresa.getNrCnpj() );
				ps.setObject( 5, filiaisEmpresa.getCdEmpresa() );
				ps.setObject( 6, filiaisEmpresa.getIdFilial() );
				
				ps.execute();
			} finally {
				ps.close();
			}
		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}

	/**
	 * Método para excluir tupla de uma tabela
	 */
	public void exclui() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "delete from PCMSO.FILIAIS_EMPRESAS" );
			cmd.append( " where CD_EMPRESA like ? and ID_FILIAL = ?" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			try {

				ps.setObject( 1, filiaisEmpresa.getCdEmpresa() );
				ps.setObject( 2, filiaisEmpresa.getIdFilial() );
				
				ps.execute();
			} finally {
				ps.close();
			}
		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}
}