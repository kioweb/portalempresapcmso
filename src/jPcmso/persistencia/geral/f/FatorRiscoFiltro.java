package jPcmso.persistencia.geral.f;

import quatro.persistencia.FiltroDeNavegador;

/**
 *
 * Classe filtro para PCMSO.FATORES_RISCOS
 *
 * @author Samuel Antonio Klein
 * @see FiltroDeTabela
 * @see FatorRisco
 * @see FatorRiscoNavegador
 * @see FatorRiscoPersistor
 * @see FatorRiscoLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:31 - importado por Samuel Antonio Klein
 *
 */
 public class FatorRiscoFiltro extends FiltroDeNavegador {

	/** Determina a ordem de navega��o que ser� passada para o navegador */
	public void setOrdemDeNavegacao( int idOrdem ) {

		super.setOrdemDeNavegacao( idOrdem );
	}
}