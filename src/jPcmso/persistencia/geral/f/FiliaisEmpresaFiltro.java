package jPcmso.persistencia.geral.f;

import quatro.persistencia.CondicaoDeFiltro;
import quatro.persistencia.FiltroDeNavegador;

/**
 *
 * Classe filtro para PCMSO.FILIAIS_EMPRESAS
 *
 * @author Samuel Antonio Klein
 * @see FiltroDeTabela
 * @see FiliaisEmpresa
 * @see FiliaisEmpresaNavegador
 * @see FiliaisEmpresaPersistor
 * @see FiliaisEmpresaLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 public class FiliaisEmpresaFiltro extends FiltroDeNavegador {

	/** Filtro por descricao */
	public void setFiltroPorDescricao( String dsFilial ) {

		super.adicionaCondicao( "DS_FILIAL",CondicaoDeFiltro.IGUAL, dsFilial );
	}

	/** Determina a ordem de navega��o que ser� passada para o navegador */
	public void setOrdemDeNavegacao( int idOrdem ) {

		super.setOrdemDeNavegacao( idOrdem );
	}
}