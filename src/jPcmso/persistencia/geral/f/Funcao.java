package jPcmso.persistencia.geral.f;

import quatro.persistencia.TabelaBasica;
import quatro.smartDB.IgnoredField;
import quatro.smartDB.PrimaryKey;
import quatro.smartDB.Table;

/**
 *
 * Classe básica para PCMSO.FUNCOES
 *
 * Tabela de Função
 * 
 * @author ronei@unimed-uau.com.br
 * @see FuncaoNavegador
 * @see FuncaoPersistor
 * @see FuncaoLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 14/12/2017 15:26 - importado por ronei@unimed-uau.com.br
 *
 */
 @Table(name="PCMSO.FUNCOES")
 public class Funcao implements TabelaBasica {

	@IgnoredField
	private static final long serialVersionUID = -134300739238779659L;

	/** Codigo da funcao (chave) */
	@PrimaryKey
	private Integer idFuncao;
	/** Descricao da funcao (varchar(250)) */
	private String dsFuncao;

	/**
	 * Alimenta Codigo da funcao
	 * @param idFuncao Codigo da funcao
	 */ 
	public void setIdFuncao( Integer idFuncao ) {
		this.idFuncao = idFuncao;
	}

	/**
	 * Retorna Codigo da funcao
	 * @return Codigo da funcao
	 */
	public Integer getIdFuncao() {
		return this.idFuncao;
	}

	/**
	 * Alimenta Descricao da funcao
	 * @param dsFuncao Descricao da funcao
	 */ 
	public void setDsFuncao( String dsFuncao ) {
		this.dsFuncao = dsFuncao;
	}

	/**
	 * Retorna Descricao da funcao
	 * @return Descricao da funcao
	 */
	public String getDsFuncao() {
		return this.dsFuncao;
	}

}