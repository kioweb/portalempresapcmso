package jPcmso.persistencia.geral.f;

import java.sql.PreparedStatement;

import quatro.persistencia.Persistor;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.util.QtException;
import quatro.util.QtData;

/**
 *
 * Classe persistor para PCMSO.FATORES_RISCOS
 *
 * @author Samuel Antonio Klein
 * @see Persistor
 * @see FatorRisco
 * @see FatorRiscoNavegador
 * @see FatorRiscoLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:31 - importado por Samuel Antonio Klein
 *
 */
 public class FatorRiscoPersistor extends Persistor {

	/** Inst�ncia de FatorRisco para manipula��o pelo persistor */
	private FatorRisco fatorRisco;

	/** Construtor gen�rico */
	public FatorRiscoPersistor() {

		this.fatorRisco = null;
	}

	/** Construtor gen�rico */
	public FatorRiscoPersistor( Conexao cnx ) throws QtSQLException {

		this.fatorRisco = null;
		super.setConexao( cnx );
	}

	/** Construtor que recebe uma inst�ncia de FatorRisco */
	public FatorRiscoPersistor( FatorRisco fatorRisco ) throws QtSQLException {

		if( fatorRisco == null ) 
			throw new QtSQLException( "Imposs�vel instanciar FatorRiscoPersistor porque est� apontando para um nulo!" );

		this.fatorRisco = fatorRisco;
	}

	/** Construtor que recebe uma conex�o e uma inst�ncia de FatorRisco */
	public FatorRiscoPersistor( Conexao conexao, FatorRisco fatorRisco ) throws QtSQLException {

		if( fatorRisco == null ) 
			throw new QtSQLException( "Imposs�vel instanciar FatorRiscoPersistor porque est� apontando para um nulo!" );

		setConexao( conexao );
		this.fatorRisco = fatorRisco;
	}

	/**
	 * Construtor que recebe as chaves da tabela PCMSO.FATORES_RISCOS
	 * @param cdBeneficiarioCartao Codigo de Beneficiario 
	 * @param sqPerfil sequencia perfil
	 * @param sqFator sequencia fator
	 */
	public FatorRiscoPersistor( String cdBeneficiarioCartao, int sqPerfil, int sqFator )
	  throws QtSQLException { 

		busca( cdBeneficiarioCartao, sqPerfil, sqFator );
	}

	/**
	 * Construtor que recebe uma conex�o e as chaves da tabela PCMSO.FATORES_RISCOS
	 * @param cdBeneficiarioCartao Codigo de Beneficiario 
	 * @param sqPerfil sequencia perfil
	 * @param sqFator sequencia fator
	 * @param conexao Conexao com o banco de dados
	 */
	public FatorRiscoPersistor( Conexao conexao, String cdBeneficiarioCartao, int sqPerfil, int sqFator )
	  throws QtSQLException { 

		super.setConexao( conexao );
		busca( cdBeneficiarioCartao, sqPerfil, sqFator );
	}

	/**
	 * M�todo interno para buscar e preencher uma tupla de uma tabela
	 * Utiliza a Inst�ncia de FatorRisco para fazer a busca
	 * @param cdBeneficiarioCartao Codigo de Beneficiario 
	 * @param sqPerfil sequencia perfil
	 * @param sqFator sequencia fator
	 */
	private void busca( String cdBeneficiarioCartao, int sqPerfil, int sqFator ) throws QtSQLException { 

		FatorRiscoLocalizador fatorRiscoLocalizador = new FatorRiscoLocalizador( getConexao(), cdBeneficiarioCartao, sqPerfil, sqFator );

		if( fatorRiscoLocalizador.isEmpty() ) {
			throw new QtSQLException( "Chaves da Tabela FatorRisco n�o encontradas - cdBeneficiarioCartao: " + cdBeneficiarioCartao + " / sqPerfil: " + sqPerfil + " / sqFator: " + sqFator );
		}

		fatorRisco = (FatorRisco ) fatorRiscoLocalizador.getTabelaBasica();
	}

	/**
	 * Permite alimentar o Persistor com um novo registro
	 */
	public void setTabelaBasica( TabelaBasica fatorRisco ) throws QtException {

		if( fatorRisco instanceof FatorRisco ) {
			this.fatorRisco = (FatorRisco) fatorRisco;
		} else {
			throw new QtException( "Tabela B�sica fornecida tem que ser uma inst�ncia de FatorRisco" );
		}
	}

	/**
	 * Permite acessar o registro corrente do persistor
	 */
	public TabelaBasica getTabelaBasica() throws QtException {

		return fatorRisco;
	}

	/**
	 * M�todo para inserir tupla na tabela
	 */
	public void insere() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "insert into PCMSO.FATORES_RISCOS" );
			cmd.append( "( CD_BENEFICIARIO_CARTAO, SQ_PERFIL, SQ_FATOR, DT_INICIO, DT_FINAL, ID_RISCO, DS_INTENSIDADE_CONCENTRACAO, DS_TECNICA_UTILIZADA, SN_EPC_EFICAZ, SN_EPI_EFICAZ, NR_CA_EPI )" );
			cmd.append( "values " );
			cmd.append( "( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			ps.setObject( 1, fatorRisco.getCdBeneficiarioCartao() );
			ps.setObject( 2, fatorRisco.getSqPerfil() );
			ps.setObject( 3, fatorRisco.getSqFator() );
			ps.setObject( 4, QtData.getQtDataAsTimestamp( fatorRisco.getDtInicio() ) );
			ps.setObject( 5, QtData.getQtDataAsTimestamp( fatorRisco.getDtFinal() ) );
			ps.setObject( 6, fatorRisco.getIdRisco() );
			ps.setObject( 7, fatorRisco.getDsIntensidadeConcentracao() );
			ps.setObject( 8, fatorRisco.getDsTecnicaUtilizada() );
			ps.setObject( 9, fatorRisco.isSnEpcEficaz() ? "S": "N"  );
			ps.setObject( 10, fatorRisco.isSnEpiEficaz() ? "S": "N"  );
			ps.setObject( 11, fatorRisco.getNrCaEpi() );
			
			ps.execute();

		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}

	/**
	 * M�todo para alterar tupla na tabela
	 */
	public void altera() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "update PCMSO.FATORES_RISCOS" );
			cmd.append( "  set CD_BENEFICIARIO_CARTAO = ?, " );
			cmd.append( " SQ_PERFIL = ?, " );
			cmd.append( " SQ_FATOR = ?, " );
			cmd.append( " DT_INICIO = ?, " );
			cmd.append( " DT_FINAL = ?, " );
			cmd.append( " ID_RISCO = ?, " );
			cmd.append( " DS_INTENSIDADE_CONCENTRACAO = ?, " );
			cmd.append( " DS_TECNICA_UTILIZADA = ?, " );
			cmd.append( " SN_EPC_EFICAZ = ?, " );
			cmd.append( " SN_EPI_EFICAZ = ?, " );
			cmd.append( " NR_CA_EPI = ?" );
			cmd.append( " where CD_BENEFICIARIO_CARTAO like ? and SQ_PERFIL = ? and SQ_FATOR = ?" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			ps.setObject( 1, fatorRisco.getCdBeneficiarioCartao() );
			ps.setObject( 2, fatorRisco.getSqPerfil() );
			ps.setObject( 3, fatorRisco.getSqFator() );
			ps.setObject( 4, QtData.getQtDataAsTimestamp( fatorRisco.getDtInicio() ) );
			ps.setObject( 5, QtData.getQtDataAsTimestamp( fatorRisco.getDtFinal() ) );
			ps.setObject( 6, fatorRisco.getIdRisco() );
			ps.setObject( 7, fatorRisco.getDsIntensidadeConcentracao() );
			ps.setObject( 8, fatorRisco.getDsTecnicaUtilizada() );
			ps.setObject( 9, fatorRisco.isSnEpcEficaz() ? "S": "N"  );
			ps.setObject( 10, fatorRisco.isSnEpiEficaz() ? "S": "N"  );
			ps.setObject( 11, fatorRisco.getNrCaEpi() );
			
			ps.setObject( 12, fatorRisco.getCdBeneficiarioCartao() );
			ps.setObject( 13, fatorRisco.getSqPerfil() );
			ps.setObject( 14, fatorRisco.getSqFator() );
			
			ps.execute();

		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}

	/**
	 * M�todo para excluir tupla de uma tabela
	 */
	public void exclui() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "delete from PCMSO.FATORES_RISCOS" );
			cmd.append( " where CD_BENEFICIARIO_CARTAO like ? and SQ_PERFIL = ? and SQ_FATOR = ?" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			ps.setObject( 1, fatorRisco.getCdBeneficiarioCartao() );
			ps.setObject( 2, fatorRisco.getSqPerfil() );
			ps.setObject( 3, fatorRisco.getSqFator() );
			
			ps.execute();

		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}
}