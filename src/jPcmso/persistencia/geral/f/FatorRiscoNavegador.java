package jPcmso.persistencia.geral.f;

import quatro.persistencia.FiltroDeNavegador;
import quatro.persistencia.Navegador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.sql.Query;
import quatro.util.QtData;

/**
 *
 * Classe navegador para PCMSO.FATORES_RISCOS
 *
 * @author Samuel Antonio Klein
 * @see Navegador
 * @see FatorRisco
 * @see FatorRiscoPersistor
 * @see FatorRiscoLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:31 - importado por Samuel Antonio Klein
 *
 */
 public class FatorRiscoNavegador extends Navegador {

	
	/** Inst�ncia de FatorRisco para manipula��o pelo navegador */
	private FatorRisco fatorRisco;

	/** Construtor b�sico */
	public FatorRiscoNavegador()
	  throws QtSQLException { 

		fatorRisco = new FatorRisco();
		primeiro();
	}

	/** Construtor b�sico que recebe uma conex�o */
	public FatorRiscoNavegador( Conexao conexao )
	  throws QtSQLException { 

		super.setConexao( conexao ); 
		fatorRisco = new FatorRisco();
		primeiro();
	}

	/**
	 * Construtor que recebe uma inst�ncia de FatorRiscoFiltro e posiciona
	 * no primeiro registro que satisfaz suas condi��es 
	 */
	public FatorRiscoNavegador( FiltroDeNavegador filtro ) throws QtSQLException  {
		super.setFiltro( filtro );
		this.fatorRisco = new FatorRisco();
		primeiro();
	}

	/** Construtor que recebe as chaves da tabela PCMSO.FATORES_RISCOS
	 * @param cdBeneficiarioCartao Codigo de Beneficiario 
	 * @param sqPerfil sequencia perfil
	 * @param sqFator sequencia fator
	 */
	public FatorRiscoNavegador( String cdBeneficiarioCartao, int sqPerfil, int sqFator )
	  throws QtSQLException { 

		fatorRisco = new FatorRisco();
		this.fatorRisco.setCdBeneficiarioCartao( cdBeneficiarioCartao );
		this.fatorRisco.setSqPerfil( new Integer( sqPerfil ) );
		this.fatorRisco.setSqFator( new Integer( sqFator ) );
		
		busca();
	}

	/** Construtor que recebe  uma conex�o e as chaves da tabela PCMSO.FATORES_RISCOS
	 * @param cdBeneficiarioCartao Codigo de Beneficiario 
	 * @param sqPerfil sequencia perfil
	 * @param sqFator sequencia fator
	 * @param conexao Conexao com o banco de dados
	 */
	public FatorRiscoNavegador( Conexao conexao, String cdBeneficiarioCartao, int sqPerfil, int sqFator )
	  throws QtSQLException { 

		super.setConexao( conexao ); 
		fatorRisco = new FatorRisco();
		this.fatorRisco.setCdBeneficiarioCartao( cdBeneficiarioCartao );
		this.fatorRisco.setSqPerfil( new Integer( sqPerfil ) );
		this.fatorRisco.setSqFator( new Integer( sqFator ) );
		
		busca();
	}

	/** M�todo que retorna uma tabela b�sica a que se refere o navegador */
	public TabelaBasica getTabelaBasica() {

		return this.fatorRisco;
	}

	/** M�todo que retorna a classe FatorRisco relacionada ao navegador */
	public FatorRisco getFatorRisco() {

		return this.fatorRisco;
	}

	 /** M�todo que move para o primeiro registro da tabela */
	 public boolean primeiro() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select CD_BENEFICIARIO_CARTAO, SQ_PERFIL, SQ_FATOR" );
			query.addSQL( "  from PCMSO.FATORES_RISCOS" );
			query.addSQL( super.getCondicaoDeFiltro( FiltroDeNavegador.COM_WHERE ) );
			query.addSQL( "  order by CD_BENEFICIARIO_CARTAO, SQ_PERFIL, SQ_FATOR" );
			query.addSQL( "  limit 1 " );
			
			query.executeQuery();

			
			if( query.isEmpty() ) {

				super.setQueryStates( false, true, true );
			} else {
				this.fatorRisco = new FatorRisco();

				fatorRisco.setCdBeneficiarioCartao( query.getString( "CD_BENEFICIARIO_CARTAO" ) );
				fatorRisco.setSqPerfil( query.getInteger( "SQ_PERFIL" ) );
				fatorRisco.setSqFator( query.getInteger( "SQ_FATOR" ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o registro anterior na tabela */
	 public boolean anterior() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select CD_BENEFICIARIO_CARTAO, SQ_PERFIL, SQ_FATOR" );
			query.addSQL( "  from PCMSO.FATORES_RISCOS" );
			query.addSQL( "  where ( ( CD_BENEFICIARIO_CARTAO = :prm1CdBeneficiarioCartao and  SQ_PERFIL = :prm2SqPerfil and  SQ_FATOR < :prm3SqFator )" );
			query.addSQL( "  or (  CD_BENEFICIARIO_CARTAO = :prm1CdBeneficiarioCartao and  SQ_PERFIL < :prm2SqPerfil )" );
			query.addSQL( "  or (  CD_BENEFICIARIO_CARTAO < :prm1CdBeneficiarioCartao ) )" );
			query.addSQL( super.getCondicaoDeFiltro() );
			query.addSQL( "  order by CD_BENEFICIARIO_CARTAO desc, SQ_PERFIL desc, SQ_FATOR desc" );
			query.addSQL( "  limit 1 " );

			query.setParameter( "prm1CdBeneficiarioCartao", fatorRisco.getCdBeneficiarioCartao() );
			query.setParameter( "prm2SqPerfil", fatorRisco.getSqPerfil() );
			query.setParameter( "prm3SqFator", fatorRisco.getSqFator() );
		
			query.executeQuery();

			
			if( query.isEmpty() ) {

				super.setFound( false );
				super.setBof( true );
			} else {
				this.fatorRisco = new FatorRisco();

				fatorRisco.setCdBeneficiarioCartao( query.getString( "CD_BENEFICIARIO_CARTAO" ) );
				fatorRisco.setSqPerfil( query.getInteger( "SQ_PERFIL" ) );
				fatorRisco.setSqFator( query.getInteger( "SQ_FATOR" ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o proximo registro da tabela */
	 public boolean proximo() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select CD_BENEFICIARIO_CARTAO, SQ_PERFIL, SQ_FATOR" );
			query.addSQL( "  from PCMSO.FATORES_RISCOS" );
			query.addSQL( "  where ( (  CD_BENEFICIARIO_CARTAO = :prm1CdBeneficiarioCartao and  SQ_PERFIL = :prm2SqPerfil and  SQ_FATOR > :prm3SqFator )" );
			query.addSQL( "  or (  CD_BENEFICIARIO_CARTAO = :prm1CdBeneficiarioCartao and  SQ_PERFIL > :prm2SqPerfil )" );
			query.addSQL( "  or (  CD_BENEFICIARIO_CARTAO > :prm1CdBeneficiarioCartao ) )" );
			query.addSQL( super.getCondicaoDeFiltro() );
			query.addSQL( "  order by CD_BENEFICIARIO_CARTAO, SQ_PERFIL, SQ_FATOR" );
			query.addSQL( "  limit 1 " );

			query.setParameter( "prm1CdBeneficiarioCartao", fatorRisco.getCdBeneficiarioCartao() );
			query.setParameter( "prm2SqPerfil", fatorRisco.getSqPerfil() );
			query.setParameter( "prm3SqFator", fatorRisco.getSqFator() );
		
			query.executeQuery();

			
			if( query.isEmpty() ) {

				super.setFound( false );
				super.setEof( true );
			} else {
				this.fatorRisco = new FatorRisco();

				fatorRisco.setCdBeneficiarioCartao( query.getString( "CD_BENEFICIARIO_CARTAO" ) );
				fatorRisco.setSqPerfil( query.getInteger( "SQ_PERFIL" ) );
				fatorRisco.setSqFator( query.getInteger( "SQ_FATOR" ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o �ltimo registro da tabela */
	 public boolean ultimo() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select CD_BENEFICIARIO_CARTAO, SQ_PERFIL, SQ_FATOR" );
			query.addSQL( "  from PCMSO.FATORES_RISCOS" );
			query.addSQL( super.getCondicaoDeFiltro( FiltroDeNavegador.COM_WHERE ) 	);
			query.addSQL( "  order by CD_BENEFICIARIO_CARTAO desc, SQ_PERFIL desc, SQ_FATOR desc" );
			query.addSQL( "  limit 1 " );

			query.executeQuery();

			
			if( query.isEmpty() ) {

				super.setQueryStates( false, true, true );
			} else {
				this.fatorRisco = new FatorRisco();

				fatorRisco.setCdBeneficiarioCartao( query.getString( "CD_BENEFICIARIO_CARTAO" ) );
				fatorRisco.setSqPerfil( query.getInteger( "SQ_PERFIL" ) );
				fatorRisco.setSqFator( query.getInteger( "SQ_FATOR" ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	/**
	 * M�todo para buscar e preencher uma tupla de uma tabela
	 * @param tabelaBasica Recebe uma tabela basica com os atributos chave preenchidos
	 */
	public boolean busca( TabelaBasica tabelaBasica ) throws QtSQLException {

		if( !( tabelaBasica instanceof FatorRisco ) )
			throw new QtSQLException( "Esperava um objeto FatorRisco!" );

		this.fatorRisco = (FatorRisco) tabelaBasica;
		return busca();

	}

	/**
	 * m�todo interno para buscar e preencher uma tupla de uma tabela
	 * Utiliza a Inst�ncia de FatorRisco da classe para fazer a busca
	 */
	private boolean busca() throws QtSQLException {

		FatorRiscoLocalizador localizador = new FatorRiscoLocalizador( getConexao(), this.fatorRisco );

		if( localizador.isEmpty() ) {

			super.setQueryStates( false, true, true );
			return false;

		} else {

			super.setQueryStates( true, false, false );
			return true;
		}
	}
}