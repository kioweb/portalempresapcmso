package jPcmso.persistencia.geral.f;

import quatro.persistencia.Caminhador;
import quatro.persistencia.FiltroDeNavegador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;

 /**
 *
 * Classe Caminhador para FiliaisEmpresa
 *
 * @author ronei@unimed-uau.com.br
 * @see Caminhador
 * @see FiltroDeNavegador
 * @see FiliaisEmpresaNavegador
 * @see FiliaisEmpresaPersistor
 * @see FiliaisEmpresaLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 21/12/2017 07:46 - importado por ronei@unimed-uau.com.br
 *
 */
 public class FiliaisEmpresaCaminhador extends Caminhador {

	public static final int POR_ORDEM_DEFAULT = -1;
	public static final int POR_CODIGO = 0;
	public static final int POR_DESCRICAO = 1;
	
	private FiliaisEmpresa filiaisEmpresa;

	public FiliaisEmpresaCaminhador( FiltroDeNavegador filtro ) {

		setFiltro( filtro );
		inicializaVisao();
	}

	public FiliaisEmpresaCaminhador( Conexao conexao, FiltroDeNavegador filtro ) throws QtSQLException {

		super.setConexao( conexao );
		setFiltro( filtro );
		inicializaVisao();
	}

	private void inicializaVisao() {

		setNmVisao( "PCMSO.FILIAIS_EMPRESAS" );
	}

	protected void carregaCampos() throws QtSQLException {

		filiaisEmpresa = new FiliaisEmpresa();

		if( isRecordAvailable() ) {
			filiaisEmpresa.setCdEmpresa( query.getString( "CD_EMPRESA" ) );
			filiaisEmpresa.setIdFilial( query.getInteger( "ID_FILIAL" ) );
			filiaisEmpresa.setDsFilial( query.getString( "DS_FILIAL" ) );
			filiaisEmpresa.setNrCnpj( query.getString( "NR_CNPJ" ) );
		}
	}

	/** Método que retorna uma tabela básica a que se refere o caminhador */
	public TabelaBasica getTabelaBasica() {

		return this.filiaisEmpresa;
	}

	/** Método que retorna a classe FiliaisEmpresa relacionada ao caminhador */
	public FiliaisEmpresa getFiliaisEmpresa() {

		return this.filiaisEmpresa;
	}

	public void ativaVisao() throws QtSQLException {

		if(  super.getOrdemDeNavegacao() == FiliaisEmpresaCaminhador.POR_ORDEM_DEFAULT ) {
			super.ativaCaminhador( "order by CD_EMPRESA, ID_FILIAL" );
		} else if(  super.getOrdemDeNavegacao() == FiliaisEmpresaCaminhador.POR_CODIGO ) {
			super.ativaCaminhador( "order by CD_EMPRESA, ID_FILIAL" );
		} else if(  super.getOrdemDeNavegacao() == FiliaisEmpresaCaminhador.POR_DESCRICAO ) {
			super.ativaCaminhador( "order by DS_FILIAL" );
	
		} else {
			throw new QtSQLException( "FiliaisEmpresaCaminhador - Ordem de Navegação não definida!" );
		}
	}

	public String getCamposIndice( int qualIndice ) {
		return getDsIndice( qualIndice );
	}

	public String getCamposChave() {
		return getDsChave();
	}

	/**
	 * Método estático que devolve os campos que compõe a chave da tabela;
	 * @return campos que compõe a chave.
	 */
	public static String getDsChave() {
		return "CD_EMPRESA, ID_FILIAL";
	}

	/**
	 * Método estático que devolve os campos que fazem parte de uma dada ordem
	 * de navegação.
	 * @param qualIndice indica a ordem de navegação desejada.
	 * @return campos que compõe a ordem de navegação.
	 */
	public static String getDsIndice( int qualIndice ) {

		switch( qualIndice ) {

			case POR_ORDEM_DEFAULT: return "CD_EMPRESA, ID_FILIAL";
			case POR_CODIGO: return "CD_EMPRESA, ID_FILIAL";
			case POR_DESCRICAO: return "DS_FILIAL";
		}
		return null;
	}
}