package jPcmso.persistencia.geral.f;

import quatro.persistencia.FiltroDeNavegador;
import quatro.persistencia.Navegador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.sql.Query;

/**
 *
 * Classe navegador para PCMSO.FUNCOES
 *
 * @author ronei@unimed-uau.com.br
 * @see Navegador
 * @see Funcao
 * @see FuncaoPersistor
 * @see FuncaoLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 14/12/2017 15:26 - importado por ronei@unimed-uau.com.br
 *
 */
 public class FuncaoNavegador extends Navegador {

	
	/** Instância de Funcao para manipulação pelo navegador */
	private Funcao funcao;

	/** Construtor básico */
	public FuncaoNavegador()
	  throws QtSQLException { 

		funcao = new Funcao();
		primeiro();
	}

	/** Construtor básico que recebe uma conexão */
	public FuncaoNavegador( Conexao conexao )
	  throws QtSQLException { 

		super.setConexao( conexao ); 
		funcao = new Funcao();
		primeiro();
	}

	/**
	 * Construtor que recebe uma instância de FuncaoFiltro e posiciona
	 * no primeiro registro que satisfaz suas condições 
	 */
	public FuncaoNavegador( FiltroDeNavegador filtro ) throws QtSQLException  {
		super.setFiltro( filtro );
		this.funcao = new Funcao();
		primeiro();
	}

	/** Construtor que recebe a chave da tabela PCMSO.FUNCOES
	 * @param idFuncao Codigo da funcao
	 */
	public FuncaoNavegador( int idFuncao )
	  throws QtSQLException { 

		funcao = new Funcao();
		this.funcao.setIdFuncao( new Integer( idFuncao ) );
		
		busca();
	}

	/** Construtor que recebe  uma conexão e a chave da tabela PCMSO.FUNCOES
	 * @param idFuncao Codigo da funcao
	 * @param conexao Conexao com o banco de dados
	 */
	public FuncaoNavegador( Conexao conexao, int idFuncao )
	  throws QtSQLException { 

		super.setConexao( conexao ); 
		funcao = new Funcao();
		this.funcao.setIdFuncao( new Integer( idFuncao ) );
		
		busca();
	}

	/** Método que retorna uma tabela básica a que se refere o navegador */
	public TabelaBasica getTabelaBasica() {

		return this.funcao;
	}

	/** Método que retorna a classe Funcao relacionada ao navegador */
	public Funcao getFuncao() {

		return this.funcao;
	}

	 /** Método que move para o primeiro registro da tabela */
	 public boolean primeiro() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select min( ID_FUNCAO ) from PCMSO.FUNCOES" );
			query.addSQL( super.getCondicaoDeFiltro( FiltroDeNavegador.COM_WHERE ) 	);

			query.executeQuery();

			
			if( query.getString( 1 ) == null ) {

				super.setQueryStates( false, true, true );
			} else {
				this.funcao = new Funcao();

				funcao.setIdFuncao( query.getInteger( 1 ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** Método que move para o registro anterior na tabela */
	 public boolean anterior() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select max( ID_FUNCAO ) from PCMSO.FUNCOES" );
			query.addSQL( " where ID_FUNCAO < :prm1IdFuncao" );
			query.addSQL( super.getCondicaoDeFiltro() );

			query.setParameter( "prm1IdFuncao", funcao.getIdFuncao() );
			
			query.executeQuery();

			
			if( query.getString( 1 ) == null ) {

				super.setFound( false );
				super.setBof( true );
			} else {
				this.funcao = new Funcao();

				funcao.setIdFuncao( query.getInteger( 1 ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** Método que move para o proximo registro da tabela */
	 public boolean proximo() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select min( ID_FUNCAO ) from PCMSO.FUNCOES" );
			query.addSQL( " where ID_FUNCAO > :prm1IdFuncao" );
			query.addSQL( super.getCondicaoDeFiltro() );

			query.setParameter( "prm1IdFuncao", funcao.getIdFuncao() );

			query.executeQuery();

			
			if( query.getString( 1 ) == null ) {

				super.setFound( false );
				super.setEof( true );
			} else {
				this.funcao = new Funcao();

				funcao.setIdFuncao( query.getInteger( 1 ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** Método que move para o último registro da tabela */
	 public boolean ultimo() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select max( ID_FUNCAO ) from PCMSO.FUNCOES" );
			query.addSQL( super.getCondicaoDeFiltro( FiltroDeNavegador.COM_WHERE ) 	);

			query.executeQuery();

			
			if( query.getString( 1 ) == null ) {

				super.setQueryStates( false, true, true );
			} else {
				this.funcao = new Funcao();

				funcao.setIdFuncao( query.getInteger( 1 ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	/**
	 * Método para buscar e preencher uma tupla de uma tabela
	 * @param tabelaBasica Recebe uma tabela basica com os atributos chave preenchidos
	 */
	public boolean busca( TabelaBasica tabelaBasica ) throws QtSQLException {

		if( !( tabelaBasica instanceof Funcao ) )
			throw new QtSQLException( "Esperava um objeto Funcao!" );

		this.funcao = (Funcao) tabelaBasica;
		return busca();

	}

	/**
	 * método interno para buscar e preencher uma tupla de uma tabela
	 * Utiliza a Instância de Funcao da classe para fazer a busca
	 */
	private boolean busca() throws QtSQLException {

		FuncaoLocalizador localizador = new FuncaoLocalizador( getConexao(), this.funcao );

		if( localizador.isEmpty() ) {

			super.setQueryStates( false, true, true );
			return false;

		} else {

			super.setQueryStates( true, false, false );
			return true;
		}
	}
}