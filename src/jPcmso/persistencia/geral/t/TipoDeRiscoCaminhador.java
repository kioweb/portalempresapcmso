package jPcmso.persistencia.geral.t;

import quatro.persistencia.Caminhador;
import quatro.persistencia.FiltroDeNavegador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;

 /**
 *
 * Classe Caminhador para TipoDeRisco
 *
 * @author Samuel Antonio Klein
 * @see Caminhador
 * @see FiltroDeNavegador
 * @see TipoDeRiscoNavegador
 * @see TipoDeRiscoPersistor
 * @see TipoDeRiscoLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 public class TipoDeRiscoCaminhador extends Caminhador {

	public static final int POR_ORDEM_DEFAULT = -1;
	public static final int POR_CODIGO = 0;
	public static final int POR_DESCRICAO = 1;
	
	private TipoDeRisco tipoDeRisco;

	public TipoDeRiscoCaminhador( FiltroDeNavegador filtro ) {

		setFiltro( filtro );
		inicializaVisao();
	}

	public TipoDeRiscoCaminhador( Conexao conexao, FiltroDeNavegador filtro ) throws QtSQLException {

		super.setConexao( conexao );
		setFiltro( filtro );
		inicializaVisao();
	}

	private void inicializaVisao() {

		setNmVisao( "PCMSO.TIPOS_RISCOS" );
	}

	protected void carregaCampos() throws QtSQLException {

		tipoDeRisco = new TipoDeRisco();

		if( isRecordAvailable() ) {
			tipoDeRisco.setTpRisco( query.getString( "TP_RISCO" ) );
			tipoDeRisco.setDsTipoRisco( query.getString( "DS_TIPO_RISCO" ) );
		}
	}

	/** M�todo que retorna uma tabela b�sica a que se refere o caminhador */
	public TabelaBasica getTabelaBasica() {

		return this.tipoDeRisco;
	}

	/** M�todo que retorna a classe TipoDeRisco relacionada ao caminhador */
	public TipoDeRisco getTipoDeRisco() {

		return this.tipoDeRisco;
	}

	public void ativaVisao() throws QtSQLException {

		if(  super.getOrdemDeNavegacao() == TipoDeRiscoCaminhador.POR_ORDEM_DEFAULT ) {
			super.ativaCaminhador( "order by TP_RISCO" );
		} else if(  super.getOrdemDeNavegacao() == TipoDeRiscoCaminhador.POR_CODIGO ) {
			super.ativaCaminhador( "order by TP_RISCO" );
		} else if(  super.getOrdemDeNavegacao() == TipoDeRiscoCaminhador.POR_DESCRICAO ) {
			super.ativaCaminhador( "order by DS_TIPO_RISCO" );
	
		} else {
			throw new QtSQLException( "TipoDeRiscoCaminhador - Ordem de Navega��o n�o definida!" );
		}
	}

	public String getCamposIndice( int qualIndice ) {
		return getDsIndice( qualIndice );
	}

	public String getCamposChave() {
		return getDsChave();
	}

	/**
	 * M�todo est�tico que devolve os campos que comp�e a chave da tabela;
	 * @return campos que comp�e a chave.
	 */
	public static String getDsChave() {
		return "TP_RISCO";
	}

	/**
	 * M�todo est�tico que devolve os campos que fazem parte de uma dada ordem
	 * de navega��o.
	 * @param qualIndice indica a ordem de navega��o desejada.
	 * @return campos que comp�e a ordem de navega��o.
	 */
	public static String getDsIndice( int qualIndice ) {

		switch( qualIndice ) {

			case POR_ORDEM_DEFAULT: return "TP_RISCO";
			case POR_CODIGO: return "TP_RISCO";
			case POR_DESCRICAO: return "DS_TIPO_RISCO";
		}
		return null;
	}
}