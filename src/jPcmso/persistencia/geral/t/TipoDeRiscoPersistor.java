package jPcmso.persistencia.geral.t;

import java.sql.PreparedStatement;

import quatro.persistencia.Persistor;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.util.QtException;

/**
 *
 * Classe persistor para PCMSO.TIPOS_RISCOS
 *
 * @author Samuel Antonio Klein
 * @see Persistor
 * @see TipoDeRisco
 * @see TipoDeRiscoNavegador
 * @see TipoDeRiscoLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 public class TipoDeRiscoPersistor extends Persistor {

	/** Inst�ncia de TipoDeRisco para manipula��o pelo persistor */
	private TipoDeRisco tipoDeRisco;

	/** Construtor gen�rico */
	public TipoDeRiscoPersistor() {

		this.tipoDeRisco = null;
	}

	/** Construtor gen�rico */
	public TipoDeRiscoPersistor( Conexao cnx ) throws QtSQLException {

		this.tipoDeRisco = null;
		super.setConexao( cnx );
	}

	/** Construtor que recebe uma inst�ncia de TipoDeRisco */
	public TipoDeRiscoPersistor( TipoDeRisco tipoDeRisco ) throws QtSQLException {

		if( tipoDeRisco == null ) 
			throw new QtSQLException( "Imposs�vel instanciar TipoDeRiscoPersistor porque est� apontando para um nulo!" );

		this.tipoDeRisco = tipoDeRisco;
	}

	/** Construtor que recebe uma conex�o e uma inst�ncia de TipoDeRisco */
	public TipoDeRiscoPersistor( Conexao conexao, TipoDeRisco tipoDeRisco ) throws QtSQLException {

		if( tipoDeRisco == null ) 
			throw new QtSQLException( "Imposs�vel instanciar TipoDeRiscoPersistor porque est� apontando para um nulo!" );

		setConexao( conexao );
		this.tipoDeRisco = tipoDeRisco;
	}

	/**
	 * Construtor que recebe a chave da tabela PCMSO.TIPOS_RISCOS
	 * @param tpRisco Tipo de risco
	 */
	public TipoDeRiscoPersistor( String tpRisco )
	  throws QtSQLException { 

		busca( tpRisco );
	}

	/**
	 * Construtor que recebe uma conex�o e a chave da tabela PCMSO.TIPOS_RISCOS
	 * @param tpRisco Tipo de risco
	 * @param conexao Conexao com o banco de dados
	 */
	public TipoDeRiscoPersistor( Conexao conexao, String tpRisco )
	  throws QtSQLException { 

		super.setConexao( conexao );
		busca( tpRisco );
	}

	/**
	 * M�todo interno para buscar e preencher uma tupla de uma tabela
	 * Utiliza a Inst�ncia de TipoDeRisco para fazer a busca
	 * @param tpRisco Tipo de risco
	 */
	private void busca( String tpRisco ) throws QtSQLException { 

		TipoDeRiscoLocalizador tipoDeRiscoLocalizador = new TipoDeRiscoLocalizador( getConexao(), tpRisco );

		if( tipoDeRiscoLocalizador.isEmpty() ) {
			throw new QtSQLException( "Chaves da Tabela TipoDeRisco n�o encontradas - tpRisco: " + tpRisco );
		}

		tipoDeRisco = (TipoDeRisco ) tipoDeRiscoLocalizador.getTabelaBasica();
	}

	/**
	 * Permite alimentar o Persistor com um novo registro
	 */
	public void setTabelaBasica( TabelaBasica tipoDeRisco ) throws QtException {

		if( tipoDeRisco instanceof TipoDeRisco ) {
			this.tipoDeRisco = (TipoDeRisco) tipoDeRisco;
		} else {
			throw new QtException( "Tabela B�sica fornecida tem que ser uma inst�ncia de TipoDeRisco" );
		}
	}

	/**
	 * Permite acessar o registro corrente do persistor
	 */
	public TabelaBasica getTabelaBasica() throws QtException {

		return tipoDeRisco;
	}

	/**
	 * M�todo para inserir tupla na tabela
	 */
	public void insere() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "insert into PCMSO.TIPOS_RISCOS" );
			cmd.append( "( TP_RISCO, DS_TIPO_RISCO )" );
			cmd.append( "values " );
			cmd.append( "( ?, ?)" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			ps.setObject( 1, tipoDeRisco.getTpRisco() );
			ps.setObject( 2, tipoDeRisco.getDsTipoRisco() );
			
			ps.execute();

		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}

	/**
	 * M�todo para alterar tupla na tabela
	 */
	public void altera() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "update PCMSO.TIPOS_RISCOS" );
			cmd.append( "  set TP_RISCO = ?, " );
			cmd.append( " DS_TIPO_RISCO = ?" );
			cmd.append( " where TP_RISCO like ?" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			ps.setObject( 1, tipoDeRisco.getTpRisco() );
			ps.setObject( 2, tipoDeRisco.getDsTipoRisco() );
			
			ps.setObject( 3, tipoDeRisco.getTpRisco() );
			
			ps.execute();

		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}

	/**
	 * M�todo para excluir tupla de uma tabela
	 */
	public void exclui() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "delete from PCMSO.TIPOS_RISCOS" );
			cmd.append( " where TP_RISCO like ?" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			ps.setObject( 1, tipoDeRisco.getTpRisco() );
			
			ps.execute();

		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}
}