package jPcmso.persistencia.geral.t;

import quatro.persistencia.FiltroDeNavegador;
import quatro.persistencia.Navegador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.sql.Query;

/**
 *
 * Classe navegador para PCMSO.TIPOS_RISCOS
 *
 * @author Samuel Antonio Klein
 * @see Navegador
 * @see TipoDeRisco
 * @see TipoDeRiscoPersistor
 * @see TipoDeRiscoLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 public class TipoDeRiscoNavegador extends Navegador {

	
	/** Inst�ncia de TipoDeRisco para manipula��o pelo navegador */
	private TipoDeRisco tipoDeRisco;

	/** Construtor b�sico */
	public TipoDeRiscoNavegador()
	  throws QtSQLException { 

		tipoDeRisco = new TipoDeRisco();
		primeiro();
	}

	/** Construtor b�sico que recebe uma conex�o */
	public TipoDeRiscoNavegador( Conexao conexao )
	  throws QtSQLException { 

		super.setConexao( conexao ); 
		tipoDeRisco = new TipoDeRisco();
		primeiro();
	}

	/**
	 * Construtor que recebe uma inst�ncia de TipoDeRiscoFiltro e posiciona
	 * no primeiro registro que satisfaz suas condi��es 
	 */
	public TipoDeRiscoNavegador( FiltroDeNavegador filtro ) throws QtSQLException  {
		super.setFiltro( filtro );
		this.tipoDeRisco = new TipoDeRisco();
		primeiro();
	}

	/** Construtor que recebe a chave da tabela PCMSO.TIPOS_RISCOS
	 * @param tpRisco Tipo de risco
	 */
	public TipoDeRiscoNavegador( String tpRisco )
	  throws QtSQLException { 

		tipoDeRisco = new TipoDeRisco();
		this.tipoDeRisco.setTpRisco( tpRisco );
		
		busca();
	}

	/** Construtor que recebe  uma conex�o e a chave da tabela PCMSO.TIPOS_RISCOS
	 * @param tpRisco Tipo de risco
	 * @param conexao Conexao com o banco de dados
	 */
	public TipoDeRiscoNavegador( Conexao conexao, String tpRisco )
	  throws QtSQLException { 

		super.setConexao( conexao ); 
		tipoDeRisco = new TipoDeRisco();
		this.tipoDeRisco.setTpRisco( tpRisco );
		
		busca();
	}

	/** M�todo que retorna uma tabela b�sica a que se refere o navegador */
	public TabelaBasica getTabelaBasica() {

		return this.tipoDeRisco;
	}

	/** M�todo que retorna a classe TipoDeRisco relacionada ao navegador */
	public TipoDeRisco getTipoDeRisco() {

		return this.tipoDeRisco;
	}

	 /** M�todo que move para o primeiro registro da tabela */
	 public boolean primeiro() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select min( TP_RISCO ) from PCMSO.TIPOS_RISCOS" );
			query.addSQL( super.getCondicaoDeFiltro( FiltroDeNavegador.COM_WHERE ) 	);

			query.executeQuery();

			
			if( query.getString( 1 ) == null ) {

				super.setQueryStates( false, true, true );
			} else {
				this.tipoDeRisco = new TipoDeRisco();

				tipoDeRisco.setTpRisco( query.getString( 1 ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o registro anterior na tabela */
	 public boolean anterior() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select max( TP_RISCO ) from PCMSO.TIPOS_RISCOS" );
			query.addSQL( " where TP_RISCO < :prm1TpRisco" );
			query.addSQL( super.getCondicaoDeFiltro() );

			query.setParameter( "prm1TpRisco", tipoDeRisco.getTpRisco() );
			
			query.executeQuery();

			
			if( query.getString( 1 ) == null ) {

				super.setFound( false );
				super.setBof( true );
			} else {
				this.tipoDeRisco = new TipoDeRisco();

				tipoDeRisco.setTpRisco( query.getString( 1 ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o proximo registro da tabela */
	 public boolean proximo() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select min( TP_RISCO ) from PCMSO.TIPOS_RISCOS" );
			query.addSQL( " where TP_RISCO > :prm1TpRisco" );
			query.addSQL( super.getCondicaoDeFiltro() );

			query.setParameter( "prm1TpRisco", tipoDeRisco.getTpRisco() );

			query.executeQuery();

			
			if( query.getString( 1 ) == null ) {

				super.setFound( false );
				super.setEof( true );
			} else {
				this.tipoDeRisco = new TipoDeRisco();

				tipoDeRisco.setTpRisco( query.getString( 1 ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o �ltimo registro da tabela */
	 public boolean ultimo() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select max( TP_RISCO ) from PCMSO.TIPOS_RISCOS" );
			query.addSQL( super.getCondicaoDeFiltro( FiltroDeNavegador.COM_WHERE ) 	);

			query.executeQuery();

			
			if( query.getString( 1 ) == null ) {

				super.setQueryStates( false, true, true );
			} else {
				this.tipoDeRisco = new TipoDeRisco();

				tipoDeRisco.setTpRisco( query.getString( 1 ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	/**
	 * M�todo para buscar e preencher uma tupla de uma tabela
	 * @param tabelaBasica Recebe uma tabela basica com os atributos chave preenchidos
	 */
	public boolean busca( TabelaBasica tabelaBasica ) throws QtSQLException {

		if( !( tabelaBasica instanceof TipoDeRisco ) )
			throw new QtSQLException( "Esperava um objeto TipoDeRisco!" );

		this.tipoDeRisco = (TipoDeRisco) tabelaBasica;
		return busca();

	}

	/**
	 * m�todo interno para buscar e preencher uma tupla de uma tabela
	 * Utiliza a Inst�ncia de TipoDeRisco da classe para fazer a busca
	 */
	private boolean busca() throws QtSQLException {

		TipoDeRiscoLocalizador localizador = new TipoDeRiscoLocalizador( getConexao(), this.tipoDeRisco );

		if( localizador.isEmpty() ) {

			super.setQueryStates( false, true, true );
			return false;

		} else {

			super.setQueryStates( true, false, false );
			return true;
		}
	}
}