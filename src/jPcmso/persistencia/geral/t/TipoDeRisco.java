package jPcmso.persistencia.geral.t;

import quatro.persistencia.TabelaBasica;
import quatro.smartDB.IgnoredField;
import quatro.smartDB.PrimaryKey;
import quatro.smartDB.Table;

/**
 *
 * Classe b�sica para PCMSO.TIPOS_RISCOS
 *
 * Tabela Tipos de Riscos
 * 
 * @author Samuel Antonio Klein
 * @see TipoDeRiscoNavegador
 * @see TipoDeRiscoPersistor
 * @see TipoDeRiscoLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 @Table(name="PCMSO.TIPOS_RISCOS")
 public class TipoDeRisco implements TabelaBasica {

	@IgnoredField
	private static final long serialVersionUID = -541156033221818913L;

	/** Tipo de risco (chave-char(1)) */
	@PrimaryKey
	private String tpRisco;
	/** Descricao do risco  (char(20)) */
	private String dsTipoRisco;

	/**
	 * Alimenta Tipo de risco
	 * @param tpRisco Tipo de risco
	 */ 
	public void setTpRisco( String tpRisco ) {
		this.tpRisco = tpRisco;
	}

	/**
	 * Retorna Tipo de risco
	 * @return Tipo de risco
	 */
	public String getTpRisco() {
		return this.tpRisco;
	}

	/**
	 * Alimenta Descricao do risco 
	 * @param dsTipoRisco Descricao do risco 
	 */ 
	public void setDsTipoRisco( String dsTipoRisco ) {
		this.dsTipoRisco = dsTipoRisco;
	}

	/**
	 * Retorna Descricao do risco 
	 * @return Descricao do risco 
	 */
	public String getDsTipoRisco() {
		return this.dsTipoRisco;
	}

}