package jPcmso.persistencia.geral.t;

import quatro.persistencia.Localizador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.QtSQLException;
import quatro.sql.Query;
import quatro.sql.Conexao;
import quatro.sql.PoolDeConexoes;
import quatro.sql.ConexaoParaPoolDeConexoes;

/**
 *
 * Classe localizador para PCMSO.TIPOS_RISCOS
 *
 * @author Samuel Antonio Klein
 * @see Localizador
 * @see TipoDeRisco
 * @see TipoDeRiscoNavegador
 * @see TipoDeRiscoPersistor
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 public class TipoDeRiscoLocalizador extends Localizador {

	/** Inst�ncia de TipoDeRisco para manipula��o pelo localizador */
	private TipoDeRisco tipoDeRisco;

	/** Construtor que recebe uma conex�o e um objeto TipoDeRisco completo */
	public TipoDeRiscoLocalizador( Conexao conexao, TipoDeRisco tipoDeRisco ) throws QtSQLException {

		super.setConexao( conexao );

		this.tipoDeRisco = tipoDeRisco;
		busca();
	}

	/** Construtor que recebe um objeto TipoDeRisco completo */
	public TipoDeRiscoLocalizador( TipoDeRisco tipoDeRisco ) throws QtSQLException {

		this.tipoDeRisco = tipoDeRisco;
		busca();
	}

	/** 
	 * Construtor que recebe uma conex�o e a chave da tabela PCMSO.TIPOS_RISCOS
	 * @param tpRisco Tipo de risco
	 * @param conexao Conexao com o banco de dados
	 */
	public TipoDeRiscoLocalizador( Conexao conexao, String tpRisco ) throws QtSQLException { 

		super.setConexao( conexao ); 
		tipoDeRisco = new TipoDeRisco();
		this.tipoDeRisco.setTpRisco( tpRisco );
		
		busca();
	}

	/** 
	 * Construtor que recebe a chave da tabela PCMSO.TIPOS_RISCOS
	 * @param tpRisco Tipo de risco
	 */
	public TipoDeRiscoLocalizador( String tpRisco ) throws QtSQLException { 

		tipoDeRisco = new TipoDeRisco();
		this.tipoDeRisco.setTpRisco( tpRisco );
		
		busca();
	}

	public static boolean existe( Conexao cnx, String tpRisco ) throws QtSQLException {

		Query q = new Query( cnx );

		try {
			q.setSQL( "select count(*) from PCMSO.TIPOS_RISCOS" );
			q.addSQL( " where TP_RISCO = :prm1TpRisco" );

			q.setParameter( "prm1TpRisco", tpRisco );
			q.executeQuery();

			return q.getInt( 1 ) > 0;
		} finally {
			q.liberaRecursos();
		}
	}

	public static boolean existe( String tpRisco ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return existe( cnx, tpRisco );
		} finally {
			cnx.libera();
		}
	}

	public static boolean existe( Conexao cnx, TipoDeRisco tipoDeRisco ) throws QtSQLException {

		return existe( cnx, tipoDeRisco.getTpRisco() );
	}

	public static boolean existe( TipoDeRisco tipoDeRisco ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return existe( cnx, tipoDeRisco.getTpRisco() );
		} finally {
			cnx.libera();
		}
	}

	public static TipoDeRisco buscaTipoDeRisco( Conexao cnx, String tpRisco ) throws QtSQLException {

		Query q = new Query( cnx );

		try {
			q.setSQL( "select * from PCMSO.TIPOS_RISCOS" );
			q.addSQL( " where TP_RISCO = :prm1TpRisco" );

			q.setParameter( "prm1TpRisco", tpRisco );
			q.executeQuery();

			if( q.isEmpty() ) {
				return null;
			} else {
				TipoDeRisco tipoDeRisco = new TipoDeRisco();
				buscaCampos( tipoDeRisco, q );
				return tipoDeRisco;
			}
		} finally {
			q.liberaRecursos();
		}
	}

	public static TipoDeRisco buscaTipoDeRisco( String tpRisco ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return buscaTipoDeRisco( cnx, tpRisco );
		} finally { 
			cnx.libera();
		}
	}

	public static void buscaCampos( TipoDeRisco tipoDeRisco, Query query ) throws QtSQLException {

		tipoDeRisco.setTpRisco( query.getString( "TP_RISCO" ) );
		tipoDeRisco.setDsTipoRisco( query.getString( "DS_TIPO_RISCO" ) );
	}

	/** M�todo que retorna uma tabela b�sica a que se refere o localizador */
	public TabelaBasica getTabelaBasica() {

		return this.tipoDeRisco;
	}

	/** M�todo que retorna a classe TipoDeRisco relacionada ao localizador */
	public TipoDeRisco getTipoDeRisco() {

		return this.tipoDeRisco;
	}

	/**
	 * M�todo para buscar e preencher uma tupla de uma tabela
	 * @param tabelaBasica Recebe uma tabela basica com os atributos chave preenchidos
	 */
	public boolean busca( TabelaBasica tabelaBasica ) throws QtSQLException {

		if( !( tabelaBasica instanceof TipoDeRisco ) )
			throw new QtSQLException( "Esperava um objeto TipoDeRisco!" );

		this.tipoDeRisco = (TipoDeRisco) tabelaBasica;
		return busca();

	}

	/**
	 * M�todo privado para buscar e preencher uma tupla de uma tabela
	 * Este m�todo busca os dados da inst�ncia de TipoDeRisco atual
	 */
	private boolean busca() throws QtSQLException {

		boolean ret = false;

		super.estabeleceConexao();
		Query query = super.criaNovaQuery();

		try {

			query.setSQL( "select * from PCMSO.TIPOS_RISCOS" );
			query.addSQL( " where TP_RISCO like :prm1TpRisco" );

			query.setParameter( "prm1TpRisco", tipoDeRisco.getTpRisco() );
			query.executeQuery();

			if( query.isEmpty() ) {
				super.setEmpty( true );
				tipoDeRisco = null;
			} else {
				super.setEmpty( false );
				
				tipoDeRisco.setTpRisco( query.getString( "TP_RISCO" ) );
				tipoDeRisco.setDsTipoRisco( query.getString( "DS_TIPO_RISCO" ) );
				ret = true;
			}
		} finally {
			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}
 }