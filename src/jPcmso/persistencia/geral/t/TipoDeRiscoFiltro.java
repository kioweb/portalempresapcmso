package jPcmso.persistencia.geral.t;

import quatro.persistencia.FiltroDeNavegador;

/**
 *
 * Classe filtro para PCMSO.TIPOS_RISCOS
 *
 * @author Samuel Antonio Klein
 * @see FiltroDeTabela
 * @see TipoDeRisco
 * @see TipoDeRiscoNavegador
 * @see TipoDeRiscoPersistor
 * @see TipoDeRiscoLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 public class TipoDeRiscoFiltro extends FiltroDeNavegador {

	/** Determina a ordem de navega��o que ser� passada para o navegador */
	public void setOrdemDeNavegacao( int idOrdem ) {

		super.setOrdemDeNavegacao( idOrdem );
	}
}