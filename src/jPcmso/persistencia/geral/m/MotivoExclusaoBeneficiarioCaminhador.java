package jPcmso.persistencia.geral.m;

import quatro.persistencia.Caminhador;
import quatro.persistencia.FiltroDeNavegador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;

 /**
 *
 * Classe Caminhador para MotivoExclusaoBeneficiario
 *
 * @author Samuel Antonio Klein
 * @see Caminhador
 * @see FiltroDeNavegador
 * @see MotivoExclusaoBeneficiarioNavegador
 * @see MotivoExclusaoBeneficiarioPersistor
 * @see MotivoExclusaoBeneficiarioLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 public class MotivoExclusaoBeneficiarioCaminhador extends Caminhador {

	public static final int POR_ORDEM_DEFAULT = -1;
	public static final int POR_CODIGO = 0;
	public static final int POR_DESCRICAO = 1;
	public static final int POR_TIPO = 2;
	
	private MotivoExclusaoBeneficiario motivoExclusaoBeneficiario;

	public MotivoExclusaoBeneficiarioCaminhador( FiltroDeNavegador filtro ) {

		setFiltro( filtro );
		inicializaVisao();
	}

	public MotivoExclusaoBeneficiarioCaminhador( Conexao conexao, FiltroDeNavegador filtro ) throws QtSQLException {

		super.setConexao( conexao );
		setFiltro( filtro );
		inicializaVisao();
	}

	private void inicializaVisao() {

		setNmVisao( "MOTIVOS_EXCLUSAO_BENEFICIARIO" );
	}

	protected void carregaCampos() throws QtSQLException {

		motivoExclusaoBeneficiario = new MotivoExclusaoBeneficiario();

		if( isRecordAvailable() ) {
			motivoExclusaoBeneficiario.setIdMotivoExclusao( query.getInteger( "ID_MOTIVO_EXCLUSAO" ) );
			motivoExclusaoBeneficiario.setTpMotivoExclusao( query.getString( "TP_MOTIVO_EXCLUSAO" ) );
			motivoExclusaoBeneficiario.setDsMotivoExclusao( query.getString( "DS_MOTIVO_EXCLUSAO" ) );
		}
	}

	/** M�todo que retorna uma tabela b�sica a que se refere o caminhador */
	public TabelaBasica getTabelaBasica() {

		return this.motivoExclusaoBeneficiario;
	}

	/** M�todo que retorna a classe MotivoExclusaoBeneficiario relacionada ao caminhador */
	public MotivoExclusaoBeneficiario getMotivoExclusaoBeneficiario() {

		return this.motivoExclusaoBeneficiario;
	}

	public void ativaVisao() throws QtSQLException {

		if(  super.getOrdemDeNavegacao() == MotivoExclusaoBeneficiarioCaminhador.POR_ORDEM_DEFAULT ) {
			super.ativaCaminhador( "order by ID_MOTIVO_EXCLUSAO" );
		} else if(  super.getOrdemDeNavegacao() == MotivoExclusaoBeneficiarioCaminhador.POR_CODIGO ) {
			super.ativaCaminhador( "order by ID_MOTIVO_EXCLUSAO" );
		} else if(  super.getOrdemDeNavegacao() == MotivoExclusaoBeneficiarioCaminhador.POR_DESCRICAO ) {
			super.ativaCaminhador( "order by DS_MOTIVO_EXCLUSAO" );
		} else if(  super.getOrdemDeNavegacao() == MotivoExclusaoBeneficiarioCaminhador.POR_TIPO ) {
			super.ativaCaminhador( "order by TP_MOTIVO_EXCLUSAO" );
	
		} else {
			throw new QtSQLException( "MotivoExclusaoBeneficiarioCaminhador - Ordem de Navega��o n�o definida!" );
		}
	}

	public String getCamposIndice( int qualIndice ) {
		return getDsIndice( qualIndice );
	}

	public String getCamposChave() {
		return getDsChave();
	}

	/**
	 * M�todo est�tico que devolve os campos que comp�e a chave da tabela;
	 * @return campos que comp�e a chave.
	 */
	public static String getDsChave() {
		return "ID_MOTIVO_EXCLUSAO";
	}

	/**
	 * M�todo est�tico que devolve os campos que fazem parte de uma dada ordem
	 * de navega��o.
	 * @param qualIndice indica a ordem de navega��o desejada.
	 * @return campos que comp�e a ordem de navega��o.
	 */
	public static String getDsIndice( int qualIndice ) {

		switch( qualIndice ) {

			case POR_ORDEM_DEFAULT: return "ID_MOTIVO_EXCLUSAO";
			case POR_CODIGO: return "ID_MOTIVO_EXCLUSAO";
			case POR_DESCRICAO: return "DS_MOTIVO_EXCLUSAO";
			case POR_TIPO: return "TP_MOTIVO_EXCLUSAO";
		}
		return null;
	}
}