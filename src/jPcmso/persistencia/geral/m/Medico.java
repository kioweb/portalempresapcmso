package jPcmso.persistencia.geral.m;

import quatro.persistencia.TabelaBasica;
import quatro.smartDB.IgnoredField;
import quatro.smartDB.PrimaryKey;
import quatro.smartDB.Table;
import quatro.util.QtString;

/**
 *
 * Classe b�sica para PCMSO.MEDICOS
 *
 * Tabela de Medicos
 * 
 * @author Samuel Antonio Klein
 * @see MedicoNavegador
 * @see MedicoPersistor
 * @see MedicoLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 @Table(name="PCMSO.MEDICOS")
 public class Medico implements TabelaBasica {

	@IgnoredField
	private static final long serialVersionUID = -10905697975242921L;

	/** Codigo do medico (chave-char(6)) */
	@PrimaryKey
	private String cdMedico;
	/** Codigo do conselho  (varchar(12)) */
	private String cdSigla;
	/** UF do CRM (char(2)) */
	private String ufCrm;
	/** Numero do CRM */
	private Integer nrCrm;
	/** Nome do Medico (varchar(250)) */
	private String nmMedico;
	/** Coodenador (char(1)) */
	private boolean snCoordenador;
	/** Codigo de area DDD (char(2)) */
	private String nrDdd;
	/** Numero do telefone do medico (char(8)) */
	private String nrTelefone;
	/** Descri��o do Logradouro (varchar(250)) */
	private String dsLogradouro;
	/** Descri��o de Bairro (varchar(250)) */
	private String dsBairro;
	/** Descri��o de Cidade (varchar(250)) */
	private String dsCidade;
	/** UF da residencia (char(20)) */
	private String cdEstado;

	/**
	 * Alimenta Codigo do medico
	 * @param cdMedico Codigo do medico
	 */ 
	public void setCdMedico( String cdMedico ) {
		this.cdMedico = cdMedico;
	}

	/**
	 * Retorna Codigo do medico
	 * @return Codigo do medico
	 */
	public String getCdMedico() {
		return this.cdMedico;
	}

	/**
	 * Alimenta Codigo do conselho 
	 * @param cdSigla Codigo do conselho 
	 */ 
	public void setCdSigla( String cdSigla ) {
		this.cdSigla = cdSigla;
	}

	/**
	 * Retorna Codigo do conselho 
	 * @return Codigo do conselho 
	 */
	public String getCdSigla() {
		return this.cdSigla;
	}

	/**
	 * Alimenta UF do CRM
	 * @param ufCrm UF do CRM
	 */ 
	public void setUfCrm( String ufCrm ) {
		this.ufCrm = ufCrm;
	}

	/**
	 * Retorna UF do CRM
	 * @return UF do CRM
	 */
	public String getUfCrm() {
		return this.ufCrm;
	}

	/**
	 * Alimenta Numero do CRM
	 * @param nrCrm Numero do CRM
	 */ 
	public void setNrCrm( Integer nrCrm ) {
		this.nrCrm = nrCrm;
	}

	/**
	 * Retorna Numero do CRM
	 * @return Numero do CRM
	 */
	public Integer getNrCrm() {
		return this.nrCrm;
	}

	/**
	 * Alimenta Nome do Medico
	 * @param nmMedico Nome do Medico
	 */ 
	public void setNmMedico( String nmMedico ) {
		this.nmMedico = nmMedico;
	}

	/**
	 * Retorna Nome do Medico
	 * @return Nome do Medico
	 */
	public String getNmMedico() {
		return this.nmMedico;
	}

	/**
	 * Alimenta Coodenador
	 * @param snCoordenador Coodenador
	 */ 
	public void setSnCoordenador( String snCoordenador ) {
		this.snCoordenador = QtString.toBoolean( snCoordenador );
	}

	/**
	 * Retorna Coodenador
	 * @return Coodenador
	 */
	public boolean isSnCoordenador() {
		return this.snCoordenador;
	}

	/**
	 * Alimenta Codigo de area DDD
	 * @param nrDdd Codigo de area DDD
	 */ 
	public void setNrDdd( String nrDdd ) {
		this.nrDdd = nrDdd;
	}

	/**
	 * Retorna Codigo de area DDD
	 * @return Codigo de area DDD
	 */
	public String getNrDdd() {
		return this.nrDdd;
	}

	/**
	 * Alimenta Numero do telefone do medico
	 * @param nrTelefone Numero do telefone do medico
	 */ 
	public void setNrTelefone( String nrTelefone ) {
		this.nrTelefone = nrTelefone;
	}

	/**
	 * Retorna Numero do telefone do medico
	 * @return Numero do telefone do medico
	 */
	public String getNrTelefone() {
		return this.nrTelefone;
	}

	/**
	 * Alimenta Descri��o do Logradouro
	 * @param dsLogradouro Descri��o do Logradouro
	 */ 
	public void setDsLogradouro( String dsLogradouro ) {
		this.dsLogradouro = dsLogradouro;
	}

	/**
	 * Retorna Descri��o do Logradouro
	 * @return Descri��o do Logradouro
	 */
	public String getDsLogradouro() {
		return this.dsLogradouro;
	}

	/**
	 * Alimenta Descri��o de Bairro
	 * @param dsBairro Descri��o de Bairro
	 */ 
	public void setDsBairro( String dsBairro ) {
		this.dsBairro = dsBairro;
	}

	/**
	 * Retorna Descri��o de Bairro
	 * @return Descri��o de Bairro
	 */
	public String getDsBairro() {
		return this.dsBairro;
	}

	/**
	 * Alimenta Descri��o de Cidade
	 * @param dsCidade Descri��o de Cidade
	 */ 
	public void setDsCidade( String dsCidade ) {
		this.dsCidade = dsCidade;
	}

	/**
	 * Retorna Descri��o de Cidade
	 * @return Descri��o de Cidade
	 */
	public String getDsCidade() {
		return this.dsCidade;
	}

	/**
	 * Alimenta UF da residencia
	 * @param cdEstado UF da residencia
	 */ 
	public void setCdEstado( String cdEstado ) {
		this.cdEstado = cdEstado;
	}

	/**
	 * Retorna UF da residencia
	 * @return UF da residencia
	 */
	public String getCdEstado() {
		return this.cdEstado;
	}

}