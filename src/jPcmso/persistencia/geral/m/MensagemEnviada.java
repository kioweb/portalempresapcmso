package jPcmso.persistencia.geral.m;

import quatro.persistencia.TabelaBasica;
import quatro.smartDB.IgnoredField;
import quatro.smartDB.PrimaryKey;
import quatro.smartDB.Table;
import quatro.util.QtData;

/**
 *
 * Classe b�sica para PCMSO.MENSAGENS_ENVIADAS
 *
 * Tabela de Beneficiarios CBO
 * 
 * @author Samuel Antonio Klein
 * @see MensagemEnviadaNavegador
 * @see MensagemEnviadaPersistor
 * @see MensagemEnviadaLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 @Table(name="PCMSO.MENSAGENS_ENVIADAS")
 public class MensagemEnviada implements TabelaBasica {

	@IgnoredField
	private static final long serialVersionUID = -4697646631378357249L;

	/**  (chave) */
	@PrimaryKey
	private Integer sqMensagem;
	/**  */
	private String dsMensagem;
	/**  */
	private QtData dtMensagem;
	/**  */
	private String dsErroEmail;

	/**
	 * Alimenta 
	 * @param sqMensagem 
	 */ 
	public void setSqMensagem( Integer sqMensagem ) {
		this.sqMensagem = sqMensagem;
	}

	/**
	 * Retorna 
	 * @return 
	 */
	public Integer getSqMensagem() {
		return this.sqMensagem;
	}

	/**
	 * Alimenta 
	 * @param dsMensagem 
	 */ 
	public void setDsMensagem( String dsMensagem ) {
		this.dsMensagem = dsMensagem;
	}

	/**
	 * Retorna 
	 * @return 
	 */
	public String getDsMensagem() {
		return this.dsMensagem;
	}

	/**
	 * Alimenta 
	 * @param dtMensagem 
	 */ 
	public void setDtMensagem( QtData dtMensagem ) {
		this.dtMensagem = dtMensagem;
	}

	/**
	 * Retorna 
	 * @return 
	 */
	public QtData getDtMensagem() {
		return this.dtMensagem;
	}

	/**
	 * Alimenta 
	 * @param dsErroEmail 
	 */ 
	public void setDsErroEmail( String dsErroEmail ) {
		this.dsErroEmail = dsErroEmail;
	}

	/**
	 * Retorna 
	 * @return 
	 */
	public String getDsErroEmail() {
		return this.dsErroEmail;
	}

}