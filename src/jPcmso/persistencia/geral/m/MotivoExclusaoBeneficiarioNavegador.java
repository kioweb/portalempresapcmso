package jPcmso.persistencia.geral.m;

import quatro.persistencia.FiltroDeNavegador;
import quatro.persistencia.Navegador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.sql.Query;

/**
 *
 * Classe navegador para MOTIVOS_EXCLUSAO_BENEFICIARIO
 *
 * @author Samuel Antonio Klein
 * @see Navegador
 * @see MotivoExclusaoBeneficiario
 * @see MotivoExclusaoBeneficiarioPersistor
 * @see MotivoExclusaoBeneficiarioLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 public class MotivoExclusaoBeneficiarioNavegador extends Navegador {

	
	/** Inst�ncia de MotivoExclusaoBeneficiario para manipula��o pelo navegador */
	private MotivoExclusaoBeneficiario motivoExclusaoBeneficiario;

	/** Construtor b�sico */
	public MotivoExclusaoBeneficiarioNavegador()
	  throws QtSQLException { 

		motivoExclusaoBeneficiario = new MotivoExclusaoBeneficiario();
		primeiro();
	}

	/** Construtor b�sico que recebe uma conex�o */
	public MotivoExclusaoBeneficiarioNavegador( Conexao conexao )
	  throws QtSQLException { 

		super.setConexao( conexao ); 
		motivoExclusaoBeneficiario = new MotivoExclusaoBeneficiario();
		primeiro();
	}

	/**
	 * Construtor que recebe uma inst�ncia de MotivoExclusaoBeneficiarioFiltro e posiciona
	 * no primeiro registro que satisfaz suas condi��es 
	 */
	public MotivoExclusaoBeneficiarioNavegador( FiltroDeNavegador filtro ) throws QtSQLException  {
		super.setFiltro( filtro );
		this.motivoExclusaoBeneficiario = new MotivoExclusaoBeneficiario();
		primeiro();
	}

	/** Construtor que recebe a chave da tabela MOTIVOS_EXCLUSAO_BENEFICIARIO
	 * @param idMotivoExclusao Motivo de Exclus�o
	 */
	public MotivoExclusaoBeneficiarioNavegador( int idMotivoExclusao )
	  throws QtSQLException { 

		motivoExclusaoBeneficiario = new MotivoExclusaoBeneficiario();
		this.motivoExclusaoBeneficiario.setIdMotivoExclusao( new Integer( idMotivoExclusao ) );
		
		busca();
	}

	/** Construtor que recebe  uma conex�o e a chave da tabela MOTIVOS_EXCLUSAO_BENEFICIARIO
	 * @param idMotivoExclusao Motivo de Exclus�o
	 * @param conexao Conexao com o banco de dados
	 */
	public MotivoExclusaoBeneficiarioNavegador( Conexao conexao, int idMotivoExclusao )
	  throws QtSQLException { 

		super.setConexao( conexao ); 
		motivoExclusaoBeneficiario = new MotivoExclusaoBeneficiario();
		this.motivoExclusaoBeneficiario.setIdMotivoExclusao( new Integer( idMotivoExclusao ) );
		
		busca();
	}

	/** M�todo que retorna uma tabela b�sica a que se refere o navegador */
	public TabelaBasica getTabelaBasica() {

		return this.motivoExclusaoBeneficiario;
	}

	/** M�todo que retorna a classe MotivoExclusaoBeneficiario relacionada ao navegador */
	public MotivoExclusaoBeneficiario getMotivoExclusaoBeneficiario() {

		return this.motivoExclusaoBeneficiario;
	}

	 /** M�todo que move para o primeiro registro da tabela */
	 public boolean primeiro() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select min( ID_MOTIVO_EXCLUSAO ) from MOTIVOS_EXCLUSAO_BENEFICIARIO" );
			query.addSQL( super.getCondicaoDeFiltro( FiltroDeNavegador.COM_WHERE ) 	);

			query.executeQuery();

			
			if( query.getString( 1 ) == null ) {

				super.setQueryStates( false, true, true );
			} else {
				this.motivoExclusaoBeneficiario = new MotivoExclusaoBeneficiario();

				motivoExclusaoBeneficiario.setIdMotivoExclusao( query.getInteger( 1 ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o registro anterior na tabela */
	 public boolean anterior() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select max( ID_MOTIVO_EXCLUSAO ) from MOTIVOS_EXCLUSAO_BENEFICIARIO" );
			query.addSQL( " where ID_MOTIVO_EXCLUSAO < :prm1IdMotivoExclusao" );
			query.addSQL( super.getCondicaoDeFiltro() );

			query.setParameter( "prm1IdMotivoExclusao", motivoExclusaoBeneficiario.getIdMotivoExclusao() );
			
			query.executeQuery();

			
			if( query.getString( 1 ) == null ) {

				super.setFound( false );
				super.setBof( true );
			} else {
				this.motivoExclusaoBeneficiario = new MotivoExclusaoBeneficiario();

				motivoExclusaoBeneficiario.setIdMotivoExclusao( query.getInteger( 1 ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o proximo registro da tabela */
	 public boolean proximo() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select min( ID_MOTIVO_EXCLUSAO ) from MOTIVOS_EXCLUSAO_BENEFICIARIO" );
			query.addSQL( " where ID_MOTIVO_EXCLUSAO > :prm1IdMotivoExclusao" );
			query.addSQL( super.getCondicaoDeFiltro() );

			query.setParameter( "prm1IdMotivoExclusao", motivoExclusaoBeneficiario.getIdMotivoExclusao() );

			query.executeQuery();

			
			if( query.getString( 1 ) == null ) {

				super.setFound( false );
				super.setEof( true );
			} else {
				this.motivoExclusaoBeneficiario = new MotivoExclusaoBeneficiario();

				motivoExclusaoBeneficiario.setIdMotivoExclusao( query.getInteger( 1 ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o �ltimo registro da tabela */
	 public boolean ultimo() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select max( ID_MOTIVO_EXCLUSAO ) from MOTIVOS_EXCLUSAO_BENEFICIARIO" );
			query.addSQL( super.getCondicaoDeFiltro( FiltroDeNavegador.COM_WHERE ) 	);

			query.executeQuery();

			
			if( query.getString( 1 ) == null ) {

				super.setQueryStates( false, true, true );
			} else {
				this.motivoExclusaoBeneficiario = new MotivoExclusaoBeneficiario();

				motivoExclusaoBeneficiario.setIdMotivoExclusao( query.getInteger( 1 ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	/**
	 * M�todo para buscar e preencher uma tupla de uma tabela
	 * @param tabelaBasica Recebe uma tabela basica com os atributos chave preenchidos
	 */
	public boolean busca( TabelaBasica tabelaBasica ) throws QtSQLException {

		if( !( tabelaBasica instanceof MotivoExclusaoBeneficiario ) )
			throw new QtSQLException( "Esperava um objeto MotivoExclusaoBeneficiario!" );

		this.motivoExclusaoBeneficiario = (MotivoExclusaoBeneficiario) tabelaBasica;
		return busca();

	}

	/**
	 * m�todo interno para buscar e preencher uma tupla de uma tabela
	 * Utiliza a Inst�ncia de MotivoExclusaoBeneficiario da classe para fazer a busca
	 */
	private boolean busca() throws QtSQLException {

		MotivoExclusaoBeneficiarioLocalizador localizador = new MotivoExclusaoBeneficiarioLocalizador( getConexao(), this.motivoExclusaoBeneficiario );

		if( localizador.isEmpty() ) {

			super.setQueryStates( false, true, true );
			return false;

		} else {

			super.setQueryStates( true, false, false );
			return true;
		}
	}
}