package jPcmso.persistencia.geral.m;

import java.sql.PreparedStatement;

import quatro.persistencia.Persistor;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.util.QtException;

/**
 *
 * Classe persistor para PCMSO.MEDICOS
 *
 * @author Samuel Antonio Klein
 * @see Persistor
 * @see Medico
 * @see MedicoNavegador
 * @see MedicoLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 public class MedicoPersistor extends Persistor {

	/** Inst�ncia de Medico para manipula��o pelo persistor */
	private Medico medico;

	/** Construtor gen�rico */
	public MedicoPersistor() {

		this.medico = null;
	}

	/** Construtor gen�rico */
	public MedicoPersistor( Conexao cnx ) throws QtSQLException {

		this.medico = null;
		super.setConexao( cnx );
	}

	/** Construtor que recebe uma inst�ncia de Medico */
	public MedicoPersistor( Medico medico ) throws QtSQLException {

		if( medico == null ) 
			throw new QtSQLException( "Imposs�vel instanciar MedicoPersistor porque est� apontando para um nulo!" );

		this.medico = medico;
	}

	/** Construtor que recebe uma conex�o e uma inst�ncia de Medico */
	public MedicoPersistor( Conexao conexao, Medico medico ) throws QtSQLException {

		if( medico == null ) 
			throw new QtSQLException( "Imposs�vel instanciar MedicoPersistor porque est� apontando para um nulo!" );

		setConexao( conexao );
		this.medico = medico;
	}

	/**
	 * Construtor que recebe a chave da tabela PCMSO.MEDICOS
	 * @param cdMedico Codigo do medico
	 */
	public MedicoPersistor( String cdMedico )
	  throws QtSQLException { 

		busca( cdMedico );
	}

	/**
	 * Construtor que recebe uma conex�o e a chave da tabela PCMSO.MEDICOS
	 * @param cdMedico Codigo do medico
	 * @param conexao Conexao com o banco de dados
	 */
	public MedicoPersistor( Conexao conexao, String cdMedico )
	  throws QtSQLException { 

		super.setConexao( conexao );
		busca( cdMedico );
	}

	/**
	 * M�todo interno para buscar e preencher uma tupla de uma tabela
	 * Utiliza a Inst�ncia de Medico para fazer a busca
	 * @param cdMedico Codigo do medico
	 */
	private void busca( String cdMedico ) throws QtSQLException { 

		MedicoLocalizador medicoLocalizador = new MedicoLocalizador( getConexao(), cdMedico );

		if( medicoLocalizador.isEmpty() ) {
			throw new QtSQLException( "Chaves da Tabela Medico n�o encontradas - cdMedico: " + cdMedico );
		}

		medico = (Medico ) medicoLocalizador.getTabelaBasica();
	}

	/**
	 * Permite alimentar o Persistor com um novo registro
	 */
	public void setTabelaBasica( TabelaBasica medico ) throws QtException {

		if( medico instanceof Medico ) {
			this.medico = (Medico) medico;
		} else {
			throw new QtException( "Tabela B�sica fornecida tem que ser uma inst�ncia de Medico" );
		}
	}

	/**
	 * Permite acessar o registro corrente do persistor
	 */
	public TabelaBasica getTabelaBasica() throws QtException {

		return medico;
	}

	/**
	 * M�todo para inserir tupla na tabela
	 */
	public void insere() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "insert into PCMSO.MEDICOS" );
			cmd.append( "( CD_MEDICO, CD_SIGLA, UF_CRM, NR_CRM, NM_MEDICO, SN_COORDENADOR, NR_DDD, NR_TELEFONE, DS_LOGRADOURO, DS_BAIRRO, DS_CIDADE, CD_ESTADO )" );
			cmd.append( "values " );
			cmd.append( "( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			ps.setObject( 1, medico.getCdMedico() );
			ps.setObject( 2, medico.getCdSigla() );
			ps.setObject( 3, medico.getUfCrm() );
			ps.setObject( 4, medico.getNrCrm() );
			ps.setObject( 5, medico.getNmMedico() );
			ps.setObject( 6, medico.isSnCoordenador() ? "S": "N"  );
			ps.setObject( 7, medico.getNrDdd() );
			ps.setObject( 8, medico.getNrTelefone() );
			ps.setObject( 9, medico.getDsLogradouro() );
			ps.setObject( 10, medico.getDsBairro() );
			ps.setObject( 11, medico.getDsCidade() );
			ps.setObject( 12, medico.getCdEstado() );
			
			ps.execute();

		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}

	/**
	 * M�todo para alterar tupla na tabela
	 */
	public void altera() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "update PCMSO.MEDICOS" );
			cmd.append( "  set CD_MEDICO = ?, " );
			cmd.append( " CD_SIGLA = ?, " );
			cmd.append( " UF_CRM = ?, " );
			cmd.append( " NR_CRM = ?, " );
			cmd.append( " NM_MEDICO = ?, " );
			cmd.append( " SN_COORDENADOR = ?, " );
			cmd.append( " NR_DDD = ?, " );
			cmd.append( " NR_TELEFONE = ?, " );
			cmd.append( " DS_LOGRADOURO = ?, " );
			cmd.append( " DS_BAIRRO = ?, " );
			cmd.append( " DS_CIDADE = ?, " );
			cmd.append( " CD_ESTADO = ?" );
			cmd.append( " where CD_MEDICO like ?" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			ps.setObject( 1, medico.getCdMedico() );
			ps.setObject( 2, medico.getCdSigla() );
			ps.setObject( 3, medico.getUfCrm() );
			ps.setObject( 4, medico.getNrCrm() );
			ps.setObject( 5, medico.getNmMedico() );
			ps.setObject( 6, medico.isSnCoordenador() ? "S": "N"  );
			ps.setObject( 7, medico.getNrDdd() );
			ps.setObject( 8, medico.getNrTelefone() );
			ps.setObject( 9, medico.getDsLogradouro() );
			ps.setObject( 10, medico.getDsBairro() );
			ps.setObject( 11, medico.getDsCidade() );
			ps.setObject( 12, medico.getCdEstado() );
			
			ps.setObject( 13, medico.getCdMedico() );
			
			ps.execute();

		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}

	/**
	 * M�todo para excluir tupla de uma tabela
	 */
	public void exclui() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "delete from PCMSO.MEDICOS" );
			cmd.append( " where CD_MEDICO like ?" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			ps.setObject( 1, medico.getCdMedico() );
			
			ps.execute();

		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}
}