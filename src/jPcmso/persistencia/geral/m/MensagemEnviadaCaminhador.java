package jPcmso.persistencia.geral.m;

import quatro.persistencia.Caminhador;
import quatro.persistencia.FiltroDeNavegador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.util.QtData;

 /**
 *
 * Classe Caminhador para MensagemEnviada
 *
 * @author Samuel Antonio Klein
 * @see Caminhador
 * @see FiltroDeNavegador
 * @see MensagemEnviadaNavegador
 * @see MensagemEnviadaPersistor
 * @see MensagemEnviadaLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 public class MensagemEnviadaCaminhador extends Caminhador {

	public static final int POR_ORDEM_DEFAULT = -1;
	
	private MensagemEnviada mensagemEnviada;

	public MensagemEnviadaCaminhador( FiltroDeNavegador filtro ) {

		setFiltro( filtro );
		inicializaVisao();
	}

	public MensagemEnviadaCaminhador( Conexao conexao, FiltroDeNavegador filtro ) throws QtSQLException {

		super.setConexao( conexao );
		setFiltro( filtro );
		inicializaVisao();
	}

	private void inicializaVisao() {

		setNmVisao( "PCMSO.MENSAGENS_ENVIADAS" );
	}

	protected void carregaCampos() throws QtSQLException {

		mensagemEnviada = new MensagemEnviada();

		if( isRecordAvailable() ) {
			mensagemEnviada.setSqMensagem( query.getInteger( "SQ_MENSAGEM" ) );
			mensagemEnviada.setDsMensagem( query.getString( "DS_MENSAGEM" ) );
			mensagemEnviada.setDtMensagem(  QtData.criaQtData( query.getTimestamp( "DT_MENSAGEM" ) ) );
			mensagemEnviada.setDsErroEmail( query.getString( "DS_ERRO_EMAIL" ) );
		}
	}

	/** M�todo que retorna uma tabela b�sica a que se refere o caminhador */
	public TabelaBasica getTabelaBasica() {

		return this.mensagemEnviada;
	}

	/** M�todo que retorna a classe MensagemEnviada relacionada ao caminhador */
	public MensagemEnviada getMensagemEnviada() {

		return this.mensagemEnviada;
	}

	public void ativaVisao() throws QtSQLException {

		if(  super.getOrdemDeNavegacao() == MensagemEnviadaCaminhador.POR_ORDEM_DEFAULT ) {
			super.ativaCaminhador( "order by SQ_MENSAGEM" );
	
		} else {
			throw new QtSQLException( "MensagemEnviadaCaminhador - Ordem de Navega��o n�o definida!" );
		}
	}

	public String getCamposIndice( int qualIndice ) {
		return getDsIndice( qualIndice );
	}

	public String getCamposChave() {
		return getDsChave();
	}

	/**
	 * M�todo est�tico que devolve os campos que comp�e a chave da tabela;
	 * @return campos que comp�e a chave.
	 */
	public static String getDsChave() {
		return "SQ_MENSAGEM";
	}

	/**
	 * M�todo est�tico que devolve os campos que fazem parte de uma dada ordem
	 * de navega��o.
	 * @param qualIndice indica a ordem de navega��o desejada.
	 * @return campos que comp�e a ordem de navega��o.
	 */
	public static String getDsIndice( int qualIndice ) {

		switch( qualIndice ) {

			case POR_ORDEM_DEFAULT: return "SQ_MENSAGEM";
		}
		return null;
	}
}