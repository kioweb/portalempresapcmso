package jPcmso.persistencia.geral.m;

import quatro.persistencia.CondicaoDeFiltro;
import quatro.persistencia.FiltroDeNavegador;

/**
 *
 * Classe filtro para MOTIVOS_EXCLUSAO_BENEFICIARIO
 *
 * @author Samuel Antonio Klein
 * @see FiltroDeTabela
 * @see MotivoExclusaoBeneficiario
 * @see MotivoExclusaoBeneficiarioNavegador
 * @see MotivoExclusaoBeneficiarioPersistor
 * @see MotivoExclusaoBeneficiarioLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 public class MotivoExclusaoBeneficiarioFiltro extends FiltroDeNavegador {

	/** Filtro por descricao */
	public void setFiltroPorDescricao( String dsMotivoExclusao ) {

		super.adicionaCondicao( "DS_MOTIVO_EXCLUSAO",CondicaoDeFiltro.IGUAL, dsMotivoExclusao );
	}

	/** Determina a ordem de navega��o que ser� passada para o navegador */
	public void setOrdemDeNavegacao( int idOrdem ) {

		super.setOrdemDeNavegacao( idOrdem );
	}
}