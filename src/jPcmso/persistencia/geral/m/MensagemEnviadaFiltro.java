package jPcmso.persistencia.geral.m;

import quatro.persistencia.FiltroDeNavegador;

/**
 *
 * Classe filtro para PCMSO.MENSAGENS_ENVIADAS
 *
 * @author Samuel Antonio Klein
 * @see FiltroDeTabela
 * @see MensagemEnviada
 * @see MensagemEnviadaNavegador
 * @see MensagemEnviadaPersistor
 * @see MensagemEnviadaLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 public class MensagemEnviadaFiltro extends FiltroDeNavegador {

	/** Determina a ordem de navega��o que ser� passada para o navegador */
	public void setOrdemDeNavegacao( int idOrdem ) {

		super.setOrdemDeNavegacao( idOrdem );
	}
}