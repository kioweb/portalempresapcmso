package jPcmso.persistencia.geral.m;

import quatro.persistencia.FiltroDeNavegador;

/**
 *
 * Classe filtro para PCMSO.MENSAGEM_PARAMETRO
 *
 * @author Samuel Antonio Klein
 * @see FiltroDeTabela
 * @see MensagemParametro
 * @see MensagemParametroNavegador
 * @see MensagemParametroPersistor
 * @see MensagemParametroLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 public class MensagemParametroFiltro extends FiltroDeNavegador {

	/** Determina a ordem de navega��o que ser� passada para o navegador */
	public void setOrdemDeNavegacao( int idOrdem ) {

		super.setOrdemDeNavegacao( idOrdem );
	}
}