package jPcmso.persistencia.geral.m;

import quatro.persistencia.FiltroDeNavegador;
import quatro.persistencia.Navegador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.sql.Query;

/**
 *
 * Classe navegador para PCMSO.MEDICOS
 *
 * @author Samuel Antonio Klein
 * @see Navegador
 * @see Medico
 * @see MedicoPersistor
 * @see MedicoLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 public class MedicoNavegador extends Navegador {

	
	/** Inst�ncia de Medico para manipula��o pelo navegador */
	private Medico medico;

	/** Construtor b�sico */
	public MedicoNavegador()
	  throws QtSQLException { 

		medico = new Medico();
		primeiro();
	}

	/** Construtor b�sico que recebe uma conex�o */
	public MedicoNavegador( Conexao conexao )
	  throws QtSQLException { 

		super.setConexao( conexao ); 
		medico = new Medico();
		primeiro();
	}

	/**
	 * Construtor que recebe uma inst�ncia de MedicoFiltro e posiciona
	 * no primeiro registro que satisfaz suas condi��es 
	 */
	public MedicoNavegador( FiltroDeNavegador filtro ) throws QtSQLException  {
		super.setFiltro( filtro );
		this.medico = new Medico();
		primeiro();
	}

	/** Construtor que recebe a chave da tabela PCMSO.MEDICOS
	 * @param cdMedico Codigo do medico
	 */
	public MedicoNavegador( String cdMedico )
	  throws QtSQLException { 

		medico = new Medico();
		this.medico.setCdMedico( cdMedico );
		
		busca();
	}

	/** Construtor que recebe  uma conex�o e a chave da tabela PCMSO.MEDICOS
	 * @param cdMedico Codigo do medico
	 * @param conexao Conexao com o banco de dados
	 */
	public MedicoNavegador( Conexao conexao, String cdMedico )
	  throws QtSQLException { 

		super.setConexao( conexao ); 
		medico = new Medico();
		this.medico.setCdMedico( cdMedico );
		
		busca();
	}

	/** M�todo que retorna uma tabela b�sica a que se refere o navegador */
	public TabelaBasica getTabelaBasica() {

		return this.medico;
	}

	/** M�todo que retorna a classe Medico relacionada ao navegador */
	public Medico getMedico() {

		return this.medico;
	}

	 /** M�todo que move para o primeiro registro da tabela */
	 public boolean primeiro() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select min( CD_MEDICO ) from PCMSO.MEDICOS" );
			query.addSQL( super.getCondicaoDeFiltro( FiltroDeNavegador.COM_WHERE ) 	);

			query.executeQuery();

			
			if( query.getString( 1 ) == null ) {

				super.setQueryStates( false, true, true );
			} else {
				this.medico = new Medico();

				medico.setCdMedico( query.getString( 1 ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o registro anterior na tabela */
	 public boolean anterior() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select max( CD_MEDICO ) from PCMSO.MEDICOS" );
			query.addSQL( " where CD_MEDICO < :prm1CdMedico" );
			query.addSQL( super.getCondicaoDeFiltro() );

			query.setParameter( "prm1CdMedico", medico.getCdMedico() );
			
			query.executeQuery();

			
			if( query.getString( 1 ) == null ) {

				super.setFound( false );
				super.setBof( true );
			} else {
				this.medico = new Medico();

				medico.setCdMedico( query.getString( 1 ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o proximo registro da tabela */
	 public boolean proximo() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select min( CD_MEDICO ) from PCMSO.MEDICOS" );
			query.addSQL( " where CD_MEDICO > :prm1CdMedico" );
			query.addSQL( super.getCondicaoDeFiltro() );

			query.setParameter( "prm1CdMedico", medico.getCdMedico() );

			query.executeQuery();

			
			if( query.getString( 1 ) == null ) {

				super.setFound( false );
				super.setEof( true );
			} else {
				this.medico = new Medico();

				medico.setCdMedico( query.getString( 1 ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o �ltimo registro da tabela */
	 public boolean ultimo() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select max( CD_MEDICO ) from PCMSO.MEDICOS" );
			query.addSQL( super.getCondicaoDeFiltro( FiltroDeNavegador.COM_WHERE ) 	);

			query.executeQuery();

			
			if( query.getString( 1 ) == null ) {

				super.setQueryStates( false, true, true );
			} else {
				this.medico = new Medico();

				medico.setCdMedico( query.getString( 1 ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	/**
	 * M�todo para buscar e preencher uma tupla de uma tabela
	 * @param tabelaBasica Recebe uma tabela basica com os atributos chave preenchidos
	 */
	public boolean busca( TabelaBasica tabelaBasica ) throws QtSQLException {

		if( !( tabelaBasica instanceof Medico ) )
			throw new QtSQLException( "Esperava um objeto Medico!" );

		this.medico = (Medico) tabelaBasica;
		return busca();

	}

	/**
	 * m�todo interno para buscar e preencher uma tupla de uma tabela
	 * Utiliza a Inst�ncia de Medico da classe para fazer a busca
	 */
	private boolean busca() throws QtSQLException {

		MedicoLocalizador localizador = new MedicoLocalizador( getConexao(), this.medico );

		if( localizador.isEmpty() ) {

			super.setQueryStates( false, true, true );
			return false;

		} else {

			super.setQueryStates( true, false, false );
			return true;
		}
	}
}