package jPcmso.persistencia.geral.m;

import quatro.persistencia.Caminhador;
import quatro.persistencia.FiltroDeNavegador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;

 /**
 *
 * Classe Caminhador para Medico
 *
 * @author Samuel Antonio Klein
 * @see Caminhador
 * @see FiltroDeNavegador
 * @see MedicoNavegador
 * @see MedicoPersistor
 * @see MedicoLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 public class MedicoCaminhador extends Caminhador {

	public static final int POR_ORDEM_DEFAULT = -1;
	
	private Medico medico;

	public MedicoCaminhador( FiltroDeNavegador filtro ) {

		setFiltro( filtro );
		inicializaVisao();
	}

	public MedicoCaminhador( Conexao conexao, FiltroDeNavegador filtro ) throws QtSQLException {

		super.setConexao( conexao );
		setFiltro( filtro );
		inicializaVisao();
	}

	private void inicializaVisao() {

		setNmVisao( "PCMSO.MEDICOS" );
	}

	protected void carregaCampos() throws QtSQLException {

		medico = new Medico();

		if( isRecordAvailable() ) {
			medico.setCdMedico( query.getString( "CD_MEDICO" ) );
			medico.setCdSigla( query.getString( "CD_SIGLA" ) );
			medico.setUfCrm( query.getString( "UF_CRM" ) );
			medico.setNrCrm( query.getInteger( "NR_CRM" ) );
			medico.setNmMedico( query.getString( "NM_MEDICO" ) );
			medico.setSnCoordenador( query.getString( "SN_COORDENADOR" ) );
			medico.setNrDdd( query.getString( "NR_DDD" ) );
			medico.setNrTelefone( query.getString( "NR_TELEFONE" ) );
			medico.setDsLogradouro( query.getString( "DS_LOGRADOURO" ) );
			medico.setDsBairro( query.getString( "DS_BAIRRO" ) );
			medico.setDsCidade( query.getString( "DS_CIDADE" ) );
			medico.setCdEstado( query.getString( "CD_ESTADO" ) );
		}
	}

	/** M�todo que retorna uma tabela b�sica a que se refere o caminhador */
	public TabelaBasica getTabelaBasica() {

		return this.medico;
	}

	/** M�todo que retorna a classe Medico relacionada ao caminhador */
	public Medico getMedico() {

		return this.medico;
	}

	public void ativaVisao() throws QtSQLException {

		if(  super.getOrdemDeNavegacao() == MedicoCaminhador.POR_ORDEM_DEFAULT ) {
			super.ativaCaminhador( "order by CD_MEDICO" );
	
		} else {
			throw new QtSQLException( "MedicoCaminhador - Ordem de Navega��o n�o definida!" );
		}
	}

	public String getCamposIndice( int qualIndice ) {
		return getDsIndice( qualIndice );
	}

	public String getCamposChave() {
		return getDsChave();
	}

	/**
	 * M�todo est�tico que devolve os campos que comp�e a chave da tabela;
	 * @return campos que comp�e a chave.
	 */
	public static String getDsChave() {
		return "CD_MEDICO";
	}

	/**
	 * M�todo est�tico que devolve os campos que fazem parte de uma dada ordem
	 * de navega��o.
	 * @param qualIndice indica a ordem de navega��o desejada.
	 * @return campos que comp�e a ordem de navega��o.
	 */
	public static String getDsIndice( int qualIndice ) {

		switch( qualIndice ) {

			case POR_ORDEM_DEFAULT: return "CD_MEDICO";
		}
		return null;
	}
}