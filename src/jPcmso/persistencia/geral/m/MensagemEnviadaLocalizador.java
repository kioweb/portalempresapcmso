package jPcmso.persistencia.geral.m;

import quatro.persistencia.Localizador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.QtSQLException;
import quatro.sql.Query;
import quatro.sql.Conexao;
import quatro.sql.PoolDeConexoes;
import quatro.sql.ConexaoParaPoolDeConexoes;
import quatro.util.QtData;

/**
 *
 * Classe localizador para PCMSO.MENSAGENS_ENVIADAS
 *
 * @author Samuel Antonio Klein
 * @see Localizador
 * @see MensagemEnviada
 * @see MensagemEnviadaNavegador
 * @see MensagemEnviadaPersistor
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 public class MensagemEnviadaLocalizador extends Localizador {

	/** Inst�ncia de MensagemEnviada para manipula��o pelo localizador */
	private MensagemEnviada mensagemEnviada;

	/** Construtor que recebe uma conex�o e um objeto MensagemEnviada completo */
	public MensagemEnviadaLocalizador( Conexao conexao, MensagemEnviada mensagemEnviada ) throws QtSQLException {

		super.setConexao( conexao );

		this.mensagemEnviada = mensagemEnviada;
		busca();
	}

	/** Construtor que recebe um objeto MensagemEnviada completo */
	public MensagemEnviadaLocalizador( MensagemEnviada mensagemEnviada ) throws QtSQLException {

		this.mensagemEnviada = mensagemEnviada;
		busca();
	}

	/** 
	 * Construtor que recebe uma conex�o e a chave da tabela PCMSO.MENSAGENS_ENVIADAS
	 * @param sqMensagem 
	 * @param conexao Conexao com o banco de dados
	 */
	public MensagemEnviadaLocalizador( Conexao conexao, int sqMensagem ) throws QtSQLException { 

		super.setConexao( conexao ); 
		mensagemEnviada = new MensagemEnviada();
		this.mensagemEnviada.setSqMensagem( new Integer( sqMensagem ) );
		
		busca();
	}

	/** 
	 * Construtor que recebe a chave da tabela PCMSO.MENSAGENS_ENVIADAS
	 * @param sqMensagem 
	 */
	public MensagemEnviadaLocalizador( int sqMensagem ) throws QtSQLException { 

		mensagemEnviada = new MensagemEnviada();
		this.mensagemEnviada.setSqMensagem( new Integer( sqMensagem ) );
		
		busca();
	}

	public static boolean existe( Conexao cnx, int sqMensagem ) throws QtSQLException {

		Query q = new Query( cnx );

		try {
			q.setSQL( "select count(*) from PCMSO.MENSAGENS_ENVIADAS" );
			q.addSQL( " where SQ_MENSAGEM = :prm1SqMensagem" );

			q.setParameter( "prm1SqMensagem", sqMensagem );
			q.executeQuery();

			return q.getInt( 1 ) > 0;
		} finally {
			q.liberaRecursos();
		}
	}

	public static boolean existe( int sqMensagem ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return existe( cnx, sqMensagem );
		} finally {
			cnx.libera();
		}
	}

	public static boolean existe( Conexao cnx, MensagemEnviada mensagemEnviada ) throws QtSQLException {

		return existe( cnx, mensagemEnviada.getSqMensagem() );
	}

	public static boolean existe( MensagemEnviada mensagemEnviada ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return existe( cnx, mensagemEnviada.getSqMensagem() );
		} finally {
			cnx.libera();
		}
	}

	public static MensagemEnviada buscaMensagemEnviada( Conexao cnx, int sqMensagem ) throws QtSQLException {

		Query q = new Query( cnx );

		try {
			q.setSQL( "select * from PCMSO.MENSAGENS_ENVIADAS" );
			q.addSQL( " where SQ_MENSAGEM = :prm1SqMensagem" );

			q.setParameter( "prm1SqMensagem", sqMensagem );
			q.executeQuery();

			if( q.isEmpty() ) {
				return null;
			} else {
				MensagemEnviada mensagemEnviada = new MensagemEnviada();
				buscaCampos( mensagemEnviada, q );
				return mensagemEnviada;
			}
		} finally {
			q.liberaRecursos();
		}
	}

	public static MensagemEnviada buscaMensagemEnviada( int sqMensagem ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return buscaMensagemEnviada( cnx, sqMensagem );
		} finally { 
			cnx.libera();
		}
	}

	/** 
	 * Construtor que recebe uma conex�o e a chave da tabela PCMSO.MENSAGENS_ENVIADAS
	 * @param sqMensagem 
	 * @param conexao Conexao com o banco de dados
	 */
	public MensagemEnviadaLocalizador( Conexao conexao, Integer sqMensagem ) throws QtSQLException {

		super.setConexao( conexao ); 
		mensagemEnviada = new MensagemEnviada();
		this.mensagemEnviada.setSqMensagem( sqMensagem );
		
		busca();
	}

	/** 
	 * Construtor que recebe a chave da tabela PCMSO.MENSAGENS_ENVIADAS
	 * @param sqMensagem 
	 */
	public MensagemEnviadaLocalizador( Integer sqMensagem ) throws QtSQLException {

		mensagemEnviada = new MensagemEnviada();
		this.mensagemEnviada.setSqMensagem( sqMensagem );
		
		busca();
	}

	public static void buscaCampos( MensagemEnviada mensagemEnviada, Query query ) throws QtSQLException {

		mensagemEnviada.setSqMensagem( query.getInteger( "SQ_MENSAGEM" ) );
		mensagemEnviada.setDsMensagem( query.getString( "DS_MENSAGEM" ) );
		mensagemEnviada.setDtMensagem( QtData.criaQtData( query.getTimestamp( "DT_MENSAGEM" ) ) );
		mensagemEnviada.setDsErroEmail( query.getString( "DS_ERRO_EMAIL" ) );
	}

	/** M�todo que retorna uma tabela b�sica a que se refere o localizador */
	public TabelaBasica getTabelaBasica() {

		return this.mensagemEnviada;
	}

	/** M�todo que retorna a classe MensagemEnviada relacionada ao localizador */
	public MensagemEnviada getMensagemEnviada() {

		return this.mensagemEnviada;
	}

	/**
	 * M�todo para buscar e preencher uma tupla de uma tabela
	 * @param tabelaBasica Recebe uma tabela basica com os atributos chave preenchidos
	 */
	public boolean busca( TabelaBasica tabelaBasica ) throws QtSQLException {

		if( !( tabelaBasica instanceof MensagemEnviada ) )
			throw new QtSQLException( "Esperava um objeto MensagemEnviada!" );

		this.mensagemEnviada = (MensagemEnviada) tabelaBasica;
		return busca();

	}

	/**
	 * M�todo privado para buscar e preencher uma tupla de uma tabela
	 * Este m�todo busca os dados da inst�ncia de MensagemEnviada atual
	 */
	private boolean busca() throws QtSQLException {

		boolean ret = false;

		super.estabeleceConexao();
		Query query = super.criaNovaQuery();

		try {

			query.setSQL( "select * from PCMSO.MENSAGENS_ENVIADAS" );
			query.addSQL( " where SQ_MENSAGEM = :prm1SqMensagem" );

			query.setParameter( "prm1SqMensagem", mensagemEnviada.getSqMensagem() );
			query.executeQuery();

			if( query.isEmpty() ) {
				super.setEmpty( true );
				mensagemEnviada = null;
			} else {
				super.setEmpty( false );
				
				mensagemEnviada.setSqMensagem( query.getInteger( "SQ_MENSAGEM" ) );
				mensagemEnviada.setDsMensagem( query.getString( "DS_MENSAGEM" ) );
				mensagemEnviada.setDtMensagem( QtData.criaQtData( query.getTimestamp( "DT_MENSAGEM" ) ) );
				mensagemEnviada.setDsErroEmail( query.getString( "DS_ERRO_EMAIL" ) );
				ret = true;
			}
		} finally {
			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}
 }