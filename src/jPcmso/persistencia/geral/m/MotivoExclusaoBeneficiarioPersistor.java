package jPcmso.persistencia.geral.m;

import java.sql.PreparedStatement;

import quatro.persistencia.Persistor;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.util.QtException;

/**
 *
 * Classe persistor para MOTIVOS_EXCLUSAO_BENEFICIARIO
 *
 * @author Samuel Antonio Klein
 * @see Persistor
 * @see MotivoExclusaoBeneficiario
 * @see MotivoExclusaoBeneficiarioNavegador
 * @see MotivoExclusaoBeneficiarioLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 public class MotivoExclusaoBeneficiarioPersistor extends Persistor {

	/** Inst�ncia de MotivoExclusaoBeneficiario para manipula��o pelo persistor */
	private MotivoExclusaoBeneficiario motivoExclusaoBeneficiario;

	/** Construtor gen�rico */
	public MotivoExclusaoBeneficiarioPersistor() {

		this.motivoExclusaoBeneficiario = null;
	}

	/** Construtor gen�rico */
	public MotivoExclusaoBeneficiarioPersistor( Conexao cnx ) throws QtSQLException {

		this.motivoExclusaoBeneficiario = null;
		super.setConexao( cnx );
	}

	/** Construtor que recebe uma inst�ncia de MotivoExclusaoBeneficiario */
	public MotivoExclusaoBeneficiarioPersistor( MotivoExclusaoBeneficiario motivoExclusaoBeneficiario ) throws QtSQLException {

		if( motivoExclusaoBeneficiario == null ) 
			throw new QtSQLException( "Imposs�vel instanciar MotivoExclusaoBeneficiarioPersistor porque est� apontando para um nulo!" );

		this.motivoExclusaoBeneficiario = motivoExclusaoBeneficiario;
	}

	/** Construtor que recebe uma conex�o e uma inst�ncia de MotivoExclusaoBeneficiario */
	public MotivoExclusaoBeneficiarioPersistor( Conexao conexao, MotivoExclusaoBeneficiario motivoExclusaoBeneficiario ) throws QtSQLException {

		if( motivoExclusaoBeneficiario == null ) 
			throw new QtSQLException( "Imposs�vel instanciar MotivoExclusaoBeneficiarioPersistor porque est� apontando para um nulo!" );

		setConexao( conexao );
		this.motivoExclusaoBeneficiario = motivoExclusaoBeneficiario;
	}

	/**
	 * Construtor que recebe a chave da tabela MOTIVOS_EXCLUSAO_BENEFICIARIO
	 * @param idMotivoExclusao Motivo de Exclus�o
	 */
	public MotivoExclusaoBeneficiarioPersistor( int idMotivoExclusao )
	  throws QtSQLException { 

		busca( idMotivoExclusao );
	}

	/**
	 * Construtor que recebe uma conex�o e a chave da tabela MOTIVOS_EXCLUSAO_BENEFICIARIO
	 * @param idMotivoExclusao Motivo de Exclus�o
	 * @param conexao Conexao com o banco de dados
	 */
	public MotivoExclusaoBeneficiarioPersistor( Conexao conexao, int idMotivoExclusao )
	  throws QtSQLException { 

		super.setConexao( conexao );
		busca( idMotivoExclusao );
	}

	/**
	 * M�todo interno para buscar e preencher uma tupla de uma tabela
	 * Utiliza a Inst�ncia de MotivoExclusaoBeneficiario para fazer a busca
	 * @param idMotivoExclusao Motivo de Exclus�o
	 */
	private void busca( int idMotivoExclusao ) throws QtSQLException { 

		MotivoExclusaoBeneficiarioLocalizador motivoExclusaoBeneficiarioLocalizador = new MotivoExclusaoBeneficiarioLocalizador( getConexao(), idMotivoExclusao );

		if( motivoExclusaoBeneficiarioLocalizador.isEmpty() ) {
			throw new QtSQLException( "Chaves da Tabela MotivoExclusaoBeneficiario n�o encontradas - idMotivoExclusao: " + idMotivoExclusao );
		}

		motivoExclusaoBeneficiario = (MotivoExclusaoBeneficiario ) motivoExclusaoBeneficiarioLocalizador.getTabelaBasica();
	}

	/**
	 * Permite alimentar o Persistor com um novo registro
	 */
	public void setTabelaBasica( TabelaBasica motivoExclusaoBeneficiario ) throws QtException {

		if( motivoExclusaoBeneficiario instanceof MotivoExclusaoBeneficiario ) {
			this.motivoExclusaoBeneficiario = (MotivoExclusaoBeneficiario) motivoExclusaoBeneficiario;
		} else {
			throw new QtException( "Tabela B�sica fornecida tem que ser uma inst�ncia de MotivoExclusaoBeneficiario" );
		}
	}

	/**
	 * Permite acessar o registro corrente do persistor
	 */
	public TabelaBasica getTabelaBasica() throws QtException {

		return motivoExclusaoBeneficiario;
	}

	/**
	 * M�todo para inserir tupla na tabela
	 */
	public void insere() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "insert into MOTIVOS_EXCLUSAO_BENEFICIARIO" );
			cmd.append( "( ID_MOTIVO_EXCLUSAO, TP_MOTIVO_EXCLUSAO, DS_MOTIVO_EXCLUSAO )" );
			cmd.append( "values " );
			cmd.append( "( ?, ?, ?)" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			ps.setObject( 1, motivoExclusaoBeneficiario.getIdMotivoExclusao() );
			ps.setObject( 2, motivoExclusaoBeneficiario.getTpMotivoExclusao() );
			ps.setObject( 3, motivoExclusaoBeneficiario.getDsMotivoExclusao() );
			
			ps.execute();

		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}

	/**
	 * M�todo para alterar tupla na tabela
	 */
	public void altera() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "update MOTIVOS_EXCLUSAO_BENEFICIARIO" );
			cmd.append( "  set ID_MOTIVO_EXCLUSAO = ?, " );
			cmd.append( " TP_MOTIVO_EXCLUSAO = ?, " );
			cmd.append( " DS_MOTIVO_EXCLUSAO = ?" );
			cmd.append( " where ID_MOTIVO_EXCLUSAO = ?" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			ps.setObject( 1, motivoExclusaoBeneficiario.getIdMotivoExclusao() );
			ps.setObject( 2, motivoExclusaoBeneficiario.getTpMotivoExclusao() );
			ps.setObject( 3, motivoExclusaoBeneficiario.getDsMotivoExclusao() );
			
			ps.setObject( 4, motivoExclusaoBeneficiario.getIdMotivoExclusao() );
			
			ps.execute();

		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}

	/**
	 * M�todo para excluir tupla de uma tabela
	 */
	public void exclui() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "delete from MOTIVOS_EXCLUSAO_BENEFICIARIO" );
			cmd.append( " where ID_MOTIVO_EXCLUSAO = ?" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			ps.setObject( 1, motivoExclusaoBeneficiario.getIdMotivoExclusao() );
			
			ps.execute();

		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}
}