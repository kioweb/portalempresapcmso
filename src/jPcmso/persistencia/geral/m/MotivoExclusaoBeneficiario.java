package jPcmso.persistencia.geral.m;

import quatro.persistencia.TabelaBasica;
import quatro.smartDB.IgnoredField;
import quatro.smartDB.PrimaryKey;
import quatro.smartDB.Table;

/**
 *
 * Classe b�sica para MOTIVOS_EXCLUSAO_BENEFICIARIO
 *
 * Tabela de Motivos de Exclus�o do Beneficiario
 * 
 * @author Samuel Antonio Klein
 * @see MotivoExclusaoBeneficiarioNavegador
 * @see MotivoExclusaoBeneficiarioPersistor
 * @see MotivoExclusaoBeneficiarioLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 @Table(name="MOTIVOS_EXCLUSAO_BENEFICIARIO")
 public class MotivoExclusaoBeneficiario implements TabelaBasica {

	@IgnoredField
	private static final long serialVersionUID = -6627662811157649416L;

	/** Motivo de Exclus�o (chave) */
	@PrimaryKey
	private Integer idMotivoExclusao;
	/** 
	 * Descri��o do Motivo de Exclus�o (char(2))
	 * 
	 * 01 - Rompimento do contrato por iniciativa do Benefici�rio
	 * 02 - T�rmino da rela��o de vinculado a um benefici�rio
	 * 03 - Desligamento da Empresa (planos coletivos)
	 * 04 - Inadimpl�ncia
	 * 05 - �bito
	 * 06 - Mudan�a de plano
	 * 07 - Exclus�o decorrente de mudan�a do c�digo do benefici�rio, motivada por adapta��o de sistema da operadora
	 * 08 - Transfer�ncia de carteira
	 * 09 - Altera��o do c�digo do benefici�rio.
	 * 11 - Plano antigo migrado
	 * 12 - Plano antigo adaptado
	 * 13 - Inclus�o indevida de benefici�rios
	 * 99 - Outros 
	 * 
	 * 
	 * 
	 * 
	 */
	private String tpMotivoExclusao;
	/** Descri��o do Motivo de Exclus�o Cadastrado (varchar(200)) */
	private String dsMotivoExclusao;

	/**
	 * Alimenta Motivo de Exclus�o
	 * @param idMotivoExclusao Motivo de Exclus�o
	 */ 
	public void setIdMotivoExclusao( Integer idMotivoExclusao ) {
		this.idMotivoExclusao = idMotivoExclusao;
	}

	/**
	 * Retorna Motivo de Exclus�o
	 * @return Motivo de Exclus�o
	 */
	public Integer getIdMotivoExclusao() {
		return this.idMotivoExclusao;
	}

	/**
	 * Alimenta Descri��o do Motivo de Exclus�o
	 * @param tpMotivoExclusao Descri��o do Motivo de Exclus�o
	 */ 
	public void setTpMotivoExclusao( String tpMotivoExclusao ) {
		this.tpMotivoExclusao = tpMotivoExclusao;
	}

	/**
	 * Retorna Descri��o do Motivo de Exclus�o
	 * @return Descri��o do Motivo de Exclus�o
	 */
	public String getTpMotivoExclusao() {
		return this.tpMotivoExclusao;
	}

	/**
	 * Alimenta Descri��o do Motivo de Exclus�o Cadastrado
	 * @param dsMotivoExclusao Descri��o do Motivo de Exclus�o Cadastrado
	 */ 
	public void setDsMotivoExclusao( String dsMotivoExclusao ) {
		this.dsMotivoExclusao = dsMotivoExclusao;
	}

	/**
	 * Retorna Descri��o do Motivo de Exclus�o Cadastrado
	 * @return Descri��o do Motivo de Exclus�o Cadastrado
	 */
	public String getDsMotivoExclusao() {
		return this.dsMotivoExclusao;
	}

}