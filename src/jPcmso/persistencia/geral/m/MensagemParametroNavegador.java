package jPcmso.persistencia.geral.m;

import quatro.persistencia.FiltroDeNavegador;
import quatro.persistencia.Navegador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.sql.Query;

/**
 *
 * Classe navegador para PCMSO.MENSAGEM_PARAMETRO
 *
 * @author Samuel Antonio Klein
 * @see Navegador
 * @see MensagemParametro
 * @see MensagemParametroPersistor
 * @see MensagemParametroLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 public class MensagemParametroNavegador extends Navegador {

	
	/** Inst�ncia de MensagemParametro para manipula��o pelo navegador */
	private MensagemParametro mensagemParametro;

	/** Construtor b�sico */
	public MensagemParametroNavegador()
	  throws QtSQLException { 

		mensagemParametro = new MensagemParametro();
		primeiro();
	}

	/** Construtor b�sico que recebe uma conex�o */
	public MensagemParametroNavegador( Conexao conexao )
	  throws QtSQLException { 

		super.setConexao( conexao ); 
		mensagemParametro = new MensagemParametro();
		primeiro();
	}

	/**
	 * Construtor que recebe uma inst�ncia de MensagemParametroFiltro e posiciona
	 * no primeiro registro que satisfaz suas condi��es 
	 */
	public MensagemParametroNavegador( FiltroDeNavegador filtro ) throws QtSQLException  {
		super.setFiltro( filtro );
		this.mensagemParametro = new MensagemParametro();
		primeiro();
	}

	/** Construtor que recebe a chave da tabela PCMSO.MENSAGEM_PARAMETRO
	 * @param idParametro 
	 */
	public MensagemParametroNavegador( int idParametro )
	  throws QtSQLException { 

		mensagemParametro = new MensagemParametro();
		this.mensagemParametro.setIdParametro( new Integer( idParametro ) );
		
		busca();
	}

	/** Construtor que recebe  uma conex�o e a chave da tabela PCMSO.MENSAGEM_PARAMETRO
	 * @param idParametro 
	 * @param conexao Conexao com o banco de dados
	 */
	public MensagemParametroNavegador( Conexao conexao, int idParametro )
	  throws QtSQLException { 

		super.setConexao( conexao ); 
		mensagemParametro = new MensagemParametro();
		this.mensagemParametro.setIdParametro( new Integer( idParametro ) );
		
		busca();
	}

	/** M�todo que retorna uma tabela b�sica a que se refere o navegador */
	public TabelaBasica getTabelaBasica() {

		return this.mensagemParametro;
	}

	/** M�todo que retorna a classe MensagemParametro relacionada ao navegador */
	public MensagemParametro getMensagemParametro() {

		return this.mensagemParametro;
	}

	 /** M�todo que move para o primeiro registro da tabela */
	 public boolean primeiro() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select min( ID_PARAMETRO ) from PCMSO.MENSAGEM_PARAMETRO" );
			query.addSQL( super.getCondicaoDeFiltro( FiltroDeNavegador.COM_WHERE ) 	);

			query.executeQuery();

			
			if( query.getString( 1 ) == null ) {

				super.setQueryStates( false, true, true );
			} else {
				this.mensagemParametro = new MensagemParametro();

				mensagemParametro.setIdParametro( query.getInteger( 1 ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o registro anterior na tabela */
	 public boolean anterior() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select max( ID_PARAMETRO ) from PCMSO.MENSAGEM_PARAMETRO" );
			query.addSQL( " where ID_PARAMETRO < :prm1IdParametro" );
			query.addSQL( super.getCondicaoDeFiltro() );

			query.setParameter( "prm1IdParametro", mensagemParametro.getIdParametro() );
			
			query.executeQuery();

			
			if( query.getString( 1 ) == null ) {

				super.setFound( false );
				super.setBof( true );
			} else {
				this.mensagemParametro = new MensagemParametro();

				mensagemParametro.setIdParametro( query.getInteger( 1 ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o proximo registro da tabela */
	 public boolean proximo() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select min( ID_PARAMETRO ) from PCMSO.MENSAGEM_PARAMETRO" );
			query.addSQL( " where ID_PARAMETRO > :prm1IdParametro" );
			query.addSQL( super.getCondicaoDeFiltro() );

			query.setParameter( "prm1IdParametro", mensagemParametro.getIdParametro() );

			query.executeQuery();

			
			if( query.getString( 1 ) == null ) {

				super.setFound( false );
				super.setEof( true );
			} else {
				this.mensagemParametro = new MensagemParametro();

				mensagemParametro.setIdParametro( query.getInteger( 1 ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o �ltimo registro da tabela */
	 public boolean ultimo() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select max( ID_PARAMETRO ) from PCMSO.MENSAGEM_PARAMETRO" );
			query.addSQL( super.getCondicaoDeFiltro( FiltroDeNavegador.COM_WHERE ) 	);

			query.executeQuery();

			
			if( query.getString( 1 ) == null ) {

				super.setQueryStates( false, true, true );
			} else {
				this.mensagemParametro = new MensagemParametro();

				mensagemParametro.setIdParametro( query.getInteger( 1 ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	/**
	 * M�todo para buscar e preencher uma tupla de uma tabela
	 * @param tabelaBasica Recebe uma tabela basica com os atributos chave preenchidos
	 */
	public boolean busca( TabelaBasica tabelaBasica ) throws QtSQLException {

		if( !( tabelaBasica instanceof MensagemParametro ) )
			throw new QtSQLException( "Esperava um objeto MensagemParametro!" );

		this.mensagemParametro = (MensagemParametro) tabelaBasica;
		return busca();

	}

	/**
	 * m�todo interno para buscar e preencher uma tupla de uma tabela
	 * Utiliza a Inst�ncia de MensagemParametro da classe para fazer a busca
	 */
	private boolean busca() throws QtSQLException {

		MensagemParametroLocalizador localizador = new MensagemParametroLocalizador( getConexao(), this.mensagemParametro );

		if( localizador.isEmpty() ) {

			super.setQueryStates( false, true, true );
			return false;

		} else {

			super.setQueryStates( true, false, false );
			return true;
		}
	}
}