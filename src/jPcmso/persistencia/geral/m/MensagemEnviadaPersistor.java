package jPcmso.persistencia.geral.m;

import java.sql.PreparedStatement;

import quatro.persistencia.Persistor;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.ConexaoParaPoolDeConexoes;
import quatro.sql.PoolDeConexoes;
import quatro.sql.QtSQLException;
import quatro.sql.Query;
import quatro.util.QtData;
import quatro.util.QtException;

/**
 *
 * Classe persistor para PCMSO.MENSAGENS_ENVIADAS
 *
 * @author Samuel Antonio Klein
 * @see Persistor
 * @see MensagemEnviada
 * @see MensagemEnviadaNavegador
 * @see MensagemEnviadaLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 public class MensagemEnviadaPersistor extends Persistor {

	/** Inst�ncia de MensagemEnviada para manipula��o pelo persistor */
	private MensagemEnviada mensagemEnviada;

	/** Construtor gen�rico */
	public MensagemEnviadaPersistor() {

		this.mensagemEnviada = null;
	}

	/** Construtor gen�rico */
	public MensagemEnviadaPersistor( Conexao cnx ) throws QtSQLException {

		this.mensagemEnviada = null;
		super.setConexao( cnx );
	}

	/** Construtor que recebe uma inst�ncia de MensagemEnviada */
	public MensagemEnviadaPersistor( MensagemEnviada mensagemEnviada ) throws QtSQLException {

		if( mensagemEnviada == null ) 
			throw new QtSQLException( "Impossivel instanciar MensagemEnviadaPersistor porque esta apontando para um nulo!" );

		this.mensagemEnviada = mensagemEnviada;
	}

	/** Construtor que recebe uma conex�o e uma inst�ncia de MensagemEnviada */
	public MensagemEnviadaPersistor( Conexao conexao, MensagemEnviada mensagemEnviada ) throws QtSQLException {
		System.out.println("instanciando 1");
		if( mensagemEnviada == null ) 
			throw new QtSQLException( "Impossivel instanciar MensagemEnviadaPersistor porque esta apontando para um nulo!" );
		System.out.println("instanciando 2");
		setConexao( conexao );
		System.out.println("instanciando 3");
		this.mensagemEnviada = mensagemEnviada;
		System.out.println("instanciando 4");
	}

	/**
	 * Construtor que recebe a chave da tabela PCMSO.MENSAGENS_ENVIADAS
	 * @param sqMensagem 
	 */
	public MensagemEnviadaPersistor( int sqMensagem )
	  throws QtSQLException { 

		busca( sqMensagem );
	}

	/**
	 * Construtor que recebe uma conex�o e a chave da tabela PCMSO.MENSAGENS_ENVIADAS
	 * @param sqMensagem 
	 * @param conexao Conexao com o banco de dados
	 */
	public MensagemEnviadaPersistor( Conexao conexao, int sqMensagem )
	  throws QtSQLException { 

		super.setConexao( conexao );
		busca( sqMensagem );
	}

	/**
	 * M�todo interno para buscar e preencher uma tupla de uma tabela
	 * Utiliza a Inst�ncia de MensagemEnviada para fazer a busca
	 * @param sqMensagem 
	 */
	private void busca( int sqMensagem ) throws QtSQLException { 

		MensagemEnviadaLocalizador mensagemEnviadaLocalizador = new MensagemEnviadaLocalizador( getConexao(), sqMensagem );

		if( mensagemEnviadaLocalizador.isEmpty() ) {
			throw new QtSQLException( "Chaves da Tabela MensagemEnviada nao encontradas - sqMensagem: " + sqMensagem );
		}

		mensagemEnviada = (MensagemEnviada ) mensagemEnviadaLocalizador.getTabelaBasica();
	}

	/**
	 * Permite alimentar o Persistor com um novo registro
	 */
	public void setTabelaBasica( TabelaBasica mensagemEnviada ) throws QtException {

		if( mensagemEnviada instanceof MensagemEnviada ) {
			this.mensagemEnviada = (MensagemEnviada) mensagemEnviada;
		} else {
			throw new QtException( "Tabela Basica fornecida tem que ser uma instancia de MensagemEnviada" );
		}
	}

	/**
	 * Permite acessar o registro corrente do persistor
	 */
	public TabelaBasica getTabelaBasica() throws QtException {

		return mensagemEnviada;
	}

	/**
	 * M�todo para gerar automaticamente uma nova chave
	 */
	public static Integer geraNovoSqMensagem( Conexao cnx ) throws QtSQLException {

		Integer result = null;

		Query query = new Query( cnx );

		try{

			query.setSQL( "select max( sq_mensagem ) + 1 from pcmso.mensagens_enviadas" );

			query.executeQuery();
			result = new Integer( query.getInt( 1 ) );
		} catch( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			query.liberaRecursos();
		}
		return result;
	}

	/**
	 * M�todo para gerar automaticamente uma nova chave
	 */
	public static Integer geraNovoSqMensagem(  ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return geraNovoSqMensagem(  cnx  );
		} finally {
			cnx.libera();
		}
	}


	/**
	 * M�todo para inserir tupla na tabela
	 */
	public void insere() throws QtSQLException {
		
		System.out.println("inserindo 1" );
		estabeleceConexao();
		System.out.println("inserindo 2" );
		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "insert into PCMSO.MENSAGENS_ENVIADAS" );
			cmd.append( "( SQ_MENSAGEM, DS_MENSAGEM, DT_MENSAGEM, DS_ERRO_EMAIL )" );
			cmd.append( "values " );
			cmd.append( "( ?, ?, ?, ?)" );
			System.out.println("inserindo 3 " + getConexao().isTransacaoEmAndamento() );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			ps.setObject( 1, mensagemEnviada.getSqMensagem() );
			ps.setObject( 2, mensagemEnviada.getDsMensagem() );
			ps.setObject( 3, QtData.getQtDataAsTimestamp( mensagemEnviada.getDtMensagem() ) );
			ps.setObject( 4, mensagemEnviada.getDsErroEmail() );
			System.out.println("inserindo 4" );
			
			System.out.println(ps.toString());
			ps.execute();
			System.out.println("inserindo 5" );

		} catch ( Exception e ) {
			e.printStackTrace();
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}

	/**
	 * M�todo para alterar tupla na tabela
	 */
	public void altera() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "update PCMSO.MENSAGENS_ENVIADAS" );
			cmd.append( "  set SQ_MENSAGEM = ?, " );
			cmd.append( " DS_MENSAGEM = ?, " );
			cmd.append( " DT_MENSAGEM = ?, " );
			cmd.append( " DS_ERRO_EMAIL = ?" );
			cmd.append( " where SQ_MENSAGEM = ?" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			ps.setObject( 1, mensagemEnviada.getSqMensagem() );
			ps.setObject( 2, mensagemEnviada.getDsMensagem() );
			ps.setObject( 3, QtData.getQtDataAsTimestamp( mensagemEnviada.getDtMensagem() ) );
			ps.setObject( 4, mensagemEnviada.getDsErroEmail() );
			
			ps.setObject( 5, mensagemEnviada.getSqMensagem() );
			
			ps.execute();

		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}

	/**
	 * M�todo para excluir tupla de uma tabela
	 */
	public void exclui() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "delete from PCMSO.MENSAGENS_ENVIADAS" );
			cmd.append( " where SQ_MENSAGEM = ?" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			ps.setObject( 1, mensagemEnviada.getSqMensagem() );
			
			ps.execute();

		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}
}