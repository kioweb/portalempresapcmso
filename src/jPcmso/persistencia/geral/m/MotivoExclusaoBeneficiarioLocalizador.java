package jPcmso.persistencia.geral.m;

import quatro.persistencia.Localizador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.QtSQLException;
import quatro.sql.Query;
import quatro.sql.Conexao;
import quatro.sql.PoolDeConexoes;
import quatro.sql.ConexaoParaPoolDeConexoes;

/**
 *
 * Classe localizador para MOTIVOS_EXCLUSAO_BENEFICIARIO
 *
 * @author Samuel Antonio Klein
 * @see Localizador
 * @see MotivoExclusaoBeneficiario
 * @see MotivoExclusaoBeneficiarioNavegador
 * @see MotivoExclusaoBeneficiarioPersistor
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 public class MotivoExclusaoBeneficiarioLocalizador extends Localizador {

	/** Inst�ncia de MotivoExclusaoBeneficiario para manipula��o pelo localizador */
	private MotivoExclusaoBeneficiario motivoExclusaoBeneficiario;

	/** Construtor que recebe uma conex�o e um objeto MotivoExclusaoBeneficiario completo */
	public MotivoExclusaoBeneficiarioLocalizador( Conexao conexao, MotivoExclusaoBeneficiario motivoExclusaoBeneficiario ) throws QtSQLException {

		super.setConexao( conexao );

		this.motivoExclusaoBeneficiario = motivoExclusaoBeneficiario;
		busca();
	}

	/** Construtor que recebe um objeto MotivoExclusaoBeneficiario completo */
	public MotivoExclusaoBeneficiarioLocalizador( MotivoExclusaoBeneficiario motivoExclusaoBeneficiario ) throws QtSQLException {

		this.motivoExclusaoBeneficiario = motivoExclusaoBeneficiario;
		busca();
	}

	/** 
	 * Construtor que recebe uma conex�o e a chave da tabela MOTIVOS_EXCLUSAO_BENEFICIARIO
	 * @param idMotivoExclusao Motivo de Exclus�o
	 * @param conexao Conexao com o banco de dados
	 */
	public MotivoExclusaoBeneficiarioLocalizador( Conexao conexao, int idMotivoExclusao ) throws QtSQLException { 

		super.setConexao( conexao ); 
		motivoExclusaoBeneficiario = new MotivoExclusaoBeneficiario();
		this.motivoExclusaoBeneficiario.setIdMotivoExclusao( new Integer( idMotivoExclusao ) );
		
		busca();
	}

	/** 
	 * Construtor que recebe a chave da tabela MOTIVOS_EXCLUSAO_BENEFICIARIO
	 * @param idMotivoExclusao Motivo de Exclus�o
	 */
	public MotivoExclusaoBeneficiarioLocalizador( int idMotivoExclusao ) throws QtSQLException { 

		motivoExclusaoBeneficiario = new MotivoExclusaoBeneficiario();
		this.motivoExclusaoBeneficiario.setIdMotivoExclusao( new Integer( idMotivoExclusao ) );
		
		busca();
	}

	public static boolean existe( Conexao cnx, int idMotivoExclusao ) throws QtSQLException {

		Query q = new Query( cnx );

		try {
			q.setSQL( "select count(*) from MOTIVOS_EXCLUSAO_BENEFICIARIO" );
			q.addSQL( " where ID_MOTIVO_EXCLUSAO = :prm1IdMotivoExclusao" );

			q.setParameter( "prm1IdMotivoExclusao", idMotivoExclusao );
			q.executeQuery();

			return q.getInt( 1 ) > 0;
		} finally {
			q.liberaRecursos();
		}
	}

	public static boolean existe( int idMotivoExclusao ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return existe( cnx, idMotivoExclusao );
		} finally {
			cnx.libera();
		}
	}

	public static boolean existe( Conexao cnx, MotivoExclusaoBeneficiario motivoExclusaoBeneficiario ) throws QtSQLException {

		return existe( cnx, motivoExclusaoBeneficiario.getIdMotivoExclusao() );
	}

	public static boolean existe( MotivoExclusaoBeneficiario motivoExclusaoBeneficiario ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return existe( cnx, motivoExclusaoBeneficiario.getIdMotivoExclusao() );
		} finally {
			cnx.libera();
		}
	}

	public static MotivoExclusaoBeneficiario buscaMotivoExclusaoBeneficiario( Conexao cnx, int idMotivoExclusao ) throws QtSQLException {

		Query q = new Query( cnx );

		try {
			q.setSQL( "select * from MOTIVOS_EXCLUSAO_BENEFICIARIO" );
			q.addSQL( " where ID_MOTIVO_EXCLUSAO = :prm1IdMotivoExclusao" );

			q.setParameter( "prm1IdMotivoExclusao", idMotivoExclusao );
			q.executeQuery();

			if( q.isEmpty() ) {
				return null;
			} else {
				MotivoExclusaoBeneficiario motivoExclusaoBeneficiario = new MotivoExclusaoBeneficiario();
				buscaCampos( motivoExclusaoBeneficiario, q );
				return motivoExclusaoBeneficiario;
			}
		} finally {
			q.liberaRecursos();
		}
	}

	public static MotivoExclusaoBeneficiario buscaMotivoExclusaoBeneficiario( int idMotivoExclusao ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return buscaMotivoExclusaoBeneficiario( cnx, idMotivoExclusao );
		} finally { 
			cnx.libera();
		}
	}

	/** 
	 * Construtor que recebe uma conex�o e a chave da tabela MOTIVOS_EXCLUSAO_BENEFICIARIO
	 * @param idMotivoExclusao Motivo de Exclus�o
	 * @param conexao Conexao com o banco de dados
	 */
	public MotivoExclusaoBeneficiarioLocalizador( Conexao conexao, Integer idMotivoExclusao ) throws QtSQLException {

		super.setConexao( conexao ); 
		motivoExclusaoBeneficiario = new MotivoExclusaoBeneficiario();
		this.motivoExclusaoBeneficiario.setIdMotivoExclusao( idMotivoExclusao );
		
		busca();
	}

	/** 
	 * Construtor que recebe a chave da tabela MOTIVOS_EXCLUSAO_BENEFICIARIO
	 * @param idMotivoExclusao Motivo de Exclus�o
	 */
	public MotivoExclusaoBeneficiarioLocalizador( Integer idMotivoExclusao ) throws QtSQLException {

		motivoExclusaoBeneficiario = new MotivoExclusaoBeneficiario();
		this.motivoExclusaoBeneficiario.setIdMotivoExclusao( idMotivoExclusao );
		
		busca();
	}

	public static void buscaCampos( MotivoExclusaoBeneficiario motivoExclusaoBeneficiario, Query query ) throws QtSQLException {

		motivoExclusaoBeneficiario.setIdMotivoExclusao( query.getInteger( "ID_MOTIVO_EXCLUSAO" ) );
		motivoExclusaoBeneficiario.setTpMotivoExclusao( query.getString( "TP_MOTIVO_EXCLUSAO" ) );
		motivoExclusaoBeneficiario.setDsMotivoExclusao( query.getString( "DS_MOTIVO_EXCLUSAO" ) );
	}

	/** M�todo que retorna uma tabela b�sica a que se refere o localizador */
	public TabelaBasica getTabelaBasica() {

		return this.motivoExclusaoBeneficiario;
	}

	/** M�todo que retorna a classe MotivoExclusaoBeneficiario relacionada ao localizador */
	public MotivoExclusaoBeneficiario getMotivoExclusaoBeneficiario() {

		return this.motivoExclusaoBeneficiario;
	}

	/**
	 * M�todo para buscar e preencher uma tupla de uma tabela
	 * @param tabelaBasica Recebe uma tabela basica com os atributos chave preenchidos
	 */
	public boolean busca( TabelaBasica tabelaBasica ) throws QtSQLException {

		if( !( tabelaBasica instanceof MotivoExclusaoBeneficiario ) )
			throw new QtSQLException( "Esperava um objeto MotivoExclusaoBeneficiario!" );

		this.motivoExclusaoBeneficiario = (MotivoExclusaoBeneficiario) tabelaBasica;
		return busca();

	}

	/**
	 * M�todo privado para buscar e preencher uma tupla de uma tabela
	 * Este m�todo busca os dados da inst�ncia de MotivoExclusaoBeneficiario atual
	 */
	private boolean busca() throws QtSQLException {

		boolean ret = false;

		super.estabeleceConexao();
		Query query = super.criaNovaQuery();

		try {

			query.setSQL( "select * from MOTIVOS_EXCLUSAO_BENEFICIARIO" );
			query.addSQL( " where ID_MOTIVO_EXCLUSAO = :prm1IdMotivoExclusao" );

			query.setParameter( "prm1IdMotivoExclusao", motivoExclusaoBeneficiario.getIdMotivoExclusao() );
			query.executeQuery();

			if( query.isEmpty() ) {
				super.setEmpty( true );
				motivoExclusaoBeneficiario = null;
			} else {
				super.setEmpty( false );
				
				motivoExclusaoBeneficiario.setIdMotivoExclusao( query.getInteger( "ID_MOTIVO_EXCLUSAO" ) );
				motivoExclusaoBeneficiario.setTpMotivoExclusao( query.getString( "TP_MOTIVO_EXCLUSAO" ) );
				motivoExclusaoBeneficiario.setDsMotivoExclusao( query.getString( "DS_MOTIVO_EXCLUSAO" ) );
				ret = true;
			}
		} finally {
			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}
 }