package jPcmso.persistencia.geral.m;

import quatro.persistencia.Localizador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.QtSQLException;
import quatro.sql.Query;
import quatro.sql.Conexao;
import quatro.sql.PoolDeConexoes;
import quatro.sql.ConexaoParaPoolDeConexoes;

/**
 *
 * Classe localizador para PCMSO.MENSAGEM_PARAMETRO
 *
 * @author Samuel Antonio Klein
 * @see Localizador
 * @see MensagemParametro
 * @see MensagemParametroNavegador
 * @see MensagemParametroPersistor
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 public class MensagemParametroLocalizador extends Localizador {

	/** Inst�ncia de MensagemParametro para manipula��o pelo localizador */
	private MensagemParametro mensagemParametro;

	/** Construtor que recebe uma conex�o e um objeto MensagemParametro completo */
	public MensagemParametroLocalizador( Conexao conexao, MensagemParametro mensagemParametro ) throws QtSQLException {

		super.setConexao( conexao );

		this.mensagemParametro = mensagemParametro;
		busca();
	}

	/** Construtor que recebe um objeto MensagemParametro completo */
	public MensagemParametroLocalizador( MensagemParametro mensagemParametro ) throws QtSQLException {

		this.mensagemParametro = mensagemParametro;
		busca();
	}

	/** 
	 * Construtor que recebe uma conex�o e a chave da tabela PCMSO.MENSAGEM_PARAMETRO
	 * @param idParametro 
	 * @param conexao Conexao com o banco de dados
	 */
	public MensagemParametroLocalizador( Conexao conexao, int idParametro ) throws QtSQLException { 

		super.setConexao( conexao ); 
		mensagemParametro = new MensagemParametro();
		this.mensagemParametro.setIdParametro( new Integer( idParametro ) );
		
		busca();
	}

	/** 
	 * Construtor que recebe a chave da tabela PCMSO.MENSAGEM_PARAMETRO
	 * @param idParametro 
	 */
	public MensagemParametroLocalizador( int idParametro ) throws QtSQLException { 

		mensagemParametro = new MensagemParametro();
		this.mensagemParametro.setIdParametro( new Integer( idParametro ) );
		
		busca();
	}

	public static boolean existe( Conexao cnx, int idParametro ) throws QtSQLException {

		Query q = new Query( cnx );

		try {
			q.setSQL( "select count(*) from PCMSO.MENSAGEM_PARAMETRO" );
			q.addSQL( " where ID_PARAMETRO = :prm1IdParametro" );

			q.setParameter( "prm1IdParametro", idParametro );
			q.executeQuery();

			return q.getInt( 1 ) > 0;
		} finally {
			q.liberaRecursos();
		}
	}

	public static boolean existe( int idParametro ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return existe( cnx, idParametro );
		} finally {
			cnx.libera();
		}
	}

	public static boolean existe( Conexao cnx, MensagemParametro mensagemParametro ) throws QtSQLException {

		return existe( cnx, mensagemParametro.getIdParametro() );
	}

	public static boolean existe( MensagemParametro mensagemParametro ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return existe( cnx, mensagemParametro.getIdParametro() );
		} finally {
			cnx.libera();
		}
	}

	public static MensagemParametro buscaMensagemParametro( Conexao cnx, int idParametro ) throws QtSQLException {

		Query q = new Query( cnx );

		try {
			q.setSQL( "select * from PCMSO.MENSAGEM_PARAMETRO" );
			q.addSQL( " where ID_PARAMETRO = :prm1IdParametro" );

			q.setParameter( "prm1IdParametro", idParametro );
			q.executeQuery();

			if( q.isEmpty() ) {
				return null;
			} else {
				MensagemParametro mensagemParametro = new MensagemParametro();
				buscaCampos( mensagemParametro, q );
				return mensagemParametro;
			}
		} finally {
			q.liberaRecursos();
		}
	}

	public static MensagemParametro buscaMensagemParametro( int idParametro ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return buscaMensagemParametro( cnx, idParametro );
		} finally { 
			cnx.libera();
		}
	}

	/** 
	 * Construtor que recebe uma conex�o e a chave da tabela PCMSO.MENSAGEM_PARAMETRO
	 * @param idParametro 
	 * @param conexao Conexao com o banco de dados
	 */
	public MensagemParametroLocalizador( Conexao conexao, Integer idParametro ) throws QtSQLException {

		super.setConexao( conexao ); 
		mensagemParametro = new MensagemParametro();
		this.mensagemParametro.setIdParametro( idParametro );
		
		busca();
	}

	/** 
	 * Construtor que recebe a chave da tabela PCMSO.MENSAGEM_PARAMETRO
	 * @param idParametro 
	 */
	public MensagemParametroLocalizador( Integer idParametro ) throws QtSQLException {

		mensagemParametro = new MensagemParametro();
		this.mensagemParametro.setIdParametro( idParametro );
		
		busca();
	}

	public static void buscaCampos( MensagemParametro mensagemParametro, Query query ) throws QtSQLException {

		mensagemParametro.setIdParametro( query.getInteger( "ID_PARAMETRO" ) );
		mensagemParametro.setDsDestinatarios( query.getString( "DS_DESTINATARIOS" ) );
	}

	/** M�todo que retorna uma tabela b�sica a que se refere o localizador */
	public TabelaBasica getTabelaBasica() {

		return this.mensagemParametro;
	}

	/** M�todo que retorna a classe MensagemParametro relacionada ao localizador */
	public MensagemParametro getMensagemParametro() {

		return this.mensagemParametro;
	}

	/**
	 * M�todo para buscar e preencher uma tupla de uma tabela
	 * @param tabelaBasica Recebe uma tabela basica com os atributos chave preenchidos
	 */
	public boolean busca( TabelaBasica tabelaBasica ) throws QtSQLException {

		if( !( tabelaBasica instanceof MensagemParametro ) )
			throw new QtSQLException( "Esperava um objeto MensagemParametro!" );

		this.mensagemParametro = (MensagemParametro) tabelaBasica;
		return busca();

	}

	/**
	 * M�todo privado para buscar e preencher uma tupla de uma tabela
	 * Este m�todo busca os dados da inst�ncia de MensagemParametro atual
	 */
	private boolean busca() throws QtSQLException {

		boolean ret = false;

		super.estabeleceConexao();
		Query query = super.criaNovaQuery();

		try {

			query.setSQL( "select * from PCMSO.MENSAGEM_PARAMETRO" );
			query.addSQL( " where ID_PARAMETRO = :prm1IdParametro" );

			query.setParameter( "prm1IdParametro", mensagemParametro.getIdParametro() );
			query.executeQuery();

			if( query.isEmpty() ) {
				super.setEmpty( true );
				mensagemParametro = null;
			} else {
				super.setEmpty( false );
				
				mensagemParametro.setIdParametro( query.getInteger( "ID_PARAMETRO" ) );
				mensagemParametro.setDsDestinatarios( query.getString( "DS_DESTINATARIOS" ) );
				ret = true;
			}
		} finally {
			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}
 }