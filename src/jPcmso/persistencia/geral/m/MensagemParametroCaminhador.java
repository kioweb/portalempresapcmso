package jPcmso.persistencia.geral.m;

import quatro.persistencia.Caminhador;
import quatro.persistencia.FiltroDeNavegador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;

 /**
 *
 * Classe Caminhador para MensagemParametro
 *
 * @author Samuel Antonio Klein
 * @see Caminhador
 * @see FiltroDeNavegador
 * @see MensagemParametroNavegador
 * @see MensagemParametroPersistor
 * @see MensagemParametroLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 public class MensagemParametroCaminhador extends Caminhador {

	public static final int POR_ORDEM_DEFAULT = -1;
	
	private MensagemParametro mensagemParametro;

	public MensagemParametroCaminhador( FiltroDeNavegador filtro ) {

		setFiltro( filtro );
		inicializaVisao();
	}

	public MensagemParametroCaminhador( Conexao conexao, FiltroDeNavegador filtro ) throws QtSQLException {

		super.setConexao( conexao );
		setFiltro( filtro );
		inicializaVisao();
	}

	private void inicializaVisao() {

		setNmVisao( "PCMSO.MENSAGEM_PARAMETRO" );
	}

	protected void carregaCampos() throws QtSQLException {

		mensagemParametro = new MensagemParametro();

		if( isRecordAvailable() ) {
			mensagemParametro.setIdParametro( query.getInteger( "ID_PARAMETRO" ) );
			mensagemParametro.setDsDestinatarios( query.getString( "DS_DESTINATARIOS" ) );
		}
	}

	/** M�todo que retorna uma tabela b�sica a que se refere o caminhador */
	public TabelaBasica getTabelaBasica() {

		return this.mensagemParametro;
	}

	/** M�todo que retorna a classe MensagemParametro relacionada ao caminhador */
	public MensagemParametro getMensagemParametro() {

		return this.mensagemParametro;
	}

	public void ativaVisao() throws QtSQLException {

		if(  super.getOrdemDeNavegacao() == MensagemParametroCaminhador.POR_ORDEM_DEFAULT ) {
			super.ativaCaminhador( "order by ID_PARAMETRO" );
	
		} else {
			throw new QtSQLException( "MensagemParametroCaminhador - Ordem de Navega��o n�o definida!" );
		}
	}

	public String getCamposIndice( int qualIndice ) {
		return getDsIndice( qualIndice );
	}

	public String getCamposChave() {
		return getDsChave();
	}

	/**
	 * M�todo est�tico que devolve os campos que comp�e a chave da tabela;
	 * @return campos que comp�e a chave.
	 */
	public static String getDsChave() {
		return "ID_PARAMETRO";
	}

	/**
	 * M�todo est�tico que devolve os campos que fazem parte de uma dada ordem
	 * de navega��o.
	 * @param qualIndice indica a ordem de navega��o desejada.
	 * @return campos que comp�e a ordem de navega��o.
	 */
	public static String getDsIndice( int qualIndice ) {

		switch( qualIndice ) {

			case POR_ORDEM_DEFAULT: return "ID_PARAMETRO";
		}
		return null;
	}
}