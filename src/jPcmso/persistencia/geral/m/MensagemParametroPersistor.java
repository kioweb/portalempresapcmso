package jPcmso.persistencia.geral.m;

import java.sql.PreparedStatement;

import quatro.persistencia.Persistor;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.ConexaoParaPoolDeConexoes;
import quatro.sql.PoolDeConexoes;
import quatro.sql.QtSQLException;
import quatro.sql.Query;
import quatro.util.QtException;

/**
 *
 * Classe persistor para PCMSO.MENSAGEM_PARAMETRO
 *
 * @author Samuel Antonio Klein
 * @see Persistor
 * @see MensagemParametro
 * @see MensagemParametroNavegador
 * @see MensagemParametroLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 public class MensagemParametroPersistor extends Persistor {

	/** Inst�ncia de MensagemParametro para manipula��o pelo persistor */
	private MensagemParametro mensagemParametro;

	/** Construtor gen�rico */
	public MensagemParametroPersistor() {

		this.mensagemParametro = null;
	}

	/** Construtor gen�rico */
	public MensagemParametroPersistor( Conexao cnx ) throws QtSQLException {

		this.mensagemParametro = null;
		super.setConexao( cnx );
	}

	/** Construtor que recebe uma inst�ncia de MensagemParametro */
	public MensagemParametroPersistor( MensagemParametro mensagemParametro ) throws QtSQLException {

		if( mensagemParametro == null ) 
			throw new QtSQLException( "Imposs�vel instanciar MensagemParametroPersistor porque est� apontando para um nulo!" );

		this.mensagemParametro = mensagemParametro;
	}

	/** Construtor que recebe uma conex�o e uma inst�ncia de MensagemParametro */
	public MensagemParametroPersistor( Conexao conexao, MensagemParametro mensagemParametro ) throws QtSQLException {

		if( mensagemParametro == null ) 
			throw new QtSQLException( "Imposs�vel instanciar MensagemParametroPersistor porque est� apontando para um nulo!" );

		setConexao( conexao );
		this.mensagemParametro = mensagemParametro;
	}

	/**
	 * Construtor que recebe a chave da tabela PCMSO.MENSAGEM_PARAMETRO
	 * @param idParametro 
	 */
	public MensagemParametroPersistor( int idParametro )
	  throws QtSQLException { 

		busca( idParametro );
	}

	/**
	 * Construtor que recebe uma conex�o e a chave da tabela PCMSO.MENSAGEM_PARAMETRO
	 * @param idParametro 
	 * @param conexao Conexao com o banco de dados
	 */
	public MensagemParametroPersistor( Conexao conexao, int idParametro )
	  throws QtSQLException { 

		super.setConexao( conexao );
		busca( idParametro );
	}

	/**
	 * M�todo interno para buscar e preencher uma tupla de uma tabela
	 * Utiliza a Inst�ncia de MensagemParametro para fazer a busca
	 * @param idParametro 
	 */
	private void busca( int idParametro ) throws QtSQLException { 

		MensagemParametroLocalizador mensagemParametroLocalizador = new MensagemParametroLocalizador( getConexao(), idParametro );

		if( mensagemParametroLocalizador.isEmpty() ) {
			throw new QtSQLException( "Chaves da Tabela MensagemParametro n�o encontradas - idParametro: " + idParametro );
		}

		mensagemParametro = (MensagemParametro ) mensagemParametroLocalizador.getTabelaBasica();
	}

	/**
	 * Permite alimentar o Persistor com um novo registro
	 */
	public void setTabelaBasica( TabelaBasica mensagemParametro ) throws QtException {

		if( mensagemParametro instanceof MensagemParametro ) {
			this.mensagemParametro = (MensagemParametro) mensagemParametro;
		} else {
			throw new QtException( "Tabela B�sica fornecida tem que ser uma inst�ncia de MensagemParametro" );
		}
	}

	/**
	 * Permite acessar o registro corrente do persistor
	 */
	public TabelaBasica getTabelaBasica() throws QtException {

		return mensagemParametro;
	}

	/**
	 * M�todo para gerar automaticamente uma nova chave
	 */
	public static Integer geraNovoIdParametro( Conexao cnx ) throws QtSQLException {

		Integer result = null;

		Query query = new Query( cnx );

		try{

			query.setSQL( "select max( ID_PARAMETRO ) from PCMSO.MENSAGEM_PARAMETRO" );

			query.executeQuery();
			result = new Integer( query.getInt( 1 ) + 1 );
		} catch( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			query.liberaRecursos();
		}
		return result;
	}

	/**
	 * M�todo para gerar automaticamente uma nova chave
	 */
	public static Integer geraNovoIdParametro(  ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return geraNovoIdParametro(  cnx  );
		} finally {
			cnx.libera();
		}
	}


	/**
	 * M�todo para inserir tupla na tabela
	 */
	public void insere() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "insert into PCMSO.MENSAGEM_PARAMETRO" );
			cmd.append( "( ID_PARAMETRO, DS_DESTINATARIOS )" );
			cmd.append( "values " );
			cmd.append( "( ?, ?)" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			ps.setObject( 1, mensagemParametro.getIdParametro() );
			ps.setObject( 2, mensagemParametro.getDsDestinatarios() );
			
			ps.execute();

		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}

	/**
	 * M�todo para alterar tupla na tabela
	 */
	public void altera() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "update PCMSO.MENSAGEM_PARAMETRO" );
			cmd.append( "  set ID_PARAMETRO = ?, " );
			cmd.append( " DS_DESTINATARIOS = ?" );
			cmd.append( " where ID_PARAMETRO = ?" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			ps.setObject( 1, mensagemParametro.getIdParametro() );
			ps.setObject( 2, mensagemParametro.getDsDestinatarios() );
			
			ps.setObject( 3, mensagemParametro.getIdParametro() );
			
			ps.execute();

		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}

	/**
	 * M�todo para excluir tupla de uma tabela
	 */
	public void exclui() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "delete from PCMSO.MENSAGEM_PARAMETRO" );
			cmd.append( " where ID_PARAMETRO = ?" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			ps.setObject( 1, mensagemParametro.getIdParametro() );
			
			ps.execute();

		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}
}