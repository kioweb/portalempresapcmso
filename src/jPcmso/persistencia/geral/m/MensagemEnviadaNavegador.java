package jPcmso.persistencia.geral.m;

import quatro.persistencia.FiltroDeNavegador;
import quatro.persistencia.Navegador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.sql.Query;
import quatro.util.QtData;

/**
 *
 * Classe navegador para PCMSO.MENSAGENS_ENVIADAS
 *
 * @author Samuel Antonio Klein
 * @see Navegador
 * @see MensagemEnviada
 * @see MensagemEnviadaPersistor
 * @see MensagemEnviadaLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 public class MensagemEnviadaNavegador extends Navegador {

	
	/** Inst�ncia de MensagemEnviada para manipula��o pelo navegador */
	private MensagemEnviada mensagemEnviada;

	/** Construtor b�sico */
	public MensagemEnviadaNavegador()
	  throws QtSQLException { 

		mensagemEnviada = new MensagemEnviada();
		primeiro();
	}

	/** Construtor b�sico que recebe uma conex�o */
	public MensagemEnviadaNavegador( Conexao conexao )
	  throws QtSQLException { 

		super.setConexao( conexao ); 
		mensagemEnviada = new MensagemEnviada();
		primeiro();
	}

	/**
	 * Construtor que recebe uma inst�ncia de MensagemEnviadaFiltro e posiciona
	 * no primeiro registro que satisfaz suas condi��es 
	 */
	public MensagemEnviadaNavegador( FiltroDeNavegador filtro ) throws QtSQLException  {
		super.setFiltro( filtro );
		this.mensagemEnviada = new MensagemEnviada();
		primeiro();
	}

	/** Construtor que recebe a chave da tabela PCMSO.MENSAGENS_ENVIADAS
	 * @param sqMensagem 
	 */
	public MensagemEnviadaNavegador( int sqMensagem )
	  throws QtSQLException { 

		mensagemEnviada = new MensagemEnviada();
		this.mensagemEnviada.setSqMensagem( new Integer( sqMensagem ) );
		
		busca();
	}

	/** Construtor que recebe  uma conex�o e a chave da tabela PCMSO.MENSAGENS_ENVIADAS
	 * @param sqMensagem 
	 * @param conexao Conexao com o banco de dados
	 */
	public MensagemEnviadaNavegador( Conexao conexao, int sqMensagem )
	  throws QtSQLException { 

		super.setConexao( conexao ); 
		mensagemEnviada = new MensagemEnviada();
		this.mensagemEnviada.setSqMensagem( new Integer( sqMensagem ) );
		
		busca();
	}

	/** M�todo que retorna uma tabela b�sica a que se refere o navegador */
	public TabelaBasica getTabelaBasica() {

		return this.mensagemEnviada;
	}

	/** M�todo que retorna a classe MensagemEnviada relacionada ao navegador */
	public MensagemEnviada getMensagemEnviada() {

		return this.mensagemEnviada;
	}

	 /** M�todo que move para o primeiro registro da tabela */
	 public boolean primeiro() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select min( SQ_MENSAGEM ) from PCMSO.MENSAGENS_ENVIADAS" );
			query.addSQL( super.getCondicaoDeFiltro( FiltroDeNavegador.COM_WHERE ) 	);

			query.executeQuery();

			
			if( query.getString( 1 ) == null ) {

				super.setQueryStates( false, true, true );
			} else {
				this.mensagemEnviada = new MensagemEnviada();

				mensagemEnviada.setSqMensagem( query.getInteger( 1 ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o registro anterior na tabela */
	 public boolean anterior() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select max( SQ_MENSAGEM ) from PCMSO.MENSAGENS_ENVIADAS" );
			query.addSQL( " where SQ_MENSAGEM < :prm1SqMensagem" );
			query.addSQL( super.getCondicaoDeFiltro() );

			query.setParameter( "prm1SqMensagem", mensagemEnviada.getSqMensagem() );
			
			query.executeQuery();

			
			if( query.getString( 1 ) == null ) {

				super.setFound( false );
				super.setBof( true );
			} else {
				this.mensagemEnviada = new MensagemEnviada();

				mensagemEnviada.setSqMensagem( query.getInteger( 1 ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o proximo registro da tabela */
	 public boolean proximo() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select min( SQ_MENSAGEM ) from PCMSO.MENSAGENS_ENVIADAS" );
			query.addSQL( " where SQ_MENSAGEM > :prm1SqMensagem" );
			query.addSQL( super.getCondicaoDeFiltro() );

			query.setParameter( "prm1SqMensagem", mensagemEnviada.getSqMensagem() );

			query.executeQuery();

			
			if( query.getString( 1 ) == null ) {

				super.setFound( false );
				super.setEof( true );
			} else {
				this.mensagemEnviada = new MensagemEnviada();

				mensagemEnviada.setSqMensagem( query.getInteger( 1 ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o �ltimo registro da tabela */
	 public boolean ultimo() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select max( SQ_MENSAGEM ) from PCMSO.MENSAGENS_ENVIADAS" );
			query.addSQL( super.getCondicaoDeFiltro( FiltroDeNavegador.COM_WHERE ) 	);

			query.executeQuery();

			
			if( query.getString( 1 ) == null ) {

				super.setQueryStates( false, true, true );
			} else {
				this.mensagemEnviada = new MensagemEnviada();

				mensagemEnviada.setSqMensagem( query.getInteger( 1 ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	/**
	 * M�todo para buscar e preencher uma tupla de uma tabela
	 * @param tabelaBasica Recebe uma tabela basica com os atributos chave preenchidos
	 */
	public boolean busca( TabelaBasica tabelaBasica ) throws QtSQLException {

		if( !( tabelaBasica instanceof MensagemEnviada ) )
			throw new QtSQLException( "Esperava um objeto MensagemEnviada!" );

		this.mensagemEnviada = (MensagemEnviada) tabelaBasica;
		return busca();

	}

	/**
	 * m�todo interno para buscar e preencher uma tupla de uma tabela
	 * Utiliza a Inst�ncia de MensagemEnviada da classe para fazer a busca
	 */
	private boolean busca() throws QtSQLException {

		MensagemEnviadaLocalizador localizador = new MensagemEnviadaLocalizador( getConexao(), this.mensagemEnviada );

		if( localizador.isEmpty() ) {

			super.setQueryStates( false, true, true );
			return false;

		} else {

			super.setQueryStates( true, false, false );
			return true;
		}
	}
}