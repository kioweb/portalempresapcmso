package jPcmso.persistencia.geral.m;

import quatro.persistencia.TabelaBasica;
import quatro.smartDB.IgnoredField;
import quatro.smartDB.PrimaryKey;
import quatro.smartDB.Table;

/**
 *
 * Classe b�sica para PCMSO.MENSAGEM_PARAMETRO
 *
 * Tabela de Beneficiarios CBO
 * 
 * @author Samuel Antonio Klein
 * @see MensagemParametroNavegador
 * @see MensagemParametroPersistor
 * @see MensagemParametroLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 @Table(name="PCMSO.MENSAGEM_PARAMETRO")
 public class MensagemParametro implements TabelaBasica {

	@IgnoredField
	private static final long serialVersionUID = -212432672146904679L;

	/**  (chave) */
	@PrimaryKey
	private Integer idParametro;
	/**  */
	private String dsDestinatarios;

	/**
	 * Alimenta 
	 * @param idParametro 
	 */ 
	public void setIdParametro( Integer idParametro ) {
		this.idParametro = idParametro;
	}

	/**
	 * Retorna 
	 * @return 
	 */
	public Integer getIdParametro() {
		return this.idParametro;
	}

	/**
	 * Alimenta 
	 * @param dsDestinatarios 
	 */ 
	public void setDsDestinatarios( String dsDestinatarios ) {
		this.dsDestinatarios = dsDestinatarios;
	}

	/**
	 * Retorna 
	 * @return 
	 */
	public String getDsDestinatarios() {
		return this.dsDestinatarios;
	}

}