package jPcmso.persistencia.geral.m;

import quatro.persistencia.Localizador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.QtSQLException;
import quatro.sql.Query;
import quatro.sql.Conexao;
import quatro.sql.PoolDeConexoes;
import quatro.sql.ConexaoParaPoolDeConexoes;

/**
 *
 * Classe localizador para PCMSO.MEDICOS
 *
 * @author Samuel Antonio Klein
 * @see Localizador
 * @see Medico
 * @see MedicoNavegador
 * @see MedicoPersistor
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 public class MedicoLocalizador extends Localizador {

	/** Inst�ncia de Medico para manipula��o pelo localizador */
	private Medico medico;

	/** Construtor que recebe uma conex�o e um objeto Medico completo */
	public MedicoLocalizador( Conexao conexao, Medico medico ) throws QtSQLException {

		super.setConexao( conexao );

		this.medico = medico;
		busca();
	}

	/** Construtor que recebe um objeto Medico completo */
	public MedicoLocalizador( Medico medico ) throws QtSQLException {

		this.medico = medico;
		busca();
	}

	/** 
	 * Construtor que recebe uma conex�o e a chave da tabela PCMSO.MEDICOS
	 * @param cdMedico Codigo do medico
	 * @param conexao Conexao com o banco de dados
	 */
	public MedicoLocalizador( Conexao conexao, String cdMedico ) throws QtSQLException { 

		super.setConexao( conexao ); 
		medico = new Medico();
		this.medico.setCdMedico( cdMedico );
		
		busca();
	}

	/** 
	 * Construtor que recebe a chave da tabela PCMSO.MEDICOS
	 * @param cdMedico Codigo do medico
	 */
	public MedicoLocalizador( String cdMedico ) throws QtSQLException { 

		medico = new Medico();
		this.medico.setCdMedico( cdMedico );
		
		busca();
	}

	public static boolean existe( Conexao cnx, String cdMedico ) throws QtSQLException {

		Query q = new Query( cnx );

		try {
			q.setSQL( "select count(*) from PCMSO.MEDICOS" );
			q.addSQL( " where CD_MEDICO = :prm1CdMedico" );

			q.setParameter( "prm1CdMedico", cdMedico );
			q.executeQuery();

			return q.getInt( 1 ) > 0;
		} finally {
			q.liberaRecursos();
		}
	}

	public static boolean existe( String cdMedico ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return existe( cnx, cdMedico );
		} finally {
			cnx.libera();
		}
	}

	public static boolean existe( Conexao cnx, Medico medico ) throws QtSQLException {

		return existe( cnx, medico.getCdMedico() );
	}

	public static boolean existe( Medico medico ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return existe( cnx, medico.getCdMedico() );
		} finally {
			cnx.libera();
		}
	}

	public static Medico buscaMedico( Conexao cnx, String cdMedico ) throws QtSQLException {

		Query q = new Query( cnx );

		try {
			q.setSQL( "select * from PCMSO.MEDICOS" );
			q.addSQL( " where CD_MEDICO = :prm1CdMedico" );

			q.setParameter( "prm1CdMedico", cdMedico );
			q.executeQuery();

			if( q.isEmpty() ) {
				return null;
			} else {
				Medico medico = new Medico();
				buscaCampos( medico, q );
				return medico;
			}
		} finally {
			q.liberaRecursos();
		}
	}

	public static Medico buscaMedico( String cdMedico ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return buscaMedico( cnx, cdMedico );
		} finally { 
			cnx.libera();
		}
	}

	public static void buscaCampos( Medico medico, Query query ) throws QtSQLException {

		medico.setCdMedico( query.getString( "CD_MEDICO" ) );
		medico.setCdSigla( query.getString( "CD_SIGLA" ) );
		medico.setUfCrm( query.getString( "UF_CRM" ) );
		medico.setNrCrm( query.getInteger( "NR_CRM" ) );
		medico.setNmMedico( query.getString( "NM_MEDICO" ) );
		medico.setSnCoordenador( query.getString( "SN_COORDENADOR" ) );
		medico.setNrDdd( query.getString( "NR_DDD" ) );
		medico.setNrTelefone( query.getString( "NR_TELEFONE" ) );
		medico.setDsLogradouro( query.getString( "DS_LOGRADOURO" ) );
		medico.setDsBairro( query.getString( "DS_BAIRRO" ) );
		medico.setDsCidade( query.getString( "DS_CIDADE" ) );
		medico.setCdEstado( query.getString( "CD_ESTADO" ) );
	}

	/** M�todo que retorna uma tabela b�sica a que se refere o localizador */
	public TabelaBasica getTabelaBasica() {

		return this.medico;
	}

	/** M�todo que retorna a classe Medico relacionada ao localizador */
	public Medico getMedico() {

		return this.medico;
	}

	/**
	 * M�todo para buscar e preencher uma tupla de uma tabela
	 * @param tabelaBasica Recebe uma tabela basica com os atributos chave preenchidos
	 */
	public boolean busca( TabelaBasica tabelaBasica ) throws QtSQLException {

		if( !( tabelaBasica instanceof Medico ) )
			throw new QtSQLException( "Esperava um objeto Medico!" );

		this.medico = (Medico) tabelaBasica;
		return busca();

	}

	/**
	 * M�todo privado para buscar e preencher uma tupla de uma tabela
	 * Este m�todo busca os dados da inst�ncia de Medico atual
	 */
	private boolean busca() throws QtSQLException {

		boolean ret = false;

		super.estabeleceConexao();
		Query query = super.criaNovaQuery();

		try {

			query.setSQL( "select * from PCMSO.MEDICOS" );
			query.addSQL( " where CD_MEDICO like :prm1CdMedico" );

			query.setParameter( "prm1CdMedico", medico.getCdMedico() );
			query.executeQuery();

			if( query.isEmpty() ) {
				super.setEmpty( true );
				medico = null;
			} else {
				super.setEmpty( false );
				
				medico.setCdMedico( query.getString( "CD_MEDICO" ) );
				medico.setCdSigla( query.getString( "CD_SIGLA" ) );
				medico.setUfCrm( query.getString( "UF_CRM" ) );
				medico.setNrCrm( query.getInteger( "NR_CRM" ) );
				medico.setNmMedico( query.getString( "NM_MEDICO" ) );
				medico.setSnCoordenador( query.getString( "SN_COORDENADOR" ) );
				medico.setNrDdd( query.getString( "NR_DDD" ) );
				medico.setNrTelefone( query.getString( "NR_TELEFONE" ) );
				medico.setDsLogradouro( query.getString( "DS_LOGRADOURO" ) );
				medico.setDsBairro( query.getString( "DS_BAIRRO" ) );
				medico.setDsCidade( query.getString( "DS_CIDADE" ) );
				medico.setCdEstado( query.getString( "CD_ESTADO" ) );
				ret = true;
			}
		} finally {
			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}
 }