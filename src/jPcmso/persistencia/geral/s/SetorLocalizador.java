package jPcmso.persistencia.geral.s;

import quatro.persistencia.Localizador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.QtSQLException;
import quatro.sql.Query;
import quatro.sql.Conexao;
import quatro.sql.PoolDeConexoes;
import quatro.sql.ConexaoParaPoolDeConexoes;

/**
 *
 * Classe localizador para PCMSO.SETORES
 *
 * @author ronei@unimed-uau.com.br
 * @see Localizador
 * @see Setor
 * @see SetorNavegador
 * @see SetorPersistor
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 14/12/2017 15:26 - importado por ronei@unimed-uau.com.br
 *
 */
 public class SetorLocalizador extends Localizador {

	/** Instância de Setor para manipulação pelo localizador */
	private Setor setor;

	/** Construtor que recebe uma conexão e um objeto Setor completo */
	public SetorLocalizador( Conexao conexao, Setor setor ) throws QtSQLException {

		super.setConexao( conexao );

		this.setor = setor;
		busca();
	}

	/** Construtor que recebe um objeto Setor completo */
	public SetorLocalizador( Setor setor ) throws QtSQLException {

		this.setor = setor;
		busca();
	}

	/** 
	 * Construtor que recebe uma conexão e a chave da tabela PCMSO.SETORES
	 * @param idSetor Codigo do setor
	 * @param conexao Conexao com o banco de dados
	 */
	public SetorLocalizador( Conexao conexao, int idSetor ) throws QtSQLException { 

		super.setConexao( conexao ); 
		setor = new Setor();
		this.setor.setIdSetor( new Integer( idSetor ) );
		
		busca();
	}

	/** 
	 * Construtor que recebe a chave da tabela PCMSO.SETORES
	 * @param idSetor Codigo do setor
	 */
	public SetorLocalizador( int idSetor ) throws QtSQLException { 

		setor = new Setor();
		this.setor.setIdSetor( new Integer( idSetor ) );
		
		busca();
	}

	public static boolean existe( Conexao cnx, int idSetor ) throws QtSQLException {

		Query q = new Query( cnx );

		try {
			q.setSQL( "select count(*) from PCMSO.SETORES" );
			q.addSQL( " where ID_SETOR = :prm1IdSetor" );

			q.setParameter( "prm1IdSetor", idSetor );
			q.executeQuery();

			return q.getInt( 1 ) > 0;
		} finally {
			q.liberaRecursos();
		}
	}

	public static boolean existe( int idSetor ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return existe( cnx, idSetor );
		} finally {
			cnx.libera();
		}
	}

	public static boolean existe( Conexao cnx, Setor setor ) throws QtSQLException {

		return existe( cnx, setor.getIdSetor() );
	}

	public static boolean existe( Setor setor ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return existe( cnx, setor.getIdSetor() );
		} finally {
			cnx.libera();
		}
	}

	public static Setor buscaSetor( Conexao cnx, int idSetor ) throws QtSQLException {

		Query q = new Query( cnx );

		try {
			q.setSQL( "select * from PCMSO.SETORES" );
			q.addSQL( " where ID_SETOR = :prm1IdSetor" );

			q.setParameter( "prm1IdSetor", idSetor );
			q.executeQuery();

			if( q.isEmpty() ) {
				return null;
			} else {
				Setor setor = new Setor();
				buscaCampos( setor, q );
				return setor;
			}
		} finally {
			q.liberaRecursos();
		}
	}

	public static Setor buscaSetor( int idSetor ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return buscaSetor( cnx, idSetor );
		} finally { 
			cnx.libera();
		}
	}

	/** 
	 * Construtor que recebe uma conexão e a chave da tabela PCMSO.SETORES
	 * @param idSetor Codigo do setor
	 * @param conexao Conexao com o banco de dados
	 */
	public SetorLocalizador( Conexao conexao, Integer idSetor ) throws QtSQLException {

		super.setConexao( conexao ); 
		setor = new Setor();
		this.setor.setIdSetor( idSetor );
		
		busca();
	}

	/** 
	 * Construtor que recebe a chave da tabela PCMSO.SETORES
	 * @param idSetor Codigo do setor
	 */
	public SetorLocalizador( Integer idSetor ) throws QtSQLException {

		setor = new Setor();
		this.setor.setIdSetor( idSetor );
		
		busca();
	}

	public static void buscaCampos( Setor setor, Query query ) throws QtSQLException {

		setor.setIdSetor( query.getInteger( "ID_SETOR" ) );
		setor.setDsSetor( query.getString( "DS_SETOR" ) );
	}

	/** Método que retorna uma tabela básica a que se refere o localizador */
	public TabelaBasica getTabelaBasica() {

		return this.setor;
	}

	/** Método que retorna a classe Setor relacionada ao localizador */
	public Setor getSetor() {

		return this.setor;
	}

	/**
	 * Método para buscar e preencher uma tupla de uma tabela
	 * @param tabelaBasica Recebe uma tabela basica com os atributos chave preenchidos
	 */
	public boolean busca( TabelaBasica tabelaBasica ) throws QtSQLException {

		if( !( tabelaBasica instanceof Setor ) )
			throw new QtSQLException( "Esperava um objeto Setor!" );

		this.setor = (Setor) tabelaBasica;
		return busca();

	}

	/**
	 * Método privado para buscar e preencher uma tupla de uma tabela
	 * Este método busca os dados da instância de Setor atual
	 */
	private boolean busca() throws QtSQLException {

		boolean ret = false;

		super.estabeleceConexao();
		Query query = super.criaNovaQuery();

		try {

			query.setSQL( "select * from PCMSO.SETORES" );
			query.addSQL( " where ID_SETOR = :prm1IdSetor" );

			query.setParameter( "prm1IdSetor", setor.getIdSetor() );
			query.executeQuery();

			if( query.isEmpty() ) {
				super.setEmpty( true );
				setor = null;
			} else {
				super.setEmpty( false );
				
				setor.setIdSetor( query.getInteger( "ID_SETOR" ) );
				setor.setDsSetor( query.getString( "DS_SETOR" ) );
				ret = true;
			}
		} finally {
			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}
 }