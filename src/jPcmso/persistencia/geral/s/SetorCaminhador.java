package jPcmso.persistencia.geral.s;

import quatro.persistencia.Caminhador;
import quatro.persistencia.FiltroDeNavegador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;

 /**
 *
 * Classe Caminhador para Setor
 *
 * @author ronei@unimed-uau.com.br
 * @see Caminhador
 * @see FiltroDeNavegador
 * @see SetorNavegador
 * @see SetorPersistor
 * @see SetorLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 14/12/2017 15:26 - importado por ronei@unimed-uau.com.br
 *
 */
 public class SetorCaminhador extends Caminhador {

	public static final int POR_ORDEM_DEFAULT = -1;
	public static final int POR_DS_SETOR = 0;
	
	private Setor setor;

	public SetorCaminhador( FiltroDeNavegador filtro ) {

		setFiltro( filtro );
		inicializaVisao();
	}

	public SetorCaminhador( Conexao conexao, FiltroDeNavegador filtro ) throws QtSQLException {

		super.setConexao( conexao );
		setFiltro( filtro );
		inicializaVisao();
	}

	private void inicializaVisao() {

		setNmVisao( "PCMSO.SETORES" );
	}

	protected void carregaCampos() throws QtSQLException {

		setor = new Setor();

		if( isRecordAvailable() ) {
			setor.setIdSetor( query.getInteger( "ID_SETOR" ) );
			setor.setDsSetor( query.getString( "DS_SETOR" ) );
		}
	}

	/** Método que retorna uma tabela básica a que se refere o caminhador */
	public TabelaBasica getTabelaBasica() {

		return this.setor;
	}

	/** Método que retorna a classe Setor relacionada ao caminhador */
	public Setor getSetor() {

		return this.setor;
	}

	public void ativaVisao() throws QtSQLException {

		if(  super.getOrdemDeNavegacao() == SetorCaminhador.POR_ORDEM_DEFAULT ) {
			super.ativaCaminhador( "order by ID_SETOR" );
		} else if(  super.getOrdemDeNavegacao() == SetorCaminhador.POR_DS_SETOR ) {
			super.ativaCaminhador( "order by DS_SETOR" );
	
		} else {
			throw new QtSQLException( "SetorCaminhador - Ordem de Navegação não definida!" );
		}
	}

	public String getCamposIndice( int qualIndice ) {
		return getDsIndice( qualIndice );
	}

	public String getCamposChave() {
		return getDsChave();
	}

	/**
	 * Método estático que devolve os campos que compõe a chave da tabela;
	 * @return campos que compõe a chave.
	 */
	public static String getDsChave() {
		return "ID_SETOR";
	}

	/**
	 * Método estático que devolve os campos que fazem parte de uma dada ordem
	 * de navegação.
	 * @param qualIndice indica a ordem de navegação desejada.
	 * @return campos que compõe a ordem de navegação.
	 */
	public static String getDsIndice( int qualIndice ) {

		switch( qualIndice ) {

			case POR_ORDEM_DEFAULT: return "ID_SETOR";
			case POR_DS_SETOR: return "DS_SETOR";
		}
		return null;
	}
}