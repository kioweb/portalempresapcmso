package jPcmso.persistencia.geral.s;

import quatro.persistencia.FiltroDeNavegador;
import quatro.persistencia.Navegador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.sql.Query;

/**
 *
 * Classe navegador para PCMSO.SETORES
 *
 * @author ronei@unimed-uau.com.br
 * @see Navegador
 * @see Setor
 * @see SetorPersistor
 * @see SetorLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 14/12/2017 15:26 - importado por ronei@unimed-uau.com.br
 *
 */
 public class SetorNavegador extends Navegador {

	
	/** Instância de Setor para manipulação pelo navegador */
	private Setor setor;

	/** Construtor básico */
	public SetorNavegador()
	  throws QtSQLException { 

		setor = new Setor();
		primeiro();
	}

	/** Construtor básico que recebe uma conexão */
	public SetorNavegador( Conexao conexao )
	  throws QtSQLException { 

		super.setConexao( conexao ); 
		setor = new Setor();
		primeiro();
	}

	/**
	 * Construtor que recebe uma instância de SetorFiltro e posiciona
	 * no primeiro registro que satisfaz suas condições 
	 */
	public SetorNavegador( FiltroDeNavegador filtro ) throws QtSQLException  {
		super.setFiltro( filtro );
		this.setor = new Setor();
		primeiro();
	}

	/** Construtor que recebe a chave da tabela PCMSO.SETORES
	 * @param idSetor Codigo do setor
	 */
	public SetorNavegador( int idSetor )
	  throws QtSQLException { 

		setor = new Setor();
		this.setor.setIdSetor( new Integer( idSetor ) );
		
		busca();
	}

	/** Construtor que recebe  uma conexão e a chave da tabela PCMSO.SETORES
	 * @param idSetor Codigo do setor
	 * @param conexao Conexao com o banco de dados
	 */
	public SetorNavegador( Conexao conexao, int idSetor )
	  throws QtSQLException { 

		super.setConexao( conexao ); 
		setor = new Setor();
		this.setor.setIdSetor( new Integer( idSetor ) );
		
		busca();
	}

	/** Método que retorna uma tabela básica a que se refere o navegador */
	public TabelaBasica getTabelaBasica() {

		return this.setor;
	}

	/** Método que retorna a classe Setor relacionada ao navegador */
	public Setor getSetor() {

		return this.setor;
	}

	 /** Método que move para o primeiro registro da tabela */
	 public boolean primeiro() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select min( ID_SETOR ) from PCMSO.SETORES" );
			query.addSQL( super.getCondicaoDeFiltro( FiltroDeNavegador.COM_WHERE ) 	);

			query.executeQuery();

			
			if( query.getString( 1 ) == null ) {

				super.setQueryStates( false, true, true );
			} else {
				this.setor = new Setor();

				setor.setIdSetor( query.getInteger( 1 ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** Método que move para o registro anterior na tabela */
	 public boolean anterior() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select max( ID_SETOR ) from PCMSO.SETORES" );
			query.addSQL( " where ID_SETOR < :prm1IdSetor" );
			query.addSQL( super.getCondicaoDeFiltro() );

			query.setParameter( "prm1IdSetor", setor.getIdSetor() );
			
			query.executeQuery();

			
			if( query.getString( 1 ) == null ) {

				super.setFound( false );
				super.setBof( true );
			} else {
				this.setor = new Setor();

				setor.setIdSetor( query.getInteger( 1 ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** Método que move para o proximo registro da tabela */
	 public boolean proximo() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select min( ID_SETOR ) from PCMSO.SETORES" );
			query.addSQL( " where ID_SETOR > :prm1IdSetor" );
			query.addSQL( super.getCondicaoDeFiltro() );

			query.setParameter( "prm1IdSetor", setor.getIdSetor() );

			query.executeQuery();

			
			if( query.getString( 1 ) == null ) {

				super.setFound( false );
				super.setEof( true );
			} else {
				this.setor = new Setor();

				setor.setIdSetor( query.getInteger( 1 ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** Método que move para o último registro da tabela */
	 public boolean ultimo() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select max( ID_SETOR ) from PCMSO.SETORES" );
			query.addSQL( super.getCondicaoDeFiltro( FiltroDeNavegador.COM_WHERE ) 	);

			query.executeQuery();

			
			if( query.getString( 1 ) == null ) {

				super.setQueryStates( false, true, true );
			} else {
				this.setor = new Setor();

				setor.setIdSetor( query.getInteger( 1 ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	/**
	 * Método para buscar e preencher uma tupla de uma tabela
	 * @param tabelaBasica Recebe uma tabela basica com os atributos chave preenchidos
	 */
	public boolean busca( TabelaBasica tabelaBasica ) throws QtSQLException {

		if( !( tabelaBasica instanceof Setor ) )
			throw new QtSQLException( "Esperava um objeto Setor!" );

		this.setor = (Setor) tabelaBasica;
		return busca();

	}

	/**
	 * método interno para buscar e preencher uma tupla de uma tabela
	 * Utiliza a Instância de Setor da classe para fazer a busca
	 */
	private boolean busca() throws QtSQLException {

		SetorLocalizador localizador = new SetorLocalizador( getConexao(), this.setor );

		if( localizador.isEmpty() ) {

			super.setQueryStates( false, true, true );
			return false;

		} else {

			super.setQueryStates( true, false, false );
			return true;
		}
	}
}