package jPcmso.persistencia.geral.s;

import quatro.persistencia.TabelaBasica;
import quatro.smartDB.IgnoredField;
import quatro.smartDB.PrimaryKey;
import quatro.smartDB.Table;

/**
 *
 * Classe básica para PCMSO.SETORES
 *
 * Tabela de Setor
 * 
 * @author ronei@unimed-uau.com.br
 * @see SetorNavegador
 * @see SetorPersistor
 * @see SetorLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 14/12/2017 15:26 - importado por ronei@unimed-uau.com.br
 *
 */
 @Table(name="PCMSO.SETORES")
 public class Setor implements TabelaBasica {

	@IgnoredField
	private static final long serialVersionUID = -1619526290483114487L;

	/** Codigo do setor (chave) */
	@PrimaryKey
	private Integer idSetor;
	/** Descricao do setor (varchar(250)) */
	private String dsSetor;

	/**
	 * Alimenta Codigo do setor
	 * @param idSetor Codigo do setor
	 */ 
	public void setIdSetor( Integer idSetor ) {
		this.idSetor = idSetor;
	}

	/**
	 * Retorna Codigo do setor
	 * @return Codigo do setor
	 */
	public Integer getIdSetor() {
		return this.idSetor;
	}

	/**
	 * Alimenta Descricao do setor
	 * @param dsSetor Descricao do setor
	 */ 
	public void setDsSetor( String dsSetor ) {
		this.dsSetor = dsSetor;
	}

	/**
	 * Retorna Descricao do setor
	 * @return Descricao do setor
	 */
	public String getDsSetor() {
		return this.dsSetor;
	}

}