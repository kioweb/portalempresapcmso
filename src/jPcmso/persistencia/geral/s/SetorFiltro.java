package jPcmso.persistencia.geral.s;

import quatro.persistencia.FiltroDeNavegador;

/**
 *
 * Classe filtro para PCMSO.SETORES
 *
 * @author ronei@unimed-uau.com.br
 * @see FiltroDeTabela
 * @see Setor
 * @see SetorNavegador
 * @see SetorPersistor
 * @see SetorLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 14/12/2017 15:26 - importado por ronei@unimed-uau.com.br
 *
 */
 public class SetorFiltro extends FiltroDeNavegador {

	/** Determina a ordem de navegação que será passada para o navegador */
	public void setOrdemDeNavegacao( int idOrdem ) {

		super.setOrdemDeNavegacao( idOrdem );
	}
}