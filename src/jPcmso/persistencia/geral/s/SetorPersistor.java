package jPcmso.persistencia.geral.s;

import java.sql.PreparedStatement;

import quatro.persistencia.Persistor;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.ConexaoParaPoolDeConexoes;
import quatro.sql.PoolDeConexoes;
import quatro.sql.QtSQLException;
import quatro.sql.Query;
import quatro.util.QtException;

/**
 *
 * Classe persistor para PCMSO.SETORES
 *
 * @author ronei@unimed-uau.com.br
 * @see Persistor
 * @see Setor
 * @see SetorNavegador
 * @see SetorLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 14/12/2017 15:26 - importado por ronei@unimed-uau.com.br
 *
 */
 public class SetorPersistor extends Persistor {

	/** Instância de Setor para manipulação pelo persistor */
	private Setor setor;

	/** Construtor genérico */
	public SetorPersistor() {

		this.setor = null;
	}

	/** Construtor genérico */
	public SetorPersistor( Conexao cnx ) throws QtSQLException {

		this.setor = null;
		super.setConexao( cnx );
	}

	/** Construtor que recebe uma instância de Setor */
	public SetorPersistor( Setor setor ) throws QtSQLException {

		if( setor == null ) 
			throw new QtSQLException( "Impossível instanciar SetorPersistor porque está apontando para um nulo!" );

		this.setor = setor;
	}

	/** Construtor que recebe uma conexão e uma instância de Setor */
	public SetorPersistor( Conexao conexao, Setor setor ) throws QtSQLException {

		if( setor == null ) 
			throw new QtSQLException( "Impossível instanciar SetorPersistor porque está apontando para um nulo!" );

		setConexao( conexao );
		this.setor = setor;
	}

	/**
	 * Construtor que recebe a chave da tabela PCMSO.SETORES
	 * @param idSetor Codigo do setor
	 */
	public SetorPersistor( int idSetor )
	  throws QtSQLException { 

		busca( idSetor );
	}

	/**
	 * Construtor que recebe uma conexão e a chave da tabela PCMSO.SETORES
	 * @param idSetor Codigo do setor
	 * @param conexao Conexao com o banco de dados
	 */
	public SetorPersistor( Conexao conexao, int idSetor )
	  throws QtSQLException { 

		super.setConexao( conexao );
		busca( idSetor );
	}

	/**
	 * Método interno para buscar e preencher uma tupla de uma tabela
	 * Utiliza a Instância de Setor para fazer a busca
	 * @param idSetor Codigo do setor
	 */
	private void busca( int idSetor ) throws QtSQLException { 

		SetorLocalizador setorLocalizador = new SetorLocalizador( getConexao(), idSetor );

		if( setorLocalizador.isEmpty() ) {
			throw new QtSQLException( "Chaves da Tabela Setor não encontradas - idSetor: " + idSetor );
		}

		setor = (Setor ) setorLocalizador.getTabelaBasica();
	}

	/**
	 * Permite alimentar o Persistor com um novo registro
	 */
	public void setTabelaBasica( TabelaBasica setor ) throws QtException {

		if( setor instanceof Setor ) {
			this.setor = (Setor) setor;
		} else {
			throw new QtException( "Tabela Básica fornecida tem que ser uma instância de Setor" );
		}
	}

	/**
	 * Permite acessar o registro corrente do persistor
	 */
	public TabelaBasica getTabelaBasica() throws QtException {

		return setor;
	}

	/**
	 * Método para gerar automaticamente uma nova chave
	 */
	public static Integer geraNovoIdSetor( Conexao cnx ) throws QtSQLException {

		Integer result = null;

		Query query = new Query( cnx );

		try{

			query.setSQL( "select max( ID_SETOR ) from PCMSO.SETORES" );

			query.executeQuery();
			result = new Integer( query.getInt( 1 ) + 1 );
		} catch( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			query.liberaRecursos();
		}
		return result;
	}

	/**
	 * Método para gerar automaticamente uma nova chave
	 */
	public static Integer geraNovoIdSetor(  ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return geraNovoIdSetor(  cnx  );
		} finally {
			cnx.libera();
		}
	}


	/**
	 * Método para inserir tupla na tabela
	 */
	public void insere() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "insert into PCMSO.SETORES" );
			cmd.append( "( ID_SETOR, DS_SETOR )" );
			cmd.append( "values " );
			cmd.append( "( ?, ?)" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			try {

				ps.setObject( 1, setor.getIdSetor() );
				ps.setObject( 2, setor.getDsSetor() );
				
				ps.execute();
			} finally { 
				ps.close();
			}
		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}

	/**
	 * Método para alterar tupla na tabela
	 */
	public void altera() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "update PCMSO.SETORES" );
			cmd.append( "  set ID_SETOR = ?, " );
			cmd.append( " DS_SETOR = ?" );
			cmd.append( " where ID_SETOR = ?" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			try {

				ps.setObject( 1, setor.getIdSetor() );
				ps.setObject( 2, setor.getDsSetor() );
				ps.setObject( 3, setor.getIdSetor() );
				
				ps.execute();
			} finally {
				ps.close();
			}
		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}

	/**
	 * Método para excluir tupla de uma tabela
	 */
	public void exclui() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "delete from PCMSO.SETORES" );
			cmd.append( " where ID_SETOR = ?" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			try {

				ps.setObject( 1, setor.getIdSetor() );
				
				ps.execute();
			} finally {
				ps.close();
			}
		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}
}