package jPcmso.persistencia.geral.e;

import java.sql.PreparedStatement;

import quatro.persistencia.Persistor;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.util.QtException;
import quatro.util.QtData;

/**
 *
 * Classe persistor para PCMSO.EXAMES_OCUPACIONAIS
 *
 * @author Samuel Antonio Klein
 * @see Persistor
 * @see ExameOcupacional
 * @see ExameOcupacionalNavegador
 * @see ExameOcupacionalLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 public class ExameOcupacionalPersistor extends Persistor {

	/** Inst�ncia de ExameOcupacional para manipula��o pelo persistor */
	private ExameOcupacional exameOcupacional;

	/** Construtor gen�rico */
	public ExameOcupacionalPersistor() {

		this.exameOcupacional = null;
	}

	/** Construtor gen�rico */
	public ExameOcupacionalPersistor( Conexao cnx ) throws QtSQLException {

		this.exameOcupacional = null;
		super.setConexao( cnx );
	}

	/** Construtor que recebe uma inst�ncia de ExameOcupacional */
	public ExameOcupacionalPersistor( ExameOcupacional exameOcupacional ) throws QtSQLException {

		if( exameOcupacional == null ) 
			throw new QtSQLException( "Imposs�vel instanciar ExameOcupacionalPersistor porque est� apontando para um nulo!" );

		this.exameOcupacional = exameOcupacional;
	}

	/** Construtor que recebe uma conex�o e uma inst�ncia de ExameOcupacional */
	public ExameOcupacionalPersistor( Conexao conexao, ExameOcupacional exameOcupacional ) throws QtSQLException {

		if( exameOcupacional == null ) 
			throw new QtSQLException( "Imposs�vel instanciar ExameOcupacionalPersistor porque est� apontando para um nulo!" );

		setConexao( conexao );
		this.exameOcupacional = exameOcupacional;
	}

	/**
	 * Construtor que recebe a chave da tabela PCMSO.EXAMES_OCUPACIONAIS
	 * @param nrExame Numero do exame
	 */
	public ExameOcupacionalPersistor( String nrExame )
	  throws QtSQLException { 

		busca( nrExame );
	}

	/**
	 * Construtor que recebe uma conex�o e a chave da tabela PCMSO.EXAMES_OCUPACIONAIS
	 * @param nrExame Numero do exame
	 * @param conexao Conexao com o banco de dados
	 */
	public ExameOcupacionalPersistor( Conexao conexao, String nrExame )
	  throws QtSQLException { 

		super.setConexao( conexao );
		busca( nrExame );
	}

	/**
	 * M�todo interno para buscar e preencher uma tupla de uma tabela
	 * Utiliza a Inst�ncia de ExameOcupacional para fazer a busca
	 * @param nrExame Numero do exame
	 */
	private void busca( String nrExame ) throws QtSQLException { 

		ExameOcupacionalLocalizador exameOcupacionalLocalizador = new ExameOcupacionalLocalizador( getConexao(), nrExame );

		if( exameOcupacionalLocalizador.isEmpty() ) {
			throw new QtSQLException( "Chaves da Tabela ExameOcupacional n�o encontradas - nrExame: " + nrExame );
		}

		exameOcupacional = (ExameOcupacional ) exameOcupacionalLocalizador.getTabelaBasica();
	}

	/**
	 * Permite alimentar o Persistor com um novo registro
	 */
	public void setTabelaBasica( TabelaBasica exameOcupacional ) throws QtException {

		if( exameOcupacional instanceof ExameOcupacional ) {
			this.exameOcupacional = (ExameOcupacional) exameOcupacional;
		} else {
			throw new QtException( "Tabela B�sica fornecida tem que ser uma inst�ncia de ExameOcupacional" );
		}
	}

	/**
	 * Permite acessar o registro corrente do persistor
	 */
	public TabelaBasica getTabelaBasica() throws QtException {

		return exameOcupacional;
	}

	/**
	 * M�todo para inserir tupla na tabela
	 */
	public void insere() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "insert into PCMSO.EXAMES_OCUPACIONAIS" );
			cmd.append( "( NR_EXAME, ID_SETOR, ID_FUNCAO, ID_CARGO, CD_CBO, CD_MEDICO_EXAMINADOR, CD_BENEFICIARIO_CARTAO, CD_PROCEDIMENTO, TP_EXAME, DT_EMISSAO_NOTA, DT_EXECUCAO_EXAME, DT_RETORNO_NOTA, DT_CANCELAMENTO, DS_ATIVIDADE, VL_PRESSAO_SISTOLICA, VL_PRESSAO_DIASTOLICA, VL_PULSO_ARTERIAL, VL_PESO, VL_ALTURA, SN_NORMOLINEO, SN_LONGILINEO, SN_BREVILINEO, SN_DIABETE, SN_HIPERTENCAO, SN_ETILISTA, SN_TABAGISTA, QT_CIGARROS_DIA, DS_USO_MEDICAMENTOS, DS_SUMARIO_CONSULTA, TP_CONCIDERADO, DS_OBSERVACOES, DS_LOCAL, SN_PRESSAO_ALTA_PAI, SN_PRESSAO_ALTA_MAE, SN_PRESSAO_ALTA_IRMAOS, SN_PRESSAO_ALTA_AVOS, SN_DIABETE_PAI, SN_DIABETE_MAE, SN_DIABETE_IRMAOS, SN_DIABETE_AVOS, SN_TUBERCULOSE_PAI, SN_TUBERCULOSE_MAE, SN_TUBERCULOSE_IRMAOS, SN_TUBERCULOSE_AVOS, SN_CANCER_PAI, SN_CANCER_MAE, SN_CANCER_IRMAOS, SN_CANCER_AVOS, SN_DOENCA_CARDIACA_PAI, SN_DOENCA_CARDIACA_MAE, SN_DOENCA_CARDIACA_IRMAOS, SN_DOENCA_CARDIACA_AVOS, SN_DOENCA_MENTAL_PAI, SN_DOENCA_MENTAL_MAE, SN_DOENCA_MENTAL_IRMAOS, SN_DOENCA_MENTAL_AVOS, SN_ASMA_ALERGIA_URTICARIA_PAI, SN_ASMA_ALERGIA_URTICARIA_MAE, SN_ASMA_ALERGIA_URTICARIA_IRMAOS, SN_ASMA_ALERGIA_URTICARIA_AVOS, SN_CONVULSAO_PAI, SN_CONVULSAO_MAE, SN_CONVULSAO_IRMAOS, SN_CONVULSAO_AVOS, SN_LENTES_CONTATO_OCULOS, SN_BRONQUITE_ASMA_TOSSE, SN_PROBLEMA_AUDICAO, SN_DOENCA_CONTAGIOSAS, SN_DOENCA_CARDIACA, SN_PRESSAO_ALTA, SN_DOENCA_MENTAL, SN_DOR_PEITO, SN_DISTURBIO_FALA, SN_VARIZES, SN_HEMORROIDAS, SN_HERNIAS, SN_DIFICULDADE_TAREFAS_PESADAS, SN_RESFRIADOS_FREQUENTES, SN_DOENCA_TIREOIDE, SN_PROBLEMAS_DENTARIOS, SN_FOMIGAMENTO_DORMENCIA, SN_ANTERACAO_SONO, SN_TONTURA_VERTIGEM, SN_PROBLEMAS_PESO, SN_ALERGIA, SN_DOR_DE_CABECA, SN_ZUMBIDO, SN_DOENCA_ESTOMAGO, SN_DOENCA_COLUNA_DOR_COSTA, SN_DOENCA_SANGUE, SN_DOENCA_OUVIDO, SN_DOR_ARTICULACOES, SN_DOR_MUSCULAR, SN_NERVOSSIMO, SN_DIAREIAS_FREQUENTES, SN_DOENCA_PELE, SN_AFASTAMENTO_DOENCA, SN_PORTADOR_DEFICIENCIA, DS_DEFINICAO_DEFICIENCIA, TP_FREQUENCIA_USO_MEDICAMENTOS, SN_INTERNADO, DS_MOTIVO_INTERNACAO, DS_CIRURGIAS, DS_OUTRAS_DOENCAS, SN_DOENCA_ACIDENTE_INSS, SN_ACIDENTE_TRABALHO, SN_PRODUTO_QUIMICO, SN_LOCAL_FRIO, SN_LOCAL_POEIRA, SN_LOCAL_CALOR, SN_LOCAL_BARULHO, DS_ULTIMA_OCUPACAO, DS_TEMPO_OCUPACAO_ULTIMO, DS_PENULTIMA_OCUPACAO, DS_TEMPO_OCUPACAO_PENULTIMO, DS_OUTRAS_ATIVIDADES, DS_ESPORTE_PRATICADOS, DS_HOBBY_LAZER, DS_COMENTARIOS )" );
			cmd.append( "values " );
			cmd.append( "( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			ps.setObject( 1, exameOcupacional.getNrExame() );
			ps.setObject( 2, exameOcupacional.getIdSetor() );
			ps.setObject( 3, exameOcupacional.getIdFuncao() );
			ps.setObject( 4, exameOcupacional.getIdCargo() );
			ps.setObject( 5, exameOcupacional.getCdCbo() );
			ps.setObject( 6, exameOcupacional.getCdMedicoExaminador() );
			ps.setObject( 7, exameOcupacional.getCdBeneficiarioCartao() );
			ps.setObject( 8, exameOcupacional.getCdProcedimento() );
			ps.setObject( 9, exameOcupacional.getTpExame() );
			ps.setObject( 10, QtData.getQtDataAsTimestamp( exameOcupacional.getDtEmissaoNota() ) );
			ps.setObject( 11, QtData.getQtDataAsTimestamp( exameOcupacional.getDtExecucaoExame() ) );
			ps.setObject( 12, QtData.getQtDataAsTimestamp( exameOcupacional.getDtRetornoNota() ) );
			ps.setObject( 13, QtData.getQtDataAsTimestamp( exameOcupacional.getDtCancelamento() ) );
			ps.setObject( 14, exameOcupacional.getDsAtividade() );
			ps.setObject( 15, exameOcupacional.getVlPressaoSistolica() );
			ps.setObject( 16, exameOcupacional.getVlPressaoDiastolica() );
			ps.setObject( 17, exameOcupacional.getVlPulsoArterial() );
			ps.setObject( 18, exameOcupacional.getVlPeso() );
			ps.setObject( 19, exameOcupacional.getVlAltura() );
			ps.setObject( 20, exameOcupacional.isSnNormolineo() ? "S": "N"  );
			ps.setObject( 21, exameOcupacional.isSnLongilineo() ? "S": "N"  );
			ps.setObject( 22, exameOcupacional.isSnBrevilineo() ? "S": "N"  );
			ps.setObject( 23, exameOcupacional.isSnDiabete() ? "S": "N"  );
			ps.setObject( 24, exameOcupacional.isSnHipertencao() ? "S": "N"  );
			ps.setObject( 25, exameOcupacional.isSnEtilista() ? "S": "N"  );
			ps.setObject( 26, exameOcupacional.isSnTabagista() ? "S": "N"  );
			ps.setObject( 27, exameOcupacional.getQtCigarrosDia() );
			ps.setObject( 28, exameOcupacional.getDsUsoMedicamentos() );
			ps.setObject( 29, exameOcupacional.getDsSumarioConsulta() );
			ps.setObject( 30, exameOcupacional.getTpConciderado() );
			ps.setObject( 31, exameOcupacional.getDsObservacoes() );
			ps.setObject( 32, exameOcupacional.getDsLocal() );
			ps.setObject( 33, exameOcupacional.isSnPressaoAltaPai() ? "S": "N"  );
			ps.setObject( 34, exameOcupacional.isSnPressaoAltaMae() ? "S": "N"  );
			ps.setObject( 35, exameOcupacional.isSnPressaoAltaIrmaos() ? "S": "N"  );
			ps.setObject( 36, exameOcupacional.isSnPressaoAltaAvos() ? "S": "N"  );
			ps.setObject( 37, exameOcupacional.isSnDiabetePai() ? "S": "N"  );
			ps.setObject( 38, exameOcupacional.isSnDiabeteMae() ? "S": "N"  );
			ps.setObject( 39, exameOcupacional.isSnDiabeteIrmaos() ? "S": "N"  );
			ps.setObject( 40, exameOcupacional.isSnDiabeteAvos() ? "S": "N"  );
			ps.setObject( 41, exameOcupacional.isSnTuberculosePai() ? "S": "N"  );
			ps.setObject( 42, exameOcupacional.isSnTuberculoseMae() ? "S": "N"  );
			ps.setObject( 43, exameOcupacional.isSnTuberculoseIrmaos() ? "S": "N"  );
			ps.setObject( 44, exameOcupacional.isSnTuberculoseAvos() ? "S": "N"  );
			ps.setObject( 45, exameOcupacional.isSnCancerPai() ? "S": "N"  );
			ps.setObject( 46, exameOcupacional.isSnCancerMae() ? "S": "N"  );
			ps.setObject( 47, exameOcupacional.isSnCancerIrmaos() ? "S": "N"  );
			ps.setObject( 48, exameOcupacional.isSnCancerAvos() ? "S": "N"  );
			ps.setObject( 49, exameOcupacional.isSnDoencaCardiacaPai() ? "S": "N"  );
			ps.setObject( 50, exameOcupacional.isSnDoencaCardiacaMae() ? "S": "N"  );
			ps.setObject( 51, exameOcupacional.isSnDoencaCardiacaIrmaos() ? "S": "N"  );
			ps.setObject( 52, exameOcupacional.isSnDoencaCardiacaAvos() ? "S": "N"  );
			ps.setObject( 53, exameOcupacional.isSnDoencaMentalPai() ? "S": "N"  );
			ps.setObject( 54, exameOcupacional.isSnDoencaMentalMae() ? "S": "N"  );
			ps.setObject( 55, exameOcupacional.isSnDoencaMentalIrmaos() ? "S": "N"  );
			ps.setObject( 56, exameOcupacional.isSnDoencaMentalAvos() ? "S": "N"  );
			ps.setObject( 57, exameOcupacional.isSnAsmaAlergiaUrticariaPai() ? "S": "N"  );
			ps.setObject( 58, exameOcupacional.isSnAsmaAlergiaUrticariaMae() ? "S": "N"  );
			ps.setObject( 59, exameOcupacional.isSnAsmaAlergiaUrticariaIrmaos() ? "S": "N"  );
			ps.setObject( 60, exameOcupacional.isSnAsmaAlergiaUrticariaAvos() ? "S": "N"  );
			ps.setObject( 61, exameOcupacional.isSnConvulsaoPai() ? "S": "N"  );
			ps.setObject( 62, exameOcupacional.isSnConvulsaoMae() ? "S": "N"  );
			ps.setObject( 63, exameOcupacional.isSnConvulsaoIrmaos() ? "S": "N"  );
			ps.setObject( 64, exameOcupacional.isSnConvulsaoAvos() ? "S": "N"  );
			ps.setObject( 65, exameOcupacional.isSnLentesContatoOculos() ? "S": "N"  );
			ps.setObject( 66, exameOcupacional.isSnBronquiteAsmaTosse() ? "S": "N"  );
			ps.setObject( 67, exameOcupacional.isSnProblemaAudicao() ? "S": "N"  );
			ps.setObject( 68, exameOcupacional.isSnDoencaContagiosas() ? "S": "N"  );
			ps.setObject( 69, exameOcupacional.isSnDoencaCardiaca() ? "S": "N"  );
			ps.setObject( 70, exameOcupacional.isSnPressaoAlta() ? "S": "N"  );
			ps.setObject( 71, exameOcupacional.isSnDoencaMental() ? "S": "N"  );
			ps.setObject( 72, exameOcupacional.isSnDorPeito() ? "S": "N"  );
			ps.setObject( 73, exameOcupacional.isSnDisturbioFala() ? "S": "N"  );
			ps.setObject( 74, exameOcupacional.isSnVarizes() ? "S": "N"  );
			ps.setObject( 75, exameOcupacional.isSnHemorroidas() ? "S": "N"  );
			ps.setObject( 76, exameOcupacional.isSnHernias() ? "S": "N"  );
			ps.setObject( 77, exameOcupacional.isSnDificuldadeTarefasPesadas() ? "S": "N"  );
			ps.setObject( 78, exameOcupacional.isSnResfriadosFrequentes() ? "S": "N"  );
			ps.setObject( 79, exameOcupacional.isSnDoencaTireoide() ? "S": "N"  );
			ps.setObject( 80, exameOcupacional.isSnProblemasDentarios() ? "S": "N"  );
			ps.setObject( 81, exameOcupacional.isSnFomigamentoDormencia() ? "S": "N"  );
			ps.setObject( 82, exameOcupacional.isSnAnteracaoSono() ? "S": "N"  );
			ps.setObject( 83, exameOcupacional.isSnTonturaVertigem() ? "S": "N"  );
			ps.setObject( 84, exameOcupacional.isSnProblemasPeso() ? "S": "N"  );
			ps.setObject( 85, exameOcupacional.isSnAlergia() ? "S": "N"  );
			ps.setObject( 86, exameOcupacional.isSnDorDeCabeca() ? "S": "N"  );
			ps.setObject( 87, exameOcupacional.isSnZumbido() ? "S": "N"  );
			ps.setObject( 88, exameOcupacional.isSnDoencaEstomago() ? "S": "N"  );
			ps.setObject( 89, exameOcupacional.isSnDoencaColunaDorCosta() ? "S": "N"  );
			ps.setObject( 90, exameOcupacional.isSnDoencaSangue() ? "S": "N"  );
			ps.setObject( 91, exameOcupacional.isSnDoencaOuvido() ? "S": "N"  );
			ps.setObject( 92, exameOcupacional.isSnDorArticulacoes() ? "S": "N"  );
			ps.setObject( 93, exameOcupacional.isSnDorMuscular() ? "S": "N"  );
			ps.setObject( 94, exameOcupacional.isSnNervossimo() ? "S": "N"  );
			ps.setObject( 95, exameOcupacional.isSnDiareiasFrequentes() ? "S": "N"  );
			ps.setObject( 96, exameOcupacional.isSnDoencaPele() ? "S": "N"  );
			ps.setObject( 97, exameOcupacional.isSnAfastamentoDoenca() ? "S": "N"  );
			ps.setObject( 98, exameOcupacional.isSnPortadorDeficiencia() ? "S": "N"  );
			ps.setObject( 99, exameOcupacional.getDsDefinicaoDeficiencia() );
			ps.setObject( 100, exameOcupacional.getTpFrequenciaUsoMedicamentos() );
			ps.setObject( 101, exameOcupacional.isSnInternado() ? "S": "N"  );
			ps.setObject( 102, exameOcupacional.getDsMotivoInternacao() );
			ps.setObject( 103, exameOcupacional.getDsCirurgias() );
			ps.setObject( 104, exameOcupacional.getDsOutrasDoencas() );
			ps.setObject( 105, exameOcupacional.isSnDoencaAcidenteInss() ? "S": "N"  );
			ps.setObject( 106, exameOcupacional.isSnAcidenteTrabalho() ? "S": "N"  );
			ps.setObject( 107, exameOcupacional.isSnProdutoQuimico() ? "S": "N"  );
			ps.setObject( 108, exameOcupacional.isSnLocalFrio() ? "S": "N"  );
			ps.setObject( 109, exameOcupacional.isSnLocalPoeira() ? "S": "N"  );
			ps.setObject( 110, exameOcupacional.isSnLocalCalor() ? "S": "N"  );
			ps.setObject( 111, exameOcupacional.isSnLocalBarulho() ? "S": "N"  );
			ps.setObject( 112, exameOcupacional.getDsUltimaOcupacao() );
			ps.setObject( 113, exameOcupacional.getDsTempoOcupacaoUltimo() );
			ps.setObject( 114, exameOcupacional.getDsPenultimaOcupacao() );
			ps.setObject( 115, exameOcupacional.getDsTempoOcupacaoPenultimo() );
			ps.setObject( 116, exameOcupacional.getDsOutrasAtividades() );
			ps.setObject( 117, exameOcupacional.getDsEsportePraticados() );
			ps.setObject( 118, exameOcupacional.getDsHobbyLazer() );
			ps.setObject( 119, exameOcupacional.getDsComentarios() );
			
			ps.execute();

		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}

	/**
	 * M�todo para alterar tupla na tabela
	 */
	public void altera() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "update PCMSO.EXAMES_OCUPACIONAIS" );
			cmd.append( "  set NR_EXAME = ?, " );
			cmd.append( " ID_SETOR = ?, " );
			cmd.append( " ID_FUNCAO = ?, " );
			cmd.append( " ID_CARGO = ?, " );
			cmd.append( " CD_CBO = ?, " );
			cmd.append( " CD_MEDICO_EXAMINADOR = ?, " );
			cmd.append( " CD_BENEFICIARIO_CARTAO = ?, " );
			cmd.append( " CD_PROCEDIMENTO = ?, " );
			cmd.append( " TP_EXAME = ?, " );
			cmd.append( " DT_EMISSAO_NOTA = ?, " );
			cmd.append( " DT_EXECUCAO_EXAME = ?, " );
			cmd.append( " DT_RETORNO_NOTA = ?, " );
			cmd.append( " DT_CANCELAMENTO = ?, " );
			cmd.append( " DS_ATIVIDADE = ?, " );
			cmd.append( " VL_PRESSAO_SISTOLICA = ?, " );
			cmd.append( " VL_PRESSAO_DIASTOLICA = ?, " );
			cmd.append( " VL_PULSO_ARTERIAL = ?, " );
			cmd.append( " VL_PESO = ?, " );
			cmd.append( " VL_ALTURA = ?, " );
			cmd.append( " SN_NORMOLINEO = ?, " );
			cmd.append( " SN_LONGILINEO = ?, " );
			cmd.append( " SN_BREVILINEO = ?, " );
			cmd.append( " SN_DIABETE = ?, " );
			cmd.append( " SN_HIPERTENCAO = ?, " );
			cmd.append( " SN_ETILISTA = ?, " );
			cmd.append( " SN_TABAGISTA = ?, " );
			cmd.append( " QT_CIGARROS_DIA = ?, " );
			cmd.append( " DS_USO_MEDICAMENTOS = ?, " );
			cmd.append( " DS_SUMARIO_CONSULTA = ?, " );
			cmd.append( " TP_CONCIDERADO = ?, " );
			cmd.append( " DS_OBSERVACOES = ?, " );
			cmd.append( " DS_LOCAL = ?, " );
			cmd.append( " SN_PRESSAO_ALTA_PAI = ?, " );
			cmd.append( " SN_PRESSAO_ALTA_MAE = ?, " );
			cmd.append( " SN_PRESSAO_ALTA_IRMAOS = ?, " );
			cmd.append( " SN_PRESSAO_ALTA_AVOS = ?, " );
			cmd.append( " SN_DIABETE_PAI = ?, " );
			cmd.append( " SN_DIABETE_MAE = ?, " );
			cmd.append( " SN_DIABETE_IRMAOS = ?, " );
			cmd.append( " SN_DIABETE_AVOS = ?, " );
			cmd.append( " SN_TUBERCULOSE_PAI = ?, " );
			cmd.append( " SN_TUBERCULOSE_MAE = ?, " );
			cmd.append( " SN_TUBERCULOSE_IRMAOS = ?, " );
			cmd.append( " SN_TUBERCULOSE_AVOS = ?, " );
			cmd.append( " SN_CANCER_PAI = ?, " );
			cmd.append( " SN_CANCER_MAE = ?, " );
			cmd.append( " SN_CANCER_IRMAOS = ?, " );
			cmd.append( " SN_CANCER_AVOS = ?, " );
			cmd.append( " SN_DOENCA_CARDIACA_PAI = ?, " );
			cmd.append( " SN_DOENCA_CARDIACA_MAE = ?, " );
			cmd.append( " SN_DOENCA_CARDIACA_IRMAOS = ?, " );
			cmd.append( " SN_DOENCA_CARDIACA_AVOS = ?, " );
			cmd.append( " SN_DOENCA_MENTAL_PAI = ?, " );
			cmd.append( " SN_DOENCA_MENTAL_MAE = ?, " );
			cmd.append( " SN_DOENCA_MENTAL_IRMAOS = ?, " );
			cmd.append( " SN_DOENCA_MENTAL_AVOS = ?, " );
			cmd.append( " SN_ASMA_ALERGIA_URTICARIA_PAI = ?, " );
			cmd.append( " SN_ASMA_ALERGIA_URTICARIA_MAE = ?, " );
			cmd.append( " SN_ASMA_ALERGIA_URTICARIA_IRMAOS = ?, " );
			cmd.append( " SN_ASMA_ALERGIA_URTICARIA_AVOS = ?, " );
			cmd.append( " SN_CONVULSAO_PAI = ?, " );
			cmd.append( " SN_CONVULSAO_MAE = ?, " );
			cmd.append( " SN_CONVULSAO_IRMAOS = ?, " );
			cmd.append( " SN_CONVULSAO_AVOS = ?, " );
			cmd.append( " SN_LENTES_CONTATO_OCULOS = ?, " );
			cmd.append( " SN_BRONQUITE_ASMA_TOSSE = ?, " );
			cmd.append( " SN_PROBLEMA_AUDICAO = ?, " );
			cmd.append( " SN_DOENCA_CONTAGIOSAS = ?, " );
			cmd.append( " SN_DOENCA_CARDIACA = ?, " );
			cmd.append( " SN_PRESSAO_ALTA = ?, " );
			cmd.append( " SN_DOENCA_MENTAL = ?, " );
			cmd.append( " SN_DOR_PEITO = ?, " );
			cmd.append( " SN_DISTURBIO_FALA = ?, " );
			cmd.append( " SN_VARIZES = ?, " );
			cmd.append( " SN_HEMORROIDAS = ?, " );
			cmd.append( " SN_HERNIAS = ?, " );
			cmd.append( " SN_DIFICULDADE_TAREFAS_PESADAS = ?, " );
			cmd.append( " SN_RESFRIADOS_FREQUENTES = ?, " );
			cmd.append( " SN_DOENCA_TIREOIDE = ?, " );
			cmd.append( " SN_PROBLEMAS_DENTARIOS = ?, " );
			cmd.append( " SN_FOMIGAMENTO_DORMENCIA = ?, " );
			cmd.append( " SN_ANTERACAO_SONO = ?, " );
			cmd.append( " SN_TONTURA_VERTIGEM = ?, " );
			cmd.append( " SN_PROBLEMAS_PESO = ?, " );
			cmd.append( " SN_ALERGIA = ?, " );
			cmd.append( " SN_DOR_DE_CABECA = ?, " );
			cmd.append( " SN_ZUMBIDO = ?, " );
			cmd.append( " SN_DOENCA_ESTOMAGO = ?, " );
			cmd.append( " SN_DOENCA_COLUNA_DOR_COSTA = ?, " );
			cmd.append( " SN_DOENCA_SANGUE = ?, " );
			cmd.append( " SN_DOENCA_OUVIDO = ?, " );
			cmd.append( " SN_DOR_ARTICULACOES = ?, " );
			cmd.append( " SN_DOR_MUSCULAR = ?, " );
			cmd.append( " SN_NERVOSSIMO = ?, " );
			cmd.append( " SN_DIAREIAS_FREQUENTES = ?, " );
			cmd.append( " SN_DOENCA_PELE = ?, " );
			cmd.append( " SN_AFASTAMENTO_DOENCA = ?, " );
			cmd.append( " SN_PORTADOR_DEFICIENCIA = ?, " );
			cmd.append( " DS_DEFINICAO_DEFICIENCIA = ?, " );
			cmd.append( " TP_FREQUENCIA_USO_MEDICAMENTOS = ?, " );
			cmd.append( " SN_INTERNADO = ?, " );
			cmd.append( " DS_MOTIVO_INTERNACAO = ?, " );
			cmd.append( " DS_CIRURGIAS = ?, " );
			cmd.append( " DS_OUTRAS_DOENCAS = ?, " );
			cmd.append( " SN_DOENCA_ACIDENTE_INSS = ?, " );
			cmd.append( " SN_ACIDENTE_TRABALHO = ?, " );
			cmd.append( " SN_PRODUTO_QUIMICO = ?, " );
			cmd.append( " SN_LOCAL_FRIO = ?, " );
			cmd.append( " SN_LOCAL_POEIRA = ?, " );
			cmd.append( " SN_LOCAL_CALOR = ?, " );
			cmd.append( " SN_LOCAL_BARULHO = ?, " );
			cmd.append( " DS_ULTIMA_OCUPACAO = ?, " );
			cmd.append( " DS_TEMPO_OCUPACAO_ULTIMO = ?, " );
			cmd.append( " DS_PENULTIMA_OCUPACAO = ?, " );
			cmd.append( " DS_TEMPO_OCUPACAO_PENULTIMO = ?, " );
			cmd.append( " DS_OUTRAS_ATIVIDADES = ?, " );
			cmd.append( " DS_ESPORTE_PRATICADOS = ?, " );
			cmd.append( " DS_HOBBY_LAZER = ?, " );
			cmd.append( " DS_COMENTARIOS = ?" );
			cmd.append( " where NR_EXAME like ?" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			ps.setObject( 1, exameOcupacional.getNrExame() );
			ps.setObject( 2, exameOcupacional.getIdSetor() );
			ps.setObject( 3, exameOcupacional.getIdFuncao() );
			ps.setObject( 4, exameOcupacional.getIdCargo() );
			ps.setObject( 5, exameOcupacional.getCdCbo() );
			ps.setObject( 6, exameOcupacional.getCdMedicoExaminador() );
			ps.setObject( 7, exameOcupacional.getCdBeneficiarioCartao() );
			ps.setObject( 8, exameOcupacional.getCdProcedimento() );
			ps.setObject( 9, exameOcupacional.getTpExame() );
			ps.setObject( 10, QtData.getQtDataAsTimestamp( exameOcupacional.getDtEmissaoNota() ) );
			ps.setObject( 11, QtData.getQtDataAsTimestamp( exameOcupacional.getDtExecucaoExame() ) );
			ps.setObject( 12, QtData.getQtDataAsTimestamp( exameOcupacional.getDtRetornoNota() ) );
			ps.setObject( 13, QtData.getQtDataAsTimestamp( exameOcupacional.getDtCancelamento() ) );
			ps.setObject( 14, exameOcupacional.getDsAtividade() );
			ps.setObject( 15, exameOcupacional.getVlPressaoSistolica() );
			ps.setObject( 16, exameOcupacional.getVlPressaoDiastolica() );
			ps.setObject( 17, exameOcupacional.getVlPulsoArterial() );
			ps.setObject( 18, exameOcupacional.getVlPeso() );
			ps.setObject( 19, exameOcupacional.getVlAltura() );
			ps.setObject( 20, exameOcupacional.isSnNormolineo() ? "S": "N"  );
			ps.setObject( 21, exameOcupacional.isSnLongilineo() ? "S": "N"  );
			ps.setObject( 22, exameOcupacional.isSnBrevilineo() ? "S": "N"  );
			ps.setObject( 23, exameOcupacional.isSnDiabete() ? "S": "N"  );
			ps.setObject( 24, exameOcupacional.isSnHipertencao() ? "S": "N"  );
			ps.setObject( 25, exameOcupacional.isSnEtilista() ? "S": "N"  );
			ps.setObject( 26, exameOcupacional.isSnTabagista() ? "S": "N"  );
			ps.setObject( 27, exameOcupacional.getQtCigarrosDia() );
			ps.setObject( 28, exameOcupacional.getDsUsoMedicamentos() );
			ps.setObject( 29, exameOcupacional.getDsSumarioConsulta() );
			ps.setObject( 30, exameOcupacional.getTpConciderado() );
			ps.setObject( 31, exameOcupacional.getDsObservacoes() );
			ps.setObject( 32, exameOcupacional.getDsLocal() );
			ps.setObject( 33, exameOcupacional.isSnPressaoAltaPai() ? "S": "N"  );
			ps.setObject( 34, exameOcupacional.isSnPressaoAltaMae() ? "S": "N"  );
			ps.setObject( 35, exameOcupacional.isSnPressaoAltaIrmaos() ? "S": "N"  );
			ps.setObject( 36, exameOcupacional.isSnPressaoAltaAvos() ? "S": "N"  );
			ps.setObject( 37, exameOcupacional.isSnDiabetePai() ? "S": "N"  );
			ps.setObject( 38, exameOcupacional.isSnDiabeteMae() ? "S": "N"  );
			ps.setObject( 39, exameOcupacional.isSnDiabeteIrmaos() ? "S": "N"  );
			ps.setObject( 40, exameOcupacional.isSnDiabeteAvos() ? "S": "N"  );
			ps.setObject( 41, exameOcupacional.isSnTuberculosePai() ? "S": "N"  );
			ps.setObject( 42, exameOcupacional.isSnTuberculoseMae() ? "S": "N"  );
			ps.setObject( 43, exameOcupacional.isSnTuberculoseIrmaos() ? "S": "N"  );
			ps.setObject( 44, exameOcupacional.isSnTuberculoseAvos() ? "S": "N"  );
			ps.setObject( 45, exameOcupacional.isSnCancerPai() ? "S": "N"  );
			ps.setObject( 46, exameOcupacional.isSnCancerMae() ? "S": "N"  );
			ps.setObject( 47, exameOcupacional.isSnCancerIrmaos() ? "S": "N"  );
			ps.setObject( 48, exameOcupacional.isSnCancerAvos() ? "S": "N"  );
			ps.setObject( 49, exameOcupacional.isSnDoencaCardiacaPai() ? "S": "N"  );
			ps.setObject( 50, exameOcupacional.isSnDoencaCardiacaMae() ? "S": "N"  );
			ps.setObject( 51, exameOcupacional.isSnDoencaCardiacaIrmaos() ? "S": "N"  );
			ps.setObject( 52, exameOcupacional.isSnDoencaCardiacaAvos() ? "S": "N"  );
			ps.setObject( 53, exameOcupacional.isSnDoencaMentalPai() ? "S": "N"  );
			ps.setObject( 54, exameOcupacional.isSnDoencaMentalMae() ? "S": "N"  );
			ps.setObject( 55, exameOcupacional.isSnDoencaMentalIrmaos() ? "S": "N"  );
			ps.setObject( 56, exameOcupacional.isSnDoencaMentalAvos() ? "S": "N"  );
			ps.setObject( 57, exameOcupacional.isSnAsmaAlergiaUrticariaPai() ? "S": "N"  );
			ps.setObject( 58, exameOcupacional.isSnAsmaAlergiaUrticariaMae() ? "S": "N"  );
			ps.setObject( 59, exameOcupacional.isSnAsmaAlergiaUrticariaIrmaos() ? "S": "N"  );
			ps.setObject( 60, exameOcupacional.isSnAsmaAlergiaUrticariaAvos() ? "S": "N"  );
			ps.setObject( 61, exameOcupacional.isSnConvulsaoPai() ? "S": "N"  );
			ps.setObject( 62, exameOcupacional.isSnConvulsaoMae() ? "S": "N"  );
			ps.setObject( 63, exameOcupacional.isSnConvulsaoIrmaos() ? "S": "N"  );
			ps.setObject( 64, exameOcupacional.isSnConvulsaoAvos() ? "S": "N"  );
			ps.setObject( 65, exameOcupacional.isSnLentesContatoOculos() ? "S": "N"  );
			ps.setObject( 66, exameOcupacional.isSnBronquiteAsmaTosse() ? "S": "N"  );
			ps.setObject( 67, exameOcupacional.isSnProblemaAudicao() ? "S": "N"  );
			ps.setObject( 68, exameOcupacional.isSnDoencaContagiosas() ? "S": "N"  );
			ps.setObject( 69, exameOcupacional.isSnDoencaCardiaca() ? "S": "N"  );
			ps.setObject( 70, exameOcupacional.isSnPressaoAlta() ? "S": "N"  );
			ps.setObject( 71, exameOcupacional.isSnDoencaMental() ? "S": "N"  );
			ps.setObject( 72, exameOcupacional.isSnDorPeito() ? "S": "N"  );
			ps.setObject( 73, exameOcupacional.isSnDisturbioFala() ? "S": "N"  );
			ps.setObject( 74, exameOcupacional.isSnVarizes() ? "S": "N"  );
			ps.setObject( 75, exameOcupacional.isSnHemorroidas() ? "S": "N"  );
			ps.setObject( 76, exameOcupacional.isSnHernias() ? "S": "N"  );
			ps.setObject( 77, exameOcupacional.isSnDificuldadeTarefasPesadas() ? "S": "N"  );
			ps.setObject( 78, exameOcupacional.isSnResfriadosFrequentes() ? "S": "N"  );
			ps.setObject( 79, exameOcupacional.isSnDoencaTireoide() ? "S": "N"  );
			ps.setObject( 80, exameOcupacional.isSnProblemasDentarios() ? "S": "N"  );
			ps.setObject( 81, exameOcupacional.isSnFomigamentoDormencia() ? "S": "N"  );
			ps.setObject( 82, exameOcupacional.isSnAnteracaoSono() ? "S": "N"  );
			ps.setObject( 83, exameOcupacional.isSnTonturaVertigem() ? "S": "N"  );
			ps.setObject( 84, exameOcupacional.isSnProblemasPeso() ? "S": "N"  );
			ps.setObject( 85, exameOcupacional.isSnAlergia() ? "S": "N"  );
			ps.setObject( 86, exameOcupacional.isSnDorDeCabeca() ? "S": "N"  );
			ps.setObject( 87, exameOcupacional.isSnZumbido() ? "S": "N"  );
			ps.setObject( 88, exameOcupacional.isSnDoencaEstomago() ? "S": "N"  );
			ps.setObject( 89, exameOcupacional.isSnDoencaColunaDorCosta() ? "S": "N"  );
			ps.setObject( 90, exameOcupacional.isSnDoencaSangue() ? "S": "N"  );
			ps.setObject( 91, exameOcupacional.isSnDoencaOuvido() ? "S": "N"  );
			ps.setObject( 92, exameOcupacional.isSnDorArticulacoes() ? "S": "N"  );
			ps.setObject( 93, exameOcupacional.isSnDorMuscular() ? "S": "N"  );
			ps.setObject( 94, exameOcupacional.isSnNervossimo() ? "S": "N"  );
			ps.setObject( 95, exameOcupacional.isSnDiareiasFrequentes() ? "S": "N"  );
			ps.setObject( 96, exameOcupacional.isSnDoencaPele() ? "S": "N"  );
			ps.setObject( 97, exameOcupacional.isSnAfastamentoDoenca() ? "S": "N"  );
			ps.setObject( 98, exameOcupacional.isSnPortadorDeficiencia() ? "S": "N"  );
			ps.setObject( 99, exameOcupacional.getDsDefinicaoDeficiencia() );
			ps.setObject( 100, exameOcupacional.getTpFrequenciaUsoMedicamentos() );
			ps.setObject( 101, exameOcupacional.isSnInternado() ? "S": "N"  );
			ps.setObject( 102, exameOcupacional.getDsMotivoInternacao() );
			ps.setObject( 103, exameOcupacional.getDsCirurgias() );
			ps.setObject( 104, exameOcupacional.getDsOutrasDoencas() );
			ps.setObject( 105, exameOcupacional.isSnDoencaAcidenteInss() ? "S": "N"  );
			ps.setObject( 106, exameOcupacional.isSnAcidenteTrabalho() ? "S": "N"  );
			ps.setObject( 107, exameOcupacional.isSnProdutoQuimico() ? "S": "N"  );
			ps.setObject( 108, exameOcupacional.isSnLocalFrio() ? "S": "N"  );
			ps.setObject( 109, exameOcupacional.isSnLocalPoeira() ? "S": "N"  );
			ps.setObject( 110, exameOcupacional.isSnLocalCalor() ? "S": "N"  );
			ps.setObject( 111, exameOcupacional.isSnLocalBarulho() ? "S": "N"  );
			ps.setObject( 112, exameOcupacional.getDsUltimaOcupacao() );
			ps.setObject( 113, exameOcupacional.getDsTempoOcupacaoUltimo() );
			ps.setObject( 114, exameOcupacional.getDsPenultimaOcupacao() );
			ps.setObject( 115, exameOcupacional.getDsTempoOcupacaoPenultimo() );
			ps.setObject( 116, exameOcupacional.getDsOutrasAtividades() );
			ps.setObject( 117, exameOcupacional.getDsEsportePraticados() );
			ps.setObject( 118, exameOcupacional.getDsHobbyLazer() );
			ps.setObject( 119, exameOcupacional.getDsComentarios() );
			
			ps.setObject( 120, exameOcupacional.getNrExame() );
			
			ps.execute();

		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}

	/**
	 * M�todo para excluir tupla de uma tabela
	 */
	public void exclui() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "delete from PCMSO.EXAMES_OCUPACIONAIS" );
			cmd.append( " where NR_EXAME like ?" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			ps.setObject( 1, exameOcupacional.getNrExame() );
			
			ps.execute();

		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}
}