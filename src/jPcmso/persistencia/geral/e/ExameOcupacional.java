package jPcmso.persistencia.geral.e;

import quatro.persistencia.TabelaBasica;
import quatro.smartDB.IgnoredField;
import quatro.smartDB.PrimaryKey;
import quatro.smartDB.Table;
import quatro.util.QtData;
import quatro.util.QtString;

/**
 *
 * Classe b�sica para PCMSO.EXAMES_OCUPACIONAIS
 *
 * exames ocupacional so se tiver funcao do exame
 * 
 * @author Samuel Antonio Klein
 * @see ExameOcupacionalNavegador
 * @see ExameOcupacionalPersistor
 * @see ExameOcupacionalLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 @Table(name="PCMSO.EXAMES_OCUPACIONAIS")
 public class ExameOcupacional implements TabelaBasica {

	@IgnoredField
	private static final long serialVersionUID = -2030059523807702025L;

	/** Numero do exame (chave-char(7)) */
	@PrimaryKey
	private String nrExame;
	/** ID_SETOR */
	private Integer idSetor;
	/** ID_FUNCAO */
	private Integer idFuncao;
	/** ID_CARGO */
	private Integer idCargo;
	/** CD_CBO (char(6)) */
	private String cdCbo;
	/** Medico examinador (char(6)) */
	private String cdMedicoExaminador;
	/** Codigo do benenficiario (char(12)) */
	private String cdBeneficiarioCartao;
	/** 
	 * Codigo do procedimento (char(8))
	 * 
	 * Talvez excluir
	 */
	private String cdProcedimento;
	/** 
	 * Tipo de Exame (char(1))
	 * A - Admissional
	 * P - Peri�dico
	 * R - Retorno ao Trabalho
	 * M - Mudan�a de Fun��o
	 * D - Demissional
	 * E - Referencial Peri�dico
	 */
	private String tpExame;
	/** Data de emissao */
	private QtData dtEmissaoNota;
	/** Data do exame */
	private QtData dtExecucaoExame;
	/** Data da execucao */
	private QtData dtRetornoNota;
	/** Data do retorno */
	private QtData dtCancelamento;
	/** Descri��o da atividade (varchar(250)) */
	private String dsAtividade;
	/** Valor press�o arterial sist�lica */
	private Integer vlPressaoSistolica;
	/** Valor press�o arterial diast�lica */
	private Integer vlPressaoDiastolica;
	/** Valor de pulso arterial */
	private Double vlPulsoArterial;
	/** Valor do peso */
	private Double vlPeso;
	/** Valor da altura */
	private Double vlAltura;
	/** Nomolineo (char(1)) */
	private boolean snNormolineo;
	/** Longilineo (char(1)) */
	private boolean snLongilineo;
	/** Brevilineo (char(1)) */
	private boolean snBrevilineo;
	/** Possui diabete (char(1)) */
	private boolean snDiabete;
	/** Possui Hiperten��o (char(1)) */
	private boolean snHipertencao;
	/** Possui Etilista (char(1)) */
	private boolean snEtilista;
	/** Identificador se � Tabagista (char(1)) */
	private boolean snTabagista;
	/** Quantidade de Cigarros por dia */
	private Integer qtCigarrosDia;
	/** Descricao de uso de medicamento */
	private String dsUsoMedicamentos;
	/** Sumario da Consulta */
	private String dsSumarioConsulta;
	/** 
	 * Tipo de concidera��es (char(1))
	 * 
	 * A - apto
	 * I - Inapto
	 */
	private String tpConciderado;
	/** Descri��es de Observacoes */
	private String dsObservacoes;
	/** Descri��o do local (varchar(250)) */
	private String dsLocal;
	/** 
	 * Antecedentes Familiares (char(1))
	 * press�o alta PAI
	 */
	private boolean snPressaoAltaPai;
	/** 
	 * Antecedentes Familiares (char(1))
	 * press�o alta M�e
	 */
	private boolean snPressaoAltaMae;
	/** 
	 * Antecedentes Familiares (char(1))
	 * press�o alta IRM�OS
	 */
	private boolean snPressaoAltaIrmaos;
	/** 
	 * Antecedentes Familiares (char(1))
	 * press�o alta AVOS
	 */
	private boolean snPressaoAltaAvos;
	/** 
	 * Antecedentes Familiares (char(1))
	 * Diabete PAI
	 */
	private boolean snDiabetePai;
	/** 
	 * Antecedentes Familiares (char(1))
	 * Diabete M�E
	 */
	private boolean snDiabeteMae;
	/** 
	 * Antecedentes Familiares (char(1))
	 * Diabete IRM�OS
	 */
	private boolean snDiabeteIrmaos;
	/** 
	 * Antecedentes Familiares (char(1))
	 * Diabete AVOS
	 */
	private boolean snDiabeteAvos;
	/** 
	 * Antecedentes Familiares (char(1))
	 * TUBERCULOSE PAI
	 */
	private boolean snTuberculosePai;
	/** 
	 * Antecedentes Familiares (char(1))
	 * TUBERCULOSE M�E
	 */
	private boolean snTuberculoseMae;
	/** 
	 * Antecedentes Familiares (char(1))
	 * TUBERCULOSE IRM�OS
	 */
	private boolean snTuberculoseIrmaos;
	/** 
	 * Antecedentes Familiares (char(1))
	 * TUBERCULOSE AVOS
	 */
	private boolean snTuberculoseAvos;
	/** 
	 * Antecedentes Familiares (char(1))
	 * CANCER PAI
	 */
	private boolean snCancerPai;
	/** 
	 * Antecedentes Familiares (char(1))
	 * CANCER M�E
	 */
	private boolean snCancerMae;
	/** 
	 * Antecedentes Familiares (char(1))
	 * CANCER IRM�OS
	 */
	private boolean snCancerIrmaos;
	/** 
	 * Antecedentes Familiares (char(1))
	 * CANCER AVOS
	 */
	private boolean snCancerAvos;
	/** Antecedentes Pessoal (char(1)) */
	private boolean snDoencaCardiacaPai;
	/** Antecedentes Pessoal (char(1)) */
	private boolean snDoencaCardiacaMae;
	/** Antecedentes Pessoal (char(1)) */
	private boolean snDoencaCardiacaIrmaos;
	/** Antecedentes Pessoal (char(1)) */
	private boolean snDoencaCardiacaAvos;
	/** Antecedentes Pessoal (char(1)) */
	private boolean snDoencaMentalPai;
	/** Antecedentes Pessoal (char(1)) */
	private boolean snDoencaMentalMae;
	/** Antecedentes Pessoal (char(1)) */
	private boolean snDoencaMentalIrmaos;
	/** Antecedentes Pessoal (char(1)) */
	private boolean snDoencaMentalAvos;
	/** Antecedentes Pessoal (char(1)) */
	private boolean snAsmaAlergiaUrticariaPai;
	/** Antecedentes Pessoal (char(1)) */
	private boolean snAsmaAlergiaUrticariaMae;
	/** Antecedentes Pessoal (char(1)) */
	private boolean snAsmaAlergiaUrticariaIrmaos;
	/** Antecedentes Pessoal (char(1)) */
	private boolean snAsmaAlergiaUrticariaAvos;
	/** Antecedentes Pessoal (char(1)) */
	private boolean snConvulsaoPai;
	/** Antecedentes Pessoal (char(1)) */
	private boolean snConvulsaoMae;
	/** Antecedentes Pessoal (char(1)) */
	private boolean snConvulsaoIrmaos;
	/** Antecedentes Pessoal (char(1)) */
	private boolean snConvulsaoAvos;
	/** Antecedentes Pessoal (char(1)) */
	private boolean snLentesContatoOculos;
	/** Antecedentes Pessoal (char(1)) */
	private boolean snBronquiteAsmaTosse;
	/** Antecedentes Pessoal (char(1)) */
	private boolean snProblemaAudicao;
	/** Antecedentes Pessoal (char(1)) */
	private boolean snDoencaContagiosas;
	/** Antecedentes Pessoal (char(1)) */
	private boolean snDoencaCardiaca;
	/** Antecedentes Pessoal (char(1)) */
	private boolean snPressaoAlta;
	/** Antecedentes Pessoal (char(1)) */
	private boolean snDoencaMental;
	/** Antecedentes Pessoal (char(1)) */
	private boolean snDorPeito;
	/** Antecedentes Pessoal (char(1)) */
	private boolean snDisturbioFala;
	/** Antecedentes Pessoal (char(1)) */
	private boolean snVarizes;
	/** Antecedentes Pessoal (char(1)) */
	private boolean snHemorroidas;
	/** Antecedentes Pessoal (char(1)) */
	private boolean snHernias;
	/** Antecedentes Pessoal (char(1)) */
	private boolean snDificuldadeTarefasPesadas;
	/** Antecedentes Pessoal (char(1)) */
	private boolean snResfriadosFrequentes;
	/** Antecedentes Pessoal (char(1)) */
	private boolean snDoencaTireoide;
	/** Antecedentes Pessoal (char(1)) */
	private boolean snProblemasDentarios;
	/** Antecedentes Pessoal (char(1)) */
	private boolean snFomigamentoDormencia;
	/** Antecedentes Pessoal (char(1)) */
	private boolean snAnteracaoSono;
	/** Antecedentes Pessoal (char(1)) */
	private boolean snTonturaVertigem;
	/** Antecedentes Pessoal (char(1)) */
	private boolean snProblemasPeso;
	/** Antecedentes Pessoal (char(1)) */
	private boolean snAlergia;
	/** Antecedentes Pessoal (char(1)) */
	private boolean snDorDeCabeca;
	/** Antecedentes Pessoal (char(1)) */
	private boolean snZumbido;
	/** Antecedentes Pessoal (char(1)) */
	private boolean snDoencaEstomago;
	/** Antecedentes Pessoal (char(1)) */
	private boolean snDoencaColunaDorCosta;
	/** Antecedentes Pessoal (char(1)) */
	private boolean snDoencaSangue;
	/** Antecedentes Pessoal (char(1)) */
	private boolean snDoencaOuvido;
	/** Antecedentes Pessoal (char(1)) */
	private boolean snDorArticulacoes;
	/** Antecedentes Pessoal (char(1)) */
	private boolean snDorMuscular;
	/** Antecedentes Pessoal (char(1)) */
	private boolean snNervossimo;
	/** Antecedentes Pessoal (char(1)) */
	private boolean snDiareiasFrequentes;
	/** Antecedentes Pessoal (char(1)) */
	private boolean snDoencaPele;
	/** Antecedentes Pessoal (char(1)) */
	private boolean snAfastamentoDoenca;
	/** Antecedentes Pessoal (char(1)) */
	private boolean snPortadorDeficiencia;
	/** Antecedentes Pessoal (varchar(250)) */
	private String dsDefinicaoDeficiencia;
	/** 
	 * Antecedentes Pessoal (char(1))
	 * Tipo de frequencia o uso do medimento
	 * D - Diariamente
	 * S - Semanalmente
	 * M - Mensalmente
	 * 
	 */
	private String tpFrequenciaUsoMedicamentos;
	/** Antecedentes Pessoal (char(1)) */
	private boolean snInternado;
	/** Antecedentes Pessoal (varchar(250)) */
	private String dsMotivoInternacao;
	/** Antecedentes Pessoal (varchar(250)) */
	private String dsCirurgias;
	/** Antecedentes Pessoal */
	private String dsOutrasDoencas;
	/** Antecedentes Ocupacionais (char(1)) */
	private boolean snDoencaAcidenteInss;
	/** Antecedentes Ocupacionais (char(1)) */
	private boolean snAcidenteTrabalho;
	/** Antecedentes Ocupacionais (char(1)) */
	private boolean snProdutoQuimico;
	/** Antecedentes Ocupacionais (char(1)) */
	private boolean snLocalFrio;
	/** Antecedentes Ocupacionais (char(1)) */
	private boolean snLocalPoeira;
	/** Antecedentes Ocupacionais (char(1)) */
	private boolean snLocalCalor;
	/** Antecedentes Ocupacionais (char(1)) */
	private boolean snLocalBarulho;
	/** Antecedentes Ocupacionais (varchar(250)) */
	private String dsUltimaOcupacao;
	/** Antecedentes Ocupacionais (varchar(250)) */
	private String dsTempoOcupacaoUltimo;
	/** Antecedentes Ocupacionais (varchar(250)) */
	private String dsPenultimaOcupacao;
	/** Antecedentes Ocupacionais (varchar(250)) */
	private String dsTempoOcupacaoPenultimo;
	/** Habitos de Vida (varchar(250)) */
	private String dsOutrasAtividades;
	/** Habitos de Vida (varchar(250)) */
	private String dsEsportePraticados;
	/** Habitos de Vida (varchar(250)) */
	private String dsHobbyLazer;
	/** Area de Comentarios */
	private String dsComentarios;

	/**
	 * Alimenta Numero do exame
	 * @param nrExame Numero do exame
	 */ 
	public void setNrExame( String nrExame ) {
		this.nrExame = nrExame;
	}

	/**
	 * Retorna Numero do exame
	 * @return Numero do exame
	 */
	public String getNrExame() {
		return this.nrExame;
	}

	/**
	 * Alimenta ID_SETOR
	 * @param idSetor ID_SETOR
	 */ 
	public void setIdSetor( Integer idSetor ) {
		this.idSetor = idSetor;
	}

	/**
	 * Retorna ID_SETOR
	 * @return ID_SETOR
	 */
	public Integer getIdSetor() {
		return this.idSetor;
	}

	/**
	 * Alimenta ID_FUNCAO
	 * @param idFuncao ID_FUNCAO
	 */ 
	public void setIdFuncao( Integer idFuncao ) {
		this.idFuncao = idFuncao;
	}

	/**
	 * Retorna ID_FUNCAO
	 * @return ID_FUNCAO
	 */
	public Integer getIdFuncao() {
		return this.idFuncao;
	}

	/**
	 * Alimenta ID_CARGO
	 * @param idCargo ID_CARGO
	 */ 
	public void setIdCargo( Integer idCargo ) {
		this.idCargo = idCargo;
	}

	/**
	 * Retorna ID_CARGO
	 * @return ID_CARGO
	 */
	public Integer getIdCargo() {
		return this.idCargo;
	}

	/**
	 * Alimenta CD_CBO
	 * @param cdCbo CD_CBO
	 */ 
	public void setCdCbo( String cdCbo ) {
		this.cdCbo = cdCbo;
	}

	/**
	 * Retorna CD_CBO
	 * @return CD_CBO
	 */
	public String getCdCbo() {
		return this.cdCbo;
	}

	/**
	 * Alimenta Medico examinador
	 * @param cdMedicoExaminador Medico examinador
	 */ 
	public void setCdMedicoExaminador( String cdMedicoExaminador ) {
		this.cdMedicoExaminador = cdMedicoExaminador;
	}

	/**
	 * Retorna Medico examinador
	 * @return Medico examinador
	 */
	public String getCdMedicoExaminador() {
		return this.cdMedicoExaminador;
	}

	/**
	 * Alimenta Codigo do benenficiario
	 * @param cdBeneficiarioCartao Codigo do benenficiario
	 */ 
	public void setCdBeneficiarioCartao( String cdBeneficiarioCartao ) {
		this.cdBeneficiarioCartao = cdBeneficiarioCartao;
	}

	/**
	 * Retorna Codigo do benenficiario
	 * @return Codigo do benenficiario
	 */
	public String getCdBeneficiarioCartao() {
		return this.cdBeneficiarioCartao;
	}

	/**
	 * Alimenta Codigo do procedimento
	 * @param cdProcedimento Codigo do procedimento
	 */ 
	public void setCdProcedimento( String cdProcedimento ) {
		this.cdProcedimento = cdProcedimento;
	}

	/**
	 * Retorna Codigo do procedimento
	 * @return Codigo do procedimento
	 */
	public String getCdProcedimento() {
		return this.cdProcedimento;
	}

	/**
	 * Alimenta Tipo de Exame
	 * @param tpExame Tipo de Exame
	 */ 
	public void setTpExame( String tpExame ) {
		this.tpExame = tpExame;
	}

	/**
	 * Retorna Tipo de Exame
	 * @return Tipo de Exame
	 */
	public String getTpExame() {
		return this.tpExame;
	}

	/**
	 * Alimenta Data de emissao
	 * @param dtEmissaoNota Data de emissao
	 */ 
	public void setDtEmissaoNota( QtData dtEmissaoNota ) {
		this.dtEmissaoNota = dtEmissaoNota;
	}

	/**
	 * Retorna Data de emissao
	 * @return Data de emissao
	 */
	public QtData getDtEmissaoNota() {
		return this.dtEmissaoNota;
	}

	/**
	 * Alimenta Data do exame
	 * @param dtExecucaoExame Data do exame
	 */ 
	public void setDtExecucaoExame( QtData dtExecucaoExame ) {
		this.dtExecucaoExame = dtExecucaoExame;
	}

	/**
	 * Retorna Data do exame
	 * @return Data do exame
	 */
	public QtData getDtExecucaoExame() {
		return this.dtExecucaoExame;
	}

	/**
	 * Alimenta Data da execucao
	 * @param dtRetornoNota Data da execucao
	 */ 
	public void setDtRetornoNota( QtData dtRetornoNota ) {
		this.dtRetornoNota = dtRetornoNota;
	}

	/**
	 * Retorna Data da execucao
	 * @return Data da execucao
	 */
	public QtData getDtRetornoNota() {
		return this.dtRetornoNota;
	}

	/**
	 * Alimenta Data do retorno
	 * @param dtCancelamento Data do retorno
	 */ 
	public void setDtCancelamento( QtData dtCancelamento ) {
		this.dtCancelamento = dtCancelamento;
	}

	/**
	 * Retorna Data do retorno
	 * @return Data do retorno
	 */
	public QtData getDtCancelamento() {
		return this.dtCancelamento;
	}

	/**
	 * Alimenta Descri��o da atividade
	 * @param dsAtividade Descri��o da atividade
	 */ 
	public void setDsAtividade( String dsAtividade ) {
		this.dsAtividade = dsAtividade;
	}

	/**
	 * Retorna Descri��o da atividade
	 * @return Descri��o da atividade
	 */
	public String getDsAtividade() {
		return this.dsAtividade;
	}

	/**
	 * Alimenta Valor press�o arterial sist�lica
	 * @param vlPressaoSistolica Valor press�o arterial sist�lica
	 */ 
	public void setVlPressaoSistolica( Integer vlPressaoSistolica ) {
		this.vlPressaoSistolica = vlPressaoSistolica;
	}

	/**
	 * Retorna Valor press�o arterial sist�lica
	 * @return Valor press�o arterial sist�lica
	 */
	public Integer getVlPressaoSistolica() {
		return this.vlPressaoSistolica;
	}

	/**
	 * Alimenta Valor press�o arterial diast�lica
	 * @param vlPressaoDiastolica Valor press�o arterial diast�lica
	 */ 
	public void setVlPressaoDiastolica( Integer vlPressaoDiastolica ) {
		this.vlPressaoDiastolica = vlPressaoDiastolica;
	}

	/**
	 * Retorna Valor press�o arterial diast�lica
	 * @return Valor press�o arterial diast�lica
	 */
	public Integer getVlPressaoDiastolica() {
		return this.vlPressaoDiastolica;
	}

	/**
	 * Alimenta Valor de pulso arterial
	 * @param vlPulsoArterial Valor de pulso arterial
	 */ 
	public void setVlPulsoArterial( Double vlPulsoArterial ) {
		this.vlPulsoArterial = vlPulsoArterial;
	}

	/**
	 * Retorna Valor de pulso arterial
	 * @return Valor de pulso arterial
	 */
	public Double getVlPulsoArterial() {
		return this.vlPulsoArterial;
	}

	/**
	 * Alimenta Valor do peso
	 * @param vlPeso Valor do peso
	 */ 
	public void setVlPeso( Double vlPeso ) {
		this.vlPeso = vlPeso;
	}

	/**
	 * Retorna Valor do peso
	 * @return Valor do peso
	 */
	public Double getVlPeso() {
		return this.vlPeso;
	}

	/**
	 * Alimenta Valor da altura
	 * @param vlAltura Valor da altura
	 */ 
	public void setVlAltura( Double vlAltura ) {
		this.vlAltura = vlAltura;
	}

	/**
	 * Retorna Valor da altura
	 * @return Valor da altura
	 */
	public Double getVlAltura() {
		return this.vlAltura;
	}

	/**
	 * Alimenta Nomolineo
	 * @param snNormolineo Nomolineo
	 */ 
	public void setSnNormolineo( String snNormolineo ) {
		this.snNormolineo = QtString.toBoolean( snNormolineo );
	}

	/**
	 * Retorna Nomolineo
	 * @return Nomolineo
	 */
	public boolean isSnNormolineo() {
		return this.snNormolineo;
	}

	/**
	 * Alimenta Longilineo
	 * @param snLongilineo Longilineo
	 */ 
	public void setSnLongilineo( String snLongilineo ) {
		this.snLongilineo = QtString.toBoolean( snLongilineo );
	}

	/**
	 * Retorna Longilineo
	 * @return Longilineo
	 */
	public boolean isSnLongilineo() {
		return this.snLongilineo;
	}

	/**
	 * Alimenta Brevilineo
	 * @param snBrevilineo Brevilineo
	 */ 
	public void setSnBrevilineo( String snBrevilineo ) {
		this.snBrevilineo = QtString.toBoolean( snBrevilineo );
	}

	/**
	 * Retorna Brevilineo
	 * @return Brevilineo
	 */
	public boolean isSnBrevilineo() {
		return this.snBrevilineo;
	}

	/**
	 * Alimenta Possui diabete
	 * @param snDiabete Possui diabete
	 */ 
	public void setSnDiabete( String snDiabete ) {
		this.snDiabete = QtString.toBoolean( snDiabete );
	}

	/**
	 * Retorna Possui diabete
	 * @return Possui diabete
	 */
	public boolean isSnDiabete() {
		return this.snDiabete;
	}

	/**
	 * Alimenta Possui Hiperten��o
	 * @param snHipertencao Possui Hiperten��o
	 */ 
	public void setSnHipertencao( String snHipertencao ) {
		this.snHipertencao = QtString.toBoolean( snHipertencao );
	}

	/**
	 * Retorna Possui Hiperten��o
	 * @return Possui Hiperten��o
	 */
	public boolean isSnHipertencao() {
		return this.snHipertencao;
	}

	/**
	 * Alimenta Possui Etilista
	 * @param snEtilista Possui Etilista
	 */ 
	public void setSnEtilista( String snEtilista ) {
		this.snEtilista = QtString.toBoolean( snEtilista );
	}

	/**
	 * Retorna Possui Etilista
	 * @return Possui Etilista
	 */
	public boolean isSnEtilista() {
		return this.snEtilista;
	}

	/**
	 * Alimenta Identificador se � Tabagista
	 * @param snTabagista Identificador se � Tabagista
	 */ 
	public void setSnTabagista( String snTabagista ) {
		this.snTabagista = QtString.toBoolean( snTabagista );
	}

	/**
	 * Retorna Identificador se � Tabagista
	 * @return Identificador se � Tabagista
	 */
	public boolean isSnTabagista() {
		return this.snTabagista;
	}

	/**
	 * Alimenta Quantidade de Cigarros por dia
	 * @param qtCigarrosDia Quantidade de Cigarros por dia
	 */ 
	public void setQtCigarrosDia( Integer qtCigarrosDia ) {
		this.qtCigarrosDia = qtCigarrosDia;
	}

	/**
	 * Retorna Quantidade de Cigarros por dia
	 * @return Quantidade de Cigarros por dia
	 */
	public Integer getQtCigarrosDia() {
		return this.qtCigarrosDia;
	}

	/**
	 * Alimenta Descricao de uso de medicamento
	 * @param dsUsoMedicamentos Descricao de uso de medicamento
	 */ 
	public void setDsUsoMedicamentos( String dsUsoMedicamentos ) {
		this.dsUsoMedicamentos = dsUsoMedicamentos;
	}

	/**
	 * Retorna Descricao de uso de medicamento
	 * @return Descricao de uso de medicamento
	 */
	public String getDsUsoMedicamentos() {
		return this.dsUsoMedicamentos;
	}

	/**
	 * Alimenta Sumario da Consulta
	 * @param dsSumarioConsulta Sumario da Consulta
	 */ 
	public void setDsSumarioConsulta( String dsSumarioConsulta ) {
		this.dsSumarioConsulta = dsSumarioConsulta;
	}

	/**
	 * Retorna Sumario da Consulta
	 * @return Sumario da Consulta
	 */
	public String getDsSumarioConsulta() {
		return this.dsSumarioConsulta;
	}

	/**
	 * Alimenta Tipo de concidera��es
	 * @param tpConciderado Tipo de concidera��es
	 */ 
	public void setTpConciderado( String tpConciderado ) {
		this.tpConciderado = tpConciderado;
	}

	/**
	 * Retorna Tipo de concidera��es
	 * @return Tipo de concidera��es
	 */
	public String getTpConciderado() {
		return this.tpConciderado;
	}

	/**
	 * Alimenta Descri��es de Observacoes
	 * @param dsObservacoes Descri��es de Observacoes
	 */ 
	public void setDsObservacoes( String dsObservacoes ) {
		this.dsObservacoes = dsObservacoes;
	}

	/**
	 * Retorna Descri��es de Observacoes
	 * @return Descri��es de Observacoes
	 */
	public String getDsObservacoes() {
		return this.dsObservacoes;
	}

	/**
	 * Alimenta Descri��o do local
	 * @param dsLocal Descri��o do local
	 */ 
	public void setDsLocal( String dsLocal ) {
		this.dsLocal = dsLocal;
	}

	/**
	 * Retorna Descri��o do local
	 * @return Descri��o do local
	 */
	public String getDsLocal() {
		return this.dsLocal;
	}

	/**
	 * Alimenta Antecedentes Familiares
	 * @param snPressaoAltaPai Antecedentes Familiares
	 */ 
	public void setSnPressaoAltaPai( String snPressaoAltaPai ) {
		this.snPressaoAltaPai = QtString.toBoolean( snPressaoAltaPai );
	}

	/**
	 * Retorna Antecedentes Familiares
	 * @return Antecedentes Familiares
	 */
	public boolean isSnPressaoAltaPai() {
		return this.snPressaoAltaPai;
	}

	/**
	 * Alimenta Antecedentes Familiares
	 * @param snPressaoAltaMae Antecedentes Familiares
	 */ 
	public void setSnPressaoAltaMae( String snPressaoAltaMae ) {
		this.snPressaoAltaMae = QtString.toBoolean( snPressaoAltaMae );
	}

	/**
	 * Retorna Antecedentes Familiares
	 * @return Antecedentes Familiares
	 */
	public boolean isSnPressaoAltaMae() {
		return this.snPressaoAltaMae;
	}

	/**
	 * Alimenta Antecedentes Familiares
	 * @param snPressaoAltaIrmaos Antecedentes Familiares
	 */ 
	public void setSnPressaoAltaIrmaos( String snPressaoAltaIrmaos ) {
		this.snPressaoAltaIrmaos = QtString.toBoolean( snPressaoAltaIrmaos );
	}

	/**
	 * Retorna Antecedentes Familiares
	 * @return Antecedentes Familiares
	 */
	public boolean isSnPressaoAltaIrmaos() {
		return this.snPressaoAltaIrmaos;
	}

	/**
	 * Alimenta Antecedentes Familiares
	 * @param snPressaoAltaAvos Antecedentes Familiares
	 */ 
	public void setSnPressaoAltaAvos( String snPressaoAltaAvos ) {
		this.snPressaoAltaAvos = QtString.toBoolean( snPressaoAltaAvos );
	}

	/**
	 * Retorna Antecedentes Familiares
	 * @return Antecedentes Familiares
	 */
	public boolean isSnPressaoAltaAvos() {
		return this.snPressaoAltaAvos;
	}

	/**
	 * Alimenta Antecedentes Familiares
	 * @param snDiabetePai Antecedentes Familiares
	 */ 
	public void setSnDiabetePai( String snDiabetePai ) {
		this.snDiabetePai = QtString.toBoolean( snDiabetePai );
	}

	/**
	 * Retorna Antecedentes Familiares
	 * @return Antecedentes Familiares
	 */
	public boolean isSnDiabetePai() {
		return this.snDiabetePai;
	}

	/**
	 * Alimenta Antecedentes Familiares
	 * @param snDiabeteMae Antecedentes Familiares
	 */ 
	public void setSnDiabeteMae( String snDiabeteMae ) {
		this.snDiabeteMae = QtString.toBoolean( snDiabeteMae );
	}

	/**
	 * Retorna Antecedentes Familiares
	 * @return Antecedentes Familiares
	 */
	public boolean isSnDiabeteMae() {
		return this.snDiabeteMae;
	}

	/**
	 * Alimenta Antecedentes Familiares
	 * @param snDiabeteIrmaos Antecedentes Familiares
	 */ 
	public void setSnDiabeteIrmaos( String snDiabeteIrmaos ) {
		this.snDiabeteIrmaos = QtString.toBoolean( snDiabeteIrmaos );
	}

	/**
	 * Retorna Antecedentes Familiares
	 * @return Antecedentes Familiares
	 */
	public boolean isSnDiabeteIrmaos() {
		return this.snDiabeteIrmaos;
	}

	/**
	 * Alimenta Antecedentes Familiares
	 * @param snDiabeteAvos Antecedentes Familiares
	 */ 
	public void setSnDiabeteAvos( String snDiabeteAvos ) {
		this.snDiabeteAvos = QtString.toBoolean( snDiabeteAvos );
	}

	/**
	 * Retorna Antecedentes Familiares
	 * @return Antecedentes Familiares
	 */
	public boolean isSnDiabeteAvos() {
		return this.snDiabeteAvos;
	}

	/**
	 * Alimenta Antecedentes Familiares
	 * @param snTuberculosePai Antecedentes Familiares
	 */ 
	public void setSnTuberculosePai( String snTuberculosePai ) {
		this.snTuberculosePai = QtString.toBoolean( snTuberculosePai );
	}

	/**
	 * Retorna Antecedentes Familiares
	 * @return Antecedentes Familiares
	 */
	public boolean isSnTuberculosePai() {
		return this.snTuberculosePai;
	}

	/**
	 * Alimenta Antecedentes Familiares
	 * @param snTuberculoseMae Antecedentes Familiares
	 */ 
	public void setSnTuberculoseMae( String snTuberculoseMae ) {
		this.snTuberculoseMae = QtString.toBoolean( snTuberculoseMae );
	}

	/**
	 * Retorna Antecedentes Familiares
	 * @return Antecedentes Familiares
	 */
	public boolean isSnTuberculoseMae() {
		return this.snTuberculoseMae;
	}

	/**
	 * Alimenta Antecedentes Familiares
	 * @param snTuberculoseIrmaos Antecedentes Familiares
	 */ 
	public void setSnTuberculoseIrmaos( String snTuberculoseIrmaos ) {
		this.snTuberculoseIrmaos = QtString.toBoolean( snTuberculoseIrmaos );
	}

	/**
	 * Retorna Antecedentes Familiares
	 * @return Antecedentes Familiares
	 */
	public boolean isSnTuberculoseIrmaos() {
		return this.snTuberculoseIrmaos;
	}

	/**
	 * Alimenta Antecedentes Familiares
	 * @param snTuberculoseAvos Antecedentes Familiares
	 */ 
	public void setSnTuberculoseAvos( String snTuberculoseAvos ) {
		this.snTuberculoseAvos = QtString.toBoolean( snTuberculoseAvos );
	}

	/**
	 * Retorna Antecedentes Familiares
	 * @return Antecedentes Familiares
	 */
	public boolean isSnTuberculoseAvos() {
		return this.snTuberculoseAvos;
	}

	/**
	 * Alimenta Antecedentes Familiares
	 * @param snCancerPai Antecedentes Familiares
	 */ 
	public void setSnCancerPai( String snCancerPai ) {
		this.snCancerPai = QtString.toBoolean( snCancerPai );
	}

	/**
	 * Retorna Antecedentes Familiares
	 * @return Antecedentes Familiares
	 */
	public boolean isSnCancerPai() {
		return this.snCancerPai;
	}

	/**
	 * Alimenta Antecedentes Familiares
	 * @param snCancerMae Antecedentes Familiares
	 */ 
	public void setSnCancerMae( String snCancerMae ) {
		this.snCancerMae = QtString.toBoolean( snCancerMae );
	}

	/**
	 * Retorna Antecedentes Familiares
	 * @return Antecedentes Familiares
	 */
	public boolean isSnCancerMae() {
		return this.snCancerMae;
	}

	/**
	 * Alimenta Antecedentes Familiares
	 * @param snCancerIrmaos Antecedentes Familiares
	 */ 
	public void setSnCancerIrmaos( String snCancerIrmaos ) {
		this.snCancerIrmaos = QtString.toBoolean( snCancerIrmaos );
	}

	/**
	 * Retorna Antecedentes Familiares
	 * @return Antecedentes Familiares
	 */
	public boolean isSnCancerIrmaos() {
		return this.snCancerIrmaos;
	}

	/**
	 * Alimenta Antecedentes Familiares
	 * @param snCancerAvos Antecedentes Familiares
	 */ 
	public void setSnCancerAvos( String snCancerAvos ) {
		this.snCancerAvos = QtString.toBoolean( snCancerAvos );
	}

	/**
	 * Retorna Antecedentes Familiares
	 * @return Antecedentes Familiares
	 */
	public boolean isSnCancerAvos() {
		return this.snCancerAvos;
	}

	/**
	 * Alimenta Antecedentes Pessoal
	 * @param snDoencaCardiacaPai Antecedentes Pessoal
	 */ 
	public void setSnDoencaCardiacaPai( String snDoencaCardiacaPai ) {
		this.snDoencaCardiacaPai = QtString.toBoolean( snDoencaCardiacaPai );
	}

	/**
	 * Retorna Antecedentes Pessoal
	 * @return Antecedentes Pessoal
	 */
	public boolean isSnDoencaCardiacaPai() {
		return this.snDoencaCardiacaPai;
	}

	/**
	 * Alimenta Antecedentes Pessoal
	 * @param snDoencaCardiacaMae Antecedentes Pessoal
	 */ 
	public void setSnDoencaCardiacaMae( String snDoencaCardiacaMae ) {
		this.snDoencaCardiacaMae = QtString.toBoolean( snDoencaCardiacaMae );
	}

	/**
	 * Retorna Antecedentes Pessoal
	 * @return Antecedentes Pessoal
	 */
	public boolean isSnDoencaCardiacaMae() {
		return this.snDoencaCardiacaMae;
	}

	/**
	 * Alimenta Antecedentes Pessoal
	 * @param snDoencaCardiacaIrmaos Antecedentes Pessoal
	 */ 
	public void setSnDoencaCardiacaIrmaos( String snDoencaCardiacaIrmaos ) {
		this.snDoencaCardiacaIrmaos = QtString.toBoolean( snDoencaCardiacaIrmaos );
	}

	/**
	 * Retorna Antecedentes Pessoal
	 * @return Antecedentes Pessoal
	 */
	public boolean isSnDoencaCardiacaIrmaos() {
		return this.snDoencaCardiacaIrmaos;
	}

	/**
	 * Alimenta Antecedentes Pessoal
	 * @param snDoencaCardiacaAvos Antecedentes Pessoal
	 */ 
	public void setSnDoencaCardiacaAvos( String snDoencaCardiacaAvos ) {
		this.snDoencaCardiacaAvos = QtString.toBoolean( snDoencaCardiacaAvos );
	}

	/**
	 * Retorna Antecedentes Pessoal
	 * @return Antecedentes Pessoal
	 */
	public boolean isSnDoencaCardiacaAvos() {
		return this.snDoencaCardiacaAvos;
	}

	/**
	 * Alimenta Antecedentes Pessoal
	 * @param snDoencaMentalPai Antecedentes Pessoal
	 */ 
	public void setSnDoencaMentalPai( String snDoencaMentalPai ) {
		this.snDoencaMentalPai = QtString.toBoolean( snDoencaMentalPai );
	}

	/**
	 * Retorna Antecedentes Pessoal
	 * @return Antecedentes Pessoal
	 */
	public boolean isSnDoencaMentalPai() {
		return this.snDoencaMentalPai;
	}

	/**
	 * Alimenta Antecedentes Pessoal
	 * @param snDoencaMentalMae Antecedentes Pessoal
	 */ 
	public void setSnDoencaMentalMae( String snDoencaMentalMae ) {
		this.snDoencaMentalMae = QtString.toBoolean( snDoencaMentalMae );
	}

	/**
	 * Retorna Antecedentes Pessoal
	 * @return Antecedentes Pessoal
	 */
	public boolean isSnDoencaMentalMae() {
		return this.snDoencaMentalMae;
	}

	/**
	 * Alimenta Antecedentes Pessoal
	 * @param snDoencaMentalIrmaos Antecedentes Pessoal
	 */ 
	public void setSnDoencaMentalIrmaos( String snDoencaMentalIrmaos ) {
		this.snDoencaMentalIrmaos = QtString.toBoolean( snDoencaMentalIrmaos );
	}

	/**
	 * Retorna Antecedentes Pessoal
	 * @return Antecedentes Pessoal
	 */
	public boolean isSnDoencaMentalIrmaos() {
		return this.snDoencaMentalIrmaos;
	}

	/**
	 * Alimenta Antecedentes Pessoal
	 * @param snDoencaMentalAvos Antecedentes Pessoal
	 */ 
	public void setSnDoencaMentalAvos( String snDoencaMentalAvos ) {
		this.snDoencaMentalAvos = QtString.toBoolean( snDoencaMentalAvos );
	}

	/**
	 * Retorna Antecedentes Pessoal
	 * @return Antecedentes Pessoal
	 */
	public boolean isSnDoencaMentalAvos() {
		return this.snDoencaMentalAvos;
	}

	/**
	 * Alimenta Antecedentes Pessoal
	 * @param snAsmaAlergiaUrticariaPai Antecedentes Pessoal
	 */ 
	public void setSnAsmaAlergiaUrticariaPai( String snAsmaAlergiaUrticariaPai ) {
		this.snAsmaAlergiaUrticariaPai = QtString.toBoolean( snAsmaAlergiaUrticariaPai );
	}

	/**
	 * Retorna Antecedentes Pessoal
	 * @return Antecedentes Pessoal
	 */
	public boolean isSnAsmaAlergiaUrticariaPai() {
		return this.snAsmaAlergiaUrticariaPai;
	}

	/**
	 * Alimenta Antecedentes Pessoal
	 * @param snAsmaAlergiaUrticariaMae Antecedentes Pessoal
	 */ 
	public void setSnAsmaAlergiaUrticariaMae( String snAsmaAlergiaUrticariaMae ) {
		this.snAsmaAlergiaUrticariaMae = QtString.toBoolean( snAsmaAlergiaUrticariaMae );
	}

	/**
	 * Retorna Antecedentes Pessoal
	 * @return Antecedentes Pessoal
	 */
	public boolean isSnAsmaAlergiaUrticariaMae() {
		return this.snAsmaAlergiaUrticariaMae;
	}

	/**
	 * Alimenta Antecedentes Pessoal
	 * @param snAsmaAlergiaUrticariaIrmaos Antecedentes Pessoal
	 */ 
	public void setSnAsmaAlergiaUrticariaIrmaos( String snAsmaAlergiaUrticariaIrmaos ) {
		this.snAsmaAlergiaUrticariaIrmaos = QtString.toBoolean( snAsmaAlergiaUrticariaIrmaos );
	}

	/**
	 * Retorna Antecedentes Pessoal
	 * @return Antecedentes Pessoal
	 */
	public boolean isSnAsmaAlergiaUrticariaIrmaos() {
		return this.snAsmaAlergiaUrticariaIrmaos;
	}

	/**
	 * Alimenta Antecedentes Pessoal
	 * @param snAsmaAlergiaUrticariaAvos Antecedentes Pessoal
	 */ 
	public void setSnAsmaAlergiaUrticariaAvos( String snAsmaAlergiaUrticariaAvos ) {
		this.snAsmaAlergiaUrticariaAvos = QtString.toBoolean( snAsmaAlergiaUrticariaAvos );
	}

	/**
	 * Retorna Antecedentes Pessoal
	 * @return Antecedentes Pessoal
	 */
	public boolean isSnAsmaAlergiaUrticariaAvos() {
		return this.snAsmaAlergiaUrticariaAvos;
	}

	/**
	 * Alimenta Antecedentes Pessoal
	 * @param snConvulsaoPai Antecedentes Pessoal
	 */ 
	public void setSnConvulsaoPai( String snConvulsaoPai ) {
		this.snConvulsaoPai = QtString.toBoolean( snConvulsaoPai );
	}

	/**
	 * Retorna Antecedentes Pessoal
	 * @return Antecedentes Pessoal
	 */
	public boolean isSnConvulsaoPai() {
		return this.snConvulsaoPai;
	}

	/**
	 * Alimenta Antecedentes Pessoal
	 * @param snConvulsaoMae Antecedentes Pessoal
	 */ 
	public void setSnConvulsaoMae( String snConvulsaoMae ) {
		this.snConvulsaoMae = QtString.toBoolean( snConvulsaoMae );
	}

	/**
	 * Retorna Antecedentes Pessoal
	 * @return Antecedentes Pessoal
	 */
	public boolean isSnConvulsaoMae() {
		return this.snConvulsaoMae;
	}

	/**
	 * Alimenta Antecedentes Pessoal
	 * @param snConvulsaoIrmaos Antecedentes Pessoal
	 */ 
	public void setSnConvulsaoIrmaos( String snConvulsaoIrmaos ) {
		this.snConvulsaoIrmaos = QtString.toBoolean( snConvulsaoIrmaos );
	}

	/**
	 * Retorna Antecedentes Pessoal
	 * @return Antecedentes Pessoal
	 */
	public boolean isSnConvulsaoIrmaos() {
		return this.snConvulsaoIrmaos;
	}

	/**
	 * Alimenta Antecedentes Pessoal
	 * @param snConvulsaoAvos Antecedentes Pessoal
	 */ 
	public void setSnConvulsaoAvos( String snConvulsaoAvos ) {
		this.snConvulsaoAvos = QtString.toBoolean( snConvulsaoAvos );
	}

	/**
	 * Retorna Antecedentes Pessoal
	 * @return Antecedentes Pessoal
	 */
	public boolean isSnConvulsaoAvos() {
		return this.snConvulsaoAvos;
	}

	/**
	 * Alimenta Antecedentes Pessoal
	 * @param snLentesContatoOculos Antecedentes Pessoal
	 */ 
	public void setSnLentesContatoOculos( String snLentesContatoOculos ) {
		this.snLentesContatoOculos = QtString.toBoolean( snLentesContatoOculos );
	}

	/**
	 * Retorna Antecedentes Pessoal
	 * @return Antecedentes Pessoal
	 */
	public boolean isSnLentesContatoOculos() {
		return this.snLentesContatoOculos;
	}

	/**
	 * Alimenta Antecedentes Pessoal
	 * @param snBronquiteAsmaTosse Antecedentes Pessoal
	 */ 
	public void setSnBronquiteAsmaTosse( String snBronquiteAsmaTosse ) {
		this.snBronquiteAsmaTosse = QtString.toBoolean( snBronquiteAsmaTosse );
	}

	/**
	 * Retorna Antecedentes Pessoal
	 * @return Antecedentes Pessoal
	 */
	public boolean isSnBronquiteAsmaTosse() {
		return this.snBronquiteAsmaTosse;
	}

	/**
	 * Alimenta Antecedentes Pessoal
	 * @param snProblemaAudicao Antecedentes Pessoal
	 */ 
	public void setSnProblemaAudicao( String snProblemaAudicao ) {
		this.snProblemaAudicao = QtString.toBoolean( snProblemaAudicao );
	}

	/**
	 * Retorna Antecedentes Pessoal
	 * @return Antecedentes Pessoal
	 */
	public boolean isSnProblemaAudicao() {
		return this.snProblemaAudicao;
	}

	/**
	 * Alimenta Antecedentes Pessoal
	 * @param snDoencaContagiosas Antecedentes Pessoal
	 */ 
	public void setSnDoencaContagiosas( String snDoencaContagiosas ) {
		this.snDoencaContagiosas = QtString.toBoolean( snDoencaContagiosas );
	}

	/**
	 * Retorna Antecedentes Pessoal
	 * @return Antecedentes Pessoal
	 */
	public boolean isSnDoencaContagiosas() {
		return this.snDoencaContagiosas;
	}

	/**
	 * Alimenta Antecedentes Pessoal
	 * @param snDoencaCardiaca Antecedentes Pessoal
	 */ 
	public void setSnDoencaCardiaca( String snDoencaCardiaca ) {
		this.snDoencaCardiaca = QtString.toBoolean( snDoencaCardiaca );
	}

	/**
	 * Retorna Antecedentes Pessoal
	 * @return Antecedentes Pessoal
	 */
	public boolean isSnDoencaCardiaca() {
		return this.snDoencaCardiaca;
	}

	/**
	 * Alimenta Antecedentes Pessoal
	 * @param snPressaoAlta Antecedentes Pessoal
	 */ 
	public void setSnPressaoAlta( String snPressaoAlta ) {
		this.snPressaoAlta = QtString.toBoolean( snPressaoAlta );
	}

	/**
	 * Retorna Antecedentes Pessoal
	 * @return Antecedentes Pessoal
	 */
	public boolean isSnPressaoAlta() {
		return this.snPressaoAlta;
	}

	/**
	 * Alimenta Antecedentes Pessoal
	 * @param snDoencaMental Antecedentes Pessoal
	 */ 
	public void setSnDoencaMental( String snDoencaMental ) {
		this.snDoencaMental = QtString.toBoolean( snDoencaMental );
	}

	/**
	 * Retorna Antecedentes Pessoal
	 * @return Antecedentes Pessoal
	 */
	public boolean isSnDoencaMental() {
		return this.snDoencaMental;
	}

	/**
	 * Alimenta Antecedentes Pessoal
	 * @param snDorPeito Antecedentes Pessoal
	 */ 
	public void setSnDorPeito( String snDorPeito ) {
		this.snDorPeito = QtString.toBoolean( snDorPeito );
	}

	/**
	 * Retorna Antecedentes Pessoal
	 * @return Antecedentes Pessoal
	 */
	public boolean isSnDorPeito() {
		return this.snDorPeito;
	}

	/**
	 * Alimenta Antecedentes Pessoal
	 * @param snDisturbioFala Antecedentes Pessoal
	 */ 
	public void setSnDisturbioFala( String snDisturbioFala ) {
		this.snDisturbioFala = QtString.toBoolean( snDisturbioFala );
	}

	/**
	 * Retorna Antecedentes Pessoal
	 * @return Antecedentes Pessoal
	 */
	public boolean isSnDisturbioFala() {
		return this.snDisturbioFala;
	}

	/**
	 * Alimenta Antecedentes Pessoal
	 * @param snVarizes Antecedentes Pessoal
	 */ 
	public void setSnVarizes( String snVarizes ) {
		this.snVarizes = QtString.toBoolean( snVarizes );
	}

	/**
	 * Retorna Antecedentes Pessoal
	 * @return Antecedentes Pessoal
	 */
	public boolean isSnVarizes() {
		return this.snVarizes;
	}

	/**
	 * Alimenta Antecedentes Pessoal
	 * @param snHemorroidas Antecedentes Pessoal
	 */ 
	public void setSnHemorroidas( String snHemorroidas ) {
		this.snHemorroidas = QtString.toBoolean( snHemorroidas );
	}

	/**
	 * Retorna Antecedentes Pessoal
	 * @return Antecedentes Pessoal
	 */
	public boolean isSnHemorroidas() {
		return this.snHemorroidas;
	}

	/**
	 * Alimenta Antecedentes Pessoal
	 * @param snHernias Antecedentes Pessoal
	 */ 
	public void setSnHernias( String snHernias ) {
		this.snHernias = QtString.toBoolean( snHernias );
	}

	/**
	 * Retorna Antecedentes Pessoal
	 * @return Antecedentes Pessoal
	 */
	public boolean isSnHernias() {
		return this.snHernias;
	}

	/**
	 * Alimenta Antecedentes Pessoal
	 * @param snDificuldadeTarefasPesadas Antecedentes Pessoal
	 */ 
	public void setSnDificuldadeTarefasPesadas( String snDificuldadeTarefasPesadas ) {
		this.snDificuldadeTarefasPesadas = QtString.toBoolean( snDificuldadeTarefasPesadas );
	}

	/**
	 * Retorna Antecedentes Pessoal
	 * @return Antecedentes Pessoal
	 */
	public boolean isSnDificuldadeTarefasPesadas() {
		return this.snDificuldadeTarefasPesadas;
	}

	/**
	 * Alimenta Antecedentes Pessoal
	 * @param snResfriadosFrequentes Antecedentes Pessoal
	 */ 
	public void setSnResfriadosFrequentes( String snResfriadosFrequentes ) {
		this.snResfriadosFrequentes = QtString.toBoolean( snResfriadosFrequentes );
	}

	/**
	 * Retorna Antecedentes Pessoal
	 * @return Antecedentes Pessoal
	 */
	public boolean isSnResfriadosFrequentes() {
		return this.snResfriadosFrequentes;
	}

	/**
	 * Alimenta Antecedentes Pessoal
	 * @param snDoencaTireoide Antecedentes Pessoal
	 */ 
	public void setSnDoencaTireoide( String snDoencaTireoide ) {
		this.snDoencaTireoide = QtString.toBoolean( snDoencaTireoide );
	}

	/**
	 * Retorna Antecedentes Pessoal
	 * @return Antecedentes Pessoal
	 */
	public boolean isSnDoencaTireoide() {
		return this.snDoencaTireoide;
	}

	/**
	 * Alimenta Antecedentes Pessoal
	 * @param snProblemasDentarios Antecedentes Pessoal
	 */ 
	public void setSnProblemasDentarios( String snProblemasDentarios ) {
		this.snProblemasDentarios = QtString.toBoolean( snProblemasDentarios );
	}

	/**
	 * Retorna Antecedentes Pessoal
	 * @return Antecedentes Pessoal
	 */
	public boolean isSnProblemasDentarios() {
		return this.snProblemasDentarios;
	}

	/**
	 * Alimenta Antecedentes Pessoal
	 * @param snFomigamentoDormencia Antecedentes Pessoal
	 */ 
	public void setSnFomigamentoDormencia( String snFomigamentoDormencia ) {
		this.snFomigamentoDormencia = QtString.toBoolean( snFomigamentoDormencia );
	}

	/**
	 * Retorna Antecedentes Pessoal
	 * @return Antecedentes Pessoal
	 */
	public boolean isSnFomigamentoDormencia() {
		return this.snFomigamentoDormencia;
	}

	/**
	 * Alimenta Antecedentes Pessoal
	 * @param snAnteracaoSono Antecedentes Pessoal
	 */ 
	public void setSnAnteracaoSono( String snAnteracaoSono ) {
		this.snAnteracaoSono = QtString.toBoolean( snAnteracaoSono );
	}

	/**
	 * Retorna Antecedentes Pessoal
	 * @return Antecedentes Pessoal
	 */
	public boolean isSnAnteracaoSono() {
		return this.snAnteracaoSono;
	}

	/**
	 * Alimenta Antecedentes Pessoal
	 * @param snTonturaVertigem Antecedentes Pessoal
	 */ 
	public void setSnTonturaVertigem( String snTonturaVertigem ) {
		this.snTonturaVertigem = QtString.toBoolean( snTonturaVertigem );
	}

	/**
	 * Retorna Antecedentes Pessoal
	 * @return Antecedentes Pessoal
	 */
	public boolean isSnTonturaVertigem() {
		return this.snTonturaVertigem;
	}

	/**
	 * Alimenta Antecedentes Pessoal
	 * @param snProblemasPeso Antecedentes Pessoal
	 */ 
	public void setSnProblemasPeso( String snProblemasPeso ) {
		this.snProblemasPeso = QtString.toBoolean( snProblemasPeso );
	}

	/**
	 * Retorna Antecedentes Pessoal
	 * @return Antecedentes Pessoal
	 */
	public boolean isSnProblemasPeso() {
		return this.snProblemasPeso;
	}

	/**
	 * Alimenta Antecedentes Pessoal
	 * @param snAlergia Antecedentes Pessoal
	 */ 
	public void setSnAlergia( String snAlergia ) {
		this.snAlergia = QtString.toBoolean( snAlergia );
	}

	/**
	 * Retorna Antecedentes Pessoal
	 * @return Antecedentes Pessoal
	 */
	public boolean isSnAlergia() {
		return this.snAlergia;
	}

	/**
	 * Alimenta Antecedentes Pessoal
	 * @param snDorDeCabeca Antecedentes Pessoal
	 */ 
	public void setSnDorDeCabeca( String snDorDeCabeca ) {
		this.snDorDeCabeca = QtString.toBoolean( snDorDeCabeca );
	}

	/**
	 * Retorna Antecedentes Pessoal
	 * @return Antecedentes Pessoal
	 */
	public boolean isSnDorDeCabeca() {
		return this.snDorDeCabeca;
	}

	/**
	 * Alimenta Antecedentes Pessoal
	 * @param snZumbido Antecedentes Pessoal
	 */ 
	public void setSnZumbido( String snZumbido ) {
		this.snZumbido = QtString.toBoolean( snZumbido );
	}

	/**
	 * Retorna Antecedentes Pessoal
	 * @return Antecedentes Pessoal
	 */
	public boolean isSnZumbido() {
		return this.snZumbido;
	}

	/**
	 * Alimenta Antecedentes Pessoal
	 * @param snDoencaEstomago Antecedentes Pessoal
	 */ 
	public void setSnDoencaEstomago( String snDoencaEstomago ) {
		this.snDoencaEstomago = QtString.toBoolean( snDoencaEstomago );
	}

	/**
	 * Retorna Antecedentes Pessoal
	 * @return Antecedentes Pessoal
	 */
	public boolean isSnDoencaEstomago() {
		return this.snDoencaEstomago;
	}

	/**
	 * Alimenta Antecedentes Pessoal
	 * @param snDoencaColunaDorCosta Antecedentes Pessoal
	 */ 
	public void setSnDoencaColunaDorCosta( String snDoencaColunaDorCosta ) {
		this.snDoencaColunaDorCosta = QtString.toBoolean( snDoencaColunaDorCosta );
	}

	/**
	 * Retorna Antecedentes Pessoal
	 * @return Antecedentes Pessoal
	 */
	public boolean isSnDoencaColunaDorCosta() {
		return this.snDoencaColunaDorCosta;
	}

	/**
	 * Alimenta Antecedentes Pessoal
	 * @param snDoencaSangue Antecedentes Pessoal
	 */ 
	public void setSnDoencaSangue( String snDoencaSangue ) {
		this.snDoencaSangue = QtString.toBoolean( snDoencaSangue );
	}

	/**
	 * Retorna Antecedentes Pessoal
	 * @return Antecedentes Pessoal
	 */
	public boolean isSnDoencaSangue() {
		return this.snDoencaSangue;
	}

	/**
	 * Alimenta Antecedentes Pessoal
	 * @param snDoencaOuvido Antecedentes Pessoal
	 */ 
	public void setSnDoencaOuvido( String snDoencaOuvido ) {
		this.snDoencaOuvido = QtString.toBoolean( snDoencaOuvido );
	}

	/**
	 * Retorna Antecedentes Pessoal
	 * @return Antecedentes Pessoal
	 */
	public boolean isSnDoencaOuvido() {
		return this.snDoencaOuvido;
	}

	/**
	 * Alimenta Antecedentes Pessoal
	 * @param snDorArticulacoes Antecedentes Pessoal
	 */ 
	public void setSnDorArticulacoes( String snDorArticulacoes ) {
		this.snDorArticulacoes = QtString.toBoolean( snDorArticulacoes );
	}

	/**
	 * Retorna Antecedentes Pessoal
	 * @return Antecedentes Pessoal
	 */
	public boolean isSnDorArticulacoes() {
		return this.snDorArticulacoes;
	}

	/**
	 * Alimenta Antecedentes Pessoal
	 * @param snDorMuscular Antecedentes Pessoal
	 */ 
	public void setSnDorMuscular( String snDorMuscular ) {
		this.snDorMuscular = QtString.toBoolean( snDorMuscular );
	}

	/**
	 * Retorna Antecedentes Pessoal
	 * @return Antecedentes Pessoal
	 */
	public boolean isSnDorMuscular() {
		return this.snDorMuscular;
	}

	/**
	 * Alimenta Antecedentes Pessoal
	 * @param snNervossimo Antecedentes Pessoal
	 */ 
	public void setSnNervossimo( String snNervossimo ) {
		this.snNervossimo = QtString.toBoolean( snNervossimo );
	}

	/**
	 * Retorna Antecedentes Pessoal
	 * @return Antecedentes Pessoal
	 */
	public boolean isSnNervossimo() {
		return this.snNervossimo;
	}

	/**
	 * Alimenta Antecedentes Pessoal
	 * @param snDiareiasFrequentes Antecedentes Pessoal
	 */ 
	public void setSnDiareiasFrequentes( String snDiareiasFrequentes ) {
		this.snDiareiasFrequentes = QtString.toBoolean( snDiareiasFrequentes );
	}

	/**
	 * Retorna Antecedentes Pessoal
	 * @return Antecedentes Pessoal
	 */
	public boolean isSnDiareiasFrequentes() {
		return this.snDiareiasFrequentes;
	}

	/**
	 * Alimenta Antecedentes Pessoal
	 * @param snDoencaPele Antecedentes Pessoal
	 */ 
	public void setSnDoencaPele( String snDoencaPele ) {
		this.snDoencaPele = QtString.toBoolean( snDoencaPele );
	}

	/**
	 * Retorna Antecedentes Pessoal
	 * @return Antecedentes Pessoal
	 */
	public boolean isSnDoencaPele() {
		return this.snDoencaPele;
	}

	/**
	 * Alimenta Antecedentes Pessoal
	 * @param snAfastamentoDoenca Antecedentes Pessoal
	 */ 
	public void setSnAfastamentoDoenca( String snAfastamentoDoenca ) {
		this.snAfastamentoDoenca = QtString.toBoolean( snAfastamentoDoenca );
	}

	/**
	 * Retorna Antecedentes Pessoal
	 * @return Antecedentes Pessoal
	 */
	public boolean isSnAfastamentoDoenca() {
		return this.snAfastamentoDoenca;
	}

	/**
	 * Alimenta Antecedentes Pessoal
	 * @param snPortadorDeficiencia Antecedentes Pessoal
	 */ 
	public void setSnPortadorDeficiencia( String snPortadorDeficiencia ) {
		this.snPortadorDeficiencia = QtString.toBoolean( snPortadorDeficiencia );
	}

	/**
	 * Retorna Antecedentes Pessoal
	 * @return Antecedentes Pessoal
	 */
	public boolean isSnPortadorDeficiencia() {
		return this.snPortadorDeficiencia;
	}

	/**
	 * Alimenta Antecedentes Pessoal
	 * @param dsDefinicaoDeficiencia Antecedentes Pessoal
	 */ 
	public void setDsDefinicaoDeficiencia( String dsDefinicaoDeficiencia ) {
		this.dsDefinicaoDeficiencia = dsDefinicaoDeficiencia;
	}

	/**
	 * Retorna Antecedentes Pessoal
	 * @return Antecedentes Pessoal
	 */
	public String getDsDefinicaoDeficiencia() {
		return this.dsDefinicaoDeficiencia;
	}

	/**
	 * Alimenta Antecedentes Pessoal
	 * @param tpFrequenciaUsoMedicamentos Antecedentes Pessoal
	 */ 
	public void setTpFrequenciaUsoMedicamentos( String tpFrequenciaUsoMedicamentos ) {
		this.tpFrequenciaUsoMedicamentos = tpFrequenciaUsoMedicamentos;
	}

	/**
	 * Retorna Antecedentes Pessoal
	 * @return Antecedentes Pessoal
	 */
	public String getTpFrequenciaUsoMedicamentos() {
		return this.tpFrequenciaUsoMedicamentos;
	}

	/**
	 * Alimenta Antecedentes Pessoal
	 * @param snInternado Antecedentes Pessoal
	 */ 
	public void setSnInternado( String snInternado ) {
		this.snInternado = QtString.toBoolean( snInternado );
	}

	/**
	 * Retorna Antecedentes Pessoal
	 * @return Antecedentes Pessoal
	 */
	public boolean isSnInternado() {
		return this.snInternado;
	}

	/**
	 * Alimenta Antecedentes Pessoal
	 * @param dsMotivoInternacao Antecedentes Pessoal
	 */ 
	public void setDsMotivoInternacao( String dsMotivoInternacao ) {
		this.dsMotivoInternacao = dsMotivoInternacao;
	}

	/**
	 * Retorna Antecedentes Pessoal
	 * @return Antecedentes Pessoal
	 */
	public String getDsMotivoInternacao() {
		return this.dsMotivoInternacao;
	}

	/**
	 * Alimenta Antecedentes Pessoal
	 * @param dsCirurgias Antecedentes Pessoal
	 */ 
	public void setDsCirurgias( String dsCirurgias ) {
		this.dsCirurgias = dsCirurgias;
	}

	/**
	 * Retorna Antecedentes Pessoal
	 * @return Antecedentes Pessoal
	 */
	public String getDsCirurgias() {
		return this.dsCirurgias;
	}

	/**
	 * Alimenta Antecedentes Pessoal
	 * @param dsOutrasDoencas Antecedentes Pessoal
	 */ 
	public void setDsOutrasDoencas( String dsOutrasDoencas ) {
		this.dsOutrasDoencas = dsOutrasDoencas;
	}

	/**
	 * Retorna Antecedentes Pessoal
	 * @return Antecedentes Pessoal
	 */
	public String getDsOutrasDoencas() {
		return this.dsOutrasDoencas;
	}

	/**
	 * Alimenta Antecedentes Ocupacionais
	 * @param snDoencaAcidenteInss Antecedentes Ocupacionais
	 */ 
	public void setSnDoencaAcidenteInss( String snDoencaAcidenteInss ) {
		this.snDoencaAcidenteInss = QtString.toBoolean( snDoencaAcidenteInss );
	}

	/**
	 * Retorna Antecedentes Ocupacionais
	 * @return Antecedentes Ocupacionais
	 */
	public boolean isSnDoencaAcidenteInss() {
		return this.snDoencaAcidenteInss;
	}

	/**
	 * Alimenta Antecedentes Ocupacionais
	 * @param snAcidenteTrabalho Antecedentes Ocupacionais
	 */ 
	public void setSnAcidenteTrabalho( String snAcidenteTrabalho ) {
		this.snAcidenteTrabalho = QtString.toBoolean( snAcidenteTrabalho );
	}

	/**
	 * Retorna Antecedentes Ocupacionais
	 * @return Antecedentes Ocupacionais
	 */
	public boolean isSnAcidenteTrabalho() {
		return this.snAcidenteTrabalho;
	}

	/**
	 * Alimenta Antecedentes Ocupacionais
	 * @param snProdutoQuimico Antecedentes Ocupacionais
	 */ 
	public void setSnProdutoQuimico( String snProdutoQuimico ) {
		this.snProdutoQuimico = QtString.toBoolean( snProdutoQuimico );
	}

	/**
	 * Retorna Antecedentes Ocupacionais
	 * @return Antecedentes Ocupacionais
	 */
	public boolean isSnProdutoQuimico() {
		return this.snProdutoQuimico;
	}

	/**
	 * Alimenta Antecedentes Ocupacionais
	 * @param snLocalFrio Antecedentes Ocupacionais
	 */ 
	public void setSnLocalFrio( String snLocalFrio ) {
		this.snLocalFrio = QtString.toBoolean( snLocalFrio );
	}

	/**
	 * Retorna Antecedentes Ocupacionais
	 * @return Antecedentes Ocupacionais
	 */
	public boolean isSnLocalFrio() {
		return this.snLocalFrio;
	}

	/**
	 * Alimenta Antecedentes Ocupacionais
	 * @param snLocalPoeira Antecedentes Ocupacionais
	 */ 
	public void setSnLocalPoeira( String snLocalPoeira ) {
		this.snLocalPoeira = QtString.toBoolean( snLocalPoeira );
	}

	/**
	 * Retorna Antecedentes Ocupacionais
	 * @return Antecedentes Ocupacionais
	 */
	public boolean isSnLocalPoeira() {
		return this.snLocalPoeira;
	}

	/**
	 * Alimenta Antecedentes Ocupacionais
	 * @param snLocalCalor Antecedentes Ocupacionais
	 */ 
	public void setSnLocalCalor( String snLocalCalor ) {
		this.snLocalCalor = QtString.toBoolean( snLocalCalor );
	}

	/**
	 * Retorna Antecedentes Ocupacionais
	 * @return Antecedentes Ocupacionais
	 */
	public boolean isSnLocalCalor() {
		return this.snLocalCalor;
	}

	/**
	 * Alimenta Antecedentes Ocupacionais
	 * @param snLocalBarulho Antecedentes Ocupacionais
	 */ 
	public void setSnLocalBarulho( String snLocalBarulho ) {
		this.snLocalBarulho = QtString.toBoolean( snLocalBarulho );
	}

	/**
	 * Retorna Antecedentes Ocupacionais
	 * @return Antecedentes Ocupacionais
	 */
	public boolean isSnLocalBarulho() {
		return this.snLocalBarulho;
	}

	/**
	 * Alimenta Antecedentes Ocupacionais
	 * @param dsUltimaOcupacao Antecedentes Ocupacionais
	 */ 
	public void setDsUltimaOcupacao( String dsUltimaOcupacao ) {
		this.dsUltimaOcupacao = dsUltimaOcupacao;
	}

	/**
	 * Retorna Antecedentes Ocupacionais
	 * @return Antecedentes Ocupacionais
	 */
	public String getDsUltimaOcupacao() {
		return this.dsUltimaOcupacao;
	}

	/**
	 * Alimenta Antecedentes Ocupacionais
	 * @param dsTempoOcupacaoUltimo Antecedentes Ocupacionais
	 */ 
	public void setDsTempoOcupacaoUltimo( String dsTempoOcupacaoUltimo ) {
		this.dsTempoOcupacaoUltimo = dsTempoOcupacaoUltimo;
	}

	/**
	 * Retorna Antecedentes Ocupacionais
	 * @return Antecedentes Ocupacionais
	 */
	public String getDsTempoOcupacaoUltimo() {
		return this.dsTempoOcupacaoUltimo;
	}

	/**
	 * Alimenta Antecedentes Ocupacionais
	 * @param dsPenultimaOcupacao Antecedentes Ocupacionais
	 */ 
	public void setDsPenultimaOcupacao( String dsPenultimaOcupacao ) {
		this.dsPenultimaOcupacao = dsPenultimaOcupacao;
	}

	/**
	 * Retorna Antecedentes Ocupacionais
	 * @return Antecedentes Ocupacionais
	 */
	public String getDsPenultimaOcupacao() {
		return this.dsPenultimaOcupacao;
	}

	/**
	 * Alimenta Antecedentes Ocupacionais
	 * @param dsTempoOcupacaoPenultimo Antecedentes Ocupacionais
	 */ 
	public void setDsTempoOcupacaoPenultimo( String dsTempoOcupacaoPenultimo ) {
		this.dsTempoOcupacaoPenultimo = dsTempoOcupacaoPenultimo;
	}

	/**
	 * Retorna Antecedentes Ocupacionais
	 * @return Antecedentes Ocupacionais
	 */
	public String getDsTempoOcupacaoPenultimo() {
		return this.dsTempoOcupacaoPenultimo;
	}

	/**
	 * Alimenta Habitos de Vida
	 * @param dsOutrasAtividades Habitos de Vida
	 */ 
	public void setDsOutrasAtividades( String dsOutrasAtividades ) {
		this.dsOutrasAtividades = dsOutrasAtividades;
	}

	/**
	 * Retorna Habitos de Vida
	 * @return Habitos de Vida
	 */
	public String getDsOutrasAtividades() {
		return this.dsOutrasAtividades;
	}

	/**
	 * Alimenta Habitos de Vida
	 * @param dsEsportePraticados Habitos de Vida
	 */ 
	public void setDsEsportePraticados( String dsEsportePraticados ) {
		this.dsEsportePraticados = dsEsportePraticados;
	}

	/**
	 * Retorna Habitos de Vida
	 * @return Habitos de Vida
	 */
	public String getDsEsportePraticados() {
		return this.dsEsportePraticados;
	}

	/**
	 * Alimenta Habitos de Vida
	 * @param dsHobbyLazer Habitos de Vida
	 */ 
	public void setDsHobbyLazer( String dsHobbyLazer ) {
		this.dsHobbyLazer = dsHobbyLazer;
	}

	/**
	 * Retorna Habitos de Vida
	 * @return Habitos de Vida
	 */
	public String getDsHobbyLazer() {
		return this.dsHobbyLazer;
	}

	/**
	 * Alimenta Area de Comentarios
	 * @param dsComentarios Area de Comentarios
	 */ 
	public void setDsComentarios( String dsComentarios ) {
		this.dsComentarios = dsComentarios;
	}

	/**
	 * Retorna Area de Comentarios
	 * @return Area de Comentarios
	 */
	public String getDsComentarios() {
		return this.dsComentarios;
	}

}