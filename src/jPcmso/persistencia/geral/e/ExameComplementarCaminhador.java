package jPcmso.persistencia.geral.e;

import quatro.persistencia.Caminhador;
import quatro.persistencia.FiltroDeNavegador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.util.QtData;

 /**
 *
 * Classe Caminhador para ExameComplementar
 *
 * @author Samuel Antonio Klein
 * @see Caminhador
 * @see FiltroDeNavegador
 * @see ExameComplementarNavegador
 * @see ExameComplementarPersistor
 * @see ExameComplementarLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 public class ExameComplementarCaminhador extends Caminhador {

	public static final int POR_ORDEM_DEFAULT = -1;
	
	private ExameComplementar exameComplementar;

	public ExameComplementarCaminhador( FiltroDeNavegador filtro ) {

		setFiltro( filtro );
		inicializaVisao();
	}

	public ExameComplementarCaminhador( Conexao conexao, FiltroDeNavegador filtro ) throws QtSQLException {

		super.setConexao( conexao );
		setFiltro( filtro );
		inicializaVisao();
	}

	private void inicializaVisao() {

		setNmVisao( "PCMSO.EXAMES_COMPLEMENTARES" );
	}

	protected void carregaCampos() throws QtSQLException {

		exameComplementar = new ExameComplementar();

		if( isRecordAvailable() ) {
			exameComplementar.setNrExame( query.getString( "NR_EXAME" ) );
			exameComplementar.setSqExame( query.getInteger( "SQ_EXAME" ) );
			exameComplementar.setCdProcedimento( query.getString( "CD_PROCEDIMENTO" ) );
			exameComplementar.setDtExameComplementar(  QtData.criaQtData( query.getTimestamp( "DT_EXAME_COMPLEMENTAR" ) ) );
			exameComplementar.setTpResultado( query.getString( "TP_RESULTADO" ) );
			exameComplementar.setDsResultado( query.getString( "DS_RESULTADO" ) );
		}
	}

	/** M�todo que retorna uma tabela b�sica a que se refere o caminhador */
	public TabelaBasica getTabelaBasica() {

		return this.exameComplementar;
	}

	/** M�todo que retorna a classe ExameComplementar relacionada ao caminhador */
	public ExameComplementar getExameComplementar() {

		return this.exameComplementar;
	}

	public void ativaVisao() throws QtSQLException {

		if(  super.getOrdemDeNavegacao() == ExameComplementarCaminhador.POR_ORDEM_DEFAULT ) {
			super.ativaCaminhador( "order by NR_EXAME, SQ_EXAME" );
	
		} else {
			throw new QtSQLException( "ExameComplementarCaminhador - Ordem de Navega��o n�o definida!" );
		}
	}

	public String getCamposIndice( int qualIndice ) {
		return getDsIndice( qualIndice );
	}

	public String getCamposChave() {
		return getDsChave();
	}

	/**
	 * M�todo est�tico que devolve os campos que comp�e a chave da tabela;
	 * @return campos que comp�e a chave.
	 */
	public static String getDsChave() {
		return "NR_EXAME, SQ_EXAME";
	}

	/**
	 * M�todo est�tico que devolve os campos que fazem parte de uma dada ordem
	 * de navega��o.
	 * @param qualIndice indica a ordem de navega��o desejada.
	 * @return campos que comp�e a ordem de navega��o.
	 */
	public static String getDsIndice( int qualIndice ) {

		switch( qualIndice ) {

			case POR_ORDEM_DEFAULT: return "NR_EXAME, SQ_EXAME";
		}
		return null;
	}
}