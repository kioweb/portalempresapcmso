package jPcmso.persistencia.geral.e;

import java.sql.PreparedStatement;

import quatro.persistencia.Persistor;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.util.QtException;
import quatro.util.QtData;

/**
 *
 * Classe persistor para PCMSO.EMPRESAS
 *
 * @author Samuel Antonio Klein
 * @see Persistor
 * @see Empresa
 * @see EmpresaNavegador
 * @see EmpresaLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 public class EmpresaPersistor extends Persistor {

	/** Inst�ncia de Empresa para manipula��o pelo persistor */
	private Empresa empresa;

	/** Construtor gen�rico */
	public EmpresaPersistor() {

		this.empresa = null;
	}

	/** Construtor gen�rico */
	public EmpresaPersistor( Conexao cnx ) throws QtSQLException {

		this.empresa = null;
		super.setConexao( cnx );
	}

	/** Construtor que recebe uma inst�ncia de Empresa */
	public EmpresaPersistor( Empresa empresa ) throws QtSQLException {

		if( empresa == null ) 
			throw new QtSQLException( "Imposs�vel instanciar EmpresaPersistor porque est� apontando para um nulo!" );

		this.empresa = empresa;
	}

	/** Construtor que recebe uma conex�o e uma inst�ncia de Empresa */
	public EmpresaPersistor( Conexao conexao, Empresa empresa ) throws QtSQLException {

		if( empresa == null ) 
			throw new QtSQLException( "Imposs�vel instanciar EmpresaPersistor porque est� apontando para um nulo!" );

		setConexao( conexao );
		this.empresa = empresa;
	}

	/**
	 * Construtor que recebe a chave da tabela PCMSO.EMPRESAS
	 * @param cdEmpresa Codigo da Empresa
	 */
	public EmpresaPersistor( String cdEmpresa )
	  throws QtSQLException { 

		busca( cdEmpresa );
	}

	/**
	 * Construtor que recebe uma conex�o e a chave da tabela PCMSO.EMPRESAS
	 * @param cdEmpresa Codigo da Empresa
	 * @param conexao Conexao com o banco de dados
	 */
	public EmpresaPersistor( Conexao conexao, String cdEmpresa )
	  throws QtSQLException { 

		super.setConexao( conexao );
		busca( cdEmpresa );
	}

	/**
	 * M�todo interno para buscar e preencher uma tupla de uma tabela
	 * Utiliza a Inst�ncia de Empresa para fazer a busca
	 * @param cdEmpresa Codigo da Empresa
	 */
	private void busca( String cdEmpresa ) throws QtSQLException { 

		EmpresaLocalizador empresaLocalizador = new EmpresaLocalizador( getConexao(), cdEmpresa );

		if( empresaLocalizador.isEmpty() ) {
			throw new QtSQLException( "Chaves da Tabela Empresa n�o encontradas - cdEmpresa: " + cdEmpresa );
		}

		empresa = (Empresa ) empresaLocalizador.getTabelaBasica();
	}

	/**
	 * Permite alimentar o Persistor com um novo registro
	 */
	public void setTabelaBasica( TabelaBasica empresa ) throws QtException {

		if( empresa instanceof Empresa ) {
			this.empresa = (Empresa) empresa;
		} else {
			throw new QtException( "Tabela B�sica fornecida tem que ser uma inst�ncia de Empresa" );
		}
	}

	/**
	 * Permite acessar o registro corrente do persistor
	 */
	public TabelaBasica getTabelaBasica() throws QtException {

		return empresa;
	}

	/**
	 * M�todo para inserir tupla na tabela
	 */
	public void insere() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "insert into PCMSO.EMPRESAS" );
			cmd.append( "( CD_EMPRESA, DS_EMPRESA, NR_CNPJ, NR_CNAE, DT_INCLUSAO, NR_NIT_RESPRESENTANTE, NR_CPF_REPRESENTANTE, DS_NOME_REPRESENTANTE, CD_MEDICO_COORDENADOR, SN_CONTRATO_PACOTE )" );
			cmd.append( "values " );
			cmd.append( "( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			ps.setObject( 1, empresa.getCdEmpresa() );
			ps.setObject( 2, empresa.getDsEmpresa() );
			ps.setObject( 3, empresa.getNrCnpj() );
			ps.setObject( 4, empresa.getNrCnae() );
			ps.setObject( 5, QtData.getQtDataAsTimestamp( empresa.getDtInclusao() ) );
			ps.setObject( 6, empresa.getNrNitRespresentante() );
			ps.setObject( 7, empresa.getNrCpfRepresentante() );
			ps.setObject( 8, empresa.getDsNomeRepresentante() );
			ps.setObject( 9, empresa.getCdMedicoCoordenador() );
			ps.setObject( 10, empresa.isSnContratoPacote() ? "S": "N"  );
			
			ps.execute();

		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}

	/**
	 * M�todo para alterar tupla na tabela
	 */
	public void altera() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "update PCMSO.EMPRESAS" );
			cmd.append( "  set CD_EMPRESA = ?, " );
			cmd.append( " DS_EMPRESA = ?, " );
			cmd.append( " NR_CNPJ = ?, " );
			cmd.append( " NR_CNAE = ?, " );
			cmd.append( " DT_INCLUSAO = ?, " );
			cmd.append( " NR_NIT_RESPRESENTANTE = ?, " );
			cmd.append( " NR_CPF_REPRESENTANTE = ?, " );
			cmd.append( " DS_NOME_REPRESENTANTE = ?, " );
			cmd.append( " CD_MEDICO_COORDENADOR = ?, " );
			cmd.append( " SN_CONTRATO_PACOTE = ?" );
			cmd.append( " where CD_EMPRESA like ?" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			ps.setObject( 1, empresa.getCdEmpresa() );
			ps.setObject( 2, empresa.getDsEmpresa() );
			ps.setObject( 3, empresa.getNrCnpj() );
			ps.setObject( 4, empresa.getNrCnae() );
			ps.setObject( 5, QtData.getQtDataAsTimestamp( empresa.getDtInclusao() ) );
			ps.setObject( 6, empresa.getNrNitRespresentante() );
			ps.setObject( 7, empresa.getNrCpfRepresentante() );
			ps.setObject( 8, empresa.getDsNomeRepresentante() );
			ps.setObject( 9, empresa.getCdMedicoCoordenador() );
			ps.setObject( 10, empresa.isSnContratoPacote() ? "S": "N"  );
			
			ps.setObject( 11, empresa.getCdEmpresa() );
			
			ps.execute();

		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}

	/**
	 * M�todo para excluir tupla de uma tabela
	 */
	public void exclui() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "delete from PCMSO.EMPRESAS" );
			cmd.append( " where CD_EMPRESA like ?" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			ps.setObject( 1, empresa.getCdEmpresa() );
			
			ps.execute();

		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}
}