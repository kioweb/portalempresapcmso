package jPcmso.persistencia.geral.e;

import quatro.persistencia.TabelaBasica;
import quatro.smartDB.IgnoredField;
import quatro.smartDB.PrimaryKey;
import quatro.smartDB.Table;

/**
 *
 * Classe b�sica para PCMSO.EXAMES_COMPLEMENTARES_POR_CBO
 *
 * Tabela de Exames complementares por CBO
 * 
 * @author Samuel Antonio Klein
 * @see ExameComplementarCboNavegador
 * @see ExameComplementarCboPersistor
 * @see ExameComplementarCboLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 @Table(name="PCMSO.EXAMES_COMPLEMENTARES_POR_CBO")
 public class ExameComplementarCbo implements TabelaBasica {

	@IgnoredField
	private static final long serialVersionUID = -40412787325172162L;

	/** Codigo do procedimento (chave-char(8)) */
	@PrimaryKey
	private String cdProcedimento;
	/** Codigo de CBO (chave-char(6)) */
	@PrimaryKey
	private String cdCbo;
	/** 
	 * N�mero de meses
	 * 
	 * Peridiocidade de meses entre um exame e outro
	 */
	private Integer nrMesExame;
	/** 
	 * Meses de Renova��o de Exame ap�s a inclus�o
	 * 
	 */
	private Integer nrMesAposInclusao;

	/**
	 * Alimenta Codigo do procedimento
	 * @param cdProcedimento Codigo do procedimento
	 */ 
	public void setCdProcedimento( String cdProcedimento ) {
		this.cdProcedimento = cdProcedimento;
	}

	/**
	 * Retorna Codigo do procedimento
	 * @return Codigo do procedimento
	 */
	public String getCdProcedimento() {
		return this.cdProcedimento;
	}

	/**
	 * Alimenta Codigo de CBO
	 * @param cdCbo Codigo de CBO
	 */ 
	public void setCdCbo( String cdCbo ) {
		this.cdCbo = cdCbo;
	}

	/**
	 * Retorna Codigo de CBO
	 * @return Codigo de CBO
	 */
	public String getCdCbo() {
		return this.cdCbo;
	}

	/**
	 * Alimenta N�mero de meses
	 * @param nrMesExame N�mero de meses
	 */ 
	public void setNrMesExame( Integer nrMesExame ) {
		this.nrMesExame = nrMesExame;
	}

	/**
	 * Retorna N�mero de meses
	 * @return N�mero de meses
	 */
	public Integer getNrMesExame() {
		return this.nrMesExame;
	}

	/**
	 * Alimenta Meses de Renova��o de Exame ap�s a inclus�o
	 * @param nrMesAposInclusao Meses de Renova��o de Exame ap�s a inclus�o
	 */ 
	public void setNrMesAposInclusao( Integer nrMesAposInclusao ) {
		this.nrMesAposInclusao = nrMesAposInclusao;
	}

	/**
	 * Retorna Meses de Renova��o de Exame ap�s a inclus�o
	 * @return Meses de Renova��o de Exame ap�s a inclus�o
	 */
	public Integer getNrMesAposInclusao() {
		return this.nrMesAposInclusao;
	}

}