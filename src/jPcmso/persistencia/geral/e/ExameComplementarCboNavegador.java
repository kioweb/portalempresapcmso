package jPcmso.persistencia.geral.e;

import quatro.persistencia.FiltroDeNavegador;
import quatro.persistencia.Navegador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.sql.Query;

/**
 *
 * Classe navegador para PCMSO.EXAMES_COMPLEMENTARES_POR_CBO
 *
 * @author Samuel Antonio Klein
 * @see Navegador
 * @see ExameComplementarCbo
 * @see ExameComplementarCboPersistor
 * @see ExameComplementarCboLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 public class ExameComplementarCboNavegador extends Navegador {

	
	/** Inst�ncia de ExameComplementarCbo para manipula��o pelo navegador */
	private ExameComplementarCbo exameComplementarCbo;

	/** Construtor b�sico */
	public ExameComplementarCboNavegador()
	  throws QtSQLException { 

		exameComplementarCbo = new ExameComplementarCbo();
		primeiro();
	}

	/** Construtor b�sico que recebe uma conex�o */
	public ExameComplementarCboNavegador( Conexao conexao )
	  throws QtSQLException { 

		super.setConexao( conexao ); 
		exameComplementarCbo = new ExameComplementarCbo();
		primeiro();
	}

	/**
	 * Construtor que recebe uma inst�ncia de ExameComplementarCboFiltro e posiciona
	 * no primeiro registro que satisfaz suas condi��es 
	 */
	public ExameComplementarCboNavegador( FiltroDeNavegador filtro ) throws QtSQLException  {
		super.setFiltro( filtro );
		this.exameComplementarCbo = new ExameComplementarCbo();
		primeiro();
	}

	/** Construtor que recebe as chaves da tabela PCMSO.EXAMES_COMPLEMENTARES_POR_CBO
	 * @param cdProcedimento Codigo do procedimento
	 * @param cdCbo Codigo de CBO
	 */
	public ExameComplementarCboNavegador( String cdProcedimento, String cdCbo )
	  throws QtSQLException { 

		exameComplementarCbo = new ExameComplementarCbo();
		this.exameComplementarCbo.setCdProcedimento( cdProcedimento );
		this.exameComplementarCbo.setCdCbo( cdCbo );
		
		busca();
	}

	/** Construtor que recebe  uma conex�o e as chaves da tabela PCMSO.EXAMES_COMPLEMENTARES_POR_CBO
	 * @param cdProcedimento Codigo do procedimento
	 * @param cdCbo Codigo de CBO
	 * @param conexao Conexao com o banco de dados
	 */
	public ExameComplementarCboNavegador( Conexao conexao, String cdProcedimento, String cdCbo )
	  throws QtSQLException { 

		super.setConexao( conexao ); 
		exameComplementarCbo = new ExameComplementarCbo();
		this.exameComplementarCbo.setCdProcedimento( cdProcedimento );
		this.exameComplementarCbo.setCdCbo( cdCbo );
		
		busca();
	}

	/** M�todo que retorna uma tabela b�sica a que se refere o navegador */
	public TabelaBasica getTabelaBasica() {

		return this.exameComplementarCbo;
	}

	/** M�todo que retorna a classe ExameComplementarCbo relacionada ao navegador */
	public ExameComplementarCbo getExameComplementarCbo() {

		return this.exameComplementarCbo;
	}

	 /** M�todo que move para o primeiro registro da tabela */
	 public boolean primeiro() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select CD_PROCEDIMENTO, CD_CBO" );
			query.addSQL( "  from PCMSO.EXAMES_COMPLEMENTARES_POR_CBO" );
			query.addSQL( super.getCondicaoDeFiltro( FiltroDeNavegador.COM_WHERE ) );
			query.addSQL( "  order by CD_PROCEDIMENTO, CD_CBO" );
			query.addSQL( "  limit 1 " );
			
			query.executeQuery();

			
			if( query.isEmpty() ) {

				super.setQueryStates( false, true, true );
			} else {
				this.exameComplementarCbo = new ExameComplementarCbo();

				exameComplementarCbo.setCdProcedimento( query.getString( "CD_PROCEDIMENTO" ) );
				exameComplementarCbo.setCdCbo( query.getString( "CD_CBO" ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o registro anterior na tabela */
	 public boolean anterior() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select CD_PROCEDIMENTO, CD_CBO" );
			query.addSQL( "  from PCMSO.EXAMES_COMPLEMENTARES_POR_CBO" );
			query.addSQL( "  where ( ( CD_PROCEDIMENTO = :prm1CdProcedimento and  CD_CBO < :prm2CdCbo )" );
			query.addSQL( "  or (  CD_PROCEDIMENTO < :prm1CdProcedimento ) )" );
			query.addSQL( super.getCondicaoDeFiltro() );
			query.addSQL( "  order by CD_PROCEDIMENTO desc, CD_CBO desc" );
			query.addSQL( "  limit 1 " );

			query.setParameter( "prm1CdProcedimento", exameComplementarCbo.getCdProcedimento() );
			query.setParameter( "prm2CdCbo", exameComplementarCbo.getCdCbo() );
		
			query.executeQuery();

			
			if( query.isEmpty() ) {

				super.setFound( false );
				super.setBof( true );
			} else {
				this.exameComplementarCbo = new ExameComplementarCbo();

				exameComplementarCbo.setCdProcedimento( query.getString( "CD_PROCEDIMENTO" ) );
				exameComplementarCbo.setCdCbo( query.getString( "CD_CBO" ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o proximo registro da tabela */
	 public boolean proximo() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select CD_PROCEDIMENTO, CD_CBO" );
			query.addSQL( "  from PCMSO.EXAMES_COMPLEMENTARES_POR_CBO" );
			query.addSQL( "  where ( (  CD_PROCEDIMENTO = :prm1CdProcedimento and  CD_CBO > :prm2CdCbo )" );
			query.addSQL( "  or (  CD_PROCEDIMENTO > :prm1CdProcedimento ) )" );
			query.addSQL( super.getCondicaoDeFiltro() );
			query.addSQL( "  order by CD_PROCEDIMENTO, CD_CBO" );
			query.addSQL( "  limit 1 " );

			query.setParameter( "prm1CdProcedimento", exameComplementarCbo.getCdProcedimento() );
			query.setParameter( "prm2CdCbo", exameComplementarCbo.getCdCbo() );
		
			query.executeQuery();

			
			if( query.isEmpty() ) {

				super.setFound( false );
				super.setEof( true );
			} else {
				this.exameComplementarCbo = new ExameComplementarCbo();

				exameComplementarCbo.setCdProcedimento( query.getString( "CD_PROCEDIMENTO" ) );
				exameComplementarCbo.setCdCbo( query.getString( "CD_CBO" ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o �ltimo registro da tabela */
	 public boolean ultimo() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select CD_PROCEDIMENTO, CD_CBO" );
			query.addSQL( "  from PCMSO.EXAMES_COMPLEMENTARES_POR_CBO" );
			query.addSQL( super.getCondicaoDeFiltro( FiltroDeNavegador.COM_WHERE ) 	);
			query.addSQL( "  order by CD_PROCEDIMENTO desc, CD_CBO desc" );
			query.addSQL( "  limit 1 " );

			query.executeQuery();

			
			if( query.isEmpty() ) {

				super.setQueryStates( false, true, true );
			} else {
				this.exameComplementarCbo = new ExameComplementarCbo();

				exameComplementarCbo.setCdProcedimento( query.getString( "CD_PROCEDIMENTO" ) );
				exameComplementarCbo.setCdCbo( query.getString( "CD_CBO" ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	/**
	 * M�todo para buscar e preencher uma tupla de uma tabela
	 * @param tabelaBasica Recebe uma tabela basica com os atributos chave preenchidos
	 */
	public boolean busca( TabelaBasica tabelaBasica ) throws QtSQLException {

		if( !( tabelaBasica instanceof ExameComplementarCbo ) )
			throw new QtSQLException( "Esperava um objeto ExameComplementarCbo!" );

		this.exameComplementarCbo = (ExameComplementarCbo) tabelaBasica;
		return busca();

	}

	/**
	 * m�todo interno para buscar e preencher uma tupla de uma tabela
	 * Utiliza a Inst�ncia de ExameComplementarCbo da classe para fazer a busca
	 */
	private boolean busca() throws QtSQLException {

		ExameComplementarCboLocalizador localizador = new ExameComplementarCboLocalizador( getConexao(), this.exameComplementarCbo );

		if( localizador.isEmpty() ) {

			super.setQueryStates( false, true, true );
			return false;

		} else {

			super.setQueryStates( true, false, false );
			return true;
		}
	}
}