package jPcmso.persistencia.geral.e;

import quatro.persistencia.Localizador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.QtSQLException;
import quatro.sql.Query;
import quatro.sql.Conexao;
import quatro.sql.PoolDeConexoes;
import quatro.sql.ConexaoParaPoolDeConexoes;

/**
 *
 * Classe localizador para PCMSO.EXAMES_BENEFICIARIO
 *
 * @author Samuel Antonio Klein
 * @see Localizador
 * @see ExameBeneficiario
 * @see ExameBeneficiarioNavegador
 * @see ExameBeneficiarioPersistor
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 public class ExameBeneficiarioLocalizador extends Localizador {

	/** Inst�ncia de ExameBeneficiario para manipula��o pelo localizador */
	private ExameBeneficiario exameBeneficiario;

	/** Construtor que recebe uma conex�o e um objeto ExameBeneficiario completo */
	public ExameBeneficiarioLocalizador( Conexao conexao, ExameBeneficiario exameBeneficiario ) throws QtSQLException {

		super.setConexao( conexao );

		this.exameBeneficiario = exameBeneficiario;
		busca();
	}

	/** Construtor que recebe um objeto ExameBeneficiario completo */
	public ExameBeneficiarioLocalizador( ExameBeneficiario exameBeneficiario ) throws QtSQLException {

		this.exameBeneficiario = exameBeneficiario;
		busca();
	}

	/** 
	 * Construtor que recebe uma conex�o e as chaves da tabela PCMSO.EXAMES_BENEFICIARIO
	 * @param cdBeneficiarioCartao Codigo de beneficiario 
	 * @param cdProcedimento Codigo do procedimento
	 * @param conexao Conexao com o banco de dados
	 */
	public ExameBeneficiarioLocalizador( Conexao conexao, String cdBeneficiarioCartao, String cdProcedimento ) throws QtSQLException { 

		super.setConexao( conexao ); 
		exameBeneficiario = new ExameBeneficiario();
		this.exameBeneficiario.setCdBeneficiarioCartao( cdBeneficiarioCartao );
		this.exameBeneficiario.setCdProcedimento( cdProcedimento );
		
		busca();
	}

	/** 
	 * Construtor que recebe as chaves da tabela PCMSO.EXAMES_BENEFICIARIO
	 * @param cdBeneficiarioCartao Codigo de beneficiario 
	 * @param cdProcedimento Codigo do procedimento
	 */
	public ExameBeneficiarioLocalizador( String cdBeneficiarioCartao, String cdProcedimento ) throws QtSQLException { 

		exameBeneficiario = new ExameBeneficiario();
		this.exameBeneficiario.setCdBeneficiarioCartao( cdBeneficiarioCartao );
		this.exameBeneficiario.setCdProcedimento( cdProcedimento );
		
		busca();
	}

	public static boolean existe( Conexao cnx, String cdBeneficiarioCartao, String cdProcedimento ) throws QtSQLException {

		Query q = new Query( cnx );

		try {
			q.setSQL( "select count(*) from PCMSO.EXAMES_BENEFICIARIO" );
			q.addSQL( " where CD_BENEFICIARIO_CARTAO = :prm1CdBeneficiarioCartao and CD_PROCEDIMENTO = :prm2CdProcedimento" );

			q.setParameter( "prm1CdBeneficiarioCartao", cdBeneficiarioCartao );
			q.setParameter( "prm2CdProcedimento", cdProcedimento );
			q.executeQuery();

			return q.getInt( 1 ) > 0;
		} finally {
			q.liberaRecursos();
		}
	}

	public static boolean existe( String cdBeneficiarioCartao, String cdProcedimento ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return existe( cnx, cdBeneficiarioCartao, cdProcedimento );
		} finally {
			cnx.libera();
		}
	}

	public static boolean existe( Conexao cnx, ExameBeneficiario exameBeneficiario ) throws QtSQLException {

		return existe( cnx, exameBeneficiario.getCdBeneficiarioCartao(), exameBeneficiario.getCdProcedimento() );
	}

	public static boolean existe( ExameBeneficiario exameBeneficiario ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return existe( cnx, exameBeneficiario.getCdBeneficiarioCartao(), exameBeneficiario.getCdProcedimento() );
		} finally {
			cnx.libera();
		}
	}

	public static ExameBeneficiario buscaExameBeneficiario( Conexao cnx, String cdBeneficiarioCartao, String cdProcedimento ) throws QtSQLException {

		Query q = new Query( cnx );

		try {
			q.setSQL( "select * from PCMSO.EXAMES_BENEFICIARIO" );
			q.addSQL( " where CD_BENEFICIARIO_CARTAO = :prm1CdBeneficiarioCartao and CD_PROCEDIMENTO = :prm2CdProcedimento" );

			q.setParameter( "prm1CdBeneficiarioCartao", cdBeneficiarioCartao );
			q.setParameter( "prm2CdProcedimento", cdProcedimento );
			q.executeQuery();

			if( q.isEmpty() ) {
				return null;
			} else {
				ExameBeneficiario exameBeneficiario = new ExameBeneficiario();
				buscaCampos( exameBeneficiario, q );
				return exameBeneficiario;
			}
		} finally {
			q.liberaRecursos();
		}
	}

	public static ExameBeneficiario buscaExameBeneficiario( String cdBeneficiarioCartao, String cdProcedimento ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return buscaExameBeneficiario( cnx, cdBeneficiarioCartao, cdProcedimento );
		} finally { 
			cnx.libera();
		}
	}

	public static void buscaCampos( ExameBeneficiario exameBeneficiario, Query query ) throws QtSQLException {

		exameBeneficiario.setCdBeneficiarioCartao( query.getString( "CD_BENEFICIARIO_CARTAO" ) );
		exameBeneficiario.setCdProcedimento( query.getString( "CD_PROCEDIMENTO" ) );
		exameBeneficiario.setNrMesExame( query.getInteger( "NR_MES_EXAME" ) );
		exameBeneficiario.setNrMesAposInclusao( query.getInteger( "NR_MES_APOS_INCLUSAO" ) );
	}

	/** M�todo que retorna uma tabela b�sica a que se refere o localizador */
	public TabelaBasica getTabelaBasica() {

		return this.exameBeneficiario;
	}

	/** M�todo que retorna a classe ExameBeneficiario relacionada ao localizador */
	public ExameBeneficiario getExameBeneficiario() {

		return this.exameBeneficiario;
	}

	/**
	 * M�todo para buscar e preencher uma tupla de uma tabela
	 * @param tabelaBasica Recebe uma tabela basica com os atributos chave preenchidos
	 */
	public boolean busca( TabelaBasica tabelaBasica ) throws QtSQLException {

		if( !( tabelaBasica instanceof ExameBeneficiario ) )
			throw new QtSQLException( "Esperava um objeto ExameBeneficiario!" );

		this.exameBeneficiario = (ExameBeneficiario) tabelaBasica;
		return busca();

	}

	/**
	 * M�todo privado para buscar e preencher uma tupla de uma tabela
	 * Este m�todo busca os dados da inst�ncia de ExameBeneficiario atual
	 */
	private boolean busca() throws QtSQLException {

		boolean ret = false;

		super.estabeleceConexao();
		Query query = super.criaNovaQuery();

		try {

			query.setSQL( "select * from PCMSO.EXAMES_BENEFICIARIO" );
			query.addSQL( " where CD_BENEFICIARIO_CARTAO like :prm1CdBeneficiarioCartao and CD_PROCEDIMENTO like :prm2CdProcedimento" );

			query.setParameter( "prm1CdBeneficiarioCartao", exameBeneficiario.getCdBeneficiarioCartao() );
			query.setParameter( "prm2CdProcedimento", exameBeneficiario.getCdProcedimento() );
			query.executeQuery();

			if( query.isEmpty() ) {
				super.setEmpty( true );
				exameBeneficiario = null;
			} else {
				super.setEmpty( false );
				
				exameBeneficiario.setCdBeneficiarioCartao( query.getString( "CD_BENEFICIARIO_CARTAO" ) );
				exameBeneficiario.setCdProcedimento( query.getString( "CD_PROCEDIMENTO" ) );
				exameBeneficiario.setNrMesExame( query.getInteger( "NR_MES_EXAME" ) );
				exameBeneficiario.setNrMesAposInclusao( query.getInteger( "NR_MES_APOS_INCLUSAO" ) );
				ret = true;
			}
		} finally {
			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}
 }