package jPcmso.persistencia.geral.e;

import quatro.persistencia.FiltroDeNavegador;
import quatro.persistencia.Navegador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.sql.Query;

/**
 *
 * Classe navegador para PCMSO.EXAMES_BENEFICIARIO
 *
 * @author Samuel Antonio Klein
 * @see Navegador
 * @see ExameBeneficiario
 * @see ExameBeneficiarioPersistor
 * @see ExameBeneficiarioLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 public class ExameBeneficiarioNavegador extends Navegador {

	
	/** Inst�ncia de ExameBeneficiario para manipula��o pelo navegador */
	private ExameBeneficiario exameBeneficiario;

	/** Construtor b�sico */
	public ExameBeneficiarioNavegador()
	  throws QtSQLException { 

		exameBeneficiario = new ExameBeneficiario();
		primeiro();
	}

	/** Construtor b�sico que recebe uma conex�o */
	public ExameBeneficiarioNavegador( Conexao conexao )
	  throws QtSQLException { 

		super.setConexao( conexao ); 
		exameBeneficiario = new ExameBeneficiario();
		primeiro();
	}

	/**
	 * Construtor que recebe uma inst�ncia de ExameBeneficiarioFiltro e posiciona
	 * no primeiro registro que satisfaz suas condi��es 
	 */
	public ExameBeneficiarioNavegador( FiltroDeNavegador filtro ) throws QtSQLException  {
		super.setFiltro( filtro );
		this.exameBeneficiario = new ExameBeneficiario();
		primeiro();
	}

	/** Construtor que recebe as chaves da tabela PCMSO.EXAMES_BENEFICIARIO
	 * @param cdBeneficiarioCartao Codigo de beneficiario 
	 * @param cdProcedimento Codigo do procedimento
	 */
	public ExameBeneficiarioNavegador( String cdBeneficiarioCartao, String cdProcedimento )
	  throws QtSQLException { 

		exameBeneficiario = new ExameBeneficiario();
		this.exameBeneficiario.setCdBeneficiarioCartao( cdBeneficiarioCartao );
		this.exameBeneficiario.setCdProcedimento( cdProcedimento );
		
		busca();
	}

	/** Construtor que recebe  uma conex�o e as chaves da tabela PCMSO.EXAMES_BENEFICIARIO
	 * @param cdBeneficiarioCartao Codigo de beneficiario 
	 * @param cdProcedimento Codigo do procedimento
	 * @param conexao Conexao com o banco de dados
	 */
	public ExameBeneficiarioNavegador( Conexao conexao, String cdBeneficiarioCartao, String cdProcedimento )
	  throws QtSQLException { 

		super.setConexao( conexao ); 
		exameBeneficiario = new ExameBeneficiario();
		this.exameBeneficiario.setCdBeneficiarioCartao( cdBeneficiarioCartao );
		this.exameBeneficiario.setCdProcedimento( cdProcedimento );
		
		busca();
	}

	/** M�todo que retorna uma tabela b�sica a que se refere o navegador */
	public TabelaBasica getTabelaBasica() {

		return this.exameBeneficiario;
	}

	/** M�todo que retorna a classe ExameBeneficiario relacionada ao navegador */
	public ExameBeneficiario getExameBeneficiario() {

		return this.exameBeneficiario;
	}

	 /** M�todo que move para o primeiro registro da tabela */
	 public boolean primeiro() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select CD_BENEFICIARIO_CARTAO, CD_PROCEDIMENTO" );
			query.addSQL( "  from PCMSO.EXAMES_BENEFICIARIO" );
			query.addSQL( super.getCondicaoDeFiltro( FiltroDeNavegador.COM_WHERE ) );
			query.addSQL( "  order by CD_BENEFICIARIO_CARTAO, CD_PROCEDIMENTO" );
			query.addSQL( "  limit 1 " );
			
			query.executeQuery();

			
			if( query.isEmpty() ) {

				super.setQueryStates( false, true, true );
			} else {
				this.exameBeneficiario = new ExameBeneficiario();

				exameBeneficiario.setCdBeneficiarioCartao( query.getString( "CD_BENEFICIARIO_CARTAO" ) );
				exameBeneficiario.setCdProcedimento( query.getString( "CD_PROCEDIMENTO" ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o registro anterior na tabela */
	 public boolean anterior() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select CD_BENEFICIARIO_CARTAO, CD_PROCEDIMENTO" );
			query.addSQL( "  from PCMSO.EXAMES_BENEFICIARIO" );
			query.addSQL( "  where ( ( CD_BENEFICIARIO_CARTAO = :prm1CdBeneficiarioCartao and  CD_PROCEDIMENTO < :prm2CdProcedimento )" );
			query.addSQL( "  or (  CD_BENEFICIARIO_CARTAO < :prm1CdBeneficiarioCartao ) )" );
			query.addSQL( super.getCondicaoDeFiltro() );
			query.addSQL( "  order by CD_BENEFICIARIO_CARTAO desc, CD_PROCEDIMENTO desc" );
			query.addSQL( "  limit 1 " );

			query.setParameter( "prm1CdBeneficiarioCartao", exameBeneficiario.getCdBeneficiarioCartao() );
			query.setParameter( "prm2CdProcedimento", exameBeneficiario.getCdProcedimento() );
		
			query.executeQuery();

			
			if( query.isEmpty() ) {

				super.setFound( false );
				super.setBof( true );
			} else {
				this.exameBeneficiario = new ExameBeneficiario();

				exameBeneficiario.setCdBeneficiarioCartao( query.getString( "CD_BENEFICIARIO_CARTAO" ) );
				exameBeneficiario.setCdProcedimento( query.getString( "CD_PROCEDIMENTO" ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o proximo registro da tabela */
	 public boolean proximo() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select CD_BENEFICIARIO_CARTAO, CD_PROCEDIMENTO" );
			query.addSQL( "  from PCMSO.EXAMES_BENEFICIARIO" );
			query.addSQL( "  where ( (  CD_BENEFICIARIO_CARTAO = :prm1CdBeneficiarioCartao and  CD_PROCEDIMENTO > :prm2CdProcedimento )" );
			query.addSQL( "  or (  CD_BENEFICIARIO_CARTAO > :prm1CdBeneficiarioCartao ) )" );
			query.addSQL( super.getCondicaoDeFiltro() );
			query.addSQL( "  order by CD_BENEFICIARIO_CARTAO, CD_PROCEDIMENTO" );
			query.addSQL( "  limit 1 " );

			query.setParameter( "prm1CdBeneficiarioCartao", exameBeneficiario.getCdBeneficiarioCartao() );
			query.setParameter( "prm2CdProcedimento", exameBeneficiario.getCdProcedimento() );
		
			query.executeQuery();

			
			if( query.isEmpty() ) {

				super.setFound( false );
				super.setEof( true );
			} else {
				this.exameBeneficiario = new ExameBeneficiario();

				exameBeneficiario.setCdBeneficiarioCartao( query.getString( "CD_BENEFICIARIO_CARTAO" ) );
				exameBeneficiario.setCdProcedimento( query.getString( "CD_PROCEDIMENTO" ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o �ltimo registro da tabela */
	 public boolean ultimo() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select CD_BENEFICIARIO_CARTAO, CD_PROCEDIMENTO" );
			query.addSQL( "  from PCMSO.EXAMES_BENEFICIARIO" );
			query.addSQL( super.getCondicaoDeFiltro( FiltroDeNavegador.COM_WHERE ) 	);
			query.addSQL( "  order by CD_BENEFICIARIO_CARTAO desc, CD_PROCEDIMENTO desc" );
			query.addSQL( "  limit 1 " );

			query.executeQuery();

			
			if( query.isEmpty() ) {

				super.setQueryStates( false, true, true );
			} else {
				this.exameBeneficiario = new ExameBeneficiario();

				exameBeneficiario.setCdBeneficiarioCartao( query.getString( "CD_BENEFICIARIO_CARTAO" ) );
				exameBeneficiario.setCdProcedimento( query.getString( "CD_PROCEDIMENTO" ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	/**
	 * M�todo para buscar e preencher uma tupla de uma tabela
	 * @param tabelaBasica Recebe uma tabela basica com os atributos chave preenchidos
	 */
	public boolean busca( TabelaBasica tabelaBasica ) throws QtSQLException {

		if( !( tabelaBasica instanceof ExameBeneficiario ) )
			throw new QtSQLException( "Esperava um objeto ExameBeneficiario!" );

		this.exameBeneficiario = (ExameBeneficiario) tabelaBasica;
		return busca();

	}

	/**
	 * m�todo interno para buscar e preencher uma tupla de uma tabela
	 * Utiliza a Inst�ncia de ExameBeneficiario da classe para fazer a busca
	 */
	private boolean busca() throws QtSQLException {

		ExameBeneficiarioLocalizador localizador = new ExameBeneficiarioLocalizador( getConexao(), this.exameBeneficiario );

		if( localizador.isEmpty() ) {

			super.setQueryStates( false, true, true );
			return false;

		} else {

			super.setQueryStates( true, false, false );
			return true;
		}
	}
}