package jPcmso.persistencia.geral.e;

import java.sql.PreparedStatement;

import quatro.persistencia.Persistor;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.util.QtException;

/**
 *
 * Classe persistor para PCMSO.EXAMES_BENEFICIARIO
 *
 * @author Samuel Antonio Klein
 * @see Persistor
 * @see ExameBeneficiario
 * @see ExameBeneficiarioNavegador
 * @see ExameBeneficiarioLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 public class ExameBeneficiarioPersistor extends Persistor {

	/** Inst�ncia de ExameBeneficiario para manipula��o pelo persistor */
	private ExameBeneficiario exameBeneficiario;

	/** Construtor gen�rico */
	public ExameBeneficiarioPersistor() {

		this.exameBeneficiario = null;
	}

	/** Construtor gen�rico */
	public ExameBeneficiarioPersistor( Conexao cnx ) throws QtSQLException {

		this.exameBeneficiario = null;
		super.setConexao( cnx );
	}

	/** Construtor que recebe uma inst�ncia de ExameBeneficiario */
	public ExameBeneficiarioPersistor( ExameBeneficiario exameBeneficiario ) throws QtSQLException {

		if( exameBeneficiario == null ) 
			throw new QtSQLException( "Imposs�vel instanciar ExameBeneficiarioPersistor porque est� apontando para um nulo!" );

		this.exameBeneficiario = exameBeneficiario;
	}

	/** Construtor que recebe uma conex�o e uma inst�ncia de ExameBeneficiario */
	public ExameBeneficiarioPersistor( Conexao conexao, ExameBeneficiario exameBeneficiario ) throws QtSQLException {

		if( exameBeneficiario == null ) 
			throw new QtSQLException( "Imposs�vel instanciar ExameBeneficiarioPersistor porque est� apontando para um nulo!" );

		setConexao( conexao );
		this.exameBeneficiario = exameBeneficiario;
	}

	/**
	 * Construtor que recebe as chaves da tabela PCMSO.EXAMES_BENEFICIARIO
	 * @param cdBeneficiarioCartao Codigo de beneficiario 
	 * @param cdProcedimento Codigo do procedimento
	 */
	public ExameBeneficiarioPersistor( String cdBeneficiarioCartao, String cdProcedimento )
	  throws QtSQLException { 

		busca( cdBeneficiarioCartao, cdProcedimento );
	}

	/**
	 * Construtor que recebe uma conex�o e as chaves da tabela PCMSO.EXAMES_BENEFICIARIO
	 * @param cdBeneficiarioCartao Codigo de beneficiario 
	 * @param cdProcedimento Codigo do procedimento
	 * @param conexao Conexao com o banco de dados
	 */
	public ExameBeneficiarioPersistor( Conexao conexao, String cdBeneficiarioCartao, String cdProcedimento )
	  throws QtSQLException { 

		super.setConexao( conexao );
		busca( cdBeneficiarioCartao, cdProcedimento );
	}

	/**
	 * M�todo interno para buscar e preencher uma tupla de uma tabela
	 * Utiliza a Inst�ncia de ExameBeneficiario para fazer a busca
	 * @param cdBeneficiarioCartao Codigo de beneficiario 
	 * @param cdProcedimento Codigo do procedimento
	 */
	private void busca( String cdBeneficiarioCartao, String cdProcedimento ) throws QtSQLException { 

		ExameBeneficiarioLocalizador exameBeneficiarioLocalizador = new ExameBeneficiarioLocalizador( getConexao(), cdBeneficiarioCartao, cdProcedimento );

		if( exameBeneficiarioLocalizador.isEmpty() ) {
			throw new QtSQLException( "Chaves da Tabela ExameBeneficiario n�o encontradas - cdBeneficiarioCartao: " + cdBeneficiarioCartao + " / cdProcedimento: " + cdProcedimento );
		}

		exameBeneficiario = (ExameBeneficiario ) exameBeneficiarioLocalizador.getTabelaBasica();
	}

	/**
	 * Permite alimentar o Persistor com um novo registro
	 */
	public void setTabelaBasica( TabelaBasica exameBeneficiario ) throws QtException {

		if( exameBeneficiario instanceof ExameBeneficiario ) {
			this.exameBeneficiario = (ExameBeneficiario) exameBeneficiario;
		} else {
			throw new QtException( "Tabela B�sica fornecida tem que ser uma inst�ncia de ExameBeneficiario" );
		}
	}

	/**
	 * Permite acessar o registro corrente do persistor
	 */
	public TabelaBasica getTabelaBasica() throws QtException {

		return exameBeneficiario;
	}

	/**
	 * M�todo para inserir tupla na tabela
	 */
	public void insere() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "insert into PCMSO.EXAMES_BENEFICIARIO" );
			cmd.append( "( CD_BENEFICIARIO_CARTAO, CD_PROCEDIMENTO, NR_MES_EXAME, NR_MES_APOS_INCLUSAO )" );
			cmd.append( "values " );
			cmd.append( "( ?, ?, ?, ?)" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			ps.setObject( 1, exameBeneficiario.getCdBeneficiarioCartao() );
			ps.setObject( 2, exameBeneficiario.getCdProcedimento() );
			ps.setObject( 3, exameBeneficiario.getNrMesExame() );
			ps.setObject( 4, exameBeneficiario.getNrMesAposInclusao() );
			
			ps.execute();

		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}

	/**
	 * M�todo para alterar tupla na tabela
	 */
	public void altera() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "update PCMSO.EXAMES_BENEFICIARIO" );
			cmd.append( "  set CD_BENEFICIARIO_CARTAO = ?, " );
			cmd.append( " CD_PROCEDIMENTO = ?, " );
			cmd.append( " NR_MES_EXAME = ?, " );
			cmd.append( " NR_MES_APOS_INCLUSAO = ?" );
			cmd.append( " where CD_BENEFICIARIO_CARTAO like ? and CD_PROCEDIMENTO like ?" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			ps.setObject( 1, exameBeneficiario.getCdBeneficiarioCartao() );
			ps.setObject( 2, exameBeneficiario.getCdProcedimento() );
			ps.setObject( 3, exameBeneficiario.getNrMesExame() );
			ps.setObject( 4, exameBeneficiario.getNrMesAposInclusao() );
			
			ps.setObject( 5, exameBeneficiario.getCdBeneficiarioCartao() );
			ps.setObject( 6, exameBeneficiario.getCdProcedimento() );
			
			ps.execute();

		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}

	/**
	 * M�todo para excluir tupla de uma tabela
	 */
	public void exclui() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "delete from PCMSO.EXAMES_BENEFICIARIO" );
			cmd.append( " where CD_BENEFICIARIO_CARTAO like ? and CD_PROCEDIMENTO like ?" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			ps.setObject( 1, exameBeneficiario.getCdBeneficiarioCartao() );
			ps.setObject( 2, exameBeneficiario.getCdProcedimento() );
			
			ps.execute();

		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}
}