package jPcmso.persistencia.geral.e;

import quatro.persistencia.Localizador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.QtSQLException;
import quatro.sql.Query;
import quatro.sql.Conexao;
import quatro.sql.PoolDeConexoes;
import quatro.sql.ConexaoParaPoolDeConexoes;
import quatro.util.QtData;

/**
 *
 * Classe localizador para PCMSO.EXAMES_OCUPACIONAIS
 *
 * @author Samuel Antonio Klein
 * @see Localizador
 * @see ExameOcupacional
 * @see ExameOcupacionalNavegador
 * @see ExameOcupacionalPersistor
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 public class ExameOcupacionalLocalizador extends Localizador {

	/** Inst�ncia de ExameOcupacional para manipula��o pelo localizador */
	private ExameOcupacional exameOcupacional;

	/** Construtor que recebe uma conex�o e um objeto ExameOcupacional completo */
	public ExameOcupacionalLocalizador( Conexao conexao, ExameOcupacional exameOcupacional ) throws QtSQLException {

		super.setConexao( conexao );

		this.exameOcupacional = exameOcupacional;
		busca();
	}

	/** Construtor que recebe um objeto ExameOcupacional completo */
	public ExameOcupacionalLocalizador( ExameOcupacional exameOcupacional ) throws QtSQLException {

		this.exameOcupacional = exameOcupacional;
		busca();
	}

	/** 
	 * Construtor que recebe uma conex�o e a chave da tabela PCMSO.EXAMES_OCUPACIONAIS
	 * @param nrExame Numero do exame
	 * @param conexao Conexao com o banco de dados
	 */
	public ExameOcupacionalLocalizador( Conexao conexao, String nrExame ) throws QtSQLException { 

		super.setConexao( conexao ); 
		exameOcupacional = new ExameOcupacional();
		this.exameOcupacional.setNrExame( nrExame );
		
		busca();
	}

	/** 
	 * Construtor que recebe a chave da tabela PCMSO.EXAMES_OCUPACIONAIS
	 * @param nrExame Numero do exame
	 */
	public ExameOcupacionalLocalizador( String nrExame ) throws QtSQLException { 

		exameOcupacional = new ExameOcupacional();
		this.exameOcupacional.setNrExame( nrExame );
		
		busca();
	}

	public static boolean existe( Conexao cnx, String nrExame ) throws QtSQLException {

		Query q = new Query( cnx );

		try {
			q.setSQL( "select count(*) from PCMSO.EXAMES_OCUPACIONAIS" );
			q.addSQL( " where NR_EXAME = :prm1NrExame" );

			q.setParameter( "prm1NrExame", nrExame );
			q.executeQuery();

			return q.getInt( 1 ) > 0;
		} finally {
			q.liberaRecursos();
		}
	}

	public static boolean existe( String nrExame ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return existe( cnx, nrExame );
		} finally {
			cnx.libera();
		}
	}

	public static boolean existe( Conexao cnx, ExameOcupacional exameOcupacional ) throws QtSQLException {

		return existe( cnx, exameOcupacional.getNrExame() );
	}

	public static boolean existe( ExameOcupacional exameOcupacional ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return existe( cnx, exameOcupacional.getNrExame() );
		} finally {
			cnx.libera();
		}
	}

	public static ExameOcupacional buscaExameOcupacional( Conexao cnx, String nrExame ) throws QtSQLException {

		Query q = new Query( cnx );

		try {
			q.setSQL( "select * from PCMSO.EXAMES_OCUPACIONAIS" );
			q.addSQL( " where NR_EXAME = :prm1NrExame" );

			q.setParameter( "prm1NrExame", nrExame );
			q.executeQuery();

			if( q.isEmpty() ) {
				return null;
			} else {
				ExameOcupacional exameOcupacional = new ExameOcupacional();
				buscaCampos( exameOcupacional, q );
				return exameOcupacional;
			}
		} finally {
			q.liberaRecursos();
		}
	}

	public static ExameOcupacional buscaExameOcupacional( String nrExame ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return buscaExameOcupacional( cnx, nrExame );
		} finally { 
			cnx.libera();
		}
	}

	public static void buscaCampos( ExameOcupacional exameOcupacional, Query query ) throws QtSQLException {

		exameOcupacional.setNrExame( query.getString( "NR_EXAME" ) );
		exameOcupacional.setIdSetor( query.getInteger( "ID_SETOR" ) );
		exameOcupacional.setIdFuncao( query.getInteger( "ID_FUNCAO" ) );
		exameOcupacional.setIdCargo( query.getInteger( "ID_CARGO" ) );
		exameOcupacional.setCdCbo( query.getString( "CD_CBO" ) );
		exameOcupacional.setCdMedicoExaminador( query.getString( "CD_MEDICO_EXAMINADOR" ) );
		exameOcupacional.setCdBeneficiarioCartao( query.getString( "CD_BENEFICIARIO_CARTAO" ) );
		exameOcupacional.setCdProcedimento( query.getString( "CD_PROCEDIMENTO" ) );
		exameOcupacional.setTpExame( query.getString( "TP_EXAME" ) );
		exameOcupacional.setDtEmissaoNota( QtData.criaQtData( query.getTimestamp( "DT_EMISSAO_NOTA" ) ) );
		exameOcupacional.setDtExecucaoExame( QtData.criaQtData( query.getTimestamp( "DT_EXECUCAO_EXAME" ) ) );
		exameOcupacional.setDtRetornoNota( QtData.criaQtData( query.getTimestamp( "DT_RETORNO_NOTA" ) ) );
		exameOcupacional.setDtCancelamento( QtData.criaQtData( query.getTimestamp( "DT_CANCELAMENTO" ) ) );
		exameOcupacional.setDsAtividade( query.getString( "DS_ATIVIDADE" ) );
		exameOcupacional.setVlPressaoSistolica( query.getInteger( "VL_PRESSAO_SISTOLICA" ) );
		exameOcupacional.setVlPressaoDiastolica( query.getInteger( "VL_PRESSAO_DIASTOLICA" ) );
		exameOcupacional.setVlPulsoArterial( query.getDoubleObject( "VL_PULSO_ARTERIAL" ) );
		exameOcupacional.setVlPeso( query.getDoubleObject( "VL_PESO" ) );
		exameOcupacional.setVlAltura( query.getDoubleObject( "VL_ALTURA" ) );
		exameOcupacional.setSnNormolineo( query.getString( "SN_NORMOLINEO" ) );
		exameOcupacional.setSnLongilineo( query.getString( "SN_LONGILINEO" ) );
		exameOcupacional.setSnBrevilineo( query.getString( "SN_BREVILINEO" ) );
		exameOcupacional.setSnDiabete( query.getString( "SN_DIABETE" ) );
		exameOcupacional.setSnHipertencao( query.getString( "SN_HIPERTENCAO" ) );
		exameOcupacional.setSnEtilista( query.getString( "SN_ETILISTA" ) );
		exameOcupacional.setSnTabagista( query.getString( "SN_TABAGISTA" ) );
		exameOcupacional.setQtCigarrosDia( query.getInteger( "QT_CIGARROS_DIA" ) );
		exameOcupacional.setDsUsoMedicamentos( query.getString( "DS_USO_MEDICAMENTOS" ) );
		exameOcupacional.setDsSumarioConsulta( query.getString( "DS_SUMARIO_CONSULTA" ) );
		exameOcupacional.setTpConciderado( query.getString( "TP_CONCIDERADO" ) );
		exameOcupacional.setDsObservacoes( query.getString( "DS_OBSERVACOES" ) );
		exameOcupacional.setDsLocal( query.getString( "DS_LOCAL" ) );
		exameOcupacional.setSnPressaoAltaPai( query.getString( "SN_PRESSAO_ALTA_PAI" ) );
		exameOcupacional.setSnPressaoAltaMae( query.getString( "SN_PRESSAO_ALTA_MAE" ) );
		exameOcupacional.setSnPressaoAltaIrmaos( query.getString( "SN_PRESSAO_ALTA_IRMAOS" ) );
		exameOcupacional.setSnPressaoAltaAvos( query.getString( "SN_PRESSAO_ALTA_AVOS" ) );
		exameOcupacional.setSnDiabetePai( query.getString( "SN_DIABETE_PAI" ) );
		exameOcupacional.setSnDiabeteMae( query.getString( "SN_DIABETE_MAE" ) );
		exameOcupacional.setSnDiabeteIrmaos( query.getString( "SN_DIABETE_IRMAOS" ) );
		exameOcupacional.setSnDiabeteAvos( query.getString( "SN_DIABETE_AVOS" ) );
		exameOcupacional.setSnTuberculosePai( query.getString( "SN_TUBERCULOSE_PAI" ) );
		exameOcupacional.setSnTuberculoseMae( query.getString( "SN_TUBERCULOSE_MAE" ) );
		exameOcupacional.setSnTuberculoseIrmaos( query.getString( "SN_TUBERCULOSE_IRMAOS" ) );
		exameOcupacional.setSnTuberculoseAvos( query.getString( "SN_TUBERCULOSE_AVOS" ) );
		exameOcupacional.setSnCancerPai( query.getString( "SN_CANCER_PAI" ) );
		exameOcupacional.setSnCancerMae( query.getString( "SN_CANCER_MAE" ) );
		exameOcupacional.setSnCancerIrmaos( query.getString( "SN_CANCER_IRMAOS" ) );
		exameOcupacional.setSnCancerAvos( query.getString( "SN_CANCER_AVOS" ) );
		exameOcupacional.setSnDoencaCardiacaPai( query.getString( "SN_DOENCA_CARDIACA_PAI" ) );
		exameOcupacional.setSnDoencaCardiacaMae( query.getString( "SN_DOENCA_CARDIACA_MAE" ) );
		exameOcupacional.setSnDoencaCardiacaIrmaos( query.getString( "SN_DOENCA_CARDIACA_IRMAOS" ) );
		exameOcupacional.setSnDoencaCardiacaAvos( query.getString( "SN_DOENCA_CARDIACA_AVOS" ) );
		exameOcupacional.setSnDoencaMentalPai( query.getString( "SN_DOENCA_MENTAL_PAI" ) );
		exameOcupacional.setSnDoencaMentalMae( query.getString( "SN_DOENCA_MENTAL_MAE" ) );
		exameOcupacional.setSnDoencaMentalIrmaos( query.getString( "SN_DOENCA_MENTAL_IRMAOS" ) );
		exameOcupacional.setSnDoencaMentalAvos( query.getString( "SN_DOENCA_MENTAL_AVOS" ) );
		exameOcupacional.setSnAsmaAlergiaUrticariaPai( query.getString( "SN_ASMA_ALERGIA_URTICARIA_PAI" ) );
		exameOcupacional.setSnAsmaAlergiaUrticariaMae( query.getString( "SN_ASMA_ALERGIA_URTICARIA_MAE" ) );
		exameOcupacional.setSnAsmaAlergiaUrticariaIrmaos( query.getString( "SN_ASMA_ALERGIA_URTICARIA_IRMAOS" ) );
		exameOcupacional.setSnAsmaAlergiaUrticariaAvos( query.getString( "SN_ASMA_ALERGIA_URTICARIA_AVOS" ) );
		exameOcupacional.setSnConvulsaoPai( query.getString( "SN_CONVULSAO_PAI" ) );
		exameOcupacional.setSnConvulsaoMae( query.getString( "SN_CONVULSAO_MAE" ) );
		exameOcupacional.setSnConvulsaoIrmaos( query.getString( "SN_CONVULSAO_IRMAOS" ) );
		exameOcupacional.setSnConvulsaoAvos( query.getString( "SN_CONVULSAO_AVOS" ) );
		exameOcupacional.setSnLentesContatoOculos( query.getString( "SN_LENTES_CONTATO_OCULOS" ) );
		exameOcupacional.setSnBronquiteAsmaTosse( query.getString( "SN_BRONQUITE_ASMA_TOSSE" ) );
		exameOcupacional.setSnProblemaAudicao( query.getString( "SN_PROBLEMA_AUDICAO" ) );
		exameOcupacional.setSnDoencaContagiosas( query.getString( "SN_DOENCA_CONTAGIOSAS" ) );
		exameOcupacional.setSnDoencaCardiaca( query.getString( "SN_DOENCA_CARDIACA" ) );
		exameOcupacional.setSnPressaoAlta( query.getString( "SN_PRESSAO_ALTA" ) );
		exameOcupacional.setSnDoencaMental( query.getString( "SN_DOENCA_MENTAL" ) );
		exameOcupacional.setSnDorPeito( query.getString( "SN_DOR_PEITO" ) );
		exameOcupacional.setSnDisturbioFala( query.getString( "SN_DISTURBIO_FALA" ) );
		exameOcupacional.setSnVarizes( query.getString( "SN_VARIZES" ) );
		exameOcupacional.setSnHemorroidas( query.getString( "SN_HEMORROIDAS" ) );
		exameOcupacional.setSnHernias( query.getString( "SN_HERNIAS" ) );
		exameOcupacional.setSnDificuldadeTarefasPesadas( query.getString( "SN_DIFICULDADE_TAREFAS_PESADAS" ) );
		exameOcupacional.setSnResfriadosFrequentes( query.getString( "SN_RESFRIADOS_FREQUENTES" ) );
		exameOcupacional.setSnDoencaTireoide( query.getString( "SN_DOENCA_TIREOIDE" ) );
		exameOcupacional.setSnProblemasDentarios( query.getString( "SN_PROBLEMAS_DENTARIOS" ) );
		exameOcupacional.setSnFomigamentoDormencia( query.getString( "SN_FOMIGAMENTO_DORMENCIA" ) );
		exameOcupacional.setSnAnteracaoSono( query.getString( "SN_ANTERACAO_SONO" ) );
		exameOcupacional.setSnTonturaVertigem( query.getString( "SN_TONTURA_VERTIGEM" ) );
		exameOcupacional.setSnProblemasPeso( query.getString( "SN_PROBLEMAS_PESO" ) );
		exameOcupacional.setSnAlergia( query.getString( "SN_ALERGIA" ) );
		exameOcupacional.setSnDorDeCabeca( query.getString( "SN_DOR_DE_CABECA" ) );
		exameOcupacional.setSnZumbido( query.getString( "SN_ZUMBIDO" ) );
		exameOcupacional.setSnDoencaEstomago( query.getString( "SN_DOENCA_ESTOMAGO" ) );
		exameOcupacional.setSnDoencaColunaDorCosta( query.getString( "SN_DOENCA_COLUNA_DOR_COSTA" ) );
		exameOcupacional.setSnDoencaSangue( query.getString( "SN_DOENCA_SANGUE" ) );
		exameOcupacional.setSnDoencaOuvido( query.getString( "SN_DOENCA_OUVIDO" ) );
		exameOcupacional.setSnDorArticulacoes( query.getString( "SN_DOR_ARTICULACOES" ) );
		exameOcupacional.setSnDorMuscular( query.getString( "SN_DOR_MUSCULAR" ) );
		exameOcupacional.setSnNervossimo( query.getString( "SN_NERVOSSIMO" ) );
		exameOcupacional.setSnDiareiasFrequentes( query.getString( "SN_DIAREIAS_FREQUENTES" ) );
		exameOcupacional.setSnDoencaPele( query.getString( "SN_DOENCA_PELE" ) );
		exameOcupacional.setSnAfastamentoDoenca( query.getString( "SN_AFASTAMENTO_DOENCA" ) );
		exameOcupacional.setSnPortadorDeficiencia( query.getString( "SN_PORTADOR_DEFICIENCIA" ) );
		exameOcupacional.setDsDefinicaoDeficiencia( query.getString( "DS_DEFINICAO_DEFICIENCIA" ) );
		exameOcupacional.setTpFrequenciaUsoMedicamentos( query.getString( "TP_FREQUENCIA_USO_MEDICAMENTOS" ) );
		exameOcupacional.setSnInternado( query.getString( "SN_INTERNADO" ) );
		exameOcupacional.setDsMotivoInternacao( query.getString( "DS_MOTIVO_INTERNACAO" ) );
		exameOcupacional.setDsCirurgias( query.getString( "DS_CIRURGIAS" ) );
		exameOcupacional.setDsOutrasDoencas( query.getString( "DS_OUTRAS_DOENCAS" ) );
		exameOcupacional.setSnDoencaAcidenteInss( query.getString( "SN_DOENCA_ACIDENTE_INSS" ) );
		exameOcupacional.setSnAcidenteTrabalho( query.getString( "SN_ACIDENTE_TRABALHO" ) );
		exameOcupacional.setSnProdutoQuimico( query.getString( "SN_PRODUTO_QUIMICO" ) );
		exameOcupacional.setSnLocalFrio( query.getString( "SN_LOCAL_FRIO" ) );
		exameOcupacional.setSnLocalPoeira( query.getString( "SN_LOCAL_POEIRA" ) );
		exameOcupacional.setSnLocalCalor( query.getString( "SN_LOCAL_CALOR" ) );
		exameOcupacional.setSnLocalBarulho( query.getString( "SN_LOCAL_BARULHO" ) );
		exameOcupacional.setDsUltimaOcupacao( query.getString( "DS_ULTIMA_OCUPACAO" ) );
		exameOcupacional.setDsTempoOcupacaoUltimo( query.getString( "DS_TEMPO_OCUPACAO_ULTIMO" ) );
		exameOcupacional.setDsPenultimaOcupacao( query.getString( "DS_PENULTIMA_OCUPACAO" ) );
		exameOcupacional.setDsTempoOcupacaoPenultimo( query.getString( "DS_TEMPO_OCUPACAO_PENULTIMO" ) );
		exameOcupacional.setDsOutrasAtividades( query.getString( "DS_OUTRAS_ATIVIDADES" ) );
		exameOcupacional.setDsEsportePraticados( query.getString( "DS_ESPORTE_PRATICADOS" ) );
		exameOcupacional.setDsHobbyLazer( query.getString( "DS_HOBBY_LAZER" ) );
		exameOcupacional.setDsComentarios( query.getString( "DS_COMENTARIOS" ) );
	}

	/** M�todo que retorna uma tabela b�sica a que se refere o localizador */
	public TabelaBasica getTabelaBasica() {

		return this.exameOcupacional;
	}

	/** M�todo que retorna a classe ExameOcupacional relacionada ao localizador */
	public ExameOcupacional getExameOcupacional() {

		return this.exameOcupacional;
	}

	/**
	 * M�todo para buscar e preencher uma tupla de uma tabela
	 * @param tabelaBasica Recebe uma tabela basica com os atributos chave preenchidos
	 */
	public boolean busca( TabelaBasica tabelaBasica ) throws QtSQLException {

		if( !( tabelaBasica instanceof ExameOcupacional ) )
			throw new QtSQLException( "Esperava um objeto ExameOcupacional!" );

		this.exameOcupacional = (ExameOcupacional) tabelaBasica;
		return busca();

	}

	/**
	 * M�todo privado para buscar e preencher uma tupla de uma tabela
	 * Este m�todo busca os dados da inst�ncia de ExameOcupacional atual
	 */
	private boolean busca() throws QtSQLException {

		boolean ret = false;

		super.estabeleceConexao();
		Query query = super.criaNovaQuery();

		try {

			query.setSQL( "select * from PCMSO.EXAMES_OCUPACIONAIS" );
			query.addSQL( " where NR_EXAME like :prm1NrExame" );

			query.setParameter( "prm1NrExame", exameOcupacional.getNrExame() );
			query.executeQuery();

			if( query.isEmpty() ) {
				super.setEmpty( true );
				exameOcupacional = null;
			} else {
				super.setEmpty( false );
				
				exameOcupacional.setNrExame( query.getString( "NR_EXAME" ) );
				exameOcupacional.setIdSetor( query.getInteger( "ID_SETOR" ) );
				exameOcupacional.setIdFuncao( query.getInteger( "ID_FUNCAO" ) );
				exameOcupacional.setIdCargo( query.getInteger( "ID_CARGO" ) );
				exameOcupacional.setCdCbo( query.getString( "CD_CBO" ) );
				exameOcupacional.setCdMedicoExaminador( query.getString( "CD_MEDICO_EXAMINADOR" ) );
				exameOcupacional.setCdBeneficiarioCartao( query.getString( "CD_BENEFICIARIO_CARTAO" ) );
				exameOcupacional.setCdProcedimento( query.getString( "CD_PROCEDIMENTO" ) );
				exameOcupacional.setTpExame( query.getString( "TP_EXAME" ) );
				exameOcupacional.setDtEmissaoNota( QtData.criaQtData( query.getTimestamp( "DT_EMISSAO_NOTA" ) ) );
				exameOcupacional.setDtExecucaoExame( QtData.criaQtData( query.getTimestamp( "DT_EXECUCAO_EXAME" ) ) );
				exameOcupacional.setDtRetornoNota( QtData.criaQtData( query.getTimestamp( "DT_RETORNO_NOTA" ) ) );
				exameOcupacional.setDtCancelamento( QtData.criaQtData( query.getTimestamp( "DT_CANCELAMENTO" ) ) );
				exameOcupacional.setDsAtividade( query.getString( "DS_ATIVIDADE" ) );
				exameOcupacional.setVlPressaoSistolica( query.getInteger( "VL_PRESSAO_SISTOLICA" ) );
				exameOcupacional.setVlPressaoDiastolica( query.getInteger( "VL_PRESSAO_DIASTOLICA" ) );
				exameOcupacional.setVlPulsoArterial( query.getDoubleObject( "VL_PULSO_ARTERIAL" ) );
				exameOcupacional.setVlPeso( query.getDoubleObject( "VL_PESO" ) );
				exameOcupacional.setVlAltura( query.getDoubleObject( "VL_ALTURA" ) );
				exameOcupacional.setSnNormolineo( query.getString( "SN_NORMOLINEO" ) );
				exameOcupacional.setSnLongilineo( query.getString( "SN_LONGILINEO" ) );
				exameOcupacional.setSnBrevilineo( query.getString( "SN_BREVILINEO" ) );
				exameOcupacional.setSnDiabete( query.getString( "SN_DIABETE" ) );
				exameOcupacional.setSnHipertencao( query.getString( "SN_HIPERTENCAO" ) );
				exameOcupacional.setSnEtilista( query.getString( "SN_ETILISTA" ) );
				exameOcupacional.setSnTabagista( query.getString( "SN_TABAGISTA" ) );
				exameOcupacional.setQtCigarrosDia( query.getInteger( "QT_CIGARROS_DIA" ) );
				exameOcupacional.setDsUsoMedicamentos( query.getString( "DS_USO_MEDICAMENTOS" ) );
				exameOcupacional.setDsSumarioConsulta( query.getString( "DS_SUMARIO_CONSULTA" ) );
				exameOcupacional.setTpConciderado( query.getString( "TP_CONCIDERADO" ) );
				exameOcupacional.setDsObservacoes( query.getString( "DS_OBSERVACOES" ) );
				exameOcupacional.setDsLocal( query.getString( "DS_LOCAL" ) );
				exameOcupacional.setSnPressaoAltaPai( query.getString( "SN_PRESSAO_ALTA_PAI" ) );
				exameOcupacional.setSnPressaoAltaMae( query.getString( "SN_PRESSAO_ALTA_MAE" ) );
				exameOcupacional.setSnPressaoAltaIrmaos( query.getString( "SN_PRESSAO_ALTA_IRMAOS" ) );
				exameOcupacional.setSnPressaoAltaAvos( query.getString( "SN_PRESSAO_ALTA_AVOS" ) );
				exameOcupacional.setSnDiabetePai( query.getString( "SN_DIABETE_PAI" ) );
				exameOcupacional.setSnDiabeteMae( query.getString( "SN_DIABETE_MAE" ) );
				exameOcupacional.setSnDiabeteIrmaos( query.getString( "SN_DIABETE_IRMAOS" ) );
				exameOcupacional.setSnDiabeteAvos( query.getString( "SN_DIABETE_AVOS" ) );
				exameOcupacional.setSnTuberculosePai( query.getString( "SN_TUBERCULOSE_PAI" ) );
				exameOcupacional.setSnTuberculoseMae( query.getString( "SN_TUBERCULOSE_MAE" ) );
				exameOcupacional.setSnTuberculoseIrmaos( query.getString( "SN_TUBERCULOSE_IRMAOS" ) );
				exameOcupacional.setSnTuberculoseAvos( query.getString( "SN_TUBERCULOSE_AVOS" ) );
				exameOcupacional.setSnCancerPai( query.getString( "SN_CANCER_PAI" ) );
				exameOcupacional.setSnCancerMae( query.getString( "SN_CANCER_MAE" ) );
				exameOcupacional.setSnCancerIrmaos( query.getString( "SN_CANCER_IRMAOS" ) );
				exameOcupacional.setSnCancerAvos( query.getString( "SN_CANCER_AVOS" ) );
				exameOcupacional.setSnDoencaCardiacaPai( query.getString( "SN_DOENCA_CARDIACA_PAI" ) );
				exameOcupacional.setSnDoencaCardiacaMae( query.getString( "SN_DOENCA_CARDIACA_MAE" ) );
				exameOcupacional.setSnDoencaCardiacaIrmaos( query.getString( "SN_DOENCA_CARDIACA_IRMAOS" ) );
				exameOcupacional.setSnDoencaCardiacaAvos( query.getString( "SN_DOENCA_CARDIACA_AVOS" ) );
				exameOcupacional.setSnDoencaMentalPai( query.getString( "SN_DOENCA_MENTAL_PAI" ) );
				exameOcupacional.setSnDoencaMentalMae( query.getString( "SN_DOENCA_MENTAL_MAE" ) );
				exameOcupacional.setSnDoencaMentalIrmaos( query.getString( "SN_DOENCA_MENTAL_IRMAOS" ) );
				exameOcupacional.setSnDoencaMentalAvos( query.getString( "SN_DOENCA_MENTAL_AVOS" ) );
				exameOcupacional.setSnAsmaAlergiaUrticariaPai( query.getString( "SN_ASMA_ALERGIA_URTICARIA_PAI" ) );
				exameOcupacional.setSnAsmaAlergiaUrticariaMae( query.getString( "SN_ASMA_ALERGIA_URTICARIA_MAE" ) );
				exameOcupacional.setSnAsmaAlergiaUrticariaIrmaos( query.getString( "SN_ASMA_ALERGIA_URTICARIA_IRMAOS" ) );
				exameOcupacional.setSnAsmaAlergiaUrticariaAvos( query.getString( "SN_ASMA_ALERGIA_URTICARIA_AVOS" ) );
				exameOcupacional.setSnConvulsaoPai( query.getString( "SN_CONVULSAO_PAI" ) );
				exameOcupacional.setSnConvulsaoMae( query.getString( "SN_CONVULSAO_MAE" ) );
				exameOcupacional.setSnConvulsaoIrmaos( query.getString( "SN_CONVULSAO_IRMAOS" ) );
				exameOcupacional.setSnConvulsaoAvos( query.getString( "SN_CONVULSAO_AVOS" ) );
				exameOcupacional.setSnLentesContatoOculos( query.getString( "SN_LENTES_CONTATO_OCULOS" ) );
				exameOcupacional.setSnBronquiteAsmaTosse( query.getString( "SN_BRONQUITE_ASMA_TOSSE" ) );
				exameOcupacional.setSnProblemaAudicao( query.getString( "SN_PROBLEMA_AUDICAO" ) );
				exameOcupacional.setSnDoencaContagiosas( query.getString( "SN_DOENCA_CONTAGIOSAS" ) );
				exameOcupacional.setSnDoencaCardiaca( query.getString( "SN_DOENCA_CARDIACA" ) );
				exameOcupacional.setSnPressaoAlta( query.getString( "SN_PRESSAO_ALTA" ) );
				exameOcupacional.setSnDoencaMental( query.getString( "SN_DOENCA_MENTAL" ) );
				exameOcupacional.setSnDorPeito( query.getString( "SN_DOR_PEITO" ) );
				exameOcupacional.setSnDisturbioFala( query.getString( "SN_DISTURBIO_FALA" ) );
				exameOcupacional.setSnVarizes( query.getString( "SN_VARIZES" ) );
				exameOcupacional.setSnHemorroidas( query.getString( "SN_HEMORROIDAS" ) );
				exameOcupacional.setSnHernias( query.getString( "SN_HERNIAS" ) );
				exameOcupacional.setSnDificuldadeTarefasPesadas( query.getString( "SN_DIFICULDADE_TAREFAS_PESADAS" ) );
				exameOcupacional.setSnResfriadosFrequentes( query.getString( "SN_RESFRIADOS_FREQUENTES" ) );
				exameOcupacional.setSnDoencaTireoide( query.getString( "SN_DOENCA_TIREOIDE" ) );
				exameOcupacional.setSnProblemasDentarios( query.getString( "SN_PROBLEMAS_DENTARIOS" ) );
				exameOcupacional.setSnFomigamentoDormencia( query.getString( "SN_FOMIGAMENTO_DORMENCIA" ) );
				exameOcupacional.setSnAnteracaoSono( query.getString( "SN_ANTERACAO_SONO" ) );
				exameOcupacional.setSnTonturaVertigem( query.getString( "SN_TONTURA_VERTIGEM" ) );
				exameOcupacional.setSnProblemasPeso( query.getString( "SN_PROBLEMAS_PESO" ) );
				exameOcupacional.setSnAlergia( query.getString( "SN_ALERGIA" ) );
				exameOcupacional.setSnDorDeCabeca( query.getString( "SN_DOR_DE_CABECA" ) );
				exameOcupacional.setSnZumbido( query.getString( "SN_ZUMBIDO" ) );
				exameOcupacional.setSnDoencaEstomago( query.getString( "SN_DOENCA_ESTOMAGO" ) );
				exameOcupacional.setSnDoencaColunaDorCosta( query.getString( "SN_DOENCA_COLUNA_DOR_COSTA" ) );
				exameOcupacional.setSnDoencaSangue( query.getString( "SN_DOENCA_SANGUE" ) );
				exameOcupacional.setSnDoencaOuvido( query.getString( "SN_DOENCA_OUVIDO" ) );
				exameOcupacional.setSnDorArticulacoes( query.getString( "SN_DOR_ARTICULACOES" ) );
				exameOcupacional.setSnDorMuscular( query.getString( "SN_DOR_MUSCULAR" ) );
				exameOcupacional.setSnNervossimo( query.getString( "SN_NERVOSSIMO" ) );
				exameOcupacional.setSnDiareiasFrequentes( query.getString( "SN_DIAREIAS_FREQUENTES" ) );
				exameOcupacional.setSnDoencaPele( query.getString( "SN_DOENCA_PELE" ) );
				exameOcupacional.setSnAfastamentoDoenca( query.getString( "SN_AFASTAMENTO_DOENCA" ) );
				exameOcupacional.setSnPortadorDeficiencia( query.getString( "SN_PORTADOR_DEFICIENCIA" ) );
				exameOcupacional.setDsDefinicaoDeficiencia( query.getString( "DS_DEFINICAO_DEFICIENCIA" ) );
				exameOcupacional.setTpFrequenciaUsoMedicamentos( query.getString( "TP_FREQUENCIA_USO_MEDICAMENTOS" ) );
				exameOcupacional.setSnInternado( query.getString( "SN_INTERNADO" ) );
				exameOcupacional.setDsMotivoInternacao( query.getString( "DS_MOTIVO_INTERNACAO" ) );
				exameOcupacional.setDsCirurgias( query.getString( "DS_CIRURGIAS" ) );
				exameOcupacional.setDsOutrasDoencas( query.getString( "DS_OUTRAS_DOENCAS" ) );
				exameOcupacional.setSnDoencaAcidenteInss( query.getString( "SN_DOENCA_ACIDENTE_INSS" ) );
				exameOcupacional.setSnAcidenteTrabalho( query.getString( "SN_ACIDENTE_TRABALHO" ) );
				exameOcupacional.setSnProdutoQuimico( query.getString( "SN_PRODUTO_QUIMICO" ) );
				exameOcupacional.setSnLocalFrio( query.getString( "SN_LOCAL_FRIO" ) );
				exameOcupacional.setSnLocalPoeira( query.getString( "SN_LOCAL_POEIRA" ) );
				exameOcupacional.setSnLocalCalor( query.getString( "SN_LOCAL_CALOR" ) );
				exameOcupacional.setSnLocalBarulho( query.getString( "SN_LOCAL_BARULHO" ) );
				exameOcupacional.setDsUltimaOcupacao( query.getString( "DS_ULTIMA_OCUPACAO" ) );
				exameOcupacional.setDsTempoOcupacaoUltimo( query.getString( "DS_TEMPO_OCUPACAO_ULTIMO" ) );
				exameOcupacional.setDsPenultimaOcupacao( query.getString( "DS_PENULTIMA_OCUPACAO" ) );
				exameOcupacional.setDsTempoOcupacaoPenultimo( query.getString( "DS_TEMPO_OCUPACAO_PENULTIMO" ) );
				exameOcupacional.setDsOutrasAtividades( query.getString( "DS_OUTRAS_ATIVIDADES" ) );
				exameOcupacional.setDsEsportePraticados( query.getString( "DS_ESPORTE_PRATICADOS" ) );
				exameOcupacional.setDsHobbyLazer( query.getString( "DS_HOBBY_LAZER" ) );
				exameOcupacional.setDsComentarios( query.getString( "DS_COMENTARIOS" ) );
				ret = true;
			}
		} finally {
			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}
 }