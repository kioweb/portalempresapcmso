package jPcmso.persistencia.geral.e;

import quatro.persistencia.FiltroDeNavegador;
import quatro.persistencia.Navegador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.sql.Query;
import quatro.util.QtData;

/**
 *
 * Classe navegador para PCMSO.EXAMES_COMPLEMENTARES
 *
 * @author Samuel Antonio Klein
 * @see Navegador
 * @see ExameComplementar
 * @see ExameComplementarPersistor
 * @see ExameComplementarLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 public class ExameComplementarNavegador extends Navegador {

	
	/** Inst�ncia de ExameComplementar para manipula��o pelo navegador */
	private ExameComplementar exameComplementar;

	/** Construtor b�sico */
	public ExameComplementarNavegador()
	  throws QtSQLException { 

		exameComplementar = new ExameComplementar();
		primeiro();
	}

	/** Construtor b�sico que recebe uma conex�o */
	public ExameComplementarNavegador( Conexao conexao )
	  throws QtSQLException { 

		super.setConexao( conexao ); 
		exameComplementar = new ExameComplementar();
		primeiro();
	}

	/**
	 * Construtor que recebe uma inst�ncia de ExameComplementarFiltro e posiciona
	 * no primeiro registro que satisfaz suas condi��es 
	 */
	public ExameComplementarNavegador( FiltroDeNavegador filtro ) throws QtSQLException  {
		super.setFiltro( filtro );
		this.exameComplementar = new ExameComplementar();
		primeiro();
	}

	/** Construtor que recebe as chaves da tabela PCMSO.EXAMES_COMPLEMENTARES
	 * @param nrExame Numero do exame
	 * @param sqExame Sequencia do exame
	 */
	public ExameComplementarNavegador( String nrExame, int sqExame )
	  throws QtSQLException { 

		exameComplementar = new ExameComplementar();
		this.exameComplementar.setNrExame( nrExame );
		this.exameComplementar.setSqExame( new Integer( sqExame ) );
		
		busca();
	}

	/** Construtor que recebe  uma conex�o e as chaves da tabela PCMSO.EXAMES_COMPLEMENTARES
	 * @param nrExame Numero do exame
	 * @param sqExame Sequencia do exame
	 * @param conexao Conexao com o banco de dados
	 */
	public ExameComplementarNavegador( Conexao conexao, String nrExame, int sqExame )
	  throws QtSQLException { 

		super.setConexao( conexao ); 
		exameComplementar = new ExameComplementar();
		this.exameComplementar.setNrExame( nrExame );
		this.exameComplementar.setSqExame( new Integer( sqExame ) );
		
		busca();
	}

	/** M�todo que retorna uma tabela b�sica a que se refere o navegador */
	public TabelaBasica getTabelaBasica() {

		return this.exameComplementar;
	}

	/** M�todo que retorna a classe ExameComplementar relacionada ao navegador */
	public ExameComplementar getExameComplementar() {

		return this.exameComplementar;
	}

	 /** M�todo que move para o primeiro registro da tabela */
	 public boolean primeiro() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select NR_EXAME, SQ_EXAME" );
			query.addSQL( "  from PCMSO.EXAMES_COMPLEMENTARES" );
			query.addSQL( super.getCondicaoDeFiltro( FiltroDeNavegador.COM_WHERE ) );
			query.addSQL( "  order by NR_EXAME, SQ_EXAME" );
			query.addSQL( "  limit 1 " );
			
			query.executeQuery();

			
			if( query.isEmpty() ) {

				super.setQueryStates( false, true, true );
			} else {
				this.exameComplementar = new ExameComplementar();

				exameComplementar.setNrExame( query.getString( "NR_EXAME" ) );
				exameComplementar.setSqExame( query.getInteger( "SQ_EXAME" ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o registro anterior na tabela */
	 public boolean anterior() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select NR_EXAME, SQ_EXAME" );
			query.addSQL( "  from PCMSO.EXAMES_COMPLEMENTARES" );
			query.addSQL( "  where ( ( NR_EXAME = :prm1NrExame and  SQ_EXAME < :prm2SqExame )" );
			query.addSQL( "  or (  NR_EXAME < :prm1NrExame ) )" );
			query.addSQL( super.getCondicaoDeFiltro() );
			query.addSQL( "  order by NR_EXAME desc, SQ_EXAME desc" );
			query.addSQL( "  limit 1 " );

			query.setParameter( "prm1NrExame", exameComplementar.getNrExame() );
			query.setParameter( "prm2SqExame", exameComplementar.getSqExame() );
		
			query.executeQuery();

			
			if( query.isEmpty() ) {

				super.setFound( false );
				super.setBof( true );
			} else {
				this.exameComplementar = new ExameComplementar();

				exameComplementar.setNrExame( query.getString( "NR_EXAME" ) );
				exameComplementar.setSqExame( query.getInteger( "SQ_EXAME" ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o proximo registro da tabela */
	 public boolean proximo() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select NR_EXAME, SQ_EXAME" );
			query.addSQL( "  from PCMSO.EXAMES_COMPLEMENTARES" );
			query.addSQL( "  where ( (  NR_EXAME = :prm1NrExame and  SQ_EXAME > :prm2SqExame )" );
			query.addSQL( "  or (  NR_EXAME > :prm1NrExame ) )" );
			query.addSQL( super.getCondicaoDeFiltro() );
			query.addSQL( "  order by NR_EXAME, SQ_EXAME" );
			query.addSQL( "  limit 1 " );

			query.setParameter( "prm1NrExame", exameComplementar.getNrExame() );
			query.setParameter( "prm2SqExame", exameComplementar.getSqExame() );
		
			query.executeQuery();

			
			if( query.isEmpty() ) {

				super.setFound( false );
				super.setEof( true );
			} else {
				this.exameComplementar = new ExameComplementar();

				exameComplementar.setNrExame( query.getString( "NR_EXAME" ) );
				exameComplementar.setSqExame( query.getInteger( "SQ_EXAME" ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o �ltimo registro da tabela */
	 public boolean ultimo() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select NR_EXAME, SQ_EXAME" );
			query.addSQL( "  from PCMSO.EXAMES_COMPLEMENTARES" );
			query.addSQL( super.getCondicaoDeFiltro( FiltroDeNavegador.COM_WHERE ) 	);
			query.addSQL( "  order by NR_EXAME desc, SQ_EXAME desc" );
			query.addSQL( "  limit 1 " );

			query.executeQuery();

			
			if( query.isEmpty() ) {

				super.setQueryStates( false, true, true );
			} else {
				this.exameComplementar = new ExameComplementar();

				exameComplementar.setNrExame( query.getString( "NR_EXAME" ) );
				exameComplementar.setSqExame( query.getInteger( "SQ_EXAME" ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	/**
	 * M�todo para buscar e preencher uma tupla de uma tabela
	 * @param tabelaBasica Recebe uma tabela basica com os atributos chave preenchidos
	 */
	public boolean busca( TabelaBasica tabelaBasica ) throws QtSQLException {

		if( !( tabelaBasica instanceof ExameComplementar ) )
			throw new QtSQLException( "Esperava um objeto ExameComplementar!" );

		this.exameComplementar = (ExameComplementar) tabelaBasica;
		return busca();

	}

	/**
	 * m�todo interno para buscar e preencher uma tupla de uma tabela
	 * Utiliza a Inst�ncia de ExameComplementar da classe para fazer a busca
	 */
	private boolean busca() throws QtSQLException {

		ExameComplementarLocalizador localizador = new ExameComplementarLocalizador( getConexao(), this.exameComplementar );

		if( localizador.isEmpty() ) {

			super.setQueryStates( false, true, true );
			return false;

		} else {

			super.setQueryStates( true, false, false );
			return true;
		}
	}
}