package jPcmso.persistencia.geral.e;

import quatro.persistencia.Caminhador;
import quatro.persistencia.FiltroDeNavegador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.util.QtData;

 /**
 *
 * Classe Caminhador para Empresa
 *
 * @author Samuel Antonio Klein
 * @see Caminhador
 * @see FiltroDeNavegador
 * @see EmpresaNavegador
 * @see EmpresaPersistor
 * @see EmpresaLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 public class EmpresaCaminhador extends Caminhador {

	public static final int POR_ORDEM_DEFAULT = -1;
	
	private Empresa empresa;

	public EmpresaCaminhador( FiltroDeNavegador filtro ) {

		setFiltro( filtro );
		inicializaVisao();
	}

	public EmpresaCaminhador( Conexao conexao, FiltroDeNavegador filtro ) throws QtSQLException {

		super.setConexao( conexao );
		setFiltro( filtro );
		inicializaVisao();
	}

	private void inicializaVisao() {

		setNmVisao( "PCMSO.EMPRESAS" );
	}

	protected void carregaCampos() throws QtSQLException {

		empresa = new Empresa();

		if( isRecordAvailable() ) {
			empresa.setCdEmpresa( query.getString( "CD_EMPRESA" ) );
			empresa.setDsEmpresa( query.getString( "DS_EMPRESA" ) );
			empresa.setNrCnpj( query.getString( "NR_CNPJ" ) );
			empresa.setNrCnae( query.getString( "NR_CNAE" ) );
			empresa.setDtInclusao(  QtData.criaQtData( query.getTimestamp( "DT_INCLUSAO" ) ) );
			empresa.setNrNitRespresentante( query.getString( "NR_NIT_RESPRESENTANTE" ) );
			empresa.setNrCpfRepresentante( query.getString( "NR_CPF_REPRESENTANTE" ) );
			empresa.setDsNomeRepresentante( query.getString( "DS_NOME_REPRESENTANTE" ) );
			empresa.setCdMedicoCoordenador( query.getString( "CD_MEDICO_COORDENADOR" ) );
			empresa.setSnContratoPacote( query.getString( "SN_CONTRATO_PACOTE" ) );
		}
	}

	/** M�todo que retorna uma tabela b�sica a que se refere o caminhador */
	public TabelaBasica getTabelaBasica() {

		return this.empresa;
	}

	/** M�todo que retorna a classe Empresa relacionada ao caminhador */
	public Empresa getEmpresa() {

		return this.empresa;
	}

	public void ativaVisao() throws QtSQLException {

		if(  super.getOrdemDeNavegacao() == EmpresaCaminhador.POR_ORDEM_DEFAULT ) {
			super.ativaCaminhador( "order by CD_EMPRESA" );
	
		} else {
			throw new QtSQLException( "EmpresaCaminhador - Ordem de Navega��o n�o definida!" );
		}
	}

	public String getCamposIndice( int qualIndice ) {
		return getDsIndice( qualIndice );
	}

	public String getCamposChave() {
		return getDsChave();
	}

	/**
	 * M�todo est�tico que devolve os campos que comp�e a chave da tabela;
	 * @return campos que comp�e a chave.
	 */
	public static String getDsChave() {
		return "CD_EMPRESA";
	}

	/**
	 * M�todo est�tico que devolve os campos que fazem parte de uma dada ordem
	 * de navega��o.
	 * @param qualIndice indica a ordem de navega��o desejada.
	 * @return campos que comp�e a ordem de navega��o.
	 */
	public static String getDsIndice( int qualIndice ) {

		switch( qualIndice ) {

			case POR_ORDEM_DEFAULT: return "CD_EMPRESA";
		}
		return null;
	}
}