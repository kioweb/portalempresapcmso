package jPcmso.persistencia.geral.e;

import quatro.persistencia.Localizador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.QtSQLException;
import quatro.sql.Query;
import quatro.sql.Conexao;
import quatro.sql.PoolDeConexoes;
import quatro.sql.ConexaoParaPoolDeConexoes;
import quatro.util.QtData;

/**
 *
 * Classe localizador para PCMSO.EMPRESAS
 *
 * @author Samuel Antonio Klein
 * @see Localizador
 * @see Empresa
 * @see EmpresaNavegador
 * @see EmpresaPersistor
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 public class EmpresaLocalizador extends Localizador {

	/** Inst�ncia de Empresa para manipula��o pelo localizador */
	private Empresa empresa;

	/** Construtor que recebe uma conex�o e um objeto Empresa completo */
	public EmpresaLocalizador( Conexao conexao, Empresa empresa ) throws QtSQLException {

		super.setConexao( conexao );

		this.empresa = empresa;
		busca();
	}

	/** Construtor que recebe um objeto Empresa completo */
	public EmpresaLocalizador( Empresa empresa ) throws QtSQLException {

		this.empresa = empresa;
		busca();
	}

	/** 
	 * Construtor que recebe uma conex�o e a chave da tabela PCMSO.EMPRESAS
	 * @param cdEmpresa Codigo da Empresa
	 * @param conexao Conexao com o banco de dados
	 */
	public EmpresaLocalizador( Conexao conexao, String cdEmpresa ) throws QtSQLException { 

		super.setConexao( conexao ); 
		empresa = new Empresa();
		this.empresa.setCdEmpresa( cdEmpresa );
		
		busca();
	}

	/** 
	 * Construtor que recebe a chave da tabela PCMSO.EMPRESAS
	 * @param cdEmpresa Codigo da Empresa
	 */
	public EmpresaLocalizador( String cdEmpresa ) throws QtSQLException { 

		empresa = new Empresa();
		this.empresa.setCdEmpresa( cdEmpresa );
		
		busca();
	}

	public static boolean existe( Conexao cnx, String cdEmpresa ) throws QtSQLException {

		Query q = new Query( cnx );

		try {
			q.setSQL( "select count(*) from PCMSO.EMPRESAS" );
			q.addSQL( " where CD_EMPRESA = :prm1CdEmpresa" );

			q.setParameter( "prm1CdEmpresa", cdEmpresa );
			q.executeQuery();

			return q.getInt( 1 ) > 0;
		} finally {
			q.liberaRecursos();
		}
	}

	public static boolean existe( String cdEmpresa ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return existe( cnx, cdEmpresa );
		} finally {
			cnx.libera();
		}
	}

	public static boolean existe( Conexao cnx, Empresa empresa ) throws QtSQLException {

		return existe( cnx, empresa.getCdEmpresa() );
	}

	public static boolean existe( Empresa empresa ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return existe( cnx, empresa.getCdEmpresa() );
		} finally {
			cnx.libera();
		}
	}

	public static Empresa buscaEmpresa( Conexao cnx, String cdEmpresa ) throws QtSQLException {

		Query q = new Query( cnx );

		try {
			q.setSQL( "select * from PCMSO.EMPRESAS" );
			q.addSQL( " where CD_EMPRESA = :prm1CdEmpresa" );

			q.setParameter( "prm1CdEmpresa", cdEmpresa );
			q.executeQuery();

			if( q.isEmpty() ) {
				return null;
			} else {
				Empresa empresa = new Empresa();
				buscaCampos( empresa, q );
				return empresa;
			}
		} finally {
			q.liberaRecursos();
		}
	}

	public static Empresa buscaEmpresa( String cdEmpresa ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return buscaEmpresa( cnx, cdEmpresa );
		} finally { 
			cnx.libera();
		}
	}

	public static void buscaCampos( Empresa empresa, Query query ) throws QtSQLException {

		empresa.setCdEmpresa( query.getString( "CD_EMPRESA" ) );
		empresa.setDsEmpresa( query.getString( "DS_EMPRESA" ) );
		empresa.setNrCnpj( query.getString( "NR_CNPJ" ) );
		empresa.setNrCnae( query.getString( "NR_CNAE" ) );
		empresa.setDtInclusao( QtData.criaQtData( query.getTimestamp( "DT_INCLUSAO" ) ) );
		empresa.setNrNitRespresentante( query.getString( "NR_NIT_RESPRESENTANTE" ) );
		empresa.setNrCpfRepresentante( query.getString( "NR_CPF_REPRESENTANTE" ) );
		empresa.setDsNomeRepresentante( query.getString( "DS_NOME_REPRESENTANTE" ) );
		empresa.setCdMedicoCoordenador( query.getString( "CD_MEDICO_COORDENADOR" ) );
		empresa.setSnContratoPacote( query.getString( "SN_CONTRATO_PACOTE" ) );
	}

	/** M�todo que retorna uma tabela b�sica a que se refere o localizador */
	public TabelaBasica getTabelaBasica() {

		return this.empresa;
	}

	/** M�todo que retorna a classe Empresa relacionada ao localizador */
	public Empresa getEmpresa() {

		return this.empresa;
	}

	/**
	 * M�todo para buscar e preencher uma tupla de uma tabela
	 * @param tabelaBasica Recebe uma tabela basica com os atributos chave preenchidos
	 */
	public boolean busca( TabelaBasica tabelaBasica ) throws QtSQLException {

		if( !( tabelaBasica instanceof Empresa ) )
			throw new QtSQLException( "Esperava um objeto Empresa!" );

		this.empresa = (Empresa) tabelaBasica;
		return busca();

	}

	/**
	 * M�todo privado para buscar e preencher uma tupla de uma tabela
	 * Este m�todo busca os dados da inst�ncia de Empresa atual
	 */
	private boolean busca() throws QtSQLException {

		boolean ret = false;

		super.estabeleceConexao();
		Query query = super.criaNovaQuery();

		try {

			query.setSQL( "select * from PCMSO.EMPRESAS" );
			query.addSQL( " where CD_EMPRESA like :prm1CdEmpresa" );

			query.setParameter( "prm1CdEmpresa", empresa.getCdEmpresa() );
			query.executeQuery();

			if( query.isEmpty() ) {
				super.setEmpty( true );
				empresa = null;
			} else {
				super.setEmpty( false );
				
				empresa.setCdEmpresa( query.getString( "CD_EMPRESA" ) );
				empresa.setDsEmpresa( query.getString( "DS_EMPRESA" ) );
				empresa.setNrCnpj( query.getString( "NR_CNPJ" ) );
				empresa.setNrCnae( query.getString( "NR_CNAE" ) );
				empresa.setDtInclusao( QtData.criaQtData( query.getTimestamp( "DT_INCLUSAO" ) ) );
				empresa.setNrNitRespresentante( query.getString( "NR_NIT_RESPRESENTANTE" ) );
				empresa.setNrCpfRepresentante( query.getString( "NR_CPF_REPRESENTANTE" ) );
				empresa.setDsNomeRepresentante( query.getString( "DS_NOME_REPRESENTANTE" ) );
				empresa.setCdMedicoCoordenador( query.getString( "CD_MEDICO_COORDENADOR" ) );
				empresa.setSnContratoPacote( query.getString( "SN_CONTRATO_PACOTE" ) );
				ret = true;
			}
		} finally {
			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}
 }