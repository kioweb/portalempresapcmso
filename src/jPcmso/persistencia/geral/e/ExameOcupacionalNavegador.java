package jPcmso.persistencia.geral.e;

import quatro.persistencia.FiltroDeNavegador;
import quatro.persistencia.Navegador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.sql.Query;
import quatro.util.QtData;

/**
 *
 * Classe navegador para PCMSO.EXAMES_OCUPACIONAIS
 *
 * @author Samuel Antonio Klein
 * @see Navegador
 * @see ExameOcupacional
 * @see ExameOcupacionalPersistor
 * @see ExameOcupacionalLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 public class ExameOcupacionalNavegador extends Navegador {

	
	/** Inst�ncia de ExameOcupacional para manipula��o pelo navegador */
	private ExameOcupacional exameOcupacional;

	/** Construtor b�sico */
	public ExameOcupacionalNavegador()
	  throws QtSQLException { 

		exameOcupacional = new ExameOcupacional();
		primeiro();
	}

	/** Construtor b�sico que recebe uma conex�o */
	public ExameOcupacionalNavegador( Conexao conexao )
	  throws QtSQLException { 

		super.setConexao( conexao ); 
		exameOcupacional = new ExameOcupacional();
		primeiro();
	}

	/**
	 * Construtor que recebe uma inst�ncia de ExameOcupacionalFiltro e posiciona
	 * no primeiro registro que satisfaz suas condi��es 
	 */
	public ExameOcupacionalNavegador( FiltroDeNavegador filtro ) throws QtSQLException  {
		super.setFiltro( filtro );
		this.exameOcupacional = new ExameOcupacional();
		primeiro();
	}

	/** Construtor que recebe a chave da tabela PCMSO.EXAMES_OCUPACIONAIS
	 * @param nrExame Numero do exame
	 */
	public ExameOcupacionalNavegador( String nrExame )
	  throws QtSQLException { 

		exameOcupacional = new ExameOcupacional();
		this.exameOcupacional.setNrExame( nrExame );
		
		busca();
	}

	/** Construtor que recebe  uma conex�o e a chave da tabela PCMSO.EXAMES_OCUPACIONAIS
	 * @param nrExame Numero do exame
	 * @param conexao Conexao com o banco de dados
	 */
	public ExameOcupacionalNavegador( Conexao conexao, String nrExame )
	  throws QtSQLException { 

		super.setConexao( conexao ); 
		exameOcupacional = new ExameOcupacional();
		this.exameOcupacional.setNrExame( nrExame );
		
		busca();
	}

	/** M�todo que retorna uma tabela b�sica a que se refere o navegador */
	public TabelaBasica getTabelaBasica() {

		return this.exameOcupacional;
	}

	/** M�todo que retorna a classe ExameOcupacional relacionada ao navegador */
	public ExameOcupacional getExameOcupacional() {

		return this.exameOcupacional;
	}

	 /** M�todo que move para o primeiro registro da tabela */
	 public boolean primeiro() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select min( NR_EXAME ) from PCMSO.EXAMES_OCUPACIONAIS" );
			query.addSQL( super.getCondicaoDeFiltro( FiltroDeNavegador.COM_WHERE ) 	);

			query.executeQuery();

			
			if( query.getString( 1 ) == null ) {

				super.setQueryStates( false, true, true );
			} else {
				this.exameOcupacional = new ExameOcupacional();

				exameOcupacional.setNrExame( query.getString( 1 ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o registro anterior na tabela */
	 public boolean anterior() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select max( NR_EXAME ) from PCMSO.EXAMES_OCUPACIONAIS" );
			query.addSQL( " where NR_EXAME < :prm1NrExame" );
			query.addSQL( super.getCondicaoDeFiltro() );

			query.setParameter( "prm1NrExame", exameOcupacional.getNrExame() );
			
			query.executeQuery();

			
			if( query.getString( 1 ) == null ) {

				super.setFound( false );
				super.setBof( true );
			} else {
				this.exameOcupacional = new ExameOcupacional();

				exameOcupacional.setNrExame( query.getString( 1 ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o proximo registro da tabela */
	 public boolean proximo() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select min( NR_EXAME ) from PCMSO.EXAMES_OCUPACIONAIS" );
			query.addSQL( " where NR_EXAME > :prm1NrExame" );
			query.addSQL( super.getCondicaoDeFiltro() );

			query.setParameter( "prm1NrExame", exameOcupacional.getNrExame() );

			query.executeQuery();

			
			if( query.getString( 1 ) == null ) {

				super.setFound( false );
				super.setEof( true );
			} else {
				this.exameOcupacional = new ExameOcupacional();

				exameOcupacional.setNrExame( query.getString( 1 ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o �ltimo registro da tabela */
	 public boolean ultimo() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select max( NR_EXAME ) from PCMSO.EXAMES_OCUPACIONAIS" );
			query.addSQL( super.getCondicaoDeFiltro( FiltroDeNavegador.COM_WHERE ) 	);

			query.executeQuery();

			
			if( query.getString( 1 ) == null ) {

				super.setQueryStates( false, true, true );
			} else {
				this.exameOcupacional = new ExameOcupacional();

				exameOcupacional.setNrExame( query.getString( 1 ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	/**
	 * M�todo para buscar e preencher uma tupla de uma tabela
	 * @param tabelaBasica Recebe uma tabela basica com os atributos chave preenchidos
	 */
	public boolean busca( TabelaBasica tabelaBasica ) throws QtSQLException {

		if( !( tabelaBasica instanceof ExameOcupacional ) )
			throw new QtSQLException( "Esperava um objeto ExameOcupacional!" );

		this.exameOcupacional = (ExameOcupacional) tabelaBasica;
		return busca();

	}

	/**
	 * m�todo interno para buscar e preencher uma tupla de uma tabela
	 * Utiliza a Inst�ncia de ExameOcupacional da classe para fazer a busca
	 */
	private boolean busca() throws QtSQLException {

		ExameOcupacionalLocalizador localizador = new ExameOcupacionalLocalizador( getConexao(), this.exameOcupacional );

		if( localizador.isEmpty() ) {

			super.setQueryStates( false, true, true );
			return false;

		} else {

			super.setQueryStates( true, false, false );
			return true;
		}
	}
}