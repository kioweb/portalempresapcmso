package jPcmso.persistencia.geral.e;

import quatro.persistencia.TabelaBasica;
import quatro.smartDB.IgnoredField;
import quatro.smartDB.PrimaryKey;
import quatro.smartDB.Table;
import quatro.util.QtData;

/**
 *
 * Classe b�sica para PCMSO.EXAMES_COMPLEMENTARES
 *
 * Tabela de Exames Complementar
 * 
 * @author Samuel Antonio Klein
 * @see ExameComplementarNavegador
 * @see ExameComplementarPersistor
 * @see ExameComplementarLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 @Table(name="PCMSO.EXAMES_COMPLEMENTARES")
 public class ExameComplementar implements TabelaBasica {

	@IgnoredField
	private static final long serialVersionUID = -134512898824479924L;

	/** Numero do exame (chave-char(7)) */
	@PrimaryKey
	private String nrExame;
	/** Sequencia do exame (chave) */
	@PrimaryKey
	private Integer sqExame;
	/** Codigo do procedimento (char(8)) */
	private String cdProcedimento;
	/** Data do exames complementar */
	private QtData dtExameComplementar;
	/** 
	 * Tipo de Resultado (char(1))
	 * 
	 * N - Normal
	 * A - alterado
	 */
	private String tpResultado;
	/** Descri��o do Resultado */
	private String dsResultado;

	/**
	 * Alimenta Numero do exame
	 * @param nrExame Numero do exame
	 */ 
	public void setNrExame( String nrExame ) {
		this.nrExame = nrExame;
	}

	/**
	 * Retorna Numero do exame
	 * @return Numero do exame
	 */
	public String getNrExame() {
		return this.nrExame;
	}

	/**
	 * Alimenta Sequencia do exame
	 * @param sqExame Sequencia do exame
	 */ 
	public void setSqExame( Integer sqExame ) {
		this.sqExame = sqExame;
	}

	/**
	 * Retorna Sequencia do exame
	 * @return Sequencia do exame
	 */
	public Integer getSqExame() {
		return this.sqExame;
	}

	/**
	 * Alimenta Codigo do procedimento
	 * @param cdProcedimento Codigo do procedimento
	 */ 
	public void setCdProcedimento( String cdProcedimento ) {
		this.cdProcedimento = cdProcedimento;
	}

	/**
	 * Retorna Codigo do procedimento
	 * @return Codigo do procedimento
	 */
	public String getCdProcedimento() {
		return this.cdProcedimento;
	}

	/**
	 * Alimenta Data do exames complementar
	 * @param dtExameComplementar Data do exames complementar
	 */ 
	public void setDtExameComplementar( QtData dtExameComplementar ) {
		this.dtExameComplementar = dtExameComplementar;
	}

	/**
	 * Retorna Data do exames complementar
	 * @return Data do exames complementar
	 */
	public QtData getDtExameComplementar() {
		return this.dtExameComplementar;
	}

	/**
	 * Alimenta Tipo de Resultado
	 * @param tpResultado Tipo de Resultado
	 */ 
	public void setTpResultado( String tpResultado ) {
		this.tpResultado = tpResultado;
	}

	/**
	 * Retorna Tipo de Resultado
	 * @return Tipo de Resultado
	 */
	public String getTpResultado() {
		return this.tpResultado;
	}

	/**
	 * Alimenta Descri��o do Resultado
	 * @param dsResultado Descri��o do Resultado
	 */ 
	public void setDsResultado( String dsResultado ) {
		this.dsResultado = dsResultado;
	}

	/**
	 * Retorna Descri��o do Resultado
	 * @return Descri��o do Resultado
	 */
	public String getDsResultado() {
		return this.dsResultado;
	}

}