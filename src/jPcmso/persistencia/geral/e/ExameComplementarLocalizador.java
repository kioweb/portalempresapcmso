package jPcmso.persistencia.geral.e;

import quatro.persistencia.Localizador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.QtSQLException;
import quatro.sql.Query;
import quatro.sql.Conexao;
import quatro.sql.PoolDeConexoes;
import quatro.sql.ConexaoParaPoolDeConexoes;
import quatro.util.QtData;

/**
 *
 * Classe localizador para PCMSO.EXAMES_COMPLEMENTARES
 *
 * @author Samuel Antonio Klein
 * @see Localizador
 * @see ExameComplementar
 * @see ExameComplementarNavegador
 * @see ExameComplementarPersistor
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 public class ExameComplementarLocalizador extends Localizador {

	/** Inst�ncia de ExameComplementar para manipula��o pelo localizador */
	private ExameComplementar exameComplementar;

	/** Construtor que recebe uma conex�o e um objeto ExameComplementar completo */
	public ExameComplementarLocalizador( Conexao conexao, ExameComplementar exameComplementar ) throws QtSQLException {

		super.setConexao( conexao );

		this.exameComplementar = exameComplementar;
		busca();
	}

	/** Construtor que recebe um objeto ExameComplementar completo */
	public ExameComplementarLocalizador( ExameComplementar exameComplementar ) throws QtSQLException {

		this.exameComplementar = exameComplementar;
		busca();
	}

	/** 
	 * Construtor que recebe uma conex�o e as chaves da tabela PCMSO.EXAMES_COMPLEMENTARES
	 * @param nrExame Numero do exame
	 * @param sqExame Sequencia do exame
	 * @param conexao Conexao com o banco de dados
	 */
	public ExameComplementarLocalizador( Conexao conexao, String nrExame, int sqExame ) throws QtSQLException { 

		super.setConexao( conexao ); 
		exameComplementar = new ExameComplementar();
		this.exameComplementar.setNrExame( nrExame );
		this.exameComplementar.setSqExame( new Integer( sqExame ) );
		
		busca();
	}

	/** 
	 * Construtor que recebe as chaves da tabela PCMSO.EXAMES_COMPLEMENTARES
	 * @param nrExame Numero do exame
	 * @param sqExame Sequencia do exame
	 */
	public ExameComplementarLocalizador( String nrExame, int sqExame ) throws QtSQLException { 

		exameComplementar = new ExameComplementar();
		this.exameComplementar.setNrExame( nrExame );
		this.exameComplementar.setSqExame( new Integer( sqExame ) );
		
		busca();
	}

	public static boolean existe( Conexao cnx, String nrExame, int sqExame ) throws QtSQLException {

		Query q = new Query( cnx );

		try {
			q.setSQL( "select count(*) from PCMSO.EXAMES_COMPLEMENTARES" );
			q.addSQL( " where NR_EXAME = :prm1NrExame and SQ_EXAME = :prm2SqExame" );

			q.setParameter( "prm1NrExame", nrExame );
			q.setParameter( "prm2SqExame", sqExame );
			q.executeQuery();

			return q.getInt( 1 ) > 0;
		} finally {
			q.liberaRecursos();
		}
	}

	public static boolean existe( String nrExame, int sqExame ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return existe( cnx, nrExame, sqExame );
		} finally {
			cnx.libera();
		}
	}

	public static boolean existe( Conexao cnx, ExameComplementar exameComplementar ) throws QtSQLException {

		return existe( cnx, exameComplementar.getNrExame(), exameComplementar.getSqExame() );
	}

	public static boolean existe( ExameComplementar exameComplementar ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return existe( cnx, exameComplementar.getNrExame(), exameComplementar.getSqExame() );
		} finally {
			cnx.libera();
		}
	}

	public static ExameComplementar buscaExameComplementar( Conexao cnx, String nrExame, int sqExame ) throws QtSQLException {

		Query q = new Query( cnx );

		try {
			q.setSQL( "select * from PCMSO.EXAMES_COMPLEMENTARES" );
			q.addSQL( " where NR_EXAME = :prm1NrExame and SQ_EXAME = :prm2SqExame" );

			q.setParameter( "prm1NrExame", nrExame );
			q.setParameter( "prm2SqExame", sqExame );
			q.executeQuery();

			if( q.isEmpty() ) {
				return null;
			} else {
				ExameComplementar exameComplementar = new ExameComplementar();
				buscaCampos( exameComplementar, q );
				return exameComplementar;
			}
		} finally {
			q.liberaRecursos();
		}
	}

	public static ExameComplementar buscaExameComplementar( String nrExame, int sqExame ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return buscaExameComplementar( cnx, nrExame, sqExame );
		} finally { 
			cnx.libera();
		}
	}

	/** 
	 * Construtor que recebe uma conex�o e as chaves da tabela PCMSO.EXAMES_COMPLEMENTARES
	 * @param nrExame Numero do exame
	 * @param sqExame Sequencia do exame
	 * @param conexao Conexao com o banco de dados
	 */
	public ExameComplementarLocalizador( Conexao conexao, String nrExame, Integer sqExame ) throws QtSQLException {

		super.setConexao( conexao ); 
		exameComplementar = new ExameComplementar();
		this.exameComplementar.setNrExame( nrExame );
		this.exameComplementar.setSqExame( sqExame );
		
		busca();
	}

	/** 
	 * Construtor que recebe as chaves da tabela PCMSO.EXAMES_COMPLEMENTARES
	 * @param nrExame Numero do exame
	 * @param sqExame Sequencia do exame
	 */
	public ExameComplementarLocalizador( String nrExame, Integer sqExame ) throws QtSQLException {

		exameComplementar = new ExameComplementar();
		this.exameComplementar.setNrExame( nrExame );
		this.exameComplementar.setSqExame( sqExame );
		
		busca();
	}

	public static void buscaCampos( ExameComplementar exameComplementar, Query query ) throws QtSQLException {

		exameComplementar.setNrExame( query.getString( "NR_EXAME" ) );
		exameComplementar.setSqExame( query.getInteger( "SQ_EXAME" ) );
		exameComplementar.setCdProcedimento( query.getString( "CD_PROCEDIMENTO" ) );
		exameComplementar.setDtExameComplementar( QtData.criaQtData( query.getTimestamp( "DT_EXAME_COMPLEMENTAR" ) ) );
		exameComplementar.setTpResultado( query.getString( "TP_RESULTADO" ) );
		exameComplementar.setDsResultado( query.getString( "DS_RESULTADO" ) );
	}

	/** M�todo que retorna uma tabela b�sica a que se refere o localizador */
	public TabelaBasica getTabelaBasica() {

		return this.exameComplementar;
	}

	/** M�todo que retorna a classe ExameComplementar relacionada ao localizador */
	public ExameComplementar getExameComplementar() {

		return this.exameComplementar;
	}

	/**
	 * M�todo para buscar e preencher uma tupla de uma tabela
	 * @param tabelaBasica Recebe uma tabela basica com os atributos chave preenchidos
	 */
	public boolean busca( TabelaBasica tabelaBasica ) throws QtSQLException {

		if( !( tabelaBasica instanceof ExameComplementar ) )
			throw new QtSQLException( "Esperava um objeto ExameComplementar!" );

		this.exameComplementar = (ExameComplementar) tabelaBasica;
		return busca();

	}

	/**
	 * M�todo privado para buscar e preencher uma tupla de uma tabela
	 * Este m�todo busca os dados da inst�ncia de ExameComplementar atual
	 */
	private boolean busca() throws QtSQLException {

		boolean ret = false;

		super.estabeleceConexao();
		Query query = super.criaNovaQuery();

		try {

			query.setSQL( "select * from PCMSO.EXAMES_COMPLEMENTARES" );
			query.addSQL( " where NR_EXAME like :prm1NrExame and SQ_EXAME = :prm2SqExame" );

			query.setParameter( "prm1NrExame", exameComplementar.getNrExame() );
			query.setParameter( "prm2SqExame", exameComplementar.getSqExame() );
			query.executeQuery();

			if( query.isEmpty() ) {
				super.setEmpty( true );
				exameComplementar = null;
			} else {
				super.setEmpty( false );
				
				exameComplementar.setNrExame( query.getString( "NR_EXAME" ) );
				exameComplementar.setSqExame( query.getInteger( "SQ_EXAME" ) );
				exameComplementar.setCdProcedimento( query.getString( "CD_PROCEDIMENTO" ) );
				exameComplementar.setDtExameComplementar( QtData.criaQtData( query.getTimestamp( "DT_EXAME_COMPLEMENTAR" ) ) );
				exameComplementar.setTpResultado( query.getString( "TP_RESULTADO" ) );
				exameComplementar.setDsResultado( query.getString( "DS_RESULTADO" ) );
				ret = true;
			}
		} finally {
			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}
 }