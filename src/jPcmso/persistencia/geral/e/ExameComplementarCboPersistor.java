package jPcmso.persistencia.geral.e;

import java.sql.PreparedStatement;

import quatro.persistencia.Persistor;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.util.QtException;

/**
 *
 * Classe persistor para PCMSO.EXAMES_COMPLEMENTARES_POR_CBO
 *
 * @author Samuel Antonio Klein
 * @see Persistor
 * @see ExameComplementarCbo
 * @see ExameComplementarCboNavegador
 * @see ExameComplementarCboLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 public class ExameComplementarCboPersistor extends Persistor {

	/** Inst�ncia de ExameComplementarCbo para manipula��o pelo persistor */
	private ExameComplementarCbo exameComplementarCbo;

	/** Construtor gen�rico */
	public ExameComplementarCboPersistor() {

		this.exameComplementarCbo = null;
	}

	/** Construtor gen�rico */
	public ExameComplementarCboPersistor( Conexao cnx ) throws QtSQLException {

		this.exameComplementarCbo = null;
		super.setConexao( cnx );
	}

	/** Construtor que recebe uma inst�ncia de ExameComplementarCbo */
	public ExameComplementarCboPersistor( ExameComplementarCbo exameComplementarCbo ) throws QtSQLException {

		if( exameComplementarCbo == null ) 
			throw new QtSQLException( "Imposs�vel instanciar ExameComplementarCboPersistor porque est� apontando para um nulo!" );

		this.exameComplementarCbo = exameComplementarCbo;
	}

	/** Construtor que recebe uma conex�o e uma inst�ncia de ExameComplementarCbo */
	public ExameComplementarCboPersistor( Conexao conexao, ExameComplementarCbo exameComplementarCbo ) throws QtSQLException {

		if( exameComplementarCbo == null ) 
			throw new QtSQLException( "Imposs�vel instanciar ExameComplementarCboPersistor porque est� apontando para um nulo!" );

		setConexao( conexao );
		this.exameComplementarCbo = exameComplementarCbo;
	}

	/**
	 * Construtor que recebe as chaves da tabela PCMSO.EXAMES_COMPLEMENTARES_POR_CBO
	 * @param cdProcedimento Codigo do procedimento
	 * @param cdCbo Codigo de CBO
	 */
	public ExameComplementarCboPersistor( String cdProcedimento, String cdCbo )
	  throws QtSQLException { 

		busca( cdProcedimento, cdCbo );
	}

	/**
	 * Construtor que recebe uma conex�o e as chaves da tabela PCMSO.EXAMES_COMPLEMENTARES_POR_CBO
	 * @param cdProcedimento Codigo do procedimento
	 * @param cdCbo Codigo de CBO
	 * @param conexao Conexao com o banco de dados
	 */
	public ExameComplementarCboPersistor( Conexao conexao, String cdProcedimento, String cdCbo )
	  throws QtSQLException { 

		super.setConexao( conexao );
		busca( cdProcedimento, cdCbo );
	}

	/**
	 * M�todo interno para buscar e preencher uma tupla de uma tabela
	 * Utiliza a Inst�ncia de ExameComplementarCbo para fazer a busca
	 * @param cdProcedimento Codigo do procedimento
	 * @param cdCbo Codigo de CBO
	 */
	private void busca( String cdProcedimento, String cdCbo ) throws QtSQLException { 

		ExameComplementarCboLocalizador exameComplementarCboLocalizador = new ExameComplementarCboLocalizador( getConexao(), cdProcedimento, cdCbo );

		if( exameComplementarCboLocalizador.isEmpty() ) {
			throw new QtSQLException( "Chaves da Tabela ExameComplementarCbo n�o encontradas - cdProcedimento: " + cdProcedimento + " / cdCbo: " + cdCbo );
		}

		exameComplementarCbo = (ExameComplementarCbo ) exameComplementarCboLocalizador.getTabelaBasica();
	}

	/**
	 * Permite alimentar o Persistor com um novo registro
	 */
	public void setTabelaBasica( TabelaBasica exameComplementarCbo ) throws QtException {

		if( exameComplementarCbo instanceof ExameComplementarCbo ) {
			this.exameComplementarCbo = (ExameComplementarCbo) exameComplementarCbo;
		} else {
			throw new QtException( "Tabela B�sica fornecida tem que ser uma inst�ncia de ExameComplementarCbo" );
		}
	}

	/**
	 * Permite acessar o registro corrente do persistor
	 */
	public TabelaBasica getTabelaBasica() throws QtException {

		return exameComplementarCbo;
	}

	/**
	 * M�todo para inserir tupla na tabela
	 */
	public void insere() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "insert into PCMSO.EXAMES_COMPLEMENTARES_POR_CBO" );
			cmd.append( "( CD_PROCEDIMENTO, CD_CBO, NR_MES_EXAME, NR_MES_APOS_INCLUSAO )" );
			cmd.append( "values " );
			cmd.append( "( ?, ?, ?, ?)" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			ps.setObject( 1, exameComplementarCbo.getCdProcedimento() );
			ps.setObject( 2, exameComplementarCbo.getCdCbo() );
			ps.setObject( 3, exameComplementarCbo.getNrMesExame() );
			ps.setObject( 4, exameComplementarCbo.getNrMesAposInclusao() );
			
			ps.execute();

		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}

	/**
	 * M�todo para alterar tupla na tabela
	 */
	public void altera() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "update PCMSO.EXAMES_COMPLEMENTARES_POR_CBO" );
			cmd.append( "  set CD_PROCEDIMENTO = ?, " );
			cmd.append( " CD_CBO = ?, " );
			cmd.append( " NR_MES_EXAME = ?, " );
			cmd.append( " NR_MES_APOS_INCLUSAO = ?" );
			cmd.append( " where CD_PROCEDIMENTO like ? and CD_CBO like ?" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			ps.setObject( 1, exameComplementarCbo.getCdProcedimento() );
			ps.setObject( 2, exameComplementarCbo.getCdCbo() );
			ps.setObject( 3, exameComplementarCbo.getNrMesExame() );
			ps.setObject( 4, exameComplementarCbo.getNrMesAposInclusao() );
			
			ps.setObject( 5, exameComplementarCbo.getCdProcedimento() );
			ps.setObject( 6, exameComplementarCbo.getCdCbo() );
			
			ps.execute();

		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}

	/**
	 * M�todo para excluir tupla de uma tabela
	 */
	public void exclui() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "delete from PCMSO.EXAMES_COMPLEMENTARES_POR_CBO" );
			cmd.append( " where CD_PROCEDIMENTO like ? and CD_CBO like ?" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			ps.setObject( 1, exameComplementarCbo.getCdProcedimento() );
			ps.setObject( 2, exameComplementarCbo.getCdCbo() );
			
			ps.execute();

		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}
}