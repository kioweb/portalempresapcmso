package jPcmso.persistencia.geral.e;

import quatro.persistencia.Localizador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.QtSQLException;
import quatro.sql.Query;
import quatro.sql.Conexao;
import quatro.sql.PoolDeConexoes;
import quatro.sql.ConexaoParaPoolDeConexoes;

/**
 *
 * Classe localizador para PCMSO.EXAMES_COMPLEMENTARES_POR_CBO
 *
 * @author Samuel Antonio Klein
 * @see Localizador
 * @see ExameComplementarCbo
 * @see ExameComplementarCboNavegador
 * @see ExameComplementarCboPersistor
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 public class ExameComplementarCboLocalizador extends Localizador {

	/** Inst�ncia de ExameComplementarCbo para manipula��o pelo localizador */
	private ExameComplementarCbo exameComplementarCbo;

	/** Construtor que recebe uma conex�o e um objeto ExameComplementarCbo completo */
	public ExameComplementarCboLocalizador( Conexao conexao, ExameComplementarCbo exameComplementarCbo ) throws QtSQLException {

		super.setConexao( conexao );

		this.exameComplementarCbo = exameComplementarCbo;
		busca();
	}

	/** Construtor que recebe um objeto ExameComplementarCbo completo */
	public ExameComplementarCboLocalizador( ExameComplementarCbo exameComplementarCbo ) throws QtSQLException {

		this.exameComplementarCbo = exameComplementarCbo;
		busca();
	}

	/** 
	 * Construtor que recebe uma conex�o e as chaves da tabela PCMSO.EXAMES_COMPLEMENTARES_POR_CBO
	 * @param cdProcedimento Codigo do procedimento
	 * @param cdCbo Codigo de CBO
	 * @param conexao Conexao com o banco de dados
	 */
	public ExameComplementarCboLocalizador( Conexao conexao, String cdProcedimento, String cdCbo ) throws QtSQLException { 

		super.setConexao( conexao ); 
		exameComplementarCbo = new ExameComplementarCbo();
		this.exameComplementarCbo.setCdProcedimento( cdProcedimento );
		this.exameComplementarCbo.setCdCbo( cdCbo );
		
		busca();
	}

	/** 
	 * Construtor que recebe as chaves da tabela PCMSO.EXAMES_COMPLEMENTARES_POR_CBO
	 * @param cdProcedimento Codigo do procedimento
	 * @param cdCbo Codigo de CBO
	 */
	public ExameComplementarCboLocalizador( String cdProcedimento, String cdCbo ) throws QtSQLException { 

		exameComplementarCbo = new ExameComplementarCbo();
		this.exameComplementarCbo.setCdProcedimento( cdProcedimento );
		this.exameComplementarCbo.setCdCbo( cdCbo );
		
		busca();
	}

	public static boolean existe( Conexao cnx, String cdProcedimento, String cdCbo ) throws QtSQLException {

		Query q = new Query( cnx );

		try {
			q.setSQL( "select count(*) from PCMSO.EXAMES_COMPLEMENTARES_POR_CBO" );
			q.addSQL( " where CD_PROCEDIMENTO = :prm1CdProcedimento and CD_CBO = :prm2CdCbo" );

			q.setParameter( "prm1CdProcedimento", cdProcedimento );
			q.setParameter( "prm2CdCbo", cdCbo );
			q.executeQuery();

			return q.getInt( 1 ) > 0;
		} finally {
			q.liberaRecursos();
		}
	}

	public static boolean existe( String cdProcedimento, String cdCbo ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return existe( cnx, cdProcedimento, cdCbo );
		} finally {
			cnx.libera();
		}
	}

	public static boolean existe( Conexao cnx, ExameComplementarCbo exameComplementarCbo ) throws QtSQLException {

		return existe( cnx, exameComplementarCbo.getCdProcedimento(), exameComplementarCbo.getCdCbo() );
	}

	public static boolean existe( ExameComplementarCbo exameComplementarCbo ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return existe( cnx, exameComplementarCbo.getCdProcedimento(), exameComplementarCbo.getCdCbo() );
		} finally {
			cnx.libera();
		}
	}

	public static ExameComplementarCbo buscaExameComplementarCbo( Conexao cnx, String cdProcedimento, String cdCbo ) throws QtSQLException {

		Query q = new Query( cnx );

		try {
			q.setSQL( "select * from PCMSO.EXAMES_COMPLEMENTARES_POR_CBO" );
			q.addSQL( " where CD_PROCEDIMENTO = :prm1CdProcedimento and CD_CBO = :prm2CdCbo" );

			q.setParameter( "prm1CdProcedimento", cdProcedimento );
			q.setParameter( "prm2CdCbo", cdCbo );
			q.executeQuery();

			if( q.isEmpty() ) {
				return null;
			} else {
				ExameComplementarCbo exameComplementarCbo = new ExameComplementarCbo();
				buscaCampos( exameComplementarCbo, q );
				return exameComplementarCbo;
			}
		} finally {
			q.liberaRecursos();
		}
	}

	public static ExameComplementarCbo buscaExameComplementarCbo( String cdProcedimento, String cdCbo ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return buscaExameComplementarCbo( cnx, cdProcedimento, cdCbo );
		} finally { 
			cnx.libera();
		}
	}

	public static void buscaCampos( ExameComplementarCbo exameComplementarCbo, Query query ) throws QtSQLException {

		exameComplementarCbo.setCdProcedimento( query.getString( "CD_PROCEDIMENTO" ) );
		exameComplementarCbo.setCdCbo( query.getString( "CD_CBO" ) );
		exameComplementarCbo.setNrMesExame( query.getInteger( "NR_MES_EXAME" ) );
		exameComplementarCbo.setNrMesAposInclusao( query.getInteger( "NR_MES_APOS_INCLUSAO" ) );
	}

	/** M�todo que retorna uma tabela b�sica a que se refere o localizador */
	public TabelaBasica getTabelaBasica() {

		return this.exameComplementarCbo;
	}

	/** M�todo que retorna a classe ExameComplementarCbo relacionada ao localizador */
	public ExameComplementarCbo getExameComplementarCbo() {

		return this.exameComplementarCbo;
	}

	/**
	 * M�todo para buscar e preencher uma tupla de uma tabela
	 * @param tabelaBasica Recebe uma tabela basica com os atributos chave preenchidos
	 */
	public boolean busca( TabelaBasica tabelaBasica ) throws QtSQLException {

		if( !( tabelaBasica instanceof ExameComplementarCbo ) )
			throw new QtSQLException( "Esperava um objeto ExameComplementarCbo!" );

		this.exameComplementarCbo = (ExameComplementarCbo) tabelaBasica;
		return busca();

	}

	/**
	 * M�todo privado para buscar e preencher uma tupla de uma tabela
	 * Este m�todo busca os dados da inst�ncia de ExameComplementarCbo atual
	 */
	private boolean busca() throws QtSQLException {

		boolean ret = false;

		super.estabeleceConexao();
		Query query = super.criaNovaQuery();

		try {

			query.setSQL( "select * from PCMSO.EXAMES_COMPLEMENTARES_POR_CBO" );
			query.addSQL( " where CD_PROCEDIMENTO like :prm1CdProcedimento and CD_CBO like :prm2CdCbo" );

			query.setParameter( "prm1CdProcedimento", exameComplementarCbo.getCdProcedimento() );
			query.setParameter( "prm2CdCbo", exameComplementarCbo.getCdCbo() );
			query.executeQuery();

			if( query.isEmpty() ) {
				super.setEmpty( true );
				exameComplementarCbo = null;
			} else {
				super.setEmpty( false );
				
				exameComplementarCbo.setCdProcedimento( query.getString( "CD_PROCEDIMENTO" ) );
				exameComplementarCbo.setCdCbo( query.getString( "CD_CBO" ) );
				exameComplementarCbo.setNrMesExame( query.getInteger( "NR_MES_EXAME" ) );
				exameComplementarCbo.setNrMesAposInclusao( query.getInteger( "NR_MES_APOS_INCLUSAO" ) );
				ret = true;
			}
		} finally {
			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}
 }