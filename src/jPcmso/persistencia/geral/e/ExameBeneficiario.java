package jPcmso.persistencia.geral.e;

import quatro.persistencia.TabelaBasica;
import quatro.smartDB.ColumnName;
import quatro.smartDB.IgnoredField;
import quatro.smartDB.PrimaryKey;
import quatro.smartDB.Table;

/**
 *
 * Classe b�sica para PCMSO.EXAMES_BENEFICIARIO
 *
 * Tabela de Exames BENEFICIARIO 
 * Estes exames s�o os que REALMENTE ser�o aplicados ao beneficiario. S�o uma sele��o retirada de EXAMES_COMPLEMENTARES_POR_CBO
 * 
 * @author Samuel Antonio Klein
 * @see ExameBeneficiarioNavegador
 * @see ExameBeneficiarioPersistor
 * @see ExameBeneficiarioLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 @Table(name="PCMSO.EXAMES_BENEFICIARIO")
 public class ExameBeneficiario implements TabelaBasica {

	@IgnoredField
	private static final long serialVersionUID = -15762305417271297L;

	/** Codigo de beneficiario  (chave-char(12)) */
	@PrimaryKey
	private String cdBeneficiarioCartao;
	/** Codigo do procedimento (chave-char(8)) */
	@PrimaryKey
	private String cdProcedimento;
	/** 
	 * N�mero de meses
	 * 
	 * Peridiocidade de meses entre um exame e outro
	 */
	private Integer nrMesExame;
	/** Meses de Renova��o de Exame ap�s a inclus�o */
	private Integer nrMesAposInclusao;

	/**
	 * Alimenta Codigo de beneficiario 
	 * @param cdBeneficiarioCartao Codigo de beneficiario 
	 */ 
	public void setCdBeneficiarioCartao( String cdBeneficiarioCartao ) {
		this.cdBeneficiarioCartao = cdBeneficiarioCartao;
	}

	/**
	 * Retorna Codigo de beneficiario 
	 * @return Codigo de beneficiario 
	 */
	public String getCdBeneficiarioCartao() {
		return this.cdBeneficiarioCartao;
	}

	/**
	 * Alimenta Codigo do procedimento
	 * @param cdProcedimento Codigo do procedimento
	 */ 
	public void setCdProcedimento( String cdProcedimento ) {
		this.cdProcedimento = cdProcedimento;
	}

	/**
	 * Retorna Codigo do procedimento
	 * @return Codigo do procedimento
	 */
	public String getCdProcedimento() {
		return this.cdProcedimento;
	}

	/**
	 * Alimenta N�mero de meses
	 * @param nrMesExame N�mero de meses
	 */ 
	public void setNrMesExame( Integer nrMesExame ) {
		this.nrMesExame = nrMesExame;
	}

	/**
	 * Retorna N�mero de meses
	 * @return N�mero de meses
	 */
	public Integer getNrMesExame() {
		return this.nrMesExame;
	}

	/**
	 * Alimenta Meses de Renova��o de Exame ap�s a inclus�o
	 * @param nrMesAposInclusao Meses de Renova��o de Exame ap�s a inclus�o
	 */ 
	public void setNrMesAposInclusao( Integer nrMesAposInclusao ) {
		this.nrMesAposInclusao = nrMesAposInclusao;
	}

	/**
	 * Retorna Meses de Renova��o de Exame ap�s a inclus�o
	 * @return Meses de Renova��o de Exame ap�s a inclus�o
	 */
	public Integer getNrMesAposInclusao() {
		return this.nrMesAposInclusao;
	}

}