package jPcmso.persistencia.geral.e;

import quatro.persistencia.FiltroDeNavegador;

/**
 *
 * Classe filtro para PCMSO.EMPRESAS
 *
 * @author Samuel Antonio Klein
 * @see FiltroDeTabela
 * @see Empresa
 * @see EmpresaNavegador
 * @see EmpresaPersistor
 * @see EmpresaLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 public class EmpresaFiltro extends FiltroDeNavegador {

	/** Determina a ordem de navega��o que ser� passada para o navegador */
	public void setOrdemDeNavegacao( int idOrdem ) {

		super.setOrdemDeNavegacao( idOrdem );
	}
}