package jPcmso.persistencia.geral.e;

import quatro.persistencia.Caminhador;
import quatro.persistencia.FiltroDeNavegador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;

 /**
 *
 * Classe Caminhador para ExameBeneficiario
 *
 * @author Samuel Antonio Klein
 * @see Caminhador
 * @see FiltroDeNavegador
 * @see ExameBeneficiarioNavegador
 * @see ExameBeneficiarioPersistor
 * @see ExameBeneficiarioLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 public class ExameBeneficiarioCaminhador extends Caminhador {

	public static final int POR_ORDEM_DEFAULT = -1;
	
	private ExameBeneficiario exameBeneficiario;

	public ExameBeneficiarioCaminhador( FiltroDeNavegador filtro ) {

		setFiltro( filtro );
		inicializaVisao();
	}

	public ExameBeneficiarioCaminhador( Conexao conexao, FiltroDeNavegador filtro ) throws QtSQLException {

		super.setConexao( conexao );
		setFiltro( filtro );
		inicializaVisao();
	}

	private void inicializaVisao() {

		setNmVisao( "PCMSO.EXAMES_BENEFICIARIO" );
	}

	protected void carregaCampos() throws QtSQLException {

		exameBeneficiario = new ExameBeneficiario();

		if( isRecordAvailable() ) {
			exameBeneficiario.setCdBeneficiarioCartao( query.getString( "CD_BENEFICIARIO_CARTAO" ) );
			exameBeneficiario.setCdProcedimento( query.getString( "CD_PROCEDIMENTO" ) );
			exameBeneficiario.setNrMesExame( query.getInteger( "NR_MES_EXAME" ) );
			exameBeneficiario.setNrMesAposInclusao( query.getInteger( "NR_MES_APOS_INCLUSAO" ) );
		}
	}

	/** M�todo que retorna uma tabela b�sica a que se refere o caminhador */
	public TabelaBasica getTabelaBasica() {

		return this.exameBeneficiario;
	}

	/** M�todo que retorna a classe ExameBeneficiario relacionada ao caminhador */
	public ExameBeneficiario getExameBeneficiario() {

		return this.exameBeneficiario;
	}

	public void ativaVisao() throws QtSQLException {

		if(  super.getOrdemDeNavegacao() == ExameBeneficiarioCaminhador.POR_ORDEM_DEFAULT ) {
			super.ativaCaminhador( "order by CD_BENEFICIARIO_CARTAO, CD_PROCEDIMENTO" );
	
		} else {
			throw new QtSQLException( "ExameBeneficiarioCaminhador - Ordem de Navega��o n�o definida!" );
		}
	}

	public String getCamposIndice( int qualIndice ) {
		return getDsIndice( qualIndice );
	}

	public String getCamposChave() {
		return getDsChave();
	}

	/**
	 * M�todo est�tico que devolve os campos que comp�e a chave da tabela;
	 * @return campos que comp�e a chave.
	 */
	public static String getDsChave() {
		return "CD_BENEFICIARIO_CARTAO, CD_PROCEDIMENTO";
	}

	/**
	 * M�todo est�tico que devolve os campos que fazem parte de uma dada ordem
	 * de navega��o.
	 * @param qualIndice indica a ordem de navega��o desejada.
	 * @return campos que comp�e a ordem de navega��o.
	 */
	public static String getDsIndice( int qualIndice ) {

		switch( qualIndice ) {

			case POR_ORDEM_DEFAULT: return "CD_BENEFICIARIO_CARTAO, CD_PROCEDIMENTO";
		}
		return null;
	}
}