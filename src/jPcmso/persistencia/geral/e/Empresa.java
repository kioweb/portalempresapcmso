package jPcmso.persistencia.geral.e;

import quatro.persistencia.TabelaBasica;
import quatro.smartDB.IgnoredField;
import quatro.smartDB.PrimaryKey;
import quatro.smartDB.Table;
import quatro.util.QtData;
import quatro.util.QtString;

/**
 *
 * Classe b�sica para PCMSO.EMPRESAS
 *
 * Tabela de empresas
 * 
 * @author Samuel Antonio Klein
 * @see EmpresaNavegador
 * @see EmpresaPersistor
 * @see EmpresaLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 @Table(name="PCMSO.EMPRESAS")
 public class Empresa implements TabelaBasica {

	@IgnoredField
	private static final long serialVersionUID = -402667524547651591L;

	/** Codigo da Empresa (chave-char(4)) */
	@PrimaryKey
	private String cdEmpresa;
	/** Descricao da Empresa (varchar(250)) */
	private String dsEmpresa;
	/** Cnpj (char(14)) */
	private String nrCnpj;
	/** Cnae (char(7)) */
	private String nrCnae;
	/** Data de inclusao */
	private QtData dtInclusao;
	/** N�mero do NIT (PIS) do Representante (char(11)) */
	private String nrNitRespresentante;
	/** N�mero do CPF do Representante (char(12)) */
	private String nrCpfRepresentante;
	/** Nome do Representante (varchar(250)) */
	private String dsNomeRepresentante;
	/** Medico coordenador (char(6)) */
	private String cdMedicoCoordenador;
	/** contrato de pacote (char(1)) */
	private boolean snContratoPacote;

	/**
	 * Alimenta Codigo da Empresa
	 * @param cdEmpresa Codigo da Empresa
	 */ 
	public void setCdEmpresa( String cdEmpresa ) {
		this.cdEmpresa = cdEmpresa;
	}

	/**
	 * Retorna Codigo da Empresa
	 * @return Codigo da Empresa
	 */
	public String getCdEmpresa() {
		return this.cdEmpresa;
	}

	/**
	 * Alimenta Descricao da Empresa
	 * @param dsEmpresa Descricao da Empresa
	 */ 
	public void setDsEmpresa( String dsEmpresa ) {
		this.dsEmpresa = dsEmpresa;
	}

	/**
	 * Retorna Descricao da Empresa
	 * @return Descricao da Empresa
	 */
	public String getDsEmpresa() {
		return this.dsEmpresa;
	}

	/**
	 * Alimenta Cnpj
	 * @param nrCnpj Cnpj
	 */ 
	public void setNrCnpj( String nrCnpj ) {
		this.nrCnpj = nrCnpj;
	}

	/**
	 * Retorna Cnpj
	 * @return Cnpj
	 */
	public String getNrCnpj() {
		return this.nrCnpj;
	}

	/**
	 * Alimenta Cnae
	 * @param nrCnae Cnae
	 */ 
	public void setNrCnae( String nrCnae ) {
		this.nrCnae = nrCnae;
	}

	/**
	 * Retorna Cnae
	 * @return Cnae
	 */
	public String getNrCnae() {
		return this.nrCnae;
	}

	/**
	 * Alimenta Data de inclusao
	 * @param dtInclusao Data de inclusao
	 */ 
	public void setDtInclusao( QtData dtInclusao ) {
		this.dtInclusao = dtInclusao;
	}

	/**
	 * Retorna Data de inclusao
	 * @return Data de inclusao
	 */
	public QtData getDtInclusao() {
		return this.dtInclusao;
	}

	/**
	 * Alimenta N�mero do NIT (PIS) do Representante
	 * @param nrNitRespresentante N�mero do NIT (PIS) do Representante
	 */ 
	public void setNrNitRespresentante( String nrNitRespresentante ) {
		this.nrNitRespresentante = nrNitRespresentante;
	}

	/**
	 * Retorna N�mero do NIT (PIS) do Representante
	 * @return N�mero do NIT (PIS) do Representante
	 */
	public String getNrNitRespresentante() {
		return this.nrNitRespresentante;
	}

	/**
	 * Alimenta N�mero do CPF do Representante
	 * @param nrCpfRepresentante N�mero do CPF do Representante
	 */ 
	public void setNrCpfRepresentante( String nrCpfRepresentante ) {
		this.nrCpfRepresentante = nrCpfRepresentante;
	}

	/**
	 * Retorna N�mero do CPF do Representante
	 * @return N�mero do CPF do Representante
	 */
	public String getNrCpfRepresentante() {
		return this.nrCpfRepresentante;
	}

	/**
	 * Alimenta Nome do Representante
	 * @param dsNomeRepresentante Nome do Representante
	 */ 
	public void setDsNomeRepresentante( String dsNomeRepresentante ) {
		this.dsNomeRepresentante = dsNomeRepresentante;
	}

	/**
	 * Retorna Nome do Representante
	 * @return Nome do Representante
	 */
	public String getDsNomeRepresentante() {
		return this.dsNomeRepresentante;
	}

	/**
	 * Alimenta Medico coordenador
	 * @param cdMedicoCoordenador Medico coordenador
	 */ 
	public void setCdMedicoCoordenador( String cdMedicoCoordenador ) {
		this.cdMedicoCoordenador = cdMedicoCoordenador;
	}

	/**
	 * Retorna Medico coordenador
	 * @return Medico coordenador
	 */
	public String getCdMedicoCoordenador() {
		return this.cdMedicoCoordenador;
	}

	/**
	 * Alimenta contrato de pacote
	 * @param snContratoPacote contrato de pacote
	 */ 
	public void setSnContratoPacote( String snContratoPacote ) {
		this.snContratoPacote = QtString.toBoolean( snContratoPacote );
	}

	/**
	 * Retorna contrato de pacote
	 * @return contrato de pacote
	 */
	public boolean isSnContratoPacote() {
		return this.snContratoPacote;
	}

}