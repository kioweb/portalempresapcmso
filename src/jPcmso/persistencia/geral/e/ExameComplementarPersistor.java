package jPcmso.persistencia.geral.e;

import java.sql.PreparedStatement;

import quatro.persistencia.Persistor;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.ConexaoParaPoolDeConexoes;
import quatro.sql.PoolDeConexoes;
import quatro.sql.QtSQLException;
import quatro.sql.Query;
import quatro.util.QtData;
import quatro.util.QtException;

/**
 *
 * Classe persistor para PCMSO.EXAMES_COMPLEMENTARES
 *
 * @author Samuel Antonio Klein
 * @see Persistor
 * @see ExameComplementar
 * @see ExameComplementarNavegador
 * @see ExameComplementarLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 public class ExameComplementarPersistor extends Persistor {

	/** Inst�ncia de ExameComplementar para manipula��o pelo persistor */
	private ExameComplementar exameComplementar;

	/** Construtor gen�rico */
	public ExameComplementarPersistor() {

		this.exameComplementar = null;
	}

	/** Construtor gen�rico */
	public ExameComplementarPersistor( Conexao cnx ) throws QtSQLException {

		this.exameComplementar = null;
		super.setConexao( cnx );
	}

	/** Construtor que recebe uma inst�ncia de ExameComplementar */
	public ExameComplementarPersistor( ExameComplementar exameComplementar ) throws QtSQLException {

		if( exameComplementar == null ) 
			throw new QtSQLException( "Imposs�vel instanciar ExameComplementarPersistor porque est� apontando para um nulo!" );

		this.exameComplementar = exameComplementar;
	}

	/** Construtor que recebe uma conex�o e uma inst�ncia de ExameComplementar */
	public ExameComplementarPersistor( Conexao conexao, ExameComplementar exameComplementar ) throws QtSQLException {

		if( exameComplementar == null ) 
			throw new QtSQLException( "Imposs�vel instanciar ExameComplementarPersistor porque est� apontando para um nulo!" );

		setConexao( conexao );
		this.exameComplementar = exameComplementar;
	}

	/**
	 * Construtor que recebe as chaves da tabela PCMSO.EXAMES_COMPLEMENTARES
	 * @param nrExame Numero do exame
	 * @param sqExame Sequencia do exame
	 */
	public ExameComplementarPersistor( String nrExame, int sqExame )
	  throws QtSQLException { 

		busca( nrExame, sqExame );
	}

	/**
	 * Construtor que recebe uma conex�o e as chaves da tabela PCMSO.EXAMES_COMPLEMENTARES
	 * @param nrExame Numero do exame
	 * @param sqExame Sequencia do exame
	 * @param conexao Conexao com o banco de dados
	 */
	public ExameComplementarPersistor( Conexao conexao, String nrExame, int sqExame )
	  throws QtSQLException { 

		super.setConexao( conexao );
		busca( nrExame, sqExame );
	}

	/**
	 * M�todo interno para buscar e preencher uma tupla de uma tabela
	 * Utiliza a Inst�ncia de ExameComplementar para fazer a busca
	 * @param nrExame Numero do exame
	 * @param sqExame Sequencia do exame
	 */
	private void busca( String nrExame, int sqExame ) throws QtSQLException { 

		ExameComplementarLocalizador exameComplementarLocalizador = new ExameComplementarLocalizador( getConexao(), nrExame, sqExame );

		if( exameComplementarLocalizador.isEmpty() ) {
			throw new QtSQLException( "Chaves da Tabela ExameComplementar n�o encontradas - nrExame: " + nrExame + " / sqExame: " + sqExame );
		}

		exameComplementar = (ExameComplementar ) exameComplementarLocalizador.getTabelaBasica();
	}

	/**
	 * Permite alimentar o Persistor com um novo registro
	 */
	public void setTabelaBasica( TabelaBasica exameComplementar ) throws QtException {

		if( exameComplementar instanceof ExameComplementar ) {
			this.exameComplementar = (ExameComplementar) exameComplementar;
		} else {
			throw new QtException( "Tabela B�sica fornecida tem que ser uma inst�ncia de ExameComplementar" );
		}
	}

	/**
	 * Permite acessar o registro corrente do persistor
	 */
	public TabelaBasica getTabelaBasica() throws QtException {

		return exameComplementar;
	}

	/**
	 * M�todo para gerar automaticamente uma nova chave
	 */
	public static Integer geraNovoSqExame( Conexao cnx, String nrExame ) throws QtSQLException {

		Integer result = null;

		Query query = new Query( cnx );

		try{

			query.setSQL( "select max( SQ_EXAME ) from PCMSO.EXAMES_COMPLEMENTARES" );

			query.executeQuery();
			result = new Integer( query.getInt( 1 ) + 1 );
		} catch( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			query.liberaRecursos();
		}
		return result;
	}

	/**
	 * M�todo para gerar automaticamente uma nova chave
	 */
	public static Integer geraNovoSqExame( String nrExame ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return geraNovoSqExame(  cnx, nrExame );
		} finally {
			cnx.libera();
		}
	}


	/**
	 * M�todo para inserir tupla na tabela
	 */
	public void insere() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "insert into PCMSO.EXAMES_COMPLEMENTARES" );
			cmd.append( "( NR_EXAME, SQ_EXAME, CD_PROCEDIMENTO, DT_EXAME_COMPLEMENTAR, TP_RESULTADO, DS_RESULTADO )" );
			cmd.append( "values " );
			cmd.append( "( ?, ?, ?, ?, ?, ?)" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			ps.setObject( 1, exameComplementar.getNrExame() );
			ps.setObject( 2, exameComplementar.getSqExame() );
			ps.setObject( 3, exameComplementar.getCdProcedimento() );
			ps.setObject( 4, QtData.getQtDataAsTimestamp( exameComplementar.getDtExameComplementar() ) );
			ps.setObject( 5, exameComplementar.getTpResultado() );
			ps.setObject( 6, exameComplementar.getDsResultado() );
			
			ps.execute();

		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}

	/**
	 * M�todo para alterar tupla na tabela
	 */
	public void altera() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "update PCMSO.EXAMES_COMPLEMENTARES" );
			cmd.append( "  set NR_EXAME = ?, " );
			cmd.append( " SQ_EXAME = ?, " );
			cmd.append( " CD_PROCEDIMENTO = ?, " );
			cmd.append( " DT_EXAME_COMPLEMENTAR = ?, " );
			cmd.append( " TP_RESULTADO = ?, " );
			cmd.append( " DS_RESULTADO = ?" );
			cmd.append( " where NR_EXAME like ? and SQ_EXAME = ?" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			ps.setObject( 1, exameComplementar.getNrExame() );
			ps.setObject( 2, exameComplementar.getSqExame() );
			ps.setObject( 3, exameComplementar.getCdProcedimento() );
			ps.setObject( 4, QtData.getQtDataAsTimestamp( exameComplementar.getDtExameComplementar() ) );
			ps.setObject( 5, exameComplementar.getTpResultado() );
			ps.setObject( 6, exameComplementar.getDsResultado() );
			
			ps.setObject( 7, exameComplementar.getNrExame() );
			ps.setObject( 8, exameComplementar.getSqExame() );
			
			ps.execute();

		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}

	/**
	 * M�todo para excluir tupla de uma tabela
	 */
	public void exclui() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "delete from PCMSO.EXAMES_COMPLEMENTARES" );
			cmd.append( " where NR_EXAME like ? and SQ_EXAME = ?" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			ps.setObject( 1, exameComplementar.getNrExame() );
			ps.setObject( 2, exameComplementar.getSqExame() );
			
			ps.execute();

		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}
}