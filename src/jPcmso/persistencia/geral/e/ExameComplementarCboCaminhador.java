package jPcmso.persistencia.geral.e;

import quatro.persistencia.Caminhador;
import quatro.persistencia.FiltroDeNavegador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;

 /**
 *
 * Classe Caminhador para ExameComplementarCbo
 *
 * @author Samuel Antonio Klein
 * @see Caminhador
 * @see FiltroDeNavegador
 * @see ExameComplementarCboNavegador
 * @see ExameComplementarCboPersistor
 * @see ExameComplementarCboLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 public class ExameComplementarCboCaminhador extends Caminhador {

	public static final int POR_ORDEM_DEFAULT = -1;
	
	private ExameComplementarCbo exameComplementarCbo;

	public ExameComplementarCboCaminhador( FiltroDeNavegador filtro ) {

		setFiltro( filtro );
		inicializaVisao();
	}

	public ExameComplementarCboCaminhador( Conexao conexao, FiltroDeNavegador filtro ) throws QtSQLException {

		super.setConexao( conexao );
		setFiltro( filtro );
		inicializaVisao();
	}

	private void inicializaVisao() {

		setNmVisao( "PCMSO.EXAMES_COMPLEMENTARES_POR_CBO" );
	}

	protected void carregaCampos() throws QtSQLException {

		exameComplementarCbo = new ExameComplementarCbo();

		if( isRecordAvailable() ) {
			exameComplementarCbo.setCdProcedimento( query.getString( "CD_PROCEDIMENTO" ) );
			exameComplementarCbo.setCdCbo( query.getString( "CD_CBO" ) );
			exameComplementarCbo.setNrMesExame( query.getInteger( "NR_MES_EXAME" ) );
			exameComplementarCbo.setNrMesAposInclusao( query.getInteger( "NR_MES_APOS_INCLUSAO" ) );
		}
	}

	/** M�todo que retorna uma tabela b�sica a que se refere o caminhador */
	public TabelaBasica getTabelaBasica() {

		return this.exameComplementarCbo;
	}

	/** M�todo que retorna a classe ExameComplementarCbo relacionada ao caminhador */
	public ExameComplementarCbo getExameComplementarCbo() {

		return this.exameComplementarCbo;
	}

	public void ativaVisao() throws QtSQLException {

		if(  super.getOrdemDeNavegacao() == ExameComplementarCboCaminhador.POR_ORDEM_DEFAULT ) {
			super.ativaCaminhador( "order by CD_PROCEDIMENTO, CD_CBO" );
	
		} else {
			throw new QtSQLException( "ExameComplementarCboCaminhador - Ordem de Navega��o n�o definida!" );
		}
	}

	public String getCamposIndice( int qualIndice ) {
		return getDsIndice( qualIndice );
	}

	public String getCamposChave() {
		return getDsChave();
	}

	/**
	 * M�todo est�tico que devolve os campos que comp�e a chave da tabela;
	 * @return campos que comp�e a chave.
	 */
	public static String getDsChave() {
		return "CD_PROCEDIMENTO, CD_CBO";
	}

	/**
	 * M�todo est�tico que devolve os campos que fazem parte de uma dada ordem
	 * de navega��o.
	 * @param qualIndice indica a ordem de navega��o desejada.
	 * @return campos que comp�e a ordem de navega��o.
	 */
	public static String getDsIndice( int qualIndice ) {

		switch( qualIndice ) {

			case POR_ORDEM_DEFAULT: return "CD_PROCEDIMENTO, CD_CBO";
		}
		return null;
	}
}