package jPcmso.persistencia.geral.e;

import quatro.persistencia.FiltroDeNavegador;
import quatro.persistencia.Navegador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.sql.Query;
import quatro.util.QtData;

/**
 *
 * Classe navegador para PCMSO.EMPRESAS
 *
 * @author Samuel Antonio Klein
 * @see Navegador
 * @see Empresa
 * @see EmpresaPersistor
 * @see EmpresaLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 public class EmpresaNavegador extends Navegador {

	
	/** Inst�ncia de Empresa para manipula��o pelo navegador */
	private Empresa empresa;

	/** Construtor b�sico */
	public EmpresaNavegador()
	  throws QtSQLException { 

		empresa = new Empresa();
		primeiro();
	}

	/** Construtor b�sico que recebe uma conex�o */
	public EmpresaNavegador( Conexao conexao )
	  throws QtSQLException { 

		super.setConexao( conexao ); 
		empresa = new Empresa();
		primeiro();
	}

	/**
	 * Construtor que recebe uma inst�ncia de EmpresaFiltro e posiciona
	 * no primeiro registro que satisfaz suas condi��es 
	 */
	public EmpresaNavegador( FiltroDeNavegador filtro ) throws QtSQLException  {
		super.setFiltro( filtro );
		this.empresa = new Empresa();
		primeiro();
	}

	/** Construtor que recebe a chave da tabela PCMSO.EMPRESAS
	 * @param cdEmpresa Codigo da Empresa
	 */
	public EmpresaNavegador( String cdEmpresa )
	  throws QtSQLException { 

		empresa = new Empresa();
		this.empresa.setCdEmpresa( cdEmpresa );
		
		busca();
	}

	/** Construtor que recebe  uma conex�o e a chave da tabela PCMSO.EMPRESAS
	 * @param cdEmpresa Codigo da Empresa
	 * @param conexao Conexao com o banco de dados
	 */
	public EmpresaNavegador( Conexao conexao, String cdEmpresa )
	  throws QtSQLException { 

		super.setConexao( conexao ); 
		empresa = new Empresa();
		this.empresa.setCdEmpresa( cdEmpresa );
		
		busca();
	}

	/** M�todo que retorna uma tabela b�sica a que se refere o navegador */
	public TabelaBasica getTabelaBasica() {

		return this.empresa;
	}

	/** M�todo que retorna a classe Empresa relacionada ao navegador */
	public Empresa getEmpresa() {

		return this.empresa;
	}

	 /** M�todo que move para o primeiro registro da tabela */
	 public boolean primeiro() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select min( CD_EMPRESA ) from PCMSO.EMPRESAS" );
			query.addSQL( super.getCondicaoDeFiltro( FiltroDeNavegador.COM_WHERE ) 	);

			query.executeQuery();

			
			if( query.getString( 1 ) == null ) {

				super.setQueryStates( false, true, true );
			} else {
				this.empresa = new Empresa();

				empresa.setCdEmpresa( query.getString( 1 ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o registro anterior na tabela */
	 public boolean anterior() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select max( CD_EMPRESA ) from PCMSO.EMPRESAS" );
			query.addSQL( " where CD_EMPRESA < :prm1CdEmpresa" );
			query.addSQL( super.getCondicaoDeFiltro() );

			query.setParameter( "prm1CdEmpresa", empresa.getCdEmpresa() );
			
			query.executeQuery();

			
			if( query.getString( 1 ) == null ) {

				super.setFound( false );
				super.setBof( true );
			} else {
				this.empresa = new Empresa();

				empresa.setCdEmpresa( query.getString( 1 ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o proximo registro da tabela */
	 public boolean proximo() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select min( CD_EMPRESA ) from PCMSO.EMPRESAS" );
			query.addSQL( " where CD_EMPRESA > :prm1CdEmpresa" );
			query.addSQL( super.getCondicaoDeFiltro() );

			query.setParameter( "prm1CdEmpresa", empresa.getCdEmpresa() );

			query.executeQuery();

			
			if( query.getString( 1 ) == null ) {

				super.setFound( false );
				super.setEof( true );
			} else {
				this.empresa = new Empresa();

				empresa.setCdEmpresa( query.getString( 1 ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o �ltimo registro da tabela */
	 public boolean ultimo() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select max( CD_EMPRESA ) from PCMSO.EMPRESAS" );
			query.addSQL( super.getCondicaoDeFiltro( FiltroDeNavegador.COM_WHERE ) 	);

			query.executeQuery();

			
			if( query.getString( 1 ) == null ) {

				super.setQueryStates( false, true, true );
			} else {
				this.empresa = new Empresa();

				empresa.setCdEmpresa( query.getString( 1 ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	/**
	 * M�todo para buscar e preencher uma tupla de uma tabela
	 * @param tabelaBasica Recebe uma tabela basica com os atributos chave preenchidos
	 */
	public boolean busca( TabelaBasica tabelaBasica ) throws QtSQLException {

		if( !( tabelaBasica instanceof Empresa ) )
			throw new QtSQLException( "Esperava um objeto Empresa!" );

		this.empresa = (Empresa) tabelaBasica;
		return busca();

	}

	/**
	 * m�todo interno para buscar e preencher uma tupla de uma tabela
	 * Utiliza a Inst�ncia de Empresa da classe para fazer a busca
	 */
	private boolean busca() throws QtSQLException {

		EmpresaLocalizador localizador = new EmpresaLocalizador( getConexao(), this.empresa );

		if( localizador.isEmpty() ) {

			super.setQueryStates( false, true, true );
			return false;

		} else {

			super.setQueryStates( true, false, false );
			return true;
		}
	}
}