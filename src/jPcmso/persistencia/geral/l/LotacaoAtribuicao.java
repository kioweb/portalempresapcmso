package jPcmso.persistencia.geral.l;

import quatro.persistencia.TabelaBasica;
import quatro.smartDB.IgnoredField;
import quatro.smartDB.PrimaryKey;
import quatro.smartDB.Table;
import quatro.util.QtData;

/**
 *
 * Classe b�sica para PCMSO.LOTACAO_ATRIBUICAO
 *
 * Tabela de Beneficiarios CBO
 * 
 * @author Samuel Antonio Klein
 * @see LotacaoAtribuicaoNavegador
 * @see LotacaoAtribuicaoPersistor
 * @see LotacaoAtribuicaoLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:31 - importado por Samuel Antonio Klein
 *
 */
 @Table(name="PCMSO.LOTACAO_ATRIBUICAO")
 public class LotacaoAtribuicao implements TabelaBasica {

	@IgnoredField
	private static final long serialVersionUID = -264770068500311L;

	/** Codigo de Beneficiario  (chave-char(12)) */
	@PrimaryKey
	private String cdBeneficiarioCartao;
	/** sequencia perfil (chave) */
	@PrimaryKey
	private Integer sqPerfil;
	/** sequencia lotacao (chave-char(20)) */
	@PrimaryKey
	private String sqLotacao;
	/** data de Inicio */
	private QtData dtInicio;
	/** data final */
	private QtData dtFinal;
	/** Codigo de Empresa (char(4)) */
	private String cdEmpresa;
	/** CNPJ do Domic�lio Tribut�rio/CEI: (char(14)) */
	private String nrCnpj;
	/** Codigo do setor */
	private Integer idSetor;
	/** Codigo do cargo */
	private Integer idCargo;
	/** Codigo do fun��o */
	private Integer idFuncao;
	/** Codigo do CBO (char(6)) */
	private String cdCbo;
	/** Codigo do GFIP (char(2)) */
	private String cdGfip;
	/** Descri��o das Atividades */
	private String dsAtividade;

	/**
	 * Alimenta Codigo de Beneficiario 
	 * @param cdBeneficiarioCartao Codigo de Beneficiario 
	 */ 
	public void setCdBeneficiarioCartao( String cdBeneficiarioCartao ) {
		this.cdBeneficiarioCartao = cdBeneficiarioCartao;
	}

	/**
	 * Retorna Codigo de Beneficiario 
	 * @return Codigo de Beneficiario 
	 */
	public String getCdBeneficiarioCartao() {
		return this.cdBeneficiarioCartao;
	}

	/**
	 * Alimenta sequencia perfil
	 * @param sqPerfil sequencia perfil
	 */ 
	public void setSqPerfil( Integer sqPerfil ) {
		this.sqPerfil = sqPerfil;
	}

	/**
	 * Retorna sequencia perfil
	 * @return sequencia perfil
	 */
	public Integer getSqPerfil() {
		return this.sqPerfil;
	}

	/**
	 * Alimenta sequencia lotacao
	 * @param sqLotacao sequencia lotacao
	 */ 
	public void setSqLotacao( String sqLotacao ) {
		this.sqLotacao = sqLotacao;
	}

	/**
	 * Retorna sequencia lotacao
	 * @return sequencia lotacao
	 */
	public String getSqLotacao() {
		return this.sqLotacao;
	}

	/**
	 * Alimenta data de Inicio
	 * @param dtInicio data de Inicio
	 */ 
	public void setDtInicio( QtData dtInicio ) {
		this.dtInicio = dtInicio;
	}

	/**
	 * Retorna data de Inicio
	 * @return data de Inicio
	 */
	public QtData getDtInicio() {
		return this.dtInicio;
	}

	/**
	 * Alimenta data final
	 * @param dtFinal data final
	 */ 
	public void setDtFinal( QtData dtFinal ) {
		this.dtFinal = dtFinal;
	}

	/**
	 * Retorna data final
	 * @return data final
	 */
	public QtData getDtFinal() {
		return this.dtFinal;
	}

	/**
	 * Alimenta Codigo de Empresa
	 * @param cdEmpresa Codigo de Empresa
	 */ 
	public void setCdEmpresa( String cdEmpresa ) {
		this.cdEmpresa = cdEmpresa;
	}

	/**
	 * Retorna Codigo de Empresa
	 * @return Codigo de Empresa
	 */
	public String getCdEmpresa() {
		return this.cdEmpresa;
	}

	/**
	 * Alimenta CNPJ do Domic�lio Tribut�rio/CEI:
	 * @param nrCnpj CNPJ do Domic�lio Tribut�rio/CEI:
	 */ 
	public void setNrCnpj( String nrCnpj ) {
		this.nrCnpj = nrCnpj;
	}

	/**
	 * Retorna CNPJ do Domic�lio Tribut�rio/CEI:
	 * @return CNPJ do Domic�lio Tribut�rio/CEI:
	 */
	public String getNrCnpj() {
		return this.nrCnpj;
	}

	/**
	 * Alimenta Codigo do setor
	 * @param idSetor Codigo do setor
	 */ 
	public void setIdSetor( Integer idSetor ) {
		this.idSetor = idSetor;
	}

	/**
	 * Retorna Codigo do setor
	 * @return Codigo do setor
	 */
	public Integer getIdSetor() {
		return this.idSetor;
	}

	/**
	 * Alimenta Codigo do cargo
	 * @param idCargo Codigo do cargo
	 */ 
	public void setIdCargo( Integer idCargo ) {
		this.idCargo = idCargo;
	}

	/**
	 * Retorna Codigo do cargo
	 * @return Codigo do cargo
	 */
	public Integer getIdCargo() {
		return this.idCargo;
	}

	/**
	 * Alimenta Codigo do fun��o
	 * @param idFuncao Codigo do fun��o
	 */ 
	public void setIdFuncao( Integer idFuncao ) {
		this.idFuncao = idFuncao;
	}

	/**
	 * Retorna Codigo do fun��o
	 * @return Codigo do fun��o
	 */
	public Integer getIdFuncao() {
		return this.idFuncao;
	}

	/**
	 * Alimenta Codigo do CBO
	 * @param cdCbo Codigo do CBO
	 */ 
	public void setCdCbo( String cdCbo ) {
		this.cdCbo = cdCbo;
	}

	/**
	 * Retorna Codigo do CBO
	 * @return Codigo do CBO
	 */
	public String getCdCbo() {
		return this.cdCbo;
	}

	/**
	 * Alimenta Codigo do GFIP
	 * @param cdGfip Codigo do GFIP
	 */ 
	public void setCdGfip( String cdGfip ) {
		this.cdGfip = cdGfip;
	}

	/**
	 * Retorna Codigo do GFIP
	 * @return Codigo do GFIP
	 */
	public String getCdGfip() {
		return this.cdGfip;
	}

	/**
	 * Alimenta Descri��o das Atividades
	 * @param dsAtividade Descri��o das Atividades
	 */ 
	public void setDsAtividade( String dsAtividade ) {
		this.dsAtividade = dsAtividade;
	}

	/**
	 * Retorna Descri��o das Atividades
	 * @return Descri��o das Atividades
	 */
	public String getDsAtividade() {
		return this.dsAtividade;
	}

}