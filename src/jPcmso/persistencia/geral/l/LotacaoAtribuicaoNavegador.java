package jPcmso.persistencia.geral.l;

import quatro.persistencia.FiltroDeNavegador;
import quatro.persistencia.Navegador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.sql.Query;
import quatro.util.QtData;

/**
 *
 * Classe navegador para PCMSO.LOTACAO_ATRIBUICAO
 *
 * @author Samuel Antonio Klein
 * @see Navegador
 * @see LotacaoAtribuicao
 * @see LotacaoAtribuicaoPersistor
 * @see LotacaoAtribuicaoLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:31 - importado por Samuel Antonio Klein
 *
 */
 public class LotacaoAtribuicaoNavegador extends Navegador {

	
	/** Inst�ncia de LotacaoAtribuicao para manipula��o pelo navegador */
	private LotacaoAtribuicao lotacaoAtribuicao;

	/** Construtor b�sico */
	public LotacaoAtribuicaoNavegador()
	  throws QtSQLException { 

		lotacaoAtribuicao = new LotacaoAtribuicao();
		primeiro();
	}

	/** Construtor b�sico que recebe uma conex�o */
	public LotacaoAtribuicaoNavegador( Conexao conexao )
	  throws QtSQLException { 

		super.setConexao( conexao ); 
		lotacaoAtribuicao = new LotacaoAtribuicao();
		primeiro();
	}

	/**
	 * Construtor que recebe uma inst�ncia de LotacaoAtribuicaoFiltro e posiciona
	 * no primeiro registro que satisfaz suas condi��es 
	 */
	public LotacaoAtribuicaoNavegador( FiltroDeNavegador filtro ) throws QtSQLException  {
		super.setFiltro( filtro );
		this.lotacaoAtribuicao = new LotacaoAtribuicao();
		primeiro();
	}

	/** Construtor que recebe as chaves da tabela PCMSO.LOTACAO_ATRIBUICAO
	 * @param cdBeneficiarioCartao Codigo de Beneficiario 
	 * @param sqPerfil sequencia perfil
	 * @param sqLotacao sequencia lotacao
	 */
	public LotacaoAtribuicaoNavegador( String cdBeneficiarioCartao, int sqPerfil, String sqLotacao )
	  throws QtSQLException { 

		lotacaoAtribuicao = new LotacaoAtribuicao();
		this.lotacaoAtribuicao.setCdBeneficiarioCartao( cdBeneficiarioCartao );
		this.lotacaoAtribuicao.setSqPerfil( new Integer( sqPerfil ) );
		this.lotacaoAtribuicao.setSqLotacao( sqLotacao );
		
		busca();
	}

	/** Construtor que recebe  uma conex�o e as chaves da tabela PCMSO.LOTACAO_ATRIBUICAO
	 * @param cdBeneficiarioCartao Codigo de Beneficiario 
	 * @param sqPerfil sequencia perfil
	 * @param sqLotacao sequencia lotacao
	 * @param conexao Conexao com o banco de dados
	 */
	public LotacaoAtribuicaoNavegador( Conexao conexao, String cdBeneficiarioCartao, int sqPerfil, String sqLotacao )
	  throws QtSQLException { 

		super.setConexao( conexao ); 
		lotacaoAtribuicao = new LotacaoAtribuicao();
		this.lotacaoAtribuicao.setCdBeneficiarioCartao( cdBeneficiarioCartao );
		this.lotacaoAtribuicao.setSqPerfil( new Integer( sqPerfil ) );
		this.lotacaoAtribuicao.setSqLotacao( sqLotacao );
		
		busca();
	}

	/** M�todo que retorna uma tabela b�sica a que se refere o navegador */
	public TabelaBasica getTabelaBasica() {

		return this.lotacaoAtribuicao;
	}

	/** M�todo que retorna a classe LotacaoAtribuicao relacionada ao navegador */
	public LotacaoAtribuicao getLotacaoAtribuicao() {

		return this.lotacaoAtribuicao;
	}

	 /** M�todo que move para o primeiro registro da tabela */
	 public boolean primeiro() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select CD_BENEFICIARIO_CARTAO, SQ_PERFIL, SQ_LOTACAO" );
			query.addSQL( "  from PCMSO.LOTACAO_ATRIBUICAO" );
			query.addSQL( super.getCondicaoDeFiltro( FiltroDeNavegador.COM_WHERE ) );
			query.addSQL( "  order by CD_BENEFICIARIO_CARTAO, SQ_PERFIL, SQ_LOTACAO" );
			query.addSQL( "  limit 1 " );
			
			query.executeQuery();

			
			if( query.isEmpty() ) {

				super.setQueryStates( false, true, true );
			} else {
				this.lotacaoAtribuicao = new LotacaoAtribuicao();

				lotacaoAtribuicao.setCdBeneficiarioCartao( query.getString( "CD_BENEFICIARIO_CARTAO" ) );
				lotacaoAtribuicao.setSqPerfil( query.getInteger( "SQ_PERFIL" ) );
				lotacaoAtribuicao.setSqLotacao( query.getString( "SQ_LOTACAO" ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o registro anterior na tabela */
	 public boolean anterior() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select CD_BENEFICIARIO_CARTAO, SQ_PERFIL, SQ_LOTACAO" );
			query.addSQL( "  from PCMSO.LOTACAO_ATRIBUICAO" );
			query.addSQL( "  where ( ( CD_BENEFICIARIO_CARTAO = :prm1CdBeneficiarioCartao and  SQ_PERFIL = :prm2SqPerfil and  SQ_LOTACAO < :prm3SqLotacao )" );
			query.addSQL( "  or (  CD_BENEFICIARIO_CARTAO = :prm1CdBeneficiarioCartao and  SQ_PERFIL < :prm2SqPerfil )" );
			query.addSQL( "  or (  CD_BENEFICIARIO_CARTAO < :prm1CdBeneficiarioCartao ) )" );
			query.addSQL( super.getCondicaoDeFiltro() );
			query.addSQL( "  order by CD_BENEFICIARIO_CARTAO desc, SQ_PERFIL desc, SQ_LOTACAO desc" );
			query.addSQL( "  limit 1 " );

			query.setParameter( "prm1CdBeneficiarioCartao", lotacaoAtribuicao.getCdBeneficiarioCartao() );
			query.setParameter( "prm2SqPerfil", lotacaoAtribuicao.getSqPerfil() );
			query.setParameter( "prm3SqLotacao", lotacaoAtribuicao.getSqLotacao() );
		
			query.executeQuery();

			
			if( query.isEmpty() ) {

				super.setFound( false );
				super.setBof( true );
			} else {
				this.lotacaoAtribuicao = new LotacaoAtribuicao();

				lotacaoAtribuicao.setCdBeneficiarioCartao( query.getString( "CD_BENEFICIARIO_CARTAO" ) );
				lotacaoAtribuicao.setSqPerfil( query.getInteger( "SQ_PERFIL" ) );
				lotacaoAtribuicao.setSqLotacao( query.getString( "SQ_LOTACAO" ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o proximo registro da tabela */
	 public boolean proximo() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select CD_BENEFICIARIO_CARTAO, SQ_PERFIL, SQ_LOTACAO" );
			query.addSQL( "  from PCMSO.LOTACAO_ATRIBUICAO" );
			query.addSQL( "  where ( (  CD_BENEFICIARIO_CARTAO = :prm1CdBeneficiarioCartao and  SQ_PERFIL = :prm2SqPerfil and  SQ_LOTACAO > :prm3SqLotacao )" );
			query.addSQL( "  or (  CD_BENEFICIARIO_CARTAO = :prm1CdBeneficiarioCartao and  SQ_PERFIL > :prm2SqPerfil )" );
			query.addSQL( "  or (  CD_BENEFICIARIO_CARTAO > :prm1CdBeneficiarioCartao ) )" );
			query.addSQL( super.getCondicaoDeFiltro() );
			query.addSQL( "  order by CD_BENEFICIARIO_CARTAO, SQ_PERFIL, SQ_LOTACAO" );
			query.addSQL( "  limit 1 " );

			query.setParameter( "prm1CdBeneficiarioCartao", lotacaoAtribuicao.getCdBeneficiarioCartao() );
			query.setParameter( "prm2SqPerfil", lotacaoAtribuicao.getSqPerfil() );
			query.setParameter( "prm3SqLotacao", lotacaoAtribuicao.getSqLotacao() );
		
			query.executeQuery();

			
			if( query.isEmpty() ) {

				super.setFound( false );
				super.setEof( true );
			} else {
				this.lotacaoAtribuicao = new LotacaoAtribuicao();

				lotacaoAtribuicao.setCdBeneficiarioCartao( query.getString( "CD_BENEFICIARIO_CARTAO" ) );
				lotacaoAtribuicao.setSqPerfil( query.getInteger( "SQ_PERFIL" ) );
				lotacaoAtribuicao.setSqLotacao( query.getString( "SQ_LOTACAO" ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o �ltimo registro da tabela */
	 public boolean ultimo() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select CD_BENEFICIARIO_CARTAO, SQ_PERFIL, SQ_LOTACAO" );
			query.addSQL( "  from PCMSO.LOTACAO_ATRIBUICAO" );
			query.addSQL( super.getCondicaoDeFiltro( FiltroDeNavegador.COM_WHERE ) 	);
			query.addSQL( "  order by CD_BENEFICIARIO_CARTAO desc, SQ_PERFIL desc, SQ_LOTACAO desc" );
			query.addSQL( "  limit 1 " );

			query.executeQuery();

			
			if( query.isEmpty() ) {

				super.setQueryStates( false, true, true );
			} else {
				this.lotacaoAtribuicao = new LotacaoAtribuicao();

				lotacaoAtribuicao.setCdBeneficiarioCartao( query.getString( "CD_BENEFICIARIO_CARTAO" ) );
				lotacaoAtribuicao.setSqPerfil( query.getInteger( "SQ_PERFIL" ) );
				lotacaoAtribuicao.setSqLotacao( query.getString( "SQ_LOTACAO" ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	/**
	 * M�todo para buscar e preencher uma tupla de uma tabela
	 * @param tabelaBasica Recebe uma tabela basica com os atributos chave preenchidos
	 */
	public boolean busca( TabelaBasica tabelaBasica ) throws QtSQLException {

		if( !( tabelaBasica instanceof LotacaoAtribuicao ) )
			throw new QtSQLException( "Esperava um objeto LotacaoAtribuicao!" );

		this.lotacaoAtribuicao = (LotacaoAtribuicao) tabelaBasica;
		return busca();

	}

	/**
	 * m�todo interno para buscar e preencher uma tupla de uma tabela
	 * Utiliza a Inst�ncia de LotacaoAtribuicao da classe para fazer a busca
	 */
	private boolean busca() throws QtSQLException {

		LotacaoAtribuicaoLocalizador localizador = new LotacaoAtribuicaoLocalizador( getConexao(), this.lotacaoAtribuicao );

		if( localizador.isEmpty() ) {

			super.setQueryStates( false, true, true );
			return false;

		} else {

			super.setQueryStates( true, false, false );
			return true;
		}
	}
}