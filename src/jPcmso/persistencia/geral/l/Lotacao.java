package jPcmso.persistencia.geral.l;

import quatro.persistencia.TabelaBasica;
import quatro.smartDB.IgnoredField;
import quatro.smartDB.PrimaryKey;
import quatro.smartDB.Table;

/**
 *
 * Classe b�sica para LOTACOES
 *
 * Esta tabela conter� as Lota��es das Empresas.
 * 
 * @author Samuel Antonio Klein
 * @see LotacaoNavegador
 * @see LotacaoPersistor
 * @see LotacaoLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:34 - importado por Samuel Antonio Klein
 *
 */
 @Table(name="LOTACOES")
 public class Lotacao implements TabelaBasica {

	@IgnoredField
	private static final long serialVersionUID = -3187735051648443521L;

	/** C�digo da Unimed (chave-char(4)) */
	@PrimaryKey
	private String cdUnimed;
	/** Identificador da Empresa (chave) */
	@PrimaryKey
	private Integer idEmpresa;
	/** Identificador da Lota��o (chave) */
	@PrimaryKey
	private Integer idLotacao;
	/** Nome da Lota��o (varchar(100)) */
	private String dsLotacao;
	/** C�digo do Estado (char(2)) */
	private String cdEstado;
	/** Identificador da Cidade */
	private Integer idCidade;
	/** Nome do Bairro (char(50)) */
	private String dsBairro;
	/** Logradouro (varchar(100)) */
	private String dsLogradouro;
	/** C�digo do CEP (char(8)) */
	private String cdCep;
	/** Telefone (varchar(50)) */
	private String dsTelefone;
	/** Fax (varchar(50)) */
	private String dsFax;
	/** Celular (varchar(50)) */
	private String dsCelular;
	/** EMail (varchar(100)) */
	private String dsEMail;

	/**
	 * Alimenta C�digo da Unimed
	 * @param cdUnimed C�digo da Unimed
	 */ 
	public void setCdUnimed( String cdUnimed ) {
		this.cdUnimed = cdUnimed;
	}

	/**
	 * Retorna C�digo da Unimed
	 * @return C�digo da Unimed
	 */
	public String getCdUnimed() {
		return this.cdUnimed;
	}

	/**
	 * Alimenta Identificador da Empresa
	 * @param idEmpresa Identificador da Empresa
	 */ 
	public void setIdEmpresa( Integer idEmpresa ) {
		this.idEmpresa = idEmpresa;
	}

	/**
	 * Retorna Identificador da Empresa
	 * @return Identificador da Empresa
	 */
	public Integer getIdEmpresa() {
		return this.idEmpresa;
	}

	/**
	 * Alimenta Identificador da Lota��o
	 * @param idLotacao Identificador da Lota��o
	 */ 
	public void setIdLotacao( Integer idLotacao ) {
		this.idLotacao = idLotacao;
	}

	/**
	 * Retorna Identificador da Lota��o
	 * @return Identificador da Lota��o
	 */
	public Integer getIdLotacao() {
		return this.idLotacao;
	}

	/**
	 * Alimenta Nome da Lota��o
	 * @param dsLotacao Nome da Lota��o
	 */ 
	public void setDsLotacao( String dsLotacao ) {
		this.dsLotacao = dsLotacao;
	}

	/**
	 * Retorna Nome da Lota��o
	 * @return Nome da Lota��o
	 */
	public String getDsLotacao() {
		return this.dsLotacao;
	}

	/**
	 * Alimenta C�digo do Estado
	 * @param cdEstado C�digo do Estado
	 */ 
	public void setCdEstado( String cdEstado ) {
		this.cdEstado = cdEstado;
	}

	/**
	 * Retorna C�digo do Estado
	 * @return C�digo do Estado
	 */
	public String getCdEstado() {
		return this.cdEstado;
	}

	/**
	 * Alimenta Identificador da Cidade
	 * @param idCidade Identificador da Cidade
	 */ 
	public void setIdCidade( Integer idCidade ) {
		this.idCidade = idCidade;
	}

	/**
	 * Retorna Identificador da Cidade
	 * @return Identificador da Cidade
	 */
	public Integer getIdCidade() {
		return this.idCidade;
	}

	/**
	 * Alimenta Nome do Bairro
	 * @param dsBairro Nome do Bairro
	 */ 
	public void setDsBairro( String dsBairro ) {
		this.dsBairro = dsBairro;
	}

	/**
	 * Retorna Nome do Bairro
	 * @return Nome do Bairro
	 */
	public String getDsBairro() {
		return this.dsBairro;
	}

	/**
	 * Alimenta Logradouro
	 * @param dsLogradouro Logradouro
	 */ 
	public void setDsLogradouro( String dsLogradouro ) {
		this.dsLogradouro = dsLogradouro;
	}

	/**
	 * Retorna Logradouro
	 * @return Logradouro
	 */
	public String getDsLogradouro() {
		return this.dsLogradouro;
	}

	/**
	 * Alimenta C�digo do CEP
	 * @param cdCep C�digo do CEP
	 */ 
	public void setCdCep( String cdCep ) {
		this.cdCep = cdCep;
	}

	/**
	 * Retorna C�digo do CEP
	 * @return C�digo do CEP
	 */
	public String getCdCep() {
		return this.cdCep;
	}

	/**
	 * Alimenta Telefone
	 * @param dsTelefone Telefone
	 */ 
	public void setDsTelefone( String dsTelefone ) {
		this.dsTelefone = dsTelefone;
	}

	/**
	 * Retorna Telefone
	 * @return Telefone
	 */
	public String getDsTelefone() {
		return this.dsTelefone;
	}

	/**
	 * Alimenta Fax
	 * @param dsFax Fax
	 */ 
	public void setDsFax( String dsFax ) {
		this.dsFax = dsFax;
	}

	/**
	 * Retorna Fax
	 * @return Fax
	 */
	public String getDsFax() {
		return this.dsFax;
	}

	/**
	 * Alimenta Celular
	 * @param dsCelular Celular
	 */ 
	public void setDsCelular( String dsCelular ) {
		this.dsCelular = dsCelular;
	}

	/**
	 * Retorna Celular
	 * @return Celular
	 */
	public String getDsCelular() {
		return this.dsCelular;
	}

	/**
	 * Alimenta EMail
	 * @param dsEMail EMail
	 */ 
	public void setDsEMail( String dsEMail ) {
		this.dsEMail = dsEMail;
	}

	/**
	 * Retorna EMail
	 * @return EMail
	 */
	public String getDsEMail() {
		return this.dsEMail;
	}

}