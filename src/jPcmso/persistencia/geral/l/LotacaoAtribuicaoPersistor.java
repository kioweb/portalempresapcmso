package jPcmso.persistencia.geral.l;

import java.sql.PreparedStatement;

import quatro.persistencia.Persistor;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.util.QtException;
import quatro.util.QtData;

/**
 *
 * Classe persistor para PCMSO.LOTACAO_ATRIBUICAO
 *
 * @author Samuel Antonio Klein
 * @see Persistor
 * @see LotacaoAtribuicao
 * @see LotacaoAtribuicaoNavegador
 * @see LotacaoAtribuicaoLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:31 - importado por Samuel Antonio Klein
 *
 */
 public class LotacaoAtribuicaoPersistor extends Persistor {

	/** Inst�ncia de LotacaoAtribuicao para manipula��o pelo persistor */
	private LotacaoAtribuicao lotacaoAtribuicao;

	/** Construtor gen�rico */
	public LotacaoAtribuicaoPersistor() {

		this.lotacaoAtribuicao = null;
	}

	/** Construtor gen�rico */
	public LotacaoAtribuicaoPersistor( Conexao cnx ) throws QtSQLException {

		this.lotacaoAtribuicao = null;
		super.setConexao( cnx );
	}

	/** Construtor que recebe uma inst�ncia de LotacaoAtribuicao */
	public LotacaoAtribuicaoPersistor( LotacaoAtribuicao lotacaoAtribuicao ) throws QtSQLException {

		if( lotacaoAtribuicao == null ) 
			throw new QtSQLException( "Imposs�vel instanciar LotacaoAtribuicaoPersistor porque est� apontando para um nulo!" );

		this.lotacaoAtribuicao = lotacaoAtribuicao;
	}

	/** Construtor que recebe uma conex�o e uma inst�ncia de LotacaoAtribuicao */
	public LotacaoAtribuicaoPersistor( Conexao conexao, LotacaoAtribuicao lotacaoAtribuicao ) throws QtSQLException {

		if( lotacaoAtribuicao == null ) 
			throw new QtSQLException( "Imposs�vel instanciar LotacaoAtribuicaoPersistor porque est� apontando para um nulo!" );

		setConexao( conexao );
		this.lotacaoAtribuicao = lotacaoAtribuicao;
	}

	/**
	 * Construtor que recebe as chaves da tabela PCMSO.LOTACAO_ATRIBUICAO
	 * @param cdBeneficiarioCartao Codigo de Beneficiario 
	 * @param sqPerfil sequencia perfil
	 * @param sqLotacao sequencia lotacao
	 */
	public LotacaoAtribuicaoPersistor( String cdBeneficiarioCartao, int sqPerfil, String sqLotacao )
	  throws QtSQLException { 

		busca( cdBeneficiarioCartao, sqPerfil, sqLotacao );
	}

	/**
	 * Construtor que recebe uma conex�o e as chaves da tabela PCMSO.LOTACAO_ATRIBUICAO
	 * @param cdBeneficiarioCartao Codigo de Beneficiario 
	 * @param sqPerfil sequencia perfil
	 * @param sqLotacao sequencia lotacao
	 * @param conexao Conexao com o banco de dados
	 */
	public LotacaoAtribuicaoPersistor( Conexao conexao, String cdBeneficiarioCartao, int sqPerfil, String sqLotacao )
	  throws QtSQLException { 

		super.setConexao( conexao );
		busca( cdBeneficiarioCartao, sqPerfil, sqLotacao );
	}

	/**
	 * M�todo interno para buscar e preencher uma tupla de uma tabela
	 * Utiliza a Inst�ncia de LotacaoAtribuicao para fazer a busca
	 * @param cdBeneficiarioCartao Codigo de Beneficiario 
	 * @param sqPerfil sequencia perfil
	 * @param sqLotacao sequencia lotacao
	 */
	private void busca( String cdBeneficiarioCartao, int sqPerfil, String sqLotacao ) throws QtSQLException { 

		LotacaoAtribuicaoLocalizador lotacaoAtribuicaoLocalizador = new LotacaoAtribuicaoLocalizador( getConexao(), cdBeneficiarioCartao, sqPerfil, sqLotacao );

		if( lotacaoAtribuicaoLocalizador.isEmpty() ) {
			throw new QtSQLException( "Chaves da Tabela LotacaoAtribuicao n�o encontradas - cdBeneficiarioCartao: " + cdBeneficiarioCartao + " / sqPerfil: " + sqPerfil + " / sqLotacao: " + sqLotacao );
		}

		lotacaoAtribuicao = (LotacaoAtribuicao ) lotacaoAtribuicaoLocalizador.getTabelaBasica();
	}

	/**
	 * Permite alimentar o Persistor com um novo registro
	 */
	public void setTabelaBasica( TabelaBasica lotacaoAtribuicao ) throws QtException {

		if( lotacaoAtribuicao instanceof LotacaoAtribuicao ) {
			this.lotacaoAtribuicao = (LotacaoAtribuicao) lotacaoAtribuicao;
		} else {
			throw new QtException( "Tabela B�sica fornecida tem que ser uma inst�ncia de LotacaoAtribuicao" );
		}
	}

	/**
	 * Permite acessar o registro corrente do persistor
	 */
	public TabelaBasica getTabelaBasica() throws QtException {

		return lotacaoAtribuicao;
	}

	/**
	 * M�todo para inserir tupla na tabela
	 */
	public void insere() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "insert into PCMSO.LOTACAO_ATRIBUICAO" );
			cmd.append( "( CD_BENEFICIARIO_CARTAO, SQ_PERFIL, SQ_LOTACAO, DT_INICIO, DT_FINAL, CD_EMPRESA, NR_CNPJ, ID_SETOR, ID_CARGO, ID_FUNCAO, CD_CBO, CD_GFIP, DS_ATIVIDADE )" );
			cmd.append( "values " );
			cmd.append( "( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			ps.setObject( 1, lotacaoAtribuicao.getCdBeneficiarioCartao() );
			ps.setObject( 2, lotacaoAtribuicao.getSqPerfil() );
			ps.setObject( 3, lotacaoAtribuicao.getSqLotacao() );
			ps.setObject( 4, QtData.getQtDataAsTimestamp( lotacaoAtribuicao.getDtInicio() ) );
			ps.setObject( 5, QtData.getQtDataAsTimestamp( lotacaoAtribuicao.getDtFinal() ) );
			ps.setObject( 6, lotacaoAtribuicao.getCdEmpresa() );
			ps.setObject( 7, lotacaoAtribuicao.getNrCnpj() );
			ps.setObject( 8, lotacaoAtribuicao.getIdSetor() );
			ps.setObject( 9, lotacaoAtribuicao.getIdCargo() );
			ps.setObject( 10, lotacaoAtribuicao.getIdFuncao() );
			ps.setObject( 11, lotacaoAtribuicao.getCdCbo() );
			ps.setObject( 12, lotacaoAtribuicao.getCdGfip() );
			ps.setObject( 13, lotacaoAtribuicao.getDsAtividade() );
			
			ps.execute();

		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}

	/**
	 * M�todo para alterar tupla na tabela
	 */
	public void altera() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "update PCMSO.LOTACAO_ATRIBUICAO" );
			cmd.append( "  set CD_BENEFICIARIO_CARTAO = ?, " );
			cmd.append( " SQ_PERFIL = ?, " );
			cmd.append( " SQ_LOTACAO = ?, " );
			cmd.append( " DT_INICIO = ?, " );
			cmd.append( " DT_FINAL = ?, " );
			cmd.append( " CD_EMPRESA = ?, " );
			cmd.append( " NR_CNPJ = ?, " );
			cmd.append( " ID_SETOR = ?, " );
			cmd.append( " ID_CARGO = ?, " );
			cmd.append( " ID_FUNCAO = ?, " );
			cmd.append( " CD_CBO = ?, " );
			cmd.append( " CD_GFIP = ?, " );
			cmd.append( " DS_ATIVIDADE = ?" );
			cmd.append( " where CD_BENEFICIARIO_CARTAO like ? and SQ_PERFIL = ? and SQ_LOTACAO like ?" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			ps.setObject( 1, lotacaoAtribuicao.getCdBeneficiarioCartao() );
			ps.setObject( 2, lotacaoAtribuicao.getSqPerfil() );
			ps.setObject( 3, lotacaoAtribuicao.getSqLotacao() );
			ps.setObject( 4, QtData.getQtDataAsTimestamp( lotacaoAtribuicao.getDtInicio() ) );
			ps.setObject( 5, QtData.getQtDataAsTimestamp( lotacaoAtribuicao.getDtFinal() ) );
			ps.setObject( 6, lotacaoAtribuicao.getCdEmpresa() );
			ps.setObject( 7, lotacaoAtribuicao.getNrCnpj() );
			ps.setObject( 8, lotacaoAtribuicao.getIdSetor() );
			ps.setObject( 9, lotacaoAtribuicao.getIdCargo() );
			ps.setObject( 10, lotacaoAtribuicao.getIdFuncao() );
			ps.setObject( 11, lotacaoAtribuicao.getCdCbo() );
			ps.setObject( 12, lotacaoAtribuicao.getCdGfip() );
			ps.setObject( 13, lotacaoAtribuicao.getDsAtividade() );
			
			ps.setObject( 14, lotacaoAtribuicao.getCdBeneficiarioCartao() );
			ps.setObject( 15, lotacaoAtribuicao.getSqPerfil() );
			ps.setObject( 16, lotacaoAtribuicao.getSqLotacao() );
			
			ps.execute();

		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}

	/**
	 * M�todo para excluir tupla de uma tabela
	 */
	public void exclui() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "delete from PCMSO.LOTACAO_ATRIBUICAO" );
			cmd.append( " where CD_BENEFICIARIO_CARTAO like ? and SQ_PERFIL = ? and SQ_LOTACAO like ?" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			ps.setObject( 1, lotacaoAtribuicao.getCdBeneficiarioCartao() );
			ps.setObject( 2, lotacaoAtribuicao.getSqPerfil() );
			ps.setObject( 3, lotacaoAtribuicao.getSqLotacao() );
			
			ps.execute();

		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}
}