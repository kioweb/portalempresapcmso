package jPcmso.persistencia.geral.l;

import quatro.persistencia.FiltroDeNavegador;

/**
 *
 * Classe filtro para PCMSO.LOTACAO_ATRIBUICAO
 *
 * @author Samuel Antonio Klein
 * @see FiltroDeTabela
 * @see LotacaoAtribuicao
 * @see LotacaoAtribuicaoNavegador
 * @see LotacaoAtribuicaoPersistor
 * @see LotacaoAtribuicaoLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:31 - importado por Samuel Antonio Klein
 *
 */
 public class LotacaoAtribuicaoFiltro extends FiltroDeNavegador {

	/** Determina a ordem de navega��o que ser� passada para o navegador */
	public void setOrdemDeNavegacao( int idOrdem ) {

		super.setOrdemDeNavegacao( idOrdem );
	}
}