package jPcmso.persistencia.geral.l;

import java.sql.PreparedStatement;

import quatro.persistencia.Persistor;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.util.QtException;

/**
 *
 * Classe persistor para LOTACOES
 *
 * @author Samuel Antonio Klein
 * @see Persistor
 * @see Lotacao
 * @see LotacaoNavegador
 * @see LotacaoLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:34 - importado por Samuel Antonio Klein
 *
 */
 public class LotacaoPersistor extends Persistor {

	/** Inst�ncia de Lotacao para manipula��o pelo persistor */
	private Lotacao lotacao;

	/** Construtor gen�rico */
	public LotacaoPersistor() {

		this.lotacao = null;
	}

	/** Construtor gen�rico */
	public LotacaoPersistor( Conexao cnx ) throws QtSQLException {

		this.lotacao = null;
		super.setConexao( cnx );
	}

	/** Construtor que recebe uma inst�ncia de Lotacao */
	public LotacaoPersistor( Lotacao lotacao ) throws QtSQLException {

		if( lotacao == null ) 
			throw new QtSQLException( "Imposs�vel instanciar LotacaoPersistor porque est� apontando para um nulo!" );

		this.lotacao = lotacao;
	}

	/** Construtor que recebe uma conex�o e uma inst�ncia de Lotacao */
	public LotacaoPersistor( Conexao conexao, Lotacao lotacao ) throws QtSQLException {

		if( lotacao == null ) 
			throw new QtSQLException( "Imposs�vel instanciar LotacaoPersistor porque est� apontando para um nulo!" );

		setConexao( conexao );
		this.lotacao = lotacao;
	}

	/**
	 * Construtor que recebe as chaves da tabela LOTACOES
	 * @param cdUnimed C�digo da Unimed
	 * @param idEmpresa Identificador da Empresa
	 * @param idLotacao Identificador da Lota��o
	 */
	public LotacaoPersistor( String cdUnimed, int idEmpresa, int idLotacao )
	  throws QtSQLException { 

		busca( cdUnimed, idEmpresa, idLotacao );
	}

	/**
	 * Construtor que recebe uma conex�o e as chaves da tabela LOTACOES
	 * @param cdUnimed C�digo da Unimed
	 * @param idEmpresa Identificador da Empresa
	 * @param idLotacao Identificador da Lota��o
	 * @param conexao Conexao com o banco de dados
	 */
	public LotacaoPersistor( Conexao conexao, String cdUnimed, int idEmpresa, int idLotacao )
	  throws QtSQLException { 

		super.setConexao( conexao );
		busca( cdUnimed, idEmpresa, idLotacao );
	}

	/**
	 * M�todo interno para buscar e preencher uma tupla de uma tabela
	 * Utiliza a Inst�ncia de Lotacao para fazer a busca
	 * @param cdUnimed C�digo da Unimed
	 * @param idEmpresa Identificador da Empresa
	 * @param idLotacao Identificador da Lota��o
	 */
	private void busca( String cdUnimed, int idEmpresa, int idLotacao ) throws QtSQLException { 

		LotacaoLocalizador lotacaoLocalizador = new LotacaoLocalizador( getConexao(), cdUnimed, idEmpresa, idLotacao );

		if( lotacaoLocalizador.isEmpty() ) {
			throw new QtSQLException( "Chaves da Tabela Lotacao n�o encontradas - cdUnimed: " + cdUnimed + " / idEmpresa: " + idEmpresa + " / idLotacao: " + idLotacao );
		}

		lotacao = (Lotacao ) lotacaoLocalizador.getTabelaBasica();
	}

	/**
	 * Permite alimentar o Persistor com um novo registro
	 */
	public void setTabelaBasica( TabelaBasica lotacao ) throws QtException {

		if( lotacao instanceof Lotacao ) {
			this.lotacao = (Lotacao) lotacao;
		} else {
			throw new QtException( "Tabela B�sica fornecida tem que ser uma inst�ncia de Lotacao" );
		}
	}

	/**
	 * Permite acessar o registro corrente do persistor
	 */
	public TabelaBasica getTabelaBasica() throws QtException {

		return lotacao;
	}

	/**
	 * M�todo para inserir tupla na tabela
	 */
	public void insere() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "insert into LOTACOES" );
			cmd.append( "( CD_UNIMED, ID_EMPRESA, ID_LOTACAO, DS_LOTACAO, CD_ESTADO, ID_CIDADE, DS_BAIRRO, DS_LOGRADOURO, CD_CEP, DS_TELEFONE, DS_FAX, DS_CELULAR, DS_E_MAIL )" );
			cmd.append( "values " );
			cmd.append( "( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			ps.setObject( 1, lotacao.getCdUnimed() );
			ps.setObject( 2, lotacao.getIdEmpresa() );
			ps.setObject( 3, lotacao.getIdLotacao() );
			ps.setObject( 4, lotacao.getDsLotacao() );
			ps.setObject( 5, lotacao.getCdEstado() );
			ps.setObject( 6, lotacao.getIdCidade() );
			ps.setObject( 7, lotacao.getDsBairro() );
			ps.setObject( 8, lotacao.getDsLogradouro() );
			ps.setObject( 9, lotacao.getCdCep() );
			ps.setObject( 10, lotacao.getDsTelefone() );
			ps.setObject( 11, lotacao.getDsFax() );
			ps.setObject( 12, lotacao.getDsCelular() );
			ps.setObject( 13, lotacao.getDsEMail() );
			
			ps.execute();

		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}

	/**
	 * M�todo para alterar tupla na tabela
	 */
	public void altera() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "update LOTACOES" );
			cmd.append( "  set CD_UNIMED = ?, " );
			cmd.append( " ID_EMPRESA = ?, " );
			cmd.append( " ID_LOTACAO = ?, " );
			cmd.append( " DS_LOTACAO = ?, " );
			cmd.append( " CD_ESTADO = ?, " );
			cmd.append( " ID_CIDADE = ?, " );
			cmd.append( " DS_BAIRRO = ?, " );
			cmd.append( " DS_LOGRADOURO = ?, " );
			cmd.append( " CD_CEP = ?, " );
			cmd.append( " DS_TELEFONE = ?, " );
			cmd.append( " DS_FAX = ?, " );
			cmd.append( " DS_CELULAR = ?, " );
			cmd.append( " DS_E_MAIL = ?" );
			cmd.append( " where CD_UNIMED like ? and ID_EMPRESA = ? and ID_LOTACAO = ?" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			ps.setObject( 1, lotacao.getCdUnimed() );
			ps.setObject( 2, lotacao.getIdEmpresa() );
			ps.setObject( 3, lotacao.getIdLotacao() );
			ps.setObject( 4, lotacao.getDsLotacao() );
			ps.setObject( 5, lotacao.getCdEstado() );
			ps.setObject( 6, lotacao.getIdCidade() );
			ps.setObject( 7, lotacao.getDsBairro() );
			ps.setObject( 8, lotacao.getDsLogradouro() );
			ps.setObject( 9, lotacao.getCdCep() );
			ps.setObject( 10, lotacao.getDsTelefone() );
			ps.setObject( 11, lotacao.getDsFax() );
			ps.setObject( 12, lotacao.getDsCelular() );
			ps.setObject( 13, lotacao.getDsEMail() );
			
			ps.setObject( 14, lotacao.getCdUnimed() );
			ps.setObject( 15, lotacao.getIdEmpresa() );
			ps.setObject( 16, lotacao.getIdLotacao() );
			
			ps.execute();

		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}

	/**
	 * M�todo para excluir tupla de uma tabela
	 */
	public void exclui() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "delete from LOTACOES" );
			cmd.append( " where CD_UNIMED like ? and ID_EMPRESA = ? and ID_LOTACAO = ?" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			ps.setObject( 1, lotacao.getCdUnimed() );
			ps.setObject( 2, lotacao.getIdEmpresa() );
			ps.setObject( 3, lotacao.getIdLotacao() );
			
			ps.execute();

		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}
}