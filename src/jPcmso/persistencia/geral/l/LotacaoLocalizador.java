package jPcmso.persistencia.geral.l;

import quatro.persistencia.Localizador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.QtSQLException;
import quatro.sql.Query;
import quatro.sql.Conexao;
import quatro.sql.PoolDeConexoes;
import quatro.sql.ConexaoParaPoolDeConexoes;

/**
 *
 * Classe localizador para LOTACOES
 *
 * @author Samuel Antonio Klein
 * @see Localizador
 * @see Lotacao
 * @see LotacaoNavegador
 * @see LotacaoPersistor
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:34 - importado por Samuel Antonio Klein
 *
 */
 public class LotacaoLocalizador extends Localizador {

	/** Inst�ncia de Lotacao para manipula��o pelo localizador */
	private Lotacao lotacao;

	/** Construtor que recebe uma conex�o e um objeto Lotacao completo */
	public LotacaoLocalizador( Conexao conexao, Lotacao lotacao ) throws QtSQLException {

		super.setConexao( conexao );

		this.lotacao = lotacao;
		busca();
	}

	/** Construtor que recebe um objeto Lotacao completo */
	public LotacaoLocalizador( Lotacao lotacao ) throws QtSQLException {

		this.lotacao = lotacao;
		busca();
	}

	/** 
	 * Construtor que recebe uma conex�o e as chaves da tabela LOTACOES
	 * @param cdUnimed C�digo da Unimed
	 * @param idEmpresa Identificador da Empresa
	 * @param idLotacao Identificador da Lota��o
	 * @param conexao Conexao com o banco de dados
	 */
	public LotacaoLocalizador( Conexao conexao, String cdUnimed, int idEmpresa, int idLotacao ) throws QtSQLException { 

		super.setConexao( conexao ); 
		lotacao = new Lotacao();
		this.lotacao.setCdUnimed( cdUnimed );
		this.lotacao.setIdEmpresa( new Integer( idEmpresa ) );
		this.lotacao.setIdLotacao( new Integer( idLotacao ) );
		
		busca();
	}

	/** 
	 * Construtor que recebe as chaves da tabela LOTACOES
	 * @param cdUnimed C�digo da Unimed
	 * @param idEmpresa Identificador da Empresa
	 * @param idLotacao Identificador da Lota��o
	 */
	public LotacaoLocalizador( String cdUnimed, int idEmpresa, int idLotacao ) throws QtSQLException { 

		lotacao = new Lotacao();
		this.lotacao.setCdUnimed( cdUnimed );
		this.lotacao.setIdEmpresa( new Integer( idEmpresa ) );
		this.lotacao.setIdLotacao( new Integer( idLotacao ) );
		
		busca();
	}

	public static boolean existe( Conexao cnx, String cdUnimed, int idEmpresa, int idLotacao ) throws QtSQLException {

		Query q = new Query( cnx );

		try {
			q.setSQL( "select count(*) from LOTACOES" );
			q.addSQL( " where CD_UNIMED = :prm1CdUnimed and ID_EMPRESA = :prm2IdEmpresa and ID_LOTACAO = :prm3IdLotacao" );

			q.setParameter( "prm1CdUnimed", cdUnimed );
			q.setParameter( "prm2IdEmpresa", idEmpresa );
			q.setParameter( "prm3IdLotacao", idLotacao );
			q.executeQuery();

			return q.getInt( 1 ) > 0;
		} finally {
			q.liberaRecursos();
		}
	}

	public static boolean existe( String cdUnimed, int idEmpresa, int idLotacao ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return existe( cnx, cdUnimed, idEmpresa, idLotacao );
		} finally {
			cnx.libera();
		}
	}

	public static boolean existe( Conexao cnx, Lotacao lotacao ) throws QtSQLException {

		return existe( cnx, lotacao.getCdUnimed(), lotacao.getIdEmpresa(), lotacao.getIdLotacao() );
	}

	public static boolean existe( Lotacao lotacao ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return existe( cnx, lotacao.getCdUnimed(), lotacao.getIdEmpresa(), lotacao.getIdLotacao() );
		} finally {
			cnx.libera();
		}
	}

	public static Lotacao buscaLotacao( Conexao cnx, String cdUnimed, int idEmpresa, int idLotacao ) throws QtSQLException {

		Query q = new Query( cnx );

		try {
			q.setSQL( "select * from LOTACOES" );
			q.addSQL( " where CD_UNIMED = :prm1CdUnimed and ID_EMPRESA = :prm2IdEmpresa and ID_LOTACAO = :prm3IdLotacao" );

			q.setParameter( "prm1CdUnimed", cdUnimed );
			q.setParameter( "prm2IdEmpresa", idEmpresa );
			q.setParameter( "prm3IdLotacao", idLotacao );
			q.executeQuery();

			if( q.isEmpty() ) {
				return null;
			} else {
				Lotacao lotacao = new Lotacao();
				buscaCampos( lotacao, q );
				return lotacao;
			}
		} finally {
			q.liberaRecursos();
		}
	}

	public static Lotacao buscaLotacao( String cdUnimed, int idEmpresa, int idLotacao ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return buscaLotacao( cnx, cdUnimed, idEmpresa, idLotacao );
		} finally { 
			cnx.libera();
		}
	}

	/** 
	 * Construtor que recebe uma conex�o e as chaves da tabela LOTACOES
	 * @param cdUnimed C�digo da Unimed
	 * @param idEmpresa Identificador da Empresa
	 * @param idLotacao Identificador da Lota��o
	 * @param conexao Conexao com o banco de dados
	 */
	public LotacaoLocalizador( Conexao conexao, String cdUnimed, Integer idEmpresa, Integer idLotacao ) throws QtSQLException {

		super.setConexao( conexao ); 
		lotacao = new Lotacao();
		this.lotacao.setCdUnimed( cdUnimed );
		this.lotacao.setIdEmpresa( idEmpresa );
		this.lotacao.setIdLotacao( idLotacao );
		
		busca();
	}

	/** 
	 * Construtor que recebe as chaves da tabela LOTACOES
	 * @param cdUnimed C�digo da Unimed
	 * @param idEmpresa Identificador da Empresa
	 * @param idLotacao Identificador da Lota��o
	 */
	public LotacaoLocalizador( String cdUnimed, Integer idEmpresa, Integer idLotacao ) throws QtSQLException {

		lotacao = new Lotacao();
		this.lotacao.setCdUnimed( cdUnimed );
		this.lotacao.setIdEmpresa( idEmpresa );
		this.lotacao.setIdLotacao( idLotacao );
		
		busca();
	}

	public static Lotacao buscaPorNumero_da_Lotacao( Conexao cnx, Integer idLotacao ) throws QtSQLException {

		Query query = new Query( cnx );

		try {
			query.setSQL( "select * from LOTACOES" );
			query.addSQL( " where ID_LOTACAO = :prm3IdLotacao" );

			query.setParameter( "prm3IdLotacao", idLotacao );
			query.executeQuery();

			if( query.isEmpty() ) {
				return null;
			} else {
				Lotacao lotacao = new Lotacao();
				buscaCampos( lotacao, query );
				return lotacao;
			}
		} finally {
			query.liberaRecursos();
		}
	}

	public static Lotacao buscaPorNumero_da_Lotacao( Integer idLotacao ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
		try {
			return buscaPorNumero_da_Lotacao( cnx, idLotacao );
		} finally { 
			cnx.libera();
		}

	}

	public static boolean existeParaNumero_da_Lotacao( Conexao cnx, Integer idLotacao ) throws QtSQLException {

		Query query = new Query( cnx );

		try {
			query.setSQL( "select count(*) from LOTACOES" );
			query.addSQL( " where ID_LOTACAO = :prm3IdLotacao" );

			query.setParameter( "prm3IdLotacao", idLotacao );
			query.executeQuery();

			return query.getInt( 1 ) > 0;
		} finally {
			query.liberaRecursos();
		}
	}

	public static boolean existeParaNumero_da_Lotacao( Integer idLotacao ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
		try {
			return existeParaNumero_da_Lotacao( cnx, idLotacao );
		} finally {
			cnx.libera();
		}
	}

	public static void buscaCampos( Lotacao lotacao, Query query ) throws QtSQLException {

		lotacao.setCdUnimed( query.getString( "CD_UNIMED" ) );
		lotacao.setIdEmpresa( query.getInteger( "ID_EMPRESA" ) );
		lotacao.setIdLotacao( query.getInteger( "ID_LOTACAO" ) );
		lotacao.setDsLotacao( query.getString( "DS_LOTACAO" ) );
		lotacao.setCdEstado( query.getString( "CD_ESTADO" ) );
		lotacao.setIdCidade( query.getInteger( "ID_CIDADE" ) );
		lotacao.setDsBairro( query.getString( "DS_BAIRRO" ) );
		lotacao.setDsLogradouro( query.getString( "DS_LOGRADOURO" ) );
		lotacao.setCdCep( query.getString( "CD_CEP" ) );
		lotacao.setDsTelefone( query.getString( "DS_TELEFONE" ) );
		lotacao.setDsFax( query.getString( "DS_FAX" ) );
		lotacao.setDsCelular( query.getString( "DS_CELULAR" ) );
		lotacao.setDsEMail( query.getString( "DS_E_MAIL" ) );
	}

	/** M�todo que retorna uma tabela b�sica a que se refere o localizador */
	public TabelaBasica getTabelaBasica() {

		return this.lotacao;
	}

	/** M�todo que retorna a classe Lotacao relacionada ao localizador */
	public Lotacao getLotacao() {

		return this.lotacao;
	}

	/**
	 * M�todo para buscar e preencher uma tupla de uma tabela
	 * @param tabelaBasica Recebe uma tabela basica com os atributos chave preenchidos
	 */
	public boolean busca( TabelaBasica tabelaBasica ) throws QtSQLException {

		if( !( tabelaBasica instanceof Lotacao ) )
			throw new QtSQLException( "Esperava um objeto Lotacao!" );

		this.lotacao = (Lotacao) tabelaBasica;
		return busca();

	}

	/**
	 * M�todo privado para buscar e preencher uma tupla de uma tabela
	 * Este m�todo busca os dados da inst�ncia de Lotacao atual
	 */
	private boolean busca() throws QtSQLException {

		boolean ret = false;

		super.estabeleceConexao();
		Query query = super.criaNovaQuery();

		try {

			query.setSQL( "select * from LOTACOES" );
			query.addSQL( " where CD_UNIMED like :prm1CdUnimed and ID_EMPRESA = :prm2IdEmpresa and ID_LOTACAO = :prm3IdLotacao" );

			query.setParameter( "prm1CdUnimed", lotacao.getCdUnimed() );
			query.setParameter( "prm2IdEmpresa", lotacao.getIdEmpresa() );
			query.setParameter( "prm3IdLotacao", lotacao.getIdLotacao() );
			query.executeQuery();

			if( query.isEmpty() ) {
				super.setEmpty( true );
				lotacao = null;
			} else {
				super.setEmpty( false );
				
				lotacao.setCdUnimed( query.getString( "CD_UNIMED" ) );
				lotacao.setIdEmpresa( query.getInteger( "ID_EMPRESA" ) );
				lotacao.setIdLotacao( query.getInteger( "ID_LOTACAO" ) );
				lotacao.setDsLotacao( query.getString( "DS_LOTACAO" ) );
				lotacao.setCdEstado( query.getString( "CD_ESTADO" ) );
				lotacao.setIdCidade( query.getInteger( "ID_CIDADE" ) );
				lotacao.setDsBairro( query.getString( "DS_BAIRRO" ) );
				lotacao.setDsLogradouro( query.getString( "DS_LOGRADOURO" ) );
				lotacao.setCdCep( query.getString( "CD_CEP" ) );
				lotacao.setDsTelefone( query.getString( "DS_TELEFONE" ) );
				lotacao.setDsFax( query.getString( "DS_FAX" ) );
				lotacao.setDsCelular( query.getString( "DS_CELULAR" ) );
				lotacao.setDsEMail( query.getString( "DS_E_MAIL" ) );
				ret = true;
			}
		} finally {
			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}
 }