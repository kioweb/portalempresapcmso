package jPcmso.persistencia.geral.l;

import quatro.persistencia.CondicaoDeFiltro;
import quatro.persistencia.FiltroDeNavegador;

/**
 *
 * Classe filtro para LOTACOES
 *
 * @author Samuel Antonio Klein
 * @see FiltroDeTabela
 * @see Lotacao
 * @see LotacaoNavegador
 * @see LotacaoPersistor
 * @see LotacaoLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:34 - importado por Samuel Antonio Klein
 *
 */
 public class LotacaoFiltro extends FiltroDeNavegador {

	/** Filtro por descricao */
	public void setFiltroPorDescricao( String dsLotacao ) {

		super.adicionaCondicao( "DS_LOTACAO",CondicaoDeFiltro.IGUAL, dsLotacao );
	}

	/** Determina a ordem de navega��o que ser� passada para o navegador */
	public void setOrdemDeNavegacao( int idOrdem ) {

		super.setOrdemDeNavegacao( idOrdem );
	}
}