package jPcmso.persistencia.geral.l;

import quatro.persistencia.Caminhador;
import quatro.persistencia.FiltroDeNavegador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;

 /**
 *
 * Classe Caminhador para Lotacao
 *
 * @author Samuel Antonio Klein
 * @see Caminhador
 * @see FiltroDeNavegador
 * @see LotacaoNavegador
 * @see LotacaoPersistor
 * @see LotacaoLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:34 - importado por Samuel Antonio Klein
 *
 */
 public class LotacaoCaminhador extends Caminhador {

	public static final int POR_ORDEM_DEFAULT = -1;
	public static final int POR_CODIGO = 0;
	public static final int POR_DESCRICAO = 1;
	
	private Lotacao lotacao;

	public LotacaoCaminhador( FiltroDeNavegador filtro ) {

		setFiltro( filtro );
		inicializaVisao();
	}

	public LotacaoCaminhador( Conexao conexao, FiltroDeNavegador filtro ) throws QtSQLException {

		super.setConexao( conexao );
		setFiltro( filtro );
		inicializaVisao();
	}

	private void inicializaVisao() {

		setNmVisao( "LOTACOES" );
	}

	protected void carregaCampos() throws QtSQLException {

		lotacao = new Lotacao();

		if( isRecordAvailable() ) {
			lotacao.setCdUnimed( query.getString( "CD_UNIMED" ) );
			lotacao.setIdEmpresa( query.getInteger( "ID_EMPRESA" ) );
			lotacao.setIdLotacao( query.getInteger( "ID_LOTACAO" ) );
			lotacao.setDsLotacao( query.getString( "DS_LOTACAO" ) );
			lotacao.setCdEstado( query.getString( "CD_ESTADO" ) );
			lotacao.setIdCidade( query.getInteger( "ID_CIDADE" ) );
			lotacao.setDsBairro( query.getString( "DS_BAIRRO" ) );
			lotacao.setDsLogradouro( query.getString( "DS_LOGRADOURO" ) );
			lotacao.setCdCep( query.getString( "CD_CEP" ) );
			lotacao.setDsTelefone( query.getString( "DS_TELEFONE" ) );
			lotacao.setDsFax( query.getString( "DS_FAX" ) );
			lotacao.setDsCelular( query.getString( "DS_CELULAR" ) );
			lotacao.setDsEMail( query.getString( "DS_E_MAIL" ) );
		}
	}

	/** M�todo que retorna uma tabela b�sica a que se refere o caminhador */
	public TabelaBasica getTabelaBasica() {

		return this.lotacao;
	}

	/** M�todo que retorna a classe Lotacao relacionada ao caminhador */
	public Lotacao getLotacao() {

		return this.lotacao;
	}

	public void ativaVisao() throws QtSQLException {

		if(  super.getOrdemDeNavegacao() == LotacaoCaminhador.POR_ORDEM_DEFAULT ) {
			super.ativaCaminhador( "order by CD_UNIMED, ID_EMPRESA, ID_LOTACAO" );
		} else if(  super.getOrdemDeNavegacao() == LotacaoCaminhador.POR_CODIGO ) {
			super.ativaCaminhador( "order by CD_UNIMED, ID_EMPRESA, ID_LOTACAO" );
		} else if(  super.getOrdemDeNavegacao() == LotacaoCaminhador.POR_DESCRICAO ) {
			super.ativaCaminhador( "order by DS_LOTACAO" );
	
		} else {
			throw new QtSQLException( "LotacaoCaminhador - Ordem de Navega��o n�o definida!" );
		}
	}

	public String getCamposIndice( int qualIndice ) {
		return getDsIndice( qualIndice );
	}

	public String getCamposChave() {
		return getDsChave();
	}

	/**
	 * M�todo est�tico que devolve os campos que comp�e a chave da tabela;
	 * @return campos que comp�e a chave.
	 */
	public static String getDsChave() {
		return "CD_UNIMED, ID_EMPRESA, ID_LOTACAO";
	}

	/**
	 * M�todo est�tico que devolve os campos que fazem parte de uma dada ordem
	 * de navega��o.
	 * @param qualIndice indica a ordem de navega��o desejada.
	 * @return campos que comp�e a ordem de navega��o.
	 */
	public static String getDsIndice( int qualIndice ) {

		switch( qualIndice ) {

			case POR_ORDEM_DEFAULT: return "CD_UNIMED, ID_EMPRESA, ID_LOTACAO";
			case POR_CODIGO: return "CD_UNIMED, ID_EMPRESA, ID_LOTACAO";
			case POR_DESCRICAO: return "DS_LOTACAO";
		}
		return null;
	}
}