package jPcmso.persistencia.geral.l;

import quatro.persistencia.Caminhador;
import quatro.persistencia.FiltroDeNavegador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.util.QtData;

 /**
 *
 * Classe Caminhador para LotacaoAtribuicao
 *
 * @author Samuel Antonio Klein
 * @see Caminhador
 * @see FiltroDeNavegador
 * @see LotacaoAtribuicaoNavegador
 * @see LotacaoAtribuicaoPersistor
 * @see LotacaoAtribuicaoLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:31 - importado por Samuel Antonio Klein
 *
 */
 public class LotacaoAtribuicaoCaminhador extends Caminhador {

	public static final int POR_ORDEM_DEFAULT = -1;
	
	private LotacaoAtribuicao lotacaoAtribuicao;

	public LotacaoAtribuicaoCaminhador( FiltroDeNavegador filtro ) {

		setFiltro( filtro );
		inicializaVisao();
	}

	public LotacaoAtribuicaoCaminhador( Conexao conexao, FiltroDeNavegador filtro ) throws QtSQLException {

		super.setConexao( conexao );
		setFiltro( filtro );
		inicializaVisao();
	}

	private void inicializaVisao() {

		setNmVisao( "PCMSO.LOTACAO_ATRIBUICAO" );
	}

	protected void carregaCampos() throws QtSQLException {

		lotacaoAtribuicao = new LotacaoAtribuicao();

		if( isRecordAvailable() ) {
			lotacaoAtribuicao.setCdBeneficiarioCartao( query.getString( "CD_BENEFICIARIO_CARTAO" ) );
			lotacaoAtribuicao.setSqPerfil( query.getInteger( "SQ_PERFIL" ) );
			lotacaoAtribuicao.setSqLotacao( query.getString( "SQ_LOTACAO" ) );
			lotacaoAtribuicao.setDtInicio(  QtData.criaQtData( query.getTimestamp( "DT_INICIO" ) ) );
			lotacaoAtribuicao.setDtFinal(  QtData.criaQtData( query.getTimestamp( "DT_FINAL" ) ) );
			lotacaoAtribuicao.setCdEmpresa( query.getString( "CD_EMPRESA" ) );
			lotacaoAtribuicao.setNrCnpj( query.getString( "NR_CNPJ" ) );
			lotacaoAtribuicao.setIdSetor( query.getInteger( "ID_SETOR" ) );
			lotacaoAtribuicao.setIdCargo( query.getInteger( "ID_CARGO" ) );
			lotacaoAtribuicao.setIdFuncao( query.getInteger( "ID_FUNCAO" ) );
			lotacaoAtribuicao.setCdCbo( query.getString( "CD_CBO" ) );
			lotacaoAtribuicao.setCdGfip( query.getString( "CD_GFIP" ) );
			lotacaoAtribuicao.setDsAtividade( query.getString( "DS_ATIVIDADE" ) );
		}
	}

	/** M�todo que retorna uma tabela b�sica a que se refere o caminhador */
	public TabelaBasica getTabelaBasica() {

		return this.lotacaoAtribuicao;
	}

	/** M�todo que retorna a classe LotacaoAtribuicao relacionada ao caminhador */
	public LotacaoAtribuicao getLotacaoAtribuicao() {

		return this.lotacaoAtribuicao;
	}

	public void ativaVisao() throws QtSQLException {

		if(  super.getOrdemDeNavegacao() == LotacaoAtribuicaoCaminhador.POR_ORDEM_DEFAULT ) {
			super.ativaCaminhador( "order by CD_BENEFICIARIO_CARTAO, SQ_PERFIL, SQ_LOTACAO" );
	
		} else {
			throw new QtSQLException( "LotacaoAtribuicaoCaminhador - Ordem de Navega��o n�o definida!" );
		}
	}

	public String getCamposIndice( int qualIndice ) {
		return getDsIndice( qualIndice );
	}

	public String getCamposChave() {
		return getDsChave();
	}

	/**
	 * M�todo est�tico que devolve os campos que comp�e a chave da tabela;
	 * @return campos que comp�e a chave.
	 */
	public static String getDsChave() {
		return "CD_BENEFICIARIO_CARTAO, SQ_PERFIL, SQ_LOTACAO";
	}

	/**
	 * M�todo est�tico que devolve os campos que fazem parte de uma dada ordem
	 * de navega��o.
	 * @param qualIndice indica a ordem de navega��o desejada.
	 * @return campos que comp�e a ordem de navega��o.
	 */
	public static String getDsIndice( int qualIndice ) {

		switch( qualIndice ) {

			case POR_ORDEM_DEFAULT: return "CD_BENEFICIARIO_CARTAO, SQ_PERFIL, SQ_LOTACAO";
		}
		return null;
	}
}