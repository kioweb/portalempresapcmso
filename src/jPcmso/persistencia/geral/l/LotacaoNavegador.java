package jPcmso.persistencia.geral.l;

import quatro.persistencia.FiltroDeNavegador;
import quatro.persistencia.Navegador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.sql.Query;

/**
 *
 * Classe navegador para LOTACOES
 *
 * @author Samuel Antonio Klein
 * @see Navegador
 * @see Lotacao
 * @see LotacaoPersistor
 * @see LotacaoLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:34 - importado por Samuel Antonio Klein
 *
 */
 public class LotacaoNavegador extends Navegador {

	
	/** Inst�ncia de Lotacao para manipula��o pelo navegador */
	private Lotacao lotacao;

	/** Construtor b�sico */
	public LotacaoNavegador()
	  throws QtSQLException { 

		lotacao = new Lotacao();
		primeiro();
	}

	/** Construtor b�sico que recebe uma conex�o */
	public LotacaoNavegador( Conexao conexao )
	  throws QtSQLException { 

		super.setConexao( conexao ); 
		lotacao = new Lotacao();
		primeiro();
	}

	/**
	 * Construtor que recebe uma inst�ncia de LotacaoFiltro e posiciona
	 * no primeiro registro que satisfaz suas condi��es 
	 */
	public LotacaoNavegador( FiltroDeNavegador filtro ) throws QtSQLException  {
		super.setFiltro( filtro );
		this.lotacao = new Lotacao();
		primeiro();
	}

	/** Construtor que recebe as chaves da tabela LOTACOES
	 * @param cdUnimed C�digo da Unimed
	 * @param idEmpresa Identificador da Empresa
	 * @param idLotacao Identificador da Lota��o
	 */
	public LotacaoNavegador( String cdUnimed, int idEmpresa, int idLotacao )
	  throws QtSQLException { 

		lotacao = new Lotacao();
		this.lotacao.setCdUnimed( cdUnimed );
		this.lotacao.setIdEmpresa( new Integer( idEmpresa ) );
		this.lotacao.setIdLotacao( new Integer( idLotacao ) );
		
		busca();
	}

	/** Construtor que recebe  uma conex�o e as chaves da tabela LOTACOES
	 * @param cdUnimed C�digo da Unimed
	 * @param idEmpresa Identificador da Empresa
	 * @param idLotacao Identificador da Lota��o
	 * @param conexao Conexao com o banco de dados
	 */
	public LotacaoNavegador( Conexao conexao, String cdUnimed, int idEmpresa, int idLotacao )
	  throws QtSQLException { 

		super.setConexao( conexao ); 
		lotacao = new Lotacao();
		this.lotacao.setCdUnimed( cdUnimed );
		this.lotacao.setIdEmpresa( new Integer( idEmpresa ) );
		this.lotacao.setIdLotacao( new Integer( idLotacao ) );
		
		busca();
	}

	/** M�todo que retorna uma tabela b�sica a que se refere o navegador */
	public TabelaBasica getTabelaBasica() {

		return this.lotacao;
	}

	/** M�todo que retorna a classe Lotacao relacionada ao navegador */
	public Lotacao getLotacao() {

		return this.lotacao;
	}

	 /** M�todo que move para o primeiro registro da tabela */
	 public boolean primeiro() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select CD_UNIMED, ID_EMPRESA, ID_LOTACAO" );
			query.addSQL( "  from LOTACOES" );
			query.addSQL( super.getCondicaoDeFiltro( FiltroDeNavegador.COM_WHERE ) );
			query.addSQL( "  order by CD_UNIMED, ID_EMPRESA, ID_LOTACAO" );
			query.addSQL( "  limit 1 " );
			
			query.executeQuery();

			
			if( query.isEmpty() ) {

				super.setQueryStates( false, true, true );
			} else {
				this.lotacao = new Lotacao();

				lotacao.setCdUnimed( query.getString( "CD_UNIMED" ) );
				lotacao.setIdEmpresa( query.getInteger( "ID_EMPRESA" ) );
				lotacao.setIdLotacao( query.getInteger( "ID_LOTACAO" ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o registro anterior na tabela */
	 public boolean anterior() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select CD_UNIMED, ID_EMPRESA, ID_LOTACAO" );
			query.addSQL( "  from LOTACOES" );
			query.addSQL( "  where ( ( CD_UNIMED = :prm1CdUnimed and  ID_EMPRESA = :prm2IdEmpresa and  ID_LOTACAO < :prm3IdLotacao )" );
			query.addSQL( "  or (  CD_UNIMED = :prm1CdUnimed and  ID_EMPRESA < :prm2IdEmpresa )" );
			query.addSQL( "  or (  CD_UNIMED < :prm1CdUnimed ) )" );
			query.addSQL( super.getCondicaoDeFiltro() );
			query.addSQL( "  order by CD_UNIMED desc, ID_EMPRESA desc, ID_LOTACAO desc" );
			query.addSQL( "  limit 1 " );

			query.setParameter( "prm1CdUnimed", lotacao.getCdUnimed() );
			query.setParameter( "prm2IdEmpresa", lotacao.getIdEmpresa() );
			query.setParameter( "prm3IdLotacao", lotacao.getIdLotacao() );
		
			query.executeQuery();

			
			if( query.isEmpty() ) {

				super.setFound( false );
				super.setBof( true );
			} else {
				this.lotacao = new Lotacao();

				lotacao.setCdUnimed( query.getString( "CD_UNIMED" ) );
				lotacao.setIdEmpresa( query.getInteger( "ID_EMPRESA" ) );
				lotacao.setIdLotacao( query.getInteger( "ID_LOTACAO" ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o proximo registro da tabela */
	 public boolean proximo() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select CD_UNIMED, ID_EMPRESA, ID_LOTACAO" );
			query.addSQL( "  from LOTACOES" );
			query.addSQL( "  where ( (  CD_UNIMED = :prm1CdUnimed and  ID_EMPRESA = :prm2IdEmpresa and  ID_LOTACAO > :prm3IdLotacao )" );
			query.addSQL( "  or (  CD_UNIMED = :prm1CdUnimed and  ID_EMPRESA > :prm2IdEmpresa )" );
			query.addSQL( "  or (  CD_UNIMED > :prm1CdUnimed ) )" );
			query.addSQL( super.getCondicaoDeFiltro() );
			query.addSQL( "  order by CD_UNIMED, ID_EMPRESA, ID_LOTACAO" );
			query.addSQL( "  limit 1 " );

			query.setParameter( "prm1CdUnimed", lotacao.getCdUnimed() );
			query.setParameter( "prm2IdEmpresa", lotacao.getIdEmpresa() );
			query.setParameter( "prm3IdLotacao", lotacao.getIdLotacao() );
		
			query.executeQuery();

			
			if( query.isEmpty() ) {

				super.setFound( false );
				super.setEof( true );
			} else {
				this.lotacao = new Lotacao();

				lotacao.setCdUnimed( query.getString( "CD_UNIMED" ) );
				lotacao.setIdEmpresa( query.getInteger( "ID_EMPRESA" ) );
				lotacao.setIdLotacao( query.getInteger( "ID_LOTACAO" ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o �ltimo registro da tabela */
	 public boolean ultimo() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select CD_UNIMED, ID_EMPRESA, ID_LOTACAO" );
			query.addSQL( "  from LOTACOES" );
			query.addSQL( super.getCondicaoDeFiltro( FiltroDeNavegador.COM_WHERE ) 	);
			query.addSQL( "  order by CD_UNIMED desc, ID_EMPRESA desc, ID_LOTACAO desc" );
			query.addSQL( "  limit 1 " );

			query.executeQuery();

			
			if( query.isEmpty() ) {

				super.setQueryStates( false, true, true );
			} else {
				this.lotacao = new Lotacao();

				lotacao.setCdUnimed( query.getString( "CD_UNIMED" ) );
				lotacao.setIdEmpresa( query.getInteger( "ID_EMPRESA" ) );
				lotacao.setIdLotacao( query.getInteger( "ID_LOTACAO" ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	/**
	 * M�todo para buscar e preencher uma tupla de uma tabela
	 * @param tabelaBasica Recebe uma tabela basica com os atributos chave preenchidos
	 */
	public boolean busca( TabelaBasica tabelaBasica ) throws QtSQLException {

		if( !( tabelaBasica instanceof Lotacao ) )
			throw new QtSQLException( "Esperava um objeto Lotacao!" );

		this.lotacao = (Lotacao) tabelaBasica;
		return busca();

	}

	/**
	 * m�todo interno para buscar e preencher uma tupla de uma tabela
	 * Utiliza a Inst�ncia de Lotacao da classe para fazer a busca
	 */
	private boolean busca() throws QtSQLException {

		LotacaoLocalizador localizador = new LotacaoLocalizador( getConexao(), this.lotacao );

		if( localizador.isEmpty() ) {

			super.setQueryStates( false, true, true );
			return false;

		} else {

			super.setQueryStates( true, false, false );
			return true;
		}
	}
}