package jPcmso.persistencia.geral.l;

import quatro.persistencia.Localizador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.QtSQLException;
import quatro.sql.Query;
import quatro.sql.Conexao;
import quatro.sql.PoolDeConexoes;
import quatro.sql.ConexaoParaPoolDeConexoes;
import quatro.util.QtData;

/**
 *
 * Classe localizador para PCMSO.LOTACAO_ATRIBUICAO
 *
 * @author Samuel Antonio Klein
 * @see Localizador
 * @see LotacaoAtribuicao
 * @see LotacaoAtribuicaoNavegador
 * @see LotacaoAtribuicaoPersistor
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:31 - importado por Samuel Antonio Klein
 *
 */
 public class LotacaoAtribuicaoLocalizador extends Localizador {

	/** Inst�ncia de LotacaoAtribuicao para manipula��o pelo localizador */
	private LotacaoAtribuicao lotacaoAtribuicao;

	/** Construtor que recebe uma conex�o e um objeto LotacaoAtribuicao completo */
	public LotacaoAtribuicaoLocalizador( Conexao conexao, LotacaoAtribuicao lotacaoAtribuicao ) throws QtSQLException {

		super.setConexao( conexao );

		this.lotacaoAtribuicao = lotacaoAtribuicao;
		busca();
	}

	/** Construtor que recebe um objeto LotacaoAtribuicao completo */
	public LotacaoAtribuicaoLocalizador( LotacaoAtribuicao lotacaoAtribuicao ) throws QtSQLException {

		this.lotacaoAtribuicao = lotacaoAtribuicao;
		busca();
	}

	/** 
	 * Construtor que recebe uma conex�o e as chaves da tabela PCMSO.LOTACAO_ATRIBUICAO
	 * @param cdBeneficiarioCartao Codigo de Beneficiario 
	 * @param sqPerfil sequencia perfil
	 * @param sqLotacao sequencia lotacao
	 * @param conexao Conexao com o banco de dados
	 */
	public LotacaoAtribuicaoLocalizador( Conexao conexao, String cdBeneficiarioCartao, int sqPerfil, String sqLotacao ) throws QtSQLException { 

		super.setConexao( conexao ); 
		lotacaoAtribuicao = new LotacaoAtribuicao();
		this.lotacaoAtribuicao.setCdBeneficiarioCartao( cdBeneficiarioCartao );
		this.lotacaoAtribuicao.setSqPerfil( new Integer( sqPerfil ) );
		this.lotacaoAtribuicao.setSqLotacao( sqLotacao );
		
		busca();
	}

	/** 
	 * Construtor que recebe as chaves da tabela PCMSO.LOTACAO_ATRIBUICAO
	 * @param cdBeneficiarioCartao Codigo de Beneficiario 
	 * @param sqPerfil sequencia perfil
	 * @param sqLotacao sequencia lotacao
	 */
	public LotacaoAtribuicaoLocalizador( String cdBeneficiarioCartao, int sqPerfil, String sqLotacao ) throws QtSQLException { 

		lotacaoAtribuicao = new LotacaoAtribuicao();
		this.lotacaoAtribuicao.setCdBeneficiarioCartao( cdBeneficiarioCartao );
		this.lotacaoAtribuicao.setSqPerfil( new Integer( sqPerfil ) );
		this.lotacaoAtribuicao.setSqLotacao( sqLotacao );
		
		busca();
	}

	public static boolean existe( Conexao cnx, String cdBeneficiarioCartao, int sqPerfil, String sqLotacao ) throws QtSQLException {

		Query q = new Query( cnx );

		try {
			q.setSQL( "select count(*) from PCMSO.LOTACAO_ATRIBUICAO" );
			q.addSQL( " where CD_BENEFICIARIO_CARTAO = :prm1CdBeneficiarioCartao and SQ_PERFIL = :prm2SqPerfil and SQ_LOTACAO = :prm3SqLotacao" );

			q.setParameter( "prm1CdBeneficiarioCartao", cdBeneficiarioCartao );
			q.setParameter( "prm2SqPerfil", sqPerfil );
			q.setParameter( "prm3SqLotacao", sqLotacao );
			q.executeQuery();

			return q.getInt( 1 ) > 0;
		} finally {
			q.liberaRecursos();
		}
	}

	public static boolean existe( String cdBeneficiarioCartao, int sqPerfil, String sqLotacao ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return existe( cnx, cdBeneficiarioCartao, sqPerfil, sqLotacao );
		} finally {
			cnx.libera();
		}
	}

	public static boolean existe( Conexao cnx, LotacaoAtribuicao lotacaoAtribuicao ) throws QtSQLException {

		return existe( cnx, lotacaoAtribuicao.getCdBeneficiarioCartao(), lotacaoAtribuicao.getSqPerfil(), lotacaoAtribuicao.getSqLotacao() );
	}

	public static boolean existe( LotacaoAtribuicao lotacaoAtribuicao ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return existe( cnx, lotacaoAtribuicao.getCdBeneficiarioCartao(), lotacaoAtribuicao.getSqPerfil(), lotacaoAtribuicao.getSqLotacao() );
		} finally {
			cnx.libera();
		}
	}

	public static LotacaoAtribuicao buscaLotacaoAtribuicao( Conexao cnx, String cdBeneficiarioCartao, int sqPerfil, String sqLotacao ) throws QtSQLException {

		Query q = new Query( cnx );

		try {
			q.setSQL( "select * from PCMSO.LOTACAO_ATRIBUICAO" );
			q.addSQL( " where CD_BENEFICIARIO_CARTAO = :prm1CdBeneficiarioCartao and SQ_PERFIL = :prm2SqPerfil and SQ_LOTACAO = :prm3SqLotacao" );

			q.setParameter( "prm1CdBeneficiarioCartao", cdBeneficiarioCartao );
			q.setParameter( "prm2SqPerfil", sqPerfil );
			q.setParameter( "prm3SqLotacao", sqLotacao );
			q.executeQuery();

			if( q.isEmpty() ) {
				return null;
			} else {
				LotacaoAtribuicao lotacaoAtribuicao = new LotacaoAtribuicao();
				buscaCampos( lotacaoAtribuicao, q );
				return lotacaoAtribuicao;
			}
		} finally {
			q.liberaRecursos();
		}
	}

	public static LotacaoAtribuicao buscaLotacaoAtribuicao( String cdBeneficiarioCartao, int sqPerfil, String sqLotacao ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return buscaLotacaoAtribuicao( cnx, cdBeneficiarioCartao, sqPerfil, sqLotacao );
		} finally { 
			cnx.libera();
		}
	}

	/** 
	 * Construtor que recebe uma conex�o e as chaves da tabela PCMSO.LOTACAO_ATRIBUICAO
	 * @param cdBeneficiarioCartao Codigo de Beneficiario 
	 * @param sqPerfil sequencia perfil
	 * @param sqLotacao sequencia lotacao
	 * @param conexao Conexao com o banco de dados
	 */
	public LotacaoAtribuicaoLocalizador( Conexao conexao, String cdBeneficiarioCartao, Integer sqPerfil, String sqLotacao ) throws QtSQLException {

		super.setConexao( conexao ); 
		lotacaoAtribuicao = new LotacaoAtribuicao();
		this.lotacaoAtribuicao.setCdBeneficiarioCartao( cdBeneficiarioCartao );
		this.lotacaoAtribuicao.setSqPerfil( sqPerfil );
		this.lotacaoAtribuicao.setSqLotacao( sqLotacao );
		
		busca();
	}

	/** 
	 * Construtor que recebe as chaves da tabela PCMSO.LOTACAO_ATRIBUICAO
	 * @param cdBeneficiarioCartao Codigo de Beneficiario 
	 * @param sqPerfil sequencia perfil
	 * @param sqLotacao sequencia lotacao
	 */
	public LotacaoAtribuicaoLocalizador( String cdBeneficiarioCartao, Integer sqPerfil, String sqLotacao ) throws QtSQLException {

		lotacaoAtribuicao = new LotacaoAtribuicao();
		this.lotacaoAtribuicao.setCdBeneficiarioCartao( cdBeneficiarioCartao );
		this.lotacaoAtribuicao.setSqPerfil( sqPerfil );
		this.lotacaoAtribuicao.setSqLotacao( sqLotacao );
		
		busca();
	}

	public static void buscaCampos( LotacaoAtribuicao lotacaoAtribuicao, Query query ) throws QtSQLException {

		lotacaoAtribuicao.setCdBeneficiarioCartao( query.getString( "CD_BENEFICIARIO_CARTAO" ) );
		lotacaoAtribuicao.setSqPerfil( query.getInteger( "SQ_PERFIL" ) );
		lotacaoAtribuicao.setSqLotacao( query.getString( "SQ_LOTACAO" ) );
		lotacaoAtribuicao.setDtInicio( QtData.criaQtData( query.getTimestamp( "DT_INICIO" ) ) );
		lotacaoAtribuicao.setDtFinal( QtData.criaQtData( query.getTimestamp( "DT_FINAL" ) ) );
		lotacaoAtribuicao.setCdEmpresa( query.getString( "CD_EMPRESA" ) );
		lotacaoAtribuicao.setNrCnpj( query.getString( "NR_CNPJ" ) );
		lotacaoAtribuicao.setIdSetor( query.getInteger( "ID_SETOR" ) );
		lotacaoAtribuicao.setIdCargo( query.getInteger( "ID_CARGO" ) );
		lotacaoAtribuicao.setIdFuncao( query.getInteger( "ID_FUNCAO" ) );
		lotacaoAtribuicao.setCdCbo( query.getString( "CD_CBO" ) );
		lotacaoAtribuicao.setCdGfip( query.getString( "CD_GFIP" ) );
		lotacaoAtribuicao.setDsAtividade( query.getString( "DS_ATIVIDADE" ) );
	}

	/** M�todo que retorna uma tabela b�sica a que se refere o localizador */
	public TabelaBasica getTabelaBasica() {

		return this.lotacaoAtribuicao;
	}

	/** M�todo que retorna a classe LotacaoAtribuicao relacionada ao localizador */
	public LotacaoAtribuicao getLotacaoAtribuicao() {

		return this.lotacaoAtribuicao;
	}

	/**
	 * M�todo para buscar e preencher uma tupla de uma tabela
	 * @param tabelaBasica Recebe uma tabela basica com os atributos chave preenchidos
	 */
	public boolean busca( TabelaBasica tabelaBasica ) throws QtSQLException {

		if( !( tabelaBasica instanceof LotacaoAtribuicao ) )
			throw new QtSQLException( "Esperava um objeto LotacaoAtribuicao!" );

		this.lotacaoAtribuicao = (LotacaoAtribuicao) tabelaBasica;
		return busca();

	}

	/**
	 * M�todo privado para buscar e preencher uma tupla de uma tabela
	 * Este m�todo busca os dados da inst�ncia de LotacaoAtribuicao atual
	 */
	private boolean busca() throws QtSQLException {

		boolean ret = false;

		super.estabeleceConexao();
		Query query = super.criaNovaQuery();

		try {

			query.setSQL( "select * from PCMSO.LOTACAO_ATRIBUICAO" );
			query.addSQL( " where CD_BENEFICIARIO_CARTAO like :prm1CdBeneficiarioCartao and SQ_PERFIL = :prm2SqPerfil and SQ_LOTACAO like :prm3SqLotacao" );

			query.setParameter( "prm1CdBeneficiarioCartao", lotacaoAtribuicao.getCdBeneficiarioCartao() );
			query.setParameter( "prm2SqPerfil", lotacaoAtribuicao.getSqPerfil() );
			query.setParameter( "prm3SqLotacao", lotacaoAtribuicao.getSqLotacao() );
			query.executeQuery();

			if( query.isEmpty() ) {
				super.setEmpty( true );
				lotacaoAtribuicao = null;
			} else {
				super.setEmpty( false );
				
				lotacaoAtribuicao.setCdBeneficiarioCartao( query.getString( "CD_BENEFICIARIO_CARTAO" ) );
				lotacaoAtribuicao.setSqPerfil( query.getInteger( "SQ_PERFIL" ) );
				lotacaoAtribuicao.setSqLotacao( query.getString( "SQ_LOTACAO" ) );
				lotacaoAtribuicao.setDtInicio( QtData.criaQtData( query.getTimestamp( "DT_INICIO" ) ) );
				lotacaoAtribuicao.setDtFinal( QtData.criaQtData( query.getTimestamp( "DT_FINAL" ) ) );
				lotacaoAtribuicao.setCdEmpresa( query.getString( "CD_EMPRESA" ) );
				lotacaoAtribuicao.setNrCnpj( query.getString( "NR_CNPJ" ) );
				lotacaoAtribuicao.setIdSetor( query.getInteger( "ID_SETOR" ) );
				lotacaoAtribuicao.setIdCargo( query.getInteger( "ID_CARGO" ) );
				lotacaoAtribuicao.setIdFuncao( query.getInteger( "ID_FUNCAO" ) );
				lotacaoAtribuicao.setCdCbo( query.getString( "CD_CBO" ) );
				lotacaoAtribuicao.setCdGfip( query.getString( "CD_GFIP" ) );
				lotacaoAtribuicao.setDsAtividade( query.getString( "DS_ATIVIDADE" ) );
				ret = true;
			}
		} finally {
			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}
 }