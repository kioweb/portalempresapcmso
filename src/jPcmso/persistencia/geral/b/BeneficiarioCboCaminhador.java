package jPcmso.persistencia.geral.b;

import quatro.persistencia.Caminhador;
import quatro.persistencia.FiltroDeNavegador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.util.QtData;

 /**
 *
 * Classe Caminhador para BeneficiarioCbo
 *
 * @author Lucio Nascimento Teixeira
 * @see Caminhador
 * @see FiltroDeNavegador
 * @see BeneficiarioCboNavegador
 * @see BeneficiarioCboPersistor
 * @see BeneficiarioCboLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 23/06/2020 23:51 - importado por Lucio Nascimento Teixeira
 *
 */
 public class BeneficiarioCboCaminhador extends Caminhador {

	public static final int POR_ORDEM_DEFAULT = -1;
	
	private BeneficiarioCbo beneficiarioCbo;

	public BeneficiarioCboCaminhador( FiltroDeNavegador filtro ) {

		setFiltro( filtro );
		inicializaVisao();
	}

	public BeneficiarioCboCaminhador( Conexao conexao, FiltroDeNavegador filtro ) throws QtSQLException {

		super.setConexao( conexao );
		setFiltro( filtro );
		inicializaVisao();
	}

	private void inicializaVisao() {

		setNmVisao( "PCMSO.BENEFICIARIOS_CBO" );
	}

	protected void carregaCampos() throws QtSQLException {

		beneficiarioCbo = new BeneficiarioCbo();

		if( isRecordAvailable() ) {
			beneficiarioCbo.setCdBeneficiarioCartao( query.getString( "CD_BENEFICIARIO_CARTAO" ) );
			beneficiarioCbo.setDsNome( query.getString( "DS_NOME" ) );
			beneficiarioCbo.setCdDigito( query.getString( "CD_DIGITO" ) );
			beneficiarioCbo.setIdLotacao( query.getInteger( "ID_LOTACAO" ) );
			beneficiarioCbo.setCdUnimed( query.getString( "CD_UNIMED" ) );
			beneficiarioCbo.setCdEmpresa( query.getString( "CD_EMPRESA" ) );
			beneficiarioCbo.setIdFilial( query.getInteger( "ID_FILIAL" ) );
			beneficiarioCbo.setIdFuncao( query.getInteger( "ID_FUNCAO" ) );
			beneficiarioCbo.setIdSetor( query.getInteger( "ID_SETOR" ) );
			beneficiarioCbo.setIdCargo( query.getInteger( "ID_CARGO" ) );
			beneficiarioCbo.setCdCbo( query.getString( "CD_CBO" ) );
			beneficiarioCbo.setCdRg( query.getString( "CD_RG" ) );
			beneficiarioCbo.setNrCpf( query.getString( "NR_CPF" ) );
			beneficiarioCbo.setDtNascimento(  QtData.criaQtData( query.getTimestamp( "DT_NASCIMENTO" ) ) );
			beneficiarioCbo.setTpSexo( query.getString( "TP_SEXO" ) );
			beneficiarioCbo.setTpEstadoCivil( query.getString( "TP_ESTADO_CIVIL" ) );
			beneficiarioCbo.setVlPeso( query.getDoubleObject( "VL_PESO" ) );
			beneficiarioCbo.setVlAltura( query.getDoubleObject( "VL_ALTURA" ) );
			beneficiarioCbo.setDsAtividade( query.getString( "DS_ATIVIDADE" ) );
			beneficiarioCbo.setTpBrPdh( query.getString( "TP_BR_PDH" ) );
			beneficiarioCbo.setNrCtps( query.getString( "NR_CTPS" ) );
			beneficiarioCbo.setNrSerieUf( query.getString( "NR_SERIE_UF" ) );
			beneficiarioCbo.setNrPis( query.getString( "NR_PIS" ) );
			beneficiarioCbo.setDtAdmissao(  QtData.criaQtData( query.getTimestamp( "DT_ADMISSAO" ) ) );
			beneficiarioCbo.setDtInclusao(  QtData.criaQtData( query.getTimestamp( "DT_INCLUSAO" ) ) );
			beneficiarioCbo.setDtExclusao(  QtData.criaQtData( query.getTimestamp( "DT_EXCLUSAO" ) ) );
			beneficiarioCbo.setDsNomeDaMae( query.getString( "DS_NOME_DA_MAE" ) );
			beneficiarioCbo.setSnTrabalhoEmAltura( query.getString( "SN_TRABALHO_EM_ALTURA" ) );
			beneficiarioCbo.setSnEspacoConfinado( query.getString( "SN_ESPACO_CONFINADO" ) );
			beneficiarioCbo.setSnFitossanitario( query.getString( "SN_FITOSSANITARIO" ) );
			beneficiarioCbo.setIdFuncaoAnterior( query.getInteger( "ID_FUNCAO_ANTERIOR" ) );
			beneficiarioCbo.setSnPossuiDeficiencia( query.getString( "SN_POSSUI_DEFICIENCIA" ) );
		}
	}

	/** M�todo que retorna uma tabela b�sica a que se refere o caminhador */
	public TabelaBasica getTabelaBasica() {

		return this.beneficiarioCbo;
	}

	/** M�todo que retorna a classe BeneficiarioCbo relacionada ao caminhador */
	public BeneficiarioCbo getBeneficiarioCbo() {

		return this.beneficiarioCbo;
	}

	public void ativaVisao() throws QtSQLException {

		if(  super.getOrdemDeNavegacao() == BeneficiarioCboCaminhador.POR_ORDEM_DEFAULT ) {
			super.ativaCaminhador( "order by CD_BENEFICIARIO_CARTAO" );
	
		} else {
			throw new QtSQLException( "BeneficiarioCboCaminhador - Ordem de Navega��o n�o definida!" );
		}
	}

	public String getCamposIndice( int qualIndice ) {
		return getDsIndice( qualIndice );
	}

	public String getCamposChave() {
		return getDsChave();
	}

	/**
	 * M�todo est�tico que devolve os campos que comp�e a chave da tabela;
	 * @return campos que comp�e a chave.
	 */
	public static String getDsChave() {
		return "CD_BENEFICIARIO_CARTAO";
	}

	/**
	 * M�todo est�tico que devolve os campos que fazem parte de uma dada ordem
	 * de navega��o.
	 * @param qualIndice indica a ordem de navega��o desejada.
	 * @return campos que comp�e a ordem de navega��o.
	 */
	public static String getDsIndice( int qualIndice ) {

		switch( qualIndice ) {

			case POR_ORDEM_DEFAULT: return "CD_BENEFICIARIO_CARTAO";
		}
		return null;
	}
}