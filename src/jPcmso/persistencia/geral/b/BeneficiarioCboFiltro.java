package jPcmso.persistencia.geral.b;

import quatro.persistencia.FiltroDeNavegador;

/**
 *
 * Classe filtro para PCMSO.BENEFICIARIOS_CBO
 *
 * @author Lucio Nascimento Teixeira
 * @see FiltroDeTabela
 * @see BeneficiarioCbo
 * @see BeneficiarioCboNavegador
 * @see BeneficiarioCboPersistor
 * @see BeneficiarioCboLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 23/06/2020 23:51 - importado por Lucio Nascimento Teixeira
 *
 */
 public class BeneficiarioCboFiltro extends FiltroDeNavegador {

	/** Determina a ordem de navega��o que ser� passada para o navegador */
	public void setOrdemDeNavegacao( int idOrdem ) {

		super.setOrdemDeNavegacao( idOrdem );
	}
}