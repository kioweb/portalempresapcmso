package jPcmso.persistencia.geral.b;

import java.sql.PreparedStatement;

import quatro.persistencia.Persistor;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.util.QtException;
import quatro.util.QtData;

/**
 *
 * Classe persistor para PCMSO.BENEFICIARIOS_CBO
 *
 * @author Lucio Nascimento Teixeira
 * @see Persistor
 * @see BeneficiarioCbo
 * @see BeneficiarioCboNavegador
 * @see BeneficiarioCboLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 23/06/2020 23:51 - importado por Lucio Nascimento Teixeira
 *
 */
 public class BeneficiarioCboPersistor extends Persistor {

	/** Inst�ncia de BeneficiarioCbo para manipula��o pelo persistor */
	private BeneficiarioCbo beneficiarioCbo;

	/** Construtor gen�rico */
	public BeneficiarioCboPersistor() {

		this.beneficiarioCbo = null;
	}

	/** Construtor gen�rico */
	public BeneficiarioCboPersistor( Conexao cnx ) throws QtSQLException {

		this.beneficiarioCbo = null;
		super.setConexao( cnx );
	}

	/** Construtor que recebe uma inst�ncia de BeneficiarioCbo */
	public BeneficiarioCboPersistor( BeneficiarioCbo beneficiarioCbo ) throws QtSQLException {

		if( beneficiarioCbo == null ) 
			throw new QtSQLException( "Imposs�vel instanciar BeneficiarioCboPersistor porque est� apontando para um nulo!" );

		this.beneficiarioCbo = beneficiarioCbo;
	}

	/** Construtor que recebe uma conex�o e uma inst�ncia de BeneficiarioCbo */
	public BeneficiarioCboPersistor( Conexao conexao, BeneficiarioCbo beneficiarioCbo ) throws QtSQLException {

		if( beneficiarioCbo == null ) 
			throw new QtSQLException( "Imposs�vel instanciar BeneficiarioCboPersistor porque est� apontando para um nulo!" );

		setConexao( conexao );
		this.beneficiarioCbo = beneficiarioCbo;
	}

	/**
	 * Construtor que recebe a chave da tabela PCMSO.BENEFICIARIOS_CBO
	 * @param cdBeneficiarioCartao Codigo de Beneficiario 
	 */
	public BeneficiarioCboPersistor( String cdBeneficiarioCartao )
	  throws QtSQLException { 

		busca( cdBeneficiarioCartao );
	}

	/**
	 * Construtor que recebe uma conex�o e a chave da tabela PCMSO.BENEFICIARIOS_CBO
	 * @param cdBeneficiarioCartao Codigo de Beneficiario 
	 * @param conexao Conexao com o banco de dados
	 */
	public BeneficiarioCboPersistor( Conexao conexao, String cdBeneficiarioCartao )
	  throws QtSQLException { 

		super.setConexao( conexao );
		busca( cdBeneficiarioCartao );
	}

	/**
	 * M�todo interno para buscar e preencher uma tupla de uma tabela
	 * Utiliza a Inst�ncia de BeneficiarioCbo para fazer a busca
	 * @param cdBeneficiarioCartao Codigo de Beneficiario 
	 */
	private void busca( String cdBeneficiarioCartao ) throws QtSQLException { 

		BeneficiarioCboLocalizador beneficiarioCboLocalizador = new BeneficiarioCboLocalizador( getConexao(), cdBeneficiarioCartao );

		if( beneficiarioCboLocalizador.isEmpty() ) {
			throw new QtSQLException( "Chaves da Tabela BeneficiarioCbo n�o encontradas - cdBeneficiarioCartao: " + cdBeneficiarioCartao );
		}

		beneficiarioCbo = (BeneficiarioCbo ) beneficiarioCboLocalizador.getTabelaBasica();
	}

	/**
	 * Permite alimentar o Persistor com um novo registro
	 */
	public void setTabelaBasica( TabelaBasica beneficiarioCbo ) throws QtException {

		if( beneficiarioCbo instanceof BeneficiarioCbo ) {
			this.beneficiarioCbo = (BeneficiarioCbo) beneficiarioCbo;
		} else {
			throw new QtException( "Tabela B�sica fornecida tem que ser uma inst�ncia de BeneficiarioCbo" );
		}
	}

	/**
	 * Permite acessar o registro corrente do persistor
	 */
	public TabelaBasica getTabelaBasica() throws QtException {

		return beneficiarioCbo;
	}

	/**
	 * M�todo para inserir tupla na tabela
	 */
	public void insere() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "insert into PCMSO.BENEFICIARIOS_CBO" );
			cmd.append( "( CD_BENEFICIARIO_CARTAO, DS_NOME, CD_DIGITO, ID_LOTACAO, CD_UNIMED, CD_EMPRESA, ID_FILIAL, ID_FUNCAO, ID_SETOR, ID_CARGO, CD_CBO, CD_RG, NR_CPF, DT_NASCIMENTO, TP_SEXO, TP_ESTADO_CIVIL, VL_PESO, VL_ALTURA, DS_ATIVIDADE, TP_BR_PDH, NR_CTPS, NR_SERIE_UF, NR_PIS, DT_ADMISSAO, DT_INCLUSAO, DT_EXCLUSAO, DS_NOME_DA_MAE, SN_TRABALHO_EM_ALTURA, SN_ESPACO_CONFINADO, SN_FITOSSANITARIO, ID_FUNCAO_ANTERIOR, SN_POSSUI_DEFICIENCIA )" );
			cmd.append( "values " );
			cmd.append( "( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			try {

				ps.setObject( 1, beneficiarioCbo.getCdBeneficiarioCartao() );
				ps.setObject( 2, beneficiarioCbo.getDsNome() );
				ps.setObject( 3, beneficiarioCbo.getCdDigito() );
				ps.setObject( 4, beneficiarioCbo.getIdLotacao() );
				ps.setObject( 5, beneficiarioCbo.getCdUnimed() );
				ps.setObject( 6, beneficiarioCbo.getCdEmpresa() );
				ps.setObject( 7, beneficiarioCbo.getIdFilial() );
				ps.setObject( 8, beneficiarioCbo.getIdFuncao() );
				ps.setObject( 9, beneficiarioCbo.getIdSetor() );
				ps.setObject( 10, beneficiarioCbo.getIdCargo() );
				ps.setObject( 11, beneficiarioCbo.getCdCbo() );
				ps.setObject( 12, beneficiarioCbo.getCdRg() );
				ps.setObject( 13, beneficiarioCbo.getNrCpf() );
				ps.setObject( 14, QtData.getQtDataAsTimestamp( beneficiarioCbo.getDtNascimento() ) );
				ps.setObject( 15, beneficiarioCbo.getTpSexo() );
				ps.setObject( 16, beneficiarioCbo.getTpEstadoCivil() );
				ps.setObject( 17, beneficiarioCbo.getVlPeso() );
				ps.setObject( 18, beneficiarioCbo.getVlAltura() );
				ps.setObject( 19, beneficiarioCbo.getDsAtividade() );
				ps.setObject( 20, beneficiarioCbo.getTpBrPdh() );
				ps.setObject( 21, beneficiarioCbo.getNrCtps() );
				ps.setObject( 22, beneficiarioCbo.getNrSerieUf() );
				ps.setObject( 23, beneficiarioCbo.getNrPis() );
				ps.setObject( 24, QtData.getQtDataAsTimestamp( beneficiarioCbo.getDtAdmissao() ) );
				ps.setObject( 25, QtData.getQtDataAsTimestamp( beneficiarioCbo.getDtInclusao() ) );
				ps.setObject( 26, QtData.getQtDataAsTimestamp( beneficiarioCbo.getDtExclusao() ) );
				ps.setObject( 27, beneficiarioCbo.getDsNomeDaMae() );
				ps.setObject( 28, beneficiarioCbo.isSnTrabalhoEmAltura() ? "S": "N"  );
				ps.setObject( 29, beneficiarioCbo.isSnEspacoConfinado() ? "S": "N"  );
				ps.setObject( 30, beneficiarioCbo.isSnFitossanitario() ? "S": "N"  );
				ps.setObject( 31, beneficiarioCbo.getIdFuncaoAnterior() );
				ps.setObject( 32, beneficiarioCbo.isSnPossuiDeficiencia() ? "S": "N"  );
				
				ps.execute();
			} finally { 
				ps.close();
			}
		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}

	/**
	 * M�todo para alterar tupla na tabela
	 */
	public void altera() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "update PCMSO.BENEFICIARIOS_CBO" );
			cmd.append( "  set CD_BENEFICIARIO_CARTAO = ?, " );
			cmd.append( " DS_NOME = ?, " );
			cmd.append( " CD_DIGITO = ?, " );
			cmd.append( " ID_LOTACAO = ?, " );
			cmd.append( " CD_UNIMED = ?, " );
			cmd.append( " CD_EMPRESA = ?, " );
			cmd.append( " ID_FILIAL = ?, " );
			cmd.append( " ID_FUNCAO = ?, " );
			cmd.append( " ID_SETOR = ?, " );
			cmd.append( " ID_CARGO = ?, " );
			cmd.append( " CD_CBO = ?, " );
			cmd.append( " CD_RG = ?, " );
			cmd.append( " NR_CPF = ?, " );
			cmd.append( " DT_NASCIMENTO = ?, " );
			cmd.append( " TP_SEXO = ?, " );
			cmd.append( " TP_ESTADO_CIVIL = ?, " );
			cmd.append( " VL_PESO = ?, " );
			cmd.append( " VL_ALTURA = ?, " );
			cmd.append( " DS_ATIVIDADE = ?, " );
			cmd.append( " TP_BR_PDH = ?, " );
			cmd.append( " NR_CTPS = ?, " );
			cmd.append( " NR_SERIE_UF = ?, " );
			cmd.append( " NR_PIS = ?, " );
			cmd.append( " DT_ADMISSAO = ?, " );
			cmd.append( " DT_INCLUSAO = ?, " );
			cmd.append( " DT_EXCLUSAO = ?, " );
			cmd.append( " DS_NOME_DA_MAE = ?, " );
			cmd.append( " SN_TRABALHO_EM_ALTURA = ?, " );
			cmd.append( " SN_ESPACO_CONFINADO = ?, " );
			cmd.append( " SN_FITOSSANITARIO = ?, " );
			cmd.append( " ID_FUNCAO_ANTERIOR = ?, " );
			cmd.append( " SN_POSSUI_DEFICIENCIA = ?" );
			cmd.append( " where CD_BENEFICIARIO_CARTAO like ?" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			try {

				ps.setObject( 1, beneficiarioCbo.getCdBeneficiarioCartao() );
				ps.setObject( 2, beneficiarioCbo.getDsNome() );
				ps.setObject( 3, beneficiarioCbo.getCdDigito() );
				ps.setObject( 4, beneficiarioCbo.getIdLotacao() );
				ps.setObject( 5, beneficiarioCbo.getCdUnimed() );
				ps.setObject( 6, beneficiarioCbo.getCdEmpresa() );
				ps.setObject( 7, beneficiarioCbo.getIdFilial() );
				ps.setObject( 8, beneficiarioCbo.getIdFuncao() );
				ps.setObject( 9, beneficiarioCbo.getIdSetor() );
				ps.setObject( 10, beneficiarioCbo.getIdCargo() );
				ps.setObject( 11, beneficiarioCbo.getCdCbo() );
				ps.setObject( 12, beneficiarioCbo.getCdRg() );
				ps.setObject( 13, beneficiarioCbo.getNrCpf() );
				ps.setObject( 14, QtData.getQtDataAsTimestamp( beneficiarioCbo.getDtNascimento() ) );
				ps.setObject( 15, beneficiarioCbo.getTpSexo() );
				ps.setObject( 16, beneficiarioCbo.getTpEstadoCivil() );
				ps.setObject( 17, beneficiarioCbo.getVlPeso() );
				ps.setObject( 18, beneficiarioCbo.getVlAltura() );
				ps.setObject( 19, beneficiarioCbo.getDsAtividade() );
				ps.setObject( 20, beneficiarioCbo.getTpBrPdh() );
				ps.setObject( 21, beneficiarioCbo.getNrCtps() );
				ps.setObject( 22, beneficiarioCbo.getNrSerieUf() );
				ps.setObject( 23, beneficiarioCbo.getNrPis() );
				ps.setObject( 24, QtData.getQtDataAsTimestamp( beneficiarioCbo.getDtAdmissao() ) );
				ps.setObject( 25, QtData.getQtDataAsTimestamp( beneficiarioCbo.getDtInclusao() ) );
				ps.setObject( 26, QtData.getQtDataAsTimestamp( beneficiarioCbo.getDtExclusao() ) );
				ps.setObject( 27, beneficiarioCbo.getDsNomeDaMae() );
				ps.setObject( 28, beneficiarioCbo.isSnTrabalhoEmAltura() ? "S": "N"  );
				ps.setObject( 29, beneficiarioCbo.isSnEspacoConfinado() ? "S": "N"  );
				ps.setObject( 30, beneficiarioCbo.isSnFitossanitario() ? "S": "N"  );
				ps.setObject( 31, beneficiarioCbo.getIdFuncaoAnterior() );
				ps.setObject( 32, beneficiarioCbo.isSnPossuiDeficiencia() ? "S": "N"  );
				ps.setObject( 33, beneficiarioCbo.getCdBeneficiarioCartao() );
				
				ps.execute();
			} finally {
				ps.close();
			}
		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}

	/**
	 * M�todo para excluir tupla de uma tabela
	 */
	public void exclui() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "delete from PCMSO.BENEFICIARIOS_CBO" );
			cmd.append( " where CD_BENEFICIARIO_CARTAO like ?" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			try {

				ps.setObject( 1, beneficiarioCbo.getCdBeneficiarioCartao() );
				
				ps.execute();
			} finally {
				ps.close();
			}
		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}
}