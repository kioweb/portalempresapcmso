package jPcmso.persistencia.geral.b;

import quatro.persistencia.FiltroDeNavegador;
import quatro.persistencia.Navegador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.sql.Query;
import quatro.util.QtData;

/**
 *
 * Classe navegador para PCMSO.BENEFICIARIOS_CBO
 *
 * @author Lucio Nascimento Teixeira
 * @see Navegador
 * @see BeneficiarioCbo
 * @see BeneficiarioCboPersistor
 * @see BeneficiarioCboLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 23/06/2020 23:51 - importado por Lucio Nascimento Teixeira
 *
 */
 public class BeneficiarioCboNavegador extends Navegador {

	
	/** Inst�ncia de BeneficiarioCbo para manipula��o pelo navegador */
	private BeneficiarioCbo beneficiarioCbo;

	/** Construtor b�sico */
	public BeneficiarioCboNavegador()
	  throws QtSQLException { 

		beneficiarioCbo = new BeneficiarioCbo();
		primeiro();
	}

	/** Construtor b�sico que recebe uma conex�o */
	public BeneficiarioCboNavegador( Conexao conexao )
	  throws QtSQLException { 

		super.setConexao( conexao ); 
		beneficiarioCbo = new BeneficiarioCbo();
		primeiro();
	}

	/**
	 * Construtor que recebe uma inst�ncia de BeneficiarioCboFiltro e posiciona
	 * no primeiro registro que satisfaz suas condi��es 
	 */
	public BeneficiarioCboNavegador( FiltroDeNavegador filtro ) throws QtSQLException  {
		super.setFiltro( filtro );
		this.beneficiarioCbo = new BeneficiarioCbo();
		primeiro();
	}

	/** Construtor que recebe a chave da tabela PCMSO.BENEFICIARIOS_CBO
	 * @param cdBeneficiarioCartao Codigo de Beneficiario 
	 */
	public BeneficiarioCboNavegador( String cdBeneficiarioCartao )
	  throws QtSQLException { 

		beneficiarioCbo = new BeneficiarioCbo();
		this.beneficiarioCbo.setCdBeneficiarioCartao( cdBeneficiarioCartao );
		
		busca();
	}

	/** Construtor que recebe  uma conex�o e a chave da tabela PCMSO.BENEFICIARIOS_CBO
	 * @param cdBeneficiarioCartao Codigo de Beneficiario 
	 * @param conexao Conexao com o banco de dados
	 */
	public BeneficiarioCboNavegador( Conexao conexao, String cdBeneficiarioCartao )
	  throws QtSQLException { 

		super.setConexao( conexao ); 
		beneficiarioCbo = new BeneficiarioCbo();
		this.beneficiarioCbo.setCdBeneficiarioCartao( cdBeneficiarioCartao );
		
		busca();
	}

	/** M�todo que retorna uma tabela b�sica a que se refere o navegador */
	public TabelaBasica getTabelaBasica() {

		return this.beneficiarioCbo;
	}

	/** M�todo que retorna a classe BeneficiarioCbo relacionada ao navegador */
	public BeneficiarioCbo getBeneficiarioCbo() {

		return this.beneficiarioCbo;
	}

	 /** M�todo que move para o primeiro registro da tabela */
	 public boolean primeiro() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select min( CD_BENEFICIARIO_CARTAO ) from PCMSO.BENEFICIARIOS_CBO" );
			query.addSQL( super.getCondicaoDeFiltro( FiltroDeNavegador.COM_WHERE ) 	);

			query.executeQuery();

			
			if( query.getString( 1 ) == null ) {

				super.setQueryStates( false, true, true );
			} else {
				this.beneficiarioCbo = new BeneficiarioCbo();

				beneficiarioCbo.setCdBeneficiarioCartao( query.getString( 1 ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o registro anterior na tabela */
	 public boolean anterior() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select max( CD_BENEFICIARIO_CARTAO ) from PCMSO.BENEFICIARIOS_CBO" );
			query.addSQL( " where CD_BENEFICIARIO_CARTAO < :prm1CdBeneficiarioCartao" );
			query.addSQL( super.getCondicaoDeFiltro() );

			query.setParameter( "prm1CdBeneficiarioCartao", beneficiarioCbo.getCdBeneficiarioCartao() );
			
			query.executeQuery();

			
			if( query.getString( 1 ) == null ) {

				super.setFound( false );
				super.setBof( true );
			} else {
				this.beneficiarioCbo = new BeneficiarioCbo();

				beneficiarioCbo.setCdBeneficiarioCartao( query.getString( 1 ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o proximo registro da tabela */
	 public boolean proximo() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select min( CD_BENEFICIARIO_CARTAO ) from PCMSO.BENEFICIARIOS_CBO" );
			query.addSQL( " where CD_BENEFICIARIO_CARTAO > :prm1CdBeneficiarioCartao" );
			query.addSQL( super.getCondicaoDeFiltro() );

			query.setParameter( "prm1CdBeneficiarioCartao", beneficiarioCbo.getCdBeneficiarioCartao() );

			query.executeQuery();

			
			if( query.getString( 1 ) == null ) {

				super.setFound( false );
				super.setEof( true );
			} else {
				this.beneficiarioCbo = new BeneficiarioCbo();

				beneficiarioCbo.setCdBeneficiarioCartao( query.getString( 1 ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o �ltimo registro da tabela */
	 public boolean ultimo() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select max( CD_BENEFICIARIO_CARTAO ) from PCMSO.BENEFICIARIOS_CBO" );
			query.addSQL( super.getCondicaoDeFiltro( FiltroDeNavegador.COM_WHERE ) 	);

			query.executeQuery();

			
			if( query.getString( 1 ) == null ) {

				super.setQueryStates( false, true, true );
			} else {
				this.beneficiarioCbo = new BeneficiarioCbo();

				beneficiarioCbo.setCdBeneficiarioCartao( query.getString( 1 ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	/**
	 * M�todo para buscar e preencher uma tupla de uma tabela
	 * @param tabelaBasica Recebe uma tabela basica com os atributos chave preenchidos
	 */
	public boolean busca( TabelaBasica tabelaBasica ) throws QtSQLException {

		if( !( tabelaBasica instanceof BeneficiarioCbo ) )
			throw new QtSQLException( "Esperava um objeto BeneficiarioCbo!" );

		this.beneficiarioCbo = (BeneficiarioCbo) tabelaBasica;
		return busca();

	}

	/**
	 * m�todo interno para buscar e preencher uma tupla de uma tabela
	 * Utiliza a Inst�ncia de BeneficiarioCbo da classe para fazer a busca
	 */
	private boolean busca() throws QtSQLException {

		BeneficiarioCboLocalizador localizador = new BeneficiarioCboLocalizador( getConexao(), this.beneficiarioCbo );

		if( localizador.isEmpty() ) {

			super.setQueryStates( false, true, true );
			return false;

		} else {

			super.setQueryStates( true, false, false );
			return true;
		}
	}
}