package jPcmso.persistencia.geral.b;

import quatro.persistencia.Localizador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.QtSQLException;
import quatro.sql.Query;
import quatro.sql.Conexao;
import quatro.sql.PoolDeConexoes;
import quatro.sql.ConexaoParaPoolDeConexoes;
import quatro.util.QtData;

/**
 *
 * Classe localizador para PCMSO.BENEFICIARIOS_CBO
 *
 * @author Lucio Nascimento Teixeira
 * @see Localizador
 * @see BeneficiarioCbo
 * @see BeneficiarioCboNavegador
 * @see BeneficiarioCboPersistor
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 23/06/2020 23:51 - importado por Lucio Nascimento Teixeira
 *
 */
 public class BeneficiarioCboLocalizador extends Localizador {

	/** Inst�ncia de BeneficiarioCbo para manipula��o pelo localizador */
	private BeneficiarioCbo beneficiarioCbo;

	/** Construtor que recebe uma conex�o e um objeto BeneficiarioCbo completo */
	public BeneficiarioCboLocalizador( Conexao conexao, BeneficiarioCbo beneficiarioCbo ) throws QtSQLException {

		super.setConexao( conexao );

		this.beneficiarioCbo = beneficiarioCbo;
		busca();
	}

	/** Construtor que recebe um objeto BeneficiarioCbo completo */
	public BeneficiarioCboLocalizador( BeneficiarioCbo beneficiarioCbo ) throws QtSQLException {

		this.beneficiarioCbo = beneficiarioCbo;
		busca();
	}

	/** 
	 * Construtor que recebe uma conex�o e a chave da tabela PCMSO.BENEFICIARIOS_CBO
	 * @param cdBeneficiarioCartao Codigo de Beneficiario 
	 * @param conexao Conexao com o banco de dados
	 */
	public BeneficiarioCboLocalizador( Conexao conexao, String cdBeneficiarioCartao ) throws QtSQLException { 

		super.setConexao( conexao ); 
		beneficiarioCbo = new BeneficiarioCbo();
		this.beneficiarioCbo.setCdBeneficiarioCartao( cdBeneficiarioCartao );
		
		busca();
	}

	/** 
	 * Construtor que recebe a chave da tabela PCMSO.BENEFICIARIOS_CBO
	 * @param cdBeneficiarioCartao Codigo de Beneficiario 
	 */
	public BeneficiarioCboLocalizador( String cdBeneficiarioCartao ) throws QtSQLException { 

		beneficiarioCbo = new BeneficiarioCbo();
		this.beneficiarioCbo.setCdBeneficiarioCartao( cdBeneficiarioCartao );
		
		busca();
	}

	public static boolean existe( Conexao cnx, String cdBeneficiarioCartao ) throws QtSQLException {

		Query q = new Query( cnx );

		try {
			q.setSQL( "select count(*) from PCMSO.BENEFICIARIOS_CBO" );
			q.addSQL( " where CD_BENEFICIARIO_CARTAO = :prm1CdBeneficiarioCartao" );

			q.setParameter( "prm1CdBeneficiarioCartao", cdBeneficiarioCartao );
			q.executeQuery();

			return q.getInt( 1 ) > 0;
		} finally {
			q.liberaRecursos();
		}
	}

	public static boolean existe( String cdBeneficiarioCartao ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return existe( cnx, cdBeneficiarioCartao );
		} finally {
			cnx.libera();
		}
	}

	public static boolean existe( Conexao cnx, BeneficiarioCbo beneficiarioCbo ) throws QtSQLException {

		return existe( cnx, beneficiarioCbo.getCdBeneficiarioCartao() );
	}

	public static boolean existe( BeneficiarioCbo beneficiarioCbo ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return existe( cnx, beneficiarioCbo.getCdBeneficiarioCartao() );
		} finally {
			cnx.libera();
		}
	}

	public static BeneficiarioCbo buscaBeneficiarioCbo( Conexao cnx, String cdBeneficiarioCartao ) throws QtSQLException {

		Query q = new Query( cnx );

		try {
			q.setSQL( "select * from PCMSO.BENEFICIARIOS_CBO" );
			q.addSQL( " where CD_BENEFICIARIO_CARTAO = :prm1CdBeneficiarioCartao" );

			q.setParameter( "prm1CdBeneficiarioCartao", cdBeneficiarioCartao );
			q.executeQuery();

			if( q.isEmpty() ) {
				return null;
			} else {
				BeneficiarioCbo beneficiarioCbo = new BeneficiarioCbo();
				buscaCampos( beneficiarioCbo, q );
				return beneficiarioCbo;
			}
		} finally {
			q.liberaRecursos();
		}
	}

	public static BeneficiarioCbo buscaBeneficiarioCbo( String cdBeneficiarioCartao ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return buscaBeneficiarioCbo( cnx, cdBeneficiarioCartao );
		} finally { 
			cnx.libera();
		}
	}

	public static void buscaCampos( BeneficiarioCbo beneficiarioCbo, Query query ) throws QtSQLException {

		beneficiarioCbo.setCdBeneficiarioCartao( query.getString( "CD_BENEFICIARIO_CARTAO" ) );
		beneficiarioCbo.setDsNome( query.getString( "DS_NOME" ) );
		beneficiarioCbo.setCdDigito( query.getString( "CD_DIGITO" ) );
		beneficiarioCbo.setIdLotacao( query.getInteger( "ID_LOTACAO" ) );
		beneficiarioCbo.setCdUnimed( query.getString( "CD_UNIMED" ) );
		beneficiarioCbo.setCdEmpresa( query.getString( "CD_EMPRESA" ) );
		beneficiarioCbo.setIdFilial( query.getInteger( "ID_FILIAL" ) );
		beneficiarioCbo.setIdFuncao( query.getInteger( "ID_FUNCAO" ) );
		beneficiarioCbo.setIdSetor( query.getInteger( "ID_SETOR" ) );
		beneficiarioCbo.setIdCargo( query.getInteger( "ID_CARGO" ) );
		beneficiarioCbo.setCdCbo( query.getString( "CD_CBO" ) );
		beneficiarioCbo.setCdRg( query.getString( "CD_RG" ) );
		beneficiarioCbo.setNrCpf( query.getString( "NR_CPF" ) );
		beneficiarioCbo.setDtNascimento( QtData.criaQtData( query.getTimestamp( "DT_NASCIMENTO" ) ) );
		beneficiarioCbo.setTpSexo( query.getString( "TP_SEXO" ) );
		beneficiarioCbo.setTpEstadoCivil( query.getString( "TP_ESTADO_CIVIL" ) );
		beneficiarioCbo.setVlPeso( query.getDoubleObject( "VL_PESO" ) );
		beneficiarioCbo.setVlAltura( query.getDoubleObject( "VL_ALTURA" ) );
		beneficiarioCbo.setDsAtividade( query.getString( "DS_ATIVIDADE" ) );
		beneficiarioCbo.setTpBrPdh( query.getString( "TP_BR_PDH" ) );
		beneficiarioCbo.setNrCtps( query.getString( "NR_CTPS" ) );
		beneficiarioCbo.setNrSerieUf( query.getString( "NR_SERIE_UF" ) );
		beneficiarioCbo.setNrPis( query.getString( "NR_PIS" ) );
		beneficiarioCbo.setDtAdmissao( QtData.criaQtData( query.getTimestamp( "DT_ADMISSAO" ) ) );
		beneficiarioCbo.setDtInclusao( QtData.criaQtData( query.getTimestamp( "DT_INCLUSAO" ) ) );
		beneficiarioCbo.setDtExclusao( QtData.criaQtData( query.getTimestamp( "DT_EXCLUSAO" ) ) );
		beneficiarioCbo.setDsNomeDaMae( query.getString( "DS_NOME_DA_MAE" ) );
		beneficiarioCbo.setSnTrabalhoEmAltura( query.getString( "SN_TRABALHO_EM_ALTURA" ) );
		beneficiarioCbo.setSnEspacoConfinado( query.getString( "SN_ESPACO_CONFINADO" ) );
		beneficiarioCbo.setSnFitossanitario( query.getString( "SN_FITOSSANITARIO" ) );
		beneficiarioCbo.setIdFuncaoAnterior( query.getInteger( "ID_FUNCAO_ANTERIOR" ) );
		beneficiarioCbo.setSnPossuiDeficiencia( query.getString( "SN_POSSUI_DEFICIENCIA" ) );
	}

	/** M�todo que retorna uma tabela b�sica a que se refere o localizador */
	public TabelaBasica getTabelaBasica() {

		return this.beneficiarioCbo;
	}

	/** M�todo que retorna a classe BeneficiarioCbo relacionada ao localizador */
	public BeneficiarioCbo getBeneficiarioCbo() {

		return this.beneficiarioCbo;
	}

	/**
	 * M�todo para buscar e preencher uma tupla de uma tabela
	 * @param tabelaBasica Recebe uma tabela basica com os atributos chave preenchidos
	 */
	public boolean busca( TabelaBasica tabelaBasica ) throws QtSQLException {

		if( !( tabelaBasica instanceof BeneficiarioCbo ) )
			throw new QtSQLException( "Esperava um objeto BeneficiarioCbo!" );

		this.beneficiarioCbo = (BeneficiarioCbo) tabelaBasica;
		return busca();

	}

	/**
	 * M�todo privado para buscar e preencher uma tupla de uma tabela
	 * Este m�todo busca os dados da inst�ncia de BeneficiarioCbo atual
	 */
	private boolean busca() throws QtSQLException {

		boolean ret = false;

		super.estabeleceConexao();
		Query query = super.criaNovaQuery();

		try {

			query.setSQL( "select * from PCMSO.BENEFICIARIOS_CBO" );
			query.addSQL( " where CD_BENEFICIARIO_CARTAO like :prm1CdBeneficiarioCartao" );

			query.setParameter( "prm1CdBeneficiarioCartao", beneficiarioCbo.getCdBeneficiarioCartao() );
			query.executeQuery();

			if( query.isEmpty() ) {
				super.setEmpty( true );
				beneficiarioCbo = null;
			} else {
				super.setEmpty( false );
				
				beneficiarioCbo.setCdBeneficiarioCartao( query.getString( "CD_BENEFICIARIO_CARTAO" ) );
				beneficiarioCbo.setDsNome( query.getString( "DS_NOME" ) );
				beneficiarioCbo.setCdDigito( query.getString( "CD_DIGITO" ) );
				beneficiarioCbo.setIdLotacao( query.getInteger( "ID_LOTACAO" ) );
				beneficiarioCbo.setCdUnimed( query.getString( "CD_UNIMED" ) );
				beneficiarioCbo.setCdEmpresa( query.getString( "CD_EMPRESA" ) );
				beneficiarioCbo.setIdFilial( query.getInteger( "ID_FILIAL" ) );
				beneficiarioCbo.setIdFuncao( query.getInteger( "ID_FUNCAO" ) );
				beneficiarioCbo.setIdSetor( query.getInteger( "ID_SETOR" ) );
				beneficiarioCbo.setIdCargo( query.getInteger( "ID_CARGO" ) );
				beneficiarioCbo.setCdCbo( query.getString( "CD_CBO" ) );
				beneficiarioCbo.setCdRg( query.getString( "CD_RG" ) );
				beneficiarioCbo.setNrCpf( query.getString( "NR_CPF" ) );
				beneficiarioCbo.setDtNascimento( QtData.criaQtData( query.getTimestamp( "DT_NASCIMENTO" ) ) );
				beneficiarioCbo.setTpSexo( query.getString( "TP_SEXO" ) );
				beneficiarioCbo.setTpEstadoCivil( query.getString( "TP_ESTADO_CIVIL" ) );
				beneficiarioCbo.setVlPeso( query.getDoubleObject( "VL_PESO" ) );
				beneficiarioCbo.setVlAltura( query.getDoubleObject( "VL_ALTURA" ) );
				beneficiarioCbo.setDsAtividade( query.getString( "DS_ATIVIDADE" ) );
				beneficiarioCbo.setTpBrPdh( query.getString( "TP_BR_PDH" ) );
				beneficiarioCbo.setNrCtps( query.getString( "NR_CTPS" ) );
				beneficiarioCbo.setNrSerieUf( query.getString( "NR_SERIE_UF" ) );
				beneficiarioCbo.setNrPis( query.getString( "NR_PIS" ) );
				beneficiarioCbo.setDtAdmissao( QtData.criaQtData( query.getTimestamp( "DT_ADMISSAO" ) ) );
				beneficiarioCbo.setDtInclusao( QtData.criaQtData( query.getTimestamp( "DT_INCLUSAO" ) ) );
				beneficiarioCbo.setDtExclusao( QtData.criaQtData( query.getTimestamp( "DT_EXCLUSAO" ) ) );
				beneficiarioCbo.setDsNomeDaMae( query.getString( "DS_NOME_DA_MAE" ) );
				beneficiarioCbo.setSnTrabalhoEmAltura( query.getString( "SN_TRABALHO_EM_ALTURA" ) );
				beneficiarioCbo.setSnEspacoConfinado( query.getString( "SN_ESPACO_CONFINADO" ) );
				beneficiarioCbo.setSnFitossanitario( query.getString( "SN_FITOSSANITARIO" ) );
				beneficiarioCbo.setIdFuncaoAnterior( query.getInteger( "ID_FUNCAO_ANTERIOR" ) );
				beneficiarioCbo.setSnPossuiDeficiencia( query.getString( "SN_POSSUI_DEFICIENCIA" ) );
				ret = true;
			}
		} finally {
			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}
 }