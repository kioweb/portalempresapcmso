package jPcmso.persistencia.geral.b;

import quatro.persistencia.TabelaBasica;
import quatro.smartDB.IgnoredField;
import quatro.smartDB.PrimaryKey;
import quatro.smartDB.Table;
import quatro.util.QtData;
import quatro.util.QtString;

/**
 *
 * Classe b�sica para PCMSO.BENEFICIARIOS_CBO
 *
 * Tabela de Beneficiarios CBO
 * 
 * @author Lucio Nascimento Teixeira
 * @see BeneficiarioCboNavegador
 * @see BeneficiarioCboPersistor
 * @see BeneficiarioCboLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 23/06/2020 23:51 - importado por Lucio Nascimento Teixeira
 *
 */
 @Table(name="PCMSO.BENEFICIARIOS_CBO")
 public class BeneficiarioCbo implements TabelaBasica {

	@IgnoredField
	private static final long serialVersionUID = -12247368018765453L;

	/** Codigo de Beneficiario  (chave-char(12)) */
	@PrimaryKey
	private String cdBeneficiarioCartao;
	/** Nome beneficiario (varchar(250)) */
	private String dsNome;
	/** codigo do digito verificador (char(1)) */
	private String cdDigito;
	/** codigo de Lota��o */
	private Integer idLotacao;
	/** C�digo da Unimed (char(4)) */
	private String cdUnimed;
	/** Codigo da empresa (char(4)) */
	private String cdEmpresa;
	/** Codigo da filial */
	private Integer idFilial;
	/** Codigo da funcao */
	private Integer idFuncao;
	/** Codigo do setor */
	private Integer idSetor;
	/** Codigo do cargo */
	private Integer idCargo;
	/** Codigo do CBO (char(6)) */
	private String cdCbo;
	/** Codigo do RG (char(10)) */
	private String cdRg;
	/** Codigo do CPF (char(12)) */
	private String nrCpf;
	/** Data do nascimento */
	private QtData dtNascimento;
	/** 
	 * Tipo de sexo (char(1))
	 * 
	 * M - Masculino
	 * F - feminino
	 */
	private String tpSexo;
	/** 
	 * Tipo de Estado Civil (char(1)) (char(1))
	 *  
	 * S-Solteiro
	 * M-Casado (M-Married)
	 * W-Viuvo (W-Widow)
	 * D-Divorciado
	 * A-Apartado (Separado)
	 * U-Uni�o Est�vel
	 */
	private String tpEstadoCivil;
	/** Valor peso */
	private Double vlPeso;
	/** Valor altura */
	private Double vlAltura;
	/** Descricao da atividade */
	private String dsAtividade;
	/** 
	 * Tipo de BR PDH (char(1))
	 * 
	 * N-N�o aplicavel (NA)
	 * B-Beneficiario Reabilitado (BR)
	 * P-Portador de Deficiencia Habilitado (PDH)
	 * 
	 * 
	 */
	private String tpBrPdh;
	/** N�mero da carteira profissional (char(7)) */
	private String nrCtps;
	/** 
	 * N�mero de s�rie da carteira profissional (char(5))
	 * 
	 */
	private String nrSerieUf;
	/** N�mero do PIS (char(11)) */
	private String nrPis;
	/** Data de admissao */
	private QtData dtAdmissao;
	/** Data de inclusao */
	private QtData dtInclusao;
	/** Data de exclusao */
	private QtData dtExclusao;
	/** Nome da m�e (varchar(250)) */
	private String dsNomeDaMae;
	/** Indica se benefici�rio trabalha em altura (char(1)) */
	private boolean snTrabalhoEmAltura;
	/** Indica se benefici�rio trabalha em espa�o confinado (char(1)) */
	private boolean snEspacoConfinado;
	/** Indica se benefici�rio trabalha com fitossanit�rios(defensivos agr�colas) (char(1)) */
	private boolean snFitossanitario;
	/** Identifica a funcao anterior do benefici�rio */
	private Integer idFuncaoAnterior;
	/** Indica se benefici�rio possui deficiencia (char(1)) */
	private boolean snPossuiDeficiencia;

	/**
	 * Alimenta Codigo de Beneficiario 
	 * @param cdBeneficiarioCartao Codigo de Beneficiario 
	 */ 
	public void setCdBeneficiarioCartao( String cdBeneficiarioCartao ) {
		this.cdBeneficiarioCartao = cdBeneficiarioCartao;
	}

	/**
	 * Retorna Codigo de Beneficiario 
	 * @return Codigo de Beneficiario 
	 */
	public String getCdBeneficiarioCartao() {
		return this.cdBeneficiarioCartao;
	}

	/**
	 * Alimenta Nome beneficiario
	 * @param dsNome Nome beneficiario
	 */ 
	public void setDsNome( String dsNome ) {
		this.dsNome = dsNome;
	}

	/**
	 * Retorna Nome beneficiario
	 * @return Nome beneficiario
	 */
	public String getDsNome() {
		return this.dsNome;
	}

	/**
	 * Alimenta codigo do digito verificador
	 * @param cdDigito codigo do digito verificador
	 */ 
	public void setCdDigito( String cdDigito ) {
		this.cdDigito = cdDigito;
	}

	/**
	 * Retorna codigo do digito verificador
	 * @return codigo do digito verificador
	 */
	public String getCdDigito() {
		return this.cdDigito;
	}

	/**
	 * Alimenta codigo de Lota��o
	 * @param idLotacao codigo de Lota��o
	 */ 
	public void setIdLotacao( Integer idLotacao ) {
		this.idLotacao = idLotacao;
	}

	/**
	 * Retorna codigo de Lota��o
	 * @return codigo de Lota��o
	 */
	public Integer getIdLotacao() {
		return this.idLotacao;
	}

	/**
	 * Alimenta C�digo da Unimed
	 * @param cdUnimed C�digo da Unimed
	 */ 
	public void setCdUnimed( String cdUnimed ) {
		this.cdUnimed = cdUnimed;
	}

	/**
	 * Retorna C�digo da Unimed
	 * @return C�digo da Unimed
	 */
	public String getCdUnimed() {
		return this.cdUnimed;
	}

	/**
	 * Alimenta Codigo da empresa
	 * @param cdEmpresa Codigo da empresa
	 */ 
	public void setCdEmpresa( String cdEmpresa ) {
		this.cdEmpresa = cdEmpresa;
	}

	/**
	 * Retorna Codigo da empresa
	 * @return Codigo da empresa
	 */
	public String getCdEmpresa() {
		return this.cdEmpresa;
	}

	/**
	 * Alimenta Codigo da filial
	 * @param idFilial Codigo da filial
	 */ 
	public void setIdFilial( Integer idFilial ) {
		this.idFilial = idFilial;
	}

	/**
	 * Retorna Codigo da filial
	 * @return Codigo da filial
	 */
	public Integer getIdFilial() {
		return this.idFilial;
	}

	/**
	 * Alimenta Codigo da funcao
	 * @param idFuncao Codigo da funcao
	 */ 
	public void setIdFuncao( Integer idFuncao ) {
		this.idFuncao = idFuncao;
	}

	/**
	 * Retorna Codigo da funcao
	 * @return Codigo da funcao
	 */
	public Integer getIdFuncao() {
		return this.idFuncao;
	}

	/**
	 * Alimenta Codigo do setor
	 * @param idSetor Codigo do setor
	 */ 
	public void setIdSetor( Integer idSetor ) {
		this.idSetor = idSetor;
	}

	/**
	 * Retorna Codigo do setor
	 * @return Codigo do setor
	 */
	public Integer getIdSetor() {
		return this.idSetor;
	}

	/**
	 * Alimenta Codigo do cargo
	 * @param idCargo Codigo do cargo
	 */ 
	public void setIdCargo( Integer idCargo ) {
		this.idCargo = idCargo;
	}

	/**
	 * Retorna Codigo do cargo
	 * @return Codigo do cargo
	 */
	public Integer getIdCargo() {
		return this.idCargo;
	}

	/**
	 * Alimenta Codigo do CBO
	 * @param cdCbo Codigo do CBO
	 */ 
	public void setCdCbo( String cdCbo ) {
		this.cdCbo = cdCbo;
	}

	/**
	 * Retorna Codigo do CBO
	 * @return Codigo do CBO
	 */
	public String getCdCbo() {
		return this.cdCbo;
	}

	/**
	 * Alimenta Codigo do RG
	 * @param cdRg Codigo do RG
	 */ 
	public void setCdRg( String cdRg ) {
		this.cdRg = cdRg;
	}

	/**
	 * Retorna Codigo do RG
	 * @return Codigo do RG
	 */
	public String getCdRg() {
		return this.cdRg;
	}

	/**
	 * Alimenta Codigo do CPF
	 * @param nrCpf Codigo do CPF
	 */ 
	public void setNrCpf( String nrCpf ) {
		this.nrCpf = nrCpf;
	}

	/**
	 * Retorna Codigo do CPF
	 * @return Codigo do CPF
	 */
	public String getNrCpf() {
		return this.nrCpf;
	}

	/**
	 * Alimenta Data do nascimento
	 * @param dtNascimento Data do nascimento
	 */ 
	public void setDtNascimento( QtData dtNascimento ) {
		this.dtNascimento = dtNascimento;
	}

	/**
	 * Retorna Data do nascimento
	 * @return Data do nascimento
	 */
	public QtData getDtNascimento() {
		return this.dtNascimento;
	}

	/**
	 * Alimenta Tipo de sexo
	 * @param tpSexo Tipo de sexo
	 */ 
	public void setTpSexo( String tpSexo ) {
		this.tpSexo = tpSexo;
	}

	/**
	 * Retorna Tipo de sexo
	 * @return Tipo de sexo
	 */
	public String getTpSexo() {
		return this.tpSexo;
	}

	/**
	 * Alimenta Tipo de Estado Civil (char(1))
	 * @param tpEstadoCivil Tipo de Estado Civil (char(1))
	 */ 
	public void setTpEstadoCivil( String tpEstadoCivil ) {
		this.tpEstadoCivil = tpEstadoCivil;
	}

	/**
	 * Retorna Tipo de Estado Civil (char(1))
	 * @return Tipo de Estado Civil (char(1))
	 */
	public String getTpEstadoCivil() {
		return this.tpEstadoCivil;
	}

	/**
	 * Alimenta Valor peso
	 * @param vlPeso Valor peso
	 */ 
	public void setVlPeso( Double vlPeso ) {
		this.vlPeso = vlPeso;
	}

	/**
	 * Retorna Valor peso
	 * @return Valor peso
	 */
	public Double getVlPeso() {
		return this.vlPeso;
	}

	/**
	 * Alimenta Valor altura
	 * @param vlAltura Valor altura
	 */ 
	public void setVlAltura( Double vlAltura ) {
		this.vlAltura = vlAltura;
	}

	/**
	 * Retorna Valor altura
	 * @return Valor altura
	 */
	public Double getVlAltura() {
		return this.vlAltura;
	}

	/**
	 * Alimenta Descricao da atividade
	 * @param dsAtividade Descricao da atividade
	 */ 
	public void setDsAtividade( String dsAtividade ) {
		this.dsAtividade = dsAtividade;
	}

	/**
	 * Retorna Descricao da atividade
	 * @return Descricao da atividade
	 */
	public String getDsAtividade() {
		return this.dsAtividade;
	}

	/**
	 * Alimenta Tipo de BR PDH
	 * @param tpBrPdh Tipo de BR PDH
	 */ 
	public void setTpBrPdh( String tpBrPdh ) {
		this.tpBrPdh = tpBrPdh;
	}

	/**
	 * Retorna Tipo de BR PDH
	 * @return Tipo de BR PDH
	 */
	public String getTpBrPdh() {
		return this.tpBrPdh;
	}

	/**
	 * Alimenta N�mero da carteira profissional
	 * @param nrCtps N�mero da carteira profissional
	 */ 
	public void setNrCtps( String nrCtps ) {
		this.nrCtps = nrCtps;
	}

	/**
	 * Retorna N�mero da carteira profissional
	 * @return N�mero da carteira profissional
	 */
	public String getNrCtps() {
		return this.nrCtps;
	}

	/**
	 * Alimenta N�mero de s�rie da carteira profissional
	 * @param nrSerieUf N�mero de s�rie da carteira profissional
	 */ 
	public void setNrSerieUf( String nrSerieUf ) {
		this.nrSerieUf = nrSerieUf;
	}

	/**
	 * Retorna N�mero de s�rie da carteira profissional
	 * @return N�mero de s�rie da carteira profissional
	 */
	public String getNrSerieUf() {
		return this.nrSerieUf;
	}

	/**
	 * Alimenta N�mero do PIS
	 * @param nrPis N�mero do PIS
	 */ 
	public void setNrPis( String nrPis ) {
		this.nrPis = nrPis;
	}

	/**
	 * Retorna N�mero do PIS
	 * @return N�mero do PIS
	 */
	public String getNrPis() {
		return this.nrPis;
	}

	/**
	 * Alimenta Data de admissao
	 * @param dtAdmissao Data de admissao
	 */ 
	public void setDtAdmissao( QtData dtAdmissao ) {
		this.dtAdmissao = dtAdmissao;
	}

	/**
	 * Retorna Data de admissao
	 * @return Data de admissao
	 */
	public QtData getDtAdmissao() {
		return this.dtAdmissao;
	}

	/**
	 * Alimenta Data de inclusao
	 * @param dtInclusao Data de inclusao
	 */ 
	public void setDtInclusao( QtData dtInclusao ) {
		this.dtInclusao = dtInclusao;
	}

	/**
	 * Retorna Data de inclusao
	 * @return Data de inclusao
	 */
	public QtData getDtInclusao() {
		return this.dtInclusao;
	}

	/**
	 * Alimenta Data de exclusao
	 * @param dtExclusao Data de exclusao
	 */ 
	public void setDtExclusao( QtData dtExclusao ) {
		this.dtExclusao = dtExclusao;
	}

	/**
	 * Retorna Data de exclusao
	 * @return Data de exclusao
	 */
	public QtData getDtExclusao() {
		return this.dtExclusao;
	}

	/**
	 * Alimenta Nome da m�e
	 * @param dsNomeDaMae Nome da m�e
	 */ 
	public void setDsNomeDaMae( String dsNomeDaMae ) {
		this.dsNomeDaMae = dsNomeDaMae;
	}

	/**
	 * Retorna Nome da m�e
	 * @return Nome da m�e
	 */
	public String getDsNomeDaMae() {
		return this.dsNomeDaMae;
	}

	/**
	 * Alimenta Indica se benefici�rio trabalha em altura
	 * @param snTrabalhoEmAltura Indica se benefici�rio trabalha em altura
	 */ 
	public void setSnTrabalhoEmAltura( String snTrabalhoEmAltura ) {
		this.snTrabalhoEmAltura = QtString.toBoolean( snTrabalhoEmAltura );
	}

	/**
	 * Retorna Indica se benefici�rio trabalha em altura
	 * @return Indica se benefici�rio trabalha em altura
	 */
	public boolean isSnTrabalhoEmAltura() {
		return this.snTrabalhoEmAltura;
	}

	/**
	 * Alimenta Indica se benefici�rio trabalha em espa�o confinado
	 * @param snEspacoConfinado Indica se benefici�rio trabalha em espa�o confinado
	 */ 
	public void setSnEspacoConfinado( String snEspacoConfinado ) {
		this.snEspacoConfinado = QtString.toBoolean( snEspacoConfinado );
	}

	/**
	 * Retorna Indica se benefici�rio trabalha em espa�o confinado
	 * @return Indica se benefici�rio trabalha em espa�o confinado
	 */
	public boolean isSnEspacoConfinado() {
		return this.snEspacoConfinado;
	}

	/**
	 * Alimenta Indica se benefici�rio trabalha com fitossanit�rios(defensivos agr�colas)
	 * @param snFitossanitario Indica se benefici�rio trabalha com fitossanit�rios(defensivos agr�colas)
	 */ 
	public void setSnFitossanitario( String snFitossanitario ) {
		this.snFitossanitario = QtString.toBoolean( snFitossanitario );
	}

	/**
	 * Retorna Indica se benefici�rio trabalha com fitossanit�rios(defensivos agr�colas)
	 * @return Indica se benefici�rio trabalha com fitossanit�rios(defensivos agr�colas)
	 */
	public boolean isSnFitossanitario() {
		return this.snFitossanitario;
	}

	/**
	 * Alimenta Identifica a funcao anterior do benefici�rio
	 * @param idFuncaoAnterior Identifica a funcao anterior do benefici�rio
	 */ 
	public void setIdFuncaoAnterior( Integer idFuncaoAnterior ) {
		this.idFuncaoAnterior = idFuncaoAnterior;
	}

	/**
	 * Retorna Identifica a funcao anterior do benefici�rio
	 * @return Identifica a funcao anterior do benefici�rio
	 */
	public Integer getIdFuncaoAnterior() {
		return this.idFuncaoAnterior;
	}

	/**
	 * Alimenta Indica se benefici�rio possui deficiencia
	 * @param snPossuiDeficiencia Indica se benefici�rio possui deficiencia
	 */ 
	public void setSnPossuiDeficiencia( String snPossuiDeficiencia ) {
		this.snPossuiDeficiencia = QtString.toBoolean( snPossuiDeficiencia );
	}

	/**
	 * Retorna Indica se benefici�rio possui deficiencia
	 * @return Indica se benefici�rio possui deficiencia
	 */
	public boolean isSnPossuiDeficiencia() {
		return this.snPossuiDeficiencia;
	}

}