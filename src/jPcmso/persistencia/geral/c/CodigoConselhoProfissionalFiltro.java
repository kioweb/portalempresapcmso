package jPcmso.persistencia.geral.c;

import quatro.persistencia.CondicaoDeFiltro;
import quatro.persistencia.FiltroDeNavegador;

/**
 *
 * Classe filtro para CODIGOS_CONSELHO_PROFISSIONAL
 *
 * @author Samuel Antonio Klein
 * @see FiltroDeTabela
 * @see CodigoConselhoProfissional
 * @see CodigoConselhoProfissionalNavegador
 * @see CodigoConselhoProfissionalPersistor
 * @see CodigoConselhoProfissionalLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 public class CodigoConselhoProfissionalFiltro extends FiltroDeNavegador {

	/** Filtro por codigo */
	public void setFiltroPorCodigo( String cdSigla ) {

		super.adicionaCondicao( "CD_SIGLA",CondicaoDeFiltro.IGUAL, cdSigla );
	}

	/** Filtro por nome */
	public void setFiltroPorNome( String cdSigla ) {

		super.adicionaCondicao( "CD_SIGLA",CondicaoDeFiltro.IGUAL, cdSigla );
	}

	/** Determina a ordem de navega��o que ser� passada para o navegador */
	public void setOrdemDeNavegacao( int idOrdem ) {

		super.setOrdemDeNavegacao( idOrdem );
	}
}