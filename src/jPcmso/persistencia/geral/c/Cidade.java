package jPcmso.persistencia.geral.c;

import quatro.persistencia.TabelaBasica;
import quatro.smartDB.IgnoredField;
import quatro.smartDB.PrimaryKey;
import quatro.smartDB.Table;

/**
 *
 * Classe b�sica para CIDADES
 *
 * Esta tabela conter� as cidades que constam dos endere�os de diversas tabelas
 * 
 * @author Samuel Antonio Klein
 * @see CidadeNavegador
 * @see CidadePersistor
 * @see CidadeLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 @Table(name="CIDADES")
 public class Cidade implements TabelaBasica {

	@IgnoredField
	private static final long serialVersionUID = -5840569611073743886L;

	/** C�digo do Estado (chave-char(2)) */
	@PrimaryKey
	private String cdEstado;
	/** Identificador da Cidade (chave) */
	@PrimaryKey
	private Integer idCidade;
	/** Descri��o da Cidade (nome) (varchar(100)) */
	private String dsCidade;
	/** C�digo do Cep da cidade (varchar(8)) */
	private String cdCep;
	/** 
	 * Pra�a Bancaria (char(6))
	 * 
	 * Atualmente � usado pela Unimed 270 (Alto Uruguai)
	 */
	private String dsPracaBancaria;
	/** 
	 * C�digo do IBGE (varchar(7))
	 * 
	 * C�digo do Munic�pio, conforme codifica��o do IBGE.
	 * 
	 * Obs: o c�digo do IBGE tem originalmente 8 (oito) posi��es.
	 * Formato: EEMMMMVD, onde:
	 * EE = Estado
	 * MMMM = Munic�pio
	 * V = D�gito verificador
	 * D = Distrito (bairro)
	 * Utilizar apenas as primeiras 7 (sete) posi��es, que identificam as cidades.
	 * 
	 */
	private String cdIbge;
	/** D�gito do IBGE (char(1)) */
	private String cdDigitoIbge;

	/**
	 * Alimenta C�digo do Estado
	 * @param cdEstado C�digo do Estado
	 */ 
	public void setCdEstado( String cdEstado ) {
		this.cdEstado = cdEstado;
	}

	/**
	 * Retorna C�digo do Estado
	 * @return C�digo do Estado
	 */
	public String getCdEstado() {
		return this.cdEstado;
	}

	/**
	 * Alimenta Identificador da Cidade
	 * @param idCidade Identificador da Cidade
	 */ 
	public void setIdCidade( Integer idCidade ) {
		this.idCidade = idCidade;
	}

	/**
	 * Retorna Identificador da Cidade
	 * @return Identificador da Cidade
	 */
	public Integer getIdCidade() {
		return this.idCidade;
	}

	/**
	 * Alimenta Descri��o da Cidade (nome)
	 * @param dsCidade Descri��o da Cidade (nome)
	 */ 
	public void setDsCidade( String dsCidade ) {
		this.dsCidade = dsCidade;
	}

	/**
	 * Retorna Descri��o da Cidade (nome)
	 * @return Descri��o da Cidade (nome)
	 */
	public String getDsCidade() {
		return this.dsCidade;
	}

	/**
	 * Alimenta C�digo do Cep da cidade
	 * @param cdCep C�digo do Cep da cidade
	 */ 
	public void setCdCep( String cdCep ) {
		this.cdCep = cdCep;
	}

	/**
	 * Retorna C�digo do Cep da cidade
	 * @return C�digo do Cep da cidade
	 */
	public String getCdCep() {
		return this.cdCep;
	}

	/**
	 * Alimenta Pra�a Bancaria
	 * @param dsPracaBancaria Pra�a Bancaria
	 */ 
	public void setDsPracaBancaria( String dsPracaBancaria ) {
		this.dsPracaBancaria = dsPracaBancaria;
	}

	/**
	 * Retorna Pra�a Bancaria
	 * @return Pra�a Bancaria
	 */
	public String getDsPracaBancaria() {
		return this.dsPracaBancaria;
	}

	/**
	 * Alimenta C�digo do IBGE
	 * @param cdIbge C�digo do IBGE
	 */ 
	public void setCdIbge( String cdIbge ) {
		this.cdIbge = cdIbge;
	}

	/**
	 * Retorna C�digo do IBGE
	 * @return C�digo do IBGE
	 */
	public String getCdIbge() {
		return this.cdIbge;
	}

	/**
	 * Alimenta D�gito do IBGE
	 * @param cdDigitoIbge D�gito do IBGE
	 */ 
	public void setCdDigitoIbge( String cdDigitoIbge ) {
		this.cdDigitoIbge = cdDigitoIbge;
	}

	/**
	 * Retorna D�gito do IBGE
	 * @return D�gito do IBGE
	 */
	public String getCdDigitoIbge() {
		return this.cdDigitoIbge;
	}

}