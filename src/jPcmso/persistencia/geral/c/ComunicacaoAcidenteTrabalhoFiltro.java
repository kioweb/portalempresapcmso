package jPcmso.persistencia.geral.c;

import quatro.persistencia.FiltroDeNavegador;

/**
 *
 * Classe filtro para PCMSO.COMUNICACAO_ACIDENTE_TRABALHO
 *
 * @author Samuel Antonio Klein
 * @see FiltroDeTabela
 * @see ComunicacaoAcidenteTrabalho
 * @see ComunicacaoAcidenteTrabalhoNavegador
 * @see ComunicacaoAcidenteTrabalhoPersistor
 * @see ComunicacaoAcidenteTrabalhoLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:31 - importado por Samuel Antonio Klein
 *
 */
 public class ComunicacaoAcidenteTrabalhoFiltro extends FiltroDeNavegador {

	/** Determina a ordem de navega��o que ser� passada para o navegador */
	public void setOrdemDeNavegacao( int idOrdem ) {

		super.setOrdemDeNavegacao( idOrdem );
	}
}