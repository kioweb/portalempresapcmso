package jPcmso.persistencia.geral.c;

import java.sql.PreparedStatement;

import quatro.persistencia.Persistor;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.ConexaoParaPoolDeConexoes;
import quatro.sql.PoolDeConexoes;
import quatro.sql.QtSQLException;
import quatro.sql.Query;
import quatro.util.QtException;

/**
 *
 * Classe persistor para CIDADES
 *
 * @author Samuel Antonio Klein
 * @see Persistor
 * @see Cidade
 * @see CidadeNavegador
 * @see CidadeLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 public class CidadePersistor extends Persistor {

	/** Inst�ncia de Cidade para manipula��o pelo persistor */
	private Cidade cidade;

	/** Construtor gen�rico */
	public CidadePersistor() {

		this.cidade = null;
	}

	/** Construtor gen�rico */
	public CidadePersistor( Conexao cnx ) throws QtSQLException {

		this.cidade = null;
		super.setConexao( cnx );
	}

	/** Construtor que recebe uma inst�ncia de Cidade */
	public CidadePersistor( Cidade cidade ) throws QtSQLException {

		if( cidade == null ) 
			throw new QtSQLException( "Imposs�vel instanciar CidadePersistor porque est� apontando para um nulo!" );

		this.cidade = cidade;
	}

	/** Construtor que recebe uma conex�o e uma inst�ncia de Cidade */
	public CidadePersistor( Conexao conexao, Cidade cidade ) throws QtSQLException {

		if( cidade == null ) 
			throw new QtSQLException( "Imposs�vel instanciar CidadePersistor porque est� apontando para um nulo!" );

		setConexao( conexao );
		this.cidade = cidade;
	}

	/**
	 * Construtor que recebe as chaves da tabela CIDADES
	 * @param cdEstado C�digo do Estado
	 * @param idCidade Identificador da Cidade
	 */
	public CidadePersistor( String cdEstado, int idCidade )
	  throws QtSQLException { 

		busca( cdEstado, idCidade );
	}

	/**
	 * Construtor que recebe uma conex�o e as chaves da tabela CIDADES
	 * @param cdEstado C�digo do Estado
	 * @param idCidade Identificador da Cidade
	 * @param conexao Conexao com o banco de dados
	 */
	public CidadePersistor( Conexao conexao, String cdEstado, int idCidade )
	  throws QtSQLException { 

		super.setConexao( conexao );
		busca( cdEstado, idCidade );
	}

	/**
	 * M�todo interno para buscar e preencher uma tupla de uma tabela
	 * Utiliza a Inst�ncia de Cidade para fazer a busca
	 * @param cdEstado C�digo do Estado
	 * @param idCidade Identificador da Cidade
	 */
	private void busca( String cdEstado, int idCidade ) throws QtSQLException { 

		CidadeLocalizador cidadeLocalizador = new CidadeLocalizador( getConexao(), cdEstado, idCidade );

		if( cidadeLocalizador.isEmpty() ) {
			throw new QtSQLException( "Chaves da Tabela Cidade n�o encontradas - cdEstado: " + cdEstado + " / idCidade: " + idCidade );
		}

		cidade = (Cidade ) cidadeLocalizador.getTabelaBasica();
	}

	/**
	 * Permite alimentar o Persistor com um novo registro
	 */
	public void setTabelaBasica( TabelaBasica cidade ) throws QtException {

		if( cidade instanceof Cidade ) {
			this.cidade = (Cidade) cidade;
		} else {
			throw new QtException( "Tabela B�sica fornecida tem que ser uma inst�ncia de Cidade" );
		}
	}

	/**
	 * Permite acessar o registro corrente do persistor
	 */
	public TabelaBasica getTabelaBasica() throws QtException {

		return cidade;
	}

	/**
	 * M�todo para gerar automaticamente uma nova chave
	 */
	public static Integer geraNovoIdCidade( Conexao cnx ) throws QtSQLException {

		Integer result = null;

		Query query = new Query( cnx );

		try{

			query.setSQL( "select max( ID_CIDADE ) from CIDADES" );

			query.executeQuery();
			result = new Integer( query.getInt( 1 ) + 1 );
		} catch( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			query.liberaRecursos();
		}
		return result;
	}

	/**
	 * M�todo para gerar automaticamente uma nova chave
	 */
	public static Integer geraNovoIdCidade(  ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return geraNovoIdCidade(  cnx  );
		} finally {
			cnx.libera();
		}
	}


	/**
	 * M�todo para inserir tupla na tabela
	 */
	public void insere() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "insert into CIDADES" );
			cmd.append( "( CD_ESTADO, ID_CIDADE, DS_CIDADE, CD_CEP, DS_PRACA_BANCARIA, CD_IBGE, CD_DIGITO_IBGE )" );
			cmd.append( "values " );
			cmd.append( "( ?, ?, ?, ?, ?, ?, ?)" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			ps.setObject( 1, cidade.getCdEstado() );
			ps.setObject( 2, cidade.getIdCidade() );
			ps.setObject( 3, cidade.getDsCidade() );
			ps.setObject( 4, cidade.getCdCep() );
			ps.setObject( 5, cidade.getDsPracaBancaria() );
			ps.setObject( 6, cidade.getCdIbge() );
			ps.setObject( 7, cidade.getCdDigitoIbge() );
			
			ps.execute();

		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}

	/**
	 * M�todo para alterar tupla na tabela
	 */
	public void altera() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "update CIDADES" );
			cmd.append( "  set CD_ESTADO = ?, " );
			cmd.append( " ID_CIDADE = ?, " );
			cmd.append( " DS_CIDADE = ?, " );
			cmd.append( " CD_CEP = ?, " );
			cmd.append( " DS_PRACA_BANCARIA = ?, " );
			cmd.append( " CD_IBGE = ?, " );
			cmd.append( " CD_DIGITO_IBGE = ?" );
			cmd.append( " where CD_ESTADO like ? and ID_CIDADE = ?" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			ps.setObject( 1, cidade.getCdEstado() );
			ps.setObject( 2, cidade.getIdCidade() );
			ps.setObject( 3, cidade.getDsCidade() );
			ps.setObject( 4, cidade.getCdCep() );
			ps.setObject( 5, cidade.getDsPracaBancaria() );
			ps.setObject( 6, cidade.getCdIbge() );
			ps.setObject( 7, cidade.getCdDigitoIbge() );
			
			ps.setObject( 8, cidade.getCdEstado() );
			ps.setObject( 9, cidade.getIdCidade() );
			
			ps.execute();

		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}

	/**
	 * M�todo para excluir tupla de uma tabela
	 */
	public void exclui() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "delete from CIDADES" );
			cmd.append( " where CD_ESTADO like ? and ID_CIDADE = ?" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			ps.setObject( 1, cidade.getCdEstado() );
			ps.setObject( 2, cidade.getIdCidade() );
			
			ps.execute();

		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}
}