package jPcmso.persistencia.geral.c;

import quatro.persistencia.Localizador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.QtSQLException;
import quatro.sql.Query;
import quatro.sql.Conexao;
import quatro.sql.PoolDeConexoes;
import quatro.sql.ConexaoParaPoolDeConexoes;

/**
 *
 * Classe localizador para PCMSO.CARGOS
 *
 * @author ronei@unimed-uau.com.br
 * @see Localizador
 * @see Cargo
 * @see CargoNavegador
 * @see CargoPersistor
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 14/12/2017 15:26 - importado por ronei@unimed-uau.com.br
 *
 */
 public class CargoLocalizador extends Localizador {

	/** Instância de Cargo para manipulação pelo localizador */
	private Cargo cargo;

	/** Construtor que recebe uma conexão e um objeto Cargo completo */
	public CargoLocalizador( Conexao conexao, Cargo cargo ) throws QtSQLException {

		super.setConexao( conexao );

		this.cargo = cargo;
		busca();
	}

	/** Construtor que recebe um objeto Cargo completo */
	public CargoLocalizador( Cargo cargo ) throws QtSQLException {

		this.cargo = cargo;
		busca();
	}

	/** 
	 * Construtor que recebe uma conexão e a chave da tabela PCMSO.CARGOS
	 * @param idCargo Codigo do cargo
	 * @param conexao Conexao com o banco de dados
	 */
	public CargoLocalizador( Conexao conexao, int idCargo ) throws QtSQLException { 

		super.setConexao( conexao ); 
		cargo = new Cargo();
		this.cargo.setIdCargo( new Integer( idCargo ) );
		
		busca();
	}

	/** 
	 * Construtor que recebe a chave da tabela PCMSO.CARGOS
	 * @param idCargo Codigo do cargo
	 */
	public CargoLocalizador( int idCargo ) throws QtSQLException { 

		cargo = new Cargo();
		this.cargo.setIdCargo( new Integer( idCargo ) );
		
		busca();
	}

	public static boolean existe( Conexao cnx, int idCargo ) throws QtSQLException {

		Query q = new Query( cnx );

		try {
			q.setSQL( "select count(*) from PCMSO.CARGOS" );
			q.addSQL( " where ID_CARGO = :prm1IdCargo" );

			q.setParameter( "prm1IdCargo", idCargo );
			q.executeQuery();

			return q.getInt( 1 ) > 0;
		} finally {
			q.liberaRecursos();
		}
	}

	public static boolean existe( int idCargo ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return existe( cnx, idCargo );
		} finally {
			cnx.libera();
		}
	}

	public static boolean existe( Conexao cnx, Cargo cargo ) throws QtSQLException {

		return existe( cnx, cargo.getIdCargo() );
	}

	public static boolean existe( Cargo cargo ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return existe( cnx, cargo.getIdCargo() );
		} finally {
			cnx.libera();
		}
	}

	public static Cargo buscaCargo( Conexao cnx, int idCargo ) throws QtSQLException {

		Query q = new Query( cnx );

		try {
			q.setSQL( "select * from PCMSO.CARGOS" );
			q.addSQL( " where ID_CARGO = :prm1IdCargo" );

			q.setParameter( "prm1IdCargo", idCargo );
			q.executeQuery();

			if( q.isEmpty() ) {
				return null;
			} else {
				Cargo cargo = new Cargo();
				buscaCampos( cargo, q );
				return cargo;
			}
		} finally {
			q.liberaRecursos();
		}
	}

	public static Cargo buscaCargo( int idCargo ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return buscaCargo( cnx, idCargo );
		} finally { 
			cnx.libera();
		}
	}

	/** 
	 * Construtor que recebe uma conexão e a chave da tabela PCMSO.CARGOS
	 * @param idCargo Codigo do cargo
	 * @param conexao Conexao com o banco de dados
	 */
	public CargoLocalizador( Conexao conexao, Integer idCargo ) throws QtSQLException {

		super.setConexao( conexao ); 
		cargo = new Cargo();
		this.cargo.setIdCargo( idCargo );
		
		busca();
	}

	/** 
	 * Construtor que recebe a chave da tabela PCMSO.CARGOS
	 * @param idCargo Codigo do cargo
	 */
	public CargoLocalizador( Integer idCargo ) throws QtSQLException {

		cargo = new Cargo();
		this.cargo.setIdCargo( idCargo );
		
		busca();
	}

	public static void buscaCampos( Cargo cargo, Query query ) throws QtSQLException {

		cargo.setIdCargo( query.getInteger( "ID_CARGO" ) );
		cargo.setDsCargo( query.getString( "DS_CARGO" ) );
	}

	/** Método que retorna uma tabela básica a que se refere o localizador */
	public TabelaBasica getTabelaBasica() {

		return this.cargo;
	}

	/** Método que retorna a classe Cargo relacionada ao localizador */
	public Cargo getCargo() {

		return this.cargo;
	}

	/**
	 * Método para buscar e preencher uma tupla de uma tabela
	 * @param tabelaBasica Recebe uma tabela basica com os atributos chave preenchidos
	 */
	public boolean busca( TabelaBasica tabelaBasica ) throws QtSQLException {

		if( !( tabelaBasica instanceof Cargo ) )
			throw new QtSQLException( "Esperava um objeto Cargo!" );

		this.cargo = (Cargo) tabelaBasica;
		return busca();

	}

	/**
	 * Método privado para buscar e preencher uma tupla de uma tabela
	 * Este método busca os dados da instância de Cargo atual
	 */
	private boolean busca() throws QtSQLException {

		boolean ret = false;

		super.estabeleceConexao();
		Query query = super.criaNovaQuery();

		try {

			query.setSQL( "select * from PCMSO.CARGOS" );
			query.addSQL( " where ID_CARGO = :prm1IdCargo" );

			query.setParameter( "prm1IdCargo", cargo.getIdCargo() );
			query.executeQuery();

			if( query.isEmpty() ) {
				super.setEmpty( true );
				cargo = null;
			} else {
				super.setEmpty( false );
				
				cargo.setIdCargo( query.getInteger( "ID_CARGO" ) );
				cargo.setDsCargo( query.getString( "DS_CARGO" ) );
				ret = true;
			}
		} finally {
			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}
 }