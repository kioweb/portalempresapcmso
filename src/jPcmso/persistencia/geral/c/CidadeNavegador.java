package jPcmso.persistencia.geral.c;

import quatro.persistencia.FiltroDeNavegador;
import quatro.persistencia.Navegador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.sql.Query;

/**
 *
 * Classe navegador para CIDADES
 *
 * @author Samuel Antonio Klein
 * @see Navegador
 * @see Cidade
 * @see CidadePersistor
 * @see CidadeLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 public class CidadeNavegador extends Navegador {

	
	/** Inst�ncia de Cidade para manipula��o pelo navegador */
	private Cidade cidade;

	/** Construtor b�sico */
	public CidadeNavegador()
	  throws QtSQLException { 

		cidade = new Cidade();
		primeiro();
	}

	/** Construtor b�sico que recebe uma conex�o */
	public CidadeNavegador( Conexao conexao )
	  throws QtSQLException { 

		super.setConexao( conexao ); 
		cidade = new Cidade();
		primeiro();
	}

	/**
	 * Construtor que recebe uma inst�ncia de CidadeFiltro e posiciona
	 * no primeiro registro que satisfaz suas condi��es 
	 */
	public CidadeNavegador( FiltroDeNavegador filtro ) throws QtSQLException  {
		super.setFiltro( filtro );
		this.cidade = new Cidade();
		primeiro();
	}

	/** Construtor que recebe as chaves da tabela CIDADES
	 * @param cdEstado C�digo do Estado
	 * @param idCidade Identificador da Cidade
	 */
	public CidadeNavegador( String cdEstado, int idCidade )
	  throws QtSQLException { 

		cidade = new Cidade();
		this.cidade.setCdEstado( cdEstado );
		this.cidade.setIdCidade( new Integer( idCidade ) );
		
		busca();
	}

	/** Construtor que recebe  uma conex�o e as chaves da tabela CIDADES
	 * @param cdEstado C�digo do Estado
	 * @param idCidade Identificador da Cidade
	 * @param conexao Conexao com o banco de dados
	 */
	public CidadeNavegador( Conexao conexao, String cdEstado, int idCidade )
	  throws QtSQLException { 

		super.setConexao( conexao ); 
		cidade = new Cidade();
		this.cidade.setCdEstado( cdEstado );
		this.cidade.setIdCidade( new Integer( idCidade ) );
		
		busca();
	}

	/** M�todo que retorna uma tabela b�sica a que se refere o navegador */
	public TabelaBasica getTabelaBasica() {

		return this.cidade;
	}

	/** M�todo que retorna a classe Cidade relacionada ao navegador */
	public Cidade getCidade() {

		return this.cidade;
	}

	 /** M�todo que move para o primeiro registro da tabela */
	 public boolean primeiro() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select CD_ESTADO, ID_CIDADE" );
			query.addSQL( "  from CIDADES" );
			query.addSQL( super.getCondicaoDeFiltro( FiltroDeNavegador.COM_WHERE ) );
			query.addSQL( "  order by CD_ESTADO, ID_CIDADE" );
			query.addSQL( "  limit 1 " );
			
			query.executeQuery();

			
			if( query.isEmpty() ) {

				super.setQueryStates( false, true, true );
			} else {
				this.cidade = new Cidade();

				cidade.setCdEstado( query.getString( "CD_ESTADO" ) );
				cidade.setIdCidade( query.getInteger( "ID_CIDADE" ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o registro anterior na tabela */
	 public boolean anterior() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select CD_ESTADO, ID_CIDADE" );
			query.addSQL( "  from CIDADES" );
			query.addSQL( "  where ( ( CD_ESTADO = :prm1CdEstado and  ID_CIDADE < :prm2IdCidade )" );
			query.addSQL( "  or (  CD_ESTADO < :prm1CdEstado ) )" );
			query.addSQL( super.getCondicaoDeFiltro() );
			query.addSQL( "  order by CD_ESTADO desc, ID_CIDADE desc" );
			query.addSQL( "  limit 1 " );

			query.setParameter( "prm1CdEstado", cidade.getCdEstado() );
			query.setParameter( "prm2IdCidade", cidade.getIdCidade() );
		
			query.executeQuery();

			
			if( query.isEmpty() ) {

				super.setFound( false );
				super.setBof( true );
			} else {
				this.cidade = new Cidade();

				cidade.setCdEstado( query.getString( "CD_ESTADO" ) );
				cidade.setIdCidade( query.getInteger( "ID_CIDADE" ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o proximo registro da tabela */
	 public boolean proximo() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select CD_ESTADO, ID_CIDADE" );
			query.addSQL( "  from CIDADES" );
			query.addSQL( "  where ( (  CD_ESTADO = :prm1CdEstado and  ID_CIDADE > :prm2IdCidade )" );
			query.addSQL( "  or (  CD_ESTADO > :prm1CdEstado ) )" );
			query.addSQL( super.getCondicaoDeFiltro() );
			query.addSQL( "  order by CD_ESTADO, ID_CIDADE" );
			query.addSQL( "  limit 1 " );

			query.setParameter( "prm1CdEstado", cidade.getCdEstado() );
			query.setParameter( "prm2IdCidade", cidade.getIdCidade() );
		
			query.executeQuery();

			
			if( query.isEmpty() ) {

				super.setFound( false );
				super.setEof( true );
			} else {
				this.cidade = new Cidade();

				cidade.setCdEstado( query.getString( "CD_ESTADO" ) );
				cidade.setIdCidade( query.getInteger( "ID_CIDADE" ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o �ltimo registro da tabela */
	 public boolean ultimo() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select CD_ESTADO, ID_CIDADE" );
			query.addSQL( "  from CIDADES" );
			query.addSQL( super.getCondicaoDeFiltro( FiltroDeNavegador.COM_WHERE ) 	);
			query.addSQL( "  order by CD_ESTADO desc, ID_CIDADE desc" );
			query.addSQL( "  limit 1 " );

			query.executeQuery();

			
			if( query.isEmpty() ) {

				super.setQueryStates( false, true, true );
			} else {
				this.cidade = new Cidade();

				cidade.setCdEstado( query.getString( "CD_ESTADO" ) );
				cidade.setIdCidade( query.getInteger( "ID_CIDADE" ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	/**
	 * M�todo para buscar e preencher uma tupla de uma tabela
	 * @param tabelaBasica Recebe uma tabela basica com os atributos chave preenchidos
	 */
	public boolean busca( TabelaBasica tabelaBasica ) throws QtSQLException {

		if( !( tabelaBasica instanceof Cidade ) )
			throw new QtSQLException( "Esperava um objeto Cidade!" );

		this.cidade = (Cidade) tabelaBasica;
		return busca();

	}

	/**
	 * m�todo interno para buscar e preencher uma tupla de uma tabela
	 * Utiliza a Inst�ncia de Cidade da classe para fazer a busca
	 */
	private boolean busca() throws QtSQLException {

		CidadeLocalizador localizador = new CidadeLocalizador( getConexao(), this.cidade );

		if( localizador.isEmpty() ) {

			super.setQueryStates( false, true, true );
			return false;

		} else {

			super.setQueryStates( true, false, false );
			return true;
		}
	}
}