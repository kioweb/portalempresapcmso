package jPcmso.persistencia.geral.c;

import quatro.persistencia.FiltroDeNavegador;

/**
 *
 * Classe filtro para PCMSO.CBOS
 *
 * @author ronei@unimed-uau.com.br
 * @see FiltroDeTabela
 * @see Cbo
 * @see CboNavegador
 * @see CboPersistor
 * @see CboLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 24/01/2018 11:18 - importado por ronei@unimed-uau.com.br
 *
 */
 public class CboFiltro extends FiltroDeNavegador {

	/** Determina a ordem de navega��o que ser� passada para o navegador */
	public void setOrdemDeNavegacao( int idOrdem ) {

		super.setOrdemDeNavegacao( idOrdem );
	}
}