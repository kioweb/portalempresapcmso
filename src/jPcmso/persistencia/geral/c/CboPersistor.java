package jPcmso.persistencia.geral.c;

import java.sql.PreparedStatement;

import quatro.persistencia.Persistor;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.util.QtException;

/**
 *
 * Classe persistor para PCMSO.CBOS
 *
 * @author ronei@unimed-uau.com.br
 * @see Persistor
 * @see Cbo
 * @see CboNavegador
 * @see CboLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 24/01/2018 11:18 - importado por ronei@unimed-uau.com.br
 *
 */
 public class CboPersistor extends Persistor {

	/** Inst�ncia de Cbo para manipula��o pelo persistor */
	private Cbo cbo;

	/** Construtor gen�rico */
	public CboPersistor() {

		this.cbo = null;
	}

	/** Construtor gen�rico */
	public CboPersistor( Conexao cnx ) throws QtSQLException {

		this.cbo = null;
		super.setConexao( cnx );
	}

	/** Construtor que recebe uma inst�ncia de Cbo */
	public CboPersistor( Cbo cbo ) throws QtSQLException {

		if( cbo == null ) 
			throw new QtSQLException( "Imposs�vel instanciar CboPersistor porque est� apontando para um nulo!" );

		this.cbo = cbo;
	}

	/** Construtor que recebe uma conex�o e uma inst�ncia de Cbo */
	public CboPersistor( Conexao conexao, Cbo cbo ) throws QtSQLException {

		if( cbo == null ) 
			throw new QtSQLException( "Imposs�vel instanciar CboPersistor porque est� apontando para um nulo!" );

		setConexao( conexao );
		this.cbo = cbo;
	}

	/**
	 * Construtor que recebe a chave da tabela PCMSO.CBOS
	 * @param cdCbo Codigo de CBO
	 */
	public CboPersistor( String cdCbo )
	  throws QtSQLException { 

		busca( cdCbo );
	}

	/**
	 * Construtor que recebe uma conex�o e a chave da tabela PCMSO.CBOS
	 * @param cdCbo Codigo de CBO
	 * @param conexao Conexao com o banco de dados
	 */
	public CboPersistor( Conexao conexao, String cdCbo )
	  throws QtSQLException { 

		super.setConexao( conexao );
		busca( cdCbo );
	}

	/**
	 * M�todo interno para buscar e preencher uma tupla de uma tabela
	 * Utiliza a Inst�ncia de Cbo para fazer a busca
	 * @param cdCbo Codigo de CBO
	 */
	private void busca( String cdCbo ) throws QtSQLException { 

		CboLocalizador cboLocalizador = new CboLocalizador( getConexao(), cdCbo );

		if( cboLocalizador.isEmpty() ) {
			throw new QtSQLException( "Chaves da Tabela Cbo n�o encontradas - cdCbo: " + cdCbo );
		}

		cbo = (Cbo ) cboLocalizador.getTabelaBasica();
	}

	/**
	 * Permite alimentar o Persistor com um novo registro
	 */
	public void setTabelaBasica( TabelaBasica cbo ) throws QtException {

		if( cbo instanceof Cbo ) {
			this.cbo = (Cbo) cbo;
		} else {
			throw new QtException( "Tabela B�sica fornecida tem que ser uma inst�ncia de Cbo" );
		}
	}

	/**
	 * Permite acessar o registro corrente do persistor
	 */
	public TabelaBasica getTabelaBasica() throws QtException {

		return cbo;
	}

	/**
	 * M�todo para inserir tupla na tabela
	 */
	public void insere() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "insert into PCMSO.CBOS" );
			cmd.append( "( CD_CBO, NM_CBO, DS_ATIVIDADE )" );
			cmd.append( "values " );
			cmd.append( "( ?, ?, ?)" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			try {

				ps.setObject( 1, cbo.getCdCbo() );
				ps.setObject( 2, cbo.getNmCbo() );
				ps.setObject( 3, cbo.getDsAtividade() );
				
				ps.execute();
			} finally { 
				ps.close();
			}
		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}

	/**
	 * M�todo para alterar tupla na tabela
	 */
	public void altera() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "update PCMSO.CBOS" );
			cmd.append( "  set CD_CBO = ?, " );
			cmd.append( " NM_CBO = ?, " );
			cmd.append( " DS_ATIVIDADE = ?" );
			cmd.append( " where CD_CBO like ?" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			try {

				ps.setObject( 1, cbo.getCdCbo() );
				ps.setObject( 2, cbo.getNmCbo() );
				ps.setObject( 3, cbo.getDsAtividade() );
				ps.setObject( 4, cbo.getCdCbo() );
				
				ps.execute();
			} finally {
				ps.close();
			}
		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}

	/**
	 * M�todo para excluir tupla de uma tabela
	 */
	public void exclui() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "delete from PCMSO.CBOS" );
			cmd.append( " where CD_CBO like ?" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			try {

				ps.setObject( 1, cbo.getCdCbo() );
				
				ps.execute();
			} finally {
				ps.close();
			}
		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}
}