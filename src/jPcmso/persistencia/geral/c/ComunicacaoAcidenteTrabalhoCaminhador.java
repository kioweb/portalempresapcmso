package jPcmso.persistencia.geral.c;

import quatro.persistencia.Caminhador;
import quatro.persistencia.FiltroDeNavegador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.util.QtData;

 /**
 *
 * Classe Caminhador para ComunicacaoAcidenteTrabalho
 *
 * @author Samuel Antonio Klein
 * @see Caminhador
 * @see FiltroDeNavegador
 * @see ComunicacaoAcidenteTrabalhoNavegador
 * @see ComunicacaoAcidenteTrabalhoPersistor
 * @see ComunicacaoAcidenteTrabalhoLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:31 - importado por Samuel Antonio Klein
 *
 */
 public class ComunicacaoAcidenteTrabalhoCaminhador extends Caminhador {

	public static final int POR_ORDEM_DEFAULT = -1;
	
	private ComunicacaoAcidenteTrabalho comunicacaoAcidenteTrabalho;

	public ComunicacaoAcidenteTrabalhoCaminhador( FiltroDeNavegador filtro ) {

		setFiltro( filtro );
		inicializaVisao();
	}

	public ComunicacaoAcidenteTrabalhoCaminhador( Conexao conexao, FiltroDeNavegador filtro ) throws QtSQLException {

		super.setConexao( conexao );
		setFiltro( filtro );
		inicializaVisao();
	}

	private void inicializaVisao() {

		setNmVisao( "PCMSO.COMUNICACAO_ACIDENTE_TRABALHO" );
	}

	protected void carregaCampos() throws QtSQLException {

		comunicacaoAcidenteTrabalho = new ComunicacaoAcidenteTrabalho();

		if( isRecordAvailable() ) {
			comunicacaoAcidenteTrabalho.setCdBeneficiarioCartao( query.getString( "CD_BENEFICIARIO_CARTAO" ) );
			comunicacaoAcidenteTrabalho.setSqPerfil( query.getInteger( "SQ_PERFIL" ) );
			comunicacaoAcidenteTrabalho.setSqComunicacao( query.getInteger( "SQ_COMUNICACAO" ) );
			comunicacaoAcidenteTrabalho.setNrCat( query.getString( "NR_CAT" ) );
			comunicacaoAcidenteTrabalho.setDtRegistro(  QtData.criaQtData( query.getTimestamp( "DT_REGISTRO" ) ) );
		}
	}

	/** M�todo que retorna uma tabela b�sica a que se refere o caminhador */
	public TabelaBasica getTabelaBasica() {

		return this.comunicacaoAcidenteTrabalho;
	}

	/** M�todo que retorna a classe ComunicacaoAcidenteTrabalho relacionada ao caminhador */
	public ComunicacaoAcidenteTrabalho getComunicacaoAcidenteTrabalho() {

		return this.comunicacaoAcidenteTrabalho;
	}

	public void ativaVisao() throws QtSQLException {

		if(  super.getOrdemDeNavegacao() == ComunicacaoAcidenteTrabalhoCaminhador.POR_ORDEM_DEFAULT ) {
			super.ativaCaminhador( "order by CD_BENEFICIARIO_CARTAO, SQ_PERFIL, SQ_COMUNICACAO" );
	
		} else {
			throw new QtSQLException( "ComunicacaoAcidenteTrabalhoCaminhador - Ordem de Navega��o n�o definida!" );
		}
	}

	public String getCamposIndice( int qualIndice ) {
		return getDsIndice( qualIndice );
	}

	public String getCamposChave() {
		return getDsChave();
	}

	/**
	 * M�todo est�tico que devolve os campos que comp�e a chave da tabela;
	 * @return campos que comp�e a chave.
	 */
	public static String getDsChave() {
		return "CD_BENEFICIARIO_CARTAO, SQ_PERFIL, SQ_COMUNICACAO";
	}

	/**
	 * M�todo est�tico que devolve os campos que fazem parte de uma dada ordem
	 * de navega��o.
	 * @param qualIndice indica a ordem de navega��o desejada.
	 * @return campos que comp�e a ordem de navega��o.
	 */
	public static String getDsIndice( int qualIndice ) {

		switch( qualIndice ) {

			case POR_ORDEM_DEFAULT: return "CD_BENEFICIARIO_CARTAO, SQ_PERFIL, SQ_COMUNICACAO";
		}
		return null;
	}
}