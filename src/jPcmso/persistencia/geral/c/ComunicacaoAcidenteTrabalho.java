package jPcmso.persistencia.geral.c;

import quatro.persistencia.TabelaBasica;
import quatro.smartDB.IgnoredField;
import quatro.smartDB.PrimaryKey;
import quatro.smartDB.Table;
import quatro.util.QtData;

/**
 *
 * Classe básica para PCMSO.COMUNICACAO_ACIDENTE_TRABALHO
 *
 * Tabela de Beneficiarios CBO
 * 
 * @author Samuel Antonio Klein
 * @see ComunicacaoAcidenteTrabalhoNavegador
 * @see ComunicacaoAcidenteTrabalhoPersistor
 * @see ComunicacaoAcidenteTrabalhoLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:31 - importado por Samuel Antonio Klein
 *
 */
 @Table(name="PCMSO.COMUNICACAO_ACIDENTE_TRABALHO")
 public class ComunicacaoAcidenteTrabalho implements TabelaBasica {

	@IgnoredField
	private static final long serialVersionUID = -63610981060101631L;

	/** Codigo de Beneficiario  (chave-char(12)) */
	@PrimaryKey
	private String cdBeneficiarioCartao;
	/** sequencia perfil (chave) */
	@PrimaryKey
	private Integer sqPerfil;
	/** sequencia Comunicação (chave) */
	@PrimaryKey
	private Integer sqComunicacao;
	/** numero Comunicações de Acidente do Trabalho  (char(13)) */
	private String nrCat;
	/** data do registro da Comunicações de Acidente do Trabalho  */
	private QtData dtRegistro;

	/**
	 * Alimenta Codigo de Beneficiario 
	 * @param cdBeneficiarioCartao Codigo de Beneficiario 
	 */ 
	public void setCdBeneficiarioCartao( String cdBeneficiarioCartao ) {
		this.cdBeneficiarioCartao = cdBeneficiarioCartao;
	}

	/**
	 * Retorna Codigo de Beneficiario 
	 * @return Codigo de Beneficiario 
	 */
	public String getCdBeneficiarioCartao() {
		return this.cdBeneficiarioCartao;
	}

	/**
	 * Alimenta sequencia perfil
	 * @param sqPerfil sequencia perfil
	 */ 
	public void setSqPerfil( Integer sqPerfil ) {
		this.sqPerfil = sqPerfil;
	}

	/**
	 * Retorna sequencia perfil
	 * @return sequencia perfil
	 */
	public Integer getSqPerfil() {
		return this.sqPerfil;
	}

	/**
	 * Alimenta sequencia Comunicação
	 * @param sqComunicacao sequencia Comunicação
	 */ 
	public void setSqComunicacao( Integer sqComunicacao ) {
		this.sqComunicacao = sqComunicacao;
	}

	/**
	 * Retorna sequencia Comunicação
	 * @return sequencia Comunicação
	 */
	public Integer getSqComunicacao() {
		return this.sqComunicacao;
	}

	/**
	 * Alimenta numero Comunicações de Acidente do Trabalho 
	 * @param nrCat numero Comunicações de Acidente do Trabalho 
	 */ 
	public void setNrCat( String nrCat ) {
		this.nrCat = nrCat;
	}

	/**
	 * Retorna numero Comunicações de Acidente do Trabalho 
	 * @return numero Comunicações de Acidente do Trabalho 
	 */
	public String getNrCat() {
		return this.nrCat;
	}

	/**
	 * Alimenta data do registro da Comunicações de Acidente do Trabalho 
	 * @param dtRegistro data do registro da Comunicações de Acidente do Trabalho 
	 */ 
	public void setDtRegistro( QtData dtRegistro ) {
		this.dtRegistro = dtRegistro;
	}

	/**
	 * Retorna data do registro da Comunicações de Acidente do Trabalho 
	 * @return data do registro da Comunicações de Acidente do Trabalho 
	 */
	public QtData getDtRegistro() {
		return this.dtRegistro;
	}

}