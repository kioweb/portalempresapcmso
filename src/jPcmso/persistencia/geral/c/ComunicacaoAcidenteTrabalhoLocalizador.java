package jPcmso.persistencia.geral.c;

import quatro.persistencia.Localizador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.QtSQLException;
import quatro.sql.Query;
import quatro.sql.Conexao;
import quatro.sql.PoolDeConexoes;
import quatro.sql.ConexaoParaPoolDeConexoes;
import quatro.util.QtData;

/**
 *
 * Classe localizador para PCMSO.COMUNICACAO_ACIDENTE_TRABALHO
 *
 * @author Samuel Antonio Klein
 * @see Localizador
 * @see ComunicacaoAcidenteTrabalho
 * @see ComunicacaoAcidenteTrabalhoNavegador
 * @see ComunicacaoAcidenteTrabalhoPersistor
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:31 - importado por Samuel Antonio Klein
 *
 */
 public class ComunicacaoAcidenteTrabalhoLocalizador extends Localizador {

	/** Inst�ncia de ComunicacaoAcidenteTrabalho para manipula��o pelo localizador */
	private ComunicacaoAcidenteTrabalho comunicacaoAcidenteTrabalho;

	/** Construtor que recebe uma conex�o e um objeto ComunicacaoAcidenteTrabalho completo */
	public ComunicacaoAcidenteTrabalhoLocalizador( Conexao conexao, ComunicacaoAcidenteTrabalho comunicacaoAcidenteTrabalho ) throws QtSQLException {

		super.setConexao( conexao );

		this.comunicacaoAcidenteTrabalho = comunicacaoAcidenteTrabalho;
		busca();
	}

	/** Construtor que recebe um objeto ComunicacaoAcidenteTrabalho completo */
	public ComunicacaoAcidenteTrabalhoLocalizador( ComunicacaoAcidenteTrabalho comunicacaoAcidenteTrabalho ) throws QtSQLException {

		this.comunicacaoAcidenteTrabalho = comunicacaoAcidenteTrabalho;
		busca();
	}

	/** 
	 * Construtor que recebe uma conex�o e as chaves da tabela PCMSO.COMUNICACAO_ACIDENTE_TRABALHO
	 * @param cdBeneficiarioCartao Codigo de Beneficiario 
	 * @param sqPerfil sequencia perfil
	 * @param sqComunicacao sequencia Comunica��o
	 * @param conexao Conexao com o banco de dados
	 */
	public ComunicacaoAcidenteTrabalhoLocalizador( Conexao conexao, String cdBeneficiarioCartao, int sqPerfil, int sqComunicacao ) throws QtSQLException { 

		super.setConexao( conexao ); 
		comunicacaoAcidenteTrabalho = new ComunicacaoAcidenteTrabalho();
		this.comunicacaoAcidenteTrabalho.setCdBeneficiarioCartao( cdBeneficiarioCartao );
		this.comunicacaoAcidenteTrabalho.setSqPerfil( new Integer( sqPerfil ) );
		this.comunicacaoAcidenteTrabalho.setSqComunicacao( new Integer( sqComunicacao ) );
		
		busca();
	}

	/** 
	 * Construtor que recebe as chaves da tabela PCMSO.COMUNICACAO_ACIDENTE_TRABALHO
	 * @param cdBeneficiarioCartao Codigo de Beneficiario 
	 * @param sqPerfil sequencia perfil
	 * @param sqComunicacao sequencia Comunica��o
	 */
	public ComunicacaoAcidenteTrabalhoLocalizador( String cdBeneficiarioCartao, int sqPerfil, int sqComunicacao ) throws QtSQLException { 

		comunicacaoAcidenteTrabalho = new ComunicacaoAcidenteTrabalho();
		this.comunicacaoAcidenteTrabalho.setCdBeneficiarioCartao( cdBeneficiarioCartao );
		this.comunicacaoAcidenteTrabalho.setSqPerfil( new Integer( sqPerfil ) );
		this.comunicacaoAcidenteTrabalho.setSqComunicacao( new Integer( sqComunicacao ) );
		
		busca();
	}

	public static boolean existe( Conexao cnx, String cdBeneficiarioCartao, int sqPerfil, int sqComunicacao ) throws QtSQLException {

		Query q = new Query( cnx );

		try {
			q.setSQL( "select count(*) from PCMSO.COMUNICACAO_ACIDENTE_TRABALHO" );
			q.addSQL( " where CD_BENEFICIARIO_CARTAO = :prm1CdBeneficiarioCartao and SQ_PERFIL = :prm2SqPerfil and SQ_COMUNICACAO = :prm3SqComunicacao" );

			q.setParameter( "prm1CdBeneficiarioCartao", cdBeneficiarioCartao );
			q.setParameter( "prm2SqPerfil", sqPerfil );
			q.setParameter( "prm3SqComunicacao", sqComunicacao );
			q.executeQuery();

			return q.getInt( 1 ) > 0;
		} finally {
			q.liberaRecursos();
		}
	}

	public static boolean existe( String cdBeneficiarioCartao, int sqPerfil, int sqComunicacao ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return existe( cnx, cdBeneficiarioCartao, sqPerfil, sqComunicacao );
		} finally {
			cnx.libera();
		}
	}

	public static boolean existe( Conexao cnx, ComunicacaoAcidenteTrabalho comunicacaoAcidenteTrabalho ) throws QtSQLException {

		return existe( cnx, comunicacaoAcidenteTrabalho.getCdBeneficiarioCartao(), comunicacaoAcidenteTrabalho.getSqPerfil(), comunicacaoAcidenteTrabalho.getSqComunicacao() );
	}

	public static boolean existe( ComunicacaoAcidenteTrabalho comunicacaoAcidenteTrabalho ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return existe( cnx, comunicacaoAcidenteTrabalho.getCdBeneficiarioCartao(), comunicacaoAcidenteTrabalho.getSqPerfil(), comunicacaoAcidenteTrabalho.getSqComunicacao() );
		} finally {
			cnx.libera();
		}
	}

	public static ComunicacaoAcidenteTrabalho buscaComunicacaoAcidenteTrabalho( Conexao cnx, String cdBeneficiarioCartao, int sqPerfil, int sqComunicacao ) throws QtSQLException {

		Query q = new Query( cnx );

		try {
			q.setSQL( "select * from PCMSO.COMUNICACAO_ACIDENTE_TRABALHO" );
			q.addSQL( " where CD_BENEFICIARIO_CARTAO = :prm1CdBeneficiarioCartao and SQ_PERFIL = :prm2SqPerfil and SQ_COMUNICACAO = :prm3SqComunicacao" );

			q.setParameter( "prm1CdBeneficiarioCartao", cdBeneficiarioCartao );
			q.setParameter( "prm2SqPerfil", sqPerfil );
			q.setParameter( "prm3SqComunicacao", sqComunicacao );
			q.executeQuery();

			if( q.isEmpty() ) {
				return null;
			} else {
				ComunicacaoAcidenteTrabalho comunicacaoAcidenteTrabalho = new ComunicacaoAcidenteTrabalho();
				buscaCampos( comunicacaoAcidenteTrabalho, q );
				return comunicacaoAcidenteTrabalho;
			}
		} finally {
			q.liberaRecursos();
		}
	}

	public static ComunicacaoAcidenteTrabalho buscaComunicacaoAcidenteTrabalho( String cdBeneficiarioCartao, int sqPerfil, int sqComunicacao ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return buscaComunicacaoAcidenteTrabalho( cnx, cdBeneficiarioCartao, sqPerfil, sqComunicacao );
		} finally { 
			cnx.libera();
		}
	}

	/** 
	 * Construtor que recebe uma conex�o e as chaves da tabela PCMSO.COMUNICACAO_ACIDENTE_TRABALHO
	 * @param cdBeneficiarioCartao Codigo de Beneficiario 
	 * @param sqPerfil sequencia perfil
	 * @param sqComunicacao sequencia Comunica��o
	 * @param conexao Conexao com o banco de dados
	 */
	public ComunicacaoAcidenteTrabalhoLocalizador( Conexao conexao, String cdBeneficiarioCartao, Integer sqPerfil, Integer sqComunicacao ) throws QtSQLException {

		super.setConexao( conexao ); 
		comunicacaoAcidenteTrabalho = new ComunicacaoAcidenteTrabalho();
		this.comunicacaoAcidenteTrabalho.setCdBeneficiarioCartao( cdBeneficiarioCartao );
		this.comunicacaoAcidenteTrabalho.setSqPerfil( sqPerfil );
		this.comunicacaoAcidenteTrabalho.setSqComunicacao( sqComunicacao );
		
		busca();
	}

	/** 
	 * Construtor que recebe as chaves da tabela PCMSO.COMUNICACAO_ACIDENTE_TRABALHO
	 * @param cdBeneficiarioCartao Codigo de Beneficiario 
	 * @param sqPerfil sequencia perfil
	 * @param sqComunicacao sequencia Comunica��o
	 */
	public ComunicacaoAcidenteTrabalhoLocalizador( String cdBeneficiarioCartao, Integer sqPerfil, Integer sqComunicacao ) throws QtSQLException {

		comunicacaoAcidenteTrabalho = new ComunicacaoAcidenteTrabalho();
		this.comunicacaoAcidenteTrabalho.setCdBeneficiarioCartao( cdBeneficiarioCartao );
		this.comunicacaoAcidenteTrabalho.setSqPerfil( sqPerfil );
		this.comunicacaoAcidenteTrabalho.setSqComunicacao( sqComunicacao );
		
		busca();
	}

	public static void buscaCampos( ComunicacaoAcidenteTrabalho comunicacaoAcidenteTrabalho, Query query ) throws QtSQLException {

		comunicacaoAcidenteTrabalho.setCdBeneficiarioCartao( query.getString( "CD_BENEFICIARIO_CARTAO" ) );
		comunicacaoAcidenteTrabalho.setSqPerfil( query.getInteger( "SQ_PERFIL" ) );
		comunicacaoAcidenteTrabalho.setSqComunicacao( query.getInteger( "SQ_COMUNICACAO" ) );
		comunicacaoAcidenteTrabalho.setNrCat( query.getString( "NR_CAT" ) );
		comunicacaoAcidenteTrabalho.setDtRegistro( QtData.criaQtData( query.getTimestamp( "DT_REGISTRO" ) ) );
	}

	/** M�todo que retorna uma tabela b�sica a que se refere o localizador */
	public TabelaBasica getTabelaBasica() {

		return this.comunicacaoAcidenteTrabalho;
	}

	/** M�todo que retorna a classe ComunicacaoAcidenteTrabalho relacionada ao localizador */
	public ComunicacaoAcidenteTrabalho getComunicacaoAcidenteTrabalho() {

		return this.comunicacaoAcidenteTrabalho;
	}

	/**
	 * M�todo para buscar e preencher uma tupla de uma tabela
	 * @param tabelaBasica Recebe uma tabela basica com os atributos chave preenchidos
	 */
	public boolean busca( TabelaBasica tabelaBasica ) throws QtSQLException {

		if( !( tabelaBasica instanceof ComunicacaoAcidenteTrabalho ) )
			throw new QtSQLException( "Esperava um objeto ComunicacaoAcidenteTrabalho!" );

		this.comunicacaoAcidenteTrabalho = (ComunicacaoAcidenteTrabalho) tabelaBasica;
		return busca();

	}

	/**
	 * M�todo privado para buscar e preencher uma tupla de uma tabela
	 * Este m�todo busca os dados da inst�ncia de ComunicacaoAcidenteTrabalho atual
	 */
	private boolean busca() throws QtSQLException {

		boolean ret = false;

		super.estabeleceConexao();
		Query query = super.criaNovaQuery();

		try {

			query.setSQL( "select * from PCMSO.COMUNICACAO_ACIDENTE_TRABALHO" );
			query.addSQL( " where CD_BENEFICIARIO_CARTAO like :prm1CdBeneficiarioCartao and SQ_PERFIL = :prm2SqPerfil and SQ_COMUNICACAO = :prm3SqComunicacao" );

			query.setParameter( "prm1CdBeneficiarioCartao", comunicacaoAcidenteTrabalho.getCdBeneficiarioCartao() );
			query.setParameter( "prm2SqPerfil", comunicacaoAcidenteTrabalho.getSqPerfil() );
			query.setParameter( "prm3SqComunicacao", comunicacaoAcidenteTrabalho.getSqComunicacao() );
			query.executeQuery();

			if( query.isEmpty() ) {
				super.setEmpty( true );
				comunicacaoAcidenteTrabalho = null;
			} else {
				super.setEmpty( false );
				
				comunicacaoAcidenteTrabalho.setCdBeneficiarioCartao( query.getString( "CD_BENEFICIARIO_CARTAO" ) );
				comunicacaoAcidenteTrabalho.setSqPerfil( query.getInteger( "SQ_PERFIL" ) );
				comunicacaoAcidenteTrabalho.setSqComunicacao( query.getInteger( "SQ_COMUNICACAO" ) );
				comunicacaoAcidenteTrabalho.setNrCat( query.getString( "NR_CAT" ) );
				comunicacaoAcidenteTrabalho.setDtRegistro( QtData.criaQtData( query.getTimestamp( "DT_REGISTRO" ) ) );
				ret = true;
			}
		} finally {
			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}
 }