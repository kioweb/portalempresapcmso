package jPcmso.persistencia.geral.c;

import quatro.persistencia.FiltroDeNavegador;

/**
 *
 * Classe filtro para PCMSO.CARGOS
 *
 * @author Samuel Antonio Klein
 * @see FiltroDeTabela
 * @see Cargo
 * @see CargoNavegador
 * @see CargoPersistor
 * @see CargoLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 public class CargoFiltro extends FiltroDeNavegador {

	/** Determina a ordem de navega��o que ser� passada para o navegador */
	public void setOrdemDeNavegacao( int idOrdem ) {

		super.setOrdemDeNavegacao( idOrdem );
	}
}