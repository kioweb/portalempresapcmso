package jPcmso.persistencia.geral.c;

import java.sql.PreparedStatement;

import quatro.persistencia.Persistor;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.util.QtException;

/**
 *
 * Classe persistor para CODIGOS_CONSELHO_PROFISSIONAL
 *
 * @author Samuel Antonio Klein
 * @see Persistor
 * @see CodigoConselhoProfissional
 * @see CodigoConselhoProfissionalNavegador
 * @see CodigoConselhoProfissionalLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 public class CodigoConselhoProfissionalPersistor extends Persistor {

	/** Inst�ncia de CodigoConselhoProfissional para manipula��o pelo persistor */
	private CodigoConselhoProfissional codigoConselhoProfissional;

	/** Construtor gen�rico */
	public CodigoConselhoProfissionalPersistor() {

		this.codigoConselhoProfissional = null;
	}

	/** Construtor gen�rico */
	public CodigoConselhoProfissionalPersistor( Conexao cnx ) throws QtSQLException {

		this.codigoConselhoProfissional = null;
		super.setConexao( cnx );
	}

	/** Construtor que recebe uma inst�ncia de CodigoConselhoProfissional */
	public CodigoConselhoProfissionalPersistor( CodigoConselhoProfissional codigoConselhoProfissional ) throws QtSQLException {

		if( codigoConselhoProfissional == null ) 
			throw new QtSQLException( "Imposs�vel instanciar CodigoConselhoProfissionalPersistor porque est� apontando para um nulo!" );

		this.codigoConselhoProfissional = codigoConselhoProfissional;
	}

	/** Construtor que recebe uma conex�o e uma inst�ncia de CodigoConselhoProfissional */
	public CodigoConselhoProfissionalPersistor( Conexao conexao, CodigoConselhoProfissional codigoConselhoProfissional ) throws QtSQLException {

		if( codigoConselhoProfissional == null ) 
			throw new QtSQLException( "Imposs�vel instanciar CodigoConselhoProfissionalPersistor porque est� apontando para um nulo!" );

		setConexao( conexao );
		this.codigoConselhoProfissional = codigoConselhoProfissional;
	}

	/**
	 * Construtor que recebe a chave da tabela CODIGOS_CONSELHO_PROFISSIONAL
	 * @param cdSigla Sigla do Conselho Profissional
	 */
	public CodigoConselhoProfissionalPersistor( String cdSigla )
	  throws QtSQLException { 

		busca( cdSigla );
	}

	/**
	 * Construtor que recebe uma conex�o e a chave da tabela CODIGOS_CONSELHO_PROFISSIONAL
	 * @param cdSigla Sigla do Conselho Profissional
	 * @param conexao Conexao com o banco de dados
	 */
	public CodigoConselhoProfissionalPersistor( Conexao conexao, String cdSigla )
	  throws QtSQLException { 

		super.setConexao( conexao );
		busca( cdSigla );
	}

	/**
	 * M�todo interno para buscar e preencher uma tupla de uma tabela
	 * Utiliza a Inst�ncia de CodigoConselhoProfissional para fazer a busca
	 * @param cdSigla Sigla do Conselho Profissional
	 */
	private void busca( String cdSigla ) throws QtSQLException { 

		CodigoConselhoProfissionalLocalizador codigoConselhoProfissionalLocalizador = new CodigoConselhoProfissionalLocalizador( getConexao(), cdSigla );

		if( codigoConselhoProfissionalLocalizador.isEmpty() ) {
			throw new QtSQLException( "Chaves da Tabela CodigoConselhoProfissional n�o encontradas - cdSigla: " + cdSigla );
		}

		codigoConselhoProfissional = (CodigoConselhoProfissional ) codigoConselhoProfissionalLocalizador.getTabelaBasica();
	}

	/**
	 * Permite alimentar o Persistor com um novo registro
	 */
	public void setTabelaBasica( TabelaBasica codigoConselhoProfissional ) throws QtException {

		if( codigoConselhoProfissional instanceof CodigoConselhoProfissional ) {
			this.codigoConselhoProfissional = (CodigoConselhoProfissional) codigoConselhoProfissional;
		} else {
			throw new QtException( "Tabela B�sica fornecida tem que ser uma inst�ncia de CodigoConselhoProfissional" );
		}
	}

	/**
	 * Permite acessar o registro corrente do persistor
	 */
	public TabelaBasica getTabelaBasica() throws QtException {

		return codigoConselhoProfissional;
	}

	/**
	 * M�todo para inserir tupla na tabela
	 */
	public void insere() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "insert into CODIGOS_CONSELHO_PROFISSIONAL" );
			cmd.append( "( CD_SIGLA, DS_CONSELHO )" );
			cmd.append( "values " );
			cmd.append( "( ?, ?)" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			ps.setObject( 1, codigoConselhoProfissional.getCdSigla() );
			ps.setObject( 2, codigoConselhoProfissional.getDsConselho() );
			
			ps.execute();

		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}

	/**
	 * M�todo para alterar tupla na tabela
	 */
	public void altera() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "update CODIGOS_CONSELHO_PROFISSIONAL" );
			cmd.append( "  set CD_SIGLA = ?, " );
			cmd.append( " DS_CONSELHO = ?" );
			cmd.append( " where CD_SIGLA like ?" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			ps.setObject( 1, codigoConselhoProfissional.getCdSigla() );
			ps.setObject( 2, codigoConselhoProfissional.getDsConselho() );
			
			ps.setObject( 3, codigoConselhoProfissional.getCdSigla() );
			
			ps.execute();

		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}

	/**
	 * M�todo para excluir tupla de uma tabela
	 */
	public void exclui() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "delete from CODIGOS_CONSELHO_PROFISSIONAL" );
			cmd.append( " where CD_SIGLA like ?" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			ps.setObject( 1, codigoConselhoProfissional.getCdSigla() );
			
			ps.execute();

		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}
}