package jPcmso.persistencia.geral.c;

import quatro.persistencia.TabelaBasica;
import quatro.smartDB.IgnoredField;
import quatro.smartDB.PrimaryKey;
import quatro.smartDB.Table;

/**
 *
 * Classe básica para PCMSO.CARGOS
 *
 * Tabela de Cargo
 * 
 * @author ronei@unimed-uau.com.br
 * @see CargoNavegador
 * @see CargoPersistor
 * @see CargoLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 14/12/2017 15:26 - importado por ronei@unimed-uau.com.br
 *
 */
 @Table(name="PCMSO.CARGOS")
 public class Cargo implements TabelaBasica {

	@IgnoredField
	private static final long serialVersionUID = -1409353737805173743L;

	/** Codigo do cargo (chave) */
	@PrimaryKey
	private Integer idCargo;
	/** Descricao do cargo (varchar(250)) */
	private String dsCargo;

	/**
	 * Alimenta Codigo do cargo
	 * @param idCargo Codigo do cargo
	 */ 
	public void setIdCargo( Integer idCargo ) {
		this.idCargo = idCargo;
	}

	/**
	 * Retorna Codigo do cargo
	 * @return Codigo do cargo
	 */
	public Integer getIdCargo() {
		return this.idCargo;
	}

	/**
	 * Alimenta Descricao do cargo
	 * @param dsCargo Descricao do cargo
	 */ 
	public void setDsCargo( String dsCargo ) {
		this.dsCargo = dsCargo;
	}

	/**
	 * Retorna Descricao do cargo
	 * @return Descricao do cargo
	 */
	public String getDsCargo() {
		return this.dsCargo;
	}

}