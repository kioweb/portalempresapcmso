package jPcmso.persistencia.geral.c;

import java.sql.PreparedStatement;

import quatro.persistencia.Persistor;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.util.QtException;
import quatro.util.QtData;

/**
 *
 * Classe persistor para PCMSO.COMUNICACAO_ACIDENTE_TRABALHO
 *
 * @author Samuel Antonio Klein
 * @see Persistor
 * @see ComunicacaoAcidenteTrabalho
 * @see ComunicacaoAcidenteTrabalhoNavegador
 * @see ComunicacaoAcidenteTrabalhoLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:31 - importado por Samuel Antonio Klein
 *
 */
 public class ComunicacaoAcidenteTrabalhoPersistor extends Persistor {

	/** Inst�ncia de ComunicacaoAcidenteTrabalho para manipula��o pelo persistor */
	private ComunicacaoAcidenteTrabalho comunicacaoAcidenteTrabalho;

	/** Construtor gen�rico */
	public ComunicacaoAcidenteTrabalhoPersistor() {

		this.comunicacaoAcidenteTrabalho = null;
	}

	/** Construtor gen�rico */
	public ComunicacaoAcidenteTrabalhoPersistor( Conexao cnx ) throws QtSQLException {

		this.comunicacaoAcidenteTrabalho = null;
		super.setConexao( cnx );
	}

	/** Construtor que recebe uma inst�ncia de ComunicacaoAcidenteTrabalho */
	public ComunicacaoAcidenteTrabalhoPersistor( ComunicacaoAcidenteTrabalho comunicacaoAcidenteTrabalho ) throws QtSQLException {

		if( comunicacaoAcidenteTrabalho == null ) 
			throw new QtSQLException( "Imposs�vel instanciar ComunicacaoAcidenteTrabalhoPersistor porque est� apontando para um nulo!" );

		this.comunicacaoAcidenteTrabalho = comunicacaoAcidenteTrabalho;
	}

	/** Construtor que recebe uma conex�o e uma inst�ncia de ComunicacaoAcidenteTrabalho */
	public ComunicacaoAcidenteTrabalhoPersistor( Conexao conexao, ComunicacaoAcidenteTrabalho comunicacaoAcidenteTrabalho ) throws QtSQLException {

		if( comunicacaoAcidenteTrabalho == null ) 
			throw new QtSQLException( "Imposs�vel instanciar ComunicacaoAcidenteTrabalhoPersistor porque est� apontando para um nulo!" );

		setConexao( conexao );
		this.comunicacaoAcidenteTrabalho = comunicacaoAcidenteTrabalho;
	}

	/**
	 * Construtor que recebe as chaves da tabela PCMSO.COMUNICACAO_ACIDENTE_TRABALHO
	 * @param cdBeneficiarioCartao Codigo de Beneficiario 
	 * @param sqPerfil sequencia perfil
	 * @param sqComunicacao sequencia Comunica��o
	 */
	public ComunicacaoAcidenteTrabalhoPersistor( String cdBeneficiarioCartao, int sqPerfil, int sqComunicacao )
	  throws QtSQLException { 

		busca( cdBeneficiarioCartao, sqPerfil, sqComunicacao );
	}

	/**
	 * Construtor que recebe uma conex�o e as chaves da tabela PCMSO.COMUNICACAO_ACIDENTE_TRABALHO
	 * @param cdBeneficiarioCartao Codigo de Beneficiario 
	 * @param sqPerfil sequencia perfil
	 * @param sqComunicacao sequencia Comunica��o
	 * @param conexao Conexao com o banco de dados
	 */
	public ComunicacaoAcidenteTrabalhoPersistor( Conexao conexao, String cdBeneficiarioCartao, int sqPerfil, int sqComunicacao )
	  throws QtSQLException { 

		super.setConexao( conexao );
		busca( cdBeneficiarioCartao, sqPerfil, sqComunicacao );
	}

	/**
	 * M�todo interno para buscar e preencher uma tupla de uma tabela
	 * Utiliza a Inst�ncia de ComunicacaoAcidenteTrabalho para fazer a busca
	 * @param cdBeneficiarioCartao Codigo de Beneficiario 
	 * @param sqPerfil sequencia perfil
	 * @param sqComunicacao sequencia Comunica��o
	 */
	private void busca( String cdBeneficiarioCartao, int sqPerfil, int sqComunicacao ) throws QtSQLException { 

		ComunicacaoAcidenteTrabalhoLocalizador comunicacaoAcidenteTrabalhoLocalizador = new ComunicacaoAcidenteTrabalhoLocalizador( getConexao(), cdBeneficiarioCartao, sqPerfil, sqComunicacao );

		if( comunicacaoAcidenteTrabalhoLocalizador.isEmpty() ) {
			throw new QtSQLException( "Chaves da Tabela ComunicacaoAcidenteTrabalho n�o encontradas - cdBeneficiarioCartao: " + cdBeneficiarioCartao + " / sqPerfil: " + sqPerfil + " / sqComunicacao: " + sqComunicacao );
		}

		comunicacaoAcidenteTrabalho = (ComunicacaoAcidenteTrabalho ) comunicacaoAcidenteTrabalhoLocalizador.getTabelaBasica();
	}

	/**
	 * Permite alimentar o Persistor com um novo registro
	 */
	public void setTabelaBasica( TabelaBasica comunicacaoAcidenteTrabalho ) throws QtException {

		if( comunicacaoAcidenteTrabalho instanceof ComunicacaoAcidenteTrabalho ) {
			this.comunicacaoAcidenteTrabalho = (ComunicacaoAcidenteTrabalho) comunicacaoAcidenteTrabalho;
		} else {
			throw new QtException( "Tabela B�sica fornecida tem que ser uma inst�ncia de ComunicacaoAcidenteTrabalho" );
		}
	}

	/**
	 * Permite acessar o registro corrente do persistor
	 */
	public TabelaBasica getTabelaBasica() throws QtException {

		return comunicacaoAcidenteTrabalho;
	}

	/**
	 * M�todo para inserir tupla na tabela
	 */
	public void insere() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "insert into PCMSO.COMUNICACAO_ACIDENTE_TRABALHO" );
			cmd.append( "( CD_BENEFICIARIO_CARTAO, SQ_PERFIL, SQ_COMUNICACAO, NR_CAT, DT_REGISTRO )" );
			cmd.append( "values " );
			cmd.append( "( ?, ?, ?, ?, ?)" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			ps.setObject( 1, comunicacaoAcidenteTrabalho.getCdBeneficiarioCartao() );
			ps.setObject( 2, comunicacaoAcidenteTrabalho.getSqPerfil() );
			ps.setObject( 3, comunicacaoAcidenteTrabalho.getSqComunicacao() );
			ps.setObject( 4, comunicacaoAcidenteTrabalho.getNrCat() );
			ps.setObject( 5, QtData.getQtDataAsTimestamp( comunicacaoAcidenteTrabalho.getDtRegistro() ) );
			
			ps.execute();

		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}

	/**
	 * M�todo para alterar tupla na tabela
	 */
	public void altera() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "update PCMSO.COMUNICACAO_ACIDENTE_TRABALHO" );
			cmd.append( "  set CD_BENEFICIARIO_CARTAO = ?, " );
			cmd.append( " SQ_PERFIL = ?, " );
			cmd.append( " SQ_COMUNICACAO = ?, " );
			cmd.append( " NR_CAT = ?, " );
			cmd.append( " DT_REGISTRO = ?" );
			cmd.append( " where CD_BENEFICIARIO_CARTAO like ? and SQ_PERFIL = ? and SQ_COMUNICACAO = ?" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			ps.setObject( 1, comunicacaoAcidenteTrabalho.getCdBeneficiarioCartao() );
			ps.setObject( 2, comunicacaoAcidenteTrabalho.getSqPerfil() );
			ps.setObject( 3, comunicacaoAcidenteTrabalho.getSqComunicacao() );
			ps.setObject( 4, comunicacaoAcidenteTrabalho.getNrCat() );
			ps.setObject( 5, QtData.getQtDataAsTimestamp( comunicacaoAcidenteTrabalho.getDtRegistro() ) );
			
			ps.setObject( 6, comunicacaoAcidenteTrabalho.getCdBeneficiarioCartao() );
			ps.setObject( 7, comunicacaoAcidenteTrabalho.getSqPerfil() );
			ps.setObject( 8, comunicacaoAcidenteTrabalho.getSqComunicacao() );
			
			ps.execute();

		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}

	/**
	 * M�todo para excluir tupla de uma tabela
	 */
	public void exclui() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "delete from PCMSO.COMUNICACAO_ACIDENTE_TRABALHO" );
			cmd.append( " where CD_BENEFICIARIO_CARTAO like ? and SQ_PERFIL = ? and SQ_COMUNICACAO = ?" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			ps.setObject( 1, comunicacaoAcidenteTrabalho.getCdBeneficiarioCartao() );
			ps.setObject( 2, comunicacaoAcidenteTrabalho.getSqPerfil() );
			ps.setObject( 3, comunicacaoAcidenteTrabalho.getSqComunicacao() );
			
			ps.execute();

		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}
}