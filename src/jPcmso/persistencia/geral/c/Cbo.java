package jPcmso.persistencia.geral.c;

import quatro.persistencia.TabelaBasica;
import quatro.smartDB.IgnoredField;
import quatro.smartDB.PrimaryKey;
import quatro.smartDB.Table;

/**
 *
 * Classe b�sica para PCMSO.CBOS
 *
 * Tabela de CBO
 * 
 * @author ronei@unimed-uau.com.br
 * @see CboNavegador
 * @see CboPersistor
 * @see CboLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 24/01/2018 11:18 - importado por ronei@unimed-uau.com.br
 *
 */
 @Table(name="PCMSO.CBOS")
 public class Cbo implements TabelaBasica {

	@IgnoredField
	private static final long serialVersionUID = -6711280731608505339L;

	/** Codigo de CBO (chave-char(6)) */
	@PrimaryKey
	private String cdCbo;
	/** Nome de CBO (varchar(250)) */
	private String nmCbo;
	/** Descri��o de CBO */
	private String dsAtividade;

	/**
	 * Alimenta Codigo de CBO
	 * @param cdCbo Codigo de CBO
	 */ 
	public void setCdCbo( String cdCbo ) {
		this.cdCbo = cdCbo;
	}

	/**
	 * Retorna Codigo de CBO
	 * @return Codigo de CBO
	 */
	public String getCdCbo() {
		return this.cdCbo;
	}

	/**
	 * Alimenta Nome de CBO
	 * @param nmCbo Nome de CBO
	 */ 
	public void setNmCbo( String nmCbo ) {
		this.nmCbo = nmCbo;
	}

	/**
	 * Retorna Nome de CBO
	 * @return Nome de CBO
	 */
	public String getNmCbo() {
		return this.nmCbo;
	}

	/**
	 * Alimenta Descri��o de CBO
	 * @param dsAtividade Descri��o de CBO
	 */ 
	public void setDsAtividade( String dsAtividade ) {
		this.dsAtividade = dsAtividade;
	}

	/**
	 * Retorna Descri��o de CBO
	 * @return Descri��o de CBO
	 */
	public String getDsAtividade() {
		return this.dsAtividade;
	}

}