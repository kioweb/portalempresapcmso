package jPcmso.persistencia.geral.c;

import quatro.persistencia.Caminhador;
import quatro.persistencia.FiltroDeNavegador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;

 /**
 *
 * Classe Caminhador para Cidade
 *
 * @author Samuel Antonio Klein
 * @see Caminhador
 * @see FiltroDeNavegador
 * @see CidadeNavegador
 * @see CidadePersistor
 * @see CidadeLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 public class CidadeCaminhador extends Caminhador {

	public static final int POR_ORDEM_DEFAULT = -1;
	public static final int POR_CODIGO = 0;
	public static final int POR_NOME = 1;
	
	private Cidade cidade;

	public CidadeCaminhador( FiltroDeNavegador filtro ) {

		setFiltro( filtro );
		inicializaVisao();
	}

	public CidadeCaminhador( Conexao conexao, FiltroDeNavegador filtro ) throws QtSQLException {

		super.setConexao( conexao );
		setFiltro( filtro );
		inicializaVisao();
	}

	private void inicializaVisao() {

		setNmVisao( "CIDADES" );
	}

	protected void carregaCampos() throws QtSQLException {

		cidade = new Cidade();

		if( isRecordAvailable() ) {
			cidade.setCdEstado( query.getString( "CD_ESTADO" ) );
			cidade.setIdCidade( query.getInteger( "ID_CIDADE" ) );
			cidade.setDsCidade( query.getString( "DS_CIDADE" ) );
			cidade.setCdCep( query.getString( "CD_CEP" ) );
			cidade.setDsPracaBancaria( query.getString( "DS_PRACA_BANCARIA" ) );
			cidade.setCdIbge( query.getString( "CD_IBGE" ) );
			cidade.setCdDigitoIbge( query.getString( "CD_DIGITO_IBGE" ) );
		}
	}

	/** M�todo que retorna uma tabela b�sica a que se refere o caminhador */
	public TabelaBasica getTabelaBasica() {

		return this.cidade;
	}

	/** M�todo que retorna a classe Cidade relacionada ao caminhador */
	public Cidade getCidade() {

		return this.cidade;
	}

	public void ativaVisao() throws QtSQLException {

		if(  super.getOrdemDeNavegacao() == CidadeCaminhador.POR_ORDEM_DEFAULT ) {
			super.ativaCaminhador( "order by CD_ESTADO, ID_CIDADE" );
		} else if(  super.getOrdemDeNavegacao() == CidadeCaminhador.POR_CODIGO ) {
			super.ativaCaminhador( "order by CD_ESTADO, ID_CIDADE" );
		} else if(  super.getOrdemDeNavegacao() == CidadeCaminhador.POR_NOME ) {
			super.ativaCaminhador( "order by DS_CIDADE, CD_ESTADO, ID_CIDADE" );
	
		} else {
			throw new QtSQLException( "CidadeCaminhador - Ordem de Navega��o n�o definida!" );
		}
	}

	public String getCamposIndice( int qualIndice ) {
		return getDsIndice( qualIndice );
	}

	public String getCamposChave() {
		return getDsChave();
	}

	/**
	 * M�todo est�tico que devolve os campos que comp�e a chave da tabela;
	 * @return campos que comp�e a chave.
	 */
	public static String getDsChave() {
		return "CD_ESTADO, ID_CIDADE";
	}

	/**
	 * M�todo est�tico que devolve os campos que fazem parte de uma dada ordem
	 * de navega��o.
	 * @param qualIndice indica a ordem de navega��o desejada.
	 * @return campos que comp�e a ordem de navega��o.
	 */
	public static String getDsIndice( int qualIndice ) {

		switch( qualIndice ) {

			case POR_ORDEM_DEFAULT: return "CD_ESTADO, ID_CIDADE";
			case POR_CODIGO: return "CD_ESTADO, ID_CIDADE";
			case POR_NOME: return "DS_CIDADE, CD_ESTADO, ID_CIDADE";
		}
		return null;
	}
}