package jPcmso.persistencia.geral.c;

import quatro.persistencia.Localizador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.QtSQLException;
import quatro.sql.Query;
import quatro.sql.Conexao;
import quatro.sql.PoolDeConexoes;
import quatro.sql.ConexaoParaPoolDeConexoes;

/**
 *
 * Classe localizador para CODIGOS_CONSELHO_PROFISSIONAL
 *
 * @author Samuel Antonio Klein
 * @see Localizador
 * @see CodigoConselhoProfissional
 * @see CodigoConselhoProfissionalNavegador
 * @see CodigoConselhoProfissionalPersistor
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 public class CodigoConselhoProfissionalLocalizador extends Localizador {

	/** Inst�ncia de CodigoConselhoProfissional para manipula��o pelo localizador */
	private CodigoConselhoProfissional codigoConselhoProfissional;

	/** Construtor que recebe uma conex�o e um objeto CodigoConselhoProfissional completo */
	public CodigoConselhoProfissionalLocalizador( Conexao conexao, CodigoConselhoProfissional codigoConselhoProfissional ) throws QtSQLException {

		super.setConexao( conexao );

		this.codigoConselhoProfissional = codigoConselhoProfissional;
		busca();
	}

	/** Construtor que recebe um objeto CodigoConselhoProfissional completo */
	public CodigoConselhoProfissionalLocalizador( CodigoConselhoProfissional codigoConselhoProfissional ) throws QtSQLException {

		this.codigoConselhoProfissional = codigoConselhoProfissional;
		busca();
	}

	/** 
	 * Construtor que recebe uma conex�o e a chave da tabela CODIGOS_CONSELHO_PROFISSIONAL
	 * @param cdSigla Sigla do Conselho Profissional
	 * @param conexao Conexao com o banco de dados
	 */
	public CodigoConselhoProfissionalLocalizador( Conexao conexao, String cdSigla ) throws QtSQLException { 

		super.setConexao( conexao ); 
		codigoConselhoProfissional = new CodigoConselhoProfissional();
		this.codigoConselhoProfissional.setCdSigla( cdSigla );
		
		busca();
	}

	/** 
	 * Construtor que recebe a chave da tabela CODIGOS_CONSELHO_PROFISSIONAL
	 * @param cdSigla Sigla do Conselho Profissional
	 */
	public CodigoConselhoProfissionalLocalizador( String cdSigla ) throws QtSQLException { 

		codigoConselhoProfissional = new CodigoConselhoProfissional();
		this.codigoConselhoProfissional.setCdSigla( cdSigla );
		
		busca();
	}

	public static boolean existe( Conexao cnx, String cdSigla ) throws QtSQLException {

		Query q = new Query( cnx );

		try {
			q.setSQL( "select count(*) from CODIGOS_CONSELHO_PROFISSIONAL" );
			q.addSQL( " where CD_SIGLA = :prm1CdSigla" );

			q.setParameter( "prm1CdSigla", cdSigla );
			q.executeQuery();

			return q.getInt( 1 ) > 0;
		} finally {
			q.liberaRecursos();
		}
	}

	public static boolean existe( String cdSigla ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return existe( cnx, cdSigla );
		} finally {
			cnx.libera();
		}
	}

	public static boolean existe( Conexao cnx, CodigoConselhoProfissional codigoConselhoProfissional ) throws QtSQLException {

		return existe( cnx, codigoConselhoProfissional.getCdSigla() );
	}

	public static boolean existe( CodigoConselhoProfissional codigoConselhoProfissional ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return existe( cnx, codigoConselhoProfissional.getCdSigla() );
		} finally {
			cnx.libera();
		}
	}

	public static CodigoConselhoProfissional buscaCodigoConselhoProfissional( Conexao cnx, String cdSigla ) throws QtSQLException {

		Query q = new Query( cnx );

		try {
			q.setSQL( "select * from CODIGOS_CONSELHO_PROFISSIONAL" );
			q.addSQL( " where CD_SIGLA = :prm1CdSigla" );

			q.setParameter( "prm1CdSigla", cdSigla );
			q.executeQuery();

			if( q.isEmpty() ) {
				return null;
			} else {
				CodigoConselhoProfissional codigoConselhoProfissional = new CodigoConselhoProfissional();
				buscaCampos( codigoConselhoProfissional, q );
				return codigoConselhoProfissional;
			}
		} finally {
			q.liberaRecursos();
		}
	}

	public static CodigoConselhoProfissional buscaCodigoConselhoProfissional( String cdSigla ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return buscaCodigoConselhoProfissional( cnx, cdSigla );
		} finally { 
			cnx.libera();
		}
	}

	public static void buscaCampos( CodigoConselhoProfissional codigoConselhoProfissional, Query query ) throws QtSQLException {

		codigoConselhoProfissional.setCdSigla( query.getString( "CD_SIGLA" ) );
		codigoConselhoProfissional.setDsConselho( query.getString( "DS_CONSELHO" ) );
	}

	/** M�todo que retorna uma tabela b�sica a que se refere o localizador */
	public TabelaBasica getTabelaBasica() {

		return this.codigoConselhoProfissional;
	}

	/** M�todo que retorna a classe CodigoConselhoProfissional relacionada ao localizador */
	public CodigoConselhoProfissional getCodigoConselhoProfissional() {

		return this.codigoConselhoProfissional;
	}

	/**
	 * M�todo para buscar e preencher uma tupla de uma tabela
	 * @param tabelaBasica Recebe uma tabela basica com os atributos chave preenchidos
	 */
	public boolean busca( TabelaBasica tabelaBasica ) throws QtSQLException {

		if( !( tabelaBasica instanceof CodigoConselhoProfissional ) )
			throw new QtSQLException( "Esperava um objeto CodigoConselhoProfissional!" );

		this.codigoConselhoProfissional = (CodigoConselhoProfissional) tabelaBasica;
		return busca();

	}

	/**
	 * M�todo privado para buscar e preencher uma tupla de uma tabela
	 * Este m�todo busca os dados da inst�ncia de CodigoConselhoProfissional atual
	 */
	private boolean busca() throws QtSQLException {

		boolean ret = false;

		super.estabeleceConexao();
		Query query = super.criaNovaQuery();

		try {

			query.setSQL( "select * from CODIGOS_CONSELHO_PROFISSIONAL" );
			query.addSQL( " where CD_SIGLA like :prm1CdSigla" );

			query.setParameter( "prm1CdSigla", codigoConselhoProfissional.getCdSigla() );
			query.executeQuery();

			if( query.isEmpty() ) {
				super.setEmpty( true );
				codigoConselhoProfissional = null;
			} else {
				super.setEmpty( false );
				
				codigoConselhoProfissional.setCdSigla( query.getString( "CD_SIGLA" ) );
				codigoConselhoProfissional.setDsConselho( query.getString( "DS_CONSELHO" ) );
				ret = true;
			}
		} finally {
			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}
 }