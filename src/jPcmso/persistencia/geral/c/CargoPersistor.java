package jPcmso.persistencia.geral.c;

import java.sql.PreparedStatement;

import quatro.persistencia.Persistor;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.ConexaoParaPoolDeConexoes;
import quatro.sql.PoolDeConexoes;
import quatro.sql.QtSQLException;
import quatro.sql.Query;
import quatro.util.QtException;

/**
 *
 * Classe persistor para PCMSO.CARGOS
 *
 * @author ronei@unimed-uau.com.br
 * @see Persistor
 * @see Cargo
 * @see CargoNavegador
 * @see CargoLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 14/12/2017 15:26 - importado por ronei@unimed-uau.com.br
 *
 */
 public class CargoPersistor extends Persistor {

	/** Instância de Cargo para manipulação pelo persistor */
	private Cargo cargo;

	/** Construtor genérico */
	public CargoPersistor() {

		this.cargo = null;
	}

	/** Construtor genérico */
	public CargoPersistor( Conexao cnx ) throws QtSQLException {

		this.cargo = null;
		super.setConexao( cnx );
	}

	/** Construtor que recebe uma instância de Cargo */
	public CargoPersistor( Cargo cargo ) throws QtSQLException {

		if( cargo == null ) 
			throw new QtSQLException( "Impossível instanciar CargoPersistor porque está apontando para um nulo!" );

		this.cargo = cargo;
	}

	/** Construtor que recebe uma conexão e uma instância de Cargo */
	public CargoPersistor( Conexao conexao, Cargo cargo ) throws QtSQLException {

		if( cargo == null ) 
			throw new QtSQLException( "Impossível instanciar CargoPersistor porque está apontando para um nulo!" );

		setConexao( conexao );
		this.cargo = cargo;
	}

	/**
	 * Construtor que recebe a chave da tabela PCMSO.CARGOS
	 * @param idCargo Codigo do cargo
	 */
	public CargoPersistor( int idCargo )
	  throws QtSQLException { 

		busca( idCargo );
	}

	/**
	 * Construtor que recebe uma conexão e a chave da tabela PCMSO.CARGOS
	 * @param idCargo Codigo do cargo
	 * @param conexao Conexao com o banco de dados
	 */
	public CargoPersistor( Conexao conexao, int idCargo )
	  throws QtSQLException { 

		super.setConexao( conexao );
		busca( idCargo );
	}

	/**
	 * Método interno para buscar e preencher uma tupla de uma tabela
	 * Utiliza a Instância de Cargo para fazer a busca
	 * @param idCargo Codigo do cargo
	 */
	private void busca( int idCargo ) throws QtSQLException { 

		CargoLocalizador cargoLocalizador = new CargoLocalizador( getConexao(), idCargo );

		if( cargoLocalizador.isEmpty() ) {
			throw new QtSQLException( "Chaves da Tabela Cargo não encontradas - idCargo: " + idCargo );
		}

		cargo = (Cargo ) cargoLocalizador.getTabelaBasica();
	}

	/**
	 * Permite alimentar o Persistor com um novo registro
	 */
	public void setTabelaBasica( TabelaBasica cargo ) throws QtException {

		if( cargo instanceof Cargo ) {
			this.cargo = (Cargo) cargo;
		} else {
			throw new QtException( "Tabela Básica fornecida tem que ser uma instância de Cargo" );
		}
	}

	/**
	 * Permite acessar o registro corrente do persistor
	 */
	public TabelaBasica getTabelaBasica() throws QtException {

		return cargo;
	}

	/**
	 * Método para gerar automaticamente uma nova chave
	 */
	public static Integer geraNovoIdCargo( Conexao cnx ) throws QtSQLException {

		Integer result = null;

		Query query = new Query( cnx );

		try{

			query.setSQL( "select max( ID_CARGO ) from PCMSO.CARGOS" );

			query.executeQuery();
			result = new Integer( query.getInt( 1 ) + 1 );
		} catch( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			query.liberaRecursos();
		}
		return result;
	}

	/**
	 * Método para gerar automaticamente uma nova chave
	 */
	public static Integer geraNovoIdCargo(  ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return geraNovoIdCargo(  cnx  );
		} finally {
			cnx.libera();
		}
	}


	/**
	 * Método para inserir tupla na tabela
	 */
	public void insere() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "insert into PCMSO.CARGOS" );
			cmd.append( "( ID_CARGO, DS_CARGO )" );
			cmd.append( "values " );
			cmd.append( "( ?, ?)" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			try {

				ps.setObject( 1, cargo.getIdCargo() );
				ps.setObject( 2, cargo.getDsCargo() );
				
				ps.execute();
			} finally { 
				ps.close();
			}
		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}

	/**
	 * Método para alterar tupla na tabela
	 */
	public void altera() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "update PCMSO.CARGOS" );
			cmd.append( "  set ID_CARGO = ?, " );
			cmd.append( " DS_CARGO = ?" );
			cmd.append( " where ID_CARGO = ?" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			try {

				ps.setObject( 1, cargo.getIdCargo() );
				ps.setObject( 2, cargo.getDsCargo() );
				ps.setObject( 3, cargo.getIdCargo() );
				
				ps.execute();
			} finally {
				ps.close();
			}
		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}

	/**
	 * Método para excluir tupla de uma tabela
	 */
	public void exclui() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "delete from PCMSO.CARGOS" );
			cmd.append( " where ID_CARGO = ?" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			try {

				ps.setObject( 1, cargo.getIdCargo() );
				
				ps.execute();
			} finally {
				ps.close();
			}
		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}
}