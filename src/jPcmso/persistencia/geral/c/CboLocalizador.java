package jPcmso.persistencia.geral.c;

import quatro.persistencia.Localizador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.QtSQLException;
import quatro.sql.Query;
import quatro.sql.Conexao;
import quatro.sql.PoolDeConexoes;
import quatro.sql.ConexaoParaPoolDeConexoes;

/**
 *
 * Classe localizador para PCMSO.CBOS
 *
 * @author ronei@unimed-uau.com.br
 * @see Localizador
 * @see Cbo
 * @see CboNavegador
 * @see CboPersistor
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 24/01/2018 11:18 - importado por ronei@unimed-uau.com.br
 *
 */
 public class CboLocalizador extends Localizador {

	/** Inst�ncia de Cbo para manipula��o pelo localizador */
	private Cbo cbo;

	/** Construtor que recebe uma conex�o e um objeto Cbo completo */
	public CboLocalizador( Conexao conexao, Cbo cbo ) throws QtSQLException {

		super.setConexao( conexao );

		this.cbo = cbo;
		busca();
	}

	/** Construtor que recebe um objeto Cbo completo */
	public CboLocalizador( Cbo cbo ) throws QtSQLException {

		this.cbo = cbo;
		busca();
	}

	/** 
	 * Construtor que recebe uma conex�o e a chave da tabela PCMSO.CBOS
	 * @param cdCbo Codigo de CBO
	 * @param conexao Conexao com o banco de dados
	 */
	public CboLocalizador( Conexao conexao, String cdCbo ) throws QtSQLException { 

		super.setConexao( conexao ); 
		cbo = new Cbo();
		this.cbo.setCdCbo( cdCbo );
		
		busca();
	}

	/** 
	 * Construtor que recebe a chave da tabela PCMSO.CBOS
	 * @param cdCbo Codigo de CBO
	 */
	public CboLocalizador( String cdCbo ) throws QtSQLException { 

		cbo = new Cbo();
		this.cbo.setCdCbo( cdCbo );
		
		busca();
	}

	public static boolean existe( Conexao cnx, String cdCbo ) throws QtSQLException {

		Query q = new Query( cnx );

		try {
			q.setSQL( "select count(*) from PCMSO.CBOS" );
			q.addSQL( " where CD_CBO = :prm1CdCbo" );

			q.setParameter( "prm1CdCbo", cdCbo );
			q.executeQuery();

			return q.getInt( 1 ) > 0;
		} finally {
			q.liberaRecursos();
		}
	}

	public static boolean existe( String cdCbo ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return existe( cnx, cdCbo );
		} finally {
			cnx.libera();
		}
	}

	public static boolean existe( Conexao cnx, Cbo cbo ) throws QtSQLException {

		return existe( cnx, cbo.getCdCbo() );
	}

	public static boolean existe( Cbo cbo ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return existe( cnx, cbo.getCdCbo() );
		} finally {
			cnx.libera();
		}
	}

	public static Cbo buscaCbo( Conexao cnx, String cdCbo ) throws QtSQLException {

		Query q = new Query( cnx );

		try {
			q.setSQL( "select * from PCMSO.CBOS" );
			q.addSQL( " where CD_CBO = :prm1CdCbo" );

			q.setParameter( "prm1CdCbo", cdCbo );
			q.executeQuery();

			if( q.isEmpty() ) {
				return null;
			} else {
				Cbo cbo = new Cbo();
				buscaCampos( cbo, q );
				return cbo;
			}
		} finally {
			q.liberaRecursos();
		}
	}

	public static Cbo buscaCbo( String cdCbo ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return buscaCbo( cnx, cdCbo );
		} finally { 
			cnx.libera();
		}
	}

	public static void buscaCampos( Cbo cbo, Query query ) throws QtSQLException {

		cbo.setCdCbo( query.getString( "CD_CBO" ) );
		cbo.setNmCbo( query.getString( "NM_CBO" ) );
		cbo.setDsAtividade( query.getString( "DS_ATIVIDADE" ) );
	}

	/** M�todo que retorna uma tabela b�sica a que se refere o localizador */
	public TabelaBasica getTabelaBasica() {

		return this.cbo;
	}

	/** M�todo que retorna a classe Cbo relacionada ao localizador */
	public Cbo getCbo() {

		return this.cbo;
	}

	/**
	 * M�todo para buscar e preencher uma tupla de uma tabela
	 * @param tabelaBasica Recebe uma tabela basica com os atributos chave preenchidos
	 */
	public boolean busca( TabelaBasica tabelaBasica ) throws QtSQLException {

		if( !( tabelaBasica instanceof Cbo ) )
			throw new QtSQLException( "Esperava um objeto Cbo!" );

		this.cbo = (Cbo) tabelaBasica;
		return busca();

	}

	/**
	 * M�todo privado para buscar e preencher uma tupla de uma tabela
	 * Este m�todo busca os dados da inst�ncia de Cbo atual
	 */
	private boolean busca() throws QtSQLException {

		boolean ret = false;

		super.estabeleceConexao();
		Query query = super.criaNovaQuery();

		try {

			query.setSQL( "select * from PCMSO.CBOS" );
			query.addSQL( " where CD_CBO like :prm1CdCbo" );

			query.setParameter( "prm1CdCbo", cbo.getCdCbo() );
			query.executeQuery();

			if( query.isEmpty() ) {
				super.setEmpty( true );
				cbo = null;
			} else {
				super.setEmpty( false );
				
				cbo.setCdCbo( query.getString( "CD_CBO" ) );
				cbo.setNmCbo( query.getString( "NM_CBO" ) );
				cbo.setDsAtividade( query.getString( "DS_ATIVIDADE" ) );
				ret = true;
			}
		} finally {
			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}
 }