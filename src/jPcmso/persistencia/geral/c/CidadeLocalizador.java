package jPcmso.persistencia.geral.c;

import quatro.persistencia.Localizador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.QtSQLException;
import quatro.sql.Query;
import quatro.sql.Conexao;
import quatro.sql.PoolDeConexoes;
import quatro.sql.ConexaoParaPoolDeConexoes;

/**
 *
 * Classe localizador para CIDADES
 *
 * @author Samuel Antonio Klein
 * @see Localizador
 * @see Cidade
 * @see CidadeNavegador
 * @see CidadePersistor
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 public class CidadeLocalizador extends Localizador {

	/** Inst�ncia de Cidade para manipula��o pelo localizador */
	private Cidade cidade;

	/** Construtor que recebe uma conex�o e um objeto Cidade completo */
	public CidadeLocalizador( Conexao conexao, Cidade cidade ) throws QtSQLException {

		super.setConexao( conexao );

		this.cidade = cidade;
		busca();
	}

	/** Construtor que recebe um objeto Cidade completo */
	public CidadeLocalizador( Cidade cidade ) throws QtSQLException {

		this.cidade = cidade;
		busca();
	}

	/** 
	 * Construtor que recebe uma conex�o e as chaves da tabela CIDADES
	 * @param cdEstado C�digo do Estado
	 * @param idCidade Identificador da Cidade
	 * @param conexao Conexao com o banco de dados
	 */
	public CidadeLocalizador( Conexao conexao, String cdEstado, int idCidade ) throws QtSQLException { 

		super.setConexao( conexao ); 
		cidade = new Cidade();
		this.cidade.setCdEstado( cdEstado );
		this.cidade.setIdCidade( new Integer( idCidade ) );
		
		busca();
	}

	/** 
	 * Construtor que recebe as chaves da tabela CIDADES
	 * @param cdEstado C�digo do Estado
	 * @param idCidade Identificador da Cidade
	 */
	public CidadeLocalizador( String cdEstado, int idCidade ) throws QtSQLException { 

		cidade = new Cidade();
		this.cidade.setCdEstado( cdEstado );
		this.cidade.setIdCidade( new Integer( idCidade ) );
		
		busca();
	}

	public static boolean existe( Conexao cnx, String cdEstado, int idCidade ) throws QtSQLException {

		Query q = new Query( cnx );

		try {
			q.setSQL( "select count(*) from CIDADES" );
			q.addSQL( " where CD_ESTADO = :prm1CdEstado and ID_CIDADE = :prm2IdCidade" );

			q.setParameter( "prm1CdEstado", cdEstado );
			q.setParameter( "prm2IdCidade", idCidade );
			q.executeQuery();

			return q.getInt( 1 ) > 0;
		} finally {
			q.liberaRecursos();
		}
	}

	public static boolean existe( String cdEstado, int idCidade ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return existe( cnx, cdEstado, idCidade );
		} finally {
			cnx.libera();
		}
	}

	public static boolean existe( Conexao cnx, Cidade cidade ) throws QtSQLException {

		return existe( cnx, cidade.getCdEstado(), cidade.getIdCidade() );
	}

	public static boolean existe( Cidade cidade ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return existe( cnx, cidade.getCdEstado(), cidade.getIdCidade() );
		} finally {
			cnx.libera();
		}
	}

	public static Cidade buscaCidade( Conexao cnx, String cdEstado, int idCidade ) throws QtSQLException {

		Query q = new Query( cnx );

		try {
			q.setSQL( "select * from CIDADES" );
			q.addSQL( " where CD_ESTADO = :prm1CdEstado and ID_CIDADE = :prm2IdCidade" );

			q.setParameter( "prm1CdEstado", cdEstado );
			q.setParameter( "prm2IdCidade", idCidade );
			q.executeQuery();

			if( q.isEmpty() ) {
				return null;
			} else {
				Cidade cidade = new Cidade();
				buscaCampos( cidade, q );
				return cidade;
			}
		} finally {
			q.liberaRecursos();
		}
	}

	public static Cidade buscaCidade( String cdEstado, int idCidade ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return buscaCidade( cnx, cdEstado, idCidade );
		} finally { 
			cnx.libera();
		}
	}

	/** 
	 * Construtor que recebe uma conex�o e as chaves da tabela CIDADES
	 * @param cdEstado C�digo do Estado
	 * @param idCidade Identificador da Cidade
	 * @param conexao Conexao com o banco de dados
	 */
	public CidadeLocalizador( Conexao conexao, String cdEstado, Integer idCidade ) throws QtSQLException {

		super.setConexao( conexao ); 
		cidade = new Cidade();
		this.cidade.setCdEstado( cdEstado );
		this.cidade.setIdCidade( idCidade );
		
		busca();
	}

	/** 
	 * Construtor que recebe as chaves da tabela CIDADES
	 * @param cdEstado C�digo do Estado
	 * @param idCidade Identificador da Cidade
	 */
	public CidadeLocalizador( String cdEstado, Integer idCidade ) throws QtSQLException {

		cidade = new Cidade();
		this.cidade.setCdEstado( cdEstado );
		this.cidade.setIdCidade( idCidade );
		
		busca();
	}

	public static void buscaCampos( Cidade cidade, Query query ) throws QtSQLException {

		cidade.setCdEstado( query.getString( "CD_ESTADO" ) );
		cidade.setIdCidade( query.getInteger( "ID_CIDADE" ) );
		cidade.setDsCidade( query.getString( "DS_CIDADE" ) );
		cidade.setCdCep( query.getString( "CD_CEP" ) );
		cidade.setDsPracaBancaria( query.getString( "DS_PRACA_BANCARIA" ) );
		cidade.setCdIbge( query.getString( "CD_IBGE" ) );
		cidade.setCdDigitoIbge( query.getString( "CD_DIGITO_IBGE" ) );
	}

	/** M�todo que retorna uma tabela b�sica a que se refere o localizador */
	public TabelaBasica getTabelaBasica() {

		return this.cidade;
	}

	/** M�todo que retorna a classe Cidade relacionada ao localizador */
	public Cidade getCidade() {

		return this.cidade;
	}

	/**
	 * M�todo para buscar e preencher uma tupla de uma tabela
	 * @param tabelaBasica Recebe uma tabela basica com os atributos chave preenchidos
	 */
	public boolean busca( TabelaBasica tabelaBasica ) throws QtSQLException {

		if( !( tabelaBasica instanceof Cidade ) )
			throw new QtSQLException( "Esperava um objeto Cidade!" );

		this.cidade = (Cidade) tabelaBasica;
		return busca();

	}

	/**
	 * M�todo privado para buscar e preencher uma tupla de uma tabela
	 * Este m�todo busca os dados da inst�ncia de Cidade atual
	 */
	private boolean busca() throws QtSQLException {

		boolean ret = false;

		super.estabeleceConexao();
		Query query = super.criaNovaQuery();

		try {

			query.setSQL( "select * from CIDADES" );
			query.addSQL( " where CD_ESTADO like :prm1CdEstado and ID_CIDADE = :prm2IdCidade" );

			query.setParameter( "prm1CdEstado", cidade.getCdEstado() );
			query.setParameter( "prm2IdCidade", cidade.getIdCidade() );
			query.executeQuery();

			if( query.isEmpty() ) {
				super.setEmpty( true );
				cidade = null;
			} else {
				super.setEmpty( false );
				
				cidade.setCdEstado( query.getString( "CD_ESTADO" ) );
				cidade.setIdCidade( query.getInteger( "ID_CIDADE" ) );
				cidade.setDsCidade( query.getString( "DS_CIDADE" ) );
				cidade.setCdCep( query.getString( "CD_CEP" ) );
				cidade.setDsPracaBancaria( query.getString( "DS_PRACA_BANCARIA" ) );
				cidade.setCdIbge( query.getString( "CD_IBGE" ) );
				cidade.setCdDigitoIbge( query.getString( "CD_DIGITO_IBGE" ) );
				ret = true;
			}
		} finally {
			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}
 }