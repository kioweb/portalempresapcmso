package jPcmso.persistencia.geral.c;

import quatro.persistencia.Caminhador;
import quatro.persistencia.FiltroDeNavegador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;

 /**
 *
 * Classe Caminhador para Cbo
 *
 * @author ronei@unimed-uau.com.br
 * @see Caminhador
 * @see FiltroDeNavegador
 * @see CboNavegador
 * @see CboPersistor
 * @see CboLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 24/01/2018 11:18 - importado por ronei@unimed-uau.com.br
 *
 */
 public class CboCaminhador extends Caminhador {

	public static final int POR_ORDEM_DEFAULT = -1;
	public static final int POR_CODIGO = 0;
	public static final int POR_DESCRICAO = 1;
	public static final int POR_DESCRICAO_DESC = 2;
	
	private Cbo cbo;

	public CboCaminhador( FiltroDeNavegador filtro ) {

		setFiltro( filtro );
		inicializaVisao();
	}

	public CboCaminhador( Conexao conexao, FiltroDeNavegador filtro ) throws QtSQLException {

		super.setConexao( conexao );
		setFiltro( filtro );
		inicializaVisao();
	}

	private void inicializaVisao() {

		setNmVisao( "PCMSO.CBOS" );
	}

	protected void carregaCampos() throws QtSQLException {

		cbo = new Cbo();

		if( isRecordAvailable() ) {
			cbo.setCdCbo( query.getString( "CD_CBO" ) );
			cbo.setNmCbo( query.getString( "NM_CBO" ) );
			cbo.setDsAtividade( query.getString( "DS_ATIVIDADE" ) );
		}
	}

	/** M�todo que retorna uma tabela b�sica a que se refere o caminhador */
	public TabelaBasica getTabelaBasica() {

		return this.cbo;
	}

	/** M�todo que retorna a classe Cbo relacionada ao caminhador */
	public Cbo getCbo() {

		return this.cbo;
	}

	public void ativaVisao() throws QtSQLException {

		if(  super.getOrdemDeNavegacao() == CboCaminhador.POR_ORDEM_DEFAULT ) {
			super.ativaCaminhador( "order by CD_CBO" );
		} else if(  super.getOrdemDeNavegacao() == CboCaminhador.POR_CODIGO ) {
			super.ativaCaminhador( "order by CD_CBO" );
		} else if(  super.getOrdemDeNavegacao() == CboCaminhador.POR_DESCRICAO ) {
			super.ativaCaminhador( "order by NM_CBO" );
		} else if(  super.getOrdemDeNavegacao() == CboCaminhador.POR_DESCRICAO_DESC ) {
			super.ativaCaminhador( "order by NM_CBO desc" );
	
		} else {
			throw new QtSQLException( "CboCaminhador - Ordem de Navega��o n�o definida!" );
		}
	}

	public String getCamposIndice( int qualIndice ) {
		return getDsIndice( qualIndice );
	}

	public String getCamposChave() {
		return getDsChave();
	}

	/**
	 * M�todo est�tico que devolve os campos que comp�e a chave da tabela;
	 * @return campos que comp�e a chave.
	 */
	public static String getDsChave() {
		return "CD_CBO";
	}

	/**
	 * M�todo est�tico que devolve os campos que fazem parte de uma dada ordem
	 * de navega��o.
	 * @param qualIndice indica a ordem de navega��o desejada.
	 * @return campos que comp�e a ordem de navega��o.
	 */
	public static String getDsIndice( int qualIndice ) {

		switch( qualIndice ) {

			case POR_ORDEM_DEFAULT: return "CD_CBO";
			case POR_CODIGO: return "CD_CBO";
			case POR_DESCRICAO: return "NM_CBO";
			case POR_DESCRICAO_DESC: return "NM_CBO desc";
		}
		return null;
	}
}