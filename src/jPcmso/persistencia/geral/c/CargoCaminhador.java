package jPcmso.persistencia.geral.c;

import quatro.persistencia.Caminhador;
import quatro.persistencia.FiltroDeNavegador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;

 /**
 *
 * Classe Caminhador para Cargo
 *
 * @author ronei@unimed-uau.com.br
 * @see Caminhador
 * @see FiltroDeNavegador
 * @see CargoNavegador
 * @see CargoPersistor
 * @see CargoLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 14/12/2017 15:26 - importado por ronei@unimed-uau.com.br
 *
 */
 public class CargoCaminhador extends Caminhador {

	public static final int POR_ORDEM_DEFAULT = -1;
	public static final int POR_DS_CARGO = 0;
	
	private Cargo cargo;

	public CargoCaminhador( FiltroDeNavegador filtro ) {

		setFiltro( filtro );
		inicializaVisao();
	}

	public CargoCaminhador( Conexao conexao, FiltroDeNavegador filtro ) throws QtSQLException {

		super.setConexao( conexao );
		setFiltro( filtro );
		inicializaVisao();
	}

	private void inicializaVisao() {

		setNmVisao( "PCMSO.CARGOS" );
	}

	protected void carregaCampos() throws QtSQLException {

		cargo = new Cargo();

		if( isRecordAvailable() ) {
			cargo.setIdCargo( query.getInteger( "ID_CARGO" ) );
			cargo.setDsCargo( query.getString( "DS_CARGO" ) );
		}
	}

	/** Método que retorna uma tabela básica a que se refere o caminhador */
	public TabelaBasica getTabelaBasica() {

		return this.cargo;
	}

	/** Método que retorna a classe Cargo relacionada ao caminhador */
	public Cargo getCargo() {

		return this.cargo;
	}

	public void ativaVisao() throws QtSQLException {

		if(  super.getOrdemDeNavegacao() == CargoCaminhador.POR_ORDEM_DEFAULT ) {
			super.ativaCaminhador( "order by ID_CARGO" );
		} else if(  super.getOrdemDeNavegacao() == CargoCaminhador.POR_DS_CARGO ) {
			super.ativaCaminhador( "order by DS_CARGO" );
	
		} else {
			throw new QtSQLException( "CargoCaminhador - Ordem de Navegação não definida!" );
		}
	}

	public String getCamposIndice( int qualIndice ) {
		return getDsIndice( qualIndice );
	}

	public String getCamposChave() {
		return getDsChave();
	}

	/**
	 * Método estático que devolve os campos que compõe a chave da tabela;
	 * @return campos que compõe a chave.
	 */
	public static String getDsChave() {
		return "ID_CARGO";
	}

	/**
	 * Método estático que devolve os campos que fazem parte de uma dada ordem
	 * de navegação.
	 * @param qualIndice indica a ordem de navegação desejada.
	 * @return campos que compõe a ordem de navegação.
	 */
	public static String getDsIndice( int qualIndice ) {

		switch( qualIndice ) {

			case POR_ORDEM_DEFAULT: return "ID_CARGO";
			case POR_DS_CARGO: return "DS_CARGO";
		}
		return null;
	}
}