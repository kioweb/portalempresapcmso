package jPcmso.persistencia.geral.c;

import quatro.persistencia.Caminhador;
import quatro.persistencia.FiltroDeNavegador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;

 /**
 *
 * Classe Caminhador para CodigoConselhoProfissional
 *
 * @author Samuel Antonio Klein
 * @see Caminhador
 * @see FiltroDeNavegador
 * @see CodigoConselhoProfissionalNavegador
 * @see CodigoConselhoProfissionalPersistor
 * @see CodigoConselhoProfissionalLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 public class CodigoConselhoProfissionalCaminhador extends Caminhador {

	public static final int POR_ORDEM_DEFAULT = -1;
	public static final int POR_SIGLA = 0;
	public static final int POR_NOME = 1;
	
	private CodigoConselhoProfissional codigoConselhoProfissional;

	public CodigoConselhoProfissionalCaminhador( FiltroDeNavegador filtro ) {

		setFiltro( filtro );
		inicializaVisao();
	}

	public CodigoConselhoProfissionalCaminhador( Conexao conexao, FiltroDeNavegador filtro ) throws QtSQLException {

		super.setConexao( conexao );
		setFiltro( filtro );
		inicializaVisao();
	}

	private void inicializaVisao() {

		setNmVisao( "CODIGOS_CONSELHO_PROFISSIONAL" );
	}

	protected void carregaCampos() throws QtSQLException {

		codigoConselhoProfissional = new CodigoConselhoProfissional();

		if( isRecordAvailable() ) {
			codigoConselhoProfissional.setCdSigla( query.getString( "CD_SIGLA" ) );
			codigoConselhoProfissional.setDsConselho( query.getString( "DS_CONSELHO" ) );
		}
	}

	/** M�todo que retorna uma tabela b�sica a que se refere o caminhador */
	public TabelaBasica getTabelaBasica() {

		return this.codigoConselhoProfissional;
	}

	/** M�todo que retorna a classe CodigoConselhoProfissional relacionada ao caminhador */
	public CodigoConselhoProfissional getCodigoConselhoProfissional() {

		return this.codigoConselhoProfissional;
	}

	public void ativaVisao() throws QtSQLException {

		if(  super.getOrdemDeNavegacao() == CodigoConselhoProfissionalCaminhador.POR_ORDEM_DEFAULT ) {
			super.ativaCaminhador( "order by CD_SIGLA" );
		} else if(  super.getOrdemDeNavegacao() == CodigoConselhoProfissionalCaminhador.POR_SIGLA ) {
			super.ativaCaminhador( "order by CD_SIGLA" );
		} else if(  super.getOrdemDeNavegacao() == CodigoConselhoProfissionalCaminhador.POR_NOME ) {
			super.ativaCaminhador( "order by DS_CONSELHO, CD_SIGLA" );
	
		} else {
			throw new QtSQLException( "CodigoConselhoProfissionalCaminhador - Ordem de Navega��o n�o definida!" );
		}
	}

	public String getCamposIndice( int qualIndice ) {
		return getDsIndice( qualIndice );
	}

	public String getCamposChave() {
		return getDsChave();
	}

	/**
	 * M�todo est�tico que devolve os campos que comp�e a chave da tabela;
	 * @return campos que comp�e a chave.
	 */
	public static String getDsChave() {
		return "CD_SIGLA";
	}

	/**
	 * M�todo est�tico que devolve os campos que fazem parte de uma dada ordem
	 * de navega��o.
	 * @param qualIndice indica a ordem de navega��o desejada.
	 * @return campos que comp�e a ordem de navega��o.
	 */
	public static String getDsIndice( int qualIndice ) {

		switch( qualIndice ) {

			case POR_ORDEM_DEFAULT: return "CD_SIGLA";
			case POR_SIGLA: return "CD_SIGLA";
			case POR_NOME: return "DS_CONSELHO, CD_SIGLA";
		}
		return null;
	}
}