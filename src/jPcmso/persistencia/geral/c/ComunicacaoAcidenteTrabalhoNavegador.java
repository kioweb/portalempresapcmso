package jPcmso.persistencia.geral.c;

import quatro.persistencia.FiltroDeNavegador;
import quatro.persistencia.Navegador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.sql.Query;
import quatro.util.QtData;

/**
 *
 * Classe navegador para PCMSO.COMUNICACAO_ACIDENTE_TRABALHO
 *
 * @author Samuel Antonio Klein
 * @see Navegador
 * @see ComunicacaoAcidenteTrabalho
 * @see ComunicacaoAcidenteTrabalhoPersistor
 * @see ComunicacaoAcidenteTrabalhoLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:31 - importado por Samuel Antonio Klein
 *
 */
 public class ComunicacaoAcidenteTrabalhoNavegador extends Navegador {

	
	/** Inst�ncia de ComunicacaoAcidenteTrabalho para manipula��o pelo navegador */
	private ComunicacaoAcidenteTrabalho comunicacaoAcidenteTrabalho;

	/** Construtor b�sico */
	public ComunicacaoAcidenteTrabalhoNavegador()
	  throws QtSQLException { 

		comunicacaoAcidenteTrabalho = new ComunicacaoAcidenteTrabalho();
		primeiro();
	}

	/** Construtor b�sico que recebe uma conex�o */
	public ComunicacaoAcidenteTrabalhoNavegador( Conexao conexao )
	  throws QtSQLException { 

		super.setConexao( conexao ); 
		comunicacaoAcidenteTrabalho = new ComunicacaoAcidenteTrabalho();
		primeiro();
	}

	/**
	 * Construtor que recebe uma inst�ncia de ComunicacaoAcidenteTrabalhoFiltro e posiciona
	 * no primeiro registro que satisfaz suas condi��es 
	 */
	public ComunicacaoAcidenteTrabalhoNavegador( FiltroDeNavegador filtro ) throws QtSQLException  {
		super.setFiltro( filtro );
		this.comunicacaoAcidenteTrabalho = new ComunicacaoAcidenteTrabalho();
		primeiro();
	}

	/** Construtor que recebe as chaves da tabela PCMSO.COMUNICACAO_ACIDENTE_TRABALHO
	 * @param cdBeneficiarioCartao Codigo de Beneficiario 
	 * @param sqPerfil sequencia perfil
	 * @param sqComunicacao sequencia Comunica��o
	 */
	public ComunicacaoAcidenteTrabalhoNavegador( String cdBeneficiarioCartao, int sqPerfil, int sqComunicacao )
	  throws QtSQLException { 

		comunicacaoAcidenteTrabalho = new ComunicacaoAcidenteTrabalho();
		this.comunicacaoAcidenteTrabalho.setCdBeneficiarioCartao( cdBeneficiarioCartao );
		this.comunicacaoAcidenteTrabalho.setSqPerfil( new Integer( sqPerfil ) );
		this.comunicacaoAcidenteTrabalho.setSqComunicacao( new Integer( sqComunicacao ) );
		
		busca();
	}

	/** Construtor que recebe  uma conex�o e as chaves da tabela PCMSO.COMUNICACAO_ACIDENTE_TRABALHO
	 * @param cdBeneficiarioCartao Codigo de Beneficiario 
	 * @param sqPerfil sequencia perfil
	 * @param sqComunicacao sequencia Comunica��o
	 * @param conexao Conexao com o banco de dados
	 */
	public ComunicacaoAcidenteTrabalhoNavegador( Conexao conexao, String cdBeneficiarioCartao, int sqPerfil, int sqComunicacao )
	  throws QtSQLException { 

		super.setConexao( conexao ); 
		comunicacaoAcidenteTrabalho = new ComunicacaoAcidenteTrabalho();
		this.comunicacaoAcidenteTrabalho.setCdBeneficiarioCartao( cdBeneficiarioCartao );
		this.comunicacaoAcidenteTrabalho.setSqPerfil( new Integer( sqPerfil ) );
		this.comunicacaoAcidenteTrabalho.setSqComunicacao( new Integer( sqComunicacao ) );
		
		busca();
	}

	/** M�todo que retorna uma tabela b�sica a que se refere o navegador */
	public TabelaBasica getTabelaBasica() {

		return this.comunicacaoAcidenteTrabalho;
	}

	/** M�todo que retorna a classe ComunicacaoAcidenteTrabalho relacionada ao navegador */
	public ComunicacaoAcidenteTrabalho getComunicacaoAcidenteTrabalho() {

		return this.comunicacaoAcidenteTrabalho;
	}

	 /** M�todo que move para o primeiro registro da tabela */
	 public boolean primeiro() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select CD_BENEFICIARIO_CARTAO, SQ_PERFIL, SQ_COMUNICACAO" );
			query.addSQL( "  from PCMSO.COMUNICACAO_ACIDENTE_TRABALHO" );
			query.addSQL( super.getCondicaoDeFiltro( FiltroDeNavegador.COM_WHERE ) );
			query.addSQL( "  order by CD_BENEFICIARIO_CARTAO, SQ_PERFIL, SQ_COMUNICACAO" );
			query.addSQL( "  limit 1 " );
			
			query.executeQuery();

			
			if( query.isEmpty() ) {

				super.setQueryStates( false, true, true );
			} else {
				this.comunicacaoAcidenteTrabalho = new ComunicacaoAcidenteTrabalho();

				comunicacaoAcidenteTrabalho.setCdBeneficiarioCartao( query.getString( "CD_BENEFICIARIO_CARTAO" ) );
				comunicacaoAcidenteTrabalho.setSqPerfil( query.getInteger( "SQ_PERFIL" ) );
				comunicacaoAcidenteTrabalho.setSqComunicacao( query.getInteger( "SQ_COMUNICACAO" ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o registro anterior na tabela */
	 public boolean anterior() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select CD_BENEFICIARIO_CARTAO, SQ_PERFIL, SQ_COMUNICACAO" );
			query.addSQL( "  from PCMSO.COMUNICACAO_ACIDENTE_TRABALHO" );
			query.addSQL( "  where ( ( CD_BENEFICIARIO_CARTAO = :prm1CdBeneficiarioCartao and  SQ_PERFIL = :prm2SqPerfil and  SQ_COMUNICACAO < :prm3SqComunicacao )" );
			query.addSQL( "  or (  CD_BENEFICIARIO_CARTAO = :prm1CdBeneficiarioCartao and  SQ_PERFIL < :prm2SqPerfil )" );
			query.addSQL( "  or (  CD_BENEFICIARIO_CARTAO < :prm1CdBeneficiarioCartao ) )" );
			query.addSQL( super.getCondicaoDeFiltro() );
			query.addSQL( "  order by CD_BENEFICIARIO_CARTAO desc, SQ_PERFIL desc, SQ_COMUNICACAO desc" );
			query.addSQL( "  limit 1 " );

			query.setParameter( "prm1CdBeneficiarioCartao", comunicacaoAcidenteTrabalho.getCdBeneficiarioCartao() );
			query.setParameter( "prm2SqPerfil", comunicacaoAcidenteTrabalho.getSqPerfil() );
			query.setParameter( "prm3SqComunicacao", comunicacaoAcidenteTrabalho.getSqComunicacao() );
		
			query.executeQuery();

			
			if( query.isEmpty() ) {

				super.setFound( false );
				super.setBof( true );
			} else {
				this.comunicacaoAcidenteTrabalho = new ComunicacaoAcidenteTrabalho();

				comunicacaoAcidenteTrabalho.setCdBeneficiarioCartao( query.getString( "CD_BENEFICIARIO_CARTAO" ) );
				comunicacaoAcidenteTrabalho.setSqPerfil( query.getInteger( "SQ_PERFIL" ) );
				comunicacaoAcidenteTrabalho.setSqComunicacao( query.getInteger( "SQ_COMUNICACAO" ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o proximo registro da tabela */
	 public boolean proximo() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select CD_BENEFICIARIO_CARTAO, SQ_PERFIL, SQ_COMUNICACAO" );
			query.addSQL( "  from PCMSO.COMUNICACAO_ACIDENTE_TRABALHO" );
			query.addSQL( "  where ( (  CD_BENEFICIARIO_CARTAO = :prm1CdBeneficiarioCartao and  SQ_PERFIL = :prm2SqPerfil and  SQ_COMUNICACAO > :prm3SqComunicacao )" );
			query.addSQL( "  or (  CD_BENEFICIARIO_CARTAO = :prm1CdBeneficiarioCartao and  SQ_PERFIL > :prm2SqPerfil )" );
			query.addSQL( "  or (  CD_BENEFICIARIO_CARTAO > :prm1CdBeneficiarioCartao ) )" );
			query.addSQL( super.getCondicaoDeFiltro() );
			query.addSQL( "  order by CD_BENEFICIARIO_CARTAO, SQ_PERFIL, SQ_COMUNICACAO" );
			query.addSQL( "  limit 1 " );

			query.setParameter( "prm1CdBeneficiarioCartao", comunicacaoAcidenteTrabalho.getCdBeneficiarioCartao() );
			query.setParameter( "prm2SqPerfil", comunicacaoAcidenteTrabalho.getSqPerfil() );
			query.setParameter( "prm3SqComunicacao", comunicacaoAcidenteTrabalho.getSqComunicacao() );
		
			query.executeQuery();

			
			if( query.isEmpty() ) {

				super.setFound( false );
				super.setEof( true );
			} else {
				this.comunicacaoAcidenteTrabalho = new ComunicacaoAcidenteTrabalho();

				comunicacaoAcidenteTrabalho.setCdBeneficiarioCartao( query.getString( "CD_BENEFICIARIO_CARTAO" ) );
				comunicacaoAcidenteTrabalho.setSqPerfil( query.getInteger( "SQ_PERFIL" ) );
				comunicacaoAcidenteTrabalho.setSqComunicacao( query.getInteger( "SQ_COMUNICACAO" ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o �ltimo registro da tabela */
	 public boolean ultimo() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select CD_BENEFICIARIO_CARTAO, SQ_PERFIL, SQ_COMUNICACAO" );
			query.addSQL( "  from PCMSO.COMUNICACAO_ACIDENTE_TRABALHO" );
			query.addSQL( super.getCondicaoDeFiltro( FiltroDeNavegador.COM_WHERE ) 	);
			query.addSQL( "  order by CD_BENEFICIARIO_CARTAO desc, SQ_PERFIL desc, SQ_COMUNICACAO desc" );
			query.addSQL( "  limit 1 " );

			query.executeQuery();

			
			if( query.isEmpty() ) {

				super.setQueryStates( false, true, true );
			} else {
				this.comunicacaoAcidenteTrabalho = new ComunicacaoAcidenteTrabalho();

				comunicacaoAcidenteTrabalho.setCdBeneficiarioCartao( query.getString( "CD_BENEFICIARIO_CARTAO" ) );
				comunicacaoAcidenteTrabalho.setSqPerfil( query.getInteger( "SQ_PERFIL" ) );
				comunicacaoAcidenteTrabalho.setSqComunicacao( query.getInteger( "SQ_COMUNICACAO" ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	/**
	 * M�todo para buscar e preencher uma tupla de uma tabela
	 * @param tabelaBasica Recebe uma tabela basica com os atributos chave preenchidos
	 */
	public boolean busca( TabelaBasica tabelaBasica ) throws QtSQLException {

		if( !( tabelaBasica instanceof ComunicacaoAcidenteTrabalho ) )
			throw new QtSQLException( "Esperava um objeto ComunicacaoAcidenteTrabalho!" );

		this.comunicacaoAcidenteTrabalho = (ComunicacaoAcidenteTrabalho) tabelaBasica;
		return busca();

	}

	/**
	 * m�todo interno para buscar e preencher uma tupla de uma tabela
	 * Utiliza a Inst�ncia de ComunicacaoAcidenteTrabalho da classe para fazer a busca
	 */
	private boolean busca() throws QtSQLException {

		ComunicacaoAcidenteTrabalhoLocalizador localizador = new ComunicacaoAcidenteTrabalhoLocalizador( getConexao(), this.comunicacaoAcidenteTrabalho );

		if( localizador.isEmpty() ) {

			super.setQueryStates( false, true, true );
			return false;

		} else {

			super.setQueryStates( true, false, false );
			return true;
		}
	}
}