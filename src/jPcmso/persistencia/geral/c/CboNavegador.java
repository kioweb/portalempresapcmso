package jPcmso.persistencia.geral.c;

import quatro.persistencia.FiltroDeNavegador;
import quatro.persistencia.Navegador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.sql.Query;

/**
 *
 * Classe navegador para PCMSO.CBOS
 *
 * @author ronei@unimed-uau.com.br
 * @see Navegador
 * @see Cbo
 * @see CboPersistor
 * @see CboLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 24/01/2018 11:18 - importado por ronei@unimed-uau.com.br
 *
 */
 public class CboNavegador extends Navegador {

	
	/** Inst�ncia de Cbo para manipula��o pelo navegador */
	private Cbo cbo;

	/** Construtor b�sico */
	public CboNavegador()
	  throws QtSQLException { 

		cbo = new Cbo();
		primeiro();
	}

	/** Construtor b�sico que recebe uma conex�o */
	public CboNavegador( Conexao conexao )
	  throws QtSQLException { 

		super.setConexao( conexao ); 
		cbo = new Cbo();
		primeiro();
	}

	/**
	 * Construtor que recebe uma inst�ncia de CboFiltro e posiciona
	 * no primeiro registro que satisfaz suas condi��es 
	 */
	public CboNavegador( FiltroDeNavegador filtro ) throws QtSQLException  {
		super.setFiltro( filtro );
		this.cbo = new Cbo();
		primeiro();
	}

	/** Construtor que recebe a chave da tabela PCMSO.CBOS
	 * @param cdCbo Codigo de CBO
	 */
	public CboNavegador( String cdCbo )
	  throws QtSQLException { 

		cbo = new Cbo();
		this.cbo.setCdCbo( cdCbo );
		
		busca();
	}

	/** Construtor que recebe  uma conex�o e a chave da tabela PCMSO.CBOS
	 * @param cdCbo Codigo de CBO
	 * @param conexao Conexao com o banco de dados
	 */
	public CboNavegador( Conexao conexao, String cdCbo )
	  throws QtSQLException { 

		super.setConexao( conexao ); 
		cbo = new Cbo();
		this.cbo.setCdCbo( cdCbo );
		
		busca();
	}

	/** M�todo que retorna uma tabela b�sica a que se refere o navegador */
	public TabelaBasica getTabelaBasica() {

		return this.cbo;
	}

	/** M�todo que retorna a classe Cbo relacionada ao navegador */
	public Cbo getCbo() {

		return this.cbo;
	}

	 /** M�todo que move para o primeiro registro da tabela */
	 public boolean primeiro() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select min( CD_CBO ) from PCMSO.CBOS" );
			query.addSQL( super.getCondicaoDeFiltro( FiltroDeNavegador.COM_WHERE ) 	);

			query.executeQuery();

			
			if( query.getString( 1 ) == null ) {

				super.setQueryStates( false, true, true );
			} else {
				this.cbo = new Cbo();

				cbo.setCdCbo( query.getString( 1 ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o registro anterior na tabela */
	 public boolean anterior() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select max( CD_CBO ) from PCMSO.CBOS" );
			query.addSQL( " where CD_CBO < :prm1CdCbo" );
			query.addSQL( super.getCondicaoDeFiltro() );

			query.setParameter( "prm1CdCbo", cbo.getCdCbo() );
			
			query.executeQuery();

			
			if( query.getString( 1 ) == null ) {

				super.setFound( false );
				super.setBof( true );
			} else {
				this.cbo = new Cbo();

				cbo.setCdCbo( query.getString( 1 ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o proximo registro da tabela */
	 public boolean proximo() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select min( CD_CBO ) from PCMSO.CBOS" );
			query.addSQL( " where CD_CBO > :prm1CdCbo" );
			query.addSQL( super.getCondicaoDeFiltro() );

			query.setParameter( "prm1CdCbo", cbo.getCdCbo() );

			query.executeQuery();

			
			if( query.getString( 1 ) == null ) {

				super.setFound( false );
				super.setEof( true );
			} else {
				this.cbo = new Cbo();

				cbo.setCdCbo( query.getString( 1 ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o �ltimo registro da tabela */
	 public boolean ultimo() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select max( CD_CBO ) from PCMSO.CBOS" );
			query.addSQL( super.getCondicaoDeFiltro( FiltroDeNavegador.COM_WHERE ) 	);

			query.executeQuery();

			
			if( query.getString( 1 ) == null ) {

				super.setQueryStates( false, true, true );
			} else {
				this.cbo = new Cbo();

				cbo.setCdCbo( query.getString( 1 ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	/**
	 * M�todo para buscar e preencher uma tupla de uma tabela
	 * @param tabelaBasica Recebe uma tabela basica com os atributos chave preenchidos
	 */
	public boolean busca( TabelaBasica tabelaBasica ) throws QtSQLException {

		if( !( tabelaBasica instanceof Cbo ) )
			throw new QtSQLException( "Esperava um objeto Cbo!" );

		this.cbo = (Cbo) tabelaBasica;
		return busca();

	}

	/**
	 * m�todo interno para buscar e preencher uma tupla de uma tabela
	 * Utiliza a Inst�ncia de Cbo da classe para fazer a busca
	 */
	private boolean busca() throws QtSQLException {

		CboLocalizador localizador = new CboLocalizador( getConexao(), this.cbo );

		if( localizador.isEmpty() ) {

			super.setQueryStates( false, true, true );
			return false;

		} else {

			super.setQueryStates( true, false, false );
			return true;
		}
	}
}