package jPcmso.persistencia.geral.c;

import quatro.persistencia.TabelaBasica;
import quatro.smartDB.IgnoredField;
import quatro.smartDB.PrimaryKey;
import quatro.smartDB.Table;

/**
 *
 * Classe b�sica para CODIGOS_CONSELHO_PROFISSIONAL
 *
 * Esta tabela conter� os Codigos do Conselho Profissional
 * 
 * @author Samuel Antonio Klein
 * @see CodigoConselhoProfissionalNavegador
 * @see CodigoConselhoProfissionalPersistor
 * @see CodigoConselhoProfissionalLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 @Table(name="CODIGOS_CONSELHO_PROFISSIONAL")
 public class CodigoConselhoProfissional implements TabelaBasica {

	@IgnoredField
	private static final long serialVersionUID = -199229501074387625L;

	/** Sigla do Conselho Profissional (chave-varchar(12)) */
	@PrimaryKey
	private String cdSigla;
	/** Descri��o do Conselho (varchar(200)) */
	private String dsConselho;

	/**
	 * Alimenta Sigla do Conselho Profissional
	 * @param cdSigla Sigla do Conselho Profissional
	 */ 
	public void setCdSigla( String cdSigla ) {
		this.cdSigla = cdSigla;
	}

	/**
	 * Retorna Sigla do Conselho Profissional
	 * @return Sigla do Conselho Profissional
	 */
	public String getCdSigla() {
		return this.cdSigla;
	}

	/**
	 * Alimenta Descri��o do Conselho
	 * @param dsConselho Descri��o do Conselho
	 */ 
	public void setDsConselho( String dsConselho ) {
		this.dsConselho = dsConselho;
	}

	/**
	 * Retorna Descri��o do Conselho
	 * @return Descri��o do Conselho
	 */
	public String getDsConselho() {
		return this.dsConselho;
	}

}