package jPcmso.persistencia.geral.c;

import quatro.persistencia.CondicaoDeFiltro;
import quatro.persistencia.FiltroDeNavegador;

/**
 *
 * Classe filtro para CIDADES
 *
 * @author Samuel Antonio Klein
 * @see FiltroDeTabela
 * @see Cidade
 * @see CidadeNavegador
 * @see CidadePersistor
 * @see CidadeLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 public class CidadeFiltro extends FiltroDeNavegador {

	/** Filtro por cd estado */
	public void setFiltroPorCdEstado( String cdEstado ) {

		super.adicionaCondicao( "CD_ESTADO",CondicaoDeFiltro.IGUAL, cdEstado );
	}

	/** Filtro por nome */
	public void setFiltroPorNome( String dsCidade ) {

		super.adicionaCondicao( "DS_CIDADE",CondicaoDeFiltro.IGUAL, dsCidade );
	}

	/** Filtro por cep */
	public void setFiltroPorCep( String cdCep ) {

		super.adicionaCondicao( "CD_CEP",CondicaoDeFiltro.IGUAL, cdCep );
	}

	/** Determina a ordem de navega��o que ser� passada para o navegador */
	public void setOrdemDeNavegacao( int idOrdem ) {

		super.setOrdemDeNavegacao( idOrdem );
	}
}