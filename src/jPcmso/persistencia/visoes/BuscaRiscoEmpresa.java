package jPcmso.persistencia.visoes;

import quatro.persistencia.TabelaBasica;
import quatro.smartDB.IgnoredField;
import quatro.smartDB.PrimaryKey;
import quatro.smartDB.Table;

/**
 *
 * Classe b�sica para PCMSO.BUSCA_RISCOS_EMPRESA
 *
 * Tabela Riscos
 * 
 * @author Samuel Antonio Klein
 * @see BuscaRiscoEmpresaNavegador
 * @see BuscaRiscoEmpresaPersistor
 * @see BuscaRiscoEmpresaLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 @Table(name="PCMSO.BUSCA_RISCOS_EMPRESA")
 public class BuscaRiscoEmpresa implements TabelaBasica {

	@IgnoredField
	private static final long serialVersionUID = -2688312521206404L;

	/** Idetificador do Risco (chave) */
	@PrimaryKey
	private Integer idRisco;
	/** tipo de Risco (char(1)) */
	private String tpRisco;
	/** Descri��o de Risco (varchar(250)) */
	private String dsRisco;
	/**  (chave-char(4)) */
	@PrimaryKey
	private String cdEmpresa;

	/**
	 * Alimenta Idetificador do Risco
	 * @param idRisco Idetificador do Risco
	 */ 
	public void setIdRisco( Integer idRisco ) {
		this.idRisco = idRisco;
	}

	/**
	 * Retorna Idetificador do Risco
	 * @return Idetificador do Risco
	 */
	public Integer getIdRisco() {
		return this.idRisco;
	}

	/**
	 * Alimenta tipo de Risco
	 * @param tpRisco tipo de Risco
	 */ 
	public void setTpRisco( String tpRisco ) {
		this.tpRisco = tpRisco;
	}

	/**
	 * Retorna tipo de Risco
	 * @return tipo de Risco
	 */
	public String getTpRisco() {
		return this.tpRisco;
	}

	/**
	 * Alimenta Descri��o de Risco
	 * @param dsRisco Descri��o de Risco
	 */ 
	public void setDsRisco( String dsRisco ) {
		this.dsRisco = dsRisco;
	}

	/**
	 * Retorna Descri��o de Risco
	 * @return Descri��o de Risco
	 */
	public String getDsRisco() {
		return this.dsRisco;
	}

	/**
	 * Alimenta 
	 * @param cdEmpresa 
	 */ 
	public void setCdEmpresa( String cdEmpresa ) {
		this.cdEmpresa = cdEmpresa;
	}

	/**
	 * Retorna 
	 * @return 
	 */
	public String getCdEmpresa() {
		return this.cdEmpresa;
	}

}