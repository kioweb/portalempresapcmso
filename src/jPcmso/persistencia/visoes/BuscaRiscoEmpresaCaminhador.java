package jPcmso.persistencia.visoes;

import quatro.persistencia.Caminhador;
import quatro.persistencia.FiltroDeNavegador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;

 /**
 *
 * Classe Caminhador para BuscaRiscoEmpresa
 *
 * @author Samuel Antonio Klein
 * @see Caminhador
 * @see FiltroDeNavegador
 * @see BuscaRiscoEmpresaNavegador
 * @see BuscaRiscoEmpresaPersistor
 * @see BuscaRiscoEmpresaLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 public class BuscaRiscoEmpresaCaminhador extends Caminhador {

	public static final int POR_ORDEM_DEFAULT = -1;
	public static final int POR_CODIGO = 0;
	public static final int POR_DESCRICAO = 1;
	public static final int POR_TIPO_RISCO = 2;
	
	private BuscaRiscoEmpresa buscaRiscoEmpresa;

	public BuscaRiscoEmpresaCaminhador( FiltroDeNavegador filtro ) {

		setFiltro( filtro );
		inicializaVisao();
	}

	public BuscaRiscoEmpresaCaminhador( Conexao conexao, FiltroDeNavegador filtro ) throws QtSQLException {

		super.setConexao( conexao );
		setFiltro( filtro );
		inicializaVisao();
	}

	private void inicializaVisao() {

		setNmVisao( "PCMSO.BUSCA_RISCOS_EMPRESA" );
	}

	protected void carregaCampos() throws QtSQLException {

		buscaRiscoEmpresa = new BuscaRiscoEmpresa();

		if( isRecordAvailable() ) {
			buscaRiscoEmpresa.setIdRisco( query.getInteger( "ID_RISCO" ) );
			buscaRiscoEmpresa.setTpRisco( query.getString( "TP_RISCO" ) );
			buscaRiscoEmpresa.setDsRisco( query.getString( "DS_RISCO" ) );
			buscaRiscoEmpresa.setCdEmpresa( query.getString( "CD_EMPRESA" ) );
		}
	}

	/** M�todo que retorna uma tabela b�sica a que se refere o caminhador */
	public TabelaBasica getTabelaBasica() {

		return this.buscaRiscoEmpresa;
	}

	/** M�todo que retorna a classe BuscaRiscoEmpresa relacionada ao caminhador */
	public BuscaRiscoEmpresa getBuscaRiscoEmpresa() {

		return this.buscaRiscoEmpresa;
	}

	public void ativaVisao() throws QtSQLException {

		if(  super.getOrdemDeNavegacao() == BuscaRiscoEmpresaCaminhador.POR_ORDEM_DEFAULT ) {
			super.ativaCaminhador( "order by ID_RISCO, CD_EMPRESA" );
		} else if(  super.getOrdemDeNavegacao() == BuscaRiscoEmpresaCaminhador.POR_CODIGO ) {
			super.ativaCaminhador( "order by ID_RISCO" );
		} else if(  super.getOrdemDeNavegacao() == BuscaRiscoEmpresaCaminhador.POR_DESCRICAO ) {
			super.ativaCaminhador( "order by DS_RISCO" );
		} else if(  super.getOrdemDeNavegacao() == BuscaRiscoEmpresaCaminhador.POR_TIPO_RISCO ) {
			super.ativaCaminhador( "order by TP_RISCO, DS_RISCO" );
	
		} else {
			throw new QtSQLException( "BuscaRiscoEmpresaCaminhador - Ordem de Navega��o n�o definida!" );
		}
	}

	public String getCamposIndice( int qualIndice ) {
		return getDsIndice( qualIndice );
	}

	public String getCamposChave() {
		return getDsChave();
	}

	/**
	 * M�todo est�tico que devolve os campos que comp�e a chave da tabela;
	 * @return campos que comp�e a chave.
	 */
	public static String getDsChave() {
		return "ID_RISCO, CD_EMPRESA";
	}

	/**
	 * M�todo est�tico que devolve os campos que fazem parte de uma dada ordem
	 * de navega��o.
	 * @param qualIndice indica a ordem de navega��o desejada.
	 * @return campos que comp�e a ordem de navega��o.
	 */
	public static String getDsIndice( int qualIndice ) {

		switch( qualIndice ) {

			case POR_ORDEM_DEFAULT: return "ID_RISCO, CD_EMPRESA";
			case POR_CODIGO: return "ID_RISCO";
			case POR_DESCRICAO: return "DS_RISCO";
			case POR_TIPO_RISCO: return "TP_RISCO, DS_RISCO";
		}
		return null;
	}
}