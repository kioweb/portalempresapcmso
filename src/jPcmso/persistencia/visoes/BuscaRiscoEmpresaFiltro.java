package jPcmso.persistencia.visoes;

import quatro.persistencia.FiltroDeNavegador;

/**
 *
 * Classe filtro para PCMSO.BUSCA_RISCOS_EMPRESA
 *
 * @author Samuel Antonio Klein
 * @see FiltroDeTabela
 * @see BuscaRiscoEmpresa
 * @see BuscaRiscoEmpresaNavegador
 * @see BuscaRiscoEmpresaPersistor
 * @see BuscaRiscoEmpresaLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 public class BuscaRiscoEmpresaFiltro extends FiltroDeNavegador {

	/** Determina a ordem de navega��o que ser� passada para o navegador */
	public void setOrdemDeNavegacao( int idOrdem ) {

		super.setOrdemDeNavegacao( idOrdem );
	}
}