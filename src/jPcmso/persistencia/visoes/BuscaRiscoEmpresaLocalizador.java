package jPcmso.persistencia.visoes;

import quatro.persistencia.Localizador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.QtSQLException;
import quatro.sql.Query;
import quatro.sql.Conexao;
import quatro.sql.PoolDeConexoes;
import quatro.sql.ConexaoParaPoolDeConexoes;

/**
 *
 * Classe localizador para PCMSO.BUSCA_RISCOS_EMPRESA
 *
 * @author Samuel Antonio Klein
 * @see Localizador
 * @see BuscaRiscoEmpresa
 * @see BuscaRiscoEmpresaNavegador
 * @see BuscaRiscoEmpresaPersistor
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 public class BuscaRiscoEmpresaLocalizador extends Localizador {

	/** Inst�ncia de BuscaRiscoEmpresa para manipula��o pelo localizador */
	private BuscaRiscoEmpresa buscaRiscoEmpresa;

	/** Construtor que recebe uma conex�o e um objeto BuscaRiscoEmpresa completo */
	public BuscaRiscoEmpresaLocalizador( Conexao conexao, BuscaRiscoEmpresa buscaRiscoEmpresa ) throws QtSQLException {

		super.setConexao( conexao );

		this.buscaRiscoEmpresa = buscaRiscoEmpresa;
		busca();
	}

	/** Construtor que recebe um objeto BuscaRiscoEmpresa completo */
	public BuscaRiscoEmpresaLocalizador( BuscaRiscoEmpresa buscaRiscoEmpresa ) throws QtSQLException {

		this.buscaRiscoEmpresa = buscaRiscoEmpresa;
		busca();
	}

	/** 
	 * Construtor que recebe uma conex�o e as chaves da tabela PCMSO.BUSCA_RISCOS_EMPRESA
	 * @param idRisco Idetificador do Risco
	 * @param cdEmpresa 
	 * @param conexao Conexao com o banco de dados
	 */
	public BuscaRiscoEmpresaLocalizador( Conexao conexao, int idRisco, String cdEmpresa ) throws QtSQLException { 

		super.setConexao( conexao ); 
		buscaRiscoEmpresa = new BuscaRiscoEmpresa();
		this.buscaRiscoEmpresa.setIdRisco( new Integer( idRisco ) );
		this.buscaRiscoEmpresa.setCdEmpresa( cdEmpresa );
		
		busca();
	}

	/** 
	 * Construtor que recebe as chaves da tabela PCMSO.BUSCA_RISCOS_EMPRESA
	 * @param idRisco Idetificador do Risco
	 * @param cdEmpresa 
	 */
	public BuscaRiscoEmpresaLocalizador( int idRisco, String cdEmpresa ) throws QtSQLException { 

		buscaRiscoEmpresa = new BuscaRiscoEmpresa();
		this.buscaRiscoEmpresa.setIdRisco( new Integer( idRisco ) );
		this.buscaRiscoEmpresa.setCdEmpresa( cdEmpresa );
		
		busca();
	}

	public static boolean existe( Conexao cnx, int idRisco, String cdEmpresa ) throws QtSQLException {

		Query q = new Query( cnx );

		try {
			q.setSQL( "select count(*) from PCMSO.BUSCA_RISCOS_EMPRESA" );
			q.addSQL( " where ID_RISCO = :prm1IdRisco and CD_EMPRESA = :prm4CdEmpresa" );

			q.setParameter( "prm1IdRisco", idRisco );
			q.setParameter( "prm4CdEmpresa", cdEmpresa );
			q.executeQuery();

			return q.getInt( 1 ) > 0;
		} finally {
			q.liberaRecursos();
		}
	}

	public static boolean existe( int idRisco, String cdEmpresa ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return existe( cnx, idRisco, cdEmpresa );
		} finally {
			cnx.libera();
		}
	}

	public static boolean existe( Conexao cnx, BuscaRiscoEmpresa buscaRiscoEmpresa ) throws QtSQLException {

		return existe( cnx, buscaRiscoEmpresa.getIdRisco(), buscaRiscoEmpresa.getCdEmpresa() );
	}

	public static boolean existe( BuscaRiscoEmpresa buscaRiscoEmpresa ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return existe( cnx, buscaRiscoEmpresa.getIdRisco(), buscaRiscoEmpresa.getCdEmpresa() );
		} finally {
			cnx.libera();
		}
	}

	public static BuscaRiscoEmpresa buscaBuscaRiscoEmpresa( Conexao cnx, int idRisco, String cdEmpresa ) throws QtSQLException {

		Query q = new Query( cnx );

		try {
			q.setSQL( "select * from PCMSO.BUSCA_RISCOS_EMPRESA" );
			q.addSQL( " where ID_RISCO = :prm1IdRisco and CD_EMPRESA = :prm4CdEmpresa" );

			q.setParameter( "prm1IdRisco", idRisco );
			q.setParameter( "prm4CdEmpresa", cdEmpresa );
			q.executeQuery();

			if( q.isEmpty() ) {
				return null;
			} else {
				BuscaRiscoEmpresa buscaRiscoEmpresa = new BuscaRiscoEmpresa();
				buscaCampos( buscaRiscoEmpresa, q );
				return buscaRiscoEmpresa;
			}
		} finally {
			q.liberaRecursos();
		}
	}

	public static BuscaRiscoEmpresa buscaBuscaRiscoEmpresa( int idRisco, String cdEmpresa ) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

		try {
			return buscaBuscaRiscoEmpresa( cnx, idRisco, cdEmpresa );
		} finally { 
			cnx.libera();
		}
	}

	/** 
	 * Construtor que recebe uma conex�o e as chaves da tabela PCMSO.BUSCA_RISCOS_EMPRESA
	 * @param idRisco Idetificador do Risco
	 * @param cdEmpresa 
	 * @param conexao Conexao com o banco de dados
	 */
	public BuscaRiscoEmpresaLocalizador( Conexao conexao, Integer idRisco, String cdEmpresa ) throws QtSQLException {

		super.setConexao( conexao ); 
		buscaRiscoEmpresa = new BuscaRiscoEmpresa();
		this.buscaRiscoEmpresa.setIdRisco( idRisco );
		this.buscaRiscoEmpresa.setCdEmpresa( cdEmpresa );
		
		busca();
	}

	/** 
	 * Construtor que recebe as chaves da tabela PCMSO.BUSCA_RISCOS_EMPRESA
	 * @param idRisco Idetificador do Risco
	 * @param cdEmpresa 
	 */
	public BuscaRiscoEmpresaLocalizador( Integer idRisco, String cdEmpresa ) throws QtSQLException {

		buscaRiscoEmpresa = new BuscaRiscoEmpresa();
		this.buscaRiscoEmpresa.setIdRisco( idRisco );
		this.buscaRiscoEmpresa.setCdEmpresa( cdEmpresa );
		
		busca();
	}

	public static void buscaCampos( BuscaRiscoEmpresa buscaRiscoEmpresa, Query query ) throws QtSQLException {

		buscaRiscoEmpresa.setIdRisco( query.getInteger( "ID_RISCO" ) );
		buscaRiscoEmpresa.setTpRisco( query.getString( "TP_RISCO" ) );
		buscaRiscoEmpresa.setDsRisco( query.getString( "DS_RISCO" ) );
		buscaRiscoEmpresa.setCdEmpresa( query.getString( "CD_EMPRESA" ) );
	}

	/** M�todo que retorna uma tabela b�sica a que se refere o localizador */
	public TabelaBasica getTabelaBasica() {

		return this.buscaRiscoEmpresa;
	}

	/** M�todo que retorna a classe BuscaRiscoEmpresa relacionada ao localizador */
	public BuscaRiscoEmpresa getBuscaRiscoEmpresa() {

		return this.buscaRiscoEmpresa;
	}

	/**
	 * M�todo para buscar e preencher uma tupla de uma tabela
	 * @param tabelaBasica Recebe uma tabela basica com os atributos chave preenchidos
	 */
	public boolean busca( TabelaBasica tabelaBasica ) throws QtSQLException {

		if( !( tabelaBasica instanceof BuscaRiscoEmpresa ) )
			throw new QtSQLException( "Esperava um objeto BuscaRiscoEmpresa!" );

		this.buscaRiscoEmpresa = (BuscaRiscoEmpresa) tabelaBasica;
		return busca();

	}

	/**
	 * M�todo privado para buscar e preencher uma tupla de uma tabela
	 * Este m�todo busca os dados da inst�ncia de BuscaRiscoEmpresa atual
	 */
	private boolean busca() throws QtSQLException {

		boolean ret = false;

		super.estabeleceConexao();
		Query query = super.criaNovaQuery();

		try {

			query.setSQL( "select * from PCMSO.BUSCA_RISCOS_EMPRESA" );
			query.addSQL( " where ID_RISCO = :prm1IdRisco and CD_EMPRESA like :prm4CdEmpresa" );

			query.setParameter( "prm1IdRisco", buscaRiscoEmpresa.getIdRisco() );
			query.setParameter( "prm4CdEmpresa", buscaRiscoEmpresa.getCdEmpresa() );
			query.executeQuery();

			if( query.isEmpty() ) {
				super.setEmpty( true );
				buscaRiscoEmpresa = null;
			} else {
				super.setEmpty( false );
				
				buscaRiscoEmpresa.setIdRisco( query.getInteger( "ID_RISCO" ) );
				buscaRiscoEmpresa.setTpRisco( query.getString( "TP_RISCO" ) );
				buscaRiscoEmpresa.setDsRisco( query.getString( "DS_RISCO" ) );
				buscaRiscoEmpresa.setCdEmpresa( query.getString( "CD_EMPRESA" ) );
				ret = true;
			}
		} finally {
			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}
 }