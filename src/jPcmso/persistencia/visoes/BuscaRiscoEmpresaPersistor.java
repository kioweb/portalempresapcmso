package jPcmso.persistencia.visoes;

import java.sql.PreparedStatement;

import quatro.persistencia.Persistor;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.util.QtException;

/**
 *
 * Classe persistor para PCMSO.BUSCA_RISCOS_EMPRESA
 *
 * @author Samuel Antonio Klein
 * @see Persistor
 * @see BuscaRiscoEmpresa
 * @see BuscaRiscoEmpresaNavegador
 * @see BuscaRiscoEmpresaLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 public class BuscaRiscoEmpresaPersistor extends Persistor {

	/** Inst�ncia de BuscaRiscoEmpresa para manipula��o pelo persistor */
	private BuscaRiscoEmpresa buscaRiscoEmpresa;

	/** Construtor gen�rico */
	public BuscaRiscoEmpresaPersistor() {

		this.buscaRiscoEmpresa = null;
	}

	/** Construtor gen�rico */
	public BuscaRiscoEmpresaPersistor( Conexao cnx ) throws QtSQLException {

		this.buscaRiscoEmpresa = null;
		super.setConexao( cnx );
	}

	/** Construtor que recebe uma inst�ncia de BuscaRiscoEmpresa */
	public BuscaRiscoEmpresaPersistor( BuscaRiscoEmpresa buscaRiscoEmpresa ) throws QtSQLException {

		if( buscaRiscoEmpresa == null ) 
			throw new QtSQLException( "Imposs�vel instanciar BuscaRiscoEmpresaPersistor porque est� apontando para um nulo!" );

		this.buscaRiscoEmpresa = buscaRiscoEmpresa;
	}

	/** Construtor que recebe uma conex�o e uma inst�ncia de BuscaRiscoEmpresa */
	public BuscaRiscoEmpresaPersistor( Conexao conexao, BuscaRiscoEmpresa buscaRiscoEmpresa ) throws QtSQLException {

		if( buscaRiscoEmpresa == null ) 
			throw new QtSQLException( "Imposs�vel instanciar BuscaRiscoEmpresaPersistor porque est� apontando para um nulo!" );

		setConexao( conexao );
		this.buscaRiscoEmpresa = buscaRiscoEmpresa;
	}

	/**
	 * Construtor que recebe as chaves da tabela PCMSO.BUSCA_RISCOS_EMPRESA
	 * @param idRisco Idetificador do Risco
	 * @param cdEmpresa 
	 */
	public BuscaRiscoEmpresaPersistor( int idRisco, String cdEmpresa )
	  throws QtSQLException { 

		busca( idRisco, cdEmpresa );
	}

	/**
	 * Construtor que recebe uma conex�o e as chaves da tabela PCMSO.BUSCA_RISCOS_EMPRESA
	 * @param idRisco Idetificador do Risco
	 * @param cdEmpresa 
	 * @param conexao Conexao com o banco de dados
	 */
	public BuscaRiscoEmpresaPersistor( Conexao conexao, int idRisco, String cdEmpresa )
	  throws QtSQLException { 

		super.setConexao( conexao );
		busca( idRisco, cdEmpresa );
	}

	/**
	 * M�todo interno para buscar e preencher uma tupla de uma tabela
	 * Utiliza a Inst�ncia de BuscaRiscoEmpresa para fazer a busca
	 * @param idRisco Idetificador do Risco
	 * @param cdEmpresa 
	 */
	private void busca( int idRisco, String cdEmpresa ) throws QtSQLException { 

		BuscaRiscoEmpresaLocalizador buscaRiscoEmpresaLocalizador = new BuscaRiscoEmpresaLocalizador( getConexao(), idRisco, cdEmpresa );

		if( buscaRiscoEmpresaLocalizador.isEmpty() ) {
			throw new QtSQLException( "Chaves da Tabela BuscaRiscoEmpresa n�o encontradas - idRisco: " + idRisco + " / cdEmpresa: " + cdEmpresa );
		}

		buscaRiscoEmpresa = (BuscaRiscoEmpresa ) buscaRiscoEmpresaLocalizador.getTabelaBasica();
	}

	/**
	 * Permite alimentar o Persistor com um novo registro
	 */
	public void setTabelaBasica( TabelaBasica buscaRiscoEmpresa ) throws QtException {

		if( buscaRiscoEmpresa instanceof BuscaRiscoEmpresa ) {
			this.buscaRiscoEmpresa = (BuscaRiscoEmpresa) buscaRiscoEmpresa;
		} else {
			throw new QtException( "Tabela B�sica fornecida tem que ser uma inst�ncia de BuscaRiscoEmpresa" );
		}
	}

	/**
	 * Permite acessar o registro corrente do persistor
	 */
	public TabelaBasica getTabelaBasica() throws QtException {

		return buscaRiscoEmpresa;
	}

	/**
	 * M�todo para inserir tupla na tabela
	 */
	public void insere() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "insert into PCMSO.BUSCA_RISCOS_EMPRESA" );
			cmd.append( "( ID_RISCO, TP_RISCO, DS_RISCO, CD_EMPRESA )" );
			cmd.append( "values " );
			cmd.append( "( ?, ?, ?, ?)" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			ps.setObject( 1, buscaRiscoEmpresa.getIdRisco() );
			ps.setObject( 2, buscaRiscoEmpresa.getTpRisco() );
			ps.setObject( 3, buscaRiscoEmpresa.getDsRisco() );
			ps.setObject( 4, buscaRiscoEmpresa.getCdEmpresa() );
			
			ps.execute();

		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}

	/**
	 * M�todo para alterar tupla na tabela
	 */
	public void altera() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "update PCMSO.BUSCA_RISCOS_EMPRESA" );
			cmd.append( "  set ID_RISCO = ?, " );
			cmd.append( " TP_RISCO = ?, " );
			cmd.append( " DS_RISCO = ?, " );
			cmd.append( " CD_EMPRESA = ?" );
			cmd.append( " where ID_RISCO = ? and CD_EMPRESA like ?" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			ps.setObject( 1, buscaRiscoEmpresa.getIdRisco() );
			ps.setObject( 2, buscaRiscoEmpresa.getTpRisco() );
			ps.setObject( 3, buscaRiscoEmpresa.getDsRisco() );
			ps.setObject( 4, buscaRiscoEmpresa.getCdEmpresa() );
			
			ps.setObject( 5, buscaRiscoEmpresa.getIdRisco() );
			ps.setObject( 6, buscaRiscoEmpresa.getCdEmpresa() );
			
			ps.execute();

		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}

	/**
	 * M�todo para excluir tupla de uma tabela
	 */
	public void exclui() throws QtSQLException {

		estabeleceConexao();

		try {
			StringBuilder cmd = new StringBuilder( 512 );

			cmd.append( "delete from PCMSO.BUSCA_RISCOS_EMPRESA" );
			cmd.append( " where ID_RISCO = ? and CD_EMPRESA like ?" );

			PreparedStatement ps = getConexao().getConnection().prepareStatement( cmd.toString() );

			ps.setObject( 1, buscaRiscoEmpresa.getIdRisco() );
			ps.setObject( 2, buscaRiscoEmpresa.getCdEmpresa() );
			
			ps.execute();

		} catch ( Exception e ) {
			throw new QtSQLException( e.getMessage() );
		} finally {
			liberaConexao();
		}
	}
}