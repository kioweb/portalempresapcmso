package jPcmso.persistencia.visoes;

import quatro.persistencia.FiltroDeNavegador;
import quatro.persistencia.Navegador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.QtSQLException;
import quatro.sql.Query;

/**
 *
 * Classe navegador para PCMSO.BUSCA_RISCOS_EMPRESA
 *
 * @author Samuel Antonio Klein
 * @see Navegador
 * @see BuscaRiscoEmpresa
 * @see BuscaRiscoEmpresaPersistor
 * @see BuscaRiscoEmpresaLocalizador
 * @version 1.0.0
 * 
 * Obs: Criado pelo Argous em 26/06/2015 00:20 - importado por Samuel Antonio Klein
 *
 */
 public class BuscaRiscoEmpresaNavegador extends Navegador {

	
	/** Inst�ncia de BuscaRiscoEmpresa para manipula��o pelo navegador */
	private BuscaRiscoEmpresa buscaRiscoEmpresa;

	/** Construtor b�sico */
	public BuscaRiscoEmpresaNavegador()
	  throws QtSQLException { 

		buscaRiscoEmpresa = new BuscaRiscoEmpresa();
		primeiro();
	}

	/** Construtor b�sico que recebe uma conex�o */
	public BuscaRiscoEmpresaNavegador( Conexao conexao )
	  throws QtSQLException { 

		super.setConexao( conexao ); 
		buscaRiscoEmpresa = new BuscaRiscoEmpresa();
		primeiro();
	}

	/**
	 * Construtor que recebe uma inst�ncia de BuscaRiscoEmpresaFiltro e posiciona
	 * no primeiro registro que satisfaz suas condi��es 
	 */
	public BuscaRiscoEmpresaNavegador( FiltroDeNavegador filtro ) throws QtSQLException  {
		super.setFiltro( filtro );
		this.buscaRiscoEmpresa = new BuscaRiscoEmpresa();
		primeiro();
	}

	/** Construtor que recebe as chaves da tabela PCMSO.BUSCA_RISCOS_EMPRESA
	 * @param idRisco Idetificador do Risco
	 * @param cdEmpresa 
	 */
	public BuscaRiscoEmpresaNavegador( int idRisco, String cdEmpresa )
	  throws QtSQLException { 

		buscaRiscoEmpresa = new BuscaRiscoEmpresa();
		this.buscaRiscoEmpresa.setIdRisco( new Integer( idRisco ) );
		this.buscaRiscoEmpresa.setCdEmpresa( cdEmpresa );
		
		busca();
	}

	/** Construtor que recebe  uma conex�o e as chaves da tabela PCMSO.BUSCA_RISCOS_EMPRESA
	 * @param idRisco Idetificador do Risco
	 * @param cdEmpresa 
	 * @param conexao Conexao com o banco de dados
	 */
	public BuscaRiscoEmpresaNavegador( Conexao conexao, int idRisco, String cdEmpresa )
	  throws QtSQLException { 

		super.setConexao( conexao ); 
		buscaRiscoEmpresa = new BuscaRiscoEmpresa();
		this.buscaRiscoEmpresa.setIdRisco( new Integer( idRisco ) );
		this.buscaRiscoEmpresa.setCdEmpresa( cdEmpresa );
		
		busca();
	}

	/** M�todo que retorna uma tabela b�sica a que se refere o navegador */
	public TabelaBasica getTabelaBasica() {

		return this.buscaRiscoEmpresa;
	}

	/** M�todo que retorna a classe BuscaRiscoEmpresa relacionada ao navegador */
	public BuscaRiscoEmpresa getBuscaRiscoEmpresa() {

		return this.buscaRiscoEmpresa;
	}

	 /** M�todo que move para o primeiro registro da tabela */
	 public boolean primeiro() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select ID_RISCO, CD_EMPRESA" );
			query.addSQL( "  from PCMSO.BUSCA_RISCOS_EMPRESA" );
			query.addSQL( super.getCondicaoDeFiltro( FiltroDeNavegador.COM_WHERE ) );
			query.addSQL( "  order by ID_RISCO, CD_EMPRESA" );
			query.addSQL( "  limit 1 " );
			
			query.executeQuery();

			
			if( query.isEmpty() ) {

				super.setQueryStates( false, true, true );
			} else {
				this.buscaRiscoEmpresa = new BuscaRiscoEmpresa();

				buscaRiscoEmpresa.setIdRisco( query.getInteger( "ID_RISCO" ) );
				buscaRiscoEmpresa.setCdEmpresa( query.getString( "CD_EMPRESA" ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o registro anterior na tabela */
	 public boolean anterior() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select ID_RISCO, CD_EMPRESA" );
			query.addSQL( "  from PCMSO.BUSCA_RISCOS_EMPRESA" );
			query.addSQL( "  where ( ( ID_RISCO = :prm1IdRisco and  CD_EMPRESA < :prm4CdEmpresa )" );
			query.addSQL( "  or (  ID_RISCO < :prm1IdRisco ) )" );
			query.addSQL( super.getCondicaoDeFiltro() );
			query.addSQL( "  order by ID_RISCO desc, CD_EMPRESA desc" );
			query.addSQL( "  limit 1 " );

			query.setParameter( "prm1IdRisco", buscaRiscoEmpresa.getIdRisco() );
			query.setParameter( "prm4CdEmpresa", buscaRiscoEmpresa.getCdEmpresa() );
		
			query.executeQuery();

			
			if( query.isEmpty() ) {

				super.setFound( false );
				super.setBof( true );
			} else {
				this.buscaRiscoEmpresa = new BuscaRiscoEmpresa();

				buscaRiscoEmpresa.setIdRisco( query.getInteger( "ID_RISCO" ) );
				buscaRiscoEmpresa.setCdEmpresa( query.getString( "CD_EMPRESA" ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o proximo registro da tabela */
	 public boolean proximo() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select ID_RISCO, CD_EMPRESA" );
			query.addSQL( "  from PCMSO.BUSCA_RISCOS_EMPRESA" );
			query.addSQL( "  where ( (  ID_RISCO = :prm1IdRisco and  CD_EMPRESA > :prm4CdEmpresa )" );
			query.addSQL( "  or (  ID_RISCO > :prm1IdRisco ) )" );
			query.addSQL( super.getCondicaoDeFiltro() );
			query.addSQL( "  order by ID_RISCO, CD_EMPRESA" );
			query.addSQL( "  limit 1 " );

			query.setParameter( "prm1IdRisco", buscaRiscoEmpresa.getIdRisco() );
			query.setParameter( "prm4CdEmpresa", buscaRiscoEmpresa.getCdEmpresa() );
		
			query.executeQuery();

			
			if( query.isEmpty() ) {

				super.setFound( false );
				super.setEof( true );
			} else {
				this.buscaRiscoEmpresa = new BuscaRiscoEmpresa();

				buscaRiscoEmpresa.setIdRisco( query.getInteger( "ID_RISCO" ) );
				buscaRiscoEmpresa.setCdEmpresa( query.getString( "CD_EMPRESA" ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	 /** M�todo que move para o �ltimo registro da tabela */
	 public boolean ultimo() throws QtSQLException {

		boolean ret = false;
		estabeleceConexao();
		Query query = criaNovaQuery();

		try {
			query.setSQL( "select ID_RISCO, CD_EMPRESA" );
			query.addSQL( "  from PCMSO.BUSCA_RISCOS_EMPRESA" );
			query.addSQL( super.getCondicaoDeFiltro( FiltroDeNavegador.COM_WHERE ) 	);
			query.addSQL( "  order by ID_RISCO desc, CD_EMPRESA desc" );
			query.addSQL( "  limit 1 " );

			query.executeQuery();

			
			if( query.isEmpty() ) {

				super.setQueryStates( false, true, true );
			} else {
				this.buscaRiscoEmpresa = new BuscaRiscoEmpresa();

				buscaRiscoEmpresa.setIdRisco( query.getInteger( "ID_RISCO" ) );
				buscaRiscoEmpresa.setCdEmpresa( query.getString( "CD_EMPRESA" ) );
				
				ret = busca();
			}
		} finally {

			query.liberaRecursos();
			liberaConexao();
		}
		return ret;
	}

	/**
	 * M�todo para buscar e preencher uma tupla de uma tabela
	 * @param tabelaBasica Recebe uma tabela basica com os atributos chave preenchidos
	 */
	public boolean busca( TabelaBasica tabelaBasica ) throws QtSQLException {

		if( !( tabelaBasica instanceof BuscaRiscoEmpresa ) )
			throw new QtSQLException( "Esperava um objeto BuscaRiscoEmpresa!" );

		this.buscaRiscoEmpresa = (BuscaRiscoEmpresa) tabelaBasica;
		return busca();

	}

	/**
	 * m�todo interno para buscar e preencher uma tupla de uma tabela
	 * Utiliza a Inst�ncia de BuscaRiscoEmpresa da classe para fazer a busca
	 */
	private boolean busca() throws QtSQLException {

		BuscaRiscoEmpresaLocalizador localizador = new BuscaRiscoEmpresaLocalizador( getConexao(), this.buscaRiscoEmpresa );

		if( localizador.isEmpty() ) {

			super.setQueryStates( false, true, true );
			return false;

		} else {

			super.setQueryStates( true, false, false );
			return true;
		}
	}
}