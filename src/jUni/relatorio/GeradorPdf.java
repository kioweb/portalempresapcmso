package jUni.relatorio;


import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jPcmso.biblioteca.relatorio.QtJrDataSource;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;
import quatro.sql.ConexaoParaPoolDeConexoes;
import quatro.sql.PoolDeConexoes;

public class GeradorPdf extends HttpServlet {
	
	private static final long serialVersionUID = 1037650025385701232L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost( req, resp );
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		DadosRelatorioJasper dadosRelatorio = (DadosRelatorioJasper) req.getSession().getAttribute( "RelatorioPdf" );
		QtJrDataSource jq = (QtJrDataSource) req.getSession().getAttribute( "QtJrDataSource" );
		req.getSession().removeAttribute( "QtJrDataSource" );
		req.getSession().removeAttribute( "RelatorioPdf" );
		
		try {
			
			if( dadosRelatorio != null ) {

				ConexaoParaPoolDeConexoes cnx = null;
				
				try {
					cnx = PoolDeConexoes.getConexao();
					resp.setContentType( "application/pdf" );
					
					try {
						
						JasperPrint impressao = null;
						
						if( jq == null ) {
							impressao = JasperFillManager.fillReport( dadosRelatorio.getBaseRelatorio(), dadosRelatorio.getParametros(), cnx.getConnection() );
						} else {
							impressao = JasperFillManager.fillReport( dadosRelatorio.getBaseRelatorio(), dadosRelatorio.getParametros(), jq );
						}
						JRPdfExporter exporter = new JRPdfExporter();

						exporter.setExporterInput( new SimpleExporterInput( impressao ));
						exporter.setExporterOutput( new SimpleOutputStreamExporterOutput( resp.getOutputStream() ) );
						
						SimplePdfExporterConfiguration config = new SimplePdfExporterConfiguration();
						
						exporter.setConfiguration( config );
						
						exporter.exportReport();
						resp.flushBuffer();
						
					} catch( Exception e ) {
						e.printStackTrace();
					}
					
				} finally {
					cnx.libera();
				}
			} 
		} catch( Exception e ) {
			e.printStackTrace();
			throw new ServletException( e );
		}
	}
}
