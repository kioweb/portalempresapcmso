package jUni.relatorio;

import java.io.Serializable;
import java.util.Hashtable;

public class DadosRelatorioJasper implements Serializable {
	
	private static final long serialVersionUID = 1646614952786138983L;

	private String baseRelatorio;
	private Hashtable<String, Object> parametros;
	
	public String getBaseRelatorio() {
		return baseRelatorio;
	}
	public void setBaseRelatorio(String baseRelatorio) {
		this.baseRelatorio = baseRelatorio;
	}
	public Hashtable<String, Object> getParametros() {
		return parametros;
	}
	public void setParametros(Hashtable<String, Object> parametros) {
		this.parametros = parametros;
	}
}