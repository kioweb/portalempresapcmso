package jUni.bean;

import java.util.LinkedList;
import java.util.List;

import jPcmso.persistencia.geral.b.BeneficiarioCbo;
import jPcmso.persistencia.geral.e.ExameBeneficiario;
import jPcmso.persistencia.geral.r.RiscoBeneficiario;
import jUni.persistencia.geral.b.Beneficiario;

public class DadosBeneficiarioSelecionado {
	
	public DadosBeneficiarioSelecionado() {
		riscos = new LinkedList<>();
		exames = new LinkedList<>();
	}

	private Beneficiario beneficiario;
	private BeneficiarioCbo beneficiarioCbo;
	private List<RiscoBeneficiario> riscos;
	private List<ExameBeneficiario> exames;

	public Beneficiario getBeneficiario() {
		return beneficiario;
	}

	public void setBeneficiario(Beneficiario beneficiario) {
		this.beneficiario = beneficiario;
	}

	public BeneficiarioCbo getBeneficiarioCbo() {
		return beneficiarioCbo;
	}

	public void setBeneficiarioCbo(BeneficiarioCbo beneficiarioCbo) {
		this.beneficiarioCbo = beneficiarioCbo;
	}

	public List<RiscoBeneficiario> getRiscos() {
		return riscos;
	}

	public void setRiscos(List<RiscoBeneficiario> riscos) {
		this.riscos = riscos;
	}

	public List<ExameBeneficiario> getExames() {
		return exames;
	}

	public void setExames(List<ExameBeneficiario> exames) {
		this.exames = exames;
	}

}
