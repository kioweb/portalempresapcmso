package jUni.bean;

import java.lang.reflect.Method;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import jPcmso.persistencia.geral.b.BeneficiarioCbo;
import jPcmso.persistencia.geral.c.CargoLocalizador;
import jPcmso.persistencia.geral.c.CboLocalizador;
import jPcmso.persistencia.geral.e.ExameBeneficiarioCaminhador;
import jPcmso.persistencia.geral.e.ExameBeneficiarioLocalizador;
import jPcmso.persistencia.geral.f.FuncaoLocalizador;
import jPcmso.persistencia.geral.p.PreCadastroCbo;
import jPcmso.persistencia.geral.r.RiscoBeneficiarioCaminhador;
import jPcmso.persistencia.geral.r.RiscoLocalizador;
import jPcmso.persistencia.geral.s.SetorLocalizador;
import jUni.biblioteca.negocio.BibliotecaBeneficiario;
import jUni.biblioteca.parametros.ParametrosCliente;
import jUni.persistencia.geral.b.Beneficiario;
import jUni.persistencia.geral.b.BeneficiarioCaminhador;
import jUni.persistencia.geral.c.Cidade;
import jUni.persistencia.geral.c.CidadeLocalizador;
import jUni.persistencia.geral.d.Dependencia;
import jUni.persistencia.geral.d.DependenciaLocalizador;
import jUni.persistencia.geral.e.Empresa;
import jUni.persistencia.geral.e.EmpresaLocalizador;
import jUni.persistencia.geral.e.Estado;
import jUni.persistencia.geral.e.EstadoLocalizador;
import jUni.persistencia.geral.g.GrupoDeCarencia;
import jUni.persistencia.geral.g.GrupoDeCarenciaLocalizador;
import jUni.persistencia.geral.l.Lotacao;
import jUni.persistencia.geral.l.LotacaoLocalizador;
import jUni.persistencia.geral.s.Servico;
import jUni.persistencia.geral.s.ServicoLocalizador;
import jUni.util.Util;
import quatro.persistencia.FiltroDeNavegador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.ConexaoParaPoolDeConexoes;
import quatro.sql.PoolDeConexoes;
import quatro.sql.QtSQLException;
import quatro.util.ParametrosSistema;
import quatro.util.QtData;

@SessionScoped
@ManagedBean
public class BeanBeneficiariosPCMSO_bkp {

//	private DadosBeneficiarioSelecionado beneficiarioSelecionado;
//	private String msgAoUsuario;
//	private String dsCbo;
//	private String dsFuncao;
//	private String dsCargo;
//	private String dsSetor;
//	private String dsFuncaoAnterior;
//	private Integer msgExibida = 0;
//	private String filtraPorNome;
//	private String filtraPorCartao;
//
//	public BeanBeneficiariosPCMSO() {
//		beneficiarioSelecionado = new DadosBeneficiarioSelecionado();
//	}
//	
//	public void cancelar() {
//		Util.sendRedirect("resources/page/dashboard.jsf");
//	}
//	
//	public static Method getMtdGet( Object object, String campo )  {
//		Method mtdGet = null;
//		 
//		Method[] mt = object.getClass().getDeclaredMethods();
//		String nmMetodoGet = getNmMetodoGet( campo );
//		String nmMetodoIs  = getNmMetodoIs( campo );
//		
//		for( int i = 0; i < mt.length; i++ ) {
//			if( mtdGet == null ) {
//				if( mt[ i ].getName().equals( nmMetodoGet ) ) {
//					mtdGet = mt[ i ];
//				} else {
//					if( mt[ i ].getName().equals( nmMetodoIs ) ) {
//						mtdGet = mt[ i ];
//					}
//				}
//			}
//
//			if( mtdGet != null ) return mtdGet;
//		}
//		
//		mt = object.getClass().getSuperclass().getDeclaredMethods();
//		nmMetodoGet = getNmMetodoGet( campo );
//		nmMetodoIs  = getNmMetodoIs( campo );
//		
//		for( int i = 0; i < mt.length; i++ ) {
//			if( mtdGet == null ) {
//				if( mt[ i ].getName().equals( nmMetodoGet ) ) {
//					mtdGet = mt[ i ];
//				} else {
//					if( mt[ i ].getName().equals( nmMetodoIs ) ) {
//						mtdGet = mt[ i ];
//					}
//				}
//			}
//
//			if( mtdGet != null ) return mtdGet;
//		}
//		
//		return mtdGet;
//	}
//	
//	public static Method getMtdGet( Class<?> classe, String campo ) {
//		Method mtdGet = null;
//		 
//		Method[] mt = classe.getDeclaredMethods();
//		String nmMetodoGet = getNmMetodoGet( campo );
//		String nmMetodoIs  = getNmMetodoIs( campo );
//		
//		for( int i = 0; i < mt.length; i++ ) {
//			if( mtdGet == null ) {
//				if( mt[ i ].getName().equals( nmMetodoGet ) ) {
//					mtdGet = mt[ i ];
//				} else {
//					if( mt[ i ].getName().equals( nmMetodoIs ) ) {
//						mtdGet = mt[ i ];
//					}
//				}
//			}
//
//			if( mtdGet != null ) return mtdGet;
//		}
//		
//		mt = classe.getSuperclass().getDeclaredMethods();
//		nmMetodoGet = getNmMetodoGet( campo );
//		nmMetodoIs  = getNmMetodoIs( campo );
//		
//		for( int i = 0; i < mt.length; i++ ) {
//			if( mtdGet == null ) {
//				if( mt[ i ].getName().equals( nmMetodoGet ) ) {
//					mtdGet = mt[ i ];
//				} else {
//					if( mt[ i ].getName().equals( nmMetodoIs ) ) {
//						mtdGet = mt[ i ];
//					}
//				}
//			}
//
//			if( mtdGet != null ) return mtdGet;
//		}
//		
//		return mtdGet;
//	}
//	
//	private static String getNmMetodoGet( String campo ) {
//		return "get" + campo.substring( 0, 1 ).toUpperCase() + campo.substring( 1 );
//	}
//
//	private static String getNmMetodoIs( String campo ) {
//		return "is" + campo.substring( 0, 1 ).toUpperCase() + campo.substring( 1 );
//	}
//	
//	public static boolean copiaTabela( TabelaBasica tabelaOrigem, TabelaBasica tabelaDestino ) {
//		try{
//			try{
//				Method[] mt = tabelaDestino.getClass().getDeclaredMethods();
//				
//				for (int i = 0; i < mt.length; i++) {
//					Method met = mt[ i ];
//					if( met.getName().startsWith( "set" ) ){
//						Method method = getMtdGet( tabelaOrigem, met.getName().substring( 3 ) );
//						if( method != null ){
//							Object vlCampo = method.invoke(tabelaOrigem, (Object[]) null  );
//							met.invoke(tabelaDestino, vlCampo );
//						}
//					}
//				}
//			}catch (Exception e) {}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return true;
//	}
//	
//	public void selecionaBeneficiario( PreCadastroCbo pre ) throws QtSQLException {
//		beneficiarioSelecionado = new DadosBeneficiarioSelecionado();
//		
//		try {
//			
//			Beneficiario b = new Beneficiario();
//			if( pre != null ) {
//
//				copiaTabela( pre, b );
//
//				b.setIdCidadeResidencia( pre.getIdCidade() );
//				b.setCdEstadoRg( pre.getCdEstado() );
//				b.setCdEstadoResidencia( pre.getCdEstado() );
//				b.setDsTelefone( pre.getDsTelefoneContato() );
//				b.setCdCpf( pre.getNrCpf() );
//				b.setCdPisPasep( pre.getNrPis() );
//				b.setIdLotacao( pre.getIdLotacao() );
//
//				try {
//					b.setDsNomeAbreviado( BibliotecaBeneficiario.abreviaNomeBeneficiario( pre.getDsNome(), true ) );
//				} catch( Exception e ) {
//					b.setDsNomeAbreviado( pre.getDsNome().substring( 0, 25 ) );
//				}
//				b.setDsNomeDaMae( pre.getDsNomeDaMae() );
//				b.setCdBeneficiarioCartao( b.getCdEmpresa() );
//				b.setCdEmpresa( null );
//				b.setIdEmpresa( null );
//				if( b.getCdUnimed() == null || b.getCdUnimed().trim().equals( "" ) ){
//					b.setCdUnimed( ParametrosCliente.getCodigoCliente() );
//				}
//				
//				if( b.getCdUnimed() == null || b.getCdUnimed().trim().equals( "" ) ){
//					b.setCdUnimed( ParametrosSistema.getInstance().getCodigoDoCliente() );
//				}
//				
//				beneficiarioSelecionado.setBeneficiario(b);
//				
//				BeneficiarioCbo cbo = new BeneficiarioCbo();
//				
//				cbo.setCdCbo( pre.getCdCbo() );
//				cbo.setDsAtividade( pre.getDsAtividade() );
//				cbo.setIdFuncao( pre.getIdFuncao() );
//				cbo.setIdSetor( pre.getIdSetor() );
//				cbo.setIdCargo( pre.getIdCargo() );
//				cbo.setVlAltura( pre.getVlAltura() );
//				cbo.setVlPeso( pre.getVlPeso() );
//				cbo.setNrCtps( pre.getNrCtps() );
//				cbo.setNrSerieUf( pre.getNrSerieUf() );
//				cbo.setDtAdmissao( pre.getDtAdmissao() );
//				cbo.setSnPossuiDeficiencia( pre.isSnPossuiDeficiencia() ? "S" : "N" );
//				
//				beneficiarioSelecionado.setBeneficiarioCbo(cbo);
//				
//				if( beneficiarioSelecionado.getBeneficiarioCbo() != null) {
//					if( beneficiarioSelecionado.getBeneficiarioCbo().getCdCbo() != null) {
//						dsCbo = CboLocalizador.buscaCbo( beneficiarioSelecionado.getBeneficiarioCbo().getCdCbo() ).getNmCbo();
//					} 
//					if( beneficiarioSelecionado.getBeneficiarioCbo().getIdFuncao() != null) {
//						dsFuncao = FuncaoLocalizador.buscaFuncao( beneficiarioSelecionado.getBeneficiarioCbo().getIdFuncao() ).getDsFuncao();
//					}
//					if( beneficiarioSelecionado.getBeneficiarioCbo().getIdFuncaoAnterior() != null) {
//						dsFuncaoAnterior = FuncaoLocalizador.buscaFuncao( beneficiarioSelecionado.getBeneficiarioCbo().getIdFuncaoAnterior() ).getDsFuncao();
//					}
//					if( beneficiarioSelecionado.getBeneficiarioCbo().getIdCargo() != null) {
//						dsCargo = CargoLocalizador.buscaCargo( beneficiarioSelecionado.getBeneficiarioCbo().getIdCargo() ).getDsCargo();
//					}
//					if( beneficiarioSelecionado.getBeneficiarioCbo().getIdSetor() != null) {
//						dsSetor = SetorLocalizador.buscaSetor( beneficiarioSelecionado.getBeneficiarioCbo().getIdSetor() ).getDsSetor();
//					}
//				}
//				
//				FiltroDeNavegador filtro = new FiltroDeNavegador();
//				filtro.adicionaCondicao( "cd_beneficiario_cartao", FiltroDeNavegador.IGUAL, beneficiarioSelecionado.getBeneficiario().getCdBeneficiarioCartao() );
//				filtro.setOrdemDeNavegacao( RiscoBeneficiarioCaminhador.POR_ORDEM_DEFAULT );
//				
//				RiscoBeneficiarioCaminhador riscos = new RiscoBeneficiarioCaminhador( filtro );
//				
//				riscos.ativaCaminhador();
//				beneficiarioSelecionado.getRiscos().clear();
//				
//				if( !riscos.isEmpty() ) {
//					while( !riscos.isEof() ) {
//						beneficiarioSelecionado.getRiscos().add(RiscoLocalizador.buscaRisco(riscos.getRiscoBeneficiario().getIdRisco()));
//						riscos.proximo();
//					}
//				}
//
//				FiltroDeNavegador filtroExames = new FiltroDeNavegador();
//				filtroExames.adicionaCondicao("cd_beneficiario_cartao", FiltroDeNavegador.IGUAL, beneficiarioSelecionado.getBeneficiario().getCdBeneficiarioCartao());
//				filtroExames.setOrdemDeNavegacao(ExameBeneficiarioCaminhador.POR_ORDEM_DEFAULT);
//
//				ExameBeneficiarioCaminhador exames = new ExameBeneficiarioCaminhador(filtro);
//				exames.ativaCaminhador();
//				beneficiarioSelecionado.getExames().clear();
//
//				if (!exames.isEmpty()) {
//					while (!exames.isEof()) {
//						beneficiarioSelecionado.getExames()
//								.add(ExameBeneficiarioLocalizador.buscaExameBeneficiario(
//										exames.getExameBeneficiario().getCdBeneficiarioCartao(),
//										exames.getExameBeneficiario().getCdProcedimento()));
//						exames.proximo();
//					}
//				}
//				
//				
//			}
//			
//		} catch( Exception e ) {
//			e.printStackTrace();
//			msgExibida = 0;
//			msgAoUsuario = "Ops! " + e.getMessage();
//		}
//		
//		
//		
//		Util.sendRedirect("resources/page/mntBeneficiario.jsf");
//	}
//
//	public void inic() {
//		if (msgAoUsuario != null) {
//			if (msgExibida > 0) {
//				msgAoUsuario = null;
//				msgExibida = 0;
//			}
//			msgExibida++;
//		}
//	}
//
//	public void novoRegistro() {
//		beneficiarioSelecionado = new DadosBeneficiarioSelecionado();
//		dsCbo = "";
//		dsFuncao = "";
//		dsCargo = "";
//		dsSetor = "";
//		dsFuncaoAnterior = "";
//		filtraPorNome = "";
//		filtraPorCartao = "";
//	}
//	
//	public String getNmLotacao( Beneficiario benef ) {
//		if( benef != null ) {
//			if( benef.getIdLotacao() != null && benef.getCdUnimed() != null && benef.getIdEmpresa() != null ) {
//				try {
//					ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
//					try {
//						
//						Lotacao lotacao = LotacaoLocalizador.buscaLotacao(cnx, benef.getCdUnimed(), benef.getIdEmpresa(), benef.getIdLotacao() ); 
//								
//						if (lotacao != null) {
//							return lotacao.getDsLotacao();
//						}
//					} finally {
//						cnx.libera();
//					}
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		}
//		return null;
//	}
//	
//	public String getGrupoCarencia( Integer idGrupo ) {
//		
//		if( idGrupo != null ) {
//			try {
//				ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
//				try {
//					GrupoDeCarencia servico = GrupoDeCarenciaLocalizador.buscaGrupoDeCarencia(cnx, idGrupo);
//					if (servico != null) {
//						return servico.getDsGrupoCarencia();
//					}
//	
//				} finally {
//					cnx.libera();
//				}
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//		}
//		return null;
//	}
//
//	public String getNomeProcedimento(String cdProcedimento) {
//
//		try {
//			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
//			try {
//				Servico servico = ServicoLocalizador.buscaServico(cnx, cdProcedimento);
//				if (servico != null) {
//					return servico.getDsServico();
//				}
//
//			} finally {
//				cnx.libera();
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return null;
//	}
//	
//	public String getNomeEstado(String cdEstado) {
//
//		try {
//			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
//			try {
//				Estado estado = EstadoLocalizador.buscaEstado(cnx, cdEstado);
//				if (estado != null) {
//					return estado.getDsEstado();
//				}
//
//			} finally {
//				cnx.libera();
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return null;
//	}
//	
//	public String getNomeCidade(String cdEstado, Integer idCidade) {
//
//		try {
//			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
//			try {
//				Cidade cidade = CidadeLocalizador.buscaCidade(cnx, cdEstado, idCidade);
//				if (cidade != null) {
//					return cidade.getDsCidade();
//				}
//
//			} finally {
//				cnx.libera();
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return null;
//	}
//	
//	public String getNomeEmpresa(String cdUnimed, Integer idEmpresa ) {
//
//		try {
//			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
//			try {
//				Empresa  empresa = EmpresaLocalizador.buscaEmpresa(cnx, cdUnimed, idEmpresa);
//				
//				if (empresa != null) {
//					return empresa.getDsRazaoSocial();
//				}
//
//			} finally {
//				cnx.libera();
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return null;
//	}
//	
//	public String getDependencia(String cdDependencia ) {
//
//		try {
//			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
//			try {
//				Dependencia dependencia = DependenciaLocalizador.buscaDependencia(cnx, cdDependencia);
//				
//				if (dependencia != null) {
//					return dependencia.getDsDependencia();
//				}
//
//			} finally {
//				cnx.libera();
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return null;
//	}
//	
//	public int getIdade( QtData data ) {
//
//		if( data != null ) {
//			try {
//				return QtData.idade(data);
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//		}
//		return 0;
//	}
//
//	public String getMsgAoUsuario() {
//		return msgAoUsuario;
//	}
//
//	public void setMsgAoUsuario(String msgAoUsuario) {
//		this.msgAoUsuario = msgAoUsuario;
//	}
//
//	public Integer getMsgExibida() {
//		return msgExibida;
//	}
//
//	public void setMsgExibida(Integer msgExibida) {
//		this.msgExibida = msgExibida;
//	}
//
//	public DadosBeneficiarioSelecionado getBeneficiarioSelecionado() {
//		return beneficiarioSelecionado;
//	}
//
//	public void setBeneficiarioSelecionado(DadosBeneficiarioSelecionado beneficiarioSelecionado) {
//		this.beneficiarioSelecionado = beneficiarioSelecionado;
//	}
//
//	public String getDsCbo() {
//		return dsCbo;
//	}
//
//	public void setDsCbo(String dsCbo) {
//		this.dsCbo = dsCbo;
//	}
//
//	public String getDsFuncao() {
//		return dsFuncao;
//	}
//
//	public void setDsFuncao(String dsFuncao) {
//		this.dsFuncao = dsFuncao;
//	}
//
//	public String getDsCargo() {
//		return dsCargo;
//	}
//
//	public void setDsCargo(String dsCargo) {
//		this.dsCargo = dsCargo;
//	}
//
//	public String getDsSetor() {
//		return dsSetor;
//	}
//
//	public void setDsSetor(String dsSetor) {
//		this.dsSetor = dsSetor;
//	}
//
//	public String getDsFuncaoAnterior() {
//		return dsFuncaoAnterior;
//	}
//
//	public void setDsFuncaoAnterior(String dsFuncaoAnterior) {
//		this.dsFuncaoAnterior = dsFuncaoAnterior;
//	}
//
//	public String getFiltraPorNome() {
//		return filtraPorNome;
//	}
//
//	public void setFiltraPorNome(String filtraPorNome) {
//		this.filtraPorNome = filtraPorNome;
//	}
//
//	public String getFiltraPorCartao() {
//		return filtraPorCartao;
//	}
//
//	public void setFiltraPorCartao(String filtraPorCartao) {
//		this.filtraPorCartao = filtraPorCartao;
//	}

}
