package jUni.bean;


import java.io.IOException;
import java.io.OutputStream;
import java.sql.SQLException;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;

import jPcmso.persistencia.geral.b.BeneficiarioCbo;
import jPcmso.persistencia.geral.b.BeneficiarioCboCaminhador;
import jPcmso.persistencia.geral.b.BeneficiarioCboLocalizador;
import jPcmso.persistencia.geral.e.Empresa;
import jPcmso.persistencia.geral.e.EmpresaLocalizador;
import jPcmso.persistencia.geral.f.FiliaisEmpresa;
import jPcmso.persistencia.geral.f.FiliaisEmpresaLocalizador;
import jPcmso.persistencia.geral.f.Funcao;
import jPcmso.persistencia.geral.f.FuncaoLocalizador;
import jPcmso.persistencia.geral.s.Setor;
import jPcmso.persistencia.geral.s.SetorLocalizador;
import jUni.bean.dados.ConfiguracaoBeneficiarios;
import jUni.bean.dados.DadosBeneficiarios;
import jUni.persistencia.geral.b.Beneficiario;
import jUni.persistencia.geral.b.BeneficiarioCaminhador;
import jUni.persistencia.geral.c.Cidade;
import jUni.persistencia.geral.c.CidadeLocalizador;
import jUni.util.Util;
import quatro.persistencia.FiltroDeNavegador;
import quatro.sql.Conexao;
import quatro.sql.ConexaoParaPoolDeConexoes;
import quatro.sql.PoolDeConexoes;
import quatro.sql.QtSQLException;
import quatro.util.ParametrosSistema;
import quatro.util.QtData;

@SessionScoped
@ManagedBean
public class BeanExportacaoBeneficiarioPCMSO {

	private String msgAoUsuario;
	private Integer msgExibida = 0;
	private String dtInicial;
	private String dtFinal;

	public BeanExportacaoBeneficiarioPCMSO() {
		dtInicial = new QtData().toString();
		dtFinal = new QtData().toString();
	}

	public void inic() {
		if (msgAoUsuario != null) {
			if (msgExibida > 0) {
				msgAoUsuario = null;
				msgExibida = 0;
			}
			msgExibida++;
		}
	}

	public void gerarRelatorio() {
		try {
			executaDownloadArquivoExportacao();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void executaDownloadArquivoExportacao() throws SQLException {

		FacesContext fc = FacesContext.getCurrentInstance();
		try {

			HttpServletResponse response = ((HttpServletResponse) fc.getExternalContext().getResponse());
			fc.responseComplete();
			response.setContentType("text/plain");

			String data = new QtData().toString();

			response.setHeader("Content-Disposition", "attachment;filename=RelatorioBeneficiarioPCMSO_" + data + ".txt");
			OutputStream respOs = response.getOutputStream();

			respOs.write( preencheArquivo().getBytes("Cp1252") );

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private static String getCnpjFilial(Conexao cnx, String empresa, Integer idFilial) throws QtSQLException {
		FiliaisEmpresa filial = FiliaisEmpresaLocalizador.buscaFiliaisEmpresa(cnx, empresa, idFilial );
		if( filial != null ) {
			if( filial.getNrCnpj() != null ) {
				return filial.getNrCnpj();
			}
		}
		return "";
	}

	private String preencheArquivo() {
		StringBuilder sb = new StringBuilder();

		ConfiguracaoBeneficiarios config = new ConfiguracaoBeneficiarios();
		config.setIdentificacao(1);
		config.setExistenciaSetorCargo(1);
		config.setExistenciaEmpregado(1);

		sb.append(ConfiguracaoBeneficiarios.getLinha(config) + "\n");

		try {
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
			try {
				FiltroDeNavegador filtro = new FiltroDeNavegador();
				if (dtInicial != null && !dtInicial.trim().equals("")) {
					filtro.adicionaCondicao("dt_inclusao", FiltroDeNavegador.MAIOR_OU_IGUAL, new QtData(dtInicial));
				}
				if (dtFinal != null && dtFinal.trim().equals("")) {
					filtro.adicionaCondicao("dt_inclusao", FiltroDeNavegador.MENOR_OU_IGUAL, new QtData(dtFinal));
				}
				filtro.adicionaCondicao("cd_empresa", FiltroDeNavegador.MAIOR_OU_IGUAL, "8000");
				filtro.adicionaCondicao("cd_empresa", FiltroDeNavegador.MENOR_OU_IGUAL, "9000");
				
				BeneficiarioCaminhador benefCaminhador = new BeneficiarioCaminhador(cnx, filtro);
				benefCaminhador.setOrdemDeNavegacao(BeneficiarioCboCaminhador.POR_ORDEM_DEFAULT);
				benefCaminhador.ativaCaminhador();

				if (!benefCaminhador.isEmpty()) {
					
					boolean duplicaRegistro = false;
					while (!benefCaminhador.isEof()) {
						BeneficiarioCbo benefCBO = BeneficiarioCboLocalizador.buscaBeneficiarioCbo(cnx, benefCaminhador.getBeneficiario().getCdBeneficiarioCartao() );
						Beneficiario beneficiario = benefCaminhador.getBeneficiario();
						
						jUni.persistencia.geral.e.Empresa empresa = jUni.persistencia.geral.e.EmpresaLocalizador.buscaEmpresa(cnx, ParametrosSistema.getInstance().getCodigoDoCliente(), new Integer( benefCaminhador.getBeneficiario().getCdEmpresa() ) );
						
						if( empresa != null && empresa.getDtExclusao() == null ) {
													
							DadosBeneficiarios benef = new DadosBeneficiarios();
							benef.setIdentificacao("02");
							
							benef.setNrCnpj(getCnpjEmpresa(cnx, benefCBO));
							
							benef.setCdIntegracaoEmpresa("");
							if( beneficiario != null && beneficiario.getIdFilial() != null ) {
								benef.setCdIntegracaoEmpresa( getCnpjFilial( cnx, beneficiario.getCdEmpresa(), beneficiario.getIdFilial() ) );
							} 
							
							benef.setUfCtps("RS");
							benef.setDtEmissaoIdentidade("");
							benef.setIdVinculo(2);
							benef.setDsRegimeRevezamento("");
							benef.setDsObservacao("");
							benef.setVlRemuneracaoMensal("");
							benef.setDsFiliacaoPrevidencia(1);
							benef.setIdAposentado(2);
							benef.setCdTituloEleitor("");
							benef.setCdCNH("");
							benef.setDtValidadeCNH("");
							benef.setDtInicioSetor("19/02/2019");
							benef.setDtSaidaSetor("");
							benef.setDsCargoDesenvolvido("");
							benef.setCdRFID("");
							benef.setCdBarras("");
							benef.setDsTurno("");
							benef.setCdTurnoJornada("");
							benef.setDsGrupoSanguineo("");
							
							if( benefCBO != null ) {
								
								if( benefCBO.getIdFilial() != null ) {
									FiliaisEmpresa filial = FiliaisEmpresaLocalizador.buscaFiliaisEmpresa(cnx, benefCBO.getCdEmpresa(), benefCBO.getIdFilial() );
									if( filial != null ) {
										if( filial.getNrCnpj() != null ) {
											if( filial.getIdFilial() > 1 ) {
												benef.setNrCnpj( filial.getNrCnpj() );
											}
										}
										
									}
								}
								
								benef.setDsNome( benefCBO.getDsNome() );
								benef.setDtNascimento( benefCBO.getDtNascimento() != null ? benefCBO.getDtNascimento().toString() : "");
								benef.setDsSexo(benefCBO.getTpSexo());
								benef.setCdCtps(benefCBO.getNrCtps());
								benef.setDsSerieCtps(benefCBO.getNrSerieUf());
								benef.setDtEmissaoCtps( benefCBO.getDtInclusao() != null ? benefCBO.getDtInclusao().toString() : "");
								benef.setNrIdentidade(benefCBO.getCdRg());
								benef.setCdNit(benefCBO.getNrPis());
								benef.setNrCpf(benefCBO.getNrCpf());
								benef.setCdMatricula( benefCBO.getCdBeneficiarioCartao() + beneficiario.getCdDigito() );
								benef.setCdBrPhd(benefCBO.isSnPossuiDeficiencia() ? "PDH" : "NA");
								benef.setDtAdmissao(benefCBO.getDtAdmissao() != null ? benefCBO.getDtAdmissao().toString() : benefCBO.getDtInclusao().toString() );
								benef.setDtDemissao( benefCBO.getDtExclusao() != null ? benefCBO.getDtExclusao().toString() : "" );
								if( benefCBO.getDtExclusao() == null ) {
									benef.setDtDemissao( beneficiario.getDtExclusao() != null ? beneficiario.getDtExclusao().toString() : "" );
								}
								if (benefCBO.getTpEstadoCivil() != null) {
									if (benefCBO.getTpEstadoCivil().equals("S")) {
										benef.setDsEstadoCivil(1);
									} else if (benefCBO.getTpEstadoCivil().equals("M")) {
										benef.setDsEstadoCivil(2);
									} else if (benefCBO.getTpEstadoCivil().equals("W")) {
										benef.setDsEstadoCivil(3);
									} else if (benefCBO.getTpEstadoCivil().equals("D")) {
										benef.setDsEstadoCivil(4);
									} else if (benefCBO.getTpEstadoCivil().equals("A")) {
										benef.setDsEstadoCivil(6);
									} else if (benefCBO.getTpEstadoCivil().equals("U")) {
										benef.setDsEstadoCivil(8);
									}
								} else {
									benef.setDsEstadoCivil(6);
								}
								benef.setCdSetor(benefCBO.getIdSetor() != null ? benefCBO.getIdSetor().toString() : "");
		
								if (benefCBO.getIdSetor() != null) {
									Setor setor = SetorLocalizador.buscaSetor(cnx, benefCBO.getIdSetor());
									if (setor != null) {
										benef.setNmSetor(setor.getDsSetor());
									} else {
										benef.setNmSetor("Não Costa");
									}
								} else {
									benef.setNmSetor("Não Consta");
								}
								
								Funcao funcao = null;
								if (benefCBO.getIdFuncao() != null) {
									funcao = FuncaoLocalizador.buscaFuncao(cnx, benefCBO.getIdFuncao() );
								}
								
								if( benefCBO.getIdFuncaoAnterior() != null ) {
									duplicaRegistro = true;								
								}
								
								benef.setCdCargo( benefCBO.getIdFuncao() != null ? benefCBO.getIdFuncao().toString() : "1211");
		
								if (benefCBO.getIdFuncao() != null && funcao != null ) {
									benef.setNmCargo(funcao.getDsFuncao());
								} else {
									benef.setNmCargo("Não Consta");
								}
								if( benefCBO.getCdCbo() != null ) {
									benef.setCdCBOCargo( benefCBO.getCdCbo() );
								} else {
									benef.setCdCBOCargo( "000001" );
								}
								benef.setIdDeficiencia(benefCBO.isSnPossuiDeficiencia() ? 1 : 2);
								benef.setCdFuncao(benefCBO.getIdFuncao() != null ? benefCBO.getIdFuncao().toString() : "1466");
		
								
								
							} else {
								
								benef.setNrCnpj( getCnpjEmpresa(cnx, beneficiario) );
								
								if( beneficiario.getIdFilial() != null ) {
									FiliaisEmpresa filial = FiliaisEmpresaLocalizador.buscaFiliaisEmpresa(cnx, beneficiario.getCdEmpresa(), beneficiario.getIdFilial() );
									if( filial != null ) {
										if( filial.getNrCnpj() != null ) {
											if( filial.getIdFilial() > 1 ) {
												benef.setNrCnpj( filial.getNrCnpj() );
											}
										}
										
									}
								}
								
								benef.setDsNome( beneficiario.getDsNome() );
								benef.setDtNascimento( beneficiario.getDtNascimento() != null ? beneficiario.getDtNascimento().toString() : "");
								benef.setDsSexo(beneficiario.getTpSexo());
								benef.setCdCtps("");
								benef.setDsSerieCtps("");
								benef.setDtEmissaoCtps( beneficiario.getDtInclusao() != null ?beneficiario.getDtInclusao().toString() : "");
								benef.setNrIdentidade(beneficiario.getCdRg());
								benef.setCdNit(beneficiario.getCdPisPasep() );
								benef.setNrCpf(beneficiario.getCdCpf());
								benef.setCdMatricula( beneficiario.getCdBeneficiarioCartao() + beneficiario.getCdDigito() );
								benef.setCdBrPhd( "NA" );
								benef.setDtAdmissao( beneficiario.getDtInclusao() != null ? beneficiario.getDtInclusao().toString() : null );
							
								benef.setDsEstadoCivil(6);
								
								benef.setDsOrgaoEmissor(beneficiario.getDsOrgaoEmissorRg());
								benef.setUfIdentidade(beneficiario.getCdEstadoRg());
								if( beneficiario.getDtExclusao() != null ) {
									benef.setDtDemissao( beneficiario.getDtExclusao().toString() );
								}
								
								benef.setDsEndereco( beneficiario.getDsLogradouro() );
								benef.setDsEstado(beneficiario.getCdEstadoResidencia());
								if( beneficiario.getCdEstadoResidencia() != null && beneficiario.getIdCidadeResidencia() != null ) {
								Cidade cidade = CidadeLocalizador.buscaCidade(cnx, beneficiario.getCdEstadoResidencia(), beneficiario.getIdCidadeResidencia() );
									if( cidade != null ) {
										benef.setDsCidade(cidade.getDsCidade());
									}
								}
								benef.setDsBairro(beneficiario.getDsBairro());
								benef.setCdCep(beneficiario.getCdCepResidencia());
								benef.setDsTelefone(beneficiario.getDsTelefone());
								benef.setNmMae(beneficiario.getDsNomeDaMae());
								
								if (beneficiario.getTpEstadoCivil() != null) {
									if (beneficiario.getTpEstadoCivil().equals("S")) {
										benef.setDsEstadoCivil(1);
									} else if (beneficiario.getTpEstadoCivil().equals("M")) {
										benef.setDsEstadoCivil(2);
									} else if (beneficiario.getTpEstadoCivil().equals("W")) {
										benef.setDsEstadoCivil(3);
									} else if (beneficiario.getTpEstadoCivil().equals("D")) {
										benef.setDsEstadoCivil(4);
									} else if (beneficiario.getTpEstadoCivil().equals("A")) {
										benef.setDsEstadoCivil(6);
									} else if (beneficiario.getTpEstadoCivil().equals("U")) {
										benef.setDsEstadoCivil(8);
									}
								} else {
									benef.setDsEstadoCivil(6);
								}
								
								benef.setCdSetor("774");
								benef.setNmSetor("Não Consta");
								benef.setCdCargo("1211");
								benef.setNmCargo("Não Consta");
								benef.setCdCBOCargo( "000001" );
								benef.setIdDeficiencia(2);
								benef.setCdFuncao("1466");
								
							}
							
							
							
							sb.append(DadosBeneficiarios.getLinha(benef) + "\n");
							
							if( duplicaRegistro ) {
								
								benef.setDtInicioSetor("18/02/2019");
								Funcao funcao = FuncaoLocalizador.buscaFuncao(cnx, benefCBO.getIdFuncaoAnterior() );
								benef.setCdCargo( benefCBO.getIdFuncaoAnterior() != null ? benefCBO.getIdFuncaoAnterior().toString() : "1211");
								benef.setCdFuncao(benefCBO.getIdFuncaoAnterior() != null ? benefCBO.getIdFuncaoAnterior().toString() : "1466");
		
								if( funcao != null ) {
									benef.setNmCargo(funcao.getDsFuncao());
								} else {
									benef.setNmCargo("Não Consta");
								}
								
								sb.append(DadosBeneficiarios.getLinha(benef) + "\n");
								duplicaRegistro = false;
							}
						}
						benefCaminhador.proximo();
					}
				}

			} finally {
				cnx.libera();
			}
			msgAoUsuario = "Exportação Concluída.";
			msgExibida = 0;
			
			

		} catch (Exception e) {
			e.printStackTrace();
			msgAoUsuario = "Ops! " + e.getMessage();
			msgExibida = 0;
			Util.sendRedirect("resources/page/exportacaoBeneficiario.jsf");
		}
		return sb.toString();
	}

	private String getCnpjEmpresa(ConexaoParaPoolDeConexoes cnx, BeneficiarioCbo dados ) {

		try {
			if( dados != null ) {
				Empresa empresa = EmpresaLocalizador.buscaEmpresa(cnx, dados.getCdEmpresa());
				
				if (empresa != null) {
					
					String retorno = empresa.getNrCpfRepresentante();
					
					if (empresa.getNrCnpj() != null && !empresa.getNrCnpj().trim().equals("")) {
						retorno = empresa.getNrCnpj();
					}
					
					jUni.persistencia.geral.e.Empresa empGeral = jUni.persistencia.geral.e.EmpresaLocalizador.buscaPorCodigosDeEmpresa( cnx,  new Integer( dados.getCdEmpresa() ) );
					if( empGeral != null && empGeral.getNrCeiSefip() != null && !empGeral.getNrCeiSefip().equals( "" ) ) {
						retorno = empGeral.getNrCeiSefip();
					}
					
					return retorno;
				}
			} else {
				return "";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}
	
	private String getCnpjEmpresa(ConexaoParaPoolDeConexoes cnx, Beneficiario dados ) {

		try {
			if( dados != null ) {
				Empresa empresa = EmpresaLocalizador.buscaEmpresa( cnx, dados.getCdEmpresa() );
				if (empresa != null) {
					if (empresa.getNrCnpj() != null && !empresa.getNrCnpj().trim().equals("")) {
						return empresa.getNrCnpj();
					} else {
						return empresa.getNrCpfRepresentante();
					}
				}
			} else {
				return "";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public String getDtInicial() {
		return dtInicial;
	}

	public void setDtInicial(String dtInicial) {
		this.dtInicial = dtInicial;
	}

	public String getDtFinal() {
		return dtFinal;
	}

	public void setDtFinal(String dtFinal) {
		this.dtFinal = dtFinal;
	}

	
	public String getMsgAoUsuario() {
		return msgAoUsuario;
	}

	
	public void setMsgAoUsuario( String msgAoUsuario ) {
		this.msgAoUsuario = msgAoUsuario;
	}
	
	

}
