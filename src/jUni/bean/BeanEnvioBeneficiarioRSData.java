package jUni.bean;

import java.util.LinkedList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import jPcmso.persistencia.geral.b.BeneficiarioCbo;
import jPcmso.persistencia.geral.b.BeneficiarioCboLocalizador;
import jPcmso.persistencia.geral.c.Cargo;
import jPcmso.persistencia.geral.c.CargoLocalizador;
import jPcmso.persistencia.geral.c.Cidade;
import jPcmso.persistencia.geral.c.CidadeLocalizador;
import jPcmso.persistencia.geral.e.Empresa;
import jPcmso.persistencia.geral.e.EmpresaLocalizador;
import jPcmso.persistencia.geral.m.MensagemEnviada;
import jPcmso.persistencia.geral.m.MensagemParametro;
import jPcmso.persistencia.geral.p.PreCadastroCbo;
import jPcmso.persistencia.geral.p.PreCadastroCboLocalizador;
import jPcmso.persistencia.geral.s.Setor;
import jPcmso.persistencia.geral.s.SetorLocalizador;
import jUni.bean.dados.DadosListaBeneficiarioCbo;
import jUni.bean.wsrsdata.TransacaoRSData;
import jUni.biblioteca.util.Util;

import quatro.negocio.ErroDeNegocio;
import quatro.sql.Conexao;
import quatro.sql.ConexaoParaPoolDeConexoes;
import quatro.sql.PoolDeConexoes;
import quatro.sql.Query;
import quatro.util.QtData;

@SessionScoped
@ManagedBean
public class BeanEnvioBeneficiarioRSData {

	private String msgAoUsuario;
	private Integer msgExibida = 0;

	private String filtraPorCodigo;
	private String filtraPorNome;
	private String filtraPorEmpresa;

	private PreCadastroCbo preCadastroCboSelecionado;

	private List<DadosListaBeneficiarioCbo> listagemPreCadastro;

	private static MensagemParametro mensagemParametro;
	private MensagemEnviada mensagemEnviada;

	public void inic() {
		if (msgAoUsuario != null) {
			if (msgExibida > 0) {
				msgAoUsuario = null;
				msgExibida = 0;
			}
			msgExibida++;
		}
	}

	public BeanEnvioBeneficiarioRSData() {
		listagemPreCadastro = new LinkedList<>();
		listagemPreCadastro = listaPreCadastro();
	}

	public void reenviarTransacao(DadosListaBeneficiarioCbo dados) {
		try {
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
			try {
				TransacaoRSData.enviaRSData(cnx, dados.getBeneficiarioCbo());
				listagemPreCadastro.clear();;
				listagemPreCadastro = listaPreCadastro();
			} finally {
				cnx.libera();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void executaPesquisa() {
		listagemPreCadastro.clear();
		listagemPreCadastro = listaPreCadastro();
	}
	
	public List<DadosListaBeneficiarioCbo> listaPreCadastro() {
		List<DadosListaBeneficiarioCbo> lista = new LinkedList<>();

		try {
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

			try {

				Query q = new Query(cnx);
				q.addSQL("select * from pcmso.beneficiarios_cbo ");
				q.addSQL("where dt_inclusao >= '2018-01-01'");
				q.addSQL("and dt_exclusao is null");
				if (filtraPorCodigo != null && !filtraPorCodigo.trim().equals("")) {
					q.addSQL("and cd_beneficiario_cartao ilike '%" + filtraPorCodigo.trim() + "%'");
				}
				if (filtraPorNome != null && !filtraPorNome.trim().equals("")) {
					q.addSQL("and ds_nome ilike '%" + filtraPorNome.trim() + "%'");
				}
				if (filtraPorEmpresa != null && !filtraPorEmpresa.trim().equals("")) {
					q.addSQL("and cd_empresa = '" + filtraPorEmpresa.trim() + "'");
				}

				q.addSQL(" order by dt_inclusao desc");

				q.executeQuery();

				if (!q.isEmpty()) {
					while (!q.isAfterLast()) {

						BeneficiarioCbo beneficiarioCbo = new BeneficiarioCbo();
						BeneficiarioCboLocalizador.buscaCampos(beneficiarioCbo, q);

						DadosListaBeneficiarioCbo dados = new DadosListaBeneficiarioCbo();
						dados.setBeneficiarioCbo(beneficiarioCbo);
						dados.setCdMsgRetorno(q.getString("cd_msg_retorno"));
						dados.setDsTxtRetorno(q.getString("ds_txt_retorno"));
						if (q.getTimestamp("dt_envio_rsdata") != null) {
							dados.setDtEnvioRsdata(new QtData(q.getTimestamp("dt_envio_rsdata")));
						}
						lista.add(dados);

						q.next();
					}
				}

			} finally {
				cnx.libera();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return lista;
	}

	public PreCadastroCbo getPreCadastroCboSelecionado() {
		return preCadastroCboSelecionado;
	}

	public void setPreCadastroCboSelecionado(PreCadastroCbo preCadastroCboSelecionado) {
		this.preCadastroCboSelecionado = preCadastroCboSelecionado;
	}

	public List<DadosListaBeneficiarioCbo> getListagemPreCadastro() {
		return listagemPreCadastro;
	}

	public void setListagemPreCadastro(List<DadosListaBeneficiarioCbo> listagemPreCadastro) {
		this.listagemPreCadastro = listagemPreCadastro;
	}

	public static MensagemParametro getMensagemParametro() {
		return mensagemParametro;
	}

	public static void setMensagemParametro(MensagemParametro mensagemParametro) {
		BeanEnvioBeneficiarioRSData.mensagemParametro = mensagemParametro;
	}

	public MensagemEnviada getMensagemEnviada() {
		return mensagemEnviada;
	}

	public void setMensagemEnviada(MensagemEnviada mensagemEnviada) {
		this.mensagemEnviada = mensagemEnviada;
	}

	public String getMsgAoUsuario() {
		return msgAoUsuario;
	}

	public void setMsgAoUsuario(String msgAoUsuario) {
		this.msgAoUsuario = msgAoUsuario;
	}

	public Integer getMsgExibida() {
		return msgExibida;
	}

	public void setMsgExibida(Integer msgExibida) {
		this.msgExibida = msgExibida;
	}

	public String getFiltraPorCodigo() {
		return filtraPorCodigo;
	}

	public void setFiltraPorCodigo(String filtraPorCodigo) {
		this.filtraPorCodigo = filtraPorCodigo;
	}

	public String getFiltraPorNome() {
		return filtraPorNome;
	}

	public void setFiltraPorNome(String filtraPorNome) {
		this.filtraPorNome = filtraPorNome;
	}

	public String getFiltraPorEmpresa() {
		return filtraPorEmpresa;
	}

	public void setFiltraPorEmpresa(String filtraPorEmpresa) {
		this.filtraPorEmpresa = filtraPorEmpresa;
	}

}
