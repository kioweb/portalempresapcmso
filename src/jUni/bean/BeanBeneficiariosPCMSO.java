package jUni.bean;

import java.lang.reflect.Method;
import java.util.LinkedList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import jPcmso.biblioteca.mensageiro.Email;
import jPcmso.biblioteca.mensageiro.Mailer;
import jPcmso.biblioteca.negocio.BibliotecaPreCadastro;
import jPcmso.negocio.geral.b.BeneficiarioCboNegocio;
import jPcmso.negocio.geral.e.ExameBeneficiarioNegocio;
import jPcmso.negocio.geral.r.RiscoBeneficiarioNegocio;
import jPcmso.negocio.geral.r.RiscoNegocio;
import jPcmso.persistencia.geral.b.BeneficiarioCbo;
import jPcmso.persistencia.geral.b.BeneficiarioCboLocalizador;
import jPcmso.persistencia.geral.c.Cargo;
import jPcmso.persistencia.geral.c.CargoCaminhador;
import jPcmso.persistencia.geral.c.CargoLocalizador;
import jPcmso.persistencia.geral.c.Cbo;
import jPcmso.persistencia.geral.c.CboCaminhador;
import jPcmso.persistencia.geral.c.CboLocalizador;
import jPcmso.persistencia.geral.e.ExameBeneficiario;
import jPcmso.persistencia.geral.e.ExameBeneficiarioCaminhador;
import jPcmso.persistencia.geral.e.ExameBeneficiarioLocalizador;
import jPcmso.persistencia.geral.f.FiliaisEmpresa;
import jPcmso.persistencia.geral.f.Funcao;
import jPcmso.persistencia.geral.f.FuncaoCaminhador;
import jPcmso.persistencia.geral.f.FuncaoLocalizador;
import jPcmso.persistencia.geral.m.MensagemEnviada;
import jPcmso.persistencia.geral.m.MensagemParametro;
import jPcmso.persistencia.geral.m.MensagemParametroLocalizador;
import jPcmso.persistencia.geral.p.PreCadastroCbo;
import jPcmso.persistencia.geral.r.Risco;
import jPcmso.persistencia.geral.r.RiscoBeneficiario;
import jPcmso.persistencia.geral.r.RiscoBeneficiarioCaminhador;
import jPcmso.persistencia.geral.r.RiscoLocalizador;
import jPcmso.persistencia.geral.s.Setor;
import jPcmso.persistencia.geral.s.SetorCaminhador;
import jPcmso.persistencia.geral.s.SetorLocalizador;
import jUni.biblioteca.conversao.Inteiro;
import jUni.biblioteca.negocio.BibliotecaBeneficiario;
import jUni.biblioteca.parametros.ParametrosCliente;
import jUni.controleLogin.Usuario;
import jUni.negocio.geral.b.BeneficiarioNegocio;
import jUni.persistencia.geral.b.Beneficiario;
import jUni.persistencia.geral.b.BeneficiarioCaminhador;
import jUni.persistencia.geral.c.Cidade;
import jUni.persistencia.geral.c.CidadeCaminhador;
import jUni.persistencia.geral.c.CidadeLocalizador;
import jUni.persistencia.geral.d.Dependencia;
import jUni.persistencia.geral.d.DependenciaLocalizador;
import jUni.persistencia.geral.e.Empresa;
import jUni.persistencia.geral.e.EmpresaLocalizador;
import jUni.persistencia.geral.e.Estado;
import jUni.persistencia.geral.e.EstadoCaminhador;
import jUni.persistencia.geral.e.EstadoLocalizador;
import jUni.persistencia.geral.g.GrupoDeCarencia;
import jUni.persistencia.geral.g.GrupoDeCarenciaLocalizador;
import jUni.persistencia.geral.l.Lotacao;
import jUni.persistencia.geral.l.LotacaoCaminhador;
import jUni.persistencia.geral.l.LotacaoLocalizador;
import jUni.persistencia.geral.p.PaisAns;
import jUni.persistencia.geral.p.PaisAnsCaminhador;
import jUni.persistencia.geral.s.Servico;
import jUni.persistencia.geral.s.ServicoLocalizador;
import jUni.util.Util;
import quatro.persistencia.FiltroDeNavegador;
import quatro.persistencia.TabelaBasica;
import quatro.sql.Conexao;
import quatro.sql.ConexaoParaPoolDeConexoes;
import quatro.sql.PoolDeConexoes;
import quatro.sql.QtSQLException;
import quatro.util.ParametrosSistema;
import quatro.util.QtData;

@SessionScoped
@ManagedBean
public class BeanBeneficiariosPCMSO {

	private Usuario usuarioLogado;
	private DadosBeneficiarioSelecionado beneficiarioSelecionado;
	private String msgAoUsuario;
	private String dsCbo;
	private String dsFuncao;
	private String dsCargo;
	private String dsSetor;
	private String dsFuncaoAnterior;
	private Integer msgExibida = 0;
	private String filtraPorNome;
	private String filtraPorCartao;
	private RiscoBeneficiario riscoEdicao;
	private ExameBeneficiario exameEdicao;
	private ExameBeneficiario exameNovo;

	private String snPossuiDeficienciaCbo;
	private String snTrabalhoEmAltura;
	private String snEspacoConfinado;
	private String snFitossanitarios;

	private Integer idEmpresaRelacioanda;

	private String dsLotacaoPesquisa;
	private String idLotacaoPesquisa;

	private Integer idLotacaoRelExame;
	private String dsNomeLotacao;
	private Integer idLotacaoFinalRelExame;
	private String dsNomeLotacaoFinal;

	private List<Beneficiario> listagem;
	private List<Cidade> listaCidades;
	private List<PaisAns> listaPais;
	private List<Estado> listaEstados;
	private List<FiliaisEmpresa> listaFiliais;
	private List<Cbo> listaCbo;
	private List<Setor> listaSetor;
	private List<Funcao> listaFuncao;
	private List<Cargo> listaCargo;
	private List<Lotacao> listaLotacao;

	private static MensagemParametro mensagemParametro;
	private MensagemEnviada mensagemEnviada;

	private static void initMensagemParametro(Conexao cnx) throws QtSQLException {
		if (mensagemParametro == null) {
			mensagemParametro = MensagemParametroLocalizador.buscaMensagemParametro(cnx, 1);
		}
	}

	public BeanBeneficiariosPCMSO() {
		beneficiarioSelecionado = new DadosBeneficiarioSelecionado();
		listaCidades = new LinkedList<>();
		listagem = new LinkedList<>();
		listagem = getLista();
		listaCidades = new LinkedList<>();
		listaCbo = new LinkedList<>();
		listaSetor = new LinkedList<>();
		listaFuncao = new LinkedList<>();
		listaCargo = new LinkedList<>();
		listaLotacao = new LinkedList<>();
		listaPais = new LinkedList<>();
		listaEstados = new LinkedList<>();
		listaFiliais = new LinkedList<>();

		try {
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
			try {
				// listaFiliais = carregaFiliais(cnx, idEmpresaRelacioanda);
				listaPais = carregaListaPaises(cnx);
				listaEstados = carregaListaEstados(cnx);

				listaCbo = carregaListaCbo(cnx);
				listaSetor = carregaListaSetor(cnx);
				listaFuncao = carregaListaFuncao(cnx);
				listaCargo = carregaListaCargo(cnx);
				listaLotacao = carregaLotacao(cnx);
			} finally {
				cnx.libera();
			}
		} catch (Exception e) {
			e.printStackTrace();
			msgExibida = 0;
			msgAoUsuario = "Ops! " + e.getMessage();
		}
	}

	private List<Lotacao> carregaLotacao(ConexaoParaPoolDeConexoes cnx) throws QtSQLException {
		List<Lotacao> lista = new LinkedList<>();

		listaLotacao.clear();

		FiltroDeNavegador filtro = new FiltroDeNavegador();
		filtro.adicionaCondicao("cd_unimed", FiltroDeNavegador.IGUAL,
				ParametrosSistema.getInstance().getCodigoDoCliente());
		if (idLotacaoPesquisa != null && !idLotacaoPesquisa.equals("")) {
			filtro.adicionaCondicao("id_lotacao", FiltroDeNavegador.IGUAL, new Integer(idLotacaoPesquisa));
		}
		if (dsLotacaoPesquisa != null && !dsLotacaoPesquisa.equals("")) {
			filtro.adicionaCondicao("ds_lotacao", FiltroDeNavegador.ILIKE,
					"%" + dsLotacaoPesquisa.replaceAll(" ", "%") + "%");
		}
		if (idEmpresaRelacioanda != null) {
			filtro.adicionaCondicao("id_empresa", FiltroDeNavegador.IGUAL, new Integer(idEmpresaRelacioanda));
		}
		filtro.setOrdemDeNavegacao(LotacaoCaminhador.POR_DESCRICAO);

		LotacaoCaminhador lotacoes = new LotacaoCaminhador(cnx, filtro);
		lotacoes.ativaCaminhador();

		if (!lotacoes.isEmpty()) {
			while (!lotacoes.isEof()) {
				lista.add(lotacoes.getLotacao());
				lotacoes.proximo();
			}
		}

		return lista;
	}

	public void selecionaRiscoEdicao(RiscoBeneficiario risco) {
		riscoEdicao = new RiscoBeneficiario();
		riscoEdicao = risco;
	}

	public void adicionaRiscoSelecionado(Risco risco) {
		try {
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
			try {

				RiscoBeneficiario novoRisco = new RiscoBeneficiario();
				novoRisco.setCdBeneficiarioCartao(beneficiarioSelecionado.getBeneficiario().getCdBeneficiarioCartao());
				novoRisco.setDtInclusao(new QtData());
				novoRisco.setIdRisco(risco.getIdRisco());

				RiscoBeneficiarioNegocio insere = new RiscoBeneficiarioNegocio(cnx, novoRisco);
				insere.insere();

				selecionaBeneficiario(beneficiarioSelecionado.getBeneficiario());

			} finally {
				cnx.libera();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void excluiRiscoSelecionado(RiscoBeneficiario risco) {
		try {
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
			try {

				risco.setDtExclusao(new QtData());
				RiscoBeneficiarioNegocio exclui = new RiscoBeneficiarioNegocio(cnx, risco);
				exclui.altera();

				selecionaBeneficiario(beneficiarioSelecionado.getBeneficiario());

			} finally {
				cnx.libera();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void alteraRiscoSelecionado(Risco risco) {
		try {
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
			try {

				RiscoBeneficiario novoRisco = new RiscoBeneficiario();
				novoRisco.setCdBeneficiarioCartao(riscoEdicao.getCdBeneficiarioCartao());
				novoRisco.setDtInclusao(new QtData());
				novoRisco.setIdRisco(risco.getIdRisco());

				riscoEdicao.setDtExclusao(new QtData());
				RiscoBeneficiarioNegocio exclui = new RiscoBeneficiarioNegocio(cnx, riscoEdicao);
				exclui.altera();

				RiscoBeneficiarioNegocio insere = new RiscoBeneficiarioNegocio(cnx, novoRisco);
				insere.insere();

				selecionaBeneficiario(beneficiarioSelecionado.getBeneficiario());

			} finally {
				cnx.libera();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void selecionaExameEdicao(ExameBeneficiario exame) {
		exameEdicao = new ExameBeneficiario();
		exameEdicao = exame;
	}

	public void adicionaExameSelecionado() {
		try {
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
			try {

				ExameBeneficiarioNegocio negocio = new ExameBeneficiarioNegocio(cnx, exameNovo);
				negocio.insere();

				selecionaBeneficiario(beneficiarioSelecionado.getBeneficiario());

			} finally {
				cnx.libera();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void excluiExameSelecionado(ExameBeneficiario exame) {
		try {
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
			try {

				ExameBeneficiarioNegocio negocio = new ExameBeneficiarioNegocio(cnx, exame);
				negocio.exclui();

				selecionaBeneficiario(beneficiarioSelecionado.getBeneficiario());

			} finally {
				cnx.libera();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void alteraExameSelecionado(ExameBeneficiario exame) {
		try {
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
			try {

				ExameBeneficiarioNegocio negocio = new ExameBeneficiarioNegocio(cnx, exame);
				negocio.altera();

				selecionaBeneficiario(beneficiarioSelecionado.getBeneficiario());

			} finally {
				cnx.libera();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private List<Cargo> carregaListaCargo(Conexao cnx) throws QtSQLException {
		List<Cargo> lista = new LinkedList<>();
		FiltroDeNavegador filtro = new FiltroDeNavegador();

		CargoCaminhador caminhador = new CargoCaminhador(cnx, filtro);
		caminhador.setOrdemDeNavegacao(CargoCaminhador.POR_DS_CARGO);

		caminhador.ativaCaminhador();

		if (!caminhador.isEmpty()) {
			while (!caminhador.isEof()) {
				lista.add(caminhador.getCargo());
				caminhador.proximo();
			}
		}

		return lista;
	}

	private List<Funcao> carregaListaFuncao(ConexaoParaPoolDeConexoes cnx) throws QtSQLException {
		List<Funcao> lista = new LinkedList<>();
		FiltroDeNavegador filtro = new FiltroDeNavegador();

		FuncaoCaminhador caminhador = new FuncaoCaminhador(cnx, filtro);
		caminhador.setOrdemDeNavegacao(FuncaoCaminhador.POR_DS_FUNCAO);

		caminhador.ativaCaminhador();

		if (!caminhador.isEmpty()) {
			while (!caminhador.isEof()) {
				lista.add(caminhador.getFuncao());
				caminhador.proximo();
			}
		}

		return lista;
	}

	private List<Setor> carregaListaSetor(ConexaoParaPoolDeConexoes cnx) throws QtSQLException {
		List<Setor> lista = new LinkedList<>();
		FiltroDeNavegador filtro = new FiltroDeNavegador();

		SetorCaminhador caminhador = new SetorCaminhador(cnx, filtro);
		caminhador.setOrdemDeNavegacao(SetorCaminhador.POR_DS_SETOR);
		caminhador.ativaCaminhador();

		if (!caminhador.isEmpty()) {
			while (!caminhador.isEof()) {
				lista.add(caminhador.getSetor());
				caminhador.proximo();
			}
		}

		return lista;
	}

	private List<Cbo> carregaListaCbo(ConexaoParaPoolDeConexoes cnx) throws QtSQLException {
		List<Cbo> lista = new LinkedList<>();
		FiltroDeNavegador filtro = new FiltroDeNavegador();

		CboCaminhador caminhador = new CboCaminhador(cnx, filtro);
		caminhador.setOrdemDeNavegacao(CboCaminhador.POR_CODIGO);

		caminhador.ativaCaminhador();

		if (!caminhador.isEmpty()) {
			while (!caminhador.isEof()) {
				lista.add(caminhador.getCbo());
				caminhador.proximo();
			}
		}

		return lista;
	}

	private List<Estado> carregaListaEstados(ConexaoParaPoolDeConexoes cnx) throws QtSQLException {
		List<Estado> lista = new LinkedList<>();
		FiltroDeNavegador filtro = new FiltroDeNavegador();

		EstadoCaminhador estados = new EstadoCaminhador(cnx, filtro);
		estados.setOrdemDeNavegacao(CidadeCaminhador.POR_NOME);
		estados.ativaCaminhador();

		if (!estados.isEmpty()) {
			while (!estados.isEof()) {
				lista.add(estados.getEstado());
				estados.proximo();
			}
		}

		return lista;
	}

	private List<PaisAns> carregaListaPaises(ConexaoParaPoolDeConexoes cnx) throws QtSQLException {
		List<PaisAns> lista = new LinkedList<>();
		FiltroDeNavegador filtro = new FiltroDeNavegador();

		PaisAnsCaminhador paises = new PaisAnsCaminhador(cnx, filtro);
		paises.setOrdemDeNavegacao(CidadeCaminhador.POR_NOME);
		paises.ativaCaminhador();

		if (!paises.isEmpty()) {
			while (!paises.isEof()) {
				lista.add(paises.getPaisAns());
				paises.proximo();
			}
		}

		return lista;
	}

	private List<Beneficiario> getLista() {
		List<Beneficiario> lista = new LinkedList<>();

		try {
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
			try {

				FiltroDeNavegador filtro = new FiltroDeNavegador();
				filtro.adicionaCondicao("cd_empresa", FiltroDeNavegador.MAIOR_OU_IGUAL, "8000");
				filtro.adicionaCondicao("cd_empresa", FiltroDeNavegador.MENOR_OU_IGUAL, "9000");

				BeneficiarioCaminhador benefs = new BeneficiarioCaminhador(cnx, filtro);
				benefs.ativaCaminhador();

				if (!benefs.isEmpty()) {
					while (!benefs.isEof()) {
						lista.add(benefs.getBeneficiario());
						benefs.proximo();
					}
				}

			} finally {
				cnx.libera();
			}

		} catch (Exception e) {
			e.printStackTrace();
			msgAoUsuario = "Ops! " + e.getMessage();
		}

		return lista;
	}

	public void salvarBeneficiarioAtivo() {

		try {
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

			try {
				mensagemEnviada = new MensagemEnviada();
				mensagemEnviada.setDtMensagem(new QtData());
				initMensagemParametro(cnx);

				BeneficiarioNegocio benef = new BeneficiarioNegocio(cnx, beneficiarioSelecionado.getBeneficiario());
				benef.altera();

				BeneficiarioCbo beneficiarioCboAnterior = BeneficiarioCboLocalizador.buscaBeneficiarioCbo(cnx,
						beneficiarioSelecionado.getBeneficiarioCbo().getCdBeneficiarioCartao());

				BeneficiarioCboNegocio benefCbo = new BeneficiarioCboNegocio(cnx,
						beneficiarioSelecionado.getBeneficiarioCbo());
				benefCbo.altera();

				if (beneficiarioCboAnterior != null) {
					boolean enviaEmail = false;
					if (compara(beneficiarioCboAnterior.getCdCbo(),
							beneficiarioSelecionado.getBeneficiarioCbo().getCdCbo())) {
						enviaEmail = true;
					}

					if (compara(beneficiarioCboAnterior.getIdFuncao() + "",
							beneficiarioSelecionado.getBeneficiarioCbo().getIdFuncao() + "")) {
						enviaEmail = true;
					}

					if (compara(beneficiarioCboAnterior.getIdSetor() + "",
							beneficiarioSelecionado.getBeneficiarioCbo().getIdSetor() + "")) {
						enviaEmail = true;
					}

					if (compara(beneficiarioCboAnterior.getIdCargo() + "",
							beneficiarioSelecionado.getBeneficiarioCbo().getIdCargo() + "")) {
						enviaEmail = true;
					}
					if (enviaEmail) {
						enviaEmail(cnx, beneficiarioSelecionado.getBeneficiarioCbo(), beneficiarioCboAnterior);
					}
				}

				msgExibida = 0;
				msgAoUsuario = "Beneficiário alterado com sucesso!";
				//Util.sendRedirect("resources/page/dashboard.jsf");
				Util.sendRedirect("resources/page/beneficiarios/mntBeneficiario.jsf");
			} finally {
				cnx.libera();
			}
		} catch (Exception e) {
			e.printStackTrace();
			msgExibida = 0;
			msgAoUsuario = "Ops! " + e.getMessage();
			Util.sendRedirect("resources/page/mntBeneficiario.jsf");
		}

	}

	public void enviaEmail(Conexao cnx, BeneficiarioCbo pre, BeneficiarioCbo beneficiarioCboAnterior) {
		try {

			Email email = new Email();

			email.setPara(mensagemParametro.getDsDestinatarios());
			email.setCc("lucionteixeira@gmail.com");
			// email.setPara( "samuel3dstudio@gmail.com" );
			email.setDe("NoReply@quatro.com.br");

			email.setAssunto("Solicitação de Pre-Cadastro de Beneficiario!!");

			if (usuarioLogado == null) {
				usuarioLogado = jUni.controleLogin.Util.getDadosUsuarioLogado();
			}

			StringBuffer mensagem = new StringBuffer();
			mensagem.append("<b>");
			mensagem.append("Usuario " + usuarioLogado.getDsNomeUsuario() + " alterou ");

			mensagem.append(" do Beneficiario:");
			if (pre.getCdBeneficiarioCartao() != null) {
				mensagem.append(pre.getCdBeneficiarioCartao());
			}

			mensagem.append("</b>");

			mensagem.append("<br/>");
			mensagem.append("<b>data alteraão: </b>");
			mensagem.append(new QtData().toString());
			mensagem.append("<br/>");
			mensagem.append("<br/>");
			mensagem.append("<br/>");
			mensagem.append("<br/>");

			jPcmso.persistencia.geral.e.Empresa e = jPcmso.persistencia.geral.e.EmpresaLocalizador
					.buscaEmpresa(pre.getCdEmpresa());
			if (e != null) {
				mensagem.append(getCampoHtml("EMPRESA", pre.getCdEmpresa() + " - " + e.getDsEmpresa()));
			}

			mensagem.append(getCampoHtml("NOME BENEFICIARIO", pre.getDsNome()));

			if (pre.getDtAdmissao() != null) {
				if (beneficiarioCboAnterior.getDtAdmissao() == null
						|| (!beneficiarioCboAnterior.getDtAdmissao().eIgual(pre.getDtAdmissao()))) {
					mensagem.append(getCampoHtml("DATA DE ADMISÃO", pre.getDtAdmissao().toString()));
				}
			}

			if (compara(beneficiarioCboAnterior.getTpSexo(), pre.getTpSexo())) {
				if (pre.getTpSexo() != null) {
					mensagem.append(getCampoHtml("SEXO", pre.getTpSexo().equals("M") ? "Masculino" : "Feminino"));
				}
			}

			if (pre.getDtNascimento() != null) {
				if (beneficiarioCboAnterior.getDtNascimento() == null
						|| (!beneficiarioCboAnterior.getDtNascimento().eIgual(pre.getDtNascimento()))) {
					mensagem.append(getCampoHtml("DATA DE NASCIMENTO", pre.getDtNascimento().toString()));
				}
			}

			if (compara(beneficiarioCboAnterior.getNrCpf(), pre.getNrCpf())) {
				if (pre.getNrCpf() != null) {
					mensagem.append(getCampoHtml("CPF", pre.getNrCpf()));
				}
			}

			if (compara(beneficiarioCboAnterior.getCdRg(), pre.getCdRg())) {
				if (pre.getCdRg() != null) {
					mensagem.append(getCampoHtml("RG", pre.getCdRg()));
				}
			}

			if (compara(beneficiarioCboAnterior.getDsNomeDaMae(), pre.getDsNomeDaMae())) {
				if (pre.getDsNomeDaMae() != null) {
					mensagem.append(getCampoHtml("NOME DA MÃE", pre.getDsNomeDaMae()));
				}
			}

			if (compara(beneficiarioCboAnterior.getTpEstadoCivil(), pre.getTpEstadoCivil())) {
				if (pre.getTpEstadoCivil() != null) {
					mensagem.append("<br/>");
					mensagem.append("<b>ESTADO CIVIL:&nbsp;</b>");
					if (pre.getTpEstadoCivil().equals("S")) {
						mensagem.append("Solteiro");
					} else if (pre.getTpEstadoCivil().equals("M")) {
						mensagem.append("Casado (M-Married)");
					} else if (pre.getTpEstadoCivil().equals("W")) {
						mensagem.append("Viuvo (W-Widow)");
					} else if (pre.getTpEstadoCivil().equals("D")) {
						mensagem.append("Divorciado");
					} else if (pre.getTpEstadoCivil().equals("A")) {
						mensagem.append("Apartado (Separado)");
					} else if (pre.getTpEstadoCivil().equals("U")) {
						mensagem.append("União Estável");
					}

				}
			}

			if (compara(beneficiarioCboAnterior.getDsAtividade(), pre.getDsAtividade())) {
				if (pre.getDsAtividade() != null) {
					mensagem.append(getCampoHtml("ATIVIDADE", pre.getDsAtividade()));
				}
			}

			if (compara(beneficiarioCboAnterior.getNrPis(), pre.getNrPis())) {
				if (pre.getNrPis() != null) {
					mensagem.append(getCampoHtml("PIS/Pasep", pre.getNrPis()));
				}
			}

			if (compara(beneficiarioCboAnterior.getNrCtps(), pre.getNrCtps())) {
				if (pre.getNrCtps() != null) {
					mensagem.append(getCampoHtml("CTPS", pre.getNrCtps()));
				}
			}

			if (compara(beneficiarioCboAnterior.getNrSerieUf(), pre.getNrSerieUf())) {
				if (pre.getNrSerieUf() != null) {
					mensagem.append(getCampoHtml("SERIE (UF)", pre.getNrSerieUf()));
				}
			}

			if (compara(beneficiarioCboAnterior.getCdCbo(), pre.getCdCbo())) {
				if (pre.getCdCbo() != null) {
					mensagem.append("<br/>");
					mensagem.append("<b>CBO:&nbsp;</b>");
					Cbo cbo = CboLocalizador.buscaCbo(pre.getCdCbo());
					if (cbo != null) {
						mensagem.append(cbo.getNmCbo());
					} else {
						mensagem.append(pre.getCdCbo());
					}
				}
			}

			if (compara(beneficiarioCboAnterior.getIdFuncao() + "", pre.getIdFuncao() + "")) {
				Funcao f = FuncaoLocalizador.buscaFuncao(pre.getIdFuncao());
				if (f != null) {
					mensagem.append(getCampoHtml("FUNÇÃO", f.getDsFuncao()));
				}
			}

			if (compara(beneficiarioCboAnterior.getIdSetor() + "", pre.getIdSetor() + "")) {
				if (pre.getIdSetor() != null) {
					Setor s = SetorLocalizador.buscaSetor(pre.getIdSetor());
					if (s != null) {
						mensagem.append(getCampoHtml("SETOR", s.getDsSetor()));
					}
				}
			}

			if (compara(beneficiarioCboAnterior.getIdCargo() + "", pre.getIdCargo() + "")) {
				Cargo c = CargoLocalizador.buscaCargo(pre.getIdCargo());
				if (c != null) {
					mensagem.append(getCampoHtml("CARGO", c.getDsCargo()));
				}
			}

			if (compara(beneficiarioCboAnterior.getIdLotacao() + "", pre.getIdLotacao() + "")) {
				if (pre.getIdLotacao() != null) {
					Lotacao lotacao = LotacaoLocalizador.buscaLotacao(ParametrosCliente.getCodigoCliente(),
							Inteiro.stoi(pre.getCdEmpresa()), pre.getIdLotacao());
					if (lotacao != null) {
						mensagem.append(
								getCampoHtml("LOTACAO", lotacao.getIdLotacao() + ": " + lotacao.getDsLotacao()));
					}
				}
			}

			if (compara(jUni.biblioteca.util.Util.mascaraFloat(beneficiarioCboAnterior.getVlPeso()),
					jUni.biblioteca.util.Util.mascaraFloat(pre.getVlPeso()))) {
				if (pre.getVlPeso() != null) {
					mensagem.append(getCampoHtml("PESO", jUni.biblioteca.util.Util.mascaraFloat(pre.getVlPeso())));
				}
			}

			if (compara(jUni.biblioteca.util.Util.mascaraFloat(beneficiarioCboAnterior.getVlAltura()),
					jUni.biblioteca.util.Util.mascaraFloat(pre.getVlAltura()))) {
				if (pre.getVlPeso() != null) {
					mensagem.append(getCampoHtml("ALTURA", jUni.biblioteca.util.Util.mascaraFloat(pre.getVlAltura())));
				}
			}

			if (compara(beneficiarioCboAnterior.isSnPossuiDeficiencia(), pre.isSnPossuiDeficiencia())) {
				mensagem.append(getCampoHtml("Possui alguma deficiência", pre.isSnPossuiDeficiencia() ? "SIM" : "NÃO"));
			}

			email.setMensagem(getFormatoHTML(mensagem.toString()));
			email.setConteudo(Email.TEXT_HTML);

			mensagemEnviada.setDsMensagem(getFormatoHTML(mensagem.toString()));
			Mailer mailer = new Mailer(email);
			mailer.send();

		} catch (Exception e) {
			e.printStackTrace();
			mensagemEnviada.setDsErroEmail(e.getMessage());
		}
	}
	
public static String getFormatoHTML( String string ) {
		
		if( string != null && !string.trim().isEmpty() ) {
			
			
			
			string = string.replaceAll( "\n", "<br />" );
			string = string.replaceAll( "á", "&aacute;" );
			string = string.replaceAll( "Á", "&Aacute;" );
			string = string.replaceAll( "é", "&eacute;" );
			string = string.replaceAll( "É", "&Eacute;" );
			string = string.replaceAll( "í", "&iacute;" );
			string = string.replaceAll( "Í", "&Iacute;" );
			string = string.replaceAll( "ó", "&oacute;" );
			string = string.replaceAll( "Ó", "&Oacute;" );
			string = string.replaceAll( "ú", "&uacute;" );
			string = string.replaceAll( "Ú", "&Uacute;" );
			string = string.replaceAll( "ã", "&atilde;" );
			string = string.replaceAll( "Ã", "&Atilde;" );
			string = string.replaceAll( "õ", "&otilde;" );
			string = string.replaceAll( "Õ", "&Otilde;" );
			string = string.replaceAll( "â", "&acirc;" );
			string = string.replaceAll( "Â", "&Acirc;" );
			string = string.replaceAll( "ê", "&ecirc;" );
			string = string.replaceAll( "Ê", "&Ecirc;" );
			string = string.replaceAll( "ô", "&ocirc;" );
			string = string.replaceAll( "Ô", "&Ocirc;" );
			string = string.replaceAll( "à", "&agrave;" );
			string = string.replaceAll( "À", "&Agrave;" );
			string = string.replaceAll( "ç", "&ccedil;" );
			string = string.replaceAll( "Ç", "&Ccedil;" );
			string = string.replaceAll( "ü", "&uuml;" );
			string = string.replaceAll( "Ü", "&Uuml;" );
			
			return string;
		}
		
		return "&nbsp;";
	}

	private String getCampoHtml(String nomeCampo, String dsCampo) {
		StringBuilder sb = new StringBuilder();
		sb.append("<br/>");
		sb.append("<b>");
		sb.append(nomeCampo);
		sb.append(":&nbsp;</b>");
		sb.append(dsCampo);

		return sb.toString();
	}

	private boolean compara(boolean atual, boolean novo) {
		return compara(atual ? "S" : "N", novo ? "S" : "N");
	}

	private boolean compara(String atual, String novo) {

		if (atual == null) {
			atual = "";
		}

		if (novo == null) {
			novo = "";
		}

		if (!atual.trim().equals(novo.trim())) {
			return true;
		}
		return false;
	}

	public void cancelar() {
		Util.sendRedirect("resources/page/beneficiarios/listaBeneficiarios.jsf");
	}

	public static Method getMtdGet(Object object, String campo) {
		Method mtdGet = null;

		Method[] mt = object.getClass().getDeclaredMethods();
		String nmMetodoGet = getNmMetodoGet(campo);
		String nmMetodoIs = getNmMetodoIs(campo);

		for (int i = 0; i < mt.length; i++) {
			if (mtdGet == null) {
				if (mt[i].getName().equals(nmMetodoGet)) {
					mtdGet = mt[i];
				} else {
					if (mt[i].getName().equals(nmMetodoIs)) {
						mtdGet = mt[i];
					}
				}
			}

			if (mtdGet != null)
				return mtdGet;
		}

		mt = object.getClass().getSuperclass().getDeclaredMethods();
		nmMetodoGet = getNmMetodoGet(campo);
		nmMetodoIs = getNmMetodoIs(campo);

		for (int i = 0; i < mt.length; i++) {
			if (mtdGet == null) {
				if (mt[i].getName().equals(nmMetodoGet)) {
					mtdGet = mt[i];
				} else {
					if (mt[i].getName().equals(nmMetodoIs)) {
						mtdGet = mt[i];
					}
				}
			}

			if (mtdGet != null)
				return mtdGet;
		}

		return mtdGet;
	}

	public static Method getMtdGet(Class<?> classe, String campo) {
		Method mtdGet = null;

		Method[] mt = classe.getDeclaredMethods();
		String nmMetodoGet = getNmMetodoGet(campo);
		String nmMetodoIs = getNmMetodoIs(campo);

		for (int i = 0; i < mt.length; i++) {
			if (mtdGet == null) {
				if (mt[i].getName().equals(nmMetodoGet)) {
					mtdGet = mt[i];
				} else {
					if (mt[i].getName().equals(nmMetodoIs)) {
						mtdGet = mt[i];
					}
				}
			}

			if (mtdGet != null)
				return mtdGet;
		}

		mt = classe.getSuperclass().getDeclaredMethods();
		nmMetodoGet = getNmMetodoGet(campo);
		nmMetodoIs = getNmMetodoIs(campo);

		for (int i = 0; i < mt.length; i++) {
			if (mtdGet == null) {
				if (mt[i].getName().equals(nmMetodoGet)) {
					mtdGet = mt[i];
				} else {
					if (mt[i].getName().equals(nmMetodoIs)) {
						mtdGet = mt[i];
					}
				}
			}

			if (mtdGet != null)
				return mtdGet;
		}

		return mtdGet;
	}

	private static String getNmMetodoGet(String campo) {
		return "get" + campo.substring(0, 1).toUpperCase() + campo.substring(1);
	}

	private static String getNmMetodoIs(String campo) {
		return "is" + campo.substring(0, 1).toUpperCase() + campo.substring(1);
	}

	public static boolean copiaTabela(TabelaBasica tabelaOrigem, TabelaBasica tabelaDestino) {
		try {
			try {
				Method[] mt = tabelaDestino.getClass().getDeclaredMethods();

				for (int i = 0; i < mt.length; i++) {
					Method met = mt[i];
					if (met.getName().startsWith("set")) {
						Method method = getMtdGet(tabelaOrigem, met.getName().substring(3));
						if (method != null) {
							Object vlCampo = method.invoke(tabelaOrigem, (Object[]) null);
							met.invoke(tabelaDestino, vlCampo);
						}
					}
				}
			} catch (Exception e) {
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}

	private List<Cidade> carregaListaCidades(String cdEstado) {
		List<Cidade> lista = new LinkedList<>();

		if (cdEstado != null) {
			try {
				ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
				try {
					FiltroDeNavegador filtro = new FiltroDeNavegador();
					filtro.adicionaCondicao("cd_estado", FiltroDeNavegador.IGUAL, cdEstado);

					CidadeCaminhador cidades = new CidadeCaminhador(cnx, filtro);
					cidades.setOrdemDeNavegacao(CidadeCaminhador.POR_NOME);

					cidades.ativaCaminhador();

					if (!cidades.isEmpty()) {
						while (!cidades.isEof()) {
							lista.add(cidades.getCidade());
							cidades.proximo();
						}
					}
				} finally {
					cnx.libera();
				}

			} catch (Exception e) {
				e.printStackTrace();
				msgExibida = 0;
				msgAoUsuario = "Ops! " + e.getMessage();
			}
		}
		return lista;
	}

	public void selecionaEstado(String cdEstado) {
		listaCidades.clear();
		listaCidades = carregaListaCidades(cdEstado);
	}

	public void selecionaBeneficiario(Beneficiario benef) {
		beneficiarioSelecionado = new DadosBeneficiarioSelecionado();
		beneficiarioSelecionado.setBeneficiario(benef);
		try {
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
			try {
				beneficiarioSelecionado.setBeneficiarioCbo(BeneficiarioCboLocalizador.buscaBeneficiarioCbo(cnx,
						beneficiarioSelecionado.getBeneficiario().getCdBeneficiarioCartao()));

				FiltroDeNavegador filtro = new FiltroDeNavegador();
				filtro.adicionaCondicao("cd_beneficiario_cartao", FiltroDeNavegador.IGUAL,
						beneficiarioSelecionado.getBeneficiario().getCdBeneficiarioCartao());
				filtro.setOrdemDeNavegacao(RiscoBeneficiarioCaminhador.POR_ORDEM_DEFAULT);

				RiscoBeneficiarioCaminhador riscos = new RiscoBeneficiarioCaminhador(filtro);
				riscos.ativaCaminhador();
				beneficiarioSelecionado.getRiscos().clear();
				selecionaEstado(beneficiarioSelecionado.getBeneficiario().getCdEstadoResidencia());

				dsCbo = null;
				dsFuncao = null;
				dsFuncaoAnterior = null;
				dsCargo = null;
				dsSetor = null;

				if (beneficiarioSelecionado.getBeneficiarioCbo() != null) {
					if (beneficiarioSelecionado.getBeneficiarioCbo().getCdCbo() != null) {
						dsCbo = CboLocalizador.buscaCbo(beneficiarioSelecionado.getBeneficiarioCbo().getCdCbo())
								.getNmCbo();
					}
					if (beneficiarioSelecionado.getBeneficiarioCbo().getIdFuncao() != null) {
						dsFuncao = FuncaoLocalizador
								.buscaFuncao(beneficiarioSelecionado.getBeneficiarioCbo().getIdFuncao()).getDsFuncao();
					}
					if (beneficiarioSelecionado.getBeneficiarioCbo().getIdFuncaoAnterior() != null) {
						dsFuncaoAnterior = FuncaoLocalizador
								.buscaFuncao(beneficiarioSelecionado.getBeneficiarioCbo().getIdFuncaoAnterior())
								.getDsFuncao();
					}
					if (beneficiarioSelecionado.getBeneficiarioCbo().getIdCargo() != null) {
						dsCargo = CargoLocalizador.buscaCargo(beneficiarioSelecionado.getBeneficiarioCbo().getIdCargo())
								.getDsCargo();
					}
					if (beneficiarioSelecionado.getBeneficiarioCbo().getIdSetor() != null) {
						dsSetor = SetorLocalizador.buscaSetor(beneficiarioSelecionado.getBeneficiarioCbo().getIdSetor())
								.getDsSetor();
					}
				}

				if (!riscos.isEmpty()) {
					while (!riscos.isEof()) {
						beneficiarioSelecionado.getRiscos().add(riscos.getRiscoBeneficiario());
						riscos.proximo();
					}
				}

				FiltroDeNavegador filtroExames = new FiltroDeNavegador();
				filtroExames.adicionaCondicao("cd_beneficiario_cartao", FiltroDeNavegador.IGUAL,
						beneficiarioSelecionado.getBeneficiario().getCdBeneficiarioCartao());
				filtroExames.setOrdemDeNavegacao(ExameBeneficiarioCaminhador.POR_ORDEM_DEFAULT);

				ExameBeneficiarioCaminhador exames = new ExameBeneficiarioCaminhador(filtro);
				exames.ativaCaminhador();
				beneficiarioSelecionado.getExames().clear();

				if (!exames.isEmpty()) {
					while (!exames.isEof()) {
						beneficiarioSelecionado.getExames()
								.add(ExameBeneficiarioLocalizador.buscaExameBeneficiario(
										exames.getExameBeneficiario().getCdBeneficiarioCartao(),
										exames.getExameBeneficiario().getCdProcedimento()));
						exames.proximo();
					}
				}

			} finally {
				cnx.libera();
			}
		} catch (Exception e) {
			msgExibida = 0;
			msgAoUsuario = "Ops! " + e.getMessage();
		}

		Util.sendRedirect("resources/page/beneficiarios/mntBeneficiario.jsf");
	}

	public Risco getRiscoList(RiscoBeneficiario risco) {
		try {
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
			try {
				return RiscoLocalizador.buscaRisco(cnx, risco.getIdRisco());
			} finally {
				cnx.libera();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public void selecionaBeneficiario(PreCadastroCbo pre) throws QtSQLException {
		beneficiarioSelecionado = new DadosBeneficiarioSelecionado();

		try {

			Beneficiario b = new Beneficiario();
			if (pre != null) {

				copiaTabela(pre, b);

				b.setIdCidadeResidencia(pre.getIdCidade());
				b.setCdEstadoRg(pre.getCdEstado());
				b.setCdEstadoResidencia(pre.getCdEstado());
				b.setDsTelefone(pre.getDsTelefoneContato());
				b.setCdCpf(pre.getNrCpf());
				b.setCdPisPasep(pre.getNrPis());
				b.setIdLotacao(pre.getIdLotacao());

				try {
					b.setDsNomeAbreviado(BibliotecaBeneficiario.abreviaNomeBeneficiario(pre.getDsNome(), true));
				} catch (Exception e) {
					b.setDsNomeAbreviado(pre.getDsNome().substring(0, 25));
				}
				b.setDsNomeDaMae(pre.getDsNomeDaMae());
				b.setCdBeneficiarioCartao(b.getCdEmpresa());
				b.setCdEmpresa(null);
				b.setIdEmpresa(null);
				if (b.getCdUnimed() == null || b.getCdUnimed().trim().equals("")) {
					b.setCdUnimed(ParametrosCliente.getCodigoCliente());
				}

				if (b.getCdUnimed() == null || b.getCdUnimed().trim().equals("")) {
					b.setCdUnimed(ParametrosSistema.getInstance().getCodigoDoCliente());
				}

				beneficiarioSelecionado.setBeneficiario(b);

				BeneficiarioCbo cbo = new BeneficiarioCbo();

				cbo.setCdCbo(pre.getCdCbo());
				cbo.setDsAtividade(pre.getDsAtividade());
				cbo.setIdFuncao(pre.getIdFuncao());
				cbo.setIdSetor(pre.getIdSetor());
				cbo.setIdCargo(pre.getIdCargo());
				cbo.setVlAltura(pre.getVlAltura());
				cbo.setVlPeso(pre.getVlPeso());
				cbo.setNrCtps(pre.getNrCtps());
				cbo.setNrSerieUf(pre.getNrSerieUf());
				cbo.setDtAdmissao(pre.getDtAdmissao());
				cbo.setSnPossuiDeficiencia(pre.isSnPossuiDeficiencia() ? "S" : "N");

				beneficiarioSelecionado.setBeneficiarioCbo(cbo);

				if (beneficiarioSelecionado.getBeneficiarioCbo() != null) {
					if (beneficiarioSelecionado.getBeneficiarioCbo().getCdCbo() != null) {
						dsCbo = CboLocalizador.buscaCbo(beneficiarioSelecionado.getBeneficiarioCbo().getCdCbo())
								.getNmCbo();
					}
					if (beneficiarioSelecionado.getBeneficiarioCbo().getIdFuncao() != null) {
						dsFuncao = FuncaoLocalizador
								.buscaFuncao(beneficiarioSelecionado.getBeneficiarioCbo().getIdFuncao()).getDsFuncao();
					}
					if (beneficiarioSelecionado.getBeneficiarioCbo().getIdFuncaoAnterior() != null) {
						dsFuncaoAnterior = FuncaoLocalizador
								.buscaFuncao(beneficiarioSelecionado.getBeneficiarioCbo().getIdFuncaoAnterior())
								.getDsFuncao();
					}
					if (beneficiarioSelecionado.getBeneficiarioCbo().getIdCargo() != null) {
						dsCargo = CargoLocalizador.buscaCargo(beneficiarioSelecionado.getBeneficiarioCbo().getIdCargo())
								.getDsCargo();
					}
					if (beneficiarioSelecionado.getBeneficiarioCbo().getIdSetor() != null) {
						dsSetor = SetorLocalizador.buscaSetor(beneficiarioSelecionado.getBeneficiarioCbo().getIdSetor())
								.getDsSetor();
					}
				}

				FiltroDeNavegador filtro = new FiltroDeNavegador();
				filtro.adicionaCondicao("cd_beneficiario_cartao", FiltroDeNavegador.IGUAL,
						beneficiarioSelecionado.getBeneficiario().getCdBeneficiarioCartao());
				filtro.setOrdemDeNavegacao(RiscoBeneficiarioCaminhador.POR_ORDEM_DEFAULT);

				RiscoBeneficiarioCaminhador riscos = new RiscoBeneficiarioCaminhador(filtro);

				riscos.ativaCaminhador();
				beneficiarioSelecionado.getRiscos().clear();

				if (!riscos.isEmpty()) {
					while (!riscos.isEof()) {
						beneficiarioSelecionado.getRiscos().add(riscos.getRiscoBeneficiario());
						riscos.proximo();
					}
				}

				FiltroDeNavegador filtroExames = new FiltroDeNavegador();
				filtroExames.adicionaCondicao("cd_beneficiario_cartao", FiltroDeNavegador.IGUAL,
						beneficiarioSelecionado.getBeneficiario().getCdBeneficiarioCartao());
				filtroExames.setOrdemDeNavegacao(ExameBeneficiarioCaminhador.POR_ORDEM_DEFAULT);

				ExameBeneficiarioCaminhador exames = new ExameBeneficiarioCaminhador(filtro);
				exames.ativaCaminhador();
				beneficiarioSelecionado.getExames().clear();

				if (!exames.isEmpty()) {
					while (!exames.isEof()) {
						beneficiarioSelecionado.getExames()
								.add(ExameBeneficiarioLocalizador.buscaExameBeneficiario(
										exames.getExameBeneficiario().getCdBeneficiarioCartao(),
										exames.getExameBeneficiario().getCdProcedimento()));
						exames.proximo();
					}
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
			msgExibida = 0;
			msgAoUsuario = "Ops! " + e.getMessage();
		}

		Util.sendRedirect("resources/page/beneficiarios/mntBeneficiario.jsf");
	}

	public void inic() {
		if (msgAoUsuario != null) {
			if (msgExibida > 0) {
				msgAoUsuario = null;
				msgExibida = 0;
			}
			msgExibida++;
		}
	}

	public void novoRegistro() {
		beneficiarioSelecionado = new DadosBeneficiarioSelecionado();
		dsCbo = "";
		dsFuncao = "";
		dsCargo = "";
		dsSetor = "";
		dsFuncaoAnterior = "";
		filtraPorNome = "";
		filtraPorCartao = "";
	}

	public String getNmLotacao(Beneficiario benef) {
		if (benef != null) {
			if (benef.getIdLotacao() != null && benef.getCdUnimed() != null && benef.getIdEmpresa() != null) {
				try {
					ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
					try {

						Lotacao lotacao = LotacaoLocalizador.buscaLotacao(cnx, benef.getCdUnimed(),
								benef.getIdEmpresa(), benef.getIdLotacao());

						if (lotacao != null) {
							return lotacao.getDsLotacao();
						}
					} finally {
						cnx.libera();
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}

	public String getGrupoCarencia(Integer idGrupo) {

		if (idGrupo != null) {
			try {
				ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
				try {
					GrupoDeCarencia servico = GrupoDeCarenciaLocalizador.buscaGrupoDeCarencia(cnx, idGrupo);
					if (servico != null) {
						return servico.getDsGrupoCarencia();
					}

				} finally {
					cnx.libera();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	public String getNomeProcedimento(String cdProcedimento) {

		try {
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
			try {
				Servico servico = ServicoLocalizador.buscaServico(cnx, cdProcedimento);
				if (servico != null) {
					return servico.getDsServico();
				}

			} finally {
				cnx.libera();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public String getNomeEstado(String cdEstado) {

		try {
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
			try {
				Estado estado = EstadoLocalizador.buscaEstado(cnx, cdEstado);
				if (estado != null) {
					return estado.getDsEstado();
				}

			} finally {
				cnx.libera();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public String getNomeCidade(String cdEstado, Integer idCidade) {

		try {
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
			try {
				Cidade cidade = CidadeLocalizador.buscaCidade(cnx, cdEstado, idCidade);
				if (cidade != null) {
					return cidade.getDsCidade();
				}

			} finally {
				cnx.libera();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public String getNomeEmpresa(String cdUnimed, Integer idEmpresa) {

		try {
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
			try {
				Empresa empresa = EmpresaLocalizador.buscaEmpresa(cnx, cdUnimed, idEmpresa);

				if (empresa != null) {
					return empresa.getDsRazaoSocial();
				}

			} finally {
				cnx.libera();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public String getDependencia(String cdDependencia) {

		try {
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
			try {
				Dependencia dependencia = DependenciaLocalizador.buscaDependencia(cnx, cdDependencia);

				if (dependencia != null) {
					return dependencia.getDsDependencia();
				}

			} finally {
				cnx.libera();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public int getIdade(QtData data) {

		if (data != null) {
			try {
				return QtData.idade(data);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return 0;
	}

	public String getMsgAoUsuario() {
		return msgAoUsuario;
	}

	public void setMsgAoUsuario(String msgAoUsuario) {
		this.msgAoUsuario = msgAoUsuario;
	}

	public Integer getMsgExibida() {
		return msgExibida;
	}

	public void setMsgExibida(Integer msgExibida) {
		this.msgExibida = msgExibida;
	}

	public DadosBeneficiarioSelecionado getBeneficiarioSelecionado() {
		return beneficiarioSelecionado;
	}

	public void setBeneficiarioSelecionado(DadosBeneficiarioSelecionado beneficiarioSelecionado) {
		this.beneficiarioSelecionado = beneficiarioSelecionado;
	}

	public String getDsCbo() {
		return dsCbo;
	}

	public void setDsCbo(String dsCbo) {
		this.dsCbo = dsCbo;
	}

	public String getDsFuncao() {
		return dsFuncao;
	}

	public void setDsFuncao(String dsFuncao) {
		this.dsFuncao = dsFuncao;
	}

	public String getDsCargo() {
		return dsCargo;
	}

	public void setDsCargo(String dsCargo) {
		this.dsCargo = dsCargo;
	}

	public String getDsSetor() {
		return dsSetor;
	}

	public void setDsSetor(String dsSetor) {
		this.dsSetor = dsSetor;
	}

	public String getDsFuncaoAnterior() {
		return dsFuncaoAnterior;
	}

	public void setDsFuncaoAnterior(String dsFuncaoAnterior) {
		this.dsFuncaoAnterior = dsFuncaoAnterior;
	}

	public String getFiltraPorNome() {
		return filtraPorNome;
	}

	public void setFiltraPorNome(String filtraPorNome) {
		this.filtraPorNome = filtraPorNome;
	}

	public String getFiltraPorCartao() {
		return filtraPorCartao;
	}

	public void setFiltraPorCartao(String filtraPorCartao) {
		this.filtraPorCartao = filtraPorCartao;
	}

	public void setListagem(List<Beneficiario> listagem) {
		this.listagem = listagem;
	}

	public List<Beneficiario> getListagem() {
		return listagem;
	}

	public Usuario getUsuarioLogado() {
		return usuarioLogado;
	}

	public void setUsuarioLogado(Usuario usuarioLogado) {
		this.usuarioLogado = usuarioLogado;
	}

	public String getDsLotacaoPesquisa() {
		return dsLotacaoPesquisa;
	}

	public void setDsLotacaoPesquisa(String dsLotacaoPesquisa) {
		this.dsLotacaoPesquisa = dsLotacaoPesquisa;
	}

	public String getIdLotacaoPesquisa() {
		return idLotacaoPesquisa;
	}

	public void setIdLotacaoPesquisa(String idLotacaoPesquisa) {
		this.idLotacaoPesquisa = idLotacaoPesquisa;
	}

	public List<Cidade> getListaCidades() {
		return listaCidades;
	}

	public void setListaCidades(List<Cidade> listaCidades) {
		this.listaCidades = listaCidades;
	}

	public List<PaisAns> getListaPais() {
		return listaPais;
	}

	public void setListaPais(List<PaisAns> listaPais) {
		this.listaPais = listaPais;
	}

	public List<Estado> getListaEstados() {
		return listaEstados;
	}

	public void setListaEstados(List<Estado> listaEstados) {
		this.listaEstados = listaEstados;
	}

	public List<FiliaisEmpresa> getListaFiliais() {
		return listaFiliais;
	}

	public void setListaFiliais(List<FiliaisEmpresa> listaFiliais) {
		this.listaFiliais = listaFiliais;
	}

	public List<Cbo> getListaCbo() {
		return listaCbo;
	}

	public void setListaCbo(List<Cbo> listaCbo) {
		this.listaCbo = listaCbo;
	}

	public List<Setor> getListaSetor() {
		return listaSetor;
	}

	public void setListaSetor(List<Setor> listaSetor) {
		this.listaSetor = listaSetor;
	}

	public List<Funcao> getListaFuncao() {
		return listaFuncao;
	}

	public void setListaFuncao(List<Funcao> listaFuncao) {
		this.listaFuncao = listaFuncao;
	}

	public List<Cargo> getListaCargo() {
		return listaCargo;
	}

	public void setListaCargo(List<Cargo> listaCargo) {
		this.listaCargo = listaCargo;
	}

	public List<Lotacao> getListaLotacao() {
		return listaLotacao;
	}

	public void setListaLotacao(List<Lotacao> listaLotacao) {
		this.listaLotacao = listaLotacao;
	}

	public String getSnPossuiDeficienciaCbo() {

		if (beneficiarioSelecionado != null && beneficiarioSelecionado.getBeneficiarioCbo() != null) {
			if (beneficiarioSelecionado.getBeneficiarioCbo().isSnPossuiDeficiencia()) {
				snPossuiDeficienciaCbo = "S";
			} else {
				snPossuiDeficienciaCbo = "N";
			}
		}
		return snPossuiDeficienciaCbo;
	}

	public void setSnPossuiDeficienciaCbo(String snPossuiDeficienciaCbo) {
		if (snPossuiDeficienciaCbo != null) {
			if (beneficiarioSelecionado != null && beneficiarioSelecionado.getBeneficiarioCbo() != null) {
				beneficiarioSelecionado.getBeneficiarioCbo().setSnPossuiDeficiencia(snPossuiDeficienciaCbo);
			}
		}
		this.snPossuiDeficienciaCbo = snPossuiDeficienciaCbo;
	}

	public String getSnTrabalhoEmAltura() {
		if (beneficiarioSelecionado != null && beneficiarioSelecionado.getBeneficiarioCbo() != null) {
			if (beneficiarioSelecionado.getBeneficiarioCbo().isSnTrabalhoEmAltura()) {
				snTrabalhoEmAltura = "S";
			} else {
				snTrabalhoEmAltura = "N";
			}
		}
		return snTrabalhoEmAltura;
	}

	public void setSnTrabalhoEmAltura(String snTrabalhoEmAltura) {
		if (snTrabalhoEmAltura != null) {
			if (beneficiarioSelecionado != null && beneficiarioSelecionado.getBeneficiarioCbo() != null) {
				beneficiarioSelecionado.getBeneficiarioCbo().setSnTrabalhoEmAltura(snTrabalhoEmAltura);
			}
		}
		this.snTrabalhoEmAltura = snTrabalhoEmAltura;
	}

	public String getSnEspacoConfinado() {
		if (beneficiarioSelecionado != null && beneficiarioSelecionado.getBeneficiarioCbo() != null) {
			if (beneficiarioSelecionado.getBeneficiarioCbo().isSnEspacoConfinado()) {
				snEspacoConfinado = "S";
			} else {
				snEspacoConfinado = "N";
			}
		}
		return snEspacoConfinado;
	}

	public void setSnEspacoConfinado(String snEspacoConfinado) {
		if (snEspacoConfinado != null) {
			if (beneficiarioSelecionado != null && beneficiarioSelecionado.getBeneficiarioCbo() != null) {
				beneficiarioSelecionado.getBeneficiarioCbo().setSnEspacoConfinado(snEspacoConfinado);
			}
		}
		this.snEspacoConfinado = snEspacoConfinado;
	}

	public String getSnFitossanitarios() {
		if (beneficiarioSelecionado != null && beneficiarioSelecionado.getBeneficiarioCbo() != null) {
			if (beneficiarioSelecionado.getBeneficiarioCbo().isSnFitossanitario()) {
				snFitossanitarios = "S";
			} else {
				snFitossanitarios = "N";
			}
		}
		return snFitossanitarios;
	}

	public void setSnFitossanitarios(String snFitossanitarios) {
		if (snFitossanitarios != null) {
			if (beneficiarioSelecionado != null && beneficiarioSelecionado.getBeneficiarioCbo() != null) {
				beneficiarioSelecionado.getBeneficiarioCbo().setSnFitossanitario(snFitossanitarios);
			}
		}
		this.snFitossanitarios = snFitossanitarios;
	}

	public RiscoBeneficiario getRiscoEdicao() {
		return riscoEdicao;
	}

	public void setRiscoEdicao(RiscoBeneficiario riscoEdicao) {
		this.riscoEdicao = riscoEdicao;
	}

	public ExameBeneficiario getExameEdicao() {
		return exameEdicao;
	}

	public void setExameEdicao(ExameBeneficiario exameEdicao) {
		this.exameEdicao = exameEdicao;
	}

}
