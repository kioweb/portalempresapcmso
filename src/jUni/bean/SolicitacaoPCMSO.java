package jUni.bean;

import jUni.bean.dados.DadosSolicitacaoPCMSO;
import quatro.sql.Conexao;
import quatro.sql.Query;

public class SolicitacaoPCMSO {
	
	private DadosSolicitacaoPCMSO dados;
	
	public SolicitacaoPCMSO( DadosSolicitacaoPCMSO dados) {
		this.dados = dados;
	}
	
	public void incluiSolicitacao( Conexao cnx ) throws Exception {
		
		try {
			Query q = new Query( cnx );
			q.setSQL( "insert into pcmso.solicitacoes_pcmso " );
			q.addSQL( "( id_usuario_solicitou, cd_unimed, cd_empresa, cd_familia, cd_dependencia, cd_digito, nm_beneficiario, dt_solicitacao, tp_solicitacao, ds_observacoes, dt_confirmacao, id_usuario_confirmacao) " );
			q.addSQL( "values" );
			q.addSQL( "( :idUsuarioSolicitou, :cdUnimed, :cdEmpresa, :cdFamilia, :cdDependencia, :cdDigito, :nmBeneficiario, '" + dados.getDtSolicitacao().getQtDataAsTimestamp() + "', :tpSolicitacao, :dsObservacoes, :dtConfirmacao, :idUsuarioConfirmacao )" );
			
			q.setParameter( "idUsuarioSolicitou", dados.getIdUsuarioSolicitou() );
			q.setParameter( "cdUnimed", dados.getCdUnimed() );
			q.setParameter( "cdEmpresa", dados.getCdEmpresa() );
			q.setParameter( "cdFamilia", dados.getCdFamilia() );
			q.setParameter( "cdDependencia", dados.getCdDependencia() );
			q.setParameter( "cdDigito", dados.getCdDigito() );
			q.setParameter( "nmBeneficiario", dados.getNmBeneficiario() );
			q.setParameter( "dtSolicitacao", dados.getDtSolicitacao() != null ? dados.getDtSolicitacao().getQtDataAsTimestamp() : null );
			q.setParameter( "tpSolicitacao", dados.getTpSolicitacao() );
			q.setParameter( "dsObservacoes", dados.getDsObservacoes() );
			q.setParameter( "dtConfirmacao", dados.getDtConfirmacao() != null ? dados.getDtConfirmacao().getQtDataAsTimestamp() : null );
			q.setParameter( "idUsuarioConfirmacao", dados.getIdUsuarioConfirmacao() );
			
			q.executeUpdate();
			
		} catch (Exception e ) {
			throw new Exception ("Erro ao incluir a Solicitação do PCMSO.");
		}
	}
	
	public void confirmaSolicitacao( Conexao cnx ) throws Exception {
		
		try {
			Query q = new Query( cnx );
			q.setSQL( "update pcmso.solicitacoes_pcmso set " );
			q.addSQL( "dt_confirmacao = '" + dados.getDtConfirmacao().getQtDataAsTimestamp() + "', id_usuario_confirmacao = :idUsuarioConfirmacao " );
			q.addSQL( " where id_solicitacao = :idSolicitacao " );
			
			q.setParameter( "idSolicitacao", dados.getIdSolicitacao() );
			q.setParameter( "idUsuarioConfirmacao", dados.getIdUsuarioConfirmacao() );
			
			q.executeUpdate();
			
		} catch (Exception e ) {
			throw new Exception ("Erro ao incluir a Solicitação do PCMSO.");
		}
	}

}
