package jUni.bean;

import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import jPcmso.biblioteca.mensageiro.Email;
import jPcmso.biblioteca.mensageiro.Mailer;
import jPcmso.negocio.geral.b.BeneficiarioCboNegocio;
import jPcmso.negocio.geral.p.PreCadastroCboNegocio;
import jPcmso.persistencia.geral.b.BeneficiarioCbo;
import jPcmso.persistencia.geral.b.BeneficiarioCboLocalizador;
import jPcmso.persistencia.geral.c.Cargo;
import jPcmso.persistencia.geral.c.CargoCaminhador;
import jPcmso.persistencia.geral.c.CargoLocalizador;
import jPcmso.persistencia.geral.c.Cbo;
import jPcmso.persistencia.geral.c.CboCaminhador;
import jPcmso.persistencia.geral.c.CboLocalizador;
import jPcmso.persistencia.geral.e.ExameBeneficiarioCaminhador;
import jPcmso.persistencia.geral.e.ExameBeneficiarioLocalizador;
import jPcmso.persistencia.geral.f.FiliaisEmpresa;
import jPcmso.persistencia.geral.f.FiliaisEmpresaCaminhador;
import jPcmso.persistencia.geral.f.FiliaisEmpresaLocalizador;
import jPcmso.persistencia.geral.f.Funcao;
import jPcmso.persistencia.geral.f.FuncaoCaminhador;
import jPcmso.persistencia.geral.f.FuncaoLocalizador;
import jPcmso.persistencia.geral.m.MensagemEnviada;
import jPcmso.persistencia.geral.m.MensagemParametro;
import jPcmso.persistencia.geral.m.MensagemParametroLocalizador;
import jPcmso.persistencia.geral.m.MotivoExclusaoBeneficiario;
import jPcmso.persistencia.geral.m.MotivoExclusaoBeneficiarioCaminhador;
import jPcmso.persistencia.geral.m.MotivoExclusaoBeneficiarioLocalizador;
import jPcmso.persistencia.geral.p.PreCadastroCbo;
import jPcmso.persistencia.geral.p.PreCadastroCboCaminhador;
import jPcmso.persistencia.geral.p.PreCadastroCboLocalizador;
import jPcmso.persistencia.geral.r.Risco;
import jPcmso.persistencia.geral.r.RiscoBeneficiarioCaminhador;
import jPcmso.persistencia.geral.r.RiscoCaminhador;
import jPcmso.persistencia.geral.r.RiscoLocalizador;
import jPcmso.persistencia.geral.s.Setor;
import jPcmso.persistencia.geral.s.SetorCaminhador;
import jPcmso.persistencia.geral.s.SetorLocalizador;
import jUni.bean.dados.DadosSolicitacaoPCMSO;
import jUni.bean.dados.DadosSolicitacaoPPPPCMSO;
import jUni.biblioteca.conversao.Inteiro;
import jUni.biblioteca.parametros.ParametrosCliente;
import jUni.controleLogin.Usuario;
import jUni.negocio.geral.b.BeneficiarioNegocio;
import jUni.persistencia.geral.b.Beneficiario;
import jUni.persistencia.geral.b.BeneficiarioCaminhador;
import jUni.persistencia.geral.b.BeneficiarioLocalizador;
import jUni.persistencia.geral.c.Cidade;
import jUni.persistencia.geral.c.CidadeCaminhador;
import jUni.persistencia.geral.c.CidadeLocalizador;
import jUni.persistencia.geral.d.Dependencia;
import jUni.persistencia.geral.d.DependenciaLocalizador;
import jUni.persistencia.geral.e.Empresa;
import jUni.persistencia.geral.e.EmpresaLocalizador;
import jUni.persistencia.geral.e.Estado;
import jUni.persistencia.geral.e.EstadoCaminhador;
import jUni.persistencia.geral.e.EstadoLocalizador;
import jUni.persistencia.geral.g.GrupoDeCarencia;
import jUni.persistencia.geral.g.GrupoDeCarenciaLocalizador;
import jUni.persistencia.geral.l.Lotacao;
import jUni.persistencia.geral.l.LotacaoCaminhador;
import jUni.persistencia.geral.l.LotacaoLocalizador;
import jUni.persistencia.geral.p.PaisAns;
import jUni.persistencia.geral.p.PaisAnsCaminhador;
import jUni.persistencia.geral.s.Servico;
import jUni.persistencia.geral.s.ServicoLocalizador;
import jUni.relatorio.DadosRelatorioJasper;
import jUni.util.Util;
import quatro.persistencia.FiltroDeNavegador;
import quatro.sql.Conexao;
import quatro.sql.ConexaoParaPoolDeConexoes;
import quatro.sql.PoolDeConexoes;
import quatro.sql.QtSQLException;
import quatro.sql.Query;
import quatro.util.DigitoVerificador;
import quatro.util.ParametrosSistema;
import quatro.util.QtData;

@SessionScoped
@ManagedBean
public class BeanPreCadastroCboPCMSO {
	
	private static String dsPortaEnvio = "465";
	private static String dsHostEnvio = "smtp.gmail.com";
	private static String dsEmailEnvio = "unimeduau@gmail.com";
	private static String dsSenhaEnvio = "Rj@0022659";

	private Usuario usuarioLogado;
	private Integer idEmpresaRelacioanda;
	private String snPossuiDeficiencia;
	private String snPossuiDeficienciaCbo;
	private String snTrabalhoEmAltura;
	private String snEspacoConfinado;

	private boolean habFuncao2;
	private boolean habFuncao3;
	private boolean habFuncao4;
	private boolean habFuncao5;
	private boolean habFuncao6;
	private boolean habFuncao7;
	private boolean habFuncao8;
	private boolean habFuncao9;
	private boolean habFuncao10;
/**
 * campos Retorno ao Trabalho
 */
	private String dtAdmissaoRetorno;
	private String motivoRetorno;
	
	
	/**
	 * campos PPP
	 */
	private String dtAdmissaoPPP;
	private String dtDemissaoPPP;
	private String dtNascimentoPPP;
	private String nrNitPPP;
	private String nrCtpsPPP;
	private String nrSeriePPP;
	private String dsUFPPP;
	private String snAcidentePPP;
	private String dtAcidentePPP;
	private String nrCatPPP;
	private String nmResponsavelPPP;
	private String nrNitResponsavelPPP;

	private String dtAdmissaoFucaoPPP;
	private String dtFimFincaoFuncaoPPP;
	private String dsFuncaoFuncaoPPP;
	private String dsCBOFuncaoPPP;
	private String dsSetorFuncaoPPP;

	private String dtAdmissaoFucaoPPP2;
	private String dtFimFincaoFuncaoPPP2;
	private String dsFuncaoFuncaoPPP2;
	private String dsCBOFuncaoPPP2;
	private String dsSetorFuncaoPPP2;

	private String dtAdmissaoFucaoPPP3;
	private String dtFimFincaoFuncaoPPP3;
	private String dsFuncaoFuncaoPPP3;
	private String dsCBOFuncaoPPP3;
	private String dsSetorFuncaoPPP3;

	private String dtAdmissaoFucaoPPP4;
	private String dtFimFincaoFuncaoPPP4;
	private String dsFuncaoFuncaoPPP4;
	private String dsCBOFuncaoPPP4;
	private String dsSetorFuncaoPPP4;

	private String dtAdmissaoFucaoPPP5;
	private String dtFimFincaoFuncaoPPP5;
	private String dsFuncaoFuncaoPPP5;
	private String dsCBOFuncaoPPP5;
	private String dsSetorFuncaoPPP5;

	private String dtAdmissaoFucaoPPP6;
	private String dtFimFincaoFuncaoPPP6;
	private String dsFuncaoFuncaoPPP6;
	private String dsCBOFuncaoPPP6;
	private String dsSetorFuncaoPPP6;

	private String dtAdmissaoFucaoPPP7;
	private String dtFimFincaoFuncaoPPP7;
	private String dsFuncaoFuncaoPPP7;
	private String dsCBOFuncaoPPP7;
	private String dsSetorFuncaoPPP7;

	private String dtAdmissaoFucaoPPP8;
	private String dtFimFincaoFuncaoPPP8;
	private String dsFuncaoFuncaoPPP8;
	private String dsCBOFuncaoPPP8;
	private String dsSetorFuncaoPPP8;

	private String dtAdmissaoFucaoPPP9;
	private String dtFimFincaoFuncaoPPP9;
	private String dsFuncaoFuncaoPPP9;
	private String dsCBOFuncaoPPP9;
	private String dsSetorFuncaoPPP9;

	private String dtAdmissaoFucaoPPP10;
	private String dtFimFincaoFuncaoPPP10;
	private String dsFuncaoFuncaoPPP10;
	private String dsCBOFuncaoPPP10;
	private String dsSetorFuncaoPPP10;

	private String dtMudancaTrocaPPP;
	private String dtFimMudancaTrocaPPP;
	private String dsFuncaoTrocaPPP;
	private String dsCBOTrocaPPP;
	private String dsSetorTrocaPPP;
	private String dsObervacoesPPP;

	private String dsLotacaoPesquisa;
	private String idLotacaoPesquisa;

	private String abaPreCadastro;
	private String abaBeneficiario;
	private String abaBeneficiarioInativo;
	private String abaSolicitacaoPPP;

	private String dsCbo;
	private String dsFuncao;
	private String dsCargo;
	private String dsSetor;
	private String dsFuncaoAnterior;

	private PreCadastroCbo preCadastroCboSelecionado;
	private DadosBeneficiarioSelecionado beneficiarioSelecionado;

	private DadosSolicitacaoPPPPCMSO dadosSolicitacaoPPPSelecionada;

	private String msgAoUsuario;
	private Integer msgExibida = 0;
	private String mntPreCadastro = "";
	private String dsLotacao;
	private Integer idLotacao;
	private List<PreCadastroCbo> listagemPreCadastro;
	private List<Beneficiario> listagemBeneficiario;
	private List<Beneficiario> listagemBeneficiarioInativo;
	private List<DadosSolicitacaoPPPPCMSO> listagemPPPPendente;
	private List<Risco> listaRiscos;

	private List<MotivoExclusaoBeneficiario> listaMotivoExclusao;

	private Integer idMotivoExclusaoSelecionado;
	private String dtExclusaoSelecionada;
	private String dtDemissaoSelecionada;

	private List<Cidade> listaCidades;
	private List<PaisAns> listaPais;
	private List<Estado> listaEstados;
	private List<FiliaisEmpresa> listaFiliais;

	private String dtNascimento;
	private String dtAdmissao;
	private String dtAdmissaoCbo;

	private List<Cbo> listaCbo;
	private List<Setor> listaSetor;
	private List<Funcao> listaFuncao;
	private List<Cargo> listaCargo;

	private List<Lotacao> listaLotacao;

	private String nmEmpresaSelecionada;

	private String cdCartaoPesquisa;
	private String dsNomePesquisa;
	private String cdEmpresaPesquisa;

	private Integer idLotacaoRelExame;
	private String dsNomeLotacao;
	private String cdEmpresaRel;
	private String dsNomeEmpresa;
	private String cdEmpresaFinalRel;
	private String dsNomeEmpresaFinal;
	private String dtInicial;
	private String dtFinal;
	private String tpRelOrdem;
	private String tpRelAcao;
	private String cdPlanoRel;
	private String nrCco;
	private String tpContratoPacote;
	private String snRelExcluidosData;

	private String dtInclusaoInicialRel;
	private String dtInclusaoFinalRel;
	private String snGeral;
	private String snExcluido;
	private String snAtivos;
	private String snComPacotes;
	private String snSemPacotes;

	public void selecionaAbaPreCadastro() {
		abaBeneficiario = "";
		abaPreCadastro = "active";
		abaBeneficiarioInativo = "";
		abaSolicitacaoPPP = "";
	}

	public void selecionaBeneficiario() {
		abaBeneficiario = "active";
		abaPreCadastro = "";
		abaBeneficiarioInativo = "";
		abaSolicitacaoPPP = "";
	}

	public void selecionaBeneficiarioAtivo() {
		abaBeneficiario = "";
		abaPreCadastro = "";
		abaBeneficiarioInativo = "active";
		abaSolicitacaoPPP = "";
	}

	public void selecionaSolicitacaoPPP() {
		abaSolicitacaoPPP = "active";
		abaBeneficiario = "";
		abaPreCadastro = "";
		abaBeneficiarioInativo = "";
	}

	private static MensagemParametro mensagemParametro;
	private MensagemEnviada mensagemEnviada;

	private static void initMensagemParametro(Conexao cnx) throws QtSQLException {
		if (mensagemParametro == null) {
			mensagemParametro = MensagemParametroLocalizador.buscaMensagemParametro(cnx, 1);
		}
	}

	public void processoRelatorioBeneficiariosCentroCusto() {

		String dsFiltro = "";

		if (usuarioLogado == null) {
			usuarioLogado = jUni.controleLogin.Util.getDadosUsuarioLogado();
		}

		String pathJasper = (String) ParametrosSistema.getInstance().getParametro("dirRelPCMSO");
		pathJasper += "/beneficiarios/";

		Hashtable<String, Object> parametros = new Hashtable<String, Object>();

		StringBuilder parametroSQL = new StringBuilder();

		String dsImagem = "uni" + ParametrosCliente.getCodigoCliente() + ".png";
		parametros.put("imagem", pathJasper + "imgs/" + dsImagem);
		String rodape = "Emitido por " + usuarioLogado.getDsNomeUsuario()
				+ ": Benefici�rios / Benefici�rios�rios / Relat�rios/Gr�ficos / Movimento de Benefici�rios";
		parametros.put("rodape", rodape);

		if (!cdEmpresaRel.trim().equals("")) {
			dsFiltro += "Empresa " + cdEmpresaRel + " ";
			parametros.put("cdEmpresaInicial", cdEmpresaRel);
		} else {
			aviso("Informe Empresa Inicial");
			return;
		}

		if (!cdEmpresaFinalRel.trim().equals("")) {
			dsFiltro += "ate " + cdEmpresaFinalRel + "\n";
			parametros.put("cdEmpresaFinal", cdEmpresaFinalRel);
		} else {
			aviso("Informe Empresa Final");
			return;
		}

		if (dtInclusaoInicialRel != null) {
			dsFiltro += "Data " + dtInclusaoInicialRel + " ";
			parametros.put("dtInicial", new QtData(dtInclusaoInicialRel).getFormatoSQL(true, false));

		} else {
			aviso("Informe Data Inicial");
			return;
		}

		if (dtInclusaoFinalRel != null) {
			dsFiltro += "ate " + dtInclusaoFinalRel;
			parametros.put("dtFinal", new QtData(dtInclusaoFinalRel).getFormatoSQL(true, false));
		} else {
			aviso("Informe Data Final");
			return;
		}

		if (!snGeral.equals("S")) {

			boolean acao = false;

			if (!(snExcluido.equals("S") && snAtivos.equals("S"))) {
				if (snExcluido.equals("S")) {
					parametroSQL.append(" and junie.dt_exclusao is not null ");
					dsFiltro += " empresas excluidas ";
					acao = true;
				}
				if (snAtivos.equals("S")) {
					parametroSQL.append(" and junie.dt_exclusao is null ");
					dsFiltro += " empresas ativas ";
					acao = true;
				}
			}

			if (!(snComPacotes.equals("S") && snSemPacotes.equals("S"))) {

				if (snComPacotes.equals("S")) {
					parametroSQL.append(" and coalesce( e.sn_contrato_pacote, 'N' ) = 'S' ");
					dsFiltro += acao ? " e " : " empresas que ";
					dsFiltro += " contrata pacote";
				}
				if (snSemPacotes.equals("S")) {
					parametroSQL.append(" and coalesce( e.sn_contrato_pacote, 'N' ) = 'N' ");
					dsFiltro += acao ? " e " : " empresas que ";
					dsFiltro += " n�o contrata pacote";
				}
			}

		}

		parametros.put("parametroSQL", parametroSQL.toString());
		parametros.put("ParametroCodigoCliente", ParametrosCliente.getCodigoCliente());
		parametros.put("ParametroCabecalhoCliente",
				ParametrosCliente.getCodigoCliente() + " - " + ParametrosCliente.getNomeCliente());
		parametros.put("ParametroPathRelatorio", pathJasper);
		parametros.put("dsFiltro", dsFiltro);

		aviso("Relatorio finalizado, iniciando download...");
		geraRelatorio("relMovimentoBeneficiarios", parametros);

	}

	public void processoRelatorioBeneficiariosLotacao() {
		String nmRelatorio = null;
		String dsFiltro = "";

		StringBuilder parametrosSql = new StringBuilder();

		try {
			if (this.tpRelOrdem.trim().equals("")) {
				this.tpRelOrdem = "A";
			}

			if (this.tpRelAcao.trim().equals("")) {
				this.tpRelAcao = "I";
			}

			if (this.tpRelAcao.equals("I")) {
				nmRelatorio = "relBeneficiariosincluidosMesPorLotacao";
			} else {
				nmRelatorio = "relBeneficiariosExcluidosMesPorLotacao";
			}

			Hashtable<String, Object> filtros = new Hashtable<String, Object>();

			if (this.tpRelOrdem.equals("A")) {
				filtros.put("ordem", "alfabetica");
			} else if (this.tpRelOrdem.equals("A")) {
				filtros.put("ordem", "numerica");
			} else {
				filtros.put("ordem", "numerica");
			}
			String dtInicial, dtFinal;

			if (this.dtInicial == null) {
				dtInicial = "";
			} else {
				dtInicial = new QtData(this.dtInicial).getSoDataFormatoSQL();
			}
			if (this.dtFinal == null) {
				dtFinal = "";
			} else {
				dtFinal = new QtData(this.dtFinal).getSoDataFormatoSQL();
			}

			if (this.tpRelAcao.equals("I")) {
				filtros.put("data_inicial", dtInicial);
				filtros.put("data_final", dtFinal);
				dsFiltro += "Benefici�rios Inclu�dos \n";
			} else {
				filtros.put("data_inicial_excluidos", dtInicial);
				filtros.put("data_final_excluidos", dtFinal);
				dsFiltro += "Benefici�rios Exclu�dos \n";
			}

			if (cdPlanoRel != null) {
				if (!cdPlanoRel.trim().equals("")) {
					filtros.put("cd_plano", cdPlanoRel);
				}
			}
			if (!this.cdEmpresaRel.trim().equals("") || !this.cdEmpresaFinalRel.trim().equals("")) {
				if (!this.cdEmpresaRel.trim().equals("")) {
					dsFiltro += "Empresa " + cdEmpresaRel;
				}

			}

			if (this.cdEmpresaRel.trim().equals(cdEmpresaFinalRel.trim())) {

				Empresa empresa = EmpresaLocalizador.buscaEmpresa(ParametrosCliente.getCodigoCliente(),
						Inteiro.stoi(cdEmpresaFinalRel));

				if (empresa != null) {
					dsFiltro += "\n" + empresa.getDsRazaoSocial() + "\n";
				}
				parametrosSql.append(" and e.cd_empresa = '" + cdEmpresaRel + "'");

			} else {
				parametrosSql.append(
						" and e.cd_empresa >= '" + cdEmpresaRel + "' and e.cd_empresa <= '" + cdEmpresaFinalRel + "'");
				if (!this.cdEmpresaFinalRel.trim().equals("")) {
					dsFiltro += " à " + cdEmpresaFinalRel + "\n";
				}
			}

			if (this.dtInicial != null || this.dtFinal != null) {
				if (this.dtInicial != null) {
					dsFiltro += "Data " + this.dtInicial;
				}

				if (this.dtFinal != null) {
					dsFiltro += " à " + this.dtFinal + "\n";
				}
			}

			if (this.dtInicial.trim().equals(this.dtFinal.trim())) {
				parametrosSql
						.append(" and b.dt_inclusao = '" + new QtData(this.dtInicial).getQtDataAsTimestamp() + "'");
			} else {
				parametrosSql.append(" and b.dt_inclusao >= '" + new QtData(this.dtInicial).getQtDataAsTimestamp()
						+ "' and b.dt_inclusao <= '" + new QtData(this.dtFinal).getQtDataAsTimestamp() + "'");
			}

			StringBuilder cco = new StringBuilder();

			if (this.nrCco.equals("C")) {
				cco.append(" and nr_cco is not null ");
				dsFiltro += nrCco + "\n";
			} else if (this.nrCco.equals("S")) {
				cco.append(" and nr_cco is null ");
				dsFiltro += nrCco + "\n";
			}

			if (this.tpContratoPacote.equals("C")) {
				cco.append(" and coalesce( e.sn_contrato_pacote, 'N' ) = 'S' ");
				dsFiltro += "Pacote n�o \n";
			} else if (this.tpContratoPacote.equals("S")) {
				cco.append(" and coalesce( e.sn_contrato_pacote, 'N' ) <> 'S' ");
				dsFiltro += "Pacote Sim \n";
			}

			if (cco != null) {
				if (!cco.toString().equals("")) {
					filtros.put("nrCco", cco.toString());
				}
			}

			parametrosSql.append(cco.toString());

			if (this.idLotacao != null) {
				filtros.put("Lotacao", this.idLotacao + "");

				Lotacao lotacao = LotacaoLocalizador.buscaLotacao(ParametrosCliente.getCodigoCliente(),
						Inteiro.stoi(this.cdEmpresaRel), this.idLotacao);

				dsFiltro += "Lota��o " + idLotacao + (lotacao != null ? " - " + lotacao.getDsLotacao() : "") + "\n";
				parametrosSql.append(" and b.id_lotacao = '" + this.idLotacao + "'");
			}

			if (cdPlanoRel != null) {
				if (!this.cdPlanoRel.trim().equals("")) {
					dsFiltro += "Plano " + cdPlanoRel;
					parametrosSql.append(" and b.cd_plano = '" + this.cdPlanoRel + "'");
				}
			}

			if (snRelExcluidosData != null) {
				if (snRelExcluidosData.trim().equals("S")) {
					parametrosSql.append(" and b.dt_exclusao is null ");
				} else {
					parametrosSql.append(" and b.dt_exclusao is not null ");
				}
			}

			String rodape = "Emitido por " + usuarioLogado.getDsNomeUsuario()
					+ ": Benefici�rios / Movimento de Benefici�rios";
			filtros.put("rodape", rodape);

			filtros.put("ParametroSql", parametrosSql.toString());
			filtros.put("empresa_inicial", cdEmpresaRel);
			filtros.put("empresa_final", cdEmpresaFinalRel);

			filtros.put("excluidos",
					(this.snRelExcluidosData.trim().equals("") || this.snRelExcluidosData.equals("N") ? "off" : "on"));
			filtros.put("dsFiltro", dsFiltro);

			geraRelatorio(nmRelatorio, filtros);

			aviso("Relatorio Finalizado...");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void processoRelatorioBeneficiariosData() {
		String nmRelatorio = null;
		String dsFiltro = "";

		StringBuilder parametrosSql = new StringBuilder();

		try {
			if (this.tpRelOrdem.trim().equals("")) {
				this.tpRelOrdem = "A";
			}

			if (this.tpRelAcao.trim().equals("")) {
				this.tpRelAcao = "I";
			}

			if (this.tpRelAcao.equals("I")) {
				nmRelatorio = "relBeneficiariosIncluidosMes";
			} else {
				nmRelatorio = "relBeneficiariosExcluidosMes";
			}

			Hashtable<String, Object> filtros = new Hashtable<String, Object>();

			if (this.tpRelOrdem.equals("A")) {
				filtros.put("ordem", "alfabetica");
			} else if (this.tpRelOrdem.equals("A")) {
				filtros.put("ordem", "numerica");
			} else {
				filtros.put("ordem", "numerica");
			}
			String dtInicial, dtFinal;

			if (this.dtInicial == null) {
				dtInicial = "";
			} else {
				dtInicial = new QtData(this.dtInicial).getSoDataFormatoSQL();
			}
			if (this.dtFinal == null) {
				dtFinal = "";
			} else {
				dtFinal = new QtData(this.dtFinal).getSoDataFormatoSQL();
			}

			if (this.tpRelAcao.equals("I")) {
				filtros.put("data_inicial", dtInicial);
				filtros.put("data_final", dtFinal);
				dsFiltro += "Benefici�rios Inclu�dos \n";
			} else {
				filtros.put("data_inicial_excluidos", dtInicial);
				filtros.put("data_final_excluidos", dtFinal);
				dsFiltro += "Benefici�rios Exclu�dos \n";
			}

			if (cdPlanoRel != null) {
				if (!cdPlanoRel.trim().equals("")) {
					filtros.put("cd_plano", cdPlanoRel);
				}
			}
			if (!this.cdEmpresaRel.trim().equals("") || !this.cdEmpresaFinalRel.trim().equals("")) {
				if (!this.cdEmpresaRel.trim().equals("")) {
					dsFiltro += "Empresa " + cdEmpresaRel;
				}

			}

			if (this.cdEmpresaRel.trim().equals(cdEmpresaFinalRel.trim())) {

				Empresa empresa = EmpresaLocalizador.buscaEmpresa(ParametrosCliente.getCodigoCliente(),
						Inteiro.stoi(cdEmpresaFinalRel));

				if (empresa != null) {
					dsFiltro += "\n" + empresa.getDsRazaoSocial() + "\n";
				}
				parametrosSql.append(" and e.cd_empresa = '" + cdEmpresaRel + "'");

			} else {
				parametrosSql.append(
						" and e.cd_empresa >= '" + cdEmpresaRel + "' and e.cd_empresa <= '" + cdEmpresaFinalRel + "'");
				if (!this.cdEmpresaFinalRel.trim().equals("")) {
					dsFiltro += " à " + cdEmpresaFinalRel + "\n";
				}
			}

			if (this.dtInicial != null || this.dtFinal != null) {
				if (this.dtInicial != null) {
					dsFiltro += "Data " + this.dtInicial;
				}

				if (this.dtFinal != null) {
					dsFiltro += " à " + this.dtFinal + "\n";
				}
			}

			if (this.idLotacaoRelExame != null) {
				if (!this.idLotacaoRelExame.equals("") && !this.idLotacaoRelExame.equals(0)) {
					parametrosSql.append(" and b.id_lotacao = " + this.idLotacaoRelExame + " ");
				}
			}

			if (this.dtInicial.trim().equals(this.dtFinal.trim())) {
				parametrosSql
						.append(" and b.dt_inclusao = '" + new QtData(this.dtInicial).getQtDataAsTimestamp() + "'");
			} else {
				parametrosSql.append(" and b.dt_inclusao >= '" + new QtData(this.dtInicial).getQtDataAsTimestamp()
						+ "' and b.dt_inclusao <= '" + new QtData(this.dtFinal).getQtDataAsTimestamp() + "'");
			}

			StringBuilder cco = new StringBuilder();

			if (this.nrCco.equals("C")) {
				cco.append(" and nr_cco is not null ");
				dsFiltro += nrCco + "\n";
			} else if (this.nrCco.equals("S")) {
				cco.append(" and nr_cco is null ");
				dsFiltro += nrCco + "\n";
			}

			if (this.tpContratoPacote.equals("C")) {
				cco.append(" and coalesce( e.sn_contrato_pacote, 'N' ) = 'S' ");
				dsFiltro += "Pacote n�o \n";
			} else if (this.tpContratoPacote.equals("S")) {
				cco.append(" and coalesce( e.sn_contrato_pacote, 'N' ) <> 'S' ");
				dsFiltro += "Pacote Sim \n";
			}

			if (cco != null) {
				if (!cco.toString().equals("")) {
					filtros.put("nrCco", cco.toString());
				}
			}

			parametrosSql.append(cco.toString());

			if (this.idLotacao != null) {
				filtros.put("Lotacao", this.idLotacao + "");

				Lotacao lotacao = LotacaoLocalizador.buscaLotacao(ParametrosCliente.getCodigoCliente(),
						Inteiro.stoi(this.cdEmpresaRel), this.idLotacao);

				dsFiltro += "Lota��o " + idLotacao + (lotacao != null ? " - " + lotacao.getDsLotacao() : "") + "\n";
				parametrosSql.append(" and b.id_lotacao = '" + this.idLotacao + "'");
			}

			if (cdPlanoRel != null) {
				if (!this.cdPlanoRel.trim().equals("")) {
					dsFiltro += "Plano " + cdPlanoRel;
					parametrosSql.append(" and b.cd_plano = '" + this.cdPlanoRel + "'");
				}
			}

			if (snRelExcluidosData != null) {
				if (snRelExcluidosData.trim().equals("S")) {
					parametrosSql.append(" and b.dt_exclusao is null ");
				} else {
					parametrosSql.append(" and b.dt_exclusao is not null ");
				}
			}

			String rodape = "Emitido por " + usuarioLogado.getDsNomeUsuario()
					+ ": Benefici�rios / Movimento de Benefici�rios";
			filtros.put("rodape", rodape);
			filtros.put("idLotacao", "");

			filtros.put("ParametroSql", parametrosSql.toString());
			filtros.put("empresa_inicial", cdEmpresaRel);
			filtros.put("empresa_final", cdEmpresaFinalRel);

			filtros.put("excluidos",
					(this.snRelExcluidosData.trim().equals("") || this.snRelExcluidosData.equals("N") ? "off" : "on"));
			filtros.put("dsFiltro", dsFiltro);

			geraRelatorio(nmRelatorio, filtros);

			aviso("Relatorio Finalizado...");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void geraRelatorio(String nomeRelatorio, Hashtable<String, Object> parametros) {

		try {

			String pathJasper = (String) ParametrosSistema.getInstance().getParametro("dirRelPCMSO");

			parametros.put("imagem", pathJasper + "imgs/logo.jpg");
			pathJasper += "beneficiarios/";

			parametros.put("CaminhoPath", pathJasper);

			DadosRelatorioJasper relatorio = new DadosRelatorioJasper();

			relatorio.setBaseRelatorio(pathJasper + nomeRelatorio + ".jasper");
			relatorio.setParametros(parametros);

			Util.getSession().setAttribute("RelatorioPdf", relatorio);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void aviso(String msg) {
		msgAoUsuario = msg;

	}

	public String enviaEmail(Conexao cnx, BeneficiarioCbo pre, BeneficiarioCbo beneficiarioCboAnterior) {
		StringBuffer mensagem = new StringBuffer();
		StringBuffer mensagemAnterior = new StringBuffer();
		try {

			Email email = new Email();

			email.setPara(mensagemParametro.getDsDestinatarios());
			email.setDe("NoReply@quatro.com.br");

			email.setAssunto("Solicita��o de Pre-Cadastro de Beneficiario!!");

			if (usuarioLogado == null) {
				usuarioLogado = jUni.controleLogin.Util.getDadosUsuarioLogado();
			}

			mensagem.append("<b>");
			mensagem.append("Usuario " + usuarioLogado.getDsNomeUsuario() + " alterou ");

			mensagem.append("o Beneficiario:");
			if (pre.getCdBeneficiarioCartao() != null) {
				mensagem.append(pre.getCdBeneficiarioCartao());
			}

			mensagem.append("</b>");

			mensagem.append("<br/>");
			mensagem.append("<b>data alter��o: </b>");
			mensagem.append(new QtData().toString());
			mensagem.append("<br/>");
			mensagem.append("<br/>");

			jPcmso.persistencia.geral.e.Empresa e = jPcmso.persistencia.geral.e.EmpresaLocalizador
					.buscaEmpresa(pre.getCdEmpresa());
			if (e != null) {
				mensagem.append(getCampoHtml("EMPRESA", pre.getCdEmpresa() + " - " + e.getDsEmpresa()));
			}
			if (pre.getIdFilial() != null) {
				mensagem.append(getCampoHtml("FILIAL", getFilial(cnx, pre.getIdFilial())));
			} else {
				mensagem.append(getCampoHtml("FILIAL", "NÃO INFORMADO"));
			}

			mensagem.append(getCampoHtml("NOME BENEFICIARIO", pre.getDsNome()));
			
			mensagem.append("<br/>");
			mensagem.append("<br/>");
			mensagem.append("<br/>");
			mensagem.append("<b>Dados Atuais:</b><br/><br/>");

			if (pre.getDtAdmissao() != null) {
				if (beneficiarioCboAnterior.getDtAdmissao() == null || (!beneficiarioCboAnterior.getDtAdmissao().eIgual(pre.getDtAdmissao()))) {
					mensagemAnterior.append(getCampoHtml("DATA DE ADMISÃO", beneficiarioCboAnterior.getDtAdmissao().toString()));
					mensagem.append(getCampoHtml("DATA DE ADMISÃO", pre.getDtAdmissao().toString()));
				}
			}

			if (compara(beneficiarioCboAnterior.getTpSexo(), pre.getTpSexo())) {
				mensagemAnterior.append(getCampoHtml("SEXO", beneficiarioCboAnterior.getTpSexo().equals("M") ? "Masculino" : "Feminino"));
				if (pre.getTpSexo() != null) {
					mensagem.append(getCampoHtml("SEXO", pre.getTpSexo().equals("M") ? "Masculino" : "Feminino"));
				}
			}

			if (pre.getDtNascimento() != null) {
				if (beneficiarioCboAnterior.getDtNascimento() == null || (!beneficiarioCboAnterior.getDtNascimento().eIgual(pre.getDtNascimento()))) {
					mensagemAnterior.append(getCampoHtml("DATA DE NASCIMENTO", beneficiarioCboAnterior.getDtNascimento().toString()));
					mensagem.append(getCampoHtml("DATA DE NASCIMENTO", pre.getDtNascimento().toString()));
				}
			}

			if (compara(beneficiarioCboAnterior.getNrCpf(), pre.getNrCpf())) {
				mensagemAnterior.append(getCampoHtml("CPF", beneficiarioCboAnterior.getNrCpf() ) );
				if (pre.getNrCpf() != null) {
					mensagem.append(getCampoHtml("CPF", pre.getNrCpf()));
				}
			}

			if (compara(beneficiarioCboAnterior.getCdRg(), pre.getCdRg())) {
				mensagemAnterior.append(getCampoHtml("RG", beneficiarioCboAnterior.getCdRg()));
				if (pre.getCdRg() != null) {
					mensagem.append(getCampoHtml("RG", pre.getCdRg()));
				}
			}

			if (compara(beneficiarioCboAnterior.getDsNomeDaMae(), pre.getDsNomeDaMae())) {
				mensagemAnterior.append(getCampoHtml("NOME DA MÃE", beneficiarioCboAnterior.getDsNomeDaMae()));
				if (pre.getDsNomeDaMae() != null) {
					mensagem.append(getCampoHtml("NOME DA MÃE", pre.getDsNomeDaMae()));
				}
			}

			if (compara(beneficiarioCboAnterior.getTpEstadoCivil(), pre.getTpEstadoCivil())) {
				mensagemAnterior.append("<br/>");
				mensagemAnterior.append("<b>ESTADO CIVIL:&nbsp;</b>");
				if (beneficiarioCboAnterior.getTpEstadoCivil() != null) {
					if (beneficiarioCboAnterior.getTpEstadoCivil().equals("S")) {
						mensagemAnterior.append("Solteiro");
					} else if (beneficiarioCboAnterior.getTpEstadoCivil().equals("M")) {
						mensagemAnterior.append("Casado (M-Married)");
					} else if (beneficiarioCboAnterior.getTpEstadoCivil().equals("W")) {
						mensagemAnterior.append("Viuvo (W-Widow)");
					} else if (beneficiarioCboAnterior.getTpEstadoCivil().equals("D")) {
						mensagemAnterior.append("Divorciado");
					} else if (beneficiarioCboAnterior.getTpEstadoCivil().equals("A")) {
						mensagemAnterior.append("Apartado (Separado)");
					} else if (beneficiarioCboAnterior.getTpEstadoCivil().equals("U")) {
						mensagemAnterior.append("Uni�o Est�vel");
					}
				}
				if (pre.getTpEstadoCivil() != null) {
					mensagem.append("<br/>");
					mensagem.append("<b>ESTADO CIVIL:&nbsp;</b>");
					if (pre.getTpEstadoCivil().equals("S")) {
						mensagem.append("Solteiro");
					} else if (pre.getTpEstadoCivil().equals("M")) {
						mensagem.append("Casado (M-Married)");
					} else if (pre.getTpEstadoCivil().equals("W")) {
						mensagem.append("Viuvo (W-Widow)");
					} else if (pre.getTpEstadoCivil().equals("D")) {
						mensagem.append("Divorciado");
					} else if (pre.getTpEstadoCivil().equals("A")) {
						mensagem.append("Apartado (Separado)");
					} else if (pre.getTpEstadoCivil().equals("U")) {
						mensagem.append("Uni�o Est�vel");
					}
				}
			}

			if (compara(beneficiarioCboAnterior.getDsAtividade(), pre.getDsAtividade())) {
				mensagemAnterior.append(getCampoHtml("ATIVIDADE", beneficiarioCboAnterior.getDsAtividade()));
				if (pre.getDsAtividade() != null) {
					mensagem.append(getCampoHtml("ATIVIDADE", pre.getDsAtividade()));
				}
			}

			if (compara(beneficiarioCboAnterior.getNrPis(), pre.getNrPis())) {
				mensagemAnterior.append(getCampoHtml("PIS/Pasep", beneficiarioCboAnterior.getNrPis()));
				if (pre.getNrPis() != null) {
					mensagem.append(getCampoHtml("PIS/Pasep", pre.getNrPis()));
				}
			}

			if (compara(beneficiarioCboAnterior.getNrCtps(), pre.getNrCtps())) {
				mensagemAnterior.append(getCampoHtml("CTPS", beneficiarioCboAnterior.getNrCtps()));
				if (pre.getNrCtps() != null) {
					mensagem.append(getCampoHtml("CTPS", pre.getNrCtps()));
				}
			}

			if (compara(beneficiarioCboAnterior.getNrSerieUf(), pre.getNrSerieUf())) {
				mensagemAnterior.append(getCampoHtml("SERIE (UF)", beneficiarioCboAnterior.getNrSerieUf()));
				if (pre.getNrSerieUf() != null) {
					mensagem.append(getCampoHtml("SERIE (UF)", pre.getNrSerieUf()));
				}
			}

			if (compara(beneficiarioCboAnterior.getCdCbo(), pre.getCdCbo())) {
				mensagemAnterior.append("<br/>");
				mensagemAnterior.append("<b>CBO:&nbsp;</b>");
				if (beneficiarioCboAnterior.getCdCbo() != null) {
					Cbo cbo = CboLocalizador.buscaCbo(beneficiarioCboAnterior.getCdCbo());
					if (cbo != null) {
						mensagemAnterior.append(beneficiarioCboAnterior.getCdCbo() + " - " + cbo.getNmCbo());
					} else {
						mensagemAnterior.append(beneficiarioCboAnterior.getCdCbo());
					}
				}
				
				if (pre.getCdCbo() != null) {
					mensagem.append("<br/>");
					mensagem.append("<b>CBO:&nbsp;</b>");
					Cbo cbo = CboLocalizador.buscaCbo(pre.getCdCbo());
					if (cbo != null) {
						mensagem.append(pre.getCdCbo() + " - " + cbo.getNmCbo());
					} else {
						mensagem.append(pre.getCdCbo());
					}
				}
			}

			if (compara(beneficiarioCboAnterior.getIdFuncao() + "", pre.getIdFuncao() + "")) {
				if( beneficiarioCboAnterior.getIdFuncao() != null ) {
					Funcao f = FuncaoLocalizador.buscaFuncao(beneficiarioCboAnterior.getIdFuncao());
					if (f != null) {
						mensagemAnterior.append(getCampoHtml("FUN�ÃO", f.getDsFuncao()));
					} else {
						mensagemAnterior.append(getCampoHtml("FUN�ÃO", "" ) );
					}
				} else {
					mensagemAnterior.append(getCampoHtml("FUN�ÃO", "" ) );
				}
 				Funcao f = FuncaoLocalizador.buscaFuncao(pre.getIdFuncao());
				if (f != null) {
					mensagem.append(getCampoHtml("FUNǇÃO", f.getDsFuncao()));
				}
			}

			if (compara(beneficiarioCboAnterior.getIdSetor() + "", pre.getIdSetor() + "")) {
				if (beneficiarioCboAnterior.getIdSetor() != null) {
					Setor s = SetorLocalizador.buscaSetor(beneficiarioCboAnterior.getIdSetor());
					if (s != null) {
						mensagemAnterior.append(getCampoHtml("SETOR", s.getDsSetor()));
					} else {
						mensagemAnterior.append(getCampoHtml("SETOR", "" ) );
					}
				} else {
					mensagemAnterior.append(getCampoHtml("SETOR", "" ) );
				}
				if (pre.getIdSetor() != null) {
					Setor s = SetorLocalizador.buscaSetor(pre.getIdSetor());
					if (s != null) {
						mensagem.append(getCampoHtml("SETOR", s.getDsSetor()));
					}
				}
			}

			if (compara(beneficiarioCboAnterior.getIdCargo() + "", pre.getIdCargo() + "")) {
				if( beneficiarioCboAnterior.getIdCargo() != null ) {
					Cargo c = CargoLocalizador.buscaCargo(beneficiarioCboAnterior.getIdCargo());
					if (c != null) {
						mensagemAnterior.append(getCampoHtml("CARGO", c.getDsCargo()));
					} else {
						mensagemAnterior.append(getCampoHtml("CARGO", "" ) );
					}
				} else {
					mensagemAnterior.append(getCampoHtml("CARGO", "" ) );
				}
				Cargo c = CargoLocalizador.buscaCargo(pre.getIdCargo());
				if (c != null) {
					mensagem.append(getCampoHtml("CARGO", c.getDsCargo()));
				}
			}

			if (compara(beneficiarioCboAnterior.getIdLotacao() + "", pre.getIdLotacao() + "")) {
				
				if (beneficiarioCboAnterior.getIdLotacao() != null) {
					Lotacao lotacao = LotacaoLocalizador.buscaLotacao(ParametrosCliente.getCodigoCliente(),
							Inteiro.stoi(beneficiarioCboAnterior.getCdEmpresa()), pre.getIdLotacao());
					if (lotacao != null) {
						mensagemAnterior.append( getCampoHtml("LOTACAO", lotacao.getIdLotacao() + ": " + lotacao.getDsLotacao()));
					} else {
						mensagemAnterior.append( getCampoHtml("LOTACAO", "" ) );
					}
				} else {
					mensagemAnterior.append( getCampoHtml("LOTACAO", "" ) );
				}
				
				if (pre.getIdLotacao() != null) {
					Lotacao lotacao = LotacaoLocalizador.buscaLotacao(ParametrosCliente.getCodigoCliente(),
							Inteiro.stoi(pre.getCdEmpresa()), pre.getIdLotacao());
					if (lotacao != null) {
						mensagem.append(
								getCampoHtml("LOTACAO", lotacao.getIdLotacao() + ": " + lotacao.getDsLotacao()));
					}
				}
			}

			if (compara(jUni.biblioteca.util.Util.mascaraFloat(beneficiarioCboAnterior.getVlPeso()), jUni.biblioteca.util.Util.mascaraFloat(pre.getVlPeso()))) {
				if (beneficiarioCboAnterior.getVlPeso() != null) {
					mensagemAnterior.append(getCampoHtml("PESO", jUni.biblioteca.util.Util.mascaraFloat(beneficiarioCboAnterior.getVlPeso())));
				} else {
					mensagemAnterior.append(getCampoHtml("PESO", "" ) );
				}
				if (pre.getVlPeso() != null) {
					mensagem.append(getCampoHtml("PESO", jUni.biblioteca.util.Util.mascaraFloat(pre.getVlPeso())));
				}
			}

			if (compara(jUni.biblioteca.util.Util.mascaraFloat(beneficiarioCboAnterior.getVlAltura()), jUni.biblioteca.util.Util.mascaraFloat(pre.getVlAltura()))) {
				if (beneficiarioCboAnterior.getVlPeso() != null) {
					mensagemAnterior.append(getCampoHtml("ALTURA", jUni.biblioteca.util.Util.mascaraFloat(beneficiarioCboAnterior.getVlAltura())));
				} else {
					mensagemAnterior.append(getCampoHtml("ALTURA", "" ) );
				}
				if (pre.getVlPeso() != null) {
					mensagem.append(getCampoHtml("ALTURA", jUni.biblioteca.util.Util.mascaraFloat(pre.getVlAltura())));
				}
			}

			if (compara(beneficiarioCboAnterior.isSnPossuiDeficiencia(), pre.isSnPossuiDeficiencia())) {
				mensagemAnterior.append(getCampoHtml("Possui alguma defici�ncia", beneficiarioCboAnterior.isSnPossuiDeficiencia() ? "SIM" : "NÃO"));
				mensagem.append(getCampoHtml("Possui alguma defici�ncia", pre.isSnPossuiDeficiencia() ? "SIM" : "NÃO"));
			}

			mensagem.append("<br/>");
			mensagem.append("<br/>");
			mensagem.append("<br/>");
			mensagem.append("<b>Dados Anteriores:</b><br/><br/>");
			mensagem.append( mensagemAnterior );
			
			email.setMensagem(getFormatoHTML(mensagem.toString()));
			email.setConteudo(Email.TEXT_HTML);

			mensagemEnviada.setDsMensagem(getFormatoHTML(mensagem.toString()));

			String smtpHost = (String) ParametrosSistema.getInstance().getParametro("SMTP-Host");
			String smtpPort = (String) ParametrosSistema.getInstance().getParametro("SMTP-Port");
			String contaEMail = (String) ParametrosSistema.getInstance().getParametro("contaEMail");
			String senhaEMail = (String) ParametrosSistema.getInstance().getParametro("senhaEMail");

			Mailer mailer = new Mailer(email, smtpHost, smtpPort, contaEMail, senhaEMail);
			mailer.send();

		} catch (Exception e) {
			e.printStackTrace();
			mensagemEnviada.setDsErroEmail(e.getMessage());
		}
		return mensagem.toString();
	}
	
	public void enviaEmailRetornoAoTrabalho(Conexao cnx, Beneficiario pre) {
		try {
			if (usuarioLogado == null) {
				usuarioLogado = jUni.controleLogin.Util.getDadosUsuarioLogado();
			}
			initMensagemParametro(cnx);
			
			Email email = new Email();

			mensagemEnviada = new MensagemEnviada();
			mensagemEnviada.setDtMensagem(new QtData());
			
			email.setPara(mensagemParametro.getDsDestinatarios());
			email.setDe("NoReply@quatro.com.br");

			email.setAssunto("Solicita��o de Retorno ao Trabalho");

			if (usuarioLogado == null) {
				usuarioLogado = jUni.controleLogin.Util.getDadosUsuarioLogado();
			}

			StringBuffer mensagem = new StringBuffer();
			mensagem.append("<b>");
			mensagem.append("Usuario " + usuarioLogado.getDsNomeUsuario() + " solicitou Retorno ao Trabalho ");

			mensagem.append(" do Beneficiario:");
			if (pre.getCdBeneficiarioCartao() != null) {
				mensagem.append(pre.getCdBeneficiarioCartao());
			}

			mensagem.append("</b>");

			mensagem.append("<br/>");
			mensagem.append("<b>data Solicita��o: </b>");
			mensagem.append(new QtData().toString());
			mensagem.append("<br/>");
			mensagem.append("<br/>");
			mensagem.append("<br/>");
			mensagem.append("<br/>");

			jPcmso.persistencia.geral.e.Empresa e = jPcmso.persistencia.geral.e.EmpresaLocalizador
					.buscaEmpresa(pre.getCdEmpresa());
			if (e != null) {
				mensagem.append(getCampoHtml("EMPRESA", pre.getCdEmpresa() + " - " + e.getDsEmpresa()));
			}

			mensagem.append(getCampoHtml("NOME BENEFICIARIO", pre.getDsNome()));
			mensagem.append(getCampoHtml("DATA RETORNO", dtAdmissaoRetorno ) );
			mensagem.append(getCampoHtml("MOTIVO RETORNO", motivoRetorno ) );

			email.setMensagem(getFormatoHTML(new String(mensagem.toString().getBytes("UTF-8"))));
			email.setConteudo(Email.TEXT_HTML);

			mensagemEnviada.setDsMensagem(getFormatoHTML(mensagem.toString()));

			DadosSolicitacaoPCMSO dados = new DadosSolicitacaoPCMSO();
			dados.setIdUsuarioSolicitou(usuarioLogado.getIdUsuario());
			dados.setCdUnimed(pre.getCdUnimed());
			dados.setCdEmpresa(pre.getCdEmpresa());
			dados.setCdFamilia(pre.getCdFamilia());
			dados.setCdDependencia(pre.getCdDependencia());
			dados.setCdDigito(pre.getCdDigito());
			dados.setNmBeneficiario(pre.getDsNome());
			dados.setDsObservacoes(new String(mensagem.toString().getBytes("UTF-8")));
			dados.setDtSolicitacao(new QtData());
			dados.setTpSolicitacao("R");
			dados.setDtConfirmacao(null);
			dados.setIdUsuarioConfirmacao(null);

			SolicitacaoPCMSO solicitacao = new SolicitacaoPCMSO(dados);
			solicitacao.incluiSolicitacao(cnx);

			String smtpHost = (String) ParametrosSistema.getInstance().getParametro("SMTP-Host");
			String smtpPort = (String) ParametrosSistema.getInstance().getParametro("SMTP-Port");
			String contaEMail = (String) ParametrosSistema.getInstance().getParametro("contaEMail");
			String senhaEMail = (String) ParametrosSistema.getInstance().getParametro("senhaEMail");

			Mailer mailer = new Mailer(email, smtpHost, smtpPort, contaEMail, senhaEMail);
			mailer.send();
			
			email.setAssunto("Confirma�nao de Solicita��o de Retorno ao Trabalho");
			email.setPara( usuarioLogado.getDsEmail() );
			
			mailer = new Mailer(email, smtpHost, smtpPort, contaEMail, senhaEMail);
			mailer.send();

		} catch (Exception e) {
			e.printStackTrace();
			mensagemEnviada.setDsErroEmail(e.getMessage());
		}
	}

	public void enviaEmailPPP(Conexao cnx, Beneficiario pre) {
		try {
			if (usuarioLogado == null) {
				usuarioLogado = jUni.controleLogin.Util.getDadosUsuarioLogado();
			}
			Email email = new Email();

			mensagemEnviada = new MensagemEnviada();
			mensagemEnviada.setDtMensagem(new QtData());
			email.setPara(mensagemParametro.getDsDestinatarios());
			// email.setPara("luciont@unimedvaledocai.com.br");
			email.setDe("NoReply@quatro.com.br");

			email.setAssunto("Solicita��o de PPP de Beneficiario!!");

			if (usuarioLogado == null) {
				usuarioLogado = jUni.controleLogin.Util.getDadosUsuarioLogado();
			}

			StringBuffer mensagem = new StringBuffer();
			mensagem.append("<b>");
			mensagem.append("Usuario " + usuarioLogado.getDsNomeUsuario() + " solicitou PPP ");

			mensagem.append(" do Beneficiario:");
			if (pre.getCdBeneficiarioCartao() != null) {
				mensagem.append(pre.getCdBeneficiarioCartao());
			}

			mensagem.append("</b>");

			mensagem.append("<br/>");
			mensagem.append("<b>data Solicita��o: </b>");
			mensagem.append(new QtData().toString());
			mensagem.append("<br/>");
			mensagem.append("<br/>");
			mensagem.append("<br/>");
			mensagem.append("<br/>");

			jPcmso.persistencia.geral.e.Empresa e = jPcmso.persistencia.geral.e.EmpresaLocalizador
					.buscaEmpresa(pre.getCdEmpresa());
			if (e != null) {
				mensagem.append(getCampoHtml("EMPRESA", pre.getCdEmpresa() + " - " + e.getDsEmpresa()));
			}

			mensagem.append(getCampoHtml("NOME BENEFICIARIO", pre.getDsNome()));

			if (pre.getTpSexo() != null) {
				mensagem.append(getCampoHtml("SEXO", pre.getTpSexo().equals("M") ? "Masculino" : "Feminino"));
			}

			if (pre.getDtNascimento() != null) {
				mensagem.append(getCampoHtml("DATA DE NASCIMENTO", pre.getDtNascimento().toString()));
			}

			if (pre.getCdCpf() != null) {
				mensagem.append(getCampoHtml("CPF", pre.getCdCpf()));
			}

			if (pre.getCdRg() != null) {
				mensagem.append(getCampoHtml("RG", pre.getCdRg()));
			}

			if (pre.getDsNomeDaMae() != null) {
				mensagem.append(getCampoHtml("NOME DA MÃE", pre.getDsNomeDaMae()));
			}

			if (pre.getTpEstadoCivil() != null) {
				mensagem.append("<br/>");
				mensagem.append("<b>ESTADO CIVIL:&nbsp;</b>");
				if (pre.getTpEstadoCivil().equals("S")) {
					mensagem.append("Solteiro");
				} else if (pre.getTpEstadoCivil().equals("M")) {
					mensagem.append("Casado (M-Married)");
				} else if (pre.getTpEstadoCivil().equals("W")) {
					mensagem.append("Viuvo (W-Widow)");
				} else if (pre.getTpEstadoCivil().equals("D")) {
					mensagem.append("Divorciado");
				} else if (pre.getTpEstadoCivil().equals("A")) {
					mensagem.append("Apartado (Separado)");
				} else if (pre.getTpEstadoCivil().equals("U")) {
					mensagem.append("Uni�o Est�vel");
				}

			}

			if (pre.getIdLotacao() != null) {
				Lotacao lotacao = LotacaoLocalizador.buscaLotacao(ParametrosCliente.getCodigoCliente(),
						Inteiro.stoi(pre.getCdEmpresa()), pre.getIdLotacao());
				if (lotacao != null) {
					mensagem.append(getCampoHtml("LOTACAO", lotacao.getIdLotacao() + ": " + lotacao.getDsLotacao()));
				}
			}

			if (dtAdmissaoPPP != null) {
				mensagem.append(getCampoHtml("Data Admiss�o", dtAdmissaoPPP));
			}
			if (dtDemissaoPPP != null) {
				mensagem.append(getCampoHtml("Data Demissao", dtDemissaoPPP));
			}
			if (dtNascimentoPPP != null) {
				mensagem.append(getCampoHtml("Data Nascimento", dtNascimentoPPP));
			}
			if (nrNitPPP != null) {
				mensagem.append(getCampoHtml("NIT/PIS", nrNitPPP));
			}
			if (nrCtpsPPP != null) {
				mensagem.append(getCampoHtml("N� CTPS", nrCtpsPPP));
			}
			if (nrSeriePPP != null) {
				mensagem.append(getCampoHtml("N� de S�rie", nrSeriePPP));
			}
			if (dsUFPPP != null) {
				mensagem.append(getCampoHtml("UF", dsUFPPP));
			}
			if (snAcidentePPP != null) {
				mensagem.append(getCampoHtml("Acidente de Trabalho", snAcidentePPP));
			}
			if (dtAcidentePPP != null) {
				mensagem.append(getCampoHtml("Data do Acidente", dtAcidentePPP));
			}
			if (nrCatPPP != null) {
				mensagem.append(getCampoHtml("CAT", nrCatPPP));
			}
			if (nmResponsavelPPP != null) {
				mensagem.append(getCampoHtml("Nome do Respons�vel PPP", nmResponsavelPPP));
			}
			if (nrNitResponsavelPPP != null) {
				mensagem.append(getCampoHtml("NIT/PIS Respons�vel PPP", nrNitResponsavelPPP));
			}
			mensagem.append(getCampoHtml("1 Fun��o - Admiss�o", " "));
			if (dtAdmissaoFucaoPPP != null) {
				mensagem.append(getCampoHtml("Data da Admiss�o", dtAdmissaoFucaoPPP));
			}
			if (dtFimFincaoFuncaoPPP != null) {
				mensagem.append(getCampoHtml("Data Fim da Fun��o", dtFimFincaoFuncaoPPP));
			}
			if (dsFuncaoFuncaoPPP != null) {
				mensagem.append(getCampoHtml("Fun��o", dsFuncaoFuncaoPPP));
			}
			if (dsCBOFuncaoPPP != null) {
				mensagem.append(getCampoHtml("CBO", dsCBOFuncaoPPP));
			}
			if (dsSetorFuncaoPPP != null) {
				mensagem.append(getCampoHtml("Setor", dsSetorFuncaoPPP));
			}
			mensagem.append(getCampoHtml("Troca de Fun��o", " "));
			if (dtMudancaTrocaPPP != null) {
				mensagem.append(getCampoHtml("Data da Mudan�a", dtMudancaTrocaPPP));
			}
			if (dtFimMudancaTrocaPPP != null) {
				mensagem.append(getCampoHtml("Data fim da Mudan�a", dtFimMudancaTrocaPPP));
			}
			if (dsFuncaoTrocaPPP != null) {
				mensagem.append(getCampoHtml("Fun��o", dsFuncaoTrocaPPP));
			}
			if (dsCBOTrocaPPP != null) {
				mensagem.append(getCampoHtml("CBO", dsCBOTrocaPPP));
			}
			if (dsSetorTrocaPPP != null) {
				mensagem.append(getCampoHtml("Setor", dsSetorTrocaPPP));
			}
			if (dsObervacoesPPP != null) {
				mensagem.append(getCampoHtml("Observa��es", dsObervacoesPPP));
			}

			email.setMensagem(getFormatoHTML(new String(mensagem.toString().getBytes("UTF-8"))));
			email.setConteudo(Email.TEXT_HTML);

			mensagemEnviada.setDsMensagem(getFormatoHTML(mensagem.toString()));

			DadosSolicitacaoPCMSO dados = new DadosSolicitacaoPCMSO();
			dados.setIdUsuarioSolicitou(usuarioLogado.getIdUsuario());
			dados.setCdUnimed(pre.getCdUnimed());
			dados.setCdEmpresa(pre.getCdEmpresa());
			dados.setCdFamilia(pre.getCdFamilia());
			dados.setCdDependencia(pre.getCdDependencia());
			dados.setCdDigito(pre.getCdDigito());
			dados.setNmBeneficiario(pre.getDsNome());
			dados.setDsObservacoes(new String(mensagem.toString().getBytes("UTF-8")));
			dados.setDtSolicitacao(new QtData());
			dados.setTpSolicitacao("P");
			dados.setDtConfirmacao(null);
			dados.setIdUsuarioConfirmacao(null);

			SolicitacaoPCMSO solicitacao = new SolicitacaoPCMSO(dados);
			solicitacao.incluiSolicitacao(cnx);

			String smtpHost = (String) ParametrosSistema.getInstance().getParametro("SMTP-Host");
			String smtpPort = (String) ParametrosSistema.getInstance().getParametro("SMTP-Port");
			String contaEMail = (String) ParametrosSistema.getInstance().getParametro("contaEMail");
			String senhaEMail = (String) ParametrosSistema.getInstance().getParametro("senhaEMail");

			Mailer mailer = new Mailer(email, smtpHost, smtpPort, contaEMail, senhaEMail);
			mailer.send();

		} catch (Exception e) {
			e.printStackTrace();
			mensagemEnviada.setDsErroEmail(e.getMessage());
		}
	}

	public static String getFormatoHTML(String string) {

		if (string != null && !string.trim().isEmpty()) {

			string = string.replaceAll("\n", "<br />");
			string = string.replaceAll("�", "&aacute;");
			string = string.replaceAll("Á", "&Aacute;");
			string = string.replaceAll("�", "&eacute;");
			string = string.replaceAll("É", "&Eacute;");
			string = string.replaceAll("í", "&iacute;");
			string = string.replaceAll("Í", "&Iacute;");
			string = string.replaceAll("�", "&oacute;");
			string = string.replaceAll("Ó", "&Oacute;");
			string = string.replaceAll("ú", "&uacute;");
			string = string.replaceAll("Ú", "&Uacute;");
			string = string.replaceAll("�", "&atilde;");
			string = string.replaceAll("Ã", "&Atilde;");
			string = string.replaceAll("õ", "&otilde;");
			string = string.replaceAll("Õ", "&Otilde;");
			string = string.replaceAll("â", "&acirc;");
			string = string.replaceAll("Â", "&Acirc;");
			string = string.replaceAll("ê", "&ecirc;");
			string = string.replaceAll("Ê", "&Ecirc;");
			string = string.replaceAll("ô", "&ocirc;");
			string = string.replaceAll("Ô", "&Ocirc;");
			string = string.replaceAll("à", "&agrave;");
			string = string.replaceAll("À", "&Agrave;");
			string = string.replaceAll("�", "&ccedil;");
			string = string.replaceAll("Ç", "&Ccedil;");
			string = string.replaceAll("ü", "&uuml;");
			string = string.replaceAll("Ü", "&Uuml;");
			string = string.replaceAll("º", "&#186;");

			return string;
		}

		return "&nbsp;";
	}

	private void enviaEmailReinclusao(Conexao cnx, BeneficiarioCbo pre, BeneficiarioCbo beneficiarioCboAnterior) {
		try {

			Email email = new Email();

			email.setPara(mensagemParametro.getDsDestinatarios());
			// email.setPara("luciont@unimedvaledocai.com.br");
			email.setDe("NoReply@quatro.com.br");

			email.setAssunto("Reativa��o de Benefici�rio!");

			if (usuarioLogado == null) {
				usuarioLogado = jUni.controleLogin.Util.getDadosUsuarioLogado();
			}

			StringBuffer mensagem = new StringBuffer();
			mensagem.append("<b>");
			mensagem.append("Usuario " + usuarioLogado.getDsNomeUsuario() + " reativou ");

			mensagem.append(" o Beneficiario:");
			if (pre.getCdBeneficiarioCartao() != null) {
				mensagem.append(pre.getCdBeneficiarioCartao());
			}

			mensagem.append("</b>");

			mensagem.append("<br/>");
			mensagem.append("<b>data Reativa��o: </b>");
			mensagem.append(new QtData().toString());
			mensagem.append("<br/>");
			mensagem.append("<br/>");
			mensagem.append("<br/>");
			mensagem.append("<br/>");

			jPcmso.persistencia.geral.e.Empresa e = jPcmso.persistencia.geral.e.EmpresaLocalizador
					.buscaEmpresa(pre.getCdEmpresa());
			if (e != null) {
				mensagem.append(getCampoHtml("EMPRESA", pre.getCdEmpresa() + " - " + e.getDsEmpresa()));
			}

			mensagem.append(getCampoHtml("NOME BENEFICIARIO", pre.getDsNome()));
			if (pre.getIdFilial() != null) {
				mensagem.append(getCampoHtml("FILIAL", getFilial(cnx, pre.getIdFilial())));
			} else {
				mensagem.append(getCampoHtml("FILIAL", "NÃO INFORMADO"));
			}
			if (pre.getIdLotacao() != null) {
				mensagem.append(getCampoHtml("LOTACAO", getLotacao(cnx, pre.getIdLotacao())));
			} else {
				mensagem.append(getCampoHtml("LOTACAO", "NÃO INFORMADO"));
			}
			if (pre.getIdFuncao() != null) {
				mensagem.append(getCampoHtml("FUNÇÃO", getFuncao(cnx, pre.getIdFuncao())));
			} else {
				mensagem.append(getCampoHtml("FUNÇÃO", "NÃO INFORMADO"));
			}

			if (beneficiarioCboAnterior != null) {
				if (pre.getDtAdmissao() != null) {
					if (beneficiarioCboAnterior.getDtAdmissao() == null
							|| (!beneficiarioCboAnterior.getDtAdmissao().eIgual(pre.getDtAdmissao()))) {
						mensagem.append(getCampoHtml("DATA DE ADMISÃO", pre.getDtAdmissao().toString()));
					}
				}
				if (compara(beneficiarioCboAnterior.getTpSexo(), pre.getTpSexo())) {
					if (pre.getTpSexo() != null) {
						mensagem.append(getCampoHtml("SEXO", pre.getTpSexo().equals("M") ? "Masculino" : "Feminino"));
					}
				}

				if (pre.getDtNascimento() != null) {
					if (beneficiarioCboAnterior.getDtNascimento() == null
							|| (!beneficiarioCboAnterior.getDtNascimento().eIgual(pre.getDtNascimento()))) {
						mensagem.append(getCampoHtml("DATA DE NASCIMENTO", pre.getDtNascimento().toString()));
					}
				}

				if (compara(beneficiarioCboAnterior.getNrCpf(), pre.getNrCpf())) {
					if (pre.getNrCpf() != null) {
						mensagem.append(getCampoHtml("CPF", pre.getNrCpf()));
					}
				}

				if (compara(beneficiarioCboAnterior.getCdRg(), pre.getCdRg())) {
					if (pre.getCdRg() != null) {
						mensagem.append(getCampoHtml("RG", pre.getCdRg()));
					}
				}

				if (compara(beneficiarioCboAnterior.getDsNomeDaMae(), pre.getDsNomeDaMae())) {
					if (pre.getDsNomeDaMae() != null) {
						mensagem.append(getCampoHtml("NOME DA MÃE", pre.getDsNomeDaMae()));
					}
				}

				if (compara(beneficiarioCboAnterior.getTpEstadoCivil(), pre.getTpEstadoCivil())) {
					if (pre.getTpEstadoCivil() != null) {
						mensagem.append("<br/>");
						mensagem.append("<b>ESTADO CIVIL:&nbsp;</b>");
						if (pre.getTpEstadoCivil().equals("S")) {
							mensagem.append("Solteiro");
						} else if (pre.getTpEstadoCivil().equals("M")) {
							mensagem.append("Casado (M-Married)");
						} else if (pre.getTpEstadoCivil().equals("W")) {
							mensagem.append("Viuvo (W-Widow)");
						} else if (pre.getTpEstadoCivil().equals("D")) {
							mensagem.append("Divorciado");
						} else if (pre.getTpEstadoCivil().equals("A")) {
							mensagem.append("Apartado (Separado)");
						} else if (pre.getTpEstadoCivil().equals("U")) {
							mensagem.append("Uni�o Est�vel");
						}

					}
				}

				if (compara(beneficiarioCboAnterior.getDsAtividade(), pre.getDsAtividade())) {
					if (pre.getDsAtividade() != null) {
						mensagem.append(getCampoHtml("ATIVIDADE", pre.getDsAtividade()));
					}
				}

				if (compara(beneficiarioCboAnterior.getNrPis(), pre.getNrPis())) {
					if (pre.getNrPis() != null) {
						mensagem.append(getCampoHtml("PIS/Pasep", pre.getNrPis()));
					}
				}

				if (compara(beneficiarioCboAnterior.getNrCtps(), pre.getNrCtps())) {
					if (pre.getNrCtps() != null) {
						mensagem.append(getCampoHtml("CTPS", pre.getNrCtps()));
					}
				}

				if (compara(beneficiarioCboAnterior.getNrSerieUf(), pre.getNrSerieUf())) {
					if (pre.getNrSerieUf() != null) {
						mensagem.append(getCampoHtml("SERIE (UF)", pre.getNrSerieUf()));
					}
				}

				if (compara(beneficiarioCboAnterior.getCdCbo(), pre.getCdCbo())) {
					if (pre.getCdCbo() != null) {
						mensagem.append("<br/>");
						mensagem.append("<b>CBO:&nbsp;</b>");
						Cbo cbo = CboLocalizador.buscaCbo(pre.getCdCbo());
						if (cbo != null) {
							mensagem.append(cbo.getCdCbo() + " - " + cbo.getNmCbo());
						} else {
							mensagem.append(pre.getCdCbo());
						}
					}
				}

				if (compara(beneficiarioCboAnterior.getIdFuncao() + "", pre.getIdFuncao() + "")) {
					Funcao f = FuncaoLocalizador.buscaFuncao(pre.getIdFuncao());
					if (f != null) {
						mensagem.append(getCampoHtml("FUNÇÃO", f.getDsFuncao()));
					}
				}

				if (compara(beneficiarioCboAnterior.getIdSetor() + "", pre.getIdSetor() + "")) {
					if (pre.getIdSetor() != null) {
						Setor s = SetorLocalizador.buscaSetor(pre.getIdSetor());
						if (s != null) {
							mensagem.append(getCampoHtml("SETOR", s.getDsSetor()));
						}
					}
				}

				if (compara(beneficiarioCboAnterior.getIdCargo() + "", pre.getIdCargo() + "")) {
					Cargo c = CargoLocalizador.buscaCargo(pre.getIdCargo());
					if (c != null) {
						mensagem.append(getCampoHtml("CARGO", c.getDsCargo()));
					}
				}

				if (compara(beneficiarioCboAnterior.getIdLotacao() + "", pre.getIdLotacao() + "")) {
					if (pre.getIdLotacao() != null) {
						Lotacao lotacao = LotacaoLocalizador.buscaLotacao(ParametrosCliente.getCodigoCliente(),
								Inteiro.stoi(pre.getCdEmpresa()), pre.getIdLotacao());
						if (lotacao != null) {
							mensagem.append(
									getCampoHtml("LOTACAO", lotacao.getIdLotacao() + ": " + lotacao.getDsLotacao()));
						}
					}
				}

				if (compara(jUni.biblioteca.util.Util.mascaraFloat(beneficiarioCboAnterior.getVlPeso()),
						jUni.biblioteca.util.Util.mascaraFloat(pre.getVlPeso()))) {
					if (pre.getVlPeso() != null) {
						mensagem.append(getCampoHtml("PESO", jUni.biblioteca.util.Util.mascaraFloat(pre.getVlPeso())));
					}
				}

				if (compara(jUni.biblioteca.util.Util.mascaraFloat(beneficiarioCboAnterior.getVlAltura()),
						jUni.biblioteca.util.Util.mascaraFloat(pre.getVlAltura()))) {
					if (pre.getVlPeso() != null) {
						mensagem.append(
								getCampoHtml("ALTURA", jUni.biblioteca.util.Util.mascaraFloat(pre.getVlAltura())));
					}
				}

				if (compara(beneficiarioCboAnterior.isSnPossuiDeficiencia(), pre.isSnPossuiDeficiencia())) {
					mensagem.append(
							getCampoHtml("Possui alguma deficiência", pre.isSnPossuiDeficiencia() ? "SIM" : "NÃO"));
				}
			}

			email.setMensagem(getFormatoHTML(mensagem.toString()));
			email.setConteudo(Email.TEXT_HTML);

			mensagemEnviada.setDsMensagem(getFormatoHTML(mensagem.toString()));

			String smtpHost = (String) ParametrosSistema.getInstance().getParametro("SMTP-Host");
			String smtpPort = (String) ParametrosSistema.getInstance().getParametro("SMTP-Port");
			String contaEMail = (String) ParametrosSistema.getInstance().getParametro("contaEMail");
			String senhaEMail = (String) ParametrosSistema.getInstance().getParametro("senhaEMail");

			Mailer mailer = new Mailer(email, smtpHost, smtpPort, contaEMail, senhaEMail);
			mailer.send();

		} catch (Exception e) {
			e.printStackTrace();
			mensagemEnviada.setDsErroEmail(e.getMessage());
		}
	}

	private String getFuncao(Conexao cnx, Integer idFuncao) {
		try {
			Funcao funcao = FuncaoLocalizador.buscaFuncao(cnx, idFuncao);

			if (funcao != null) {
				return funcao.getIdFuncao() + " - " + funcao.getDsFuncao();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}

	private String getLotacao(Conexao cnx, Integer idLot) {
		try {
			jPcmso.persistencia.geral.l.Lotacao lotacao = jPcmso.persistencia.geral.l.LotacaoLocalizador
					.buscaPorNumero_da_Lotacao(cnx, idLot);

			if (lotacao != null) {
				return lotacao.getIdLotacao() + " - " + lotacao.getDsLotacao();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}

	private String getFilial(Conexao cnx, Integer idFilial) {
		try {
			FiliaisEmpresa filial = FiliaisEmpresaLocalizador.buscaPorNumeroDaFilial(cnx, idFilial);

			if (filial != null) {
				return filial.getIdFilial() + " - " + filial.getDsFilial();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}

	private String getCampoHtml(String nomeCampo, String dsCampo) {
		StringBuilder sb = new StringBuilder();
		sb.append("<br/>");
		sb.append("<b>");
		sb.append( nomeCampo != null ? nomeCampo : "" );
		sb.append(":&nbsp;</b>");
		sb.append(dsCampo);

		return sb.toString();
	}

	private boolean compara(boolean atual, boolean novo) {
		return compara(atual ? "S" : "N", novo ? "S" : "N");
	}

	private boolean compara(String atual, String novo) {

		if (atual == null) {
			atual = "";
		}

		if (novo == null) {
			novo = "";
		}

		if (!atual.trim().equals(novo.trim())) {
			return true;
		}
		return false;
	}

	private boolean compara(Integer atual, Integer novo) {

		if (atual == null) {
			atual = 0;
		}
		if (novo == null) {
			novo = 0;
		}

		if (!atual.equals(novo)) {
			return true;
		}

		return false;
	}

	public void salvarBeneficiarioReinclusao() {
		try {
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
			try {
				cnx.beginTransaction();
				try {
					mensagemEnviada = new MensagemEnviada();
					mensagemEnviada.setDtMensagem(new QtData());
					initMensagemParametro(cnx);

					// char dig =
					// DigitoVerificador.calculaDigito(beneficiarioSelecionado.getBeneficiario().getCdUnimed()
					// + beneficiarioSelecionado.getBeneficiario().getCdEmpresa() +
					// beneficiarioSelecionado.getBeneficiario().getCdFamilia() +
					// beneficiarioSelecionado.getBeneficiario().getCdDependencia());
					//
					// beneficiarioSelecionado.getBeneficiario().setCdDigito(dig + "");

					// BeneficiarioNegocioEspecifico.geraNovoIdentificadorBeneficiario(beneficiarioSelecionado.getBeneficiario());

					// BeneficiarioNegocio benef = new BeneficiarioNegocio(cnx,
					// beneficiarioSelecionado.getBeneficiario());
					// benef.insere();

					PreCadastroCbo pre = new PreCadastroCbo();
					Beneficiario benef = beneficiarioSelecionado.getBeneficiario();
					BeneficiarioCbo cbo = beneficiarioSelecionado.getBeneficiarioCbo();

					pre.setCdBeneficiarioCartao(cbo.getCdBeneficiarioCartao());
					pre.setCdCbo(cbo.getCdCbo());
					pre.setCdEmpresa(cbo.getCdEmpresa());
					pre.setCdEstado(benef.getCdEstadoResidencia());
					pre.setCdNaEmpresa(benef.getCdNaEmpresa());
					pre.setCdRg(benef.getCdRg());
					pre.setCdUnimed(benef.getCdUnimed());
					pre.setDsAtividade(cbo.getDsAtividade());
					pre.setDsBairro(benef.getDsBairro());
					pre.setDsComplementoLogradouro(benef.getDsComplementoLogradouro());
					pre.setDsLogradouro(benef.getDsLogradouro());
					pre.setDsNome(benef.getDsNome());
					pre.setDsNomeDaMae(benef.getDsNomeDaMae());
					pre.setDsNumeroLogradouro(benef.getDsNumeroLogradouro());
					pre.setDsOrgaoEmissorRg(benef.getDsOrgaoEmissorRg());
					pre.setDsTelefoneContato(benef.getDsTelefone());
					pre.setDtAdmissao(cbo.getDtAdmissao());
					pre.setDtExclusao(benef.getDtExclusao());
					pre.setDtInclusao(benef.getDtInclusao());
					pre.setDtNascimento(benef.getDtNascimento());
					pre.setDtSolicitacao(new QtData());
					pre.setDtSolicitacaoExclusao(null);
					pre.setDtUltimoMovimento(new QtData());
					pre.setIdCargo(cbo.getIdCargo());
					pre.setIdCidade(benef.getIdCidadeResidencia());
					pre.setIdFilial(cbo.getIdFilial());
					pre.setIdFuncao(cbo.getIdFuncao());
					pre.setIdLotacao(benef.getIdLotacao());
					pre.setIdMotivoSolicitacaoExclusao(null);
					pre.setIdPreCadastro(getIdPreCadastro(cnx));
					pre.setIdSetor(cbo.getIdSetor());
					pre.setIdUsuarioDigitou(usuarioLogado.getIdUsuario());
					pre.setNrCpf(benef.getCdCpf());
					pre.setNrCtps(cbo.getNrCtps());
					pre.setNrPis(cbo.getNrPis());
					pre.setNrSerieUf(cbo.getNrSerieUf());
					pre.setSnFitossanitarios(cbo.isSnFitossanitario() ? "S" : "N");
					pre.setSnPossuiDeficiencia(cbo.isSnPossuiDeficiencia() ? "S" : "N");
					pre.setSnTrabalhoConfinado(cbo.isSnEspacoConfinado() ? "S" : "N");
					pre.setSnTrabalhoEmAltura(cbo.isSnTrabalhoEmAltura() ? "S" : "N");
					pre.setTpBrPdh(cbo.getTpBrPdh());
					pre.setTpDeficiencia(null);
					pre.setTpEstadoCivil(cbo.getTpEstadoCivil());
					pre.setTpSexo(cbo.getTpSexo());
					pre.setTpSolicitacao("I");
					pre.setVlAltura(cbo.getVlAltura());
					pre.setVlPeso(cbo.getVlPeso());
					try {
						PreCadastroCboNegocio preNegocio = new PreCadastroCboNegocio(cnx, pre);
						preNegocio.insere();
					} catch (Exception e) {
						e.printStackTrace();
					}
					BeneficiarioCbo beneficiarioCboAnterior = BeneficiarioCboLocalizador.buscaBeneficiarioCbo(cnx,
							beneficiarioSelecionado.getBeneficiarioCbo().getCdBeneficiarioCartao());

					if (beneficiarioSelecionado.getBeneficiarioCbo().getDtAdmissao() == null) {
						msgExibida = 0;
						msgAoUsuario = "Data de admiss�o necessita estar preenchida.";
						Util.sendRedirect("resources/page/mntBeneficiarioReativacao.jsf");

					} else {
						if (beneficiarioSelecionado.getBeneficiarioCbo().getCdBeneficiarioCartao() == null) {
							beneficiarioSelecionado.getBeneficiarioCbo().setCdBeneficiarioCartao(
									beneficiarioSelecionado.getBeneficiario().getCdBeneficiarioCartao());
							beneficiarioSelecionado.getBeneficiarioCbo()
									.setCdDigito(beneficiarioSelecionado.getBeneficiario().getCdDigito());
							beneficiarioSelecionado.getBeneficiarioCbo()
									.setCdEmpresa(beneficiarioSelecionado.getBeneficiario().getCdEmpresa());
							beneficiarioSelecionado.getBeneficiarioCbo()
									.setCdRg(beneficiarioSelecionado.getBeneficiario().getCdRg());
							beneficiarioSelecionado.getBeneficiarioCbo()
									.setCdUnimed(beneficiarioSelecionado.getBeneficiario().getCdUnimed());
							beneficiarioSelecionado.getBeneficiarioCbo()
									.setIdLotacao(beneficiarioSelecionado.getBeneficiario().getIdLotacao());
							beneficiarioSelecionado.getBeneficiarioCbo()
									.setDsNome(beneficiarioSelecionado.getBeneficiario().getDsNome());
							beneficiarioSelecionado.getBeneficiarioCbo()
									.setDsNomeDaMae(beneficiarioSelecionado.getBeneficiario().getDsNomeDaMae());
							beneficiarioSelecionado.getBeneficiarioCbo()
									.setCdEmpresa(beneficiarioSelecionado.getBeneficiario().getCdEmpresa());
							beneficiarioSelecionado.getBeneficiarioCbo()
									.setDtInclusao(beneficiarioSelecionado.getBeneficiario().getDtInclusao());
							beneficiarioSelecionado.getBeneficiarioCbo()
									.setDtNascimento(beneficiarioSelecionado.getBeneficiario().getDtNascimento());
							beneficiarioSelecionado.getBeneficiarioCbo()
									.setNrCpf(beneficiarioSelecionado.getBeneficiario().getCdCpf());
							beneficiarioSelecionado.getBeneficiarioCbo()
									.setNrPis(beneficiarioSelecionado.getBeneficiario().getCdPisPasep());
							beneficiarioSelecionado.getBeneficiarioCbo()
									.setTpEstadoCivil(beneficiarioSelecionado.getBeneficiario().getTpEstadoCivil());
							beneficiarioSelecionado.getBeneficiarioCbo()
									.setTpSexo(beneficiarioSelecionado.getBeneficiario().getTpSexo());
							BeneficiarioCboNegocio benefCbo = new BeneficiarioCboNegocio(cnx,
									beneficiarioSelecionado.getBeneficiarioCbo());
							benefCbo.insere();
						} else {
							beneficiarioSelecionado.getBeneficiarioCbo().setDtExclusao(null);
							try {
								BeneficiarioCboNegocio benefCbo = new BeneficiarioCboNegocio(cnx,
										beneficiarioSelecionado.getBeneficiarioCbo());
								benefCbo.altera();
							} catch (Exception e) {
								e.printStackTrace();
								throw new Exception(e);
							}

						}

						if (beneficiarioCboAnterior != null) {
							boolean enviaEmail = false;
							if (compara(beneficiarioCboAnterior.getCdCbo(),
									beneficiarioSelecionado.getBeneficiarioCbo().getCdCbo())) {
								enviaEmail = true;
							}

							if (compara(beneficiarioCboAnterior.getIdFuncao() + "",
									beneficiarioSelecionado.getBeneficiarioCbo().getIdFuncao() + "")) {
								enviaEmail = true;
							}

							if (compara(beneficiarioCboAnterior.getIdSetor() + "",
									beneficiarioSelecionado.getBeneficiarioCbo().getIdSetor() + "")) {
								enviaEmail = true;
							}

							if (compara(beneficiarioCboAnterior.getIdCargo() + "",
									beneficiarioSelecionado.getBeneficiarioCbo().getIdCargo() + "")) {
								enviaEmail = true;
							}
						}
						enviaEmailReinclusao(cnx, beneficiarioSelecionado.getBeneficiarioCbo(),
								beneficiarioCboAnterior);
						msgExibida = 0;
						msgAoUsuario = "Benefici�rio Reincluído com sucesso!";
						Util.sendRedirect("resources/page/dashboard.jsf");
					}
					cnx.commit();
				} catch (Exception e) {
					e.printStackTrace();
					cnx.rollback();
					throw new Exception(e);
				}

			} finally {
				cnx.libera();
			}
		} catch (Exception e) {
			e.printStackTrace();
			msgExibida = 0;
			msgAoUsuario = "Ops! " + e.getMessage();
			Util.sendRedirect("resources/page/mntBeneficiarioReativacao.jsf");
		}
	}

	private Integer getIdPreCadastro(Conexao cnx) throws QtSQLException {
		Query q = new Query(cnx);
		q.setSQL("select ( max( id_pre_cadastro ) + 1 ) as total from pcmso.pre_cadastro_cbo");
		q.executeQuery();

		if (!q.isEmpty()) {
			return q.getInteger("total");
		}
		return 1;
	}

	public void salvarBeneficiarioAtivo() {

		try {
			if (usuarioLogado == null) {
				usuarioLogado = jUni.controleLogin.Util.getDadosUsuarioLogado();
			}
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

			try {
				mensagemEnviada = new MensagemEnviada();
				mensagemEnviada.setDtMensagem(new QtData());
				initMensagemParametro(cnx);

				BeneficiarioNegocio benef = new BeneficiarioNegocio(cnx, beneficiarioSelecionado.getBeneficiario());
				benef.altera();
				
				beneficiarioSelecionado.getBeneficiarioCbo().setIdLotacao( beneficiarioSelecionado.getBeneficiario().getIdLotacao() );

				BeneficiarioCbo beneficiarioCboAnterior = BeneficiarioCboLocalizador.buscaBeneficiarioCbo(cnx,
						beneficiarioSelecionado.getBeneficiarioCbo().getCdBeneficiarioCartao());

				BeneficiarioCboNegocio benefCbo = new BeneficiarioCboNegocio(cnx,
						beneficiarioSelecionado.getBeneficiarioCbo());
				benefCbo.altera();
				String mensagem = "";
				if (beneficiarioCboAnterior != null) {
					boolean enviaEmail = true;
					// if (compara( beneficiarioCboAnterior.getCdCbo(),
					// beneficiarioSelecionado.getBeneficiarioCbo().getCdCbo() ) ) {
					// enviaEmail = true;
					// }
					//
					// if (compara( beneficiarioCboAnterior.getIdFuncao() + "",
					// beneficiarioSelecionado.getBeneficiarioCbo().getIdFuncao() + "" ) ) {
					// enviaEmail = true;
					// }
					//
					// if (compara( beneficiarioCboAnterior.getIdSetor() + "",
					// beneficiarioSelecionado.getBeneficiarioCbo().getIdSetor() + "" ) ) {
					// enviaEmail = true;
					// }
					//
					// if (compara( beneficiarioCboAnterior.getIdCargo() + "",
					// beneficiarioSelecionado.getBeneficiarioCbo().getIdCargo() + "" ) ) {
					// enviaEmail = true;
					// }
					//
					// if (compara( beneficiarioCboAnterior.getVlPeso() + "",
					// beneficiarioSelecionado.getBeneficiarioCbo().getVlPeso() + "" ) ) {
					// enviaEmail = true;
					// }
					// if (compara( beneficiarioCboAnterior.getVlAltura() + "",
					// beneficiarioSelecionado.getBeneficiarioCbo().getVlAltura() + "" ) ) {
					// enviaEmail = true;
					// }
					// if (compara( beneficiarioCboAnterior.getDsAtividade() + "",
					// beneficiarioSelecionado.getBeneficiarioCbo().getDsAtividade() + "" ) ) {
					// enviaEmail = true;
					// }
					// if (compara( beneficiarioCboAnterior.getDtAdmissao() + "",
					// beneficiarioSelecionado.getBeneficiarioCbo().getDtAdmissao() + "" ) ) {
					// enviaEmail = true;
					// }
					// if (compara( beneficiarioCboAnterior.isSnPossuiDeficiencia() + "",
					// beneficiarioSelecionado.getBeneficiarioCbo().isSnPossuiDeficiencia() + "" ) )
					// {
					// enviaEmail = true;
					// }

					if (enviaEmail) {
						mensagem = enviaEmail(cnx, beneficiarioSelecionado.getBeneficiarioCbo(), beneficiarioCboAnterior);
					}
				}

				DadosSolicitacaoPCMSO dados = new DadosSolicitacaoPCMSO();
				dados.setIdUsuarioSolicitou(usuarioLogado.getIdUsuario());
				dados.setCdUnimed(beneficiarioSelecionado.getBeneficiario().getCdUnimed());
				dados.setCdEmpresa(beneficiarioSelecionado.getBeneficiario().getCdEmpresa());
				dados.setCdFamilia(beneficiarioSelecionado.getBeneficiario().getCdFamilia());
				dados.setCdDependencia(beneficiarioSelecionado.getBeneficiario().getCdDependencia());
				dados.setCdDigito(beneficiarioSelecionado.getBeneficiario().getCdDigito());
				dados.setNmBeneficiario(beneficiarioSelecionado.getBeneficiarioCbo().getDsNome());
				dados.setDtSolicitacao(new QtData());
				dados.setTpSolicitacao("A");
				dados.setDtConfirmacao(null);
				dados.setIdUsuarioConfirmacao(null);
				dados.setDsObservacoes(mensagem);

				SolicitacaoPCMSO solicitacao = new SolicitacaoPCMSO(dados);
				solicitacao.incluiSolicitacao(cnx);
				
				enviaEmailAlteracao(cnx, beneficiarioSelecionado.getBeneficiarioCbo() );

				snPossuiDeficiencia = null;
				snPossuiDeficienciaCbo = null;
				snPossuiDeficiencia = null;
				snTrabalhoEmAltura = null;
				snEspacoConfinado = null;
				
				msgExibida = 0;
				msgAoUsuario = "Benefici�rio alterado com sucesso!";
				Util.sendRedirect("resources/page/dashboard.jsf");
			} finally {
				cnx.libera();
			}
		} catch (Exception e) {
			e.printStackTrace();
			msgExibida = 0;
			msgAoUsuario = "Ops! " + e.getMessage();
			Util.sendRedirect("resources/page/mntBeneficiario.jsf");
		}

	}

	private String getCdDependencia(String cdBeneficiarioCartao) {
		if (cdBeneficiarioCartao != null && cdBeneficiarioCartao.length() >= 12) {
			return cdBeneficiarioCartao.substring(10, 12);
		}
		return "";
	}

	private String getCdFamilia(String cdBeneficiarioCartao) {
		if (cdBeneficiarioCartao != null && cdBeneficiarioCartao.length() >= 10) {
			return cdBeneficiarioCartao.substring(4, 10);
		}
		return "";
	}

	private String getCdDigito(String cdBeneficiarioCartao) {

		if (cdBeneficiarioCartao != null && cdBeneficiarioCartao.length() == 16) {
			return DigitoVerificador.calculaDigito(cdBeneficiarioCartao) + "";
		}
		return "";
	}

	public BeanPreCadastroCboPCMSO() {
		if (usuarioLogado == null) {
			usuarioLogado = jUni.controleLogin.Util.getDadosUsuarioLogado();
		}

		if (usuarioLogado != null) {

			snAcidentePPP = "N";
			tpRelOrdem = "";
			tpRelAcao = "";
			nrCco = "";
			tpContratoPacote = "S";
			snRelExcluidosData = "N";

			habFuncao2 = false;
			habFuncao3 = false;
			habFuncao4 = false;
			habFuncao5 = false;
			habFuncao6 = false;
			habFuncao7 = false;
			habFuncao8 = false;
			habFuncao9 = false;
			habFuncao10 = false;

			QtData qt = new QtData();
			qt.setNrDia(1);
			dtInicial = dtInclusaoInicialRel = qt.toString();

			QtData qt2 = new QtData(qt);
			qt2.deslocaMeses(+1);
			qt2.deslocaDias(-1);
			dtFinal = dtInclusaoFinalRel = qt.toString();

			snGeral = "N";
			snExcluido = "N";
			snAtivos = "N";
			snComPacotes = "N";
			snSemPacotes = "N";

			if (usuarioLogado == null) {
				usuarioLogado = jUni.controleLogin.Util.getDadosUsuarioLogado();
			}
			listaMotivoExclusao = new LinkedList<>();
			listagemPreCadastro = new LinkedList<>();
			listagemBeneficiario = new LinkedList<>();
			listagemBeneficiarioInativo = new LinkedList<>();
			listaRiscos = new LinkedList<>();
			listaCidades = new LinkedList<>();
			listaCbo = new LinkedList<>();
			listaSetor = new LinkedList<>();
			listaFuncao = new LinkedList<>();
			listaCargo = new LinkedList<>();
			listaLotacao = new LinkedList<>();
			listaPais = new LinkedList<>();
			listaEstados = new LinkedList<>();
			listaFiliais = new LinkedList<>();
			listagemPPPPendente = new LinkedList<DadosSolicitacaoPPPPCMSO>();

			try {
				ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
				try {
					idEmpresaRelacioanda = getEmpresaRelacionada(cnx, usuarioLogado);
					listaMotivoExclusao = getMotivosDeExclusao(cnx);
					listagemBeneficiario.clear();
					listagemBeneficiario = carregaListagemBeneficiarios(cnx, null, null, null, true);
					listagemBeneficiarioInativo.clear();
					listagemBeneficiarioInativo = carregaListagemBeneficiarios(cnx, null, null, null, false);
					listaRiscos.clear();
					listaRiscos = carregaListagemRiscos(cnx);
					listagemPPPPendente = carregaListagemPPP(cnx);
					
					if (idEmpresaRelacioanda == null) {
							selecionaBeneficiarioAtivo();
					} else {
						listagemPreCadastro = carregaListagem(cnx, null, null);
						selecionaAbaPreCadastro();
					}
					
					listaFiliais = carregaFiliais(cnx, idEmpresaRelacioanda);
					listaPais = carregaListaPaises(cnx);
					listaEstados = carregaListaEstados(cnx);

					listaCbo = carregaListaCbo(cnx);
					listaSetor = carregaListaSetor(cnx);
					listaFuncao = carregaListaFuncao(cnx);
					listaCargo = carregaListaCargo(cnx);
					listaLotacao = carregaLotacao(cnx);
					
				} finally {
					cnx.libera();
				}
			} catch (Exception e) {
				e.printStackTrace();
				msgExibida = 0;
				msgAoUsuario = "Ops! " + e.getMessage();
			}
		}

	}

	public void visualizaSolicitacaoPPP(DadosSolicitacaoPPPPCMSO dados) {
		dadosSolicitacaoPPPSelecionada = dados;
		Util.sendRedirect("resources/page/solicitacoes/mntSolicitacaoPPPEmpresa.jsf");
	}

	public void cancelarPPP() {
		dadosSolicitacaoPPPSelecionada = new DadosSolicitacaoPPPPCMSO();
		Util.sendRedirect("resources/page/dashboard.jsf");
	}

	public void atualizaSolicitacaoPPP(DadosSolicitacaoPPPPCMSO dados) {

	}

	public void confirmarSolicitacaoPPP(DadosSolicitacaoPPPPCMSO dados) {

		try {
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
			try {

				Query q = new Query(cnx);
				q.setSQL("update pcmso.solicitacoes_ppp_pcmso set ");
				q.addSQL("dt_demissao = :prmdt_admissao, " + "dt_nascimento = :prmdt_nascimento, "
						+ "nr_nit_pis = :prmnr_nit_pis, " + "nr_ctps = :prmnr_ctps, " + "nr_serie = :prmnr_serie, "
						+ "uf_ctps = :prmnr_ctps, " + "sn_acidente_trabalho = :prmsn_acidente_trabalho, "
						+ "dt_acidente = :prmdt_acidente, " + "nr_cat = :prmnr_cat, "
						+ "nm_responsavel = :prmnm_responsavel, "
						+ "nr_nit_pis_responsavel =:prmnr_nit_pis_responsavel, "

						+ "dt_admissao_funcao = :prmdtadmissaofuncao, " + "dt_fim_funcao = :prmdt_fim_funcao, "
						+ "ds_funcao = :prmds_funcao, " + "ds_cbo = :prmds_cbo, " + "ds_setor = :prmds_setor, "

						+ "dt_admissao_funcao2 = :prm2dtadmissaofuncao, " + "dt_fim_funcao2      = :prm2dt_fim_funcao, "
						+ "ds_funcao2          = :prm2ds_funcao, " + "ds_cbo2             = :prm2ds_cbo, "
						+ "ds_setor2           = :prm2ds_setor, "

						+ "dt_admissao_funcao3 = :prm3dtadmissaofuncao, " + "dt_fim_funcao3      = :prm3dt_fim_funcao, "
						+ "ds_funcao3          = :prm3ds_funcao, " + "ds_cbo3             = :prm3ds_cbo, "
						+ "ds_setor3           = :prm3ds_setor, "

						+ "dt_admissao_funcao4 = :prm4dtadmissaofuncao, " + "dt_fim_funcao4      = :prm4dt_fim_funcao, "
						+ "ds_funcao4          = :prm4ds_funcao, " + "ds_cbo4             = :prm4ds_cbo, "
						+ "ds_setor4           = :prm4ds_setor, "

						+ "dt_admissao_funcao5 = :prm5dtadmissaofuncao, " + "dt_fim_funcao5      = :prm5dt_fim_funcao, "
						+ "ds_funcao5          = :prm5ds_funcao, " + "ds_cbo5             = :prm5ds_cbo, "
						+ "ds_setor5           = :prm5ds_setor, "

						+ "dt_admissao_funcao6 = :prm6dtadmissaofuncao, " + "dt_fim_funcao6      = :prm6dt_fim_funcao, "
						+ "ds_funcao6          = :prm6ds_funcao, " + "ds_cbo6             = :prm6ds_cbo, "
						+ "ds_setor6           = :prm6ds_setor, "

						+ "dt_admissao_funcao7 = :prm7dtadmissaofuncao, " + "dt_fim_funcao7      = :prm7dt_fim_funcao, "
						+ "ds_funcao7          = :prm7ds_funcao, " + "ds_cbo7             = :prm7ds_cbo, "
						+ "ds_setor7           = :prm7ds_setor, "

						+ "dt_admissao_funcao8 = :prm8dtadmissaofuncao, " + "dt_fim_funcao8      = :prm8dt_fim_funcao, "
						+ "ds_funcao8          = :prm8ds_funcao, " + "ds_cbo8             = :prm8ds_cbo, "
						+ "ds_setor8           = :prm8ds_setor, "

						+ "dt_admissao_funcao9 = :prm9dtadmissaofuncao, " + "dt_fim_funcao9      = :prm9dt_fim_funcao, "
						+ "ds_funcao9          = :prm9ds_funcao, " + "ds_cbo9             = :prm9ds_cbo, "
						+ "ds_setor9           = :prm9ds_setor, "

						+ "dt_admissao_funcao10 = :prm10dtadmissaofuncao, "
						+ "dt_fim_funcao10      = :prm10dt_fim_funcao, " + "ds_funcao10          = :prm10ds_funcao, "
						+ "ds_cbo10             = :prm10ds_cbo, " + "ds_setor10           = :prm10ds_setor, "

						+ "dt_mudanca_funcao = :prmdt_mudanca_funcao, "
						+ "dt_mudanca_fim_funcao = :prmdt_mudanca_fim_funcao, "
						+ "ds_mudanca_funcao = :prmds_mudanca_funcao, " + "ds_mudanca_cbo = :prmds_mudanca_cbo, "
						+ "ds_mudanca_setor = :prmds_mudanca_setor, " + "ds_observacoes = :prmds_observacoes ");
				q.addSQL(" where id_solicitacao = :prmIdSolicitacaoPPP");

				q.setParameter("prmIdSolicitacaoPPP", dados.getIdSolicitacao());
				q.setParameter("prmid_usuario_solicitou", dados.getIdUsuarioSolicitou());
				q.setParameter("prmcd_beneficiario_cartao", dados.getCdBeneficiarioCartao());
				q.setParameter("prmdt_solicitacao",
						dados.getDtSolicitacao() != null ? dados.getDtSolicitacao().getQtDataAsTimestamp() : null);
				q.setParameter("prmdt_admissao",
						dados.getDtAdmissao() != null ? dados.getDtAdmissao().getQtDataAsTimestamp() : null);
				q.setParameter("prmdt_demissao",
						dados.getDtDemissao() != null ? dados.getDtDemissao().getQtDataAsTimestamp() : null);
				q.setParameter("prmdt_nascimento",
						dados.getDtNascimento() != null ? dados.getDtNascimento().getQtDataAsTimestamp() : null);
				q.setParameter("prmnr_nit_pis", dados.getNrNitPis());
				q.setParameter("prmnr_ctps", dados.getNrCtps());
				q.setParameter("prmnr_serie", dados.getNrSerie());
				q.setParameter("prmuf_ctps", dados.getUfCtps());
				q.setParameter("prmsn_acidente_trabalho", dados.getSnAcidentetrabalho());
				q.setParameter("prmdt_acidente",
						dados.getDtAcidenteTrabalho() != null ? dados.getDtAcidenteTrabalho().getQtDataAsTimestamp()
								: null);
				q.setParameter("prmnr_cat", dados.getNrCat());
				q.setParameter("prmnm_responsavel", dados.getNmResponsavel());
				q.setParameter("prmnr_nit_pis_responsavel", dados.getNrNitPisResponsavel());
				q.setParameter("prmdtadmissaofuncao",
						dados.getDtAdmissaoFuncao() != null ? dados.getDtAdmissaoFuncao().getQtDataAsTimestamp()
								: null);
				q.setParameter("prmdt_fim_funcao",
						dados.getDtFimFuncao() != null ? dados.getDtFimFuncao().getQtDataAsTimestamp() : null);
				q.setParameter("prmds_funcao", dados.getDsFuncao());
				q.setParameter("prmds_cbo", dados.getDsCbo());
				q.setParameter("prmds_setor", dados.getDsSetor());

				q.setParameter("prm2dtadmissaofuncao",
						dados.getDtFimFincaoFuncaoPPP2() != null
								? dados.getDtFimFincaoFuncaoPPP2().getQtDataAsTimestamp()
								: null);
				q.setParameter("prm2dt_fim_funcao",
						dados.getDtFimFincaoFuncaoPPP2() != null
								? dados.getDtFimFincaoFuncaoPPP2().getQtDataAsTimestamp()
								: null);
				q.setParameter("prm2ds_funcao", dados.getDsFuncaoFuncaoPPP2());
				q.setParameter("prm2ds_cbo", dados.getDsCBOFuncaoPPP2());
				q.setParameter("prm2ds_setor", dados.getDsSetorFuncaoPPP2());

				q.setParameter("prm3dtadmissaofuncao",
						dados.getDtFimFincaoFuncaoPPP3() != null
								? dados.getDtFimFincaoFuncaoPPP3().getQtDataAsTimestamp()
								: null);
				q.setParameter("prm3dt_fim_funcao",
						dados.getDtFimFincaoFuncaoPPP3() != null
								? dados.getDtFimFincaoFuncaoPPP3().getQtDataAsTimestamp()
								: null);
				q.setParameter("prm3ds_funcao", dados.getDsFuncaoFuncaoPPP3());
				q.setParameter("prm3ds_cbo", dados.getDsCBOFuncaoPPP3());
				q.setParameter("prm3ds_setor", dados.getDsSetorFuncaoPPP3());

				q.setParameter("prm4dtadmissaofuncao",
						dados.getDtFimFincaoFuncaoPPP4() != null
								? dados.getDtFimFincaoFuncaoPPP4().getQtDataAsTimestamp()
								: null);
				q.setParameter("prm4dt_fim_funcao",
						dados.getDtFimFincaoFuncaoPPP4() != null
								? dados.getDtFimFincaoFuncaoPPP4().getQtDataAsTimestamp()
								: null);
				q.setParameter("prm4ds_funcao", dados.getDsFuncaoFuncaoPPP4());
				q.setParameter("prm4ds_cbo", dados.getDsCBOFuncaoPPP4());
				q.setParameter("prm4ds_setor", dados.getDsSetorFuncaoPPP4());

				q.setParameter("prm5dtadmissaofuncao",
						dados.getDtFimFincaoFuncaoPPP5() != null
								? dados.getDtFimFincaoFuncaoPPP5().getQtDataAsTimestamp()
								: null);
				q.setParameter("prm5dt_fim_funcao",
						dados.getDtFimFincaoFuncaoPPP5() != null
								? dados.getDtFimFincaoFuncaoPPP5().getQtDataAsTimestamp()
								: null);
				q.setParameter("prm5ds_funcao", dados.getDsFuncaoFuncaoPPP5());
				q.setParameter("prm5ds_cbo", dados.getDsCBOFuncaoPPP5());
				q.setParameter("prm5ds_setor", dados.getDsSetorFuncaoPPP5());

				q.setParameter("prm6dtadmissaofuncao",
						dados.getDtFimFincaoFuncaoPPP6() != null
								? dados.getDtFimFincaoFuncaoPPP6().getQtDataAsTimestamp()
								: null);
				q.setParameter("prm6dt_fim_funcao",
						dados.getDtFimFincaoFuncaoPPP6() != null
								? dados.getDtFimFincaoFuncaoPPP6().getQtDataAsTimestamp()
								: null);
				q.setParameter("prm6ds_funcao", dados.getDsFuncaoFuncaoPPP6());
				q.setParameter("prm6ds_cbo", dados.getDsCBOFuncaoPPP6());
				q.setParameter("prm6ds_setor", dados.getDsSetorFuncaoPPP6());

				q.setParameter("prm7dtadmissaofuncao",
						dados.getDtFimFincaoFuncaoPPP7() != null
								? dados.getDtFimFincaoFuncaoPPP7().getQtDataAsTimestamp()
								: null);
				q.setParameter("prm7dt_fim_funcao",
						dados.getDtFimFincaoFuncaoPPP7() != null
								? dados.getDtFimFincaoFuncaoPPP7().getQtDataAsTimestamp()
								: null);
				q.setParameter("prm7ds_funcao", dados.getDsFuncaoFuncaoPPP7());
				q.setParameter("prm7ds_cbo", dados.getDsCBOFuncaoPPP7());
				q.setParameter("prm7ds_setor", dados.getDsSetorFuncaoPPP7());

				q.setParameter("prm8dtadmissaofuncao",
						dados.getDtFimFincaoFuncaoPPP8() != null
								? dados.getDtFimFincaoFuncaoPPP8().getQtDataAsTimestamp()
								: null);
				q.setParameter("prm8dt_fim_funcao",
						dados.getDtFimFincaoFuncaoPPP8() != null
								? dados.getDtFimFincaoFuncaoPPP8().getQtDataAsTimestamp()
								: null);
				q.setParameter("prm8ds_funcao", dados.getDsFuncaoFuncaoPPP8());
				q.setParameter("prm8ds_cbo", dados.getDsCBOFuncaoPPP8());
				q.setParameter("prm8ds_setor", dados.getDsSetorFuncaoPPP8());

				q.setParameter("prm9dtadmissaofuncao",
						dados.getDtFimFincaoFuncaoPPP9() != null
								? dados.getDtFimFincaoFuncaoPPP9().getQtDataAsTimestamp()
								: null);
				q.setParameter("prm9dt_fim_funcao",
						dados.getDtFimFincaoFuncaoPPP9() != null
								? dados.getDtFimFincaoFuncaoPPP9().getQtDataAsTimestamp()
								: null);
				q.setParameter("prm9ds_funcao", dados.getDsFuncaoFuncaoPPP9());
				q.setParameter("prm9ds_cbo", dados.getDsCBOFuncaoPPP9());
				q.setParameter("prm9ds_setor", dados.getDsSetorFuncaoPPP9());

				q.setParameter("prm10dtadmissaofuncao",
						dados.getDtFimFincaoFuncaoPPP10() != null
								? dados.getDtFimFincaoFuncaoPPP10().getQtDataAsTimestamp()
								: null);
				q.setParameter("prm10dt_fim_funcao",
						dados.getDtFimFincaoFuncaoPPP10() != null
								? dados.getDtFimFincaoFuncaoPPP10().getQtDataAsTimestamp()
								: null);
				q.setParameter("prm10ds_funcao", dados.getDsFuncaoFuncaoPPP10());
				q.setParameter("prm10ds_cbo", dados.getDsCBOFuncaoPPP10());
				q.setParameter("prm10ds_setor", dados.getDsSetorFuncaoPPP10());

				q.setParameter("prmd10tadmissaofuncao",
						dados.getDtFimFincaoFuncaoPPP10() != null
								? dados.getDtFimFincaoFuncaoPPP10().getQtDataAsTimestamp()
								: null);
				q.setParameter("prmd10t_fim_funcao",
						dados.getDtFimFincaoFuncaoPPP10() != null
								? dados.getDtFimFincaoFuncaoPPP10().getQtDataAsTimestamp()
								: null);
				q.setParameter("prmd10s_funcao", dados.getDsFuncaoFuncaoPPP10());
				q.setParameter("prmd10s_cbo", dados.getDsCBOFuncaoPPP10());
				q.setParameter("prmd10s_setor", dados.getDsSetorFuncaoPPP10());

				q.setParameter("prmdt_mudanca_funcao",
						dados.getDtMudancaFuncao() != null ? dados.getDtMudancaFuncao().getQtDataAsTimestamp() : null);
				q.setParameter("prmdt_mudanca_fim_funcao",
						dados.getDtMudancaFimFuncao() != null ? dados.getDtMudancaFimFuncao().getQtDataAsTimestamp()
								: null);
				q.setParameter("prmds_mudanca_funcao", dados.getDsMudancaFuncao());
				q.setParameter("prmds_mudanca_cbo", dados.getDsMudancaCbo());
				q.setParameter("prmds_mudanca_setor", dados.getDsMudancaSetor());
				q.setParameter("prmds_observacoes", dados.getDsObservacoes());
				q.setParameter("prmnm_beneficiario", dados.getNmBeneficiario());
				q.executeUpdate();

				msgExibida = 0;
				msgAoUsuario = " Solicita��o PPP de " + dados.getNmBeneficiario() + ", foi atualizada";
				Util.sendRedirect("resources/page/dashboard.jsf");

			} finally {
				cnx.libera();
			}

		} catch (Exception e) {
			e.printStackTrace();
			msgExibida = 0;
			msgAoUsuario = "Ops! " + e.getMessage();
		}
	}

	public void habilitaFuncao(Integer numero) {
		if (numero.equals(2)) {
			habFuncao2 = true;
			habFuncao3 = false;
			habFuncao4 = false;
			habFuncao5 = false;
			habFuncao6 = false;
			habFuncao7 = false;
			habFuncao8 = false;
			habFuncao9 = false;
			habFuncao10 = false;
		} else if (numero.equals(3)) {
			habFuncao2 = true;
			habFuncao3 = true;
			habFuncao4 = false;
			habFuncao5 = false;
			habFuncao6 = false;
			habFuncao7 = false;
			habFuncao8 = false;
			habFuncao9 = false;
			habFuncao10 = false;
		} else if (numero.equals(4)) {
			habFuncao2 = true;
			habFuncao3 = true;
			habFuncao4 = true;
			habFuncao5 = false;
			habFuncao6 = false;
			habFuncao7 = false;
			habFuncao8 = false;
			habFuncao9 = false;
			habFuncao10 = false;
		} else if (numero.equals(5)) {
			habFuncao2 = true;
			habFuncao3 = true;
			habFuncao4 = true;
			habFuncao5 = true;
			habFuncao6 = false;
			habFuncao7 = false;
			habFuncao8 = false;
			habFuncao9 = false;
			habFuncao10 = false;
		} else if (numero.equals(6)) {
			habFuncao2 = true;
			habFuncao3 = true;
			habFuncao4 = true;
			habFuncao5 = true;
			habFuncao6 = true;
			habFuncao7 = false;
			habFuncao8 = false;
			habFuncao9 = false;
			habFuncao10 = false;
		} else if (numero.equals(7)) {
			habFuncao2 = true;
			habFuncao3 = true;
			habFuncao4 = true;
			habFuncao5 = true;
			habFuncao6 = true;
			habFuncao7 = true;
			habFuncao8 = false;
			habFuncao9 = false;
			habFuncao10 = false;
		} else if (numero.equals(8)) {
			habFuncao2 = true;
			habFuncao3 = true;
			habFuncao4 = true;
			habFuncao5 = true;
			habFuncao6 = true;
			habFuncao7 = true;
			habFuncao8 = true;
			habFuncao9 = false;
			habFuncao10 = false;
		} else if (numero.equals(9)) {
			habFuncao2 = true;
			habFuncao3 = true;
			habFuncao4 = true;
			habFuncao5 = true;
			habFuncao6 = true;
			habFuncao7 = true;
			habFuncao8 = true;
			habFuncao9 = true;
			habFuncao10 = false;
		} else if (numero.equals(10)) {
			habFuncao2 = true;
			habFuncao3 = true;
			habFuncao4 = true;
			habFuncao5 = true;
			habFuncao6 = true;
			habFuncao7 = true;
			habFuncao8 = true;
			habFuncao9 = true;
			habFuncao10 = true;
		}
	}
	
	public void solicitarRetornoAoTrabalho( Beneficiario benef ) {
		
		try {
		
			if (dtAdmissaoRetorno == null) {
				throw new Exception( "Ops! Preencha a data de retorno." );
			} 
			if ( motivoRetorno == null ) {
				throw new Exception(  "Ops! Preencha o motivo de retorno ao trabalho." );
			} else {
				if( motivoRetorno.isEmpty() ) {
					throw new Exception(  "Ops! Preencha o motivo de retorno ao trabalho." );
				}
			}
			
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
			try {
				
				enviaEmailRetornoAoTrabalho(cnx, benef );
				
				msgAoUsuario = "Solicita��o de Retorno ao Trabalho enviada. Enviar atestados ao setor de Sa�de Ocupacional!";
				msgExibida = 0;
				
				dtAdmissaoRetorno = null;
				motivoRetorno = null;
				
				Util.sendRedirect("resources/page/dashboard.jsf");
					
			} finally {
				cnx.libera();
			}
			
		} catch ( Exception e ) {
			e.printStackTrace();
			msgAoUsuario = e.getMessage();
			msgExibida = 0;
		}
	}
	
	public void selecionaDadosPPP(Beneficiario dados) {

		habFuncao2 = false;
		habFuncao3 = false;
		habFuncao4 = false;
		habFuncao5 = false;
		habFuncao6 = false;
		habFuncao7 = false;
		habFuncao8 = false;
		habFuncao9 = false;
		habFuncao10 = false;

		try {

			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

			try {
				BeneficiarioCbo benef = BeneficiarioCboLocalizador.buscaBeneficiarioCbo(cnx,
						dados.getCdBeneficiarioCartao());

				if (benef != null) {
					dtNascimentoPPP = benef.getDtNascimento().toString();
					nrNitPPP = benef.getNrPis();
					nrCtpsPPP = benef.getNrCtps();
					nrSeriePPP = benef.getNrSerieUf();
				} else {
					dtNascimentoPPP = dados.getDtNascimento().toString();
					nrNitPPP = null;
					nrCtpsPPP = null;
					nrSeriePPP = null;
				}

			} finally {
				cnx.libera();
			}
		} catch (Exception e) {
			e.printStackTrace();
			msgExibida = 0;
			msgAoUsuario = "Ops! " + e.getMessage();
		}
	}

	public String getDsNomeUsuario(Integer idUsuario) {

		try {
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao("jDesk");
			try {
				Query q = new Query(cnx);
				q.setSQL("select dsnomeusuario from jdesk.usuario where idusuario = :idUsuario ");
				q.setParameter("idUsuario", idUsuario);
				q.executeQuery();

				if (!q.isEmpty()) {
					return q.getString("dsnomeusuario");
				}

			} finally {
				cnx.libera();
			}

		} catch (Exception e) {
			e.printStackTrace();
			msgExibida = 0;
			msgAoUsuario = "Ops! " + e.getMessage();
			Util.sendRedirect("resources/page/solicitacoes/mntSolicitacaoPPPEmpresa.jsf");
		}

		return null;
	}

	private List<DadosSolicitacaoPPPPCMSO> carregaListagemPPP(Conexao cnx) {
		List<DadosSolicitacaoPPPPCMSO> lista = new LinkedList<DadosSolicitacaoPPPPCMSO>();
		try {
			Query q = new Query(cnx);

			q.setSQL("select * from pcmso.solicitacoes_ppp_pcmso where ");
			q.addSQL(" cd_beneficiario_cartao ilike '" + idEmpresaRelacioanda + "%' ");
			
			// se usu�rio luciont
			if ( usuarioLogado.getIdUsuario().equals(162) ) {
				q.addSQL(" order by dt_solicitacao desc limit 10");
				
				
			} else if ( usuarioLogado.getIdUsuario().equals( 2519 ) ) {
				q.addSQL(" order by dt_solicitacao desc limit 10");
				
				
			} else {
				q.addSQL(" order by dt_solicitacao desc limit 10");
				
			}
						

			q.executeQuery();

			if (!q.isEmpty()) {
				while (!q.isAfterLast()) {
					DadosSolicitacaoPPPPCMSO dados = new DadosSolicitacaoPPPPCMSO();

					dados.setIdSolicitacao(q.getInteger("id_solicitacao"));
					dados.setIdUsuarioSolicitou(q.getInteger("id_usuario_solicitou"));
					dados.setCdBeneficiarioCartao(q.getString("cd_beneficiario_cartao"));
					dados.setNmBeneficiario(q.getString("nm_beneficiario"));
					dados.setDtSolicitacao(
							q.getTimestamp("dt_solicitacao") != null ? new QtData(q.getTimestamp("dt_solicitacao"))
									: null);
					dados.setDtAdmissao(
							q.getTimestamp("dt_admissao") != null ? new QtData(q.getTimestamp("dt_admissao")) : null);
					dados.setDtDemissao(
							q.getTimestamp("dt_demissao") != null ? new QtData(q.getTimestamp("dt_demissao")) : null);
					dados.setDtNascimento(
							q.getTimestamp("dt_nascimento") != null ? new QtData(q.getTimestamp("dt_nascimento"))
									: null);
					dados.setNrNitPis(q.getString("nr_nit_pis"));
					dados.setNrCtps(q.getString("nr_ctps"));
					dados.setNrSerie(q.getString("nr_serie"));
					dados.setUfCtps(q.getString("uf_ctps"));
					dados.setSnAcidentetrabalho(q.getString("sn_acidente_trabalho"));
					dados.setDtAcidenteTrabalho(
							q.getTimestamp("dt_acidente") != null ? new QtData(q.getTimestamp("dt_acidente")) : null);
					dados.setNrCat(q.getString("nr_cat"));
					dados.setNmResponsavel(q.getString("nm_responsavel"));
					dados.setNrNitPisResponsavel(q.getString("nr_nit_pis_responsavel"));

					dados.setDtAdmissaoFuncao(q.getTimestamp("dt_admissao_funcao") != null
							? new QtData(q.getTimestamp("dt_admissao_funcao"))
							: null);
					dados.setDtFimFuncao(
							q.getTimestamp("dt_fim_funcao") != null ? new QtData(q.getTimestamp("dt_fim_funcao"))
									: null);
					dados.setDsFuncao(q.getString("ds_funcao"));
					dados.setDsCbo(q.getString("ds_cbo"));
					dados.setDsSetor(q.getString("ds_setor"));

					dados.setDtAdmissaoFucaoPPP2(q.getTimestamp("dt_admissao_funcao2") != null
							? new QtData(q.getTimestamp("dt_admissao_funcao2"))
							: null);
					dados.setDtFimFincaoFuncaoPPP2(
							q.getTimestamp("dt_fim_funcao2") != null ? new QtData(q.getTimestamp("dt_fim_funcao2"))
									: null);
					dados.setDsFuncaoFuncaoPPP2(q.getString("ds_funcao2"));
					dados.setDsCBOFuncaoPPP2(q.getString("ds_cbo2"));
					dados.setDsSetorFuncaoPPP2(q.getString("ds_setor2"));

					dados.setDtAdmissaoFucaoPPP3(q.getTimestamp("dt_admissao_funcao3") != null
							? new QtData(q.getTimestamp("dt_admissao_funcao3"))
							: null);
					dados.setDtFimFincaoFuncaoPPP3(
							q.getTimestamp("dt_fim_funcao3") != null ? new QtData(q.getTimestamp("dt_fim_funcao3"))
									: null);
					dados.setDsFuncaoFuncaoPPP3(q.getString("ds_funcao3"));
					dados.setDsCBOFuncaoPPP3(q.getString("ds_cbo3"));
					dados.setDsSetorFuncaoPPP3(q.getString("ds_setor3"));

					dados.setDtAdmissaoFucaoPPP4(q.getTimestamp("dt_admissao_funcao4") != null
							? new QtData(q.getTimestamp("dt_admissao_funcao4"))
							: null);
					dados.setDtFimFincaoFuncaoPPP4(
							q.getTimestamp("dt_fim_funcao4") != null ? new QtData(q.getTimestamp("dt_fim_funcao4"))
									: null);
					dados.setDsFuncaoFuncaoPPP4(q.getString("ds_funcao4"));
					dados.setDsCBOFuncaoPPP4(q.getString("ds_cbo4"));
					dados.setDsSetorFuncaoPPP4(q.getString("ds_setor4"));

					dados.setDtAdmissaoFucaoPPP5(q.getTimestamp("dt_admissao_funcao5") != null
							? new QtData(q.getTimestamp("dt_admissao_funcao5"))
							: null);
					dados.setDtFimFincaoFuncaoPPP5(
							q.getTimestamp("dt_fim_funcao5") != null ? new QtData(q.getTimestamp("dt_fim_funcao5"))
									: null);
					dados.setDsFuncaoFuncaoPPP5(q.getString("ds_funcao5"));
					dados.setDsCBOFuncaoPPP5(q.getString("ds_cbo5"));
					dados.setDsSetorFuncaoPPP5(q.getString("ds_setor5"));

					dados.setDtAdmissaoFucaoPPP6(q.getTimestamp("dt_admissao_funcao6") != null
							? new QtData(q.getTimestamp("dt_admissao_funcao6"))
							: null);
					dados.setDtFimFincaoFuncaoPPP6(
							q.getTimestamp("dt_fim_funcao6") != null ? new QtData(q.getTimestamp("dt_fim_funcao6"))
									: null);
					dados.setDsFuncaoFuncaoPPP6(q.getString("ds_funcao6"));
					dados.setDsCBOFuncaoPPP6(q.getString("ds_cbo6"));
					dados.setDsSetorFuncaoPPP6(q.getString("ds_setor6"));

					dados.setDtAdmissaoFucaoPPP7(q.getTimestamp("dt_admissao_funcao7") != null
							? new QtData(q.getTimestamp("dt_admissao_funcao7"))
							: null);
					dados.setDtFimFincaoFuncaoPPP7(
							q.getTimestamp("dt_fim_funcao7") != null ? new QtData(q.getTimestamp("dt_fim_funcao7"))
									: null);
					dados.setDsFuncaoFuncaoPPP7(q.getString("ds_funcao7"));
					dados.setDsCBOFuncaoPPP7(q.getString("ds_cbo7"));
					dados.setDsSetorFuncaoPPP7(q.getString("ds_setor7"));

					dados.setDtAdmissaoFucaoPPP8(q.getTimestamp("dt_admissao_funcao8") != null
							? new QtData(q.getTimestamp("dt_admissao_funcao8"))
							: null);
					dados.setDtFimFincaoFuncaoPPP8(
							q.getTimestamp("dt_fim_funcao8") != null ? new QtData(q.getTimestamp("dt_fim_funcao8"))
									: null);
					dados.setDsFuncaoFuncaoPPP8(q.getString("ds_funcao8"));
					dados.setDsCBOFuncaoPPP8(q.getString("ds_cbo8"));
					dados.setDsSetorFuncaoPPP8(q.getString("ds_setor8"));

					dados.setDtAdmissaoFucaoPPP9(q.getTimestamp("dt_admissao_funcao9") != null
							? new QtData(q.getTimestamp("dt_admissao_funcao9"))
							: null);
					dados.setDtFimFincaoFuncaoPPP9(
							q.getTimestamp("dt_fim_funcao9") != null ? new QtData(q.getTimestamp("dt_fim_funcao9"))
									: null);
					dados.setDsFuncaoFuncaoPPP9(q.getString("ds_funcao9"));
					dados.setDsCBOFuncaoPPP9(q.getString("ds_cbo9"));
					dados.setDsSetorFuncaoPPP9(q.getString("ds_setor9"));

					dados.setDtAdmissaoFucaoPPP10(q.getTimestamp("dt_admissao_funcao10") != null
							? new QtData(q.getTimestamp("dt_admissao_funcao10"))
							: null);
					dados.setDtFimFincaoFuncaoPPP10(
							q.getTimestamp("dt_fim_funcao10") != null ? new QtData(q.getTimestamp("dt_fim_funcao10"))
									: null);
					dados.setDsFuncaoFuncaoPPP10(q.getString("ds_funcao10"));
					dados.setDsCBOFuncaoPPP10(q.getString("ds_cbo10"));
					dados.setDsSetorFuncaoPPP10(q.getString("ds_setor10"));

					dados.setDtMudancaFuncao(q.getTimestamp("dt_mudanca_funcao") != null
							? new QtData(q.getTimestamp("dt_mudanca_funcao"))
							: null);
					dados.setDtMudancaFimFuncao(q.getTimestamp("dt_mudanca_fim_funcao") != null
							? new QtData(q.getTimestamp("dt_mudanca_fim_funcao"))
							: null);
					dados.setDsMudancaFuncao(q.getBString("ds_mudanca_funcao"));
					dados.setDsMudancaCbo(q.getString("ds_mudanca_cbo"));
					dados.setDsMudancaSetor(q.getString("ds_mudanca_setor"));

					dados.setDsObservacoes(q.getString("ds_observacoes"));
					dados.setDtConfirmacao(
							q.getTimestamp("dt_confirmacao") != null ? new QtData(q.getTimestamp("dt_confirmacao"))
									: null);

					dados.setIdUsuarioConfirmacao(q.getInteger("id_usuario_confirmacao"));

					lista.add(dados);
					q.next();
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
			msgExibida = 0;
			msgAoUsuario = "Ops! " + e.getMessage();
		}

		return lista;
	}

	private List<Risco> carregaListagemRiscos(ConexaoParaPoolDeConexoes cnx) throws QtSQLException {
		List<Risco> riscos = new LinkedList<>();

		FiltroDeNavegador filtro = new FiltroDeNavegador();
		filtro.setOrdemDeNavegacao(RiscoCaminhador.POR_CODIGO);

		RiscoCaminhador caminhador = new RiscoCaminhador(cnx, filtro);
		caminhador.ativaCaminhador();

		if (!caminhador.isEmpty()) {
			while (!caminhador.isEof()) {
				riscos.add(caminhador.getRisco());
				caminhador.proximo();
			}
		}

		return riscos;
	}

	private List<FiliaisEmpresa> carregaFiliais(Conexao cnx, Integer idEmpresa) {

		List<FiliaisEmpresa> lista = new LinkedList<>();
		try {
			listaFiliais.clear();
			FiltroDeNavegador filtro = new FiltroDeNavegador();
			filtro.adicionaCondicao("cd_empresa", FiltroDeNavegador.IGUAL,
					jUni.biblioteca.util.Util.strZero(idEmpresa, 4));

			FiliaisEmpresaCaminhador filiais = new FiliaisEmpresaCaminhador(cnx, filtro);
			filiais.ativaCaminhador();

			if (!filiais.isEmpty()) {
				while (!filiais.isEof()) {
					lista.add(filiais.getFiliaisEmpresa());
					filiais.proximo();
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			msgExibida = 0;
			msgAoUsuario = "Ops! " + e.getMessage();
		}

		return lista;
	}

	public void selecionaEstado(String cdEstado) {
		listaCidades.clear();
		listaCidades = carregaListaCidades(cdEstado);
	}

	private List<Estado> carregaListaEstados(ConexaoParaPoolDeConexoes cnx) throws QtSQLException {
		List<Estado> lista = new LinkedList<>();
		FiltroDeNavegador filtro = new FiltroDeNavegador();

		EstadoCaminhador estados = new EstadoCaminhador(cnx, filtro);
		estados.setOrdemDeNavegacao(CidadeCaminhador.POR_NOME);
		estados.ativaCaminhador();

		if (!estados.isEmpty()) {
			while (!estados.isEof()) {
				lista.add(estados.getEstado());
				estados.proximo();
			}
		}

		return lista;
	}

	private List<PaisAns> carregaListaPaises(ConexaoParaPoolDeConexoes cnx) throws QtSQLException {
		List<PaisAns> lista = new LinkedList<>();
		FiltroDeNavegador filtro = new FiltroDeNavegador();

		PaisAnsCaminhador paises = new PaisAnsCaminhador(cnx, filtro);
		paises.setOrdemDeNavegacao(CidadeCaminhador.POR_NOME);
		paises.ativaCaminhador();

		if (!paises.isEmpty()) {
			while (!paises.isEof()) {
				lista.add(paises.getPaisAns());
				paises.proximo();
			}
		}

		return lista;
	}

	private List<Beneficiario> carregaListagemBeneficiarios(Conexao cnx, String cdCartao, String dsNome,
			String cdEmpresaPesquisa, boolean ativo) {
		List<Beneficiario> lista = new LinkedList<>();

		FiltroDeNavegador filtro = new FiltroDeNavegador();

		if (idEmpresaRelacioanda != null) {
			filtro.adicionaCondicao("cd_empresa", FiltroDeNavegador.IGUAL,
					jUni.biblioteca.util.Util.strZero(idEmpresaRelacioanda, 4));
		} else {
			filtro.adicionaCondicao("cd_empresa", FiltroDeNavegador.MAIOR_OU_IGUAL, "8000");
			filtro.adicionaCondicao("cd_empresa", FiltroDeNavegador.MENOR_OU_IGUAL, "9000");
			if (cdEmpresaPesquisa != null && !cdEmpresaPesquisa.trim().equals("")) {
				filtro.adicionaCondicao("cd_empresa", FiltroDeNavegador.IGUAL, cdEmpresaPesquisa);
			}
		}

		if (ativo) {
			filtro.adicionaCondicao("dt_exclusao", FiltroDeNavegador.IGUAL, null);
		} else {
			filtro.adicionaCondicao("dt_exclusao", FiltroDeNavegador.DIFERENTE, null);
		}

		if (cdCartao != null && !cdCartao.equals("")) {
			filtro.adicionaCondicao("cd_beneficiario_cartao", FiltroDeNavegador.ILIKE, "%" + cdCartao + "%");
		}
		if (dsNome != null && !dsNome.equals("")) {
			filtro.adicionaCondicao("ds_nome", FiltroDeNavegador.ILIKE, "%" + dsNome.replaceAll(" ", "%") + "%");
		}

		try {
			BeneficiarioCaminhador benef = new BeneficiarioCaminhador(cnx, filtro);
			benef.setOrdemDeNavegacao(PreCadastroCboCaminhador.POR_ORDEM_DEFAULT);
			
			// se usu�rio luciont
			if ( usuarioLogado.getIdUsuario().equals(162) ) {
				benef.setNrLimiteDeRegistros(50);
				
			} else if ( usuarioLogado.getIdUsuario().equals( 2519 ) ) {
				benef.setNrLimiteDeRegistros(50);
			}
			benef.ativaCaminhador();

			if (!benef.isEmpty()) {
				while (!benef.isEof()) {
					lista.add(benef.getBeneficiario());
					benef.proximo();
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			msgExibida = 0;
			msgAoUsuario = "Ops! " + e.getMessage();
		}

		return lista;
	}

	private List<Lotacao> carregaLotacao(ConexaoParaPoolDeConexoes cnx) throws QtSQLException {
		List<Lotacao> lista = new LinkedList<>();

		listaLotacao.clear();

		FiltroDeNavegador filtro = new FiltroDeNavegador();
		filtro.adicionaCondicao("cd_unimed", FiltroDeNavegador.IGUAL,
				ParametrosSistema.getInstance().getCodigoDoCliente());
		if (idLotacaoPesquisa != null && !idLotacaoPesquisa.equals("")) {
			filtro.adicionaCondicao("id_lotacao", FiltroDeNavegador.IGUAL, new Integer(idLotacaoPesquisa));
		}
		if (dsLotacaoPesquisa != null && !dsLotacaoPesquisa.equals("")) {
			filtro.adicionaCondicao("ds_lotacao", FiltroDeNavegador.ILIKE,
					"%" + dsLotacaoPesquisa.replaceAll(" ", "%") + "%");
		}
		if (idEmpresaRelacioanda != null) {
			filtro.adicionaCondicao("id_empresa", FiltroDeNavegador.IGUAL, new Integer(idEmpresaRelacioanda));
		}
		filtro.setOrdemDeNavegacao(LotacaoCaminhador.POR_DESCRICAO);

		LotacaoCaminhador lotacoes = new LotacaoCaminhador(cnx, filtro);
		lotacoes.ativaCaminhador();

		if (!lotacoes.isEmpty()) {
			while (!lotacoes.isEof()) {
				lista.add(lotacoes.getLotacao());
				lotacoes.proximo();
			}
		}

		return lista;
	}

	private List<MotivoExclusaoBeneficiario> getMotivosDeExclusao(ConexaoParaPoolDeConexoes cnx) throws QtSQLException {
		List<MotivoExclusaoBeneficiario> lista = new LinkedList<>();
		FiltroDeNavegador filtro = new FiltroDeNavegador();

		MotivoExclusaoBeneficiarioCaminhador motivos = new MotivoExclusaoBeneficiarioCaminhador(cnx, filtro);
		motivos.ativaCaminhador();

		if (!motivos.isEmpty()) {
			while (!motivos.isEof()) {
				lista.add(motivos.getMotivoExclusaoBeneficiario());
				motivos.proximo();
			}
		}

		return lista;
	}

	public void pesquisaBeneficiario() {

		try {
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
			try {
				listagemBeneficiario.clear();
				listagemBeneficiario = carregaListagemBeneficiarios(cnx, cdCartaoPesquisa, dsNomePesquisa,
						cdEmpresaPesquisa, true);

				cdCartaoPesquisa = null;
				dsNomePesquisa = null;
				cdEmpresaPesquisa = null;

			} finally {
				cnx.libera();
			}
		} catch (Exception e) {
			e.printStackTrace();
			msgExibida = 0;
			msgAoUsuario = "Ops! " + e.getMessage();
		}

	}

	public void pesquisaBeneficiarioInativo() {

		try {
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
			try {
				listagemBeneficiarioInativo.clear();
				listagemBeneficiarioInativo = carregaListagemBeneficiarios(cnx, cdCartaoPesquisa, dsNomePesquisa,
						cdEmpresaPesquisa, false);

				cdCartaoPesquisa = null;
				dsNomePesquisa = null;
				cdEmpresaPesquisa = null;

			} finally {
				cnx.libera();
			}
		} catch (Exception e) {
			e.printStackTrace();
			msgExibida = 0;
			msgAoUsuario = "Ops! " + e.getMessage();
		}

	}

	public void pesquisaPreCadastro() {

		try {
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
			try {
				listagemPreCadastro = carregaListagem(cnx, cdCartaoPesquisa, dsNomePesquisa);
			} finally {
				cnx.libera();
			}
		} catch (Exception e) {
			e.printStackTrace();
			msgExibida = 0;
			msgAoUsuario = "Ops! " + e.getMessage();
		}

	}

	private Integer getEmpresaRelacionada(Conexao cnx, Usuario usuario) {
		if (usuario != null) {
			try {
				Query q = new Query(cnx);
				q.addSQL("select dspermissao from jdesk.permissoes_especiais_usuario u, jdesk.permissoes_especiais e");
				q.addSQL("where u.idusuario = :prmIdUsuario");
				q.addSQL("and u.sqpermissao = e.sqpermissao");
				q.setParameter("prmIdUsuario", usuario.getIdUsuario());

				q.executeQuery();

				if (!q.isEmpty()) {

					try {
						Integer idPermissao = new Integer(q.getString("dspermissao"));
						return idPermissao;
					} catch (Exception e) {
						e.printStackTrace();
						return null;
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				msgExibida = 0;
				msgAoUsuario = "Ops! " + e.getMessage();
			}
		}
		return null;
	}

	public void limparFiltro() {
		cdCartaoPesquisa = null;
		dsNomePesquisa = null;
		pesquisaBeneficiario();
	}

	public void limparFiltroPreCadastro() {
		cdCartaoPesquisa = null;
		dsNomePesquisa = null;
		pesquisaPreCadastro();
	}

	public void selecionaBeneficiarioParaExclusao(Beneficiario benef) {

		PreCadastroCbo pre = null;

		if (benef != null) {

			try {
				if (usuarioLogado == null) {
					usuarioLogado = jUni.controleLogin.Util.getDadosUsuarioLogado();
				}
				
				if( idMotivoExclusaoSelecionado == null ) {
					throw new Exception("Motivo de exclus�o � obrigat�rio.");
				}
				if( dtDemissaoSelecionada == null ) {
					throw new Exception("Data de exclus�o � obrigat�rio.");
				}
				
				ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
				try {
					pre = PreCadastroCboLocalizador.buscaporBeneficiario(cnx, benef.getCdBeneficiarioCartao());
					if (pre != null) {

						pre.setIdMotivoSolicitacaoExclusao(idMotivoExclusaoSelecionado);
						pre.setDtSolicitacaoExclusao( new QtData( dtExclusaoSelecionada ));
						pre.setDtDemissao(dtDemissaoSelecionada != null && !dtDemissaoSelecionada.isEmpty() ? new QtData(dtDemissaoSelecionada) : null );

						pre.setTpSolicitacao("E");

						PreCadastroCboNegocio preNegocio = new PreCadastroCboNegocio(cnx, pre);
						preNegocio.altera();

						DadosSolicitacaoPCMSO dados = new DadosSolicitacaoPCMSO();
						dados.setIdUsuarioSolicitou(usuarioLogado.getIdUsuario());
						dados.setCdUnimed(pre.getCdUnimed());
						dados.setCdEmpresa(pre.getCdEmpresa());
						dados.setCdFamilia(getCdFamilia(pre.getCdBeneficiarioCartao()));
						dados.setCdDependencia(getCdDependencia(pre.getCdBeneficiarioCartao()));
						if (pre.getCdUnimed() != null && pre.getCdBeneficiarioCartao() != null) {
							dados.setCdDigito(getCdDigito(pre.getCdUnimed() + pre.getCdBeneficiarioCartao()));
						}
						dados.setNmBeneficiario(pre.getDsNome());
						dados.setDsObservacoes("Motivo de exclus�o: " + idMotivoExclusaoSelecionado + ", Data: " + dtExclusaoSelecionada+ ", Demiss�o: " + dtDemissaoSelecionada );
						dados.setDtSolicitacao(new QtData());
						dados.setTpSolicitacao("E");
						dados.setDtConfirmacao(null);
						dados.setIdUsuarioConfirmacao(null);

						SolicitacaoPCMSO solicitacao = new SolicitacaoPCMSO(dados);
						solicitacao.incluiSolicitacao(cnx);

						listagemPreCadastro = carregaListagem(cnx, null, null);

						msgExibida = 0;
						msgAoUsuario = "A Solicita��o de exclus�o de " + pre.getDsNome()
								+ ", foi processada com sucesso.";
						enviaEmail( cnx, pre, "E");
						Util.sendRedirect("resources/page/dashboard.jsf");
					} else {

						pre = new PreCadastroCbo();
						pre.setIdMotivoSolicitacaoExclusao(idMotivoExclusaoSelecionado);
						pre.setDtSolicitacaoExclusao(new QtData(dtExclusaoSelecionada));
						pre.setDtDemissao(dtDemissaoSelecionada != null && !dtDemissaoSelecionada.isEmpty() ? new QtData(dtDemissaoSelecionada) : null);
						pre.setTpSolicitacao("E");
						pre.setCdBeneficiarioCartao(benef.getCdBeneficiarioCartao());
						pre.setCdEmpresa(benef.getCdEmpresa());
						pre.setCdEstado(benef.getCdEstadoRg());
						pre.setCdNaEmpresa(benef.getCdNaEmpresa());
						pre.setCdRg(benef.getCdRg());
						pre.setCdUnimed(benef.getCdUnimed());
						pre.setDsBairro(benef.getDsBairro());
						pre.setDsComplementoLogradouro(benef.getDsComplementoLogradouro());
						pre.setDsLogradouro(benef.getDsLogradouro());
						pre.setDsNome(benef.getDsNome());
						pre.setDsNomeDaMae(benef.getDsNomeDaMae());
						pre.setDsNumeroLogradouro(benef.getDsNumeroLogradouro());
						pre.setDsOrgaoEmissorRg(benef.getDsOrgaoEmissorRg());
						pre.setDsTelefoneContato(benef.getDsTelefone());
						pre.setDtExclusao(benef.getDtExclusao());
						pre.setDtInclusao(benef.getDtInclusao());
						pre.setDtNascimento(benef.getDtNascimento());
						pre.setDtUltimoMovimento(benef.getDtUltimaMovimentacao());
						pre.setIdCidade(benef.getIdCidadeResidencia());
						pre.setIdFilial(benef.getIdFilial());
						pre.setIdLotacao(benef.getIdLotacao());
						pre.setIdUsuarioDigitou((Integer) Util.getSession().getAttribute("IdUsuarioLogado"));
						pre.setNrCpf(benef.getCdCpf());
						pre.setNrPis(benef.getCdPisPasep());
						pre.setTpEstadoCivil(benef.getTpEstadoCivil());
						
						DadosSolicitacaoPCMSO dados = new DadosSolicitacaoPCMSO();
						dados.setIdUsuarioSolicitou(usuarioLogado.getIdUsuario());
						dados.setCdUnimed(pre.getCdUnimed());
						dados.setCdEmpresa(pre.getCdEmpresa());
						dados.setCdFamilia(getCdFamilia(pre.getCdBeneficiarioCartao()));
						dados.setCdDependencia(getCdDependencia(pre.getCdBeneficiarioCartao()));
						dados.setCdDigito(getCdDigito(pre.getCdBeneficiarioCartao()));
						dados.setNmBeneficiario(pre.getDsNome());
						dados.setDsObservacoes("Motivo de exclus�o: " + idMotivoExclusaoSelecionado + ", Data: " + dtExclusaoSelecionada+ ", Demiss�o: " + dtDemissaoSelecionada );
						dados.setDtSolicitacao(new QtData());
						dados.setTpSolicitacao("E");
						dados.setDtConfirmacao(null);
						dados.setIdUsuarioConfirmacao(null);

						SolicitacaoPCMSO solicitacao = new SolicitacaoPCMSO(dados);
						solicitacao.incluiSolicitacao(cnx);

						PreCadastroCboNegocio preNegocio = new PreCadastroCboNegocio(cnx, pre);
						preNegocio.insere();
						listagemPreCadastro = carregaListagem(cnx, null, null);

						msgExibida = 0;
						msgAoUsuario = "A Solicita��o de exclus�o de " + pre.getDsNome()
								+ ", foi processada com sucesso.";
						enviaEmail( cnx, pre, "E");
						Util.sendRedirect("resources/page/dashboard.jsf");

					}

				} finally {
					cnx.libera();
				}
			} catch (Exception e) {
				e.printStackTrace();
				msgExibida = 0;
				msgAoUsuario = "Ops! " + e.getMessage();
			}
		}
	}
	
	private void enviaEmailAlteracao( Conexao cnx, BeneficiarioCbo pre ) {
		
		try {
			
			jUni.biblioteca.mensageiro.Email email = new jUni.biblioteca.mensageiro.Email();
			email.setPara( usuarioLogado.getDsEmail() );
			email.setDe("NoReply@quatro.com.br");
			
			StringBuffer mensagem = new StringBuffer();

			mensagem.append("Prezado(a) <b>" + jUni.biblioteca.util.Util.trocaAcentuacao( usuarioLogado.getDsNomeUsuario() ) + "</b>," );
			mensagem.append("<br/>");
			mensagem.append("<br/>");
			mensagem.append("Recebemos a solicitacao de ");
			email.setAssunto("PCMSO Unimed - Confirma��o de Altera��o de Beneficiario");
			mensagem.append("<b>alteracao</b> que voce solicitou.");
			if (pre.getCdBeneficiarioCartao() != null) {
				mensagem.append( getCampoHtml("BENEFICIARIO", pre.getCdBeneficiarioCartao() ) );
			}

			mensagem.append(getCampoHtml("DATA ALTERACAO", new QtData().dataEHora() ) );
			mensagem.append("<br/>");
			jPcmso.persistencia.geral.e.Empresa e = jPcmso.persistencia.geral.e.EmpresaLocalizador.buscaEmpresa( cnx, pre.getCdEmpresa() );
			if (e != null) {
				mensagem.append(getCampoHtml("EMPRESA", pre.getCdEmpresa() + " - " + e.getDsEmpresa()));
			}

			mensagem.append(getCampoHtml("NOME BENEFICIARIO", pre.getDsNome()));
			if( pre.getIdFuncao() != null ) {
				Funcao f = FuncaoLocalizador.buscaFuncao(pre.getIdFuncao());
				if (f != null) {
					mensagem.append(getCampoHtml("FUNCAO", f.getDsFuncao()));
				}
			}
			if (pre.getIdSetor() != null) {
				Setor s = SetorLocalizador.buscaSetor(pre.getIdSetor());
				if (s != null) {
					mensagem.append(getCampoHtml("SETOR", s.getDsSetor()));
				}
			}
			if( pre.getIdCargo() != null ) {
				Cargo c = CargoLocalizador.buscaCargo(pre.getIdCargo());
				if (c != null) {
					mensagem.append(getCampoHtml("CARGO", c.getDsCargo()));
				}
			}
			
			if (pre.getIdLotacao() != null) {
				Lotacao lotacao = LotacaoLocalizador.buscaLotacao(ParametrosCliente.getCodigoCliente(),
						Inteiro.stoi(pre.getCdEmpresa()), pre.getIdLotacao());
				if (lotacao != null) {
					mensagem.append(
							getCampoHtml("LOTACAO", lotacao.getIdLotacao() + ": " + lotacao.getDsLotacao()));
				}
			}

			mensagem.append("<br/>");
			mensagem.append("<br/>");
			mensagem.append("Atenciosamente,");
			mensagem.append("<br/>");
			mensagem.append("Unimed Alto Uruguai");
			email.setMensagem( mensagem );
			email.setConteudo(Email.TEXT_HTML);
			
			jUni.biblioteca.mensageiro.Mailer mailer = new jUni.biblioteca.mensageiro.Mailer( email, dsHostEnvio, dsPortaEnvio, dsEmailEnvio, dsSenhaEnvio );
			mailer.send();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void enviaEmail( Conexao cnx, PreCadastroCbo pre, String tpSolicitacao ) {
		
		try {
			
			if (tpSolicitacao != null && !tpSolicitacao.equals("N")) {
				PreCadastroCbo preCadastroCboAnterior = null;

				boolean inclusao = false;

				jUni.biblioteca.mensageiro.Email email = new jUni.biblioteca.mensageiro.Email();
				email.setPara( usuarioLogado.getDsEmail() );
				email.setDe("NoReply@quatro.com.br");
				
				StringBuffer mensagem = new StringBuffer();

				mensagem.append("Prezado(a) <b>" + jUni.biblioteca.util.Util.trocaAcentuacao( usuarioLogado.getDsNomeUsuario() ) + "</b>," );
				mensagem.append("<br/>");
				mensagem.append("<br/>");
				mensagem.append("Recebemos a solicitacao de ");
				switch (tpSolicitacao.charAt(0)) {
				case 'I':
					email.setAssunto("PCMSO Unimed - Confirma��o de Inclus�o de Beneficiario");
					mensagem.append("<b>inclusao</b> que voce solicitou.");
					inclusao = true;
					break;
				case 'E':
					email.setAssunto("PCMSO Unimed - Confirma��o de exclus�o de Beneficiario");
					
					String motivo = "";

					if (pre.getIdMotivoSolicitacaoExclusao() != null) {
						MotivoExclusaoBeneficiario motivoExclusaoBeneficiario = MotivoExclusaoBeneficiarioLocalizador
								.buscaMotivoExclusaoBeneficiario(pre.getIdMotivoSolicitacaoExclusao());
						if (motivoExclusaoBeneficiario != null) {
							motivo += motivoExclusaoBeneficiario.getIdMotivoExclusao();
							motivo += " - ";
							motivo += motivoExclusaoBeneficiario.getDsMotivoExclusao();
						}
					}
					mensagem.append( "<b>exclusao</b> para a Data (" + pre.getDtSolicitacaoExclusao().toString() + ") que voce solicitou." );
					mensagem.append("<br/>");
					mensagem.append( getCampoHtml("MOTIVO", !motivo.isEmpty() ? motivo : "" ) );
					mensagem.append("<br/>");
					
					break;
				case 'A':
					email.setAssunto("PCMSO Unimed - Confirma��o de Altera��o de Beneficiario");
					mensagem.append("<b>alteracao</b> que voce solicitou.");
					break;

				}
				if (!inclusao) {
					preCadastroCboAnterior = PreCadastroCboLocalizador.buscaPreCadastroCbo(pre.getIdPreCadastro());
				}

				if (preCadastroCboAnterior == null) {
					inclusao = true;
				}
				if (pre.getCdBeneficiarioCartao() != null) {
					mensagem.append( getCampoHtml("BENEFICIARIO", pre.getCdBeneficiarioCartao() ) );
				}

				mensagem.append(getCampoHtml("DATA SOLICITAÇÃO", pre.getDtSolicitacao().dataEHora() ) );
				if( pre.getDtSolicitacaoExclusao() != null ) {
					mensagem.append(getCampoHtml("DATA EXCLUSAO", pre.getDtSolicitacaoExclusao().toString() ) );
				}
				if( pre.getDtDemissao() != null ) {
					mensagem.append(getCampoHtml("DATA DEMISSAO", pre.getDtDemissao().toString() ) );
				}
				mensagem.append("<br/>");
				jPcmso.persistencia.geral.e.Empresa e = jPcmso.persistencia.geral.e.EmpresaLocalizador.buscaEmpresa( cnx, pre.getCdEmpresa() );
				if (e != null) {
					mensagem.append(getCampoHtml("EMPRESA", pre.getCdEmpresa() + " - " + e.getDsEmpresa()));
				}

				mensagem.append(getCampoHtml("NOME BENEFICIARIO", pre.getDsNome()));

				if (pre.getDtAdmissao() != null) {
					if (inclusao || preCadastroCboAnterior.getDtAdmissao() == null
							|| (!preCadastroCboAnterior.getDtAdmissao().eIgual(pre.getDtAdmissao()))) {
						mensagem.append(getCampoHtml("DATA DE ADMISAO", pre.getDtAdmissao().toString()));
					}
				}
				if( pre.getIdFilial() != null ) {
					if( inclusao || compara( preCadastroCboAnterior.getIdFilial(), pre.getIdFilial() )  ) {
						if( pre.getIdFilial() != null ) {
							FiliaisEmpresa filial = FiliaisEmpresaLocalizador.buscaFiliaisEmpresa( pre.getCdEmpresa(), pre.getIdFilial() );
							if( filial != null ) {
								mensagem.append(getCampoHtml("FILIAL", pre.getIdFilial() + " - " + filial.getDsFilial() ) );
							}
						}
					}
				}
				
				if (inclusao || compara(preCadastroCboAnterior.getTpSexo(), pre.getTpSexo())) {
					if (pre.getTpSexo() != null) {
						mensagem.append(getCampoHtml("SEXO", pre.getTpSexo().equals("M") ? "Masculino" : "Feminino"));
					}
				}

				if (pre.getDtNascimento() != null) {
					if (inclusao || preCadastroCboAnterior.getDtNascimento() == null
							|| (!preCadastroCboAnterior.getDtNascimento().eIgual(pre.getDtNascimento()))) {
						mensagem.append(getCampoHtml("DATA DE NASCIMENTO", pre.getDtNascimento().toString()));
					}
				}

				if (inclusao || compara(preCadastroCboAnterior.getNrCpf(), pre.getNrCpf())) {
					if (pre.getNrCpf() != null) {
						mensagem.append(getCampoHtml("CPF", pre.getNrCpf()));
					}
				}

				if (inclusao || compara(preCadastroCboAnterior.getCdRg(), pre.getCdRg())) {
					if (pre.getCdRg() != null) {
						mensagem.append(getCampoHtml("RG", pre.getCdRg()));
					}
				}
				if (inclusao || compara(preCadastroCboAnterior.getDsOrgaoEmissorRg(), pre.getDsOrgaoEmissorRg())) {
					if (pre.getDsOrgaoEmissorRg() != null) {
						mensagem.append(getCampoHtml("ORGAO RG", pre.getDsOrgaoEmissorRg()));
					}
				}

				if (inclusao || compara(preCadastroCboAnterior.getDsNomeDaMae(), pre.getDsNomeDaMae())) {
					if (pre.getDsNomeDaMae() != null) {
						mensagem.append(getCampoHtml("NOME DA MAE", pre.getDsNomeDaMae()));
					}
				}

				if (inclusao || compara(preCadastroCboAnterior.getDsLogradouro(), pre.getDsLogradouro())) {
					if (pre.getDsLogradouro() != null) {
						mensagem.append(getCampoHtml("LOGRADOURO", pre.getDsLogradouro()));
					}
				}

				if (inclusao || compara(preCadastroCboAnterior.getDsNumeroLogradouro(), pre.getDsNumeroLogradouro())) {
					if (pre.getDsNumeroLogradouro() != null) {
						mensagem.append(getCampoHtml("NUMERO", pre.getDsNumeroLogradouro()));
					}
				}
				if (inclusao
						|| compara(preCadastroCboAnterior.getDsComplementoLogradouro(), pre.getDsComplementoLogradouro())) {
					if (pre.getDsComplementoLogradouro() != null) {
						mensagem.append(getCampoHtml("COMPLEMENTO", pre.getDsComplementoLogradouro()));
					}
				}
				if (inclusao || compara(preCadastroCboAnterior.getDsBairro(), pre.getDsBairro())) {
					if (pre.getDsBairro() != null) {
						mensagem.append(getCampoHtml("BAIRRO", pre.getDsBairro()));
					}
				}

				if (inclusao || compara(preCadastroCboAnterior.getIdCidade() + "", pre.getIdCidade() + "")) {
					if (pre.getIdCidade() != null) {
						Cidade c = CidadeLocalizador.buscaCidade(pre.getCdEstado(), pre.getIdCidade());
						if (c != null) {
							mensagem.append(getCampoHtml("ESTADO", pre.getCdEstado()));
							mensagem.append(getCampoHtml("CIDADE", c.getDsCidade()));
						}
					}
				}
				if (inclusao || compara(preCadastroCboAnterior.getTpEstadoCivil(), pre.getTpEstadoCivil())) {
					if (pre.getTpEstadoCivil() != null) {
						mensagem.append("<br/>");
						mensagem.append("<b>ESTADO CIVIL:&nbsp;</b>");
						if (pre.getTpEstadoCivil().equals("S")) {
							mensagem.append("Solteiro");
						} else if (pre.getTpEstadoCivil().equals("M")) {
							mensagem.append("Casado (M-Married)");
						} else if (pre.getTpEstadoCivil().equals("W")) {
							mensagem.append("Viuvo (W-Widow)");
						} else if (pre.getTpEstadoCivil().equals("D")) {
							mensagem.append("Divorciado");
						} else if (pre.getTpEstadoCivil().equals("A")) {
							mensagem.append("Apartado (Separado)");
						} else if (pre.getTpEstadoCivil().equals("U")) {
							mensagem.append("Uniao Estavel");
						}

					}
				}

				if (inclusao || compara(preCadastroCboAnterior.getDsTelefoneContato(), pre.getDsTelefoneContato())) {
					if (pre.getDsTelefoneContato() != null) {
						mensagem.append(getCampoHtml("TELEFONE DE CONTATO", pre.getDsTelefoneContato()));
					}
				}

				if (inclusao || compara(preCadastroCboAnterior.getDsAtividade(), pre.getDsAtividade())) {
					if (pre.getDsAtividade() != null) {
						mensagem.append(getCampoHtml("ATIVIDADE", pre.getDsAtividade()));
					}
				}

				if (inclusao || compara(preCadastroCboAnterior.getNrPis(), pre.getNrPis())) {
					if (pre.getNrPis() != null) {
						mensagem.append(getCampoHtml("PIS/Pasep", pre.getNrPis()));
					}
				}
				if (inclusao || compara(preCadastroCboAnterior.getNrCtps(), pre.getNrCtps())) {
					if (pre.getNrCtps() != null) {
						mensagem.append(getCampoHtml("CTPS", pre.getNrCtps()));
					}
				}

				if (inclusao || compara(preCadastroCboAnterior.getNrSerieUf(), pre.getNrSerieUf())) {
					if (pre.getNrSerieUf() != null) {
						mensagem.append(getCampoHtml("SERIE (UF)", pre.getNrSerieUf()));
					}
				}

				if (inclusao || compara(preCadastroCboAnterior.getCdCbo(), pre.getCdCbo())) {
					if (pre.getCdCbo() != null) {
						mensagem.append("<br/>");
						mensagem.append("<b>CBO:&nbsp;</b>");
						Cbo cbo = CboLocalizador.buscaCbo(pre.getCdCbo());
						if (cbo != null) {
							mensagem.append(cbo.getNmCbo());
						} else {
							mensagem.append(pre.getCdCbo());
						}
					}
				}
				
				if( pre.getIdFuncao() != null ) {
					Funcao f = FuncaoLocalizador.buscaFuncao(pre.getIdFuncao());
					if (f != null) {
						mensagem.append(getCampoHtml("FUNCAO", f.getDsFuncao()));
					}
				}
				if (pre.getIdSetor() != null) {
					Setor s = SetorLocalizador.buscaSetor(pre.getIdSetor());
					if (s != null) {
						mensagem.append(getCampoHtml("SETOR", s.getDsSetor()));
					}
				}
				if( pre.getIdCargo() != null ) {
					Cargo c = CargoLocalizador.buscaCargo(pre.getIdCargo());
					if (c != null) {
						mensagem.append(getCampoHtml("CARGO", c.getDsCargo()));
					}
				}
				
				if (pre.getIdLotacao() != null) {
					Lotacao lotacao = LotacaoLocalizador.buscaLotacao(ParametrosCliente.getCodigoCliente(),
							Inteiro.stoi(pre.getCdEmpresa()), pre.getIdLotacao());
					if (lotacao != null) {
						mensagem.append(
								getCampoHtml("LOTACAO", lotacao.getIdLotacao() + ": " + lotacao.getDsLotacao()));
					}
				}

				if (inclusao || compara(jUni.biblioteca.util.Util.mascaraFloat(preCadastroCboAnterior.getVlPeso()),
						jUni.biblioteca.util.Util.mascaraFloat(pre.getVlPeso()))) {
					if (pre.getVlPeso() != null) {
						mensagem.append(getCampoHtml("PESO", jUni.biblioteca.util.Util.mascaraFloat(pre.getVlPeso())));
					}
				}

				if (inclusao || compara(jUni.biblioteca.util.Util.mascaraFloat(preCadastroCboAnterior.getVlAltura()),
						jUni.biblioteca.util.Util.mascaraFloat(pre.getVlAltura()))) {
					if (pre.getVlPeso() != null) {
						mensagem.append(getCampoHtml("ALTURA", jUni.biblioteca.util.Util.mascaraFloat(pre.getVlAltura())));
					}
				}
				if (inclusao || compara(preCadastroCboAnterior.isSnPossuiDeficiencia(), pre.isSnPossuiDeficiencia())) {
					mensagem.append(getCampoHtml("POSSUI ALGUMA DEFICIENCIA", pre.isSnPossuiDeficiencia() ? "SIM" : "NAO"));
				}
				
				if (inclusao || compara(preCadastroCboAnterior.getTpDeficiencia(), pre.getTpDeficiencia())) {
					if (pre.getTpDeficiencia() != null) {
						mensagem.append("<br/>");
						mensagem.append("<b>TIPO DE DEFICIENCIA:&nbsp;</b>");
						/** 
						 * Tipo de deficiencia do beneficiï¿½rio: (char(1))
						 * I-Intelectual
						 * F-Fisica
						 * A-Auditiva
						 * V-Visual
						 */
						if (pre.getTpDeficiencia().equals("I")) {
							mensagem.append("Intelectual");
						} else if (pre.getTpDeficiencia().equals("F")) {
							mensagem.append("FÃ­sica");
						} else if (pre.getTpDeficiencia().equals("A")) {
							mensagem.append("Auditiva");
						} else if (pre.getTpDeficiencia().equals("V")) {
							mensagem.append("Visual");
						} else {
							mensagem.append("Nenhuma");
						}
					}
				}
				if ( inclusao || compara(preCadastroCboAnterior.isSnTrabalhoEmAltura(), pre.isSnTrabalhoEmAltura())) {
					mensagem.append(getCampoHtml("TRABALHA EM ALTURA", pre.isSnTrabalhoEmAltura() ? "SIM" : "NAO"));
				}
				
				if ( inclusao || compara(preCadastroCboAnterior.isSnTrabalhoConfinado(), pre.isSnTrabalhoConfinado())) {
					mensagem.append(getCampoHtml("TRABALHA EM ESPACO CONFINADO", pre.isSnTrabalhoConfinado() ? "SIM" : "NAO"));
				}
				
				if ( inclusao || compara(preCadastroCboAnterior.isSnFitossanitarios(), pre.isSnFitossanitarios())) {
					mensagem.append(getCampoHtml("FITOSSANITARIOS(DEFENSIVOS AGRICOLAS)", pre.isSnFitossanitarios() ? "SIM" : "NAO"));
				}
				
				mensagem.append("<br/>");
				mensagem.append("<br/>");
				mensagem.append("Atenciosamente,");
				mensagem.append("<br/>");
				mensagem.append("Unimed Alto Uruguai");
				email.setMensagem( mensagem );
				email.setConteudo(Email.TEXT_HTML);
				
				jUni.biblioteca.mensageiro.Mailer mailer = new jUni.biblioteca.mensageiro.Mailer( email, dsHostEnvio, dsPortaEnvio, dsEmailEnvio, dsSenhaEnvio );
				mailer.send();
			}

			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void selecionaBeneficiarioParaExclusao(PreCadastroCbo pre) {

		pre.setIdMotivoSolicitacaoExclusao(idMotivoExclusaoSelecionado);
		pre.setDtSolicitacaoExclusao(new QtData(dtExclusaoSelecionada));
		pre.setDtDemissao( dtDemissaoSelecionada != null ? new QtData( dtDemissaoSelecionada ) : null );
		pre.setTpSolicitacao("E");

		try {
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
			try {

				PreCadastroCboNegocio preNegocio = new PreCadastroCboNegocio(cnx, pre);
				preNegocio.altera();
				listagemPreCadastro = carregaListagem(cnx, null, null);

				msgExibida = 0;
				msgAoUsuario = "A Solicita��o de exclus�o de " + pre.getDsNome() + ", foi processada com sucesso.";
				Util.sendRedirect("resources/page/dashboard.jsf");
			} finally {
				cnx.libera();
			}

		} catch (Exception e) {
			e.printStackTrace();
			msgExibida = 0;
			msgAoUsuario = "Ops! " + e.getMessage();
		}

	}

	public void solicitarPPP(Beneficiario benef) {
		boolean campoObrigatorio = false;

		try {

			if (dtAdmissaoFucaoPPP == null) {
				campoObrigatorio = true;
				msgAoUsuario = "Ops! Preencha a data de início da Admiss�o da Fun��o.";
			} else {
				if (dtAdmissaoFucaoPPP.equals("")) {
					campoObrigatorio = true;
					msgAoUsuario = "Ops! Preencha a data de início da Admiss�o da Fun��o.";
				}
			}
			// if (dtFimFincaoFuncaoPPP == null) {
			// campoObrigatorio = true;
			// msgAoUsuario = "Ops! Preencha a data de fim da Admiss�o da Fun��o.";
			// } else {
			// if (dtFimFincaoFuncaoPPP.equals("")) {
			// campoObrigatorio = true;
			// msgAoUsuario = "Ops! Preencha a data de fim da Admiss�o da Fun��o.";
			// }
			// }
			if (dsFuncaoFuncaoPPP == null) {
				campoObrigatorio = true;
				msgAoUsuario = "Ops! Preencha a Fun��o.";
			} else {
				if (dsFuncaoFuncaoPPP.equals("")) {
					campoObrigatorio = true;
					msgAoUsuario = "Ops! Preencha a Fun��o.";
				}
			}
			if (dsCBOFuncaoPPP == null) {
				campoObrigatorio = true;
				msgAoUsuario = "Ops! Preencha o CBO da Fun��o.";
			} else {
				if (dsCBOFuncaoPPP.equals("")) {
					campoObrigatorio = true;
					msgAoUsuario = "Ops! Preencha o CBO da Fun��o.";
				}

			}
			if (dsSetorFuncaoPPP == null) {
				campoObrigatorio = true;
				msgAoUsuario = "Ops! Preencha Setor da Fun��o.";
			} else {
				if (dsSetorFuncaoPPP.equals("")) {
					campoObrigatorio = true;
					msgAoUsuario = "Ops! Preencha Setor da Fun��o.";
				}
			}
			// if( dtMudancaTrocaPPP == null && dtMudancaTrocaPPP.equals("") ) {
			// campoObrigatorio = true;
			// }
			// if( dtFimMudancaTrocaPPP == null && dtFimMudancaTrocaPPP.equals("") ) {
			// campoObrigatorio = true;
			// }
			// if( dsFuncaoTrocaPPP == null && dsFuncaoTrocaPPP.equals("") ) {
			// campoObrigatorio = true;
			// }
			// if( dsCBOTrocaPPP == null && dsCBOTrocaPPP.equals("") ) {
			// campoObrigatorio = true;
			// }
			// if( dsSetorTrocaPPP == null && dsSetorTrocaPPP.equals("") ) {
			// campoObrigatorio = true;
			// }
		} catch (Exception e) {
			campoObrigatorio = true;
		}

		if (campoObrigatorio) {
			msgExibida = 0;
			return;
		}

		try {
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
			if (usuarioLogado == null) {
				usuarioLogado = jUni.controleLogin.Util.getDadosUsuarioLogado();
			}
			try {
				insererPPP(cnx, benef);
				initMensagemParametro(cnx);
				enviaEmailPPP(cnx, benef);

				dtAdmissaoPPP = "";
				dtDemissaoPPP = "";
				dtNascimentoPPP = "";
				nrNitPPP = "";
				nrCtpsPPP = "";
				nrSeriePPP = "";
				dsUFPPP = "";
				snAcidentePPP = "N";
				dtAcidentePPP = "";
				nrCatPPP = "";

				nmResponsavelPPP = "";
				nrNitResponsavelPPP = "";
				dtAdmissaoFucaoPPP = "";
				dtFimFincaoFuncaoPPP = "";
				dsFuncaoFuncaoPPP = "";

				dtAdmissaoFucaoPPP2 = "";
				dtFimFincaoFuncaoPPP2 = "";
				dsFuncaoFuncaoPPP2 = "";
				dsCBOFuncaoPPP2 = "";
				dsSetorFuncaoPPP2 = "";

				dtAdmissaoFucaoPPP3 = "";
				dtFimFincaoFuncaoPPP3 = "";
				dsFuncaoFuncaoPPP3 = "";
				dsCBOFuncaoPPP3 = "";
				dsSetorFuncaoPPP3 = "";

				dtAdmissaoFucaoPPP4 = "";
				dtFimFincaoFuncaoPPP4 = "";
				dsFuncaoFuncaoPPP4 = "";
				dsCBOFuncaoPPP4 = "";
				dsSetorFuncaoPPP4 = "";

				dtAdmissaoFucaoPPP5 = "";
				dtFimFincaoFuncaoPPP5 = "";
				dsFuncaoFuncaoPPP5 = "";
				dsCBOFuncaoPPP5 = "";
				dsSetorFuncaoPPP5 = "";

				dtAdmissaoFucaoPPP6 = "";
				dtFimFincaoFuncaoPPP6 = "";
				dsFuncaoFuncaoPPP6 = "";
				dsCBOFuncaoPPP6 = "";
				dsSetorFuncaoPPP6 = "";

				dtAdmissaoFucaoPPP7 = "";
				dtFimFincaoFuncaoPPP7 = "";
				dsFuncaoFuncaoPPP7 = "";
				dsCBOFuncaoPPP7 = "";
				dsSetorFuncaoPPP7 = "";

				dtAdmissaoFucaoPPP8 = "";
				dtFimFincaoFuncaoPPP8 = "";
				dsFuncaoFuncaoPPP8 = "";
				dsCBOFuncaoPPP8 = "";
				dsSetorFuncaoPPP8 = "";

				dtAdmissaoFucaoPPP9 = "";
				dtFimFincaoFuncaoPPP9 = "";
				dsFuncaoFuncaoPPP9 = "";
				dsCBOFuncaoPPP9 = "";
				dsSetorFuncaoPPP9 = "";

				dtAdmissaoFucaoPPP10 = "";
				dtFimFincaoFuncaoPPP10 = "";
				dsFuncaoFuncaoPPP10 = "";
				dsCBOFuncaoPPP10 = "";
				dsSetorFuncaoPPP10 = "";

				dsCBOFuncaoPPP = "";
				dsSetorFuncaoPPP = "";
				dtMudancaTrocaPPP = "";
				dtFimMudancaTrocaPPP = "";
				dsFuncaoTrocaPPP = "";
				dsCBOTrocaPPP = "";
				dsSetorTrocaPPP = "";
				dsObervacoesPPP = "";

				msgExibida = 0;
				msgAoUsuario = "Solicita��o de PPP Enviada.";

			} finally {
				cnx.libera();
			}

		} catch (Exception e) {
			e.printStackTrace();
			msgExibida = 0;
			msgAoUsuario = "Ops! " + e.getMessage();
		}
		Util.sendRedirect("resources/page/dashboard.jsf");
	}

	private void insererPPP(Conexao cnx, Beneficiario benef) throws Exception {
		DadosSolicitacaoPPPPCMSO dados = new DadosSolicitacaoPPPPCMSO();

		dados.setIdUsuarioSolicitou(usuarioLogado.getIdUsuario());
		dados.setCdBeneficiarioCartao(benef.getCdBeneficiarioCartao());
		dados.setNmBeneficiario(benef.getDsNome());
		dados.setDtSolicitacao(new QtData());
		dados.setDtAdmissao(dtAdmissaoPPP != null ? new QtData(dtAdmissaoPPP) : null);
		dados.setDtDemissao(dtDemissaoPPP != null ? new QtData(dtDemissaoPPP) : null);
		dados.setDtNascimento(dtNascimentoPPP != null ? new QtData(dtNascimentoPPP) : null);
		dados.setNrNitPis(nrNitPPP);
		dados.setNrCtps(nrCtpsPPP);
		dados.setNrSerie(nrSeriePPP);
		dados.setUfCtps(dsUFPPP);
		dados.setSnAcidentetrabalho(snAcidentePPP);
		dados.setDtAcidenteTrabalho(dtAcidentePPP != null ? new QtData(dtAcidentePPP) : null);
		dados.setNrCat(nrCatPPP);
		dados.setNmResponsavel(nmResponsavelPPP);
		dados.setNrNitPisResponsavel(nrNitResponsavelPPP);
		dados.setDtAdmissaoFuncao(dtAdmissaoFucaoPPP != null ? new QtData(dtAdmissaoFucaoPPP) : null);
		dados.setDtFimFuncao(dtFimFincaoFuncaoPPP != null ? new QtData(dtFimFincaoFuncaoPPP) : null);
		dados.setDsFuncao(dsFuncaoFuncaoPPP);
		dados.setDsCbo(dsCBOFuncaoPPP);
		dados.setDsSetor(dsSetorFuncaoPPP);

		dados.setDtAdmissaoFucaoPPP2(dtAdmissaoFucaoPPP2 != null ? new QtData(dtAdmissaoFucaoPPP2) : null);
		dados.setDtFimFincaoFuncaoPPP2(dtFimFincaoFuncaoPPP2 != null ? new QtData(dtFimFincaoFuncaoPPP2) : null);
		dados.setDsFuncaoFuncaoPPP2(dsFuncaoFuncaoPPP2);
		dados.setDsCBOFuncaoPPP2(dsCBOFuncaoPPP2);
		dados.setDsSetorFuncaoPPP2(dsSetorFuncaoPPP2);

		dados.setDtAdmissaoFucaoPPP3(dtAdmissaoFucaoPPP3 != null ? new QtData(dtAdmissaoFucaoPPP3) : null);
		dados.setDtFimFincaoFuncaoPPP3(dtFimFincaoFuncaoPPP3 != null ? new QtData(dtFimFincaoFuncaoPPP3) : null);
		dados.setDsFuncaoFuncaoPPP3(dsFuncaoFuncaoPPP3);
		dados.setDsCBOFuncaoPPP3(dsCBOFuncaoPPP3);
		dados.setDsSetorFuncaoPPP3(dsSetorFuncaoPPP3);

		dados.setDtAdmissaoFucaoPPP4(dtAdmissaoFucaoPPP4 != null ? new QtData(dtAdmissaoFucaoPPP4) : null);
		dados.setDtFimFincaoFuncaoPPP4(dtFimFincaoFuncaoPPP4 != null ? new QtData(dtFimFincaoFuncaoPPP4) : null);
		dados.setDsFuncaoFuncaoPPP4(dsFuncaoFuncaoPPP4);
		dados.setDsCBOFuncaoPPP4(dsCBOFuncaoPPP4);
		dados.setDsSetorFuncaoPPP4(dsSetorFuncaoPPP4);

		dados.setDtAdmissaoFucaoPPP5(dtAdmissaoFucaoPPP5 != null ? new QtData(dtAdmissaoFucaoPPP5) : null);
		dados.setDtFimFincaoFuncaoPPP5(dtFimFincaoFuncaoPPP5 != null ? new QtData(dtFimFincaoFuncaoPPP5) : null);
		dados.setDsFuncaoFuncaoPPP5(dsFuncaoFuncaoPPP5);
		dados.setDsCBOFuncaoPPP5(dsCBOFuncaoPPP5);
		dados.setDsSetorFuncaoPPP5(dsSetorFuncaoPPP5);

		dados.setDtMudancaFuncao(dtMudancaTrocaPPP != null ? new QtData(dtMudancaTrocaPPP) : null);
		dados.setDtMudancaFimFuncao(dtFimMudancaTrocaPPP != null ? new QtData(dtFimMudancaTrocaPPP) : null);
		dados.setDsMudancaFuncao(dsFuncaoTrocaPPP);
		dados.setDsMudancaCbo(dsCBOTrocaPPP);
		dados.setDsMudancaSetor(dsSetorTrocaPPP);
		dados.setDsObservacoes(dsObervacoesPPP);

		try {
			Query q = new Query(cnx);
			q.setSQL("insert into pcmso.solicitacoes_ppp_pcmso ");
			q.addSQL(
					"( id_usuario_solicitou, cd_beneficiario_cartao, dt_solicitacao, dt_admissao, dt_demissao, dt_nascimento, nr_nit_pis, nr_ctps, nr_serie, uf_ctps, sn_acidente_trabalho, dt_acidente, nr_cat, nm_responsavel, nr_nit_pis_responsavel, dt_admissao_funcao, dt_fim_funcao, ds_funcao, ds_cbo, ds_setor, dt_mudanca_funcao, dt_mudanca_fim_funcao, ds_mudanca_funcao, ds_mudanca_cbo, ds_mudanca_setor, ds_observacoes, nm_beneficiario, dt_admissao_funcao2, dt_fim_funcao2, ds_funcao2, ds_cbo2, ds_setor2, dt_admissao_funcao3, dt_fim_funcao3, ds_funcao3, ds_cbo3, ds_setor3, dt_admissao_funcao4, dt_fim_funcao4, ds_funcao4, ds_cbo4, ds_setor4,  ");
			q.addSQL("dt_admissao_funcao5, dt_fim_funcao5, ds_funcao5, ds_cbo5, ds_setor5,");
			q.addSQL("dt_admissao_funcao6, dt_fim_funcao6, ds_funcao6, ds_cbo6, ds_setor6,");
			q.addSQL("dt_admissao_funcao7, dt_fim_funcao7, ds_funcao7, ds_cbo7, ds_setor7,");
			q.addSQL("dt_admissao_funcao8, dt_fim_funcao8, ds_funcao8, ds_cbo8, ds_setor8,");
			q.addSQL("dt_admissao_funcao9, dt_fim_funcao9, ds_funcao9, ds_cbo9, ds_setor9,");
			q.addSQL("dt_admissao_funcao10, dt_fim_funcao10, ds_funcao10, ds_cbo10, ds_setor10");
			q.addSQL(")");
			q.addSQL("values");
			q.addSQL(" ( :prmid_usuario_solicitou, :prmcd_beneficiario_cartao, '" + new QtData().getQtDataAsTimestamp()
					+ "', "
					+ " :prmdt_admissao, :prmdt_demissao, :prmdt_nascimento, :prmnr_nit_pis, :prmnr_ctps, :prmnr_serie, :prmuf_ctps, :prmsn_acidente_trabalho, :prmdt_acidente, :prmnr_cat, :prmnm_responsavel, :prmnr_nit_pis_responsavel, :prmdtadmissaofuncao, :prmdt_fim_funcao, :prmds_funcao, :prmds_cbo, :prmds_setor, :prmdt_mudanca_funcao, :prmdt_mudanca_fim_funcao, :prmds_mudanca_funcao, :prmds_mudanca_cbo, :prmds_mudanca_setor, :prmds_observacoes, :prmnm_beneficiario,"
					+ " :prm2dtadmissaofuncao, :prm2dt_fim_funcao, :prm2ds_funcao, :prm2ds_cbo, :prm2ds_setor,\n"
					+ " :prm3dtadmissaofuncao, :prm3dt_fim_funcao, :prm3ds_funcao, :prm3ds_cbo, :prm3ds_setor,\n"
					+ " :prm4dtadmissaofuncao, :prm4dt_fim_funcao, :prm4ds_funcao, :prm4ds_cbo, :prm4ds_setor,\n"
					+ " :prm5dtadmissaofuncao, :prm5dt_fim_funcao, :prm5ds_funcao, :prm5ds_cbo, :prm5ds_setor,\n"
					+ " :prm6dtadmissaofuncao, :prm6dt_fim_funcao, :prm6ds_funcao, :prm6ds_cbo, :prm6ds_setor,\n"
					+ " :prm7dtadmissaofuncao, :prm7dt_fim_funcao, :prm7ds_funcao, :prm7ds_cbo, :prm7ds_setor,\n"
					+ " :prm8dtadmissaofuncao, :prm8dt_fim_funcao, :prm8ds_funcao, :prm8ds_cbo, :prm8ds_setor,\n"
					+ " :prm9dtadmissaofuncao, :prm9dt_fim_funcao, :prm9ds_funcao, :prm9ds_cbo, :prm9ds_setor,\n"
					+ " :prm10dtadmissaofuncao, :prm10dt_fim_funcao, :prm10ds_funcao, :prm10ds_cbo, :prm10ds_setor )");

			q.setParameter("prmid_usuario_solicitou", dados.getIdUsuarioSolicitou());
			q.setParameter("prmcd_beneficiario_cartao", dados.getCdBeneficiarioCartao());
			q.setParameter("prmdt_solicitacao",
					dados.getDtSolicitacao() != null && !dados.getDtSolicitacao().toString().trim().equals("/  /")
							? dados.getDtSolicitacao().getQtDataAsTimestamp()
							: null);
			q.setParameter("prmdt_admissao",
					dados.getDtAdmissao() != null && !dados.getDtAdmissao().toString().trim().equals("/  /")
							? dados.getDtAdmissao().getQtDataAsTimestamp()
							: null);
			q.setParameter("prmdt_demissao",
					dados.getDtDemissao() != null && !dados.getDtDemissao().toString().trim().equals("/  /")
							? dados.getDtDemissao().getQtDataAsTimestamp()
							: null);
			q.setParameter("prmdt_nascimento",
					dados.getDtNascimento() != null && !dados.getDtNascimento().toString().trim().equals("/  /")
							? dados.getDtNascimento().getQtDataAsTimestamp()
							: null);
			q.setParameter("prmnr_nit_pis", dados.getNrNitPis());
			q.setParameter("prmnr_ctps", dados.getNrCtps());
			q.setParameter("prmnr_serie", dados.getNrSerie());
			q.setParameter("prmuf_ctps", dados.getUfCtps());
			q.setParameter("prmsn_acidente_trabalho",
					dados.getSnAcidentetrabalho() != null ? (dados.getSnAcidentetrabalho().equals("S") ? "S" : "N")
							: "N");
			q.setParameter("prmdt_acidente",
					dados.getDtAcidenteTrabalho() != null
							&& !dados.getDtAcidenteTrabalho().toString().trim().equals("/  /")
									? dados.getDtAcidenteTrabalho().getQtDataAsTimestamp()
									: null);
			q.setParameter("prmnr_cat", dados.getNrCat());
			q.setParameter("prmnm_responsavel", dados.getNmResponsavel());
			q.setParameter("prmnr_nit_pis_responsavel", dados.getNrNitPisResponsavel());

			q.setParameter("prmdtadmissaofuncao",
					dados.getDtAdmissaoFuncao() != null && !dados.getDtAdmissaoFuncao().toString().trim().equals("/  /")
							? dados.getDtAdmissaoFuncao().getQtDataAsTimestamp()
							: null);
			q.setParameter("prmdt_fim_funcao",
					dados.getDtFimFuncao() != null && !dados.getDtFimFuncao().toString().trim().equals("/  /")
							? dados.getDtFimFuncao().getQtDataAsTimestamp()
							: null);
			q.setParameter("prmds_funcao", dados.getDsFuncao());
			q.setParameter("prmds_cbo", dados.getDsCbo());
			q.setParameter("prmds_setor", dados.getDsSetor());

			q.setParameter("prm2dtadmissaofuncao",
					dados.getDtAdmissaoFucaoPPP2() != null
							&& !dados.getDtAdmissaoFucaoPPP2().toString().trim().equals("/  /")
									? dados.getDtAdmissaoFucaoPPP2().getQtDataAsTimestamp()
									: null);
			q.setParameter("prm2dt_fim_funcao",
					dados.getDtFimFincaoFuncaoPPP2() != null
							&& !dados.getDtFimFincaoFuncaoPPP2().toString().trim().equals("/  /")
									? dados.getDtFimFincaoFuncaoPPP2().getQtDataAsTimestamp()
									: null);
			q.setParameter("prm2ds_funcao", dados.getDsFuncaoFuncaoPPP2());
			q.setParameter("prm2ds_cbo", dados.getDsCBOFuncaoPPP2());
			q.setParameter("prm2ds_setor", dados.getDsSetorFuncaoPPP2());

			q.setParameter("prm3dtadmissaofuncao",
					dados.getDtAdmissaoFucaoPPP3() != null
							&& !dados.getDtAdmissaoFucaoPPP3().toString().trim().equals("/  /")
									? dados.getDtAdmissaoFucaoPPP3().getQtDataAsTimestamp()
									: null);
			q.setParameter("prm3dt_fim_funcao",
					dados.getDtFimFincaoFuncaoPPP3() != null
							&& !dados.getDtFimFincaoFuncaoPPP3().toString().trim().equals("/  /")
									? dados.getDtFimFincaoFuncaoPPP3().getQtDataAsTimestamp()
									: null);
			q.setParameter("prm3ds_funcao", dados.getDsFuncaoFuncaoPPP3());
			q.setParameter("prm3ds_cbo", dados.getDsCBOFuncaoPPP3());
			q.setParameter("prm3ds_setor", dados.getDsSetorFuncaoPPP3());

			q.setParameter("prm4dtadmissaofuncao",
					dados.getDtAdmissaoFucaoPPP4() != null
							&& !dados.getDtAdmissaoFucaoPPP4().toString().trim().equals("/  /")
									? dados.getDtAdmissaoFucaoPPP4().getQtDataAsTimestamp()
									: null);
			q.setParameter("prm4dt_fim_funcao",
					dados.getDtFimFincaoFuncaoPPP4() != null
							&& !dados.getDtFimFincaoFuncaoPPP4().toString().trim().equals("/  /")
									? dados.getDtFimFincaoFuncaoPPP4().getQtDataAsTimestamp()
									: null);
			q.setParameter("prm4ds_funcao", dados.getDsFuncaoFuncaoPPP4());
			q.setParameter("prm4ds_cbo", dados.getDsCBOFuncaoPPP4());
			q.setParameter("prm4ds_setor", dados.getDsSetorFuncaoPPP4());

			q.setParameter("prm5dtadmissaofuncao",
					dados.getDtAdmissaoFucaoPPP5() != null
							&& !dados.getDtAdmissaoFucaoPPP5().toString().trim().equals("/  /")
									? dados.getDtAdmissaoFucaoPPP5().getQtDataAsTimestamp()
									: null);
			q.setParameter("prm5dt_fim_funcao",
					dados.getDtFimFincaoFuncaoPPP5() != null
							&& !dados.getDtFimFincaoFuncaoPPP5().toString().trim().equals("/  /")
									? dados.getDtFimFincaoFuncaoPPP5().getQtDataAsTimestamp()
									: null);
			q.setParameter("prm5ds_funcao", dados.getDsFuncaoFuncaoPPP5());
			q.setParameter("prm5ds_cbo", dados.getDsCBOFuncaoPPP5());
			q.setParameter("prm5ds_setor", dados.getDsSetorFuncaoPPP5());

			q.setParameter("prm6dtadmissaofuncao",
					dados.getDtAdmissaoFucaoPPP6() != null
							&& !dados.getDtAdmissaoFucaoPPP6().toString().trim().equals("/  /")
									? dados.getDtAdmissaoFucaoPPP6().getQtDataAsTimestamp()
									: null);
			q.setParameter("prm6dt_fim_funcao",
					dados.getDtFimFincaoFuncaoPPP6() != null
							&& !dados.getDtFimFincaoFuncaoPPP6().toString().trim().equals("/  /")
									? dados.getDtFimFincaoFuncaoPPP6().getQtDataAsTimestamp()
									: null);
			q.setParameter("prm6ds_funcao", dados.getDsFuncaoFuncaoPPP6());
			q.setParameter("prm6ds_cbo", dados.getDsCBOFuncaoPPP6());
			q.setParameter("prm6ds_setor", dados.getDsSetorFuncaoPPP6());

			q.setParameter("prm7dtadmissaofuncao",
					dados.getDtAdmissaoFucaoPPP7() != null
							&& !dados.getDtAdmissaoFucaoPPP7().toString().trim().equals("/  /")
									? dados.getDtAdmissaoFucaoPPP7().getQtDataAsTimestamp()
									: null);
			q.setParameter("prm7dt_fim_funcao",
					dados.getDtFimFincaoFuncaoPPP7() != null
							&& !dados.getDtFimFincaoFuncaoPPP7().toString().trim().equals("/  /")
									? dados.getDtFimFincaoFuncaoPPP7().getQtDataAsTimestamp()
									: null);
			q.setParameter("prm7ds_funcao", dados.getDsFuncaoFuncaoPPP7());
			q.setParameter("prm7ds_cbo", dados.getDsCBOFuncaoPPP7());
			q.setParameter("prm7ds_setor", dados.getDsSetorFuncaoPPP7());

			q.setParameter("prm8dtadmissaofuncao",
					dados.getDtAdmissaoFucaoPPP8() != null
							&& !dados.getDtAdmissaoFucaoPPP8().toString().trim().equals("/  /")
									? dados.getDtAdmissaoFucaoPPP8().getQtDataAsTimestamp()
									: null);
			q.setParameter("prm8dt_fim_funcao",
					dados.getDtFimFincaoFuncaoPPP8() != null
							&& !dados.getDtFimFincaoFuncaoPPP8().toString().trim().equals("/  /")
									? dados.getDtFimFincaoFuncaoPPP8().getQtDataAsTimestamp()
									: null);
			q.setParameter("prm8ds_funcao", dados.getDsFuncaoFuncaoPPP8());
			q.setParameter("prm8ds_cbo", dados.getDsCBOFuncaoPPP8());
			q.setParameter("prm8ds_setor", dados.getDsSetorFuncaoPPP8());

			q.setParameter("prm9dtadmissaofuncao",
					dados.getDtAdmissaoFucaoPPP9() != null
							&& !dados.getDtAdmissaoFucaoPPP9().toString().trim().equals("/  /")
									? dados.getDtAdmissaoFucaoPPP9().getQtDataAsTimestamp()
									: null);
			q.setParameter("prm9dt_fim_funcao",
					dados.getDtFimFincaoFuncaoPPP9() != null
							&& !dados.getDtFimFincaoFuncaoPPP5().toString().trim().equals("/  /")
									? dados.getDtFimFincaoFuncaoPPP9().getQtDataAsTimestamp()
									: null);
			q.setParameter("prm9ds_funcao", dados.getDsFuncaoFuncaoPPP9());
			q.setParameter("prm9ds_cbo", dados.getDsCBOFuncaoPPP9());
			q.setParameter("prm9ds_setor", dados.getDsSetorFuncaoPPP9());

			q.setParameter("prm10dtadmissaofuncao",
					dados.getDtAdmissaoFucaoPPP10() != null
							&& !dados.getDtAdmissaoFucaoPPP10().toString().trim().equals("/  /")
									? dados.getDtAdmissaoFucaoPPP10().getQtDataAsTimestamp()
									: null);
			q.setParameter("prm10dt_fim_funcao",
					dados.getDtFimFincaoFuncaoPPP10() != null
							&& !dados.getDtFimFincaoFuncaoPPP10().toString().trim().equals("/  /")
									? dados.getDtFimFincaoFuncaoPPP10().getQtDataAsTimestamp()
									: null);
			q.setParameter("prm10ds_funcao", dados.getDsFuncaoFuncaoPPP10());
			q.setParameter("prm10ds_cbo", dados.getDsCBOFuncaoPPP10());
			q.setParameter("prm10ds_setor", dados.getDsSetorFuncaoPPP10());

			q.setParameter("prmdt_mudanca_funcao",
					dados.getDtMudancaFuncao() != null && !dados.getDtMudancaFuncao().toString().trim().equals("/  /")
							? dados.getDtMudancaFuncao().getQtDataAsTimestamp()
							: null);
			q.setParameter("prmdt_mudanca_fim_funcao",
					dados.getDtMudancaFimFuncao() != null
							&& !dados.getDtMudancaFimFuncao().toString().trim().equals("/  /")
									? dados.getDtMudancaFimFuncao().getQtDataAsTimestamp()
									: null);
			q.setParameter("prmds_mudanca_funcao", dados.getDsMudancaFuncao());
			q.setParameter("prmds_mudanca_cbo", dados.getDsMudancaCbo());
			q.setParameter("prmds_mudanca_setor", dados.getDsMudancaSetor());
			q.setParameter("prmds_observacoes", dados.getDsObservacoes());
			q.setParameter("prmnm_beneficiario", dados.getNmBeneficiario());


			q.executeUpdate();
			listagemPPPPendente.clear();
			listagemPPPPendente = carregaListagemPPP(cnx);

		} catch (Exception e) {
			throw new Exception("Erro PPP: " + e.getMessage());
		}

	}

	public void selecionaBeneficiario(Beneficiario benef) {
		beneficiarioSelecionado = new DadosBeneficiarioSelecionado();
		beneficiarioSelecionado.setBeneficiario(benef);
		beneficiarioSelecionado.getBeneficiario().setDtExclusao(null);
		try {
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
			try {
				beneficiarioSelecionado.setBeneficiarioCbo(BeneficiarioCboLocalizador.buscaBeneficiarioCbo(cnx,
						beneficiarioSelecionado.getBeneficiario().getCdBeneficiarioCartao()));

				if (beneficiarioSelecionado.getBeneficiarioCbo() == null) {
					beneficiarioSelecionado.setBeneficiarioCbo(new BeneficiarioCbo());
				}

				FiltroDeNavegador filtro = new FiltroDeNavegador();
				filtro.adicionaCondicao("cd_beneficiario_cartao", FiltroDeNavegador.IGUAL,
						beneficiarioSelecionado.getBeneficiario().getCdBeneficiarioCartao());
				filtro.setOrdemDeNavegacao(RiscoBeneficiarioCaminhador.POR_ORDEM_DEFAULT);

				RiscoBeneficiarioCaminhador riscos = new RiscoBeneficiarioCaminhador(filtro);
				riscos.ativaCaminhador();
				beneficiarioSelecionado.getRiscos().clear();
				selecionaEstado(beneficiarioSelecionado.getBeneficiario().getCdEstadoResidencia());

				dsCbo = null;
				dsFuncao = null;
				dsFuncaoAnterior = null;
				dsCargo = null;
				dsSetor = null;

				if (beneficiarioSelecionado.getBeneficiarioCbo() != null) {
					if (beneficiarioSelecionado.getBeneficiarioCbo().getCdCbo() != null) {
						dsCbo = CboLocalizador.buscaCbo(beneficiarioSelecionado.getBeneficiarioCbo().getCdCbo())
								.getNmCbo();
					}
					if (beneficiarioSelecionado.getBeneficiarioCbo().getIdFuncao() != null) {
						dsFuncao = FuncaoLocalizador
								.buscaFuncao(beneficiarioSelecionado.getBeneficiarioCbo().getIdFuncao()).getDsFuncao();
					}
					if (beneficiarioSelecionado.getBeneficiarioCbo().getIdFuncaoAnterior() != null) {
						dsFuncaoAnterior = FuncaoLocalizador
								.buscaFuncao(beneficiarioSelecionado.getBeneficiarioCbo().getIdFuncaoAnterior())
								.getDsFuncao();
					}
					if (beneficiarioSelecionado.getBeneficiarioCbo().getIdCargo() != null) {
						dsCargo = CargoLocalizador.buscaCargo(beneficiarioSelecionado.getBeneficiarioCbo().getIdCargo())
								.getDsCargo();
					}
					if (beneficiarioSelecionado.getBeneficiarioCbo().getIdSetor() != null) {
						dsSetor = SetorLocalizador.buscaSetor(beneficiarioSelecionado.getBeneficiarioCbo().getIdSetor())
								.getDsSetor();
					}
				}

				if (!riscos.isEmpty()) {
					while (!riscos.isEof()) {
						beneficiarioSelecionado.getRiscos().add(riscos.getRiscoBeneficiario());
						riscos.proximo();
					}
				}

				FiltroDeNavegador filtroExames = new FiltroDeNavegador();
				filtroExames.adicionaCondicao("cd_beneficiario_cartao", FiltroDeNavegador.IGUAL,
						beneficiarioSelecionado.getBeneficiario().getCdBeneficiarioCartao());
				filtroExames.setOrdemDeNavegacao(ExameBeneficiarioCaminhador.POR_ORDEM_DEFAULT);

				ExameBeneficiarioCaminhador exames = new ExameBeneficiarioCaminhador(filtro);
				exames.ativaCaminhador();
				beneficiarioSelecionado.getExames().clear();

				if (!exames.isEmpty()) {
					while (!exames.isEof()) {
						beneficiarioSelecionado.getExames()
								.add(ExameBeneficiarioLocalizador.buscaExameBeneficiario(
										exames.getExameBeneficiario().getCdBeneficiarioCartao(),
										exames.getExameBeneficiario().getCdProcedimento()));
						exames.proximo();
					}
				}

			} finally {
				cnx.libera();
			}
		} catch (Exception e) {
			msgExibida = 0;
			msgAoUsuario = "Ops! " + e.getMessage();
		}

		Util.sendRedirect("resources/page/mntBeneficiario.jsf");
	}

	public void atualizaEmpresa() {
		if (beneficiarioSelecionado != null && beneficiarioSelecionado.getBeneficiario() != null
				&& beneficiarioSelecionado.getBeneficiario().getCdEmpresa() != null
				&& !beneficiarioSelecionado.getBeneficiario().getCdEmpresa().trim().equals("")
				&& !beneficiarioSelecionado.getBeneficiario().getCdEmpresa().trim().contains("_")) {

			try {
				ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
				try {

					Empresa empresa = EmpresaLocalizador.buscaEmpresa(cnx,
							ParametrosSistema.getInstance().getCodigoDoCliente(),
							new Integer(beneficiarioSelecionado.getBeneficiario().getCdEmpresa()));
					if (empresa != null) {
						beneficiarioSelecionado.getBeneficiario().setIdEmpresa(empresa.getIdEmpresa());
						beneficiarioSelecionado.getBeneficiario().setCdEmpresa(empresa.getCdEmpresa());
					} else {
						beneficiarioSelecionado.getBeneficiario().setIdEmpresa(null);
						beneficiarioSelecionado.getBeneficiario().setCdEmpresa(null);
					}

				} finally {
					cnx.libera();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			if (beneficiarioSelecionado.getBeneficiario().getCdEmpresa() != null
					&& !beneficiarioSelecionado.getBeneficiario().getCdEmpresa().equals("")
					&& beneficiarioSelecionado.getBeneficiario().getIdEmpresa() != null
					&& beneficiarioSelecionado.getBeneficiario().getCdFamilia() != null
					&& !beneficiarioSelecionado.getBeneficiario().getCdFamilia().equals("")
					&& beneficiarioSelecionado.getBeneficiario().getCdDependencia() != null
					&& !beneficiarioSelecionado.getBeneficiario().getCdDependencia().equals("")) {

				Util.sendRedirect("resources/page/mntBeneficiarioReativacao.jsf");

			}
		}
	}

	public void atualizaFamilia() {
		if (beneficiarioSelecionado != null && beneficiarioSelecionado.getBeneficiario() != null
				&& beneficiarioSelecionado.getBeneficiario().getCdFamilia() != null
				&& !beneficiarioSelecionado.getBeneficiario().getCdFamilia().trim().equals("")
				&& !beneficiarioSelecionado.getBeneficiario().getCdFamilia().trim().contains("_")) {

			try {
				ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
				try {

					if (beneficiarioSelecionado.getBeneficiario().getCdEmpresa() != null
							&& !beneficiarioSelecionado.getBeneficiario().getCdEmpresa().equals("")
							&& beneficiarioSelecionado.getBeneficiario().getIdEmpresa() != null
							&& beneficiarioSelecionado.getBeneficiario().getCdDependencia() != null
							&& !beneficiarioSelecionado.getBeneficiario().getCdDependencia().equals("")) {

						Beneficiario beneficiario = BeneficiarioLocalizador.buscaporCodigoOriginal(cnx,
								beneficiarioSelecionado.getBeneficiario().getCdUnimed(),
								beneficiarioSelecionado.getBeneficiario().getCdEmpresa(),
								beneficiarioSelecionado.getBeneficiario().getCdFamilia(),
								beneficiarioSelecionado.getBeneficiario().getCdDependencia());

						if (beneficiario != null) {
							beneficiarioSelecionado.getBeneficiario().setCdFamilia(null);
							beneficiarioSelecionado.getBeneficiario().setCdEmpresa(null);
							beneficiarioSelecionado.getBeneficiario().setIdEmpresa(null);
							beneficiarioSelecionado.getBeneficiario().setCdDependencia(null);
						}
					}

				} finally {
					cnx.libera();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			if (beneficiarioSelecionado.getBeneficiario().getCdEmpresa() != null
					&& !beneficiarioSelecionado.getBeneficiario().getCdEmpresa().equals("")
					&& beneficiarioSelecionado.getBeneficiario().getIdEmpresa() != null
					&& beneficiarioSelecionado.getBeneficiario().getCdFamilia() != null
					&& !beneficiarioSelecionado.getBeneficiario().getCdFamilia().equals("")
					&& beneficiarioSelecionado.getBeneficiario().getCdDependencia() != null
					&& !beneficiarioSelecionado.getBeneficiario().getCdDependencia().equals("")) {

				Util.sendRedirect("resources/page/mntBeneficiarioReativacao.jsf");

			}
		}
	}

	public void atualizaDependencia() {
		if (beneficiarioSelecionado != null && beneficiarioSelecionado.getBeneficiario() != null
				&& beneficiarioSelecionado.getBeneficiario().getCdFamilia() != null
				&& !beneficiarioSelecionado.getBeneficiario().getCdFamilia().trim().equals("")
				&& !beneficiarioSelecionado.getBeneficiario().getCdFamilia().trim().contains("_")) {

			try {
				ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
				try {

					Beneficiario beneficiario = BeneficiarioLocalizador.buscaporCodigoOriginal(cnx,
							beneficiarioSelecionado.getBeneficiario().getCdUnimed(),
							beneficiarioSelecionado.getBeneficiario().getCdEmpresa(),
							beneficiarioSelecionado.getBeneficiario().getCdFamilia(),
							beneficiarioSelecionado.getBeneficiario().getCdDependencia());

					if (beneficiario != null) {
						beneficiarioSelecionado.getBeneficiario().setCdFamilia(null);
						beneficiarioSelecionado.getBeneficiario().setCdEmpresa(null);
						beneficiarioSelecionado.getBeneficiario().setIdEmpresa(null);
						beneficiarioSelecionado.getBeneficiario().setCdDependencia(null);
						throw new Exception("Benefici�rio j� cadastrado.");
					}

				} finally {
					cnx.libera();
				}
			} catch (Exception e) {
				e.printStackTrace();
				msgExibida = 0;
				msgAoUsuario = "Ops! " + e.getMessage();
				Util.sendRedirect("resources/page/mntBeneficiarioReativacao.jsf");
			}
			if (beneficiarioSelecionado.getBeneficiario().getCdEmpresa() != null
					&& !beneficiarioSelecionado.getBeneficiario().getCdEmpresa().equals("")
					&& beneficiarioSelecionado.getBeneficiario().getIdEmpresa() != null
					&& beneficiarioSelecionado.getBeneficiario().getCdFamilia() != null
					&& !beneficiarioSelecionado.getBeneficiario().getCdFamilia().equals("")
					&& beneficiarioSelecionado.getBeneficiario().getCdDependencia() != null
					&& !beneficiarioSelecionado.getBeneficiario().getCdDependencia().equals("")) {

				Util.sendRedirect("resources/page/mntBeneficiarioReativacao.jsf");

			}
		}
	}

	public void selecionaBeneficiarioReativacao(Beneficiario benef) {
		beneficiarioSelecionado = new DadosBeneficiarioSelecionado();
		// benef.setDtExclusao(null);
		//
		benef.setCdFamilia(null);
		benef.setCdDependencia(null);
		//
		// benef.setDtInclusao(new QtData());
		// benef.setDtInclusaoCobranca(new QtData());
		// benef.setDtInclusaoPlano(new QtData());
		// benef.setDtInclusaoPlanoAtendimento(new QtData());

		beneficiarioSelecionado.setBeneficiario(benef);

		try {
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
			try {
				beneficiarioSelecionado.setBeneficiarioCbo(BeneficiarioCboLocalizador.buscaBeneficiarioCbo(cnx,
						beneficiarioSelecionado.getBeneficiario().getCdBeneficiarioCartao()));

				if (beneficiarioSelecionado.getBeneficiarioCbo() == null) {
					beneficiarioSelecionado.setBeneficiarioCbo(new BeneficiarioCbo());
				}

				FiltroDeNavegador filtro = new FiltroDeNavegador();
				filtro.adicionaCondicao("cd_beneficiario_cartao", FiltroDeNavegador.IGUAL,
						beneficiarioSelecionado.getBeneficiario().getCdBeneficiarioCartao());
				filtro.setOrdemDeNavegacao(RiscoBeneficiarioCaminhador.POR_ORDEM_DEFAULT);

				RiscoBeneficiarioCaminhador riscos = new RiscoBeneficiarioCaminhador(filtro);
				riscos.ativaCaminhador();
				beneficiarioSelecionado.getRiscos().clear();
				selecionaEstado(beneficiarioSelecionado.getBeneficiario().getCdEstadoResidencia());

				dsCbo = null;
				dsFuncao = null;
				dsFuncaoAnterior = null;
				dsCargo = null;
				dsSetor = null;

				if (beneficiarioSelecionado.getBeneficiarioCbo() != null) {
					if (beneficiarioSelecionado.getBeneficiarioCbo().getCdCbo() != null) {
						dsCbo = CboLocalizador.buscaCbo(beneficiarioSelecionado.getBeneficiarioCbo().getCdCbo())
								.getNmCbo();
					}
					if (beneficiarioSelecionado.getBeneficiarioCbo().getIdFuncao() != null) {
						dsFuncao = FuncaoLocalizador
								.buscaFuncao(beneficiarioSelecionado.getBeneficiarioCbo().getIdFuncao()).getDsFuncao();
					}
					if (beneficiarioSelecionado.getBeneficiarioCbo().getIdFuncaoAnterior() != null) {
						dsFuncaoAnterior = FuncaoLocalizador
								.buscaFuncao(beneficiarioSelecionado.getBeneficiarioCbo().getIdFuncaoAnterior())
								.getDsFuncao();
					}
					if (beneficiarioSelecionado.getBeneficiarioCbo().getIdCargo() != null) {
						dsCargo = CargoLocalizador.buscaCargo(beneficiarioSelecionado.getBeneficiarioCbo().getIdCargo())
								.getDsCargo();
					}
					if (beneficiarioSelecionado.getBeneficiarioCbo().getIdSetor() != null) {
						dsSetor = SetorLocalizador.buscaSetor(beneficiarioSelecionado.getBeneficiarioCbo().getIdSetor())
								.getDsSetor();
					}
				}

				if (!riscos.isEmpty()) {
					while (!riscos.isEof()) {
						beneficiarioSelecionado.getRiscos().add(riscos.getRiscoBeneficiario());
						riscos.proximo();
					}
				}

				FiltroDeNavegador filtroExames = new FiltroDeNavegador();
				filtroExames.adicionaCondicao("cd_beneficiario_cartao", FiltroDeNavegador.IGUAL,
						beneficiarioSelecionado.getBeneficiario().getCdBeneficiarioCartao());
				filtroExames.setOrdemDeNavegacao(ExameBeneficiarioCaminhador.POR_ORDEM_DEFAULT);

				ExameBeneficiarioCaminhador exames = new ExameBeneficiarioCaminhador(filtro);
				exames.ativaCaminhador();
				beneficiarioSelecionado.getExames().clear();

				if (!exames.isEmpty()) {
					while (!exames.isEof()) {
						beneficiarioSelecionado.getExames()
								.add(ExameBeneficiarioLocalizador.buscaExameBeneficiario(
										exames.getExameBeneficiario().getCdBeneficiarioCartao(),
										exames.getExameBeneficiario().getCdProcedimento()));
						exames.proximo();
					}
				}

			} finally {
				cnx.libera();
			}
		} catch (Exception e) {
			msgExibida = 0;
			msgAoUsuario = "Ops! " + e.getMessage();
		}

		Util.sendRedirect("resources/page/mntBeneficiarioReativacao.jsf");
	}

	private List<PreCadastroCbo> carregaListagem(Conexao cnx, String cdCartao, String dsNome) {
		List<PreCadastroCbo> lista = new LinkedList<>();
		listagemPreCadastro.clear();
		FiltroDeNavegador filtro = new FiltroDeNavegador();

		if (idEmpresaRelacioanda != null) {
			filtro.adicionaCondicao("dt_exclusao", FiltroDeNavegador.IGUAL, null);
			filtro.adicionaCondicao("cd_empresa", FiltroDeNavegador.IGUAL,
					jUni.biblioteca.util.Util.strZero(idEmpresaRelacioanda, 4));

			if (cdCartao != null && !cdCartao.equals("")) {
				filtro.adicionaCondicao("cd_beneficiario_cartao", FiltroDeNavegador.ILIKE, "%" + cdCartao + "%");
			} else {
				filtro.adicionaCondicao("cd_beneficiario_cartao", FiltroDeNavegador.IGUAL, null);
			}

		} else {
			filtro.adicionaCondicao("cd_empresa", FiltroDeNavegador.MAIOR_OU_IGUAL, "8000");
			filtro.adicionaCondicao("cd_empresa", FiltroDeNavegador.MENOR_OU_IGUAL, "9000");

			if (cdCartao != null && !cdCartao.equals("")) {
				filtro.adicionaCondicao("cd_beneficiario_cartao", FiltroDeNavegador.ILIKE, "%" + cdCartao + "%");
			}
		}

		if (dsNome != null && !dsNome.equals("")) {
			filtro.adicionaCondicao("ds_nome", FiltroDeNavegador.ILIKE, "%" + dsNome.replaceAll(" ", "%") + "%");
		}

		try {
			PreCadastroCboCaminhador pre = new PreCadastroCboCaminhador(cnx, filtro);
			pre.setOrdemDeNavegacao(PreCadastroCboCaminhador.POR_ORDEM_DEFAULT);
			pre.ativaCaminhador();

			if (!pre.isEmpty()) {
				while (!pre.isEof()) {
					lista.add(pre.getPreCadastroCbo());
					pre.proximo();
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			msgExibida = 0;
			msgAoUsuario = "Ops! " + e.getMessage();
		}

		return lista;
	}

	private List<Cargo> carregaListaCargo(Conexao cnx) throws QtSQLException {
		List<Cargo> lista = new LinkedList<>();
		FiltroDeNavegador filtro = new FiltroDeNavegador();

		CargoCaminhador caminhador = new CargoCaminhador(cnx, filtro);
		caminhador.setOrdemDeNavegacao(CargoCaminhador.POR_DS_CARGO);

		caminhador.ativaCaminhador();

		if (!caminhador.isEmpty()) {
			while (!caminhador.isEof()) {
				lista.add(caminhador.getCargo());
				caminhador.proximo();
			}
		}

		return lista;
	}

	private List<Funcao> carregaListaFuncao(ConexaoParaPoolDeConexoes cnx) throws QtSQLException {
		List<Funcao> lista = new LinkedList<>();
		FiltroDeNavegador filtro = new FiltroDeNavegador();

		FuncaoCaminhador caminhador = new FuncaoCaminhador(cnx, filtro);
		caminhador.setOrdemDeNavegacao(FuncaoCaminhador.POR_DS_FUNCAO);

		caminhador.ativaCaminhador();

		if (!caminhador.isEmpty()) {
			while (!caminhador.isEof()) {
				lista.add(caminhador.getFuncao());
				caminhador.proximo();
			}
		}

		return lista;
	}

	private List<Setor> carregaListaSetor(ConexaoParaPoolDeConexoes cnx) throws QtSQLException {
		List<Setor> lista = new LinkedList<>();
		FiltroDeNavegador filtro = new FiltroDeNavegador();

		SetorCaminhador caminhador = new SetorCaminhador(cnx, filtro);
		caminhador.setOrdemDeNavegacao(SetorCaminhador.POR_DS_SETOR);
		caminhador.ativaCaminhador();

		if (!caminhador.isEmpty()) {
			while (!caminhador.isEof()) {
				lista.add(caminhador.getSetor());
				caminhador.proximo();
			}
		}

		return lista;
	}

	private List<Cbo> carregaListaCbo(ConexaoParaPoolDeConexoes cnx) throws QtSQLException {

		List<Cbo> lista = new LinkedList<>();
		FiltroDeNavegador filtro = new FiltroDeNavegador();

		CboCaminhador caminhador = new CboCaminhador(cnx, filtro);
		caminhador.setOrdemDeNavegacao(CboCaminhador.POR_CODIGO);

		caminhador.ativaCaminhador();

		if (!caminhador.isEmpty()) {
			while (!caminhador.isEof()) {
				lista.add(caminhador.getCbo());
				caminhador.proximo();
			}
		}

		return lista;
	}

	public void selecionaEmpresa() {
		if (preCadastroCboSelecionado.getCdEmpresa() != null) {
			try {
				ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
				try {

					Empresa empresa = EmpresaLocalizador.buscaPorCodigosDeEmpresa(cnx,
							new Integer(preCadastroCboSelecionado.getCdEmpresa()));

					if (empresa != null) {
						nmEmpresaSelecionada = empresa.getDsRazaoSocial();
					}

				} finally {
					cnx.libera();
				}
			} catch (Exception e) {
				e.printStackTrace();
				msgExibida = 0;
				msgAoUsuario = "Ops! " + e.getMessage();
			}
		}
	}

	public void selecionouCidade() {
		try {
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
			try {

				Cidade cidade = CidadeLocalizador.buscaPorCidadesPorIBGE(cnx, preCadastroCboSelecionado.getIdCidade());
				if (cidade != null) {
					preCadastroCboSelecionado.setCdEstado(cidade.getCdEstado());
				}

			} finally {
				cnx.libera();
			}
		} catch (Exception e) {
			e.printStackTrace();
			msgExibida = 0;
			msgAoUsuario = "Ops! " + e.getMessage();
		}
	}

	public void limpaMensagem() {
		msgExibida = 0;
		msgAoUsuario = null;
	}

	private List<Cidade> carregaListaCidades(String cdEstado) {
		List<Cidade> lista = new LinkedList<>();

		if (cdEstado != null) {
			try {
				ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
				try {
					FiltroDeNavegador filtro = new FiltroDeNavegador();
					filtro.adicionaCondicao("cd_estado", FiltroDeNavegador.IGUAL, cdEstado);

					CidadeCaminhador cidades = new CidadeCaminhador(cnx, filtro);
					cidades.setOrdemDeNavegacao(CidadeCaminhador.POR_NOME);

					cidades.ativaCaminhador();

					if (!cidades.isEmpty()) {
						while (!cidades.isEof()) {
							lista.add(cidades.getCidade());
							cidades.proximo();
						}
					}
				} finally {
					cnx.libera();
				}

			} catch (Exception e) {
				e.printStackTrace();
				msgExibida = 0;
				msgAoUsuario = "Ops! " + e.getMessage();
			}
		}
		return lista;
	}

	public void inic() {
		if (msgAoUsuario != null) {
			if (msgExibida > 0) {
				msgAoUsuario = null;
				msgExibida = 0;
			}
			msgExibida++;
		}
	}

	public String ajustaNrCpf(String cpf) {
		if (cpf != null && !cpf.trim().equals("")) {
			cpf = cpf.replaceAll("-", "");
			cpf = cpf.replaceAll("\\.", "");
			return cpf;
		}
		return null;
	}

	public void adicionaPreCadastro() {
		try {
			if (usuarioLogado == null) {
				usuarioLogado = jUni.controleLogin.Util.getDadosUsuarioLogado();
			}
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
			try {

				if (preCadastroCboSelecionado.getIdLotacao() != null) {
					if (preCadastroCboSelecionado.getIdLotacao().equals("0")) {
						throw new Exception("Ops! A Lota��o n�o pode ser 0(zero).");
					}
				} else {
					throw new Exception("Ops! A Lota��o n�o pode ser 0(zero).");
				}

				if (preCadastroCboSelecionado.getIdPreCadastro() == null || !PreCadastroCboLocalizador.existe(cnx, preCadastroCboSelecionado.getIdPreCadastro())) {
					if (preCadastroCboSelecionado.getNrCpf() != null) {
						preCadastroCboSelecionado.setNrCpf(
								preCadastroCboSelecionado.getNrCpf().replaceAll("\\.", "").replaceAll("-", ""));
					}

					preCadastroCboSelecionado.setTpSolicitacao("I");
					if (idEmpresaRelacioanda != null) {
						preCadastroCboSelecionado.setCdEmpresa(idEmpresaRelacioanda + "");
					} else {
						preCadastroCboSelecionado.setCdEmpresa("-1");
					}
					if (preCadastroCboSelecionado.getNrCpf() != null
							&& !preCadastroCboSelecionado.getNrCpf().trim().equals("")) {
						preCadastroCboSelecionado.setNrCpf(ajustaNrCpf(preCadastroCboSelecionado.getNrCpf()));
					}
					PreCadastroCboNegocio pre = new PreCadastroCboNegocio(cnx, preCadastroCboSelecionado);
					pre.insere();
					msgExibida = 0;
					msgAoUsuario = "Pr� Cadastro Inserido.";
					mntPreCadastro = "";
					nmEmpresaSelecionada = "";

					DadosSolicitacaoPCMSO dados = new DadosSolicitacaoPCMSO();

					
					FiliaisEmpresa filial = FiliaisEmpresaLocalizador.buscaFiliaisEmpresa(cnx, preCadastroCboSelecionado.getCdEmpresa(), preCadastroCboSelecionado.getIdFilial());
					if (filial != null) {
						dados.setIdFilial(filial.getIdFilial());
						dados.setDsFilial(filial.getDsFilial());
					}
					dados.setIdUsuarioSolicitou(usuarioLogado.getIdUsuario());
					dados.setCdUnimed(preCadastroCboSelecionado.getCdUnimed());
					dados.setCdEmpresa(preCadastroCboSelecionado.getCdEmpresa());
					dados.setCdFamilia(getCdFamilia(preCadastroCboSelecionado.getCdBeneficiarioCartao()));
					dados.setCdDependencia(getCdDependencia(preCadastroCboSelecionado.getCdBeneficiarioCartao()));
					dados.setCdDigito(getCdDigito(preCadastroCboSelecionado.getCdBeneficiarioCartao()));
					dados.setNmBeneficiario(preCadastroCboSelecionado.getDsNome());
					dados.setDtSolicitacao(new QtData());
					dados.setTpSolicitacao("I");
					dados.setDtConfirmacao(null);
					dados.setIdUsuarioConfirmacao(null);
					dados.setDsObservacoes(getObservacoesInclusao(cnx, preCadastroCboSelecionado));
					SolicitacaoPCMSO solicitacao = new SolicitacaoPCMSO(dados);
					solicitacao.incluiSolicitacao(cnx);
					
					enviaEmail( cnx, preCadastroCboSelecionado, "I");

				} else {

					preCadastroCboSelecionado.setTpSolicitacao("A");

					if (preCadastroCboSelecionado.getNrCpf() != null
							&& !preCadastroCboSelecionado.getNrCpf().trim().equals("")) {
						preCadastroCboSelecionado.setNrCpf(ajustaNrCpf(preCadastroCboSelecionado.getNrCpf()));
					}

					DadosSolicitacaoPCMSO dados = new DadosSolicitacaoPCMSO();
					FiliaisEmpresa filial = FiliaisEmpresaLocalizador.buscaFiliaisEmpresa(cnx,
							preCadastroCboSelecionado.getCdEmpresa(), preCadastroCboSelecionado.getIdFilial());
					if (filial != null) {
						dados.setIdFilial(filial.getIdFilial());
						dados.setDsFilial(filial.getDsFilial());
					}

					dados.setIdUsuarioSolicitou(usuarioLogado.getIdUsuario());
					dados.setCdUnimed(preCadastroCboSelecionado.getCdUnimed());
					dados.setCdFamilia(getCdFamilia(preCadastroCboSelecionado.getCdBeneficiarioCartao()));
					dados.setCdDependencia(getCdDependencia(preCadastroCboSelecionado.getCdBeneficiarioCartao()));
					dados.setCdDigito(getCdDigito(preCadastroCboSelecionado.getCdBeneficiarioCartao()));
					dados.setNmBeneficiario(preCadastroCboSelecionado.getDsNome());
					dados.setDtSolicitacao(new QtData());
					dados.setTpSolicitacao("A");
					dados.setDtConfirmacao(null);
					dados.setIdUsuarioConfirmacao(null);
					dados.setDsObservacoes(getObservacoes(cnx, preCadastroCboSelecionado));

					SolicitacaoPCMSO solicitacao = new SolicitacaoPCMSO(dados);
					solicitacao.incluiSolicitacao(cnx);

					PreCadastroCboNegocio pre = new PreCadastroCboNegocio(cnx, preCadastroCboSelecionado);
					pre.altera();
					
					enviaEmail( cnx, preCadastroCboSelecionado, "A");
					
					msgExibida = 0;
					msgAoUsuario = "Pr� Cadastro Alterado.";
					mntPreCadastro = "";
					nmEmpresaSelecionada = "";
				}
				
				snPossuiDeficiencia = null;
				snPossuiDeficienciaCbo = null;
				snTrabalhoEmAltura = null;
				snPossuiDeficiencia = null;
				snEspacoConfinado = null;
				
				listagemPreCadastro = carregaListagem(cnx, null, null);
				Util.sendRedirect("resources/page/dashboard.jsf");
			} finally {
				cnx.libera();
			}
		} catch (Exception e) {
			mntPreCadastro = "in";
			msgExibida = 0;
			msgAoUsuario = "Ops! " + e.getMessage();
			e.printStackTrace();
			Util.sendRedirect("resources/page/mntPreCadastro.jsf");
		}

	}

	private String getObservacoesInclusao(Conexao cnx, PreCadastroCbo pre) throws QtSQLException {

		StringBuilder mensagem = new StringBuilder();
		PreCadastroCbo preCadastroCboAnterior = PreCadastroCboLocalizador.buscaPreCadastroCbo(pre.getIdPreCadastro());

		mensagem.append(" Inclus�o do Beneficiario:");
		if (pre.getCdBeneficiarioCartao() != null) {
			mensagem.append(pre.getCdBeneficiarioCartao());
		}

		mensagem.append("</b>");

		mensagem.append("<br/>");
		mensagem.append("<b>DATA SOLICITACAO: </b>");
		mensagem.append(pre.getDtSolicitacao().dataEHora());

		jPcmso.persistencia.geral.e.Empresa e = jPcmso.persistencia.geral.e.EmpresaLocalizador
				.buscaEmpresa(pre.getCdEmpresa());
		if (e != null) {
			mensagem.append(getCampoHtml("EMPRESA", pre.getCdEmpresa() + " - " + e.getDsEmpresa()));
		}

		mensagem.append(getCampoHtml("NOME BENEFICIARIO", pre.getDsNome()));

		if (pre.getDtAdmissao() != null) {
			if (preCadastroCboAnterior.getDtAdmissao() == null
					|| (!preCadastroCboAnterior.getDtAdmissao().eIgual(pre.getDtAdmissao()))) {
				mensagem.append(getCampoHtml("DATA DE ADMISAO", pre.getDtAdmissao().toString()));
			}
		}

		if (pre.getIdFilial() != null) {
			if (compara(preCadastroCboAnterior.getIdFilial(), pre.getIdFilial())) {
				if (pre.getIdFilial() != null) {
					FiliaisEmpresa filial = FiliaisEmpresaLocalizador.buscaFiliaisEmpresa(pre.getCdEmpresa(),
							pre.getIdFilial());
					if (filial != null) {
						mensagem.append(getCampoHtml("FILIAL", pre.getIdFilial() + " - " + filial.getDsFilial()));
					}
				}
			}
		}

		if (pre.getTpSexo() != null) {
			mensagem.append(getCampoHtml("SEXO", pre.getTpSexo().equals("M") ? "Masculino" : "Feminino"));
		}

		if (pre.getDtNascimento() != null) {
			mensagem.append(getCampoHtml("DATA DE NASCIMENTO", pre.getDtNascimento().toString()));
		}

		if (pre.getNrCpf() != null) {
			mensagem.append(getCampoHtml("CPF", pre.getNrCpf()));
		}

		if (pre.getCdRg() != null) {
			mensagem.append(getCampoHtml("RG", pre.getCdRg()));
		}

		if (pre.getDsOrgaoEmissorRg() != null) {
			mensagem.append(getCampoHtml("ORGAO RG", pre.getDsOrgaoEmissorRg()));
		}

		if (pre.getDsNomeDaMae() != null) {
			mensagem.append(getCampoHtml("NOME DA MAE", pre.getDsNomeDaMae()));
		}

		if (pre.getDsLogradouro() != null) {
			mensagem.append(getCampoHtml("LOGRADOURO", pre.getDsLogradouro()));
		}

		if (pre.getDsNumeroLogradouro() != null) {
			mensagem.append(getCampoHtml("NUMERO", pre.getDsNumeroLogradouro()));
		}
		if (pre.getDsComplementoLogradouro() != null) {
			mensagem.append(getCampoHtml("COMPLEMENTO", pre.getDsComplementoLogradouro()));
		}

		if (pre.getDsBairro() != null) {
			mensagem.append(getCampoHtml("BAIRRO", pre.getDsBairro()));
		}

		if (pre.getIdCidade() != null) {
			Cidade c = CidadeLocalizador.buscaCidade(pre.getCdEstado(), pre.getIdCidade());
			if (c != null) {
				mensagem.append(getCampoHtml("ESTADO", pre.getCdEstado()));
				mensagem.append(getCampoHtml("CIDADE", c.getDsCidade()));
			}
		}

		if (pre.getTpEstadoCivil() != null) {
			mensagem.append("<br/>");
			mensagem.append("<b>ESTADO CIVIL:&nbsp;</b>");
			if (pre.getTpEstadoCivil().equals("S")) {
				mensagem.append("Solteiro");
			} else if (pre.getTpEstadoCivil().equals("M")) {
				mensagem.append("Casado (M-Married)");
			} else if (pre.getTpEstadoCivil().equals("W")) {
				mensagem.append("Viuvo (W-Widow)");
			} else if (pre.getTpEstadoCivil().equals("D")) {
				mensagem.append("Divorciado");
			} else if (pre.getTpEstadoCivil().equals("A")) {
				mensagem.append("Apartado (Separado)");
			} else if (pre.getTpEstadoCivil().equals("U")) {
				mensagem.append("Uniao Estavel");
			}

		}

		if (pre.getDsTelefoneContato() != null) {
			mensagem.append(getCampoHtml("TELEFONE DE CONTATO", pre.getDsTelefoneContato()));
		}

		if (pre.getDsAtividade() != null) {
			mensagem.append(getCampoHtml("ATIVIDADE", pre.getDsAtividade()));
		}

		if (pre.getNrPis() != null) {
			mensagem.append(getCampoHtml("PIS/Pasep", pre.getNrPis()));
		}
		if (pre.getNrCtps() != null) {
			mensagem.append(getCampoHtml("CTPS", pre.getNrCtps()));
		}

		if (pre.getNrSerieUf() != null) {
			mensagem.append(getCampoHtml("SERIE (UF)", pre.getNrSerieUf()));
		}

		if (pre.getCdCbo() != null) {
			mensagem.append("<br/>");
			mensagem.append("<b>CBO:&nbsp;</b>");
			Cbo cbo = CboLocalizador.buscaCbo(pre.getCdCbo());
			if (cbo != null) {
				mensagem.append(cbo.getCdCbo() + " - " + cbo.getNmCbo());
			} else {
				mensagem.append(pre.getCdCbo());
			}
		}

		if (pre.getIdFuncao() != null) {
			Funcao f = FuncaoLocalizador.buscaFuncao(pre.getIdFuncao());
			if (f != null) {
				mensagem.append(getCampoHtml("FUNCAO", f.getDsFuncao()));
			}
		}
		if (pre.getIdSetor() != null) {
			Setor s = SetorLocalizador.buscaSetor(pre.getIdSetor());
			if (s != null) {
				mensagem.append(getCampoHtml("SETOR", s.getDsSetor()));
			}
		}
		if (pre.getIdCargo() != null) {
			Cargo c = CargoLocalizador.buscaCargo(pre.getIdCargo());
			if (c != null) {
				mensagem.append(getCampoHtml("CARGO", c.getDsCargo()));
			}
		}

		if (pre.getIdLotacao() != null) {
			jPcmso.persistencia.geral.l.Lotacao lotacao = jPcmso.persistencia.geral.l.LotacaoLocalizador.buscaLotacao(
					ParametrosCliente.getCodigoCliente(), Inteiro.stoi(pre.getCdEmpresa()), pre.getIdLotacao());
			if (lotacao != null) {
				mensagem.append(getCampoHtml("LOTACAO", lotacao.getIdLotacao() + ": " + lotacao.getDsLotacao()));
			}
		}

		if (pre.getVlPeso() != null) {
			mensagem.append(getCampoHtml("PESO", pre.getVlPeso() != null ? pre.getVlPeso().toString() : ""));
		}

		if (pre.getVlPeso() != null) {
			mensagem.append(getCampoHtml("ALTURA", pre.getVlAltura() != null ? pre.getVlAltura().toString() : ""));
		}

		mensagem.append(getCampoHtml("POSSUI ALGUMA DEFICIENCIA", pre.isSnPossuiDeficiencia() ? "SIM" : "NAO"));

		if (pre.getTpDeficiencia() != null) {
			mensagem.append("<br/>");
			mensagem.append("<b>TIPO DE DEFICIENCIA:&nbsp;</b>");
			/**
			 * Tipo de deficiencia do benefici�rio: (char(1)) I-Intelectual F-Fisica
			 * A-Auditiva V-Visual
			 */
			if (pre.getTpDeficiencia().equals("I")) {
				mensagem.append("Intelectual");
			} else if (pre.getTpDeficiencia().equals("F")) {
				mensagem.append("Física");
			} else if (pre.getTpDeficiencia().equals("A")) {
				mensagem.append("Auditiva");
			} else if (pre.getTpDeficiencia().equals("V")) {
				mensagem.append("Visual");
			} else {
				mensagem.append("Nenhuma");
			}
		}

		mensagem.append(getCampoHtml("TRABALHA EM ALTURA", pre.isSnTrabalhoEmAltura() ? "SIM" : "NAO"));
		mensagem.append(getCampoHtml("TRABALHA EM ESPACO CONFINADO", pre.isSnTrabalhoConfinado() ? "SIM" : "NAO"));

		mensagem.append(
				getCampoHtml("FITOSSANITARIOS(DEFENSIVOS AGRICOLAS)", pre.isSnFitossanitarios() ? "SIM" : "NAO"));

		return mensagem.toString();
	}

	private String getObservacoes(Conexao cnx, PreCadastroCbo pre) throws QtSQLException {

		StringBuilder mensagem = new StringBuilder();
		PreCadastroCbo preCadastroCboAnterior = PreCadastroCboLocalizador.buscaPreCadastroCbo(pre.getIdPreCadastro());

		mensagem.append(" Altera��o do Beneficiario:");
		if (pre.getCdBeneficiarioCartao() != null) {
			mensagem.append(pre.getCdBeneficiarioCartao());
		}

		mensagem.append("</b>");

		mensagem.append("<br/>");
		mensagem.append("<b>DATA SOLICITACAO: </b>");
		mensagem.append(pre.getDtSolicitacao().dataEHora());

		jPcmso.persistencia.geral.e.Empresa e = jPcmso.persistencia.geral.e.EmpresaLocalizador
				.buscaEmpresa(pre.getCdEmpresa());
		if (e != null) {
			mensagem.append(getCampoHtml("EMPRESA", pre.getCdEmpresa() + " - " + e.getDsEmpresa()));
		}

		mensagem.append(getCampoHtml("NOME BENEFICIARIO", pre.getDsNome()));

		if (pre.getDtAdmissao() != null) {
			if (preCadastroCboAnterior.getDtAdmissao() == null
					|| (!preCadastroCboAnterior.getDtAdmissao().eIgual(pre.getDtAdmissao()))) {
				mensagem.append(getCampoHtml("DATA DE ADMISAO", pre.getDtAdmissao().toString()));
			}
		}

		if (pre.getIdFilial() != null) {
			if (compara(preCadastroCboAnterior.getIdFilial(), pre.getIdFilial())) {
				if (pre.getIdFilial() != null) {
					FiliaisEmpresa filial = FiliaisEmpresaLocalizador.buscaFiliaisEmpresa(pre.getCdEmpresa(),
							pre.getIdFilial());
					if (filial != null) {
						mensagem.append(getCampoHtml("FILIAL", pre.getIdFilial() + " - " + filial.getDsFilial()));
					}
				}
			}
		}

		if (compara(preCadastroCboAnterior.getTpSexo(), pre.getTpSexo())) {
			if (pre.getTpSexo() != null) {
				mensagem.append(getCampoHtml("SEXO", pre.getTpSexo().equals("M") ? "Masculino" : "Feminino"));
			}
		}

		if (pre.getDtNascimento() != null) {
			if (preCadastroCboAnterior.getDtNascimento() == null
					|| (!preCadastroCboAnterior.getDtNascimento().eIgual(pre.getDtNascimento()))) {
				mensagem.append(getCampoHtml("DATA DE NASCIMENTO", pre.getDtNascimento().toString()));
			}
		}

		if (compara(preCadastroCboAnterior.getNrCpf(), pre.getNrCpf())) {
			if (pre.getNrCpf() != null) {
				mensagem.append(getCampoHtml("CPF", pre.getNrCpf()));
			}
		}

		if (compara(preCadastroCboAnterior.getCdRg(), pre.getCdRg())) {
			if (pre.getCdRg() != null) {
				mensagem.append(getCampoHtml("RG", pre.getCdRg()));
			}
		}

		if (compara(preCadastroCboAnterior.getDsOrgaoEmissorRg(), pre.getDsOrgaoEmissorRg())) {
			if (pre.getDsOrgaoEmissorRg() != null) {
				mensagem.append(getCampoHtml("ORGAO RG", pre.getDsOrgaoEmissorRg()));
			}
		}

		if (compara(preCadastroCboAnterior.getDsNomeDaMae(), pre.getDsNomeDaMae())) {
			if (pre.getDsNomeDaMae() != null) {
				mensagem.append(getCampoHtml("NOME DA MAE", pre.getDsNomeDaMae()));
			}
		}

		if (compara(preCadastroCboAnterior.getDsLogradouro(), pre.getDsLogradouro())) {
			if (pre.getDsLogradouro() != null) {
				mensagem.append(getCampoHtml("LOGRADOURO", pre.getDsLogradouro()));
			}
		}

		if (compara(preCadastroCboAnterior.getDsNumeroLogradouro(), pre.getDsNumeroLogradouro())) {
			if (pre.getDsNumeroLogradouro() != null) {
				mensagem.append(getCampoHtml("NUMERO", pre.getDsNumeroLogradouro()));
			}
		}
		if (compara(preCadastroCboAnterior.getDsComplementoLogradouro(), pre.getDsComplementoLogradouro())) {
			if (pre.getDsComplementoLogradouro() != null) {
				mensagem.append(getCampoHtml("COMPLEMENTO", pre.getDsComplementoLogradouro()));
			}
		}

		if (compara(preCadastroCboAnterior.getDsBairro(), pre.getDsBairro())) {
			if (pre.getDsBairro() != null) {
				mensagem.append(getCampoHtml("BAIRRO", pre.getDsBairro()));
			}
		}

		if (compara(preCadastroCboAnterior.getIdCidade() + "", pre.getIdCidade() + "")) {
			if (pre.getIdCidade() != null) {
				Cidade c = CidadeLocalizador.buscaCidade(pre.getCdEstado(), pre.getIdCidade());
				if (c != null) {
					mensagem.append(getCampoHtml("ESTADO", pre.getCdEstado()));
					mensagem.append(getCampoHtml("CIDADE", c.getDsCidade()));
				}
			}
		}

		if (compara(preCadastroCboAnterior.getTpEstadoCivil(), pre.getTpEstadoCivil())) {
			if (pre.getTpEstadoCivil() != null) {
				mensagem.append("<br/>");
				mensagem.append("<b>ESTADO CIVIL:&nbsp;</b>");
				if (pre.getTpEstadoCivil().equals("S")) {
					mensagem.append("Solteiro");
				} else if (pre.getTpEstadoCivil().equals("M")) {
					mensagem.append("Casado (M-Married)");
				} else if (pre.getTpEstadoCivil().equals("W")) {
					mensagem.append("Viuvo (W-Widow)");
				} else if (pre.getTpEstadoCivil().equals("D")) {
					mensagem.append("Divorciado");
				} else if (pre.getTpEstadoCivil().equals("A")) {
					mensagem.append("Apartado (Separado)");
				} else if (pre.getTpEstadoCivil().equals("U")) {
					mensagem.append("Uniao Estavel");
				}

			}
		}

		if (compara(preCadastroCboAnterior.getDsTelefoneContato(), pre.getDsTelefoneContato())) {
			if (pre.getDsTelefoneContato() != null) {
				mensagem.append(getCampoHtml("TELEFONE DE CONTATO", pre.getDsTelefoneContato()));
			}
		}

		if (compara(preCadastroCboAnterior.getDsAtividade(), pre.getDsAtividade())) {
			if (pre.getDsAtividade() != null) {
				mensagem.append(getCampoHtml("ATIVIDADE", pre.getDsAtividade()));
			}
		}

		if (compara(preCadastroCboAnterior.getNrPis(), pre.getNrPis())) {
			if (pre.getNrPis() != null) {
				mensagem.append(getCampoHtml("PIS/Pasep", pre.getNrPis()));
			}
		}

		if (compara(preCadastroCboAnterior.getNrCtps(), pre.getNrCtps())) {
			if (pre.getNrCtps() != null) {
				mensagem.append(getCampoHtml("CTPS", pre.getNrCtps()));
			}
		}

		if (compara(preCadastroCboAnterior.getNrSerieUf(), pre.getNrSerieUf())) {
			if (pre.getNrSerieUf() != null) {
				mensagem.append(getCampoHtml("SERIE (UF)", pre.getNrSerieUf()));
			}
		}

		if (compara(preCadastroCboAnterior.getCdCbo(), pre.getCdCbo())) {
			if (pre.getCdCbo() != null) {
				mensagem.append("<br/>");
				mensagem.append("<b>CBO:&nbsp;</b>");
				Cbo cbo = CboLocalizador.buscaCbo(pre.getCdCbo());
				if (cbo != null) {
					mensagem.append(cbo.getCdCbo() + " - " + cbo.getNmCbo());
				} else {
					mensagem.append(pre.getCdCbo());
				}
			}
		}

		if (pre.getIdFuncao() != null) {
			Funcao f = FuncaoLocalizador.buscaFuncao(pre.getIdFuncao());
			if (f != null) {
				mensagem.append(getCampoHtml("FUNCAO", f.getDsFuncao()));
			}
		}
		if (pre.getIdSetor() != null) {
			Setor s = SetorLocalizador.buscaSetor(pre.getIdSetor());
			if (s != null) {
				mensagem.append(getCampoHtml("SETOR", s.getDsSetor()));
			}
		}
		if (pre.getIdCargo() != null) {
			Cargo c = CargoLocalizador.buscaCargo(pre.getIdCargo());
			if (c != null) {
				mensagem.append(getCampoHtml("CARGO", c.getDsCargo()));
			}
		}

		if (pre.getIdLotacao() != null) {
			jPcmso.persistencia.geral.l.Lotacao lotacao = jPcmso.persistencia.geral.l.LotacaoLocalizador.buscaLotacao(
					ParametrosCliente.getCodigoCliente(), Inteiro.stoi(pre.getCdEmpresa()), pre.getIdLotacao());
			if (lotacao != null) {
				mensagem.append(getCampoHtml("LOTACAO", lotacao.getIdLotacao() + ": " + lotacao.getDsLotacao()));
			}
		}

		if (pre.getVlPeso() != null) {
			mensagem.append(getCampoHtml("PESO", pre.getVlPeso() != null ? pre.getVlPeso().toString() : ""));
		}

		if (pre.getVlPeso() != null) {
			mensagem.append(getCampoHtml("ALTURA", pre.getVlAltura() != null ? pre.getVlAltura().toString() : ""));
		}

		if (compara(preCadastroCboAnterior.isSnPossuiDeficiencia(), pre.isSnPossuiDeficiencia())) {
			mensagem.append(getCampoHtml("POSSUI ALGUMA DEFICIENCIA", pre.isSnPossuiDeficiencia() ? "SIM" : "NAO"));
		}

		if (compara(preCadastroCboAnterior.getTpDeficiencia(), pre.getTpDeficiencia())) {
			if (pre.getTpDeficiencia() != null) {
				mensagem.append("<br/>");
				mensagem.append("<b>TIPO DE DEFICIENCIA:&nbsp;</b>");
				/**
				 * Tipo de deficiencia do benefici�rio: (char(1)) I-Intelectual F-Fisica
				 * A-Auditiva V-Visual
				 */
				if (pre.getTpDeficiencia().equals("I")) {
					mensagem.append("Intelectual");
				} else if (pre.getTpDeficiencia().equals("F")) {
					mensagem.append("Física");
				} else if (pre.getTpDeficiencia().equals("A")) {
					mensagem.append("Auditiva");
				} else if (pre.getTpDeficiencia().equals("V")) {
					mensagem.append("Visual");
				} else {
					mensagem.append("Nenhuma");
				}
			}
		}

		if (compara(preCadastroCboAnterior.isSnTrabalhoEmAltura(), pre.isSnTrabalhoEmAltura())) {
			mensagem.append(getCampoHtml("TRABALHA EM ALTURA", pre.isSnTrabalhoEmAltura() ? "SIM" : "NAO"));
		}

		if (compara(preCadastroCboAnterior.isSnTrabalhoConfinado(), pre.isSnTrabalhoConfinado())) {
			mensagem.append(getCampoHtml("TRABALHA EM ESPACO CONFINADO", pre.isSnTrabalhoConfinado() ? "SIM" : "NAO"));
		}

		if (compara(preCadastroCboAnterior.isSnFitossanitarios(), pre.isSnFitossanitarios())) {
			mensagem.append(
					getCampoHtml("FITOSSANITARIOS(DEFENSIVOS AGRICOLAS)", pre.isSnFitossanitarios() ? "SIM" : "NAO"));
		}

		return mensagem.toString();
	}

	public String getNmLotacao(Beneficiario benef) {
		if (benef != null) {
			if (benef.getIdLotacao() != null) {
				try {
					ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
					try {

						Lotacao lotacao = LotacaoLocalizador.buscaLotacao(cnx, benef.getCdUnimed(),
								benef.getIdEmpresa(), benef.getIdLotacao());

						if (lotacao != null) {
							return lotacao.getDsLotacao();
						}
					} finally {
						cnx.libera();
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}

	public String getGrupoCarencia(Integer idGrupo) {

		if (idGrupo != null) {
			try {
				ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
				try {
					GrupoDeCarencia servico = GrupoDeCarenciaLocalizador.buscaGrupoDeCarencia(cnx, idGrupo);
					if (servico != null) {
						return servico.getDsGrupoCarencia();
					}

				} finally {
					cnx.libera();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	public String getNomeProcedimento(String cdProcedimento) {

		try {
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
			try {
				Servico servico = ServicoLocalizador.buscaServico(cnx, cdProcedimento);
				if (servico != null) {
					return servico.getDsServico();
				}

			} finally {
				cnx.libera();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public String getNomeEstado(String cdEstado) {

		try {
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
			try {
				Estado estado = EstadoLocalizador.buscaEstado(cnx, cdEstado);
				if (estado != null) {
					return estado.getDsEstado();
				}

			} finally {
				cnx.libera();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public String getNomeCidade(String cdEstado, Integer idCidade) {

		try {
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
			try {
				Cidade cidade = CidadeLocalizador.buscaCidade(cnx, cdEstado, idCidade);
				if (cidade != null) {
					return cidade.getDsCidade();
				}

			} finally {
				cnx.libera();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public String getNomeEmpresa(String cdUnimed, Integer idEmpresa) {

		if (cdUnimed != null && idEmpresa != null) {
			try {
				ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
				try {
					Empresa empresa = EmpresaLocalizador.buscaEmpresa(cnx, cdUnimed, idEmpresa);

					if (empresa != null) {
						return empresa.getDsRazaoSocial();
					}

				} finally {
					cnx.libera();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	public String getDependencia(String cdDependencia) {

		try {
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
			try {
				Dependencia dependencia = DependenciaLocalizador.buscaDependencia(cnx, cdDependencia);

				if (dependencia != null) {
					return dependencia.getDsDependencia();
				}

			} finally {
				cnx.libera();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public int getIdade(QtData data) {

		if (data != null) {
			try {
				return QtData.idade(data);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return 0;
	}

	public void cancelarEdicao() {
		mntPreCadastro = "";
		Util.sendRedirect("resources/page/dashboard.jsf");
	}

	public void novoRegistro() {
		snPossuiDeficiencia = null;
		snPossuiDeficienciaCbo = null;
		snPossuiDeficiencia = null;
		snTrabalhoEmAltura = null;
		snEspacoConfinado = null;
		preCadastroCboSelecionado = new PreCadastroCbo();
		Util.sendRedirect("resources/page/mntPreCadastro.jsf");
	}

	public void selecionaPreCadastro(PreCadastroCbo pre) {
		preCadastroCboSelecionado = pre;
		Util.sendRedirect("resources/page/mntPreCadastro.jsf");
	}

	public String getMsgAoUsuario() {
		return msgAoUsuario;
	}

	public void setMsgAoUsuario(String msgAoUsuario) {
		this.msgAoUsuario = msgAoUsuario;
	}

	public Integer getMsgExibida() {
		return msgExibida;
	}

	public void setMsgExibida(Integer msgExibida) {
		this.msgExibida = msgExibida;
	}

	public PreCadastroCbo getPreCadastroCboSelecionado() {
		return preCadastroCboSelecionado;
	}

	public void setPreCadastroCboSelecionado(PreCadastroCbo pre) {
		setIdLotacao(pre.getIdLotacao());
		this.preCadastroCboSelecionado = pre;
	}

	public String getMntPreCadastro() {
		return mntPreCadastro;
	}

	public void setMntPreCadastro(String mntPreCadastro) {
		this.mntPreCadastro = mntPreCadastro;
	}

	public String getDsLotacao() {
		return dsLotacao;
	}

	public void setDsLotacao(String dsLotacao) {
		this.dsLotacao = dsLotacao;
	}

	public Integer getIdLotacao() {
		return idLotacao;
	}

	public void pesquisaLotacao() {
		try {
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
			try {
				listaLotacao = carregaLotacao(cnx);
			} finally {
				cnx.libera();
			}
		} catch (Exception e) {
			e.printStackTrace();
			msgExibida = 0;
			msgAoUsuario = "Ops! " + e.getMessage();
		}
	}

	public void setIdLotacao(Integer idLotacao) {
		if (preCadastroCboSelecionado != null) {
			if (idLotacao != null) {
				try {
					ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
					try {
						Lotacao lotacao = LotacaoLocalizador.buscaLotacao(
								ParametrosSistema.getInstance().getCodigoDoCliente(), new Integer(idEmpresaRelacioanda),
								idLotacao);

						if (lotacao != null) {
							preCadastroCboSelecionado.setIdLotacao(idLotacao);
							dsLotacao = lotacao.getDsLotacao();
						} else {
							idLotacao = null;
							dsLotacao = null;
						}
					} finally {
						cnx.libera();
					}
				} catch (Exception e) {
					e.printStackTrace();
					msgExibida = 0;
					msgAoUsuario = "Ops! " + e.getMessage();
				}
			} else {
				preCadastroCboSelecionado.setIdLotacao(null);
				dsLotacao = null;
			}
		}
		this.idLotacao = idLotacao;
	}

	public List<Cidade> getListaCidades() {
		return listaCidades;
	}

	public void setListaCidades(List<Cidade> listaCidades) {
		this.listaCidades = listaCidades;
	}

	public List<Cbo> getListaCbo() {
		return listaCbo;
	}

	public void setListaCbo(List<Cbo> listaCbo) {
		this.listaCbo = listaCbo;
	}

	public List<Setor> getListaSetor() {
		return listaSetor;
	}

	public void setListaSetor(List<Setor> listaSetor) {
		this.listaSetor = listaSetor;
	}

	public List<Funcao> getListaFuncao() {
		return listaFuncao;
	}

	public void setListaFuncao(List<Funcao> listaFuncao) {
		this.listaFuncao = listaFuncao;
	}

	public List<Cargo> getListaCargo() {
		return listaCargo;
	}

	public void setListaCargo(List<Cargo> listaCargo) {
		this.listaCargo = listaCargo;
	}

	public String getNmEmpresaSelecionada() {
		return nmEmpresaSelecionada;
	}

	public void setNmEmpresaSelecionada(String nmEmpresaSelecionada) {
		this.nmEmpresaSelecionada = nmEmpresaSelecionada;
	}

	public String getDtNascimento() {
		if (preCadastroCboSelecionado != null) {
			if (preCadastroCboSelecionado.getDtNascimento() != null) {
				return preCadastroCboSelecionado.getDtNascimento().toString();
			}
		}
		return dtNascimento;
	}

	public void setDtNascimento(String dtNascimento) {
		this.dtNascimento = dtNascimento;
		if (dtNascimento != null) {
			preCadastroCboSelecionado.setDtNascimento(new QtData(dtNascimento));
		}
	}

	public String getDtAdmissao() {
		if (preCadastroCboSelecionado != null) {
			if (preCadastroCboSelecionado.getDtAdmissao() != null) {
				return preCadastroCboSelecionado.getDtAdmissao().toString();
			}
		}
		return dtAdmissao;
	}

	public void setDtAdmissao(String dtAdmissao) {
		this.dtAdmissao = dtAdmissao;
		if (dtAdmissao != null) {
			preCadastroCboSelecionado.setDtAdmissao(new QtData(dtAdmissao));
		}
	}

	public List<PreCadastroCbo> getListagemPreCadastro() {
		return listagemPreCadastro;
	}

	public void setListagemPreCadastro(List<PreCadastroCbo> listagemPreCadastro) {
		this.listagemPreCadastro = listagemPreCadastro;
	}

	public List<Beneficiario> getListagemBeneficiario() {
		return listagemBeneficiario;
	}

	public void setListagemBeneficiario(List<Beneficiario> listagemBeneficiario) {
		this.listagemBeneficiario = listagemBeneficiario;
	}

	public Integer getIdEmpresaRelacioanda() {
		return idEmpresaRelacioanda;
	}

	public void setIdEmpresaRelacioanda(Integer idEmpresaRelacioanda) {
		this.idEmpresaRelacioanda = idEmpresaRelacioanda;
	}

	public String getCdCartaoPesquisa() {
		return cdCartaoPesquisa;
	}

	public void setCdCartaoPesquisa(String cdCartaoPesquisa) {
		this.cdCartaoPesquisa = cdCartaoPesquisa;
	}

	public String getDsNomePesquisa() {
		return dsNomePesquisa;
	}

	public void setDsNomePesquisa(String dsNomePesquisa) {
		this.dsNomePesquisa = dsNomePesquisa;
	}

	public List<MotivoExclusaoBeneficiario> getListaMotivoExclusao() {
		return listaMotivoExclusao;
	}

	public void setListaMotivoExclusao(List<MotivoExclusaoBeneficiario> listaMotivoExclusao) {
		this.listaMotivoExclusao = listaMotivoExclusao;
	}

	public Integer getIdMotivoExclusaoSelecionado() {
		return idMotivoExclusaoSelecionado;
	}

	public void setIdMotivoExclusaoSelecionado(Integer idMotivoExclusaoSelecionado) {
		this.idMotivoExclusaoSelecionado = idMotivoExclusaoSelecionado;
	}

	public String getDtExclusaoSelecionada() {
		return dtExclusaoSelecionada;
	}

	public void setDtExclusaoSelecionada(String dtExclusaoSelecionada) {
		this.dtExclusaoSelecionada = dtExclusaoSelecionada;
	}

	public List<Lotacao> getListaLotacao() {
		return listaLotacao;
	}

	public void setListaLotacao(List<Lotacao> listaLotacao) {
		this.listaLotacao = listaLotacao;
	}

	public String getDsLotacaoPesquisa() {
		return dsLotacaoPesquisa;
	}

	public void setDsLotacaoPesquisa(String dsLotacaoPesquisa) {
		this.dsLotacaoPesquisa = dsLotacaoPesquisa;
	}

	public String getIdLotacaoPesquisa() {
		return idLotacaoPesquisa;
	}

	public void setIdLotacaoPesquisa(String idLotacaoPesquisa) {
		this.idLotacaoPesquisa = idLotacaoPesquisa;
	}

	public String getSnTrabalhoEmAltura() {
		if (beneficiarioSelecionado != null && beneficiarioSelecionado.getBeneficiarioCbo() != null) {
			if (beneficiarioSelecionado.getBeneficiarioCbo().isSnTrabalhoEmAltura()) {
				snTrabalhoEmAltura = "S";
			} else {
				snTrabalhoEmAltura = "N";
			}
		}
		return snTrabalhoEmAltura;
	}

	public void setSnTrabalhoEmAltura(String snTrabalhoEmAltura) {
		if (snTrabalhoEmAltura != null) {
			if (beneficiarioSelecionado != null && beneficiarioSelecionado.getBeneficiarioCbo() != null) {
				beneficiarioSelecionado.getBeneficiarioCbo().setSnTrabalhoEmAltura(snTrabalhoEmAltura);
			}
		}
		this.snTrabalhoEmAltura = snTrabalhoEmAltura;
	}

	public String getSnEspacoConfinado() {
		if (beneficiarioSelecionado != null && beneficiarioSelecionado.getBeneficiarioCbo() != null) {
			if (beneficiarioSelecionado.getBeneficiarioCbo().isSnEspacoConfinado()) {
				snEspacoConfinado = "S";
			} else {
				snEspacoConfinado = "N";
			}
		}
		return snEspacoConfinado;
	}

	public void setSnEspacoConfinado(String snEspacoConfinado) {
		if (snEspacoConfinado != null) {
			if (beneficiarioSelecionado != null && beneficiarioSelecionado.getBeneficiarioCbo() != null) {
				beneficiarioSelecionado.getBeneficiarioCbo().setSnEspacoConfinado(snEspacoConfinado);
			}
		}
		this.snEspacoConfinado = snEspacoConfinado;
	}

	public String getSnPossuiDeficienciaCbo() {

		if (beneficiarioSelecionado != null && beneficiarioSelecionado.getBeneficiarioCbo() != null) {
			if( beneficiarioSelecionado.getBeneficiarioCbo().getCdBeneficiarioCartao() != null ) {
				if (beneficiarioSelecionado.getBeneficiarioCbo().isSnPossuiDeficiencia()) {
					snPossuiDeficienciaCbo = "S";
				} else {
					snPossuiDeficienciaCbo = "N";
				}
			}
		}
		return snPossuiDeficienciaCbo;
	}

	public void setSnPossuiDeficienciaCbo(String snPossuiDeficienciaCbo) {
		if (snPossuiDeficienciaCbo != null) {
			if (beneficiarioSelecionado != null && beneficiarioSelecionado.getBeneficiarioCbo() != null) {
				beneficiarioSelecionado.getBeneficiarioCbo().setSnPossuiDeficiencia(snPossuiDeficienciaCbo);
			}
		}
		this.snPossuiDeficienciaCbo = snPossuiDeficienciaCbo;
	}

	public String getSnPossuiDeficiencia() {
		if (preCadastroCboSelecionado != null) {
			if( preCadastroCboSelecionado.getCdBeneficiarioCartao() != null ) {
				if (preCadastroCboSelecionado.isSnPossuiDeficiencia()) {
					snPossuiDeficiencia = "S";
				} else {
					snPossuiDeficiencia = "N";
				}
			}
		}
		return snPossuiDeficiencia;
	}

	public void setSnPossuiDeficiencia(String snPossuiDeficiencia) {
		if (snPossuiDeficiencia != null) {
			if (preCadastroCboSelecionado != null) {
				preCadastroCboSelecionado.setSnPossuiDeficiencia(snPossuiDeficiencia);
			}
		}
		this.snPossuiDeficiencia = snPossuiDeficiencia;
	}

	public DadosBeneficiarioSelecionado getBeneficiarioSelecionado() {
		return beneficiarioSelecionado;
	}

	public void setBeneficiarioSelecionado(DadosBeneficiarioSelecionado beneficiarioSelecionado) {
		this.beneficiarioSelecionado = beneficiarioSelecionado;
	}

	public String getDsCbo() {
		return dsCbo;
	}

	public void setDsCbo(String dsCbo) {
		this.dsCbo = dsCbo;
	}

	public String getDsFuncao() {
		return dsFuncao;
	}

	public void setDsFuncao(String dsFuncao) {
		this.dsFuncao = dsFuncao;
	}

	public String getDsCargo() {
		return dsCargo;
	}

	public void setDsCargo(String dsCargo) {
		this.dsCargo = dsCargo;
	}

	public String getDsSetor() {
		return dsSetor;
	}

	public void setDsSetor(String dsSetor) {
		this.dsSetor = dsSetor;
	}

	public String getDsFuncaoAnterior() {
		return dsFuncaoAnterior;
	}

	public void setDsFuncaoAnterior(String dsFuncaoAnterior) {
		this.dsFuncaoAnterior = dsFuncaoAnterior;
	}

	public String getAbaPreCadastro() {
		return abaPreCadastro;
	}

	public void setAbaPreCadastro(String abaPreCadastro) {
		this.abaPreCadastro = abaPreCadastro;
	}

	public String getAbaBeneficiario() {
		return abaBeneficiario;
	}

	public void setAbaBeneficiario(String abaBeneficiario) {
		this.abaBeneficiario = abaBeneficiario;
	}

	public String getAbaBeneficiarioInativo() {
		return abaBeneficiarioInativo;
	}

	public void setAbaBeneficiarioInativo(String abaBeneficiarioInativo) {
		this.abaBeneficiarioInativo = abaBeneficiarioInativo;
	}

	public List<Beneficiario> getListagemBeneficiarioInativo() {
		return listagemBeneficiarioInativo;
	}

	public void setListagemBeneficiarioInativo(List<Beneficiario> listagemBeneficiarioInativo) {
		this.listagemBeneficiarioInativo = listagemBeneficiarioInativo;
	}

	public List<PaisAns> getListaPais() {
		return listaPais;
	}

	public void setListaPais(List<PaisAns> listaPais) {
		this.listaPais = listaPais;
	}

	public List<Estado> getListaEstados() {
		return listaEstados;
	}

	public void setListaEstados(List<Estado> listaEstados) {
		this.listaEstados = listaEstados;
	}

	public List<FiliaisEmpresa> getListaFiliais() {
		return listaFiliais;
	}

	public void setListaFiliais(List<FiliaisEmpresa> listaFiliais) {
		this.listaFiliais = listaFiliais;
	}

	public Usuario getUsuarioLogado() {
		return usuarioLogado;
	}

	public void setUsuarioLogado(Usuario usuarioLogado) {
		this.usuarioLogado = usuarioLogado;
	}

	public String getCdEmpresaRel() {
		return cdEmpresaRel;
	}

	public void setCdEmpresaRel(String cdEmpresaRel) {
		this.cdEmpresaRel = cdEmpresaRel;
		if (this.cdEmpresaRel != null && !this.cdEmpresaRel.trim().equals("")) {
			dsNomeEmpresa = getNomeEmpresa();
		}
	}

	private String getNomeEmpresa() {
		try {
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
			try {
				jPcmso.persistencia.geral.e.Empresa empresa = jPcmso.persistencia.geral.e.EmpresaLocalizador
						.buscaEmpresa(cnx, cdEmpresaRel);
				if (empresa != null) {
					return empresa.getDsEmpresa();
				} else {
					dsNomeEmpresa = null;
					return "n�o encontrado";
				}

			} finally {
				cnx.libera();
			}

		} catch (Exception e) {
			e.printStackTrace();
			msgAoUsuario = "Ops! " + e.getMessage();
			msgExibida = 0;
		}
		dsNomeEmpresa = null;
		return null;
	}

	private String getNomeEmpresaFinal() {
		try {
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
			try {
				jPcmso.persistencia.geral.e.Empresa empresa = jPcmso.persistencia.geral.e.EmpresaLocalizador
						.buscaEmpresa(cnx, cdEmpresaRel);
				if (empresa != null) {
					return empresa.getDsEmpresa();
				} else {
					dsNomeEmpresaFinal = null;
					return "n�o encontrado";
				}

			} finally {
				cnx.libera();
			}

		} catch (Exception e) {
			e.printStackTrace();
			msgAoUsuario = "Ops! " + e.getMessage();
			msgExibida = 0;
		}
		dsNomeEmpresaFinal = null;
		return null;
	}

	public String getDsNomeEmpresa() {
		return dsNomeEmpresa;
	}

	public void setDsNomeEmpresa(String dsNomeEmpresa) {
		this.dsNomeEmpresa = dsNomeEmpresa;
	}

	public String getCdEmpresaFinalRel() {
		return cdEmpresaFinalRel;
	}

	public void setCdEmpresaFinalRel(String cdEmpresaFinalRel) {
		this.cdEmpresaFinalRel = cdEmpresaFinalRel;
		if (this.cdEmpresaFinalRel != null && !this.cdEmpresaFinalRel.trim().equals("")) {
			dsNomeEmpresaFinal = getNomeEmpresaFinal();
		}
	}

	public String getDsNomeEmpresaFinal() {
		return dsNomeEmpresaFinal;
	}

	public void setDsNomeEmpresaFinal(String dsNomeEmpresaFinal) {
		this.dsNomeEmpresaFinal = dsNomeEmpresaFinal;
	}

	public MensagemEnviada getMensagemEnviada() {
		return mensagemEnviada;
	}

	public void setMensagemEnviada(MensagemEnviada mensagemEnviada) {
		this.mensagemEnviada = mensagemEnviada;
	}

	public String getDtInicial() {
		return dtInicial;
	}

	public void setDtInicial(String dtInicial) {
		this.dtInicial = dtInicial;
	}

	public String getDtFinal() {
		return dtFinal;
	}

	public void setDtFinal(String dtFinal) {
		this.dtFinal = dtFinal;
	}

	public String getTpRelOrdem() {
		return tpRelOrdem;
	}

	public void setTpRelOrdem(String tpRelOrdem) {
		this.tpRelOrdem = tpRelOrdem;
	}

	public String getTpRelAcao() {
		return tpRelAcao;
	}

	public void setTpRelAcao(String tpRelAcao) {
		this.tpRelAcao = tpRelAcao;
	}

	public String getCdPlanoRel() {
		return cdPlanoRel;
	}

	public void setCdPlanoRel(String cdPlanoRel) {
		this.cdPlanoRel = cdPlanoRel;
	}

	public String getNrCco() {
		return nrCco;
	}

	public void setNrCco(String nrCco) {
		this.nrCco = nrCco;
	}

	public String getTpContratoPacote() {
		return tpContratoPacote;
	}

	public void setTpContratoPacote(String tpContratoPacote) {
		this.tpContratoPacote = tpContratoPacote;
	}

	public String getSnRelExcluidosData() {
		return snRelExcluidosData;
	}

	public void setSnRelExcluidosData(String snRelExcluidosData) {
		this.snRelExcluidosData = snRelExcluidosData;
	}

	public Integer getIdLotacaoRelExame() {
		return idLotacaoRelExame;
	}

	public void setIdLotacaoRelExame(Integer idLotacaoRelExame) {
		this.idLotacaoRelExame = idLotacaoRelExame;
		if (this.idLotacaoRelExame != null) {
			dsNomeLotacao = getNomeLotacao(this.idLotacaoRelExame);
		}
	}

	private String getNomeLotacao(Integer idLotacao) {
		try {
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
			try {
				Lotacao lotacao = LotacaoLocalizador.buscaLotacao(cnx,
						ParametrosSistema.getInstance().getCodigoDoCliente(), new Integer(cdEmpresaRel), idLotacao);
				if (lotacao != null) {
					return lotacao.getDsLotacao();
				} else {
					dsNomeLotacao = null;
					return "n�o encontrado";
				}

			} finally {
				cnx.libera();
			}

		} catch (Exception e) {
			e.printStackTrace();
			msgAoUsuario = "Ops! " + e.getMessage();
			msgExibida = 0;
		}
		dsNomeLotacao = null;
		return null;
	}

	public String getNomeRisco(Integer idRisco) {
		try {
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
			try {
				Risco risco = RiscoLocalizador.buscaRisco(cnx, idRisco);
				if (risco != null) {
					return risco.getDsRisco();
				}
			} finally {
				cnx.libera();
			}

		} catch (Exception e) {
			e.printStackTrace();
			msgAoUsuario = "Ops! " + e.getMessage();
			msgExibida = 0;
		}
		return "Indefinido";
	}

	public String getDsNomeLotacao() {
		return dsNomeLotacao;
	}

	public void setDsNomeLotacao(String dsNomeLotacao) {
		this.dsNomeLotacao = dsNomeLotacao;
	}

	public String getDtInclusaoInicialRel() {
		return dtInclusaoInicialRel;
	}

	public void setDtInclusaoInicialRel(String dtInclusaoInicialRel) {
		this.dtInclusaoInicialRel = dtInclusaoInicialRel;
	}

	public String getDtInclusaoFinalRel() {
		return dtInclusaoFinalRel;
	}

	public void setDtInclusaoFinalRel(String dtInclusaoFinalRel) {
		this.dtInclusaoFinalRel = dtInclusaoFinalRel;
	}

	public String getSnGeral() {
		return snGeral;
	}

	public void setSnGeral(String snGeral) {
		this.snGeral = snGeral;
	}

	public String getSnExcluido() {
		return snExcluido;
	}

	public void setSnExcluido(String snExcluido) {
		this.snExcluido = snExcluido;
	}

	public String getSnAtivos() {
		return snAtivos;
	}

	public void setSnAtivos(String snAtivos) {
		this.snAtivos = snAtivos;
	}

	public String getSnComPacotes() {
		return snComPacotes;
	}

	public void setSnComPacotes(String snComPacotes) {
		this.snComPacotes = snComPacotes;
	}

	public String getSnSemPacotes() {
		return snSemPacotes;
	}

	public void setSnSemPacotes(String snSemPacotes) {
		this.snSemPacotes = snSemPacotes;
	}

	public String getCdEmpresaPesquisa() {
		return cdEmpresaPesquisa;
	}

	public void setCdEmpresaPesquisa(String cdEmpresaPesquisa) {
		this.cdEmpresaPesquisa = cdEmpresaPesquisa;
	}

	public List<Risco> getListaRiscos() {
		return listaRiscos;
	}

	public void setListaRiscos(List<Risco> listaRiscos) {
		this.listaRiscos = listaRiscos;
	}

	public String getDtAdmissaoCbo() {
		if (beneficiarioSelecionado != null) {
			if (beneficiarioSelecionado.getBeneficiarioCbo() != null) {
				if (beneficiarioSelecionado.getBeneficiarioCbo().getDtAdmissao() != null) {
					return beneficiarioSelecionado.getBeneficiarioCbo().getDtAdmissao().toString();
				}
			}
		} else {
			return "";
		}
		return dtAdmissaoCbo;
	}

	public void setDtAdmissaoCbo(String dtAdmissaoCbo) {
		this.dtAdmissaoCbo = dtAdmissaoCbo;

		if (this.dtAdmissaoCbo != null) {
			if (beneficiarioSelecionado.getBeneficiarioCbo() != null) {
				beneficiarioSelecionado.getBeneficiarioCbo().setDtAdmissao(new QtData(this.dtAdmissaoCbo));
			}
		}
	}

	public String getDtAdmissaoPPP() {
		return dtAdmissaoPPP;
	}

	public void setDtAdmissaoPPP(String dtAdmissaoPPP) {
		this.dtAdmissaoPPP = dtAdmissaoPPP;
	}

	public String getDtDemissaoPPP() {
		return dtDemissaoPPP;
	}

	public void setDtDemissaoPPP(String dtDemissaoPPP) {
		this.dtDemissaoPPP = dtDemissaoPPP;
	}

	public String getDtNascimentoPPP() {
		return dtNascimentoPPP;
	}

	public void setDtNascimentoPPP(String dtNascimentoPPP) {
		this.dtNascimentoPPP = dtNascimentoPPP;
	}

	public String getNrNitPPP() {
		return nrNitPPP;
	}

	public void setNrNitPPP(String nrNitPPP) {
		this.nrNitPPP = nrNitPPP;
	}

	public String getNrCtpsPPP() {
		return nrCtpsPPP;
	}

	public void setNrCtpsPPP(String nrCtpsPPP) {
		this.nrCtpsPPP = nrCtpsPPP;
	}

	public String getNrSeriePPP() {
		return nrSeriePPP;
	}

	public void setNrSeriePPP(String nrSeriePPP) {
		this.nrSeriePPP = nrSeriePPP;
	}

	public String getSnAcidentePPP() {
		return snAcidentePPP;
	}

	public void setSnAcidentePPP(String snAcidentePPP) {
		this.snAcidentePPP = snAcidentePPP;
	}

	public String getDtAcidentePPP() {
		return dtAcidentePPP;
	}

	public void setDtAcidentePPP(String dtAcidentePPP) {
		this.dtAcidentePPP = dtAcidentePPP;
	}

	public String getNrCatPPP() {
		return nrCatPPP;
	}

	public void setNrCatPPP(String nrCatPPP) {
		this.nrCatPPP = nrCatPPP;
	}

	public String getNmResponsavelPPP() {
		return nmResponsavelPPP;
	}

	public void setNmResponsavelPPP(String nmResponsavelPPP) {
		this.nmResponsavelPPP = nmResponsavelPPP;
	}

	public String getNrNitResponsavelPPP() {
		return nrNitResponsavelPPP;
	}

	public void setNrNitResponsavelPPP(String nrNitResponsavelPPP) {
		this.nrNitResponsavelPPP = nrNitResponsavelPPP;
	}

	public String getDtAdmissaoFucaoPPP() {
		return dtAdmissaoFucaoPPP;
	}

	public void setDtAdmissaoFucaoPPP(String dtAdmissaoFucaoPPP) {
		this.dtAdmissaoFucaoPPP = dtAdmissaoFucaoPPP;
	}

	public String getDtFimFincaoFuncaoPPP() {
		return dtFimFincaoFuncaoPPP;
	}

	public void setDtFimFincaoFuncaoPPP(String dtFimFincaoFuncaoPPP) {
		this.dtFimFincaoFuncaoPPP = dtFimFincaoFuncaoPPP;
	}

	public String getDsFuncaoFuncaoPPP() {
		return dsFuncaoFuncaoPPP;
	}

	public void setDsFuncaoFuncaoPPP(String dsFuncaoFuncaoPPP) {
		this.dsFuncaoFuncaoPPP = dsFuncaoFuncaoPPP;
	}

	public String getDsCBOFuncaoPPP() {
		return dsCBOFuncaoPPP;
	}

	public void setDsCBOFuncaoPPP(String dsCBOFuncaoPPP) {
		this.dsCBOFuncaoPPP = dsCBOFuncaoPPP;
	}

	public String getDsSetorFuncaoPPP() {
		return dsSetorFuncaoPPP;
	}

	public void setDsSetorFuncaoPPP(String dsSetorFuncaoPPP) {
		this.dsSetorFuncaoPPP = dsSetorFuncaoPPP;
	}

	public String getDtMudancaTrocaPPP() {
		return dtMudancaTrocaPPP;
	}

	public void setDtMudancaTrocaPPP(String dtMudancaTrocaPPP) {
		this.dtMudancaTrocaPPP = dtMudancaTrocaPPP;
	}

	public String getDtFimMudancaTrocaPPP() {
		return dtFimMudancaTrocaPPP;
	}

	public void setDtFimMudancaTrocaPPP(String dtFimMudancaTrocaPPP) {
		this.dtFimMudancaTrocaPPP = dtFimMudancaTrocaPPP;
	}

	public String getDsFuncaoTrocaPPP() {
		return dsFuncaoTrocaPPP;
	}

	public void setDsFuncaoTrocaPPP(String dsFuncaoTrocaPPP) {
		this.dsFuncaoTrocaPPP = dsFuncaoTrocaPPP;
	}

	public String getDsCBOTrocaPPP() {
		return dsCBOTrocaPPP;
	}

	public void setDsCBOTrocaPPP(String dsCBOTrocaPPP) {
		this.dsCBOTrocaPPP = dsCBOTrocaPPP;
	}

	public String getDsSetorTrocaPPP() {
		return dsSetorTrocaPPP;
	}

	public void setDsSetorTrocaPPP(String dsSetorTrocaPPP) {
		this.dsSetorTrocaPPP = dsSetorTrocaPPP;
	}

	public String getDsObervacoesPPP() {
		return dsObervacoesPPP;
	}

	public void setDsObervacoesPPP(String dsObervacoesPPP) {
		this.dsObervacoesPPP = dsObervacoesPPP;
	}

	public String getDsUFPPP() {
		return dsUFPPP;
	}

	public void setDsUFPPP(String dsUFPPP) {
		this.dsUFPPP = dsUFPPP;
	}

	public String getAbaSolicitacaoPPP() {
		return abaSolicitacaoPPP;
	}

	public void setAbaSolicitacaoPPP(String abaSolicitacaoPPP) {
		this.abaSolicitacaoPPP = abaSolicitacaoPPP;
	}

	public List<DadosSolicitacaoPPPPCMSO> getListagemPPPPendente() {
		return listagemPPPPendente;
	}

	public void setListagemPPPPendente(List<DadosSolicitacaoPPPPCMSO> listagemPPPPendente) {
		this.listagemPPPPendente = listagemPPPPendente;
	}

	public DadosSolicitacaoPPPPCMSO getDadosSolicitacaoPPPSelecionada() {
		return dadosSolicitacaoPPPSelecionada;
	}

	public void setDadosSolicitacaoPPPSelecionada(DadosSolicitacaoPPPPCMSO dadosSolicitacaoPPPSelecionada) {
		this.dadosSolicitacaoPPPSelecionada = dadosSolicitacaoPPPSelecionada;
	}

	public boolean isHabFuncao2() {
		return habFuncao2;
	}

	public void setHabFuncao2(boolean habFuncao2) {
		this.habFuncao2 = habFuncao2;
	}

	public boolean isHabFuncao3() {
		return habFuncao3;
	}

	public void setHabFuncao3(boolean habFuncao3) {
		this.habFuncao3 = habFuncao3;
	}

	public boolean isHabFuncao4() {
		return habFuncao4;
	}

	public void setHabFuncao4(boolean habFuncao4) {
		this.habFuncao4 = habFuncao4;
	}

	public boolean isHabFuncao5() {
		return habFuncao5;
	}

	public void setHabFuncao5(boolean habFuncao5) {
		this.habFuncao5 = habFuncao5;
	}

	public boolean isHabFuncao6() {
		return habFuncao6;
	}

	public void setHabFuncao6(boolean habFuncao6) {
		this.habFuncao6 = habFuncao6;
	}

	public boolean isHabFuncao7() {
		return habFuncao7;
	}

	public void setHabFuncao7(boolean habFuncao7) {
		this.habFuncao7 = habFuncao7;
	}

	public boolean isHabFuncao8() {
		return habFuncao8;
	}

	public void setHabFuncao8(boolean habFuncao8) {
		this.habFuncao8 = habFuncao8;
	}

	public boolean isHabFuncao9() {
		return habFuncao9;
	}

	public void setHabFuncao9(boolean habFuncao9) {
		this.habFuncao9 = habFuncao9;
	}

	public boolean isHabFuncao10() {
		return habFuncao10;
	}

	public void setHabFuncao10(boolean habFuncao10) {
		this.habFuncao10 = habFuncao10;
	}

	public String getDtAdmissaoFucaoPPP6() {
		return dtAdmissaoFucaoPPP6;
	}

	public void setDtAdmissaoFucaoPPP6(String dtAdmissaoFucaoPPP6) {
		this.dtAdmissaoFucaoPPP6 = dtAdmissaoFucaoPPP6;
	}

	public String getDtFimFincaoFuncaoPPP6() {
		return dtFimFincaoFuncaoPPP6;
	}

	public void setDtFimFincaoFuncaoPPP6(String dtFimFincaoFuncaoPPP6) {
		this.dtFimFincaoFuncaoPPP6 = dtFimFincaoFuncaoPPP6;
	}

	public String getDsFuncaoFuncaoPPP6() {
		return dsFuncaoFuncaoPPP6;
	}

	public void setDsFuncaoFuncaoPPP6(String dsFuncaoFuncaoPPP6) {
		this.dsFuncaoFuncaoPPP6 = dsFuncaoFuncaoPPP6;
	}

	public String getDsCBOFuncaoPPP6() {
		return dsCBOFuncaoPPP6;
	}

	public void setDsCBOFuncaoPPP6(String dsCBOFuncaoPPP6) {
		this.dsCBOFuncaoPPP6 = dsCBOFuncaoPPP6;
	}

	public String getDsSetorFuncaoPPP6() {
		return dsSetorFuncaoPPP6;
	}

	public void setDsSetorFuncaoPPP6(String dsSetorFuncaoPPP6) {
		this.dsSetorFuncaoPPP6 = dsSetorFuncaoPPP6;
	}

	public String getDtAdmissaoFucaoPPP7() {
		return dtAdmissaoFucaoPPP7;
	}

	public void setDtAdmissaoFucaoPPP7(String dtAdmissaoFucaoPPP7) {
		this.dtAdmissaoFucaoPPP7 = dtAdmissaoFucaoPPP7;
	}

	public String getDtFimFincaoFuncaoPPP7() {
		return dtFimFincaoFuncaoPPP7;
	}

	public void setDtFimFincaoFuncaoPPP7(String dtFimFincaoFuncaoPPP7) {
		this.dtFimFincaoFuncaoPPP7 = dtFimFincaoFuncaoPPP7;
	}

	public String getDsFuncaoFuncaoPPP7() {
		return dsFuncaoFuncaoPPP7;
	}

	public void setDsFuncaoFuncaoPPP7(String dsFuncaoFuncaoPPP7) {
		this.dsFuncaoFuncaoPPP7 = dsFuncaoFuncaoPPP7;
	}

	public String getDsCBOFuncaoPPP7() {
		return dsCBOFuncaoPPP7;
	}

	public void setDsCBOFuncaoPPP7(String dsCBOFuncaoPPP7) {
		this.dsCBOFuncaoPPP7 = dsCBOFuncaoPPP7;
	}

	public String getDsSetorFuncaoPPP7() {
		return dsSetorFuncaoPPP7;
	}

	public void setDsSetorFuncaoPPP7(String dsSetorFuncaoPPP7) {
		this.dsSetorFuncaoPPP7 = dsSetorFuncaoPPP7;
	}

	public String getDtAdmissaoFucaoPPP8() {
		return dtAdmissaoFucaoPPP8;
	}

	public void setDtAdmissaoFucaoPPP8(String dtAdmissaoFucaoPPP8) {
		this.dtAdmissaoFucaoPPP8 = dtAdmissaoFucaoPPP8;
	}

	public String getDtFimFincaoFuncaoPPP8() {
		return dtFimFincaoFuncaoPPP8;
	}

	public void setDtFimFincaoFuncaoPPP8(String dtFimFincaoFuncaoPPP8) {
		this.dtFimFincaoFuncaoPPP8 = dtFimFincaoFuncaoPPP8;
	}

	public String getDsFuncaoFuncaoPPP8() {
		return dsFuncaoFuncaoPPP8;
	}

	public void setDsFuncaoFuncaoPPP8(String dsFuncaoFuncaoPPP8) {
		this.dsFuncaoFuncaoPPP8 = dsFuncaoFuncaoPPP8;
	}

	public String getDsCBOFuncaoPPP8() {
		return dsCBOFuncaoPPP8;
	}

	public void setDsCBOFuncaoPPP8(String dsCBOFuncaoPPP8) {
		this.dsCBOFuncaoPPP8 = dsCBOFuncaoPPP8;
	}

	public String getDsSetorFuncaoPPP8() {
		return dsSetorFuncaoPPP8;
	}

	public void setDsSetorFuncaoPPP8(String dsSetorFuncaoPPP8) {
		this.dsSetorFuncaoPPP8 = dsSetorFuncaoPPP8;
	}

	public String getDtAdmissaoFucaoPPP9() {
		return dtAdmissaoFucaoPPP9;
	}

	public void setDtAdmissaoFucaoPPP9(String dtAdmissaoFucaoPPP9) {
		this.dtAdmissaoFucaoPPP9 = dtAdmissaoFucaoPPP9;
	}

	public String getDtFimFincaoFuncaoPPP9() {
		return dtFimFincaoFuncaoPPP9;
	}

	public void setDtFimFincaoFuncaoPPP9(String dtFimFincaoFuncaoPPP9) {
		this.dtFimFincaoFuncaoPPP9 = dtFimFincaoFuncaoPPP9;
	}

	public String getDsFuncaoFuncaoPPP9() {
		return dsFuncaoFuncaoPPP9;
	}

	public void setDsFuncaoFuncaoPPP9(String dsFuncaoFuncaoPPP9) {
		this.dsFuncaoFuncaoPPP9 = dsFuncaoFuncaoPPP9;
	}

	public String getDsCBOFuncaoPPP9() {
		return dsCBOFuncaoPPP9;
	}

	public void setDsCBOFuncaoPPP9(String dsCBOFuncaoPPP9) {
		this.dsCBOFuncaoPPP9 = dsCBOFuncaoPPP9;
	}

	public String getDsSetorFuncaoPPP9() {
		return dsSetorFuncaoPPP9;
	}

	public void setDsSetorFuncaoPPP9(String dsSetorFuncaoPPP9) {
		this.dsSetorFuncaoPPP9 = dsSetorFuncaoPPP9;
	}

	public String getDtAdmissaoFucaoPPP10() {
		return dtAdmissaoFucaoPPP10;
	}

	public void setDtAdmissaoFucaoPPP10(String dtAdmissaoFucaoPPP10) {
		this.dtAdmissaoFucaoPPP10 = dtAdmissaoFucaoPPP10;
	}

	public String getDtFimFincaoFuncaoPPP10() {
		return dtFimFincaoFuncaoPPP10;
	}

	public void setDtFimFincaoFuncaoPPP10(String dtFimFincaoFuncaoPPP10) {
		this.dtFimFincaoFuncaoPPP10 = dtFimFincaoFuncaoPPP10;
	}

	public String getDsFuncaoFuncaoPPP10() {
		return dsFuncaoFuncaoPPP10;
	}

	public void setDsFuncaoFuncaoPPP10(String dsFuncaoFuncaoPPP10) {
		this.dsFuncaoFuncaoPPP10 = dsFuncaoFuncaoPPP10;
	}

	public String getDsCBOFuncaoPPP10() {
		return dsCBOFuncaoPPP10;
	}

	public void setDsCBOFuncaoPPP10(String dsCBOFuncaoPPP10) {
		this.dsCBOFuncaoPPP10 = dsCBOFuncaoPPP10;
	}

	public String getDsSetorFuncaoPPP10() {
		return dsSetorFuncaoPPP10;
	}

	public void setDsSetorFuncaoPPP10(String dsSetorFuncaoPPP10) {
		this.dsSetorFuncaoPPP10 = dsSetorFuncaoPPP10;
	}

	public String getDtAdmissaoFucaoPPP2() {
		return dtAdmissaoFucaoPPP2;
	}

	public void setDtAdmissaoFucaoPPP2(String dtAdmissaoFucaoPPP2) {
		this.dtAdmissaoFucaoPPP2 = dtAdmissaoFucaoPPP2;
	}

	public String getDtFimFincaoFuncaoPPP2() {
		return dtFimFincaoFuncaoPPP2;
	}

	public void setDtFimFincaoFuncaoPPP2(String dtFimFincaoFuncaoPPP2) {
		this.dtFimFincaoFuncaoPPP2 = dtFimFincaoFuncaoPPP2;
	}

	public String getDsFuncaoFuncaoPPP2() {
		return dsFuncaoFuncaoPPP2;
	}

	public void setDsFuncaoFuncaoPPP2(String dsFuncaoFuncaoPPP2) {
		this.dsFuncaoFuncaoPPP2 = dsFuncaoFuncaoPPP2;
	}

	public String getDsCBOFuncaoPPP2() {
		return dsCBOFuncaoPPP2;
	}

	public void setDsCBOFuncaoPPP2(String dsCBOFuncaoPPP2) {
		this.dsCBOFuncaoPPP2 = dsCBOFuncaoPPP2;
	}

	public String getDsSetorFuncaoPPP2() {
		return dsSetorFuncaoPPP2;
	}

	public void setDsSetorFuncaoPPP2(String dsSetorFuncaoPPP2) {
		this.dsSetorFuncaoPPP2 = dsSetorFuncaoPPP2;
	}

	public String getDtAdmissaoFucaoPPP3() {
		return dtAdmissaoFucaoPPP3;
	}

	public void setDtAdmissaoFucaoPPP3(String dtAdmissaoFucaoPPP3) {
		this.dtAdmissaoFucaoPPP3 = dtAdmissaoFucaoPPP3;
	}

	public String getDtFimFincaoFuncaoPPP3() {
		return dtFimFincaoFuncaoPPP3;
	}

	public void setDtFimFincaoFuncaoPPP3(String dtFimFincaoFuncaoPPP3) {
		this.dtFimFincaoFuncaoPPP3 = dtFimFincaoFuncaoPPP3;
	}

	public String getDsFuncaoFuncaoPPP3() {
		return dsFuncaoFuncaoPPP3;
	}

	public void setDsFuncaoFuncaoPPP3(String dsFuncaoFuncaoPPP3) {
		this.dsFuncaoFuncaoPPP3 = dsFuncaoFuncaoPPP3;
	}

	public String getDsCBOFuncaoPPP3() {
		return dsCBOFuncaoPPP3;
	}

	public void setDsCBOFuncaoPPP3(String dsCBOFuncaoPPP3) {
		this.dsCBOFuncaoPPP3 = dsCBOFuncaoPPP3;
	}

	public String getDsSetorFuncaoPPP3() {
		return dsSetorFuncaoPPP3;
	}

	public void setDsSetorFuncaoPPP3(String dsSetorFuncaoPPP3) {
		this.dsSetorFuncaoPPP3 = dsSetorFuncaoPPP3;
	}

	public String getDtAdmissaoFucaoPPP4() {
		return dtAdmissaoFucaoPPP4;
	}

	public void setDtAdmissaoFucaoPPP4(String dtAdmissaoFucaoPPP4) {
		this.dtAdmissaoFucaoPPP4 = dtAdmissaoFucaoPPP4;
	}

	public String getDtFimFincaoFuncaoPPP4() {
		return dtFimFincaoFuncaoPPP4;
	}

	public void setDtFimFincaoFuncaoPPP4(String dtFimFincaoFuncaoPPP4) {
		this.dtFimFincaoFuncaoPPP4 = dtFimFincaoFuncaoPPP4;
	}

	public String getDsFuncaoFuncaoPPP4() {
		return dsFuncaoFuncaoPPP4;
	}

	public void setDsFuncaoFuncaoPPP4(String dsFuncaoFuncaoPPP4) {
		this.dsFuncaoFuncaoPPP4 = dsFuncaoFuncaoPPP4;
	}

	public String getDsCBOFuncaoPPP4() {
		return dsCBOFuncaoPPP4;
	}

	public void setDsCBOFuncaoPPP4(String dsCBOFuncaoPPP4) {
		this.dsCBOFuncaoPPP4 = dsCBOFuncaoPPP4;
	}

	public String getDsSetorFuncaoPPP4() {
		return dsSetorFuncaoPPP4;
	}

	public void setDsSetorFuncaoPPP4(String dsSetorFuncaoPPP4) {
		this.dsSetorFuncaoPPP4 = dsSetorFuncaoPPP4;
	}

	public String getDtAdmissaoFucaoPPP5() {
		return dtAdmissaoFucaoPPP5;
	}

	public void setDtAdmissaoFucaoPPP5(String dtAdmissaoFucaoPPP5) {
		this.dtAdmissaoFucaoPPP5 = dtAdmissaoFucaoPPP5;
	}

	public String getDtFimFincaoFuncaoPPP5() {
		return dtFimFincaoFuncaoPPP5;
	}

	public void setDtFimFincaoFuncaoPPP5(String dtFimFincaoFuncaoPPP5) {
		this.dtFimFincaoFuncaoPPP5 = dtFimFincaoFuncaoPPP5;
	}

	public String getDsFuncaoFuncaoPPP5() {
		return dsFuncaoFuncaoPPP5;
	}

	public void setDsFuncaoFuncaoPPP5(String dsFuncaoFuncaoPPP5) {
		this.dsFuncaoFuncaoPPP5 = dsFuncaoFuncaoPPP5;
	}

	public String getDsCBOFuncaoPPP5() {
		return dsCBOFuncaoPPP5;
	}

	public void setDsCBOFuncaoPPP5(String dsCBOFuncaoPPP5) {
		this.dsCBOFuncaoPPP5 = dsCBOFuncaoPPP5;
	}

	public String getDsSetorFuncaoPPP5() {
		return dsSetorFuncaoPPP5;
	}

	public void setDsSetorFuncaoPPP5(String dsSetorFuncaoPPP5) {
		this.dsSetorFuncaoPPP5 = dsSetorFuncaoPPP5;
	}

	public String getDtDemissaoSelecionada() {
		return dtDemissaoSelecionada;
	}

	public void setDtDemissaoSelecionada(String dtDemissaoSelecionada) {
		this.dtDemissaoSelecionada = dtDemissaoSelecionada;
	}

	public String getDtAdmissaoRetorno() {
		return dtAdmissaoRetorno;
	}

	public void setDtAdmissaoRetorno(String dtAdmissaoRetorno) {
		this.dtAdmissaoRetorno = dtAdmissaoRetorno;
	}

	public String getMotivoRetorno() {
		return motivoRetorno;
	}

	public void setMotivoRetorno(String motivoRetorno) {
		this.motivoRetorno = motivoRetorno;
	}
	
	

}
