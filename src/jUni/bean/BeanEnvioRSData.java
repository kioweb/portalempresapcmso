package jUni.bean;

import java.util.LinkedList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import jPcmso.persistencia.geral.b.BeneficiarioCbo;
import jPcmso.persistencia.geral.b.BeneficiarioCboLocalizador;
import jPcmso.persistencia.geral.m.MensagemEnviada;
import jPcmso.persistencia.geral.m.MensagemParametro;
import jPcmso.persistencia.geral.p.PreCadastroCbo;
import jUni.bean.dados.DadosListaBeneficiarioCbo;
import jUni.bean.wsrsdata.TransacaoRSData;
import quatro.sql.ConexaoParaPoolDeConexoes;
import quatro.sql.PoolDeConexoes;
import quatro.sql.Query;
import quatro.util.QtData;

@SessionScoped
@ManagedBean
public class BeanEnvioRSData {
	
	private String msgAoUsuario;
	private Integer msgExibida = 0;
	private String ordenarPor;

	private PreCadastroCbo preCadastroCboSelecionado;
	private List<DadosListaBeneficiarioCbo> listagemPreCadastro;
	private static MensagemParametro mensagemParametro;
	private MensagemEnviada mensagemEnviada;
	
	public void inic() {
		if (msgAoUsuario != null) {
			if (msgExibida > 0) {
				msgAoUsuario = null;
				msgExibida = 0;
			}
			msgExibida++;
		}
	}

	public BeanEnvioRSData() {
		ordenarPor = "order by dt_envio_rsdata desc";
		listagemPreCadastro = new LinkedList<>();
		listagemPreCadastro = listaPreCadastro();
	}
	
	public void executaPesquisa() {
		listagemPreCadastro.clear();
		listagemPreCadastro = listaPreCadastro();
	}

	public void reenviarTransacao(DadosListaBeneficiarioCbo dados) {
		try {
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
			try {
				
				TransacaoRSData.enviaRSData(cnx, dados.getBeneficiarioCbo() );
				listagemPreCadastro.clear();
				listagemPreCadastro = listaPreCadastro();
				
			} finally {
				cnx.libera();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public List<DadosListaBeneficiarioCbo> listaPreCadastro() {
		List<DadosListaBeneficiarioCbo> lista = new LinkedList<>();

		try {
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

			try {

				Query q = new Query(cnx);
				q.addSQL("select * from pcmso.beneficiarios_cbo ");
				q.addSQL("where dt_exclusao is null");
				q.addSQL("and dt_envio_rsdata is not null");
				q.addSQL(ordenarPor);
				q.addSQL("limit 100");

				q.executeQuery();

				if (!q.isEmpty()) {
					while (!q.isAfterLast()) {

						BeneficiarioCbo beneficiarioCbo = new BeneficiarioCbo();
						BeneficiarioCboLocalizador.buscaCampos(beneficiarioCbo, q);

						DadosListaBeneficiarioCbo dados = new DadosListaBeneficiarioCbo();
						dados.setBeneficiarioCbo(beneficiarioCbo);
						dados.setCdMsgRetorno(q.getString("cd_msg_retorno"));
						dados.setDsTxtRetorno(q.getString("ds_txt_retorno"));
						dados.setTrnEnvioRsdata( q.getString("trn_envio_rsdata") );
						if (q.getTimestamp("dt_envio_rsdata") != null) {
							dados.setDtEnvioRsdata( new QtData( q.getTimestamp("dt_envio_rsdata") ) );
						}
						lista.add(dados);

						q.next();
					}
				}

			} finally {
				cnx.libera();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return lista;
	}

	

	public PreCadastroCbo getPreCadastroCboSelecionado() {
		return preCadastroCboSelecionado;
	}

	public void setPreCadastroCboSelecionado(PreCadastroCbo preCadastroCboSelecionado) {
		this.preCadastroCboSelecionado = preCadastroCboSelecionado;
	}

	public List<DadosListaBeneficiarioCbo> getListagemPreCadastro() {
		return listagemPreCadastro;
	}

	public void setListagemPreCadastro(List<DadosListaBeneficiarioCbo> listagemPreCadastro) {
		this.listagemPreCadastro = listagemPreCadastro;
	}

	public static MensagemParametro getMensagemParametro() {
		return mensagemParametro;
	}

	public static void setMensagemParametro(MensagemParametro mensagemParametro) {
		BeanEnvioRSData.mensagemParametro = mensagemParametro;
	}

	public MensagemEnviada getMensagemEnviada() {
		return mensagemEnviada;
	}

	public void setMensagemEnviada(MensagemEnviada mensagemEnviada) {
		this.mensagemEnviada = mensagemEnviada;
	}

	public String getMsgAoUsuario() {
		return msgAoUsuario;
	}

	public void setMsgAoUsuario(String msgAoUsuario) {
		this.msgAoUsuario = msgAoUsuario;
	}

	public Integer getMsgExibida() {
		return msgExibida;
	}

	public void setMsgExibida(Integer msgExibida) {
		this.msgExibida = msgExibida;
	}

	public String getOrdenarPor() {
		return ordenarPor;
	}

	public void setOrdenarPor(String ordenarPor) {
		this.ordenarPor = ordenarPor;
	}

}
