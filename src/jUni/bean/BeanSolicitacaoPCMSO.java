package jUni.bean;

import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import jPcmso.persistencia.geral.b.BeneficiarioCbo;
import jPcmso.persistencia.geral.b.BeneficiarioCboLocalizador;
import jPcmso.persistencia.geral.e.Empresa;
import jPcmso.persistencia.geral.e.EmpresaLocalizador;
import jPcmso.persistencia.geral.f.FiliaisEmpresa;
import jPcmso.persistencia.geral.f.FiliaisEmpresaLocalizador;
import jUni.bean.dados.DadosSolicitacaoPCMSO;
import jUni.bean.dados.DadosSolicitacaoPPPPCMSO;
import jUni.controleLogin.Usuario;
import jUni.relatorio.DadosRelatorioJasper;
import jUni.util.Util;
import quatro.sql.Conexao;
import quatro.sql.ConexaoParaPoolDeConexoes;
import quatro.sql.PoolDeConexoes;
import quatro.sql.Query;
import quatro.util.ParametrosSistema;
import quatro.util.QtData;

@ManagedBean
@SessionScoped
public class BeanSolicitacaoPCMSO {

	private Usuario usuarioLogado;
	private List<DadosSolicitacaoPCMSO> listagem;
	private List<DadosSolicitacaoPPPPCMSO> listagemPPP;
	private List<DadosSolicitacaoPPPPCMSO> listagemPPPConfirmada;
	private List<DadosSolicitacaoPCMSO> listagemPendencia;
	private DadosSolicitacaoPCMSO dadosSolicitacaoSelecionada;
	private DadosSolicitacaoPPPPCMSO dadosSolicitacaoPPPSelecionada;
	private String msgAoUsuario;
	private Integer msgExibida = 0;
	private String tpFiltro;
	private String cdEmpresa;
	private String tpSolicitacao;

	private String filtroCartao;
	private String filtroEmpresa;
	private String filtroNome;
	private String filtroTipo;
	private String filtroData;

	private String abaConfirmada;
	private String abaPendencia;

	public BeanSolicitacaoPCMSO() {

		usuarioLogado = jUni.controleLogin.Util.getDadosUsuarioLogado();
		listagem = new LinkedList<>();
		listagemPendencia = new LinkedList<>();
		listagemPPP = new LinkedList<>();
		listagemPPPConfirmada = new LinkedList<>();
		listagemPendencia = carregaListagemPendencia();
		listagem = carregaListagem();
		listagemPPP = carregaListagemPPP();
		listagemPPPConfirmada = carregaListagemPPPConfirmada();
		abaConfirmada = "";
		abaPendencia = "active";
	}

	public void inic() {
		if (usuarioLogado == null) {
			usuarioLogado = jUni.controleLogin.Util.getDadosUsuarioLogado();
		}
		if (msgAoUsuario != null) {
			if (msgExibida > 0) {
				msgAoUsuario = null;
				msgExibida = 0;
			}
			msgExibida++;
		}
		listagem.clear();
		listagem = carregaListagem();

		listagemPendencia.clear();
		listagemPendencia = carregaListagemPendencia();

		listagemPPP.clear();
		listagemPPP = carregaListagemPPP();

		listagemPPPConfirmada.clear();
		listagemPPPConfirmada = carregaListagemPPPConfirmada();
	}

	public void limpaMensagem() {
		msgAoUsuario = null;
		msgExibida = 0;
	}
	
	public String buscaNomeEmpresa( String cdEmpresa ) {
		try {
			
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
			try {
				
				Empresa empresa = EmpresaLocalizador.buscaEmpresa(cnx, cdEmpresa);
				if( empresa != null ) {
					return empresa.getDsEmpresa();
				}
				
			} finally {
				cnx.libera();
			}
			
		} catch ( Exception  e ) {
			e.printStackTrace();
		}
		return "";
	}
	
	public void atualizaFiltroPPP() {
		listagemPPP.clear();
		listagemPPP = carregaListagemPPP();
	}
	
	private List<DadosSolicitacaoPPPPCMSO> carregaListagemPPP() {
		List<DadosSolicitacaoPPPPCMSO> lista = new LinkedList<>();
		try {
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

			try {
				Query q = new Query(cnx);

				q.setSQL("select * from pcmso.solicitacoes_ppp_pcmso where dt_confirmacao is null ");
				
				if (filtroCartao != null) {
					if (!filtroCartao.equals("")) {
						q.addSQL( " and cd_beneficiario_cartao ilike :prmCdCartao ");
						q.setParameter("prmCdCartao", "%" + filtroCartao + "%");
					}
				}
				if (filtroEmpresa != null) {
					if (!filtroEmpresa.equals("")) {
						q.addSQL(" and cd_beneficiario_cartao ilike :prmCdEmpresa ");
						q.setParameter("prmCdEmpresa", filtroEmpresa + "%");
					}
				}
				if (filtroNome != null) {
					if (!filtroNome.equals("")) {
						q.addSQL(" and nm_beneficiario ilike :prmDsNome ");
						q.setParameter("prmDsNome", "%" + filtroNome + "%");
					}
				}
				if (filtroData != null) {
					if (!filtroData.equals("")) {
						q.addSQL(" and dt_solicitacao >= :prmDtSolicitacaoInic ");
						q.addSQL(" and dt_solicitacao <= :prmDtSolicitacaoFin ");
						QtData data = new QtData(filtroData);
						q.setParameter("prmDtSolicitacaoInic",data.getQtDataAsTimestamp() );
						data.deslocaDias(1);
						q.setParameter("prmDtSolicitacaoFin", data.getQtDataAsTimestamp() );
					}
				}
				
				q.addSQL("order by dt_solicitacao desc");
				
				if( usuarioLogado != null ) {
					if (usuarioLogado.getIdUsuario() != null && ( usuarioLogado.getIdUsuario().equals(162) || usuarioLogado.getIdUsuario().equals(293) ) ) {
						q.addSQL(" limit 100");
					}
				}

				q.executeQuery();

				if (!q.isEmpty()) {
					while (!q.isAfterLast()) {
						DadosSolicitacaoPPPPCMSO dados = new DadosSolicitacaoPPPPCMSO();

						dados.setIdSolicitacao(q.getInteger("id_solicitacao"));
						dados.setIdUsuarioSolicitou(q.getInteger("id_usuario_solicitou"));
						dados.setCdBeneficiarioCartao(q.getString("cd_beneficiario_cartao"));
						dados.setNmBeneficiario(q.getString("nm_beneficiario"));
						dados.setDtSolicitacao(
								q.getTimestamp("dt_solicitacao") != null ? new QtData(q.getTimestamp("dt_solicitacao"))
										: null);
						dados.setDtAdmissao(
								q.getTimestamp("dt_admissao") != null ? new QtData(q.getTimestamp("dt_admissao"))
										: null);
						dados.setDtDemissao(
								q.getTimestamp("dt_demissao") != null ? new QtData(q.getTimestamp("dt_demissao"))
										: null);
						dados.setDtNascimento(
								q.getTimestamp("dt_nascimento") != null ? new QtData(q.getTimestamp("dt_nascimento"))
										: null);
						dados.setNrNitPis(q.getString("nr_nit_pis"));
						dados.setNrCtps(q.getString("nr_ctps"));
						dados.setNrSerie(q.getString("nr_serie"));
						dados.setUfCtps(q.getString("uf_ctps"));
						dados.setSnAcidentetrabalho(q.getString("sn_acidente_trabalho"));
						dados.setDtAcidenteTrabalho(
								q.getTimestamp("dt_acidente") != null ? new QtData(q.getTimestamp("dt_acidente"))
										: null);
						dados.setNrCat(q.getString("nr_cat"));
						dados.setNmResponsavel(q.getString("nm_responsavel"));
						dados.setNrNitPisResponsavel(q.getString("nr_nit_pis_responsavel"));

						dados.setDtAdmissaoFuncao(q.getTimestamp("dt_admissao_funcao") != null
								? new QtData(q.getTimestamp("dt_admissao_funcao"))
								: null);
						dados.setDtFimFuncao(
								q.getTimestamp("dt_fim_funcao") != null ? new QtData(q.getTimestamp("dt_fim_funcao"))
										: null);
						dados.setDsFuncao(q.getString("ds_funcao"));
						dados.setDsCbo(q.getString("ds_cbo"));
						dados.setDsSetor(q.getString("ds_setor"));

						dados.setDtAdmissaoFucaoPPP2(q.getTimestamp("dt_admissao_funcao2") != null
								? new QtData(q.getTimestamp("dt_admissao_funcao2"))
								: null);
						dados.setDtFimFincaoFuncaoPPP2(
								q.getTimestamp("dt_fim_funcao2") != null ? new QtData(q.getTimestamp("dt_fim_funcao2"))
										: null);
						dados.setDsFuncaoFuncaoPPP2(q.getString("ds_funcao2"));
						dados.setDsCBOFuncaoPPP2(q.getString("ds_cbo2"));
						dados.setDsSetorFuncaoPPP2(q.getString("ds_setor2"));

						dados.setDtAdmissaoFucaoPPP3(q.getTimestamp("dt_admissao_funcao3") != null
								? new QtData(q.getTimestamp("dt_admissao_funcao3"))
								: null);
						dados.setDtFimFincaoFuncaoPPP3(
								q.getTimestamp("dt_fim_funcao3") != null ? new QtData(q.getTimestamp("dt_fim_funcao3"))
										: null);
						dados.setDsFuncaoFuncaoPPP3(q.getString("ds_funcao3"));
						dados.setDsCBOFuncaoPPP3(q.getString("ds_cbo3"));
						dados.setDsSetorFuncaoPPP3(q.getString("ds_setor3"));

						dados.setDtAdmissaoFucaoPPP4(q.getTimestamp("dt_admissao_funcao4") != null
								? new QtData(q.getTimestamp("dt_admissao_funcao4"))
								: null);
						dados.setDtFimFincaoFuncaoPPP4(
								q.getTimestamp("dt_fim_funcao4") != null ? new QtData(q.getTimestamp("dt_fim_funcao4"))
										: null);
						dados.setDsFuncaoFuncaoPPP4(q.getString("ds_funcao4"));
						dados.setDsCBOFuncaoPPP4(q.getString("ds_cbo4"));
						dados.setDsSetorFuncaoPPP4(q.getString("ds_setor4"));

						dados.setDtAdmissaoFucaoPPP5(q.getTimestamp("dt_admissao_funcao5") != null
								? new QtData(q.getTimestamp("dt_admissao_funcao5"))
								: null);
						dados.setDtFimFincaoFuncaoPPP5(
								q.getTimestamp("dt_fim_funcao5") != null ? new QtData(q.getTimestamp("dt_fim_funcao5"))
										: null);
						dados.setDsFuncaoFuncaoPPP5(q.getString("ds_funcao5"));
						dados.setDsCBOFuncaoPPP5(q.getString("ds_cbo5"));
						dados.setDsSetorFuncaoPPP5(q.getString("ds_setor5"));

						dados.setDtMudancaFuncao(q.getTimestamp("dt_mudanca_funcao") != null
								? new QtData(q.getTimestamp("dt_mudanca_funcao"))
								: null);
						dados.setDtMudancaFimFuncao(q.getTimestamp("dt_mudanca_fim_funcao") != null
								? new QtData(q.getTimestamp("dt_mudanca_fim_funcao"))
								: null);
						dados.setDsMudancaFuncao(q.getBString("ds_mudanca_funcao"));
						dados.setDsMudancaCbo(q.getString("ds_mudanca_cbo"));
						dados.setDsMudancaSetor(q.getString("ds_mudanca_setor"));
						dados.setDsObservacoes(q.getString("ds_observacoes"));
						dados.setDtConfirmacao(
								q.getTimestamp("dt_confirmacao") != null ? new QtData(q.getTimestamp("dt_confirmacao"))
										: null);
						dados.setIdUsuarioConfirmacao(q.getInteger("id_usuario_confirmacao"));

						lista.add(dados);
						q.next();
					}

				}

			} finally {
				cnx.libera();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lista;
	}

	public void processoRelatorioPendencias(DadosSolicitacaoPCMSO solicitacao ) {
		String nmRelatorio = null;

		try {
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
			try {
				if (usuarioLogado == null) {
					usuarioLogado = jUni.controleLogin.Util.getDadosUsuarioLogado();
				}
	
				nmRelatorio = "relSolicitacoesPCMSOPorItem";
	
				Hashtable<String, Object> filtros = new Hashtable<String, Object>();
	
				String rodape = "Emitido por " + usuarioLogado.getDsNomeUsuario();
				filtros.put("rodape", rodape);
				filtros.put("idSolicitacao", solicitacao.getIdSolicitacao());
				filtros.put("dsUsuarioSolicitou",  getDsNomeUsuario( solicitacao.getIdUsuarioSolicitou() ) );
	
				geraRelatorio(cnx, nmRelatorio, filtros, solicitacao );
	
			} finally {
				cnx.libera();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void processoRelatorioPendencias() {
		String nmRelatorio = null;

		try {
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
			try {

				if (usuarioLogado == null) {
					usuarioLogado = jUni.controleLogin.Util.getDadosUsuarioLogado();
				}
	
				nmRelatorio = "relSolicitacoesPCMSO";
	
				Hashtable<String, Object> filtros = new Hashtable<String, Object>();
	
				String rodape = "Emitido por " + usuarioLogado.getDsNomeUsuario();
				filtros.put("rodape", rodape);
	
				geraRelatorio(cnx, nmRelatorio, filtros, null );
	
			} finally {
				cnx.libera();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void geraRelatorio(Conexao cnx, String nomeRelatorio, Hashtable<String, Object> parametros, DadosSolicitacaoPCMSO solicitacao ) {

		try {

			String pathJasper = (String) ParametrosSistema.getInstance().getParametro("dirRelPCMSO");

			parametros.put("imagem", pathJasper + "imgs/logo.jpg");
			pathJasper += "solicitacaoPCMSO/";

			parametros.put("CaminhoPath", pathJasper);
			parametros.put("dataRelatorio", new QtData().toString());
			if( solicitacao != null ) {
				if( solicitacao.getIdFilial() != null ) {
					parametros.put("filial", solicitacao.getIdFilial() + " - " + solicitacao.getDsFilial() );
				}
				if( solicitacao.getCdEmpresa() != null ) {
					Empresa empresa = EmpresaLocalizador.buscaEmpresa(cnx, solicitacao.getCdEmpresa() );
					parametros.put("empresa", empresa.getCdEmpresa() + " - " + empresa.getDsEmpresa() );
				}
			}

			DadosRelatorioJasper relatorio = new DadosRelatorioJasper();

			relatorio.setBaseRelatorio(pathJasper + nomeRelatorio + ".jasper");
			relatorio.setParametros(parametros);

			Util.getSession().setAttribute("RelatorioPdf", relatorio);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void atualizaFiltroPPPConfirmadas() {
		listagemPPPConfirmada.clear();
		listagemPPPConfirmada = carregaListagemPPPConfirmada();
	}

	private List<DadosSolicitacaoPPPPCMSO> carregaListagemPPPConfirmada() {
		List<DadosSolicitacaoPPPPCMSO> lista = new LinkedList<>();
		try {
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

			try {
				Query q = new Query(cnx);

				q.setSQL("select * from pcmso.solicitacoes_ppp_pcmso where dt_confirmacao is not null ");
				
				if (filtroCartao != null) {
					if (!filtroCartao.equals("")) {
						q.addSQL( " and cd_beneficiario_cartao ilike :prmCdCartao ");
						q.setParameter("prmCdCartao", "%" + filtroCartao + "%");
					}
				}
				if (filtroEmpresa != null) {
					if (!filtroEmpresa.equals("")) {
						q.addSQL(" and cd_beneficiario_cartao ilike :prmCdEmpresa ");
						q.setParameter("prmCdEmpresa", filtroEmpresa + "%");
					}
				}
				if (filtroNome != null) {
					if (!filtroNome.equals("")) {
						q.addSQL(" and nm_beneficiario ilike :prmDsNome ");
						q.setParameter("prmDsNome", "%" + filtroNome + "%");
					}
				}
				if (filtroData != null) {
					if (!filtroData.equals("")) {
						q.addSQL(" and dt_solicitacao >= :prmDtSolicitacaoInic ");
						q.addSQL(" and dt_solicitacao <= :prmDtSolicitacaoFin ");
						QtData data = new QtData(filtroData);
						q.setParameter("prmDtSolicitacaoInic",data.getQtDataAsTimestamp() );
						data.deslocaDias(1);
						q.setParameter("prmDtSolicitacaoFin", data.getQtDataAsTimestamp() );
					}
				}
				
				q.addSQL("order by dt_solicitacao desc");
				
				if( usuarioLogado != null ) {
					if (usuarioLogado.getIdUsuario() != null && ( usuarioLogado.getIdUsuario().equals(162) || usuarioLogado.getIdUsuario().equals(293) ) ) {
						q.addSQL(" limit 100");
					}
				}

				q.executeQuery();

				if (!q.isEmpty()) {
					while (!q.isAfterLast()) {
						DadosSolicitacaoPPPPCMSO dados = new DadosSolicitacaoPPPPCMSO();

						dados.setIdSolicitacao(q.getInteger("id_solicitacao"));
						dados.setIdUsuarioSolicitou(q.getInteger("id_usuario_solicitou"));
						dados.setCdBeneficiarioCartao(q.getString("cd_beneficiario_cartao"));
						dados.setNmBeneficiario(q.getString("nm_beneficiario"));
						dados.setDtSolicitacao(
								q.getTimestamp("dt_solicitacao") != null ? new QtData(q.getTimestamp("dt_solicitacao"))
										: null);
						dados.setDtAdmissao(
								q.getTimestamp("dt_admissao") != null ? new QtData(q.getTimestamp("dt_admissao"))
										: null);
						dados.setDtDemissao(
								q.getTimestamp("dt_demissao") != null ? new QtData(q.getTimestamp("dt_demissao"))
										: null);
						dados.setDtNascimento(
								q.getTimestamp("dt_nascimento") != null ? new QtData(q.getTimestamp("dt_nascimento"))
										: null);
						dados.setNrNitPis(q.getString("nr_nit_pis"));
						dados.setNrCtps(q.getString("nr_ctps"));
						dados.setNrSerie(q.getString("nr_serie"));
						dados.setUfCtps(q.getString("uf_ctps"));
						dados.setSnAcidentetrabalho(q.getString("sn_acidente_trabalho"));
						dados.setDtAcidenteTrabalho(
								q.getTimestamp("dt_acidente") != null ? new QtData(q.getTimestamp("dt_acidente"))
										: null);
						dados.setNrCat(q.getString("nr_cat"));
						dados.setNmResponsavel(q.getString("nm_responsavel"));
						dados.setNrNitPisResponsavel(q.getString("nr_nit_pis_responsavel"));

						dados.setDtAdmissaoFuncao(q.getTimestamp("dt_admissao_funcao") != null
								? new QtData(q.getTimestamp("dt_admissao_funcao"))
								: null);
						dados.setDtFimFuncao(
								q.getTimestamp("dt_fim_funcao") != null ? new QtData(q.getTimestamp("dt_fim_funcao"))
										: null);
						dados.setDsFuncao(q.getString("ds_funcao"));
						dados.setDsCbo(q.getString("ds_cbo"));
						dados.setDsSetor(q.getString("ds_setor"));

						dados.setDtAdmissaoFucaoPPP2(q.getTimestamp("dt_admissao_funcao2") != null
								? new QtData(q.getTimestamp("dt_admissao_funcao2"))
								: null);
						dados.setDtFimFincaoFuncaoPPP2(
								q.getTimestamp("dt_fim_funcao2") != null ? new QtData(q.getTimestamp("dt_fim_funcao2"))
										: null);
						dados.setDsFuncaoFuncaoPPP2(q.getString("ds_funcao2"));
						dados.setDsCBOFuncaoPPP2(q.getString("ds_cbo2"));
						dados.setDsSetorFuncaoPPP2(q.getString("ds_setor2"));

						dados.setDtAdmissaoFucaoPPP3(q.getTimestamp("dt_admissao_funcao3") != null
								? new QtData(q.getTimestamp("dt_admissao_funcao3"))
								: null);
						dados.setDtFimFincaoFuncaoPPP3(
								q.getTimestamp("dt_fim_funcao3") != null ? new QtData(q.getTimestamp("dt_fim_funcao3"))
										: null);
						dados.setDsFuncaoFuncaoPPP3(q.getString("ds_funcao3"));
						dados.setDsCBOFuncaoPPP3(q.getString("ds_cbo3"));
						dados.setDsSetorFuncaoPPP3(q.getString("ds_setor3"));

						dados.setDtAdmissaoFucaoPPP4(q.getTimestamp("dt_admissao_funcao4") != null
								? new QtData(q.getTimestamp("dt_admissao_funcao4"))
								: null);
						dados.setDtFimFincaoFuncaoPPP4(
								q.getTimestamp("dt_fim_funcao4") != null ? new QtData(q.getTimestamp("dt_fim_funcao4"))
										: null);
						dados.setDsFuncaoFuncaoPPP4(q.getString("ds_funcao4"));
						dados.setDsCBOFuncaoPPP4(q.getString("ds_cbo4"));
						dados.setDsSetorFuncaoPPP4(q.getString("ds_setor4"));

						dados.setDtAdmissaoFucaoPPP5(q.getTimestamp("dt_admissao_funcao5") != null
								? new QtData(q.getTimestamp("dt_admissao_funcao5"))
								: null);
						dados.setDtFimFincaoFuncaoPPP5(
								q.getTimestamp("dt_fim_funcao5") != null ? new QtData(q.getTimestamp("dt_fim_funcao5"))
										: null);
						dados.setDsFuncaoFuncaoPPP5(q.getString("ds_funcao5"));
						dados.setDsCBOFuncaoPPP5(q.getString("ds_cbo5"));
						dados.setDsSetorFuncaoPPP5(q.getString("ds_setor5"));

						dados.setDtMudancaFuncao(q.getTimestamp("dt_mudanca_funcao") != null
								? new QtData(q.getTimestamp("dt_mudanca_funcao"))
								: null);
						dados.setDtMudancaFimFuncao(q.getTimestamp("dt_mudanca_fim_funcao") != null
								? new QtData(q.getTimestamp("dt_mudanca_fim_funcao"))
								: null);
						dados.setDsMudancaFuncao(q.getBString("ds_mudanca_funcao"));
						dados.setDsMudancaCbo(q.getString("ds_mudanca_cbo"));
						dados.setDsMudancaSetor(q.getString("ds_mudanca_setor"));
						dados.setDsObservacoes(q.getString("ds_observacoes"));
						dados.setDtConfirmacao(
								q.getTimestamp("dt_confirmacao") != null ? new QtData(q.getTimestamp("dt_confirmacao"))
										: null);
						dados.setIdUsuarioConfirmacao(q.getInteger("id_usuario_confirmacao"));

						lista.add(dados);
						q.next();
					}

				}

			} finally {
				cnx.libera();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lista;
	}

	public void selecionaPendencias() {
		abaConfirmada = "";
		abaPendencia = "active";
		filtroCartao = null;
		filtroEmpresa  = null;
		filtroNome = null;
		filtroTipo = null;
		filtroData = null;
	}

	public void selecionaConfirmadas() {
		abaConfirmada = "active";
		abaPendencia = "";
		filtroCartao = null;
		filtroEmpresa  = null;
		filtroNome = null;
		filtroTipo = null;
		filtroData = null;
	}
	
	public void atualizaFiltroPendencias() {
		listagemPendencia.clear();
		listagemPendencia = carregaListagemPendencia();
	}
	
	public List<DadosSolicitacaoPCMSO> carregaListagemPendencia() {
		List<DadosSolicitacaoPCMSO> lista = new LinkedList<>();
		try {
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

			try {
				Query q = new Query(cnx);

				q.setSQL("select * from pcmso.solicitacoes_pcmso where dt_confirmacao is null ");
				
				// se usu�rio luciont
				if ( usuarioLogado.getIdUsuario().equals(162) ) {
					q.addSQL(" limit 10");
					
					
				} else if ( usuarioLogado.getIdUsuario().equals( 2519 ) ) {
					q.addSQL(" limit 10");
					
					
				} 
				

				if (filtroCartao != null) {
					if (!filtroCartao.equals("")) {
						q.addSQL(
								" and cd_unimed || cd_empresa || cd_familia || cd_dependencia || cd_digito ilike :prmCdCartao ");
						q.setParameter("prmCdCartao", "%" + filtroCartao + "%");
					}
				}
				if (filtroEmpresa != null) {
					if (!filtroEmpresa.equals("")) {
						q.addSQL(" and cd_empresa ilike :prmCdEmpresa ");
						q.setParameter("prmCdEmpresa", "%" + filtroEmpresa + "%");
					}
				}
				if (filtroNome != null) {
					if (!filtroNome.equals("")) {
						q.addSQL(" and nm_beneficiario ilike :prmDsNome ");
						q.setParameter("prmDsNome", "%" + filtroNome + "%");
					}
				}
				if (filtroTipo != null) {
					if (!filtroTipo.equals("")) {
						q.addSQL(" and tp_solicitacao ilike :prmTipo ");
						q.setParameter("prmTipo", "%" + filtroTipo + "%");
					}
				}
				if (filtroData != null) {
					if (!filtroData.equals("")) {
						q.addSQL(" and dt_solicitacao >= :prmDtSolicitacaoInic ");
						q.addSQL(" and dt_solicitacao <= :prmDtSolicitacaoFin ");
						QtData data = new QtData(filtroData);
						q.setParameter("prmDtSolicitacaoInic",data.getQtDataAsTimestamp() );
						data.deslocaDias(1);
						q.setParameter("prmDtSolicitacaoFin", data.getQtDataAsTimestamp() );
					}
				}

				q.addSQL("order by dt_solicitacao desc");
				
				if( usuarioLogado != null ) {
					if (usuarioLogado.getIdUsuario() != null && ( usuarioLogado.getIdUsuario().equals(162) || usuarioLogado.getIdUsuario().equals(293) ) ) {
						q.addSQL(" limit 100");
					}
				}

				q.executeQuery();

				if (!q.isEmpty()) {
					while (!q.isAfterLast()) {

						DadosSolicitacaoPCMSO dados = new DadosSolicitacaoPCMSO();
						
						BeneficiarioCbo cbo = BeneficiarioCboLocalizador.buscaBeneficiarioCbo(cnx, q.getString("cd_empresa") + q.getString("cd_familia") + q.getString("cd_dependencia") );
						
						if( cbo != null ) {
							if( cbo.getIdFilial() != null ) {
								FiliaisEmpresa filial = FiliaisEmpresaLocalizador.buscaFiliaisEmpresa(cnx, cbo.getCdEmpresa(), cbo.getIdFilial() );
								if( filial != null ) {
									dados.setIdFilial(filial.getIdFilial());
									dados.setDsFilial(filial.getDsFilial());
								}
							}
						}
						
						dados.setIdSolicitacao(q.getInteger("id_solicitacao"));
						dados.setIdUsuarioSolicitou(q.getInteger("id_usuario_solicitou"));
						dados.setCdUnimed(q.getString("cd_unimed"));
						dados.setCdEmpresa(q.getString("cd_empresa"));
						dados.setCdFamilia(q.getString("cd_familia"));
						dados.setCdDependencia(q.getString("cd_dependencia"));
						dados.setCdDigito(q.getString("cd_digito"));
						dados.setNmBeneficiario(q.getString("nm_beneficiario"));
						dados.setDtSolicitacao(
								q.getTimestamp("dt_solicitacao") != null ? new QtData(q.getTimestamp("dt_solicitacao"))
										: null);
						dados.setDtConfirmacao(
								q.getTimestamp("dt_confirmacao") != null ? new QtData(q.getTimestamp("dt_confirmacao"))
										: null);
						dados.setTpSolicitacao(q.getString("tp_solicitacao"));
						dados.setDsObservacoes(q.getString("ds_observacoes"));

						lista.add(dados);

						q.next();
					}
				}
			} finally {
				cnx.libera();
			}

		} catch (Exception e) {
			e.printStackTrace();
			msgExibida = 0;
			msgAoUsuario = "Ops! " + e.getMessage();
		}
		return lista;
	}
	
	public void atualizaFiltro() {
		listagem.clear();
		listagem = carregaListagem();
	}

	public List<DadosSolicitacaoPCMSO> carregaListagem() {
		List<DadosSolicitacaoPCMSO> lista = new LinkedList<>();
		try {
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

			try {
				Query q = new Query(cnx);
				q.setSQL("select * from pcmso.solicitacoes_pcmso where dt_confirmacao is not null ");
				
				if (filtroCartao != null) {
					if (!filtroCartao.equals("")) {
						q.addSQL(
								" and cd_unimed || cd_empresa || cd_familia || cd_dependencia || cd_digito ilike :prmCdCartao ");
						q.setParameter("prmCdCartao", "%" + filtroCartao + "%");
					}
				}
				if (filtroEmpresa != null) {
					if (!filtroEmpresa.equals("")) {
						q.addSQL(" and cd_empresa ilike :prmCdEmpresa ");
						q.setParameter("prmCdEmpresa", "%" + filtroEmpresa + "%");
					}
				}
				if (filtroNome != null) {
					if (!filtroNome.equals("")) {
						q.addSQL(" and nm_beneficiario ilike :prmDsNome ");
						q.setParameter("prmDsNome", "%" + filtroNome + "%");
					}
				}
				if (filtroTipo != null) {
					if (!filtroTipo.equals("")) {
						q.addSQL(" and tp_solicitacao ilike :prmTipo ");
						q.setParameter("prmTipo", "%" + filtroTipo + "%");
					}
				}
				if (filtroData != null) {
					if (!filtroData.equals("")) {
						q.addSQL(" and dt_solicitacao >= :prmDtSolicitacaoInic ");
						q.addSQL(" and dt_solicitacao <= :prmDtSolicitacaoFin ");
						QtData data = new QtData(filtroData);
						q.setParameter("prmDtSolicitacaoInic",data.getQtDataAsTimestamp() );
						data.deslocaDias(1);
						q.setParameter("prmDtSolicitacaoFin", data.getQtDataAsTimestamp() );
					}
				}
				
				q.addSQL("order by dt_solicitacao desc");
				
				if( usuarioLogado != null ) {
					if (usuarioLogado.getIdUsuario() != null && ( usuarioLogado.getIdUsuario().equals(162) || usuarioLogado.getIdUsuario().equals(293) ) ) {
						q.addSQL(" limit 100");
					}
				}

				q.executeQuery();

				if (!q.isEmpty()) {
					while (!q.isAfterLast()) {

						DadosSolicitacaoPCMSO dados = new DadosSolicitacaoPCMSO();
						
						BeneficiarioCbo cbo = BeneficiarioCboLocalizador.buscaBeneficiarioCbo(cnx, q.getString("cd_empresa") + q.getString("cd_familia") + q.getString("cd_dependencia") );
						
						if( cbo != null ) {
							if( cbo.getIdFilial() != null ) {
								FiliaisEmpresa filial = FiliaisEmpresaLocalizador.buscaFiliaisEmpresa(cnx, cbo.getCdEmpresa(), cbo.getIdFilial() );
								if( filial != null ) {
									dados.setIdFilial(filial.getIdFilial());
									dados.setDsFilial(filial.getDsFilial());
								}
							}
						}
						
						dados.setIdSolicitacao(q.getInteger("id_solicitacao"));
						dados.setIdUsuarioSolicitou(q.getInteger("id_usuario_solicitou"));
						dados.setCdUnimed(q.getString("cd_unimed"));
						dados.setCdEmpresa(q.getString("cd_empresa"));
						dados.setCdFamilia(q.getString("cd_familia"));
						dados.setCdDependencia(q.getString("cd_dependencia"));
						dados.setCdDigito(q.getString("cd_digito"));
						dados.setNmBeneficiario(q.getString("nm_beneficiario"));
						dados.setDtSolicitacao( q.getTimestamp("dt_solicitacao") != null ? new QtData(q.getTimestamp("dt_solicitacao"))
										: null);
						dados.setDtConfirmacao(
								q.getTimestamp("dt_confirmacao") != null ? new QtData(q.getTimestamp("dt_confirmacao"))
										: null);
						dados.setTpSolicitacao(q.getString("tp_solicitacao"));
						dados.setDsObservacoes(q.getString("ds_observacoes"));

						lista.add(dados);

						q.next();
					}
				}
			} finally {
				cnx.libera();
			}

		} catch (Exception e) {
			e.printStackTrace();
			msgExibida = 0;
			msgAoUsuario = "Ops! " + e.getMessage();
		}
		return lista;
	}

	public void confirmarSolicitacao(DadosSolicitacaoPCMSO dados) {

		if (usuarioLogado == null) {
			usuarioLogado = jUni.controleLogin.Util.getDadosUsuarioLogado();
		}

		dados.setDtConfirmacao(new QtData());
		dados.setIdUsuarioConfirmacao(usuarioLogado.getIdUsuario());

		try {
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

			try {

				SolicitacaoPCMSO solicitacao = new SolicitacaoPCMSO(dados);
				solicitacao.confirmaSolicitacao(cnx);

				msgExibida = 0;
				msgAoUsuario = "Solicita��o de " + dados.getNmBeneficiario() + " confirmada.";

				Util.sendRedirect("resources/page/solicitacoes/listaSolicitacoes.jsf");

			} finally {
				cnx.libera();
			}

		} catch (Exception e) {
			msgExibida = 0;
			msgAoUsuario = "Ops! " + e.getMessage();
			Util.sendRedirect("resources/page/solicitacoes/listaSolicitacoes.jsf");
		}

	}

	public void confirmarSolicitacaoPPP(DadosSolicitacaoPPPPCMSO dados) {

		if (usuarioLogado == null) {
			usuarioLogado = jUni.controleLogin.Util.getDadosUsuarioLogado();
		}

		try {
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

			try {

				confirmaSolicitacaoPPP(cnx, dados.getIdSolicitacao(), usuarioLogado.getIdUsuario());

				msgExibida = 0;
				msgAoUsuario = "Solicita��o de PPP de " + dados.getNmBeneficiario() + " confirmada.";

				Util.sendRedirect("resources/page/solicitacoes/listaSolicitacoesPPP.jsf");

			} finally {
				cnx.libera();
			}

		} catch (Exception e) {
			msgExibida = 0;
			msgAoUsuario = "Ops! " + e.getMessage();
			Util.sendRedirect("resources/page/solicitacoes/listaSolicitacoesPPP.jsf");
		}

	}

	private void confirmaSolicitacaoPPP(Conexao cnx, Integer idSolicitacao, Integer idUsuario) throws Exception {

		try {
			Query q = new Query(cnx);
			q.setSQL("update pcmso.solicitacoes_ppp_pcmso set ");
			q.addSQL("dt_confirmacao = '" + new QtData().getQtDataAsTimestamp()
					+ "', id_usuario_confirmacao = :idUsuarioConfirmacao ");
			q.addSQL(" where id_solicitacao = :idSolicitacao ");

			q.setParameter("idSolicitacao", idSolicitacao);
			q.setParameter("idUsuarioConfirmacao", idUsuario);

			q.executeUpdate();

		} catch (Exception e) {
			throw new Exception("Erro ao incluir a Solicita��o do PCMSO.");
		}
	}

	public void visualizaSolicitacao(DadosSolicitacaoPCMSO dados) {
		dadosSolicitacaoSelecionada = dados;
		Util.sendRedirect("resources/page/solicitacoes/mntSolicitacao.jsf");
	}

	public void visualizaSolicitacaoPPP(DadosSolicitacaoPPPPCMSO dados) {
		dadosSolicitacaoPPPSelecionada = dados;
		Util.sendRedirect("resources/page/solicitacoes/mntSolicitacaoPPP.jsf");
	}

	public String getDsNomeUsuario(Integer idUsuario) {

		try {
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
			try {
				Query q = new Query(cnx);
				q.setSQL("select dsnomeusuario from jdesk.usuario where idusuario = :idUsuario ");
				q.setParameter("idUsuario", idUsuario);
				q.executeQuery();

				if (!q.isEmpty()) {
					return q.getString("dsnomeusuario");
				}

			} finally {
				cnx.libera();
			}

		} catch (Exception e) {
			e.printStackTrace();
			msgExibida = 0;
			msgAoUsuario = "Ops! " + e.getMessage();
			Util.sendRedirect("resources/page/solicitacoes/listaSolicitacoes.jsf");
		}

		return null;
	}

	public void cancelarPPP() {
		dadosSolicitacaoPPPSelecionada = new DadosSolicitacaoPPPPCMSO();
		Util.sendRedirect("resources/page/solicitacoes/listaSolicitacoesPPP.jsf");
	}

	public void cancelar() {
		dadosSolicitacaoSelecionada = new DadosSolicitacaoPCMSO();
		Util.sendRedirect("resources/page/solicitacoes/listaSolicitacoes.jsf");
	}

	public List<DadosSolicitacaoPCMSO> getListagem() {
		return listagem;
	}

	public void setListagem(List<DadosSolicitacaoPCMSO> listagem) {
		this.listagem = listagem;
	}

	public String getMsgAoUsuario() {
		return msgAoUsuario;
	}

	public void setMsgAoUsuario(String msgAoUsuario) {
		this.msgAoUsuario = msgAoUsuario;
	}

	public Integer getMsgExibida() {
		return msgExibida;
	}

	public void setMsgExibida(Integer msgExibida) {
		this.msgExibida = msgExibida;
	}

	public DadosSolicitacaoPCMSO getDadosSolicitacaoSelecionada() {
		return dadosSolicitacaoSelecionada;
	}

	public void setDadosSolicitacaoSelecionada(DadosSolicitacaoPCMSO dadosSolicitacaoSelecionada) {
		this.dadosSolicitacaoSelecionada = dadosSolicitacaoSelecionada;
	}

	public String getTpFiltro() {
		return tpFiltro;
	}

	public void setTpFiltro(String tpFiltro) {
		this.tpFiltro = tpFiltro;
		if (tpFiltro != null) {
			if (tpFiltro.equals("")) {
				tpFiltro = null;
			}
		}
		listagem.clear();
		listagem = carregaListagem();
		Util.sendRedirect("resources/page/solicitacoes/listaSolicitacoes.jsf");
	}

	public String getCdEmpresa() {
		return cdEmpresa;
	}

	public void setCdEmpresa(String cdEmpresa) {
		if (cdEmpresa != null) {
			if (cdEmpresa.equals("")) {
				cdEmpresa = null;
			}
		}
		this.cdEmpresa = cdEmpresa;
		listagem.clear();
		listagem = carregaListagem();
		Util.sendRedirect("resources/page/solicitacoes/listaSolicitacoes.jsf");

	}

	public String getTpSolicitacao() {
		return tpSolicitacao;
	}

	public void setTpSolicitacao(String tpSolicitacao) {
		if (tpSolicitacao != null) {
			if (tpSolicitacao.equals("")) {
				tpSolicitacao = null;
			}
		}
		this.tpSolicitacao = tpSolicitacao;
		listagem.clear();
		listagem = carregaListagem();
		Util.sendRedirect("resources/page/solicitacoes/listaSolicitacoes.jsf");
	}

	public String getAbaConfirmada() {
		return abaConfirmada;
	}

	public void setAbaConfirmada(String abaConfirmada) {
		this.abaConfirmada = abaConfirmada;
	}

	public String getAbaPendencia() {
		return abaPendencia;
	}

	public void setAbaPendencia(String abaPendencia) {
		this.abaPendencia = abaPendencia;
	}

	public List<DadosSolicitacaoPCMSO> getListagemPendencia() {
		return listagemPendencia;
	}

	public void setListagemPendencia(List<DadosSolicitacaoPCMSO> listagemPendencia) {
		this.listagemPendencia = listagemPendencia;
	}

	public List<DadosSolicitacaoPPPPCMSO> getListagemPPP() {
		return listagemPPP;
	}

	public void setListagemPPP(List<DadosSolicitacaoPPPPCMSO> listagemPPP) {
		this.listagemPPP = listagemPPP;
	}

	public List<DadosSolicitacaoPPPPCMSO> getListagemPPPConfirmada() {
		return listagemPPPConfirmada;
	}

	public void setListagemPPPConfirmada(List<DadosSolicitacaoPPPPCMSO> listagemPPPConfirmada) {
		this.listagemPPPConfirmada = listagemPPPConfirmada;
	}

	public DadosSolicitacaoPPPPCMSO getDadosSolicitacaoPPPSelecionada() {
		return dadosSolicitacaoPPPSelecionada;
	}

	public void setDadosSolicitacaoPPPSelecionada(DadosSolicitacaoPPPPCMSO dadosSolicitacaoPPPSelecionada) {
		this.dadosSolicitacaoPPPSelecionada = dadosSolicitacaoPPPSelecionada;
	}

	public String getFiltroCartao() {
		return filtroCartao;
	}

	public void setFiltroCartao(String filtroCartao) {
		this.filtroCartao = filtroCartao;
	}

	public String getFiltroEmpresa() {
		return filtroEmpresa;
	}

	public void setFiltroEmpresa(String filtroEmpresa) {
		this.filtroEmpresa = filtroEmpresa;
	}

	public String getFiltroNome() {
		return filtroNome;
	}

	public void setFiltroNome(String filtroNome) {
		this.filtroNome = filtroNome;
	}

	public String getFiltroTipo() {
		return filtroTipo;
	}

	public void setFiltroTipo(String filtroTipo) {
		this.filtroTipo = filtroTipo;
	}

	public String getFiltroData() {
		return filtroData;
	}

	public void setFiltroData(String filtroData) {
		this.filtroData = filtroData;
	}

}
