package jUni.bean.wsrsdata;

public class DadosRespostaRSData {

	private String cdMsg;
	private String retornoMsg;
	private String nrCnpjEmpresa;
	private String nomeEmpresagado;
	private String trnEnvioRSData;

	private String tpRetorno;
	private String txtDescricao;

	public String getCdMsg() {
		return cdMsg;
	}

	public void setCdMsg(String cdMsg) {
		this.cdMsg = cdMsg;
	}

	public String getRetornoMsg() {
		return retornoMsg;
	}

	public void setRetornoMsg(String retornoMsg) {
		this.retornoMsg = retornoMsg;
	}

	public String getNrCnpjEmpresa() {
		return nrCnpjEmpresa;
	}

	public void setNrCnpjEmpresa(String nrCnpjEmpresa) {
		this.nrCnpjEmpresa = nrCnpjEmpresa;
	}

	public String getNomeEmpresagado() {
		return nomeEmpresagado;
	}

	public void setNomeEmpresagado(String nomeEmpresagado) {
		this.nomeEmpresagado = nomeEmpresagado;
	}

	public String getTpRetorno() {
		return tpRetorno;
	}

	public void setTpRetorno(String tpRetorno) {
		this.tpRetorno = tpRetorno;
	}

	public String getTxtDescricao() {
		return txtDescricao;
	}

	public void setTxtDescricao(String txtDescricao) {
		this.txtDescricao = txtDescricao;
	}

	public String getTrnEnvioRSData() {
		return trnEnvioRSData;
	}

	public void setTrnEnviRSData(String trnEnvioRSData) {
		this.trnEnvioRSData = trnEnvioRSData;
	}

}
