package jUni.bean.wsrsdata;

public class DadosUsuario {

	private String userName;
	private String password;

	private String tpVerEmpregado;
	private String tpVerSetorCargo;
	private String forcarInclusao;

	private String dtTransferencia;
	private String idEmpresaDestino;
	private String codIntegracaoEmpresaDestino;
	private String nrCNPJEmpresaDestino;
	private String razaoSocialEmpresaDestino;
	private String denominacaoEmpresaDestino;
	private String forcarTransferencia;
	private String idEmpresa;
	private String codIntegracaoEmpresa;
	private String idEmpregado;
	private String nrCNPJEmpresa;
	private String razaoSocialEmpresa;
	private String denominacaoEmpresa;
	private String nomeEmpregado;
	private String dtNascimento;
	private String tpSexo;
	private String nrCTPS;
	private String nrSerieCTPS;
	private String dtEmiCTPS;
	private String ufEmiCTPS;
	private String nrIdentidade;
	private String orgaoExpedidorRG;
	private String dtEmiRG;
	private String ufEmiRG;
	private String nrNIT;
	private String nrCPF;
	private String nrMatricula;
	private String matriculaRh;
	private String categoriaTrabalhador;
	private String tpVinculo;
	private String BR_PDH;
	private String regimeRevezamento;
	private String dtAdmissao;
	private String dtDemissao;
	private String txObs;
	private String enderecoEmpregado;
	private String cidadeEmpregado;
	private String cidadeCodIbge;
	private String bairroEmpregado;
	private String estadoEmpregado;
	private String nrCEP;
	private String nrTelefone;
	private String remuneracaoMensal;
	private String nomeMae;
	private String tpFilPrevidencia;
	private String tpEstadoCivil;
	private String tpAposentado;
	private String nrEleitor;
	private String nrCNH;
	private String dtValCNH;
	private String cdRFID;
	private String cdBarras;
	private String grupoSanguineo;
	private String deficiencia;
	private String tpDeficiencia;

	private SetorCargo setorCargo;
	
	public DadosUsuario() {
		setorCargo = new SetorCargo();
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getTpVerEmpregado() {
		return tpVerEmpregado;
	}

	public void setTpVerEmpregado(String tpVerEmpregado) {
		this.tpVerEmpregado = tpVerEmpregado;
	}

	public String getTpVerSetorCargo() {
		return tpVerSetorCargo;
	}

	public void setTpVerSetorCargo(String tpVerSetorCargo) {
		this.tpVerSetorCargo = tpVerSetorCargo;
	}

	public String getForcarInclusao() {
		return forcarInclusao;
	}

	public void setForcarInclusao(String forcarInclusao) {
		this.forcarInclusao = forcarInclusao;
	}

	public String getDtTransferencia() {
		return dtTransferencia;
	}

	public void setDtTransferencia(String dtTransferencia) {
		this.dtTransferencia = dtTransferencia;
	}

	public String getIdEmpresaDestino() {
		return idEmpresaDestino;
	}

	public void setIdEmpresaDestino(String idEmpresaDestino) {
		this.idEmpresaDestino = idEmpresaDestino;
	}

	public String getCodIntegracaoEmpresaDestino() {
		return codIntegracaoEmpresaDestino;
	}

	public void setCodIntegracaoEmpresaDestino(String codIntegracaoEmpresaDestino) {
		this.codIntegracaoEmpresaDestino = codIntegracaoEmpresaDestino;
	}

	public String getNrCNPJEmpresaDestino() {
		return nrCNPJEmpresaDestino;
	}

	public void setNrCNPJEmpresaDestino(String nrCNPJEmpresaDestino) {
		this.nrCNPJEmpresaDestino = nrCNPJEmpresaDestino;
	}

	public String getRazaoSocialEmpresaDestino() {
		return razaoSocialEmpresaDestino;
	}

	public void setRazaoSocialEmpresaDestino(String razaoSocialEmpresaDestino) {
		this.razaoSocialEmpresaDestino = razaoSocialEmpresaDestino;
	}

	public String getDenominacaoEmpresaDestino() {
		return denominacaoEmpresaDestino;
	}

	public void setDenominacaoEmpresaDestino(String denominacaoEmpresaDestino) {
		this.denominacaoEmpresaDestino = denominacaoEmpresaDestino;
	}

	public String getForcarTransferencia() {
		return forcarTransferencia;
	}

	public void setForcarTransferencia(String forcarTransferencia) {
		this.forcarTransferencia = forcarTransferencia;
	}

	public String getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(String idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public String getCodIntegracaoEmpresa() {
		return codIntegracaoEmpresa;
	}

	public void setCodIntegracaoEmpresa(String codIntegracaoEmpresa) {
		this.codIntegracaoEmpresa = codIntegracaoEmpresa;
	}

	public String getIdEmpregado() {
		return idEmpregado;
	}

	public void setIdEmpregado(String idEmpregado) {
		this.idEmpregado = idEmpregado;
	}

	public String getNrCNPJEmpresa() {
		return nrCNPJEmpresa;
	}

	public void setNrCNPJEmpresa(String nrCNPJEmpresa) {
		this.nrCNPJEmpresa = nrCNPJEmpresa;
	}

	public String getRazaoSocialEmpresa() {
		return razaoSocialEmpresa;
	}

	public void setRazaoSocialEmpresa(String razaoSocialEmpresa) {
		this.razaoSocialEmpresa = razaoSocialEmpresa;
	}

	public String getDenominacaoEmpresa() {
		return denominacaoEmpresa;
	}

	public void setDenominacaoEmpresa(String denominacaoEmpresa) {
		this.denominacaoEmpresa = denominacaoEmpresa;
	}

	public String getNomeEmpregado() {
		return nomeEmpregado;
	}

	public void setNomeEmpregado(String nomeEmpregado) {
		this.nomeEmpregado = nomeEmpregado;
	}

	public String getDtNascimento() {
		return dtNascimento;
	}

	public void setDtNascimento(String dtNascimento) {
		this.dtNascimento = dtNascimento;
	}

	public String getTpSexo() {
		return tpSexo;
	}

	public void setTpSexo(String tpSexo) {
		this.tpSexo = tpSexo;
	}

	public String getNrCTPS() {
		return nrCTPS;
	}

	public void setNrCTPS(String nrCTPS) {
		this.nrCTPS = nrCTPS;
	}

	public String getNrSerieCTPS() {
		return nrSerieCTPS;
	}

	public void setNrSerieCTPS(String nrSerieCTPS) {
		this.nrSerieCTPS = nrSerieCTPS;
	}

	public String getDtEmiCTPS() {
		return dtEmiCTPS;
	}

	public void setDtEmiCTPS(String dtEmiCTPS) {
		this.dtEmiCTPS = dtEmiCTPS;
	}

	public String getUfEmiCTPS() {
		return ufEmiCTPS;
	}

	public void setUfEmiCTPS(String ufEmiCTPS) {
		this.ufEmiCTPS = ufEmiCTPS;
	}

	public String getNrIdentidade() {
		return nrIdentidade;
	}

	public void setNrIdentidade(String nrIdentidade) {
		this.nrIdentidade = nrIdentidade;
	}

	public String getOrgaoExpedidorRG() {
		return orgaoExpedidorRG;
	}

	public void setOrgaoExpedidorRG(String orgaoExpedidorRG) {
		this.orgaoExpedidorRG = orgaoExpedidorRG;
	}

	public String getDtEmiRG() {
		return dtEmiRG;
	}

	public void setDtEmiRG(String dtEmiRG) {
		this.dtEmiRG = dtEmiRG;
	}

	public String getUfEmiRG() {
		return ufEmiRG;
	}

	public void setUfEmiRG(String ufEmiRG) {
		this.ufEmiRG = ufEmiRG;
	}

	public String getNrNIT() {
		return nrNIT;
	}

	public void setNrNIT(String nrNIT) {
		this.nrNIT = nrNIT;
	}

	public String getNrCPF() {
		return nrCPF;
	}

	public void setNrCPF(String nrCPF) {
		this.nrCPF = nrCPF;
	}

	public String getNrMatricula() {
		return nrMatricula;
	}

	public void setNrMatricula(String nrMatricula) {
		this.nrMatricula = nrMatricula;
	}

	public String getMatriculaRh() {
		return matriculaRh;
	}

	public void setMatriculaRh(String matriculaRh) {
		this.matriculaRh = matriculaRh;
	}

	public String getCategoriaTrabalhador() {
		return categoriaTrabalhador;
	}

	public void setCategoriaTrabalhador(String categoriaTrabalhador) {
		this.categoriaTrabalhador = categoriaTrabalhador;
	}

	public String getTpVinculo() {
		return tpVinculo;
	}

	public void setTpVinculo(String tpVinculo) {
		this.tpVinculo = tpVinculo;
	}

	public String getBR_PDH() {
		return BR_PDH;
	}

	public void setBR_PDH(String bR_PDH) {
		BR_PDH = bR_PDH;
	}

	public String getRegimeRevezamento() {
		return regimeRevezamento;
	}

	public void setRegimeRevezamento(String regimeRevezamento) {
		this.regimeRevezamento = regimeRevezamento;
	}

	public String getDtAdmissao() {
		return dtAdmissao;
	}

	public void setDtAdmissao(String dtAdmissao) {
		this.dtAdmissao = dtAdmissao;
	}

	public String getDtDemissao() {
		return dtDemissao;
	}

	public void setDtDemissao(String dtDemissao) {
		this.dtDemissao = dtDemissao;
	}

	public String getTxObs() {
		return txObs;
	}

	public void setTxObs(String txObs) {
		this.txObs = txObs;
	}

	public String getEnderecoEmpregado() {
		return enderecoEmpregado;
	}

	public void setEnderecoEmpregado(String enderecoEmpregado) {
		this.enderecoEmpregado = enderecoEmpregado;
	}

	public String getCidadeEmpregado() {
		return cidadeEmpregado;
	}

	public void setCidadeEmpregado(String cidadeEmpregado) {
		this.cidadeEmpregado = cidadeEmpregado;
	}

	public String getCidadeCodIbge() {
		return cidadeCodIbge;
	}

	public void setCidadeCodIbge(String cidadeCodIbge) {
		this.cidadeCodIbge = cidadeCodIbge;
	}

	public String getBairroEmpregado() {
		return bairroEmpregado;
	}

	public void setBairroEmpregado(String bairroEmpregado) {
		this.bairroEmpregado = bairroEmpregado;
	}

	public String getEstadoEmpregado() {
		return estadoEmpregado;
	}

	public void setEstadoEmpregado(String estadoEmpregado) {
		this.estadoEmpregado = estadoEmpregado;
	}

	public String getNrCEP() {
		return nrCEP;
	}

	public void setNrCEP(String nrCEP) {
		this.nrCEP = nrCEP;
	}

	public String getNrTelefone() {
		return nrTelefone;
	}

	public void setNrTelefone(String nrTelefone) {
		this.nrTelefone = nrTelefone;
	}

	public String getRemuneracaoMensal() {
		return remuneracaoMensal;
	}

	public void setRemuneracaoMensal(String remuneracaoMensal) {
		this.remuneracaoMensal = remuneracaoMensal;
	}

	public String getNomeMae() {
		return nomeMae;
	}

	public void setNomeMae(String nomeMae) {
		this.nomeMae = nomeMae;
	}

	public String getTpFilPrevidencia() {
		return tpFilPrevidencia;
	}

	public void setTpFilPrevidencia(String tpFilPrevidencia) {
		this.tpFilPrevidencia = tpFilPrevidencia;
	}

	public String getTpEstadoCivil() {
		return tpEstadoCivil;
	}

	public void setTpEstadoCivil(String tpEstadoCivil) {
		this.tpEstadoCivil = tpEstadoCivil;
	}

	public String getTpAposentado() {
		return tpAposentado;
	}

	public void setTpAposentado(String tpAposentado) {
		this.tpAposentado = tpAposentado;
	}

	public String getNrEleitor() {
		return nrEleitor;
	}

	public void setNrEleitor(String nrEleitor) {
		this.nrEleitor = nrEleitor;
	}

	public String getNrCNH() {
		return nrCNH;
	}

	public void setNrCNH(String nrCNH) {
		this.nrCNH = nrCNH;
	}

	public String getDtValCNH() {
		return dtValCNH;
	}

	public void setDtValCNH(String dtValCNH) {
		this.dtValCNH = dtValCNH;
	}

	public String getCdRFID() {
		return cdRFID;
	}

	public void setCdRFID(String cdRFID) {
		this.cdRFID = cdRFID;
	}

	public String getCdBarras() {
		return cdBarras;
	}

	public void setCdBarras(String cdBarras) {
		this.cdBarras = cdBarras;
	}

	public String getGrupoSanguineo() {
		return grupoSanguineo;
	}

	public void setGrupoSanguineo(String grupoSanguineo) {
		this.grupoSanguineo = grupoSanguineo;
	}

	public String getDeficiencia() {
		return deficiencia;
	}

	public void setDeficiencia(String deficiencia) {
		this.deficiencia = deficiencia;
	}

	public String getTpDeficiencia() {
		return tpDeficiencia;
	}

	public void setTpDeficiencia(String tpDeficiencia) {
		this.tpDeficiencia = tpDeficiencia;
	}

	public SetorCargo getSetorCargo() {
		return setorCargo;
	}

	public void setSetorCargo(SetorCargo setorCargo) {
		this.setorCargo = setorCargo;
	}

}
