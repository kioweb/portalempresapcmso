package jUni.bean.wsrsdata;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.URL;
import java.net.URLConnection;
import java.security.KeyStore;
import java.security.Security;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.LinkedList;
import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentProducer;
import org.apache.http.entity.EntityTemplate;
import org.apache.http.impl.client.HttpClientBuilder;

import jPcmso.persistencia.geral.b.BeneficiarioCbo;
import jPcmso.persistencia.geral.b.BeneficiarioCboLocalizador;
import jPcmso.persistencia.geral.c.Cargo;
import jPcmso.persistencia.geral.c.CargoLocalizador;
import jPcmso.persistencia.geral.c.Cidade;
import jPcmso.persistencia.geral.c.CidadeLocalizador;
import jPcmso.persistencia.geral.e.Empresa;
import jPcmso.persistencia.geral.e.EmpresaLocalizador;
import jPcmso.persistencia.geral.f.FiliaisEmpresa;
import jPcmso.persistencia.geral.f.FiliaisEmpresaLocalizador;
import jPcmso.persistencia.geral.f.Funcao;
import jPcmso.persistencia.geral.f.FuncaoLocalizador;
import jPcmso.persistencia.geral.p.PreCadastroCbo;
import jPcmso.persistencia.geral.p.PreCadastroCboLocalizador;
import jPcmso.persistencia.geral.s.Setor;
import jPcmso.persistencia.geral.s.SetorLocalizador;
import jUni.biblioteca.util.Util;
import jUni.persistencia.geral.b.Beneficiario;
import jUni.persistencia.geral.b.BeneficiarioLocalizador;
import okhttp3.OkHttpClient;
import quatro.negocio.ErroDeNegocio;
import quatro.sql.Conexao;
import quatro.sql.ConexaoParaPoolDeConexoes;
import quatro.sql.QtSQLException;
import quatro.sql.Query;

public class TransacaoRSData {

	private static final String USER_AGENT = "Mozilla/5.0";
	
	public static void enviaRSData(Conexao cnx, BeneficiarioCbo b) throws ErroDeNegocio {
		try {

			PreCadastroCbo pre = PreCadastroCboLocalizador.buscaporBeneficiario(cnx, b.getCdBeneficiarioCartao());
//			BeneficiarioCbo benefCBO = BeneficiarioCboLocalizador.buscaBeneficiarioCbo(cnx, b.getCdBeneficiarioCartao() );
			
			Beneficiario beneficiario = BeneficiarioLocalizador.buscaporCodigoCartao( cnx, b.getCdBeneficiarioCartao() );
//			Empresa empresa = EmpresaLocalizador.buscaEmpresa(cnx, b.getCdEmpresa());

			DadosUsuario dados = new DadosUsuario();
			dados.setUserName("integrador.prd@unimed-uau.com.br");
			dados.setPassword("fb82367ca705634db5ff2e896a7d0334");

			dados.setTpVerEmpregado("MATRICULA");
			dados.setTpVerSetorCargo("NOME");
			
			dados.setIdEmpresaDestino( null );
			dados.setCodIntegracaoEmpresaDestino( null );
			dados.setNrCNPJEmpresaDestino( null );
			
			dados.setCodIntegracaoEmpresa("");
			if( beneficiario != null && beneficiario.getIdFilial() != null ) {
				dados.setCodIntegracaoEmpresa( getCnpjFilial( cnx, beneficiario.getCdEmpresa(), beneficiario.getIdFilial() ) );
			} 
			
			dados.setIdEmpresa( null );
			dados.setIdEmpregado("");
			dados.setNrCNPJEmpresa( getCnpjEmpresa( cnx, b ) );
			dados.setDenominacaoEmpresa(null);
			dados.setNomeEmpregado(b.getDsNome() != null ? b.getDsNome().trim() : "" );
			dados.setDtNascimento(b.getDtNascimento().toString());
			
			System.out.println("b.getTpSexo() " + b.getTpSexo() );
			
			dados.setTpSexo( b.getTpSexo().equals("M") ? "MASCULINO" : "FEMININO");
			
			// TODO - REVISAR CTPS
			dados.setNrCTPS(b.getNrCtps() != null ? b.getNrCtps().trim() : "" );
			dados.setNrSerieCTPS(b.getNrSerieUf() != null ? b.getNrSerieUf().trim() : "" );
			dados.setDtEmiCTPS(null);
			dados.setUfEmiCTPS(null);
			// TODO - REVISAR CTPS

			dados.setNrIdentidade( b.getCdRg() != null ? b.getCdRg().trim() : "" );
			if (pre != null) {
				dados.setOrgaoExpedidorRG(pre.getDsOrgaoEmissorRg() != null ? pre.getDsOrgaoEmissorRg().trim() : "");
			}
			dados.setUfEmiRG(null);
			
			if( beneficiario != null && beneficiario.getCdPisPasep() != null ) {
				dados.setNrNIT( beneficiario.getCdPisPasep() );
			} else {
				dados.setNrNIT( null );
			}
			
			dados.setNrCPF( b.getNrCpf() != null ? b.getNrCpf().trim() : "");

			// TODO - CONFIRMAR NR MATRICULA
			dados.setNrMatricula(b.getCdBeneficiarioCartao() + beneficiario.getCdDigito() );

			dados.setTpVinculo("Efetivo");
			dados.setBR_PDH(getBrPDH(b.getTpBrPdh()));
			dados.setRegimeRevezamento(null);
			dados.setDtAdmissao(b.getDtAdmissao() != null ? b.getDtAdmissao().toString() : b.getDtInclusao().toString() );
			dados.setDtDemissao(null);
			dados.setTxObs(null);
			
			if( beneficiario != null ) {
				if( beneficiario.getDsLogradouro() != null && beneficiario.getDsNumeroLogradouro() != null ) {
					dados.setEnderecoEmpregado(beneficiario.getDsLogradouro() + ", " + beneficiario.getDsNumeroLogradouro());
				}
				if (beneficiario.getIdCidadeResidencia() != null) {
					Cidade cidade = CidadeLocalizador.buscaCidade(cnx, beneficiario.getCdEstadoResidencia(), beneficiario.getIdCidadeResidencia() );
					if( cidade != null ) {
						dados.setCidadeEmpregado( cidade.getDsCidade().trim() );
						dados.setNrCEP( cidade.getCdCep() );
						dados.setCidadeCodIbge( cidade.getCdIbge() );
						dados.setEstadoEmpregado( cidade.getCdEstado() );
					}
					
				} else {
					jUni.persistencia.geral.e.Empresa empresa = jUni.persistencia.geral.e.EmpresaLocalizador.buscaEmpresa(cnx, b.getCdUnimed(), new Integer( b.getCdEmpresa() ) );
					if( empresa != null && empresa.getIdCidade() != null ) {
						Cidade cidade = CidadeLocalizador.buscaCidade(cnx, empresa.getCdEstado(), empresa.getIdEmpresa() );
						dados.setCidadeEmpregado( cidade.getDsCidade().trim() );
						dados.setNrCEP( cidade.getCdCep() );
						dados.setCidadeCodIbge( cidade.getCdIbge() );
						dados.setEstadoEmpregado( cidade.getCdEstado() );
					}
				}
				
			}
			
			if (pre != null) {
				if (pre.getDsLogradouro() != null && pre.getDsNumeroLogradouro() != null) {
					dados.setEnderecoEmpregado(pre.getDsLogradouro() + ", " + pre.getDsNumeroLogradouro());
				}
				if (pre.getIdCidade() != null) {
					Cidade cidade = CidadeLocalizador.buscaCidade(cnx, pre.getCdEstado(), pre.getIdCidade());
					if( cidade != null ) {
						dados.setCidadeEmpregado( cidade.getDsCidade().trim() );
						dados.setNrCEP( cidade.getCdCep() );
						dados.setCidadeCodIbge( cidade.getCdIbge() );
						dados.setEstadoEmpregado( cidade.getCdEstado() );
					}
					
				} else {
					jUni.persistencia.geral.e.Empresa empresa = jUni.persistencia.geral.e.EmpresaLocalizador.buscaEmpresa(cnx, b.getCdUnimed(), new Integer( b.getCdEmpresa() ) );
					if( empresa != null && empresa.getIdCidade() != null ) {
						Cidade cidade = CidadeLocalizador.buscaCidade(cnx, empresa.getCdEstado(), empresa.getIdEmpresa() );
						dados.setCidadeEmpregado( cidade.getDsCidade().trim() );
						dados.setNrCEP( cidade.getCdCep() );
						dados.setCidadeCodIbge( cidade.getCdIbge() );
						dados.setEstadoEmpregado( cidade.getCdEstado() );
					}
				}
				dados.setBairroEmpregado(pre.getDsBairro() != null ? pre.getDsBairro().trim() : "");
				dados.setNrTelefone(pre.getDsTelefoneContato() != null ? pre.getDsTelefoneContato().trim() : "");
			}
			dados.setRemuneracaoMensal(null);
			dados.setNomeMae( b.getDsNomeDaMae() != null ? b.getDsNomeDaMae().trim() : "");
			dados.setTpFilPrevidencia("EMPREGADO");
			dados.setTpEstadoCivil(getTpEstadoCivil(b.getTpEstadoCivil()));
			dados.setTpAposentado("NAO");
			dados.setNrEleitor(null);
			dados.setNrCNH(null);
			dados.setDtValCNH(null);
			dados.setCdRFID(null);
			dados.setCdBarras(null);
			dados.setGrupoSanguineo(null);
			dados.setTpDeficiencia(b.isSnPossuiDeficiencia() ? "SIM" : "NAO");

			SetorCargo setorCargo = new SetorCargo();
			setorCargo.setDtInicio(dados.getDtAdmissao() );
			setorCargo.setDtSaida("");
			setorCargo.setCdSetor("");
			setorCargo.setNomeSetor("");
			setorCargo.setCdSetorDesenvolvido("");
			setorCargo.setNomeSetorDesenvolvido("");
			setorCargo.setCdCargo("");
			setorCargo.setNomeCargo("");

			setorCargo.setCdCargoDesenvolvido("");
			setorCargo.setCargoDesenvolvido("");
			setorCargo.setCargoCBO("");
			setorCargo.setDescSumariaCargo("");
			setorCargo.setDescDetalhadaCargo("");

			dados.setSetorCargo(setorCargo);

			DadosRespostaRSData retorno = TransacaoRSData.enviaUsuario(dados);
			
			if (retorno != null) {
				if (retorno.getCdMsg().equals("500")) {
					Query q = new Query(cnx);
					q.setSQL("update pcmso.beneficiarios_cbo ");
					q.addSQL(
							"   set dt_envio_rsdata = current_timestamp::timestamp, cd_msg_retorno = :prmCdMensagem, ds_txt_retorno = :prmDsTxt, trn_envio_rsdata = :prmTrnRSData ");
					q.addSQL(" where cd_beneficiario_cartao = :prmCdCartao;");

					q.setParameter("prmCdMensagem", retorno.getCdMsg());
					q.setParameter("prmDsTxt", new String(retorno.getTxtDescricao().getBytes("iso-8859-1")));
					q.setParameter("prmCdCartao", b.getCdBeneficiarioCartao());
					q.setParameter("prmTrnRSData", retorno.getTrnEnvioRSData() );
					
					q.executeUpdate();
//					throw new ErroDeNegocio("Erro 500: " + retorno.getRetornoMsg() + " CNPJ Empresa: "
//							+ retorno.getNrCnpjEmpresa() + ", Beneficiario: " + retorno.getNomeEmpresagado()
//							+ ", Descrição: "
//							+ Util.trocaAcentuacao(new String(retorno.getTxtDescricao().getBytes("ISO-8859-1"))));
				} else {
					Query q = new Query(cnx);
					q.setSQL("update pcmso.beneficiarios_cbo ");
					q.addSQL(
							"   set dt_envio_rsdata = current_timestamp::timestamp, cd_msg_retorno = :prmCdMensagem, ds_txt_retorno = :prmDsTxt, trn_envio_rsdata = :prmTrnRSData ");
					q.addSQL(" where cd_beneficiario_cartao = :prmCdCartao;");

					q.setParameter("prmCdMensagem", retorno.getCdMsg());
					q.setParameter("prmDsTxt", new String(retorno.getTxtDescricao().getBytes("iso-8859-1")));
					q.setParameter("prmCdCartao", b.getCdBeneficiarioCartao() );
					q.setParameter("prmTrnRSData", retorno.getTrnEnvioRSData() );

					q.executeUpdate();

				}

			}

		} catch (Exception e) {
			e.printStackTrace();
			throw new ErroDeNegocio("Não enviado para RSData: " + e.getMessage());
		}

	}
	
	
	private String getCnpjEmpresa(ConexaoParaPoolDeConexoes cnx, BeneficiarioCbo dados ) {

		try {
			if( dados != null ) {
				Empresa empresa = EmpresaLocalizador.buscaEmpresa(cnx, dados.getCdEmpresa());
				
				if (empresa != null) {
					
					String retorno = empresa.getNrCpfRepresentante();
					
					if (empresa.getNrCnpj() != null && !empresa.getNrCnpj().trim().equals("")) {
						retorno = empresa.getNrCnpj();
					}
					
					jUni.persistencia.geral.e.Empresa empGeral = jUni.persistencia.geral.e.EmpresaLocalizador.buscaPorCodigosDeEmpresa( cnx,  new Integer( dados.getCdEmpresa() ) );
					if( empGeral != null && empGeral.getNrCeiSefip() != null && !empGeral.getNrCeiSefip().equals( "" ) ) {
						retorno = empGeral.getNrCeiSefip();
					}
					
					return retorno;
				}
			} else {
				return "";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}
	
	public static String getCnpjEmpresa(Conexao cnx, BeneficiarioCbo dados ) {

		try {
			if( dados != null ) {
				Empresa empresa = EmpresaLocalizador.buscaEmpresa(cnx, dados.getCdEmpresa());
				
				if (empresa != null) {
					
					String retorno = empresa.getNrCpfRepresentante();
					
					if (empresa.getNrCnpj() != null && !empresa.getNrCnpj().trim().equals("")) {
						retorno = empresa.getNrCnpj();
					}
					
					jUni.persistencia.geral.e.Empresa empGeral = jUni.persistencia.geral.e.EmpresaLocalizador.buscaPorCodigosDeEmpresa( cnx,  new Integer( dados.getCdEmpresa() ) );
					if( empGeral != null && empGeral.getNrCeiSefip() != null && !empGeral.getNrCeiSefip().equals( "" ) ) {
						retorno = empGeral.getNrCeiSefip();
					}
					
					return retorno;
				}
			} else {
				return "";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	private static String getCnpjFilial(Conexao cnx, String empresa, Integer idFilial) throws QtSQLException {
		FiliaisEmpresa filial = FiliaisEmpresaLocalizador.buscaFiliaisEmpresa(cnx, empresa, idFilial );
		if( filial != null ) {
			if( filial.getNrCnpj() != null ) {
				return filial.getNrCnpj();
			}
		}
		return "";
	}

	public static String getTpEstadoCivil(String tpEstadoCivil) {
		/**
		 * Tipo de Estado Civil (char(1)) (char(1))
		 * 
		 * S-Solteiro M-Casado (M-Married) W-Viuvo (W-Widow) D-Divorciado
		 * A-Apartado (Separado) U-Uni�o Est�vel
		 */
		if (tpEstadoCivil != null) {
			if (tpEstadoCivil.equals("S")) {
				return "SOLTEIRO";
			} else if (tpEstadoCivil.equals("M")) {
				return "CASADO";
			} else if (tpEstadoCivil.equals("W")) {
				return "VIUVO";
			} else if (tpEstadoCivil.equals("D")) {
				return "SEPARADO";
			} else {
				return "OUTRO";
			}
		} else {
			return "OUTRO";
		}
	}

	public static String getBrPDH(String tpBrPdh) {
		/**
		 * Tipo de BR PDH (char(1))
		 * 
		 * N-N�o aplicavel (NA) B-Beneficiario Reabilitado (BR) P-Portador de
		 * Deficiencia Habilitado (PDH)
		 */
		if (tpBrPdh != null) {
			if (tpBrPdh.equals("N")) {
				return "NA";
			} else if (tpBrPdh.equals("B")) {
				return "BR";
			} else if (tpBrPdh.equals("P")) {
				return "PDH";
			}
		}

		return "NA";
	}

	public static DadosRespostaRSData enviaUsuario(DadosUsuario dados ) throws Exception {

		StringBuilder str = new StringBuilder();

		str.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
		str.append(
				"    <soapenv:Envelope xmlns:emp=\"http://rsdata.com.br/ws/empregado\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">\n");
		str.append("        <soapenv:Header>\n");
		str.append(
				"            <wsse:Security xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\">\n");
		str.append("                <wsse:UsernameToken>\n");
		str.append("                    <wsse:Username>" + dados.getUserName() + "</wsse:Username>\n");
		
		str.append( "                    <wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText\">"
							+ dados.getPassword() + "</wsse:Password>\n");
		
		str.append("                </wsse:UsernameToken>\n");
		str.append("            </wsse:Security>\n");
		str.append("        </soapenv:Header>\n");
		str.append("    <soapenv:Body>\n");
		str.append("        <emp:insertEmpregadosRequest>\n");
		str.append("            <emp:rsdata versao=\"1\">\n");
		str.append("                <emp:config>\n");

		if (dados.getTpVerEmpregado() != null) {
			// <!--Optional:-->
			str.append(
					"                    <emp:tpVerEmpregado>" + dados.getTpVerEmpregado() + "</emp:tpVerEmpregado>\n");
		}
		if (dados.getTpVerSetorCargo() != null) {
			// <!--Optional:-->
			str.append("                    <emp:tpVerSetorCargo>" + dados.getTpVerSetorCargo() + "</emp:tpVerSetorCargo>\n");
		}
		if (dados.getForcarInclusao() != null) {
			// <!--Optional:-->
			str.append( "                    <emp:forcarInclusao>" + dados.getForcarInclusao() + "</emp:forcarInclusao>\n");
		}
		str.append("                </emp:config>\n");
		str.append("                <emp:empregados>\n");

		str.append("                    <emp:empregado>\n");
		if (dados.getDtTransferencia() != null) {
			// <!--Optional:-->
			str.append("                        <emp:dtTransferencia>"  + dados.getDtTransferencia() + "</emp:dtTransferencia>\n");
		}
		
		if( dados.getIdEmpresaDestino() != null ) {
			str.append("                        <emp:idEmpresaDestino>" + dados.getIdEmpresaDestino() + "</emp:idEmpresaDestino>\n");
		} else {
			str.append("                        <emp:idEmpresaDestino />\n");
		}
		if( dados.getCodIntegracaoEmpresaDestino() != null ) {
			str.append("                        <emp:codIntegracaoEmpresaDestino>" + dados.getCodIntegracaoEmpresaDestino() + "</emp:codIntegracaoEmpresaDestino>\n");
		} else {
			str.append("                        <emp:codIntegracaoEmpresaDestino /> \n");
		}
		
		if( dados.getNrCNPJEmpresaDestino() != null ) {
			str.append("                        <emp:nrCNPJEmpresaDestino>" + dados.getNrCNPJEmpresaDestino() + "</emp:nrCNPJEmpresaDestino>\n");
		} else {
			str.append("                        <emp:nrCNPJEmpresaDestino /> \n");
		}
			
		if (dados.getRazaoSocialEmpresaDestino() != null) {
			// <!--Optional:-->
			str.append("                        <emp:razaoSocialEmpresaDestino>" + dados.getRazaoSocialEmpresaDestino() + "</emp:razaoSocialEmpresaDestino>\n");
		}
		if (dados.getDenominacaoEmpresaDestino() != null) {
			// <!--Optional:-->
			str.append("                        <emp:denominacaoEmpresaDestino>" + dados.getDenominacaoEmpresaDestino() + "</emp:denominacaoEmpresaDestino>\n");
		}
		if (dados.getForcarTransferencia() != null) {
			// <!--Optional:-->
			str.append("                        <emp:forcarTransferencia>" + dados.getForcarTransferencia() + "</emp:forcarTransferencia>\n");
		}
		if( dados.getIdEmpresa() != null ) {
			str.append("                        <emp:idEmpresa>" + dados.getIdEmpresa() + "</emp:idEmpresa>\n");
		} else {
			str.append("                        <emp:idEmpresa />\n");
		}
		if( dados.getCodIntegracaoEmpresa() != null ) {
			str.append("                        <emp:codIntegracaoEmpresa>" + dados.getCodIntegracaoEmpresa() + "</emp:codIntegracaoEmpresa>\n");
		} else {
			str.append("                        <emp:codIntegracaoEmpresa /> \n");
		}
		if( dados.getIdEmpregado() != null ) {
			str.append("                        <emp:idEmpregado>" + dados.getIdEmpregado() + "</emp:idEmpregado>\n");
		} else {
			str.append("                        <emp:idEmpregado /> \n");
		}
		if( dados.getNrCNPJEmpresa() != null ) {
			str.append("                        <emp:nrCNPJEmpresa>" + dados.getNrCNPJEmpresa() + "</emp:nrCNPJEmpresa>\n");
		} else {
			str.append("                        <emp:nrCNPJEmpresa /> \n");
		}
		if (dados.getRazaoSocialEmpresa() != null) {
			// <!--Optional:-->
			str.append("                        <emp:razaoSocialEmpresa>" + dados.getRazaoSocialEmpresa() + "</emp:razaoSocialEmpresa>\n");
		}
		if (dados.getDenominacaoEmpresa() != null) {
			// <!--Optional:-->
			str.append("                        <emp:denominacaoEmpresa>" + dados.getDenominacaoEmpresa() + "</emp:denominacaoEmpresa>\n");
		}
			str.append("                        <emp:nomeEmpregado>" + dados.getNomeEmpregado() + "</emp:nomeEmpregado>\n");

		if (dados.getDtNascimento() != null) {
			// <!--Optional:-->
			str.append( "                        <emp:dtNascimento>" + dados.getDtNascimento() + "</emp:dtNascimento>\n");
		}
		if (dados.getTpSexo() != null) {
			// <!--Optional:-->
			str.append("                        <emp:tpSexo>" + dados.getTpSexo() + "</emp:tpSexo>\n");
		}
		if (dados.getNrCEP() != null) {
			// <!--Optional:-->
			str.append("                        <emp:nrCTPS>" + dados.getNrCTPS() + "</emp:nrCTPS>\n");
		}
		if (dados.getNrSerieCTPS() != null) {
			// <!--Optional:-->
			str.append("                        <emp:nrSerieCTPS>" + dados.getNrSerieCTPS() + "</emp:nrSerieCTPS>\n");
		}
		if (dados.getDtEmiCTPS() != null) {
			// <!--Optional:-->
			str.append("                        <emp:dtEmiCTPS>" + dados.getDtEmiCTPS() + "</emp:dtEmiCTPS>\n");
		}
		if (dados.getUfEmiCTPS() != null) {
			// <!--Optional:-->
			str.append("                        <emp:ufEmiCTPS>" + dados.getUfEmiCTPS() + "</emp:ufEmiCTPS>\n");
		}
		if (dados.getNrIdentidade() != null) {
			// <!--Optional:-->
			str.append(
					"                        <emp:nrIdentidade>" + dados.getNrIdentidade() + "</emp:nrIdentidade>\n");
		}
		if (dados.getOrgaoExpedidorRG() != null) {
			// <!--Optional:-->
			str.append("                        <emp:orgaoExpedidorRG>" + dados.getOrgaoExpedidorRG() + "</emp:orgaoExpedidorRG>\n");
		}
		if (dados.getDtEmiRG() != null) {
			// <!--Optional:-->
			str.append("                        <emp:dtEmiRG>" + dados.getDtEmiRG() + "</emp:dtEmiRG>\n");
		}
		if (dados.getUfEmiRG() != null) {
			// <!--Optional:-->
			str.append("                        <emp:ufEmiRG>" + dados.getUfEmiRG() + "</emp:ufEmiRG>\n");
		}
		if (dados.getNrNIT() != null) {
			// <!--Optional:-->
			str.append("                        <emp:nrNIT>" + dados.getNrNIT() + "</emp:nrNIT>\n");
		}
		if (dados.getNrCPF() != null) {
			// <!--Optional:-->
			str.append("                        <emp:nrCPF>" + dados.getNrCPF() + "</emp:nrCPF>\n");
		}
		if (dados.getNrMatricula() != null) {
			// <!--Optional:-->
			str.append("                        <emp:nrMatricula>" + dados.getNrMatricula() + "</emp:nrMatricula>\n");
		}
		if (dados.getMatriculaRh() != null) {
			// <!--Optional:-->
			str.append("                        <emp:matriculaRh>" + dados.getMatriculaRh() + "</emp:matriculaRh>\n");
		}
		if (dados.getCategoriaTrabalhador() != null) {
			// <!--Optional:-->
			str.append("                        <emp:categoriaTrabalhador>" + dados.getCategoriaTrabalhador() + "</emp:categoriaTrabalhador>\n");
		}
		if (dados.getTpVinculo() != null) {
			// <!--Optional:-->
			str.append("                        <emp:tpVinculo>" + dados.getTpVinculo() + "</emp:tpVinculo>\n");
		}
		if (dados.getBR_PDH() != null) {
			// <!--Optional:-->
			str.append("                        <emp:BR_PDH>" + dados.getBR_PDH() + "</emp:BR_PDH>\n");
		}
		if (dados.getRegimeRevezamento() != null) {
			// <!--Optional:-->
			str.append("                        <emp:regimeRevezamento>" + dados.getRegimeRevezamento() + "</emp:regimeRevezamento>\n");
		}
		if (dados.getDtAdmissao() != null) {
			// <!--Optional:-->
			str.append("                        <emp:dtAdmissao>" + dados.getDtAdmissao() + "</emp:dtAdmissao>\n");
		}
		if (dados.getDtDemissao() != null) {
			// <!--Optional:-->
			str.append("                        <emp:dtDemissao>" + dados.getDtDemissao() + "</emp:dtDemissao>\n");
		}
		if (dados.getTxObs() != null) {
			// <!--Optional:-->
			str.append("                        <emp:txObs>" + dados.getTxObs() + "</emp:txObs>\n");
		}
		if (dados.getEnderecoEmpregado() != null) {
			// <!--Optional:-->
			str.append("                        <emp:enderecoEmpregado>" + dados.getEnderecoEmpregado() + "</emp:enderecoEmpregado>\n");
		}
		if (dados.getCidadeEmpregado() != null) {
			// <!--Optional:-->
			str.append("                        <emp:cidadeEmpregado>" + dados.getCidadeEmpregado() + "</emp:cidadeEmpregado>\n");
		}
		if (dados.getCidadeCodIbge() != null) {
			// <!--Optional:-->
			str.append("                        <emp:cidadeCodIbge>" + dados.getCidadeCodIbge() + "</emp:cidadeCodIbge>\n");
		}
		if (dados.getBairroEmpregado() != null) {
			// <!--Optional:-->
			str.append("                        <emp:bairroEmpregado>" + dados.getBairroEmpregado() + "</emp:bairroEmpregado>\n");
		}
		if (dados.getEstadoEmpregado() != null) {
			// <!--Optional:-->
			str.append("                        <emp:estadoEmpregado>" + dados.getEstadoEmpregado() + "</emp:estadoEmpregado>\n");
		}
		if (dados.getNrCEP() != null) {
			// <!--Optional:-->
			str.append("                        <emp:nrCEP>" + dados.getNrCEP() + "</emp:nrCEP>\n");
		}
		if (dados.getNrTelefone() != null) {
			// <!--Optional:-->
			str.append("                        <emp:nrTelefone>" + dados.getNrTelefone() + "</emp:nrTelefone>\n");
		}
		if (dados.getRemuneracaoMensal() != null) {
			// <!--Optional:-->
			str.append("                        <emp:remuneracaoMensal>" + dados.getRemuneracaoMensal() + "</emp:remuneracaoMensal>\n");
		}
		if (dados.getNomeMae() != null) {
			// <!--Optional:-->
			str.append("                        <emp:nomeMae>" + dados.getNomeMae() + "</emp:nomeMae>\n");
		}
		if (dados.getTpFilPrevidencia() != null) {
			// <!--Optional:-->
			str.append("                        <emp:tpFilPrevidencia>" + dados.getTpFilPrevidencia() + "</emp:tpFilPrevidencia>\n");
		}
		if (dados.getTpEstadoCivil() != null) {
			// <!--Optional:-->
			str.append("                        <emp:tpEstadoCivil>" + dados.getTpEstadoCivil() + "</emp:tpEstadoCivil>\n");
		}
		if (dados.getTpAposentado() != null) {
			// <!--Optional:-->
			str.append( "                        <emp:tpAposentado>" + dados.getTpAposentado() + "</emp:tpAposentado>\n");
		}
		if (dados.getNrEleitor() != null) {
			// <!--Optional:-->
			str.append("                        <emp:nrEleitor>" + dados.getNrEleitor() + "</emp:nrEleitor>\n");
		}
		if (dados.getNrCNH() != null) {
			// <!--Optional:-->
			str.append("                        <emp:nrCNH>" + dados.getNrCNH() + "</emp:nrCNH>\n");
		}
		if (dados.getDtValCNH() != null) {
			// <!--Optional:-->
			str.append("                        <emp:dtValCNH>" + dados.getDtValCNH() + "</emp:dtValCNH>\n");
		}
		if (dados.getCdRFID() != null) {
			// <!--Optional:-->
			str.append("                        <emp:cdRFID>" + dados.getCdRFID() + "</emp:cdRFID>\n");
		}
		if (dados.getCdBarras() != null) {
			// <!--Optional:-->
			str.append("                        <emp:cdBarras>" + dados.getCdBarras() + "</emp:cdBarras>\n");
		}
		if (dados.getGrupoSanguineo() != null) {
			// <!--Optional:-->
			str.append("                        <emp:grupoSanguineo>" + dados.getGrupoSanguineo() + "</emp:grupoSanguineo>\n");
		}
		if (dados.getDeficiencia() != null) {
			// <!--Optional:-->
			str.append("                        <emp:deficiencia>" + dados.getDeficiencia() + "</emp:deficiencia>\n");
		}
		if (dados.getTpDeficiencia() != null) {
			// <!--Optional:-->
			str.append("                        <emp:tpDeficiencia>" + dados.getTpDeficiencia() + "</emp:tpDeficiencia>\n");
		}
		if (dados.getSetorCargo() != null) {
			str.append("                        <emp:setoresCargos>\n");
			// <!--1 or more repetitions:-->
			str.append("                          <emp:setorCargo>\n");
			if( dados.getSetorCargo().getDtInicio() != null ) {
				str.append("                                <emp:dtInicio>" + dados.getSetorCargo().getDtInicio() + "</emp:dtInicio>\n");
			}
			if (dados.getSetorCargo().getDtSaida() != null) {
				// <!--Optional:-->
				str.append("                                <emp:dtSaida>" + dados.getSetorCargo().getDtSaida() + "</emp:dtSaida>\n");
			}
			if (dados.getSetorCargo().getCdSetor() != null) {
				// <!--Optional:-->
				str.append("                                <emp:cdSetor>" + dados.getSetorCargo().getCdSetor() + "</emp:cdSetor>\n");
			}
			if( dados.getSetorCargo().getNomeCargo() != null ) {
				str.append("                                <emp:nomeSetor>" + dados.getSetorCargo().getNomeSetor() + "</emp:nomeSetor>\n");
			}
			if (dados.getSetorCargo().getCdSetorDesenvolvido() != null) {
				// <!--Optional:-->
				str.append("                                <emp:cdSetorDesenvolvido>" + dados.getSetorCargo().getCdSetorDesenvolvido() + "</emp:cdSetorDesenvolvido>\n");
			}
			if (dados.getSetorCargo().getNomeSetorDesenvolvido() != null) {
				// <!--Optional:-->
				str.append("                                <emp:nomeSetorDesenvolvido>" + dados.getSetorCargo().getNomeSetorDesenvolvido() + "</emp:nomeSetorDesenvolvido>\n");
			}
			if (dados.getSetorCargo().getCdCargo() != null) {
				// <!--Optional:-->
				str.append("                                <emp:cdCargo>" + dados.getSetorCargo().getCdCargo() + "</emp:cdCargo>\n");
			}
			if( dados.getSetorCargo().getNomeCargo() != null ) {
				str.append("                                <emp:nomeCargo>" + dados.getSetorCargo().getNomeCargo() + "</emp:nomeCargo>\n");
			}
			if (dados.getSetorCargo().getCdCargoDesenvolvido() != null) {
				// <!--Optional:-->
				str.append("                                <emp:cdCargoDesenvolvido>" + dados.getSetorCargo().getCdCargoDesenvolvido() + "</emp:cdCargoDesenvolvido>\n");
			}
			if (dados.getSetorCargo().getCargoDesenvolvido() != null) {
				// <!--Optional:-->
				str.append("                                <emp:cargoDesenvolvido>" + dados.getSetorCargo().getCargoDesenvolvido() + "</emp:cargoDesenvolvido>\n");
			}
			if (dados.getSetorCargo().getCargoCBO() != null) {
				// <!--Optional:-->
				str.append("                                <emp:cargoCBO>" + dados.getSetorCargo().getCargoCBO() + "</emp:cargoCBO>\n");
			}
			if (dados.getSetorCargo().getDescSumariaCargo() != null) {
				// <!--Optional:-->
				str.append("                                <emp:descSumariaCargo>" + dados.getSetorCargo().getDescSumariaCargo() + "</emp:descSumariaCargo>\n");
			}
			if (dados.getSetorCargo().getDescDetalhadaCargo() != null) {
				// <!--Optional:-->
				str.append("                                <emp:descDetalhadaCargo>" + dados.getSetorCargo().getDescDetalhadaCargo() + "</emp:descDetalhadaCargo>\n");
			}
			if (dados.getSetorCargo().getCdPosicaoTrabalho() != null) {
				// <!--Optional:-->
				str.append("                                <emp:cdPosicaoTrabalho>" + dados.getSetorCargo().getCdPosicaoTrabalho() + "</emp:cdPosicaoTrabalho>\n");
			}
			if (dados.getSetorCargo().getNomePosicaoTrabalho() != null) {
				// <!--Optional:-->
				str.append("                                <emp:nomePosicaoTrabalho>" + dados.getSetorCargo().getNomePosicaoTrabalho() + "</emp:nomePosicaoTrabalho>\n");
			}
			if (dados.getSetorCargo().getDescSumariaPosicaoTrabalho() != null) {
				// <!--Optional:-->
				str.append("                                <emp:descSumariaPosicaoTrabalho>" + dados.getSetorCargo().getDescSumariaPosicaoTrabalho() + "</emp:descSumariaPosicaoTrabalho>\n");
			}
			if (dados.getSetorCargo().getDescDetalhadaPosicaoTrabalho() != null) {
				// <!--Optional:-->
				str.append("                                <emp:descDetalhadaPosicaoTrabalho>" + dados.getSetorCargo().getDescDetalhadaPosicaoTrabalho() + "</emp:descDetalhadaPosicaoTrabalho>\n");
			}
			str.append("                            </emp:setorCargo>\n");
			str.append("                        </emp:setoresCargos>\n");
		}
		str.append("                    </emp:empregado>\n");
		str.append("                </emp:empregados>\n");
		str.append("            </emp:rsdata>\n");
		str.append("        </emp:insertEmpregadosRequest>\n");
		str.append("    </soapenv:Body>\n");
		str.append("</soapenv:Envelope>\n");

		SSLContext sc = nicSSLContext();
		
		System.out.println(str.toString());
		
		return TransacaoRSData.enviaXml(sc, str.toString());
		
	}
	
	
	/**
	 * Força a validação de todos os certificados digitais e Timeout
	 * @return
	 */
	protected OkHttpClient.Builder getUnsafeOkHttpClient() {
	    try {
	        // Create a trust manager that does not validate certificate chains
	        final TrustManager[] trustAllCerts = new TrustManager[]{
	                new X509TrustManager() {
	                    @Override
	                    public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) {
	                    }

	                    @Override
	                    public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) {
	                    }

	                    @Override
	                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
	                        return new java.security.cert.X509Certificate[]{};
	                    }
	                }
	        };

	        // Install the all-trusting trust manager
	        final SSLContext sslContext = SSLContext.getInstance("SSL");
	        sslContext.init(null, trustAllCerts, new java.security.SecureRandom());

	        // Create an ssl socket factory with our all-trusting manager
	        final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

	        OkHttpClient.Builder builder = new OkHttpClient.Builder();

	        builder.sslSocketFactory(sslSocketFactory, (X509TrustManager) trustAllCerts[0]);
	        builder.hostnameVerifier(new HostnameVerifier() {
	            @Override
	            public boolean verify(String hostname, SSLSession session) {
	                return true;
	            }
	        });
	        return builder;
	    } catch (Exception e) {
	        throw new RuntimeException(e);
	    }
	}
	
	private static SSLContext nicSSLContext() {
		String NM_ARQUIVO_KS = "/home/luciont/projetos/Argous/workspace/PoralEmpresaPCMSO/certificado/unimed.AU.jks";
		String SENHA = "unimed@1";

		try {
			InputStream in = new FileInputStream( new File( NM_ARQUIVO_KS ) );
			KeyStore ks = KeyStore.getInstance( KeyStore.getDefaultType() );
			ks.load( in, SENHA.toCharArray() );
			in.close();
			
			System.setProperty( "sun.security.ssl.allowUnsafeRenegotiation", "true" );
			System.setProperty( "java.protocol.handler.pkgs", "com.sun.net.ssl.internal.www.protocol" );
			System.setProperty( "https.protocols", "all" );
			
//			System.setProperty( "javax.net.ssl.trustStoreType", "PKCS12" );
			System.setProperty( "javax.net.ssl.trustStoreType", "JKS" );
			System.setProperty( "javax.net.ssl.trustStore", NM_ARQUIVO_KS );
			System.setProperty( "javax.net.ssl.trustStorePassword", SENHA );
			
			System.setProperty( "javax.net.ssl.keyStoreType", "PKCS12" );
//			System.setProperty( "javax.net.ssl.keyStoreType", "JKS" );
			
			System.setProperty( "javax.net.ssl.keyStore", NM_ARQUIVO_KS );
			System.setProperty( "javax.net.ssl.keyStorePassword", SENHA );
			
			SSLContext sc = SSLContext.getInstance( "SSL" );
			TrustManagerFactory tmf = TrustManagerFactory.getInstance( TrustManagerFactory.getDefaultAlgorithm() );
			tmf.init( ks );
			
			X509TrustManager defaultTrustManager = (X509TrustManager) tmf.getTrustManagers()[ 0 ];
			SavingTrustManager tm = new SavingTrustManager( defaultTrustManager );
			

			KeyManagerFactory kmf = KeyManagerFactory.getInstance( KeyManagerFactory.getDefaultAlgorithm() );
			kmf.init( ks, SENHA.toCharArray() );
			
			sc.init( kmf.getKeyManagers(), new TrustManager[] { tm }, null );
			
			return sc;
		} catch( Exception e ) {
			e.printStackTrace();
		}
		return null;
	}

	private static class SavingTrustManager implements X509TrustManager {

		private final X509TrustManager tm;
		private X509Certificate[] chain;

		public SavingTrustManager( X509TrustManager tm ) {
			this.tm = tm;
		}

		@Override
		public X509Certificate[] getAcceptedIssuers() {
			return chain;
		}

		public void checkClientTrusted( X509Certificate[] arg0, String arg1 ) throws CertificateException {
			throw new UnsupportedOperationException();
		}

		@Override
		public void checkServerTrusted( X509Certificate[] chain, String authType ) throws CertificateException {
			this.chain = chain;
			tm.checkServerTrusted( chain, authType );
		}

	}

	private static DadosRespostaRSData enviaXml(SSLContext sc,String trnXml ) throws Exception {
		String urlStr = "https://apps.rsdata.com.br/empregadoService-v2/soapws";
//		dadosUsuario.setUserName("userhomolog@unimeduau.com.br");
//		dadosUsuario.setPassword("fb82367ca705634db5ff2e896a7d0334");
		
		try {
			OkHttpClient client = new OkHttpClient();

//			Request request = new Request.Builder()
//			                     .url(urlStr)
//			                     .build();
//			
			ContentProducer cp = new TransacaoRSDataContentProducer( trnXml );
			HttpEntity entity = new EntityTemplate( cp );

			try {
				System.out.println( "Passo 1 " );
				URL url = new URL( urlStr );
				System.out.println( "Passo 2 " );
				URLConnection conec = url.openConnection();
				System.out.println( "Passo 3 " );
				conec.setRequestProperty( "charset", "ISO-8859-1" );
				conec.setConnectTimeout( 120000 );
				conec.setReadTimeout( 120000 );
				
				System.out.println( "Passo 4 " );
				url = conec.getURL();
				System.out.println( "Passo 5 " );
				HttpPost httpPost = new HttpPost( url.toURI() );
				System.out.println( "Passo 6 " );
				httpPost.setEntity( entity );
				System.out.println( "Passo 7 " + sc );

				HttpClient httpClient = HttpClientBuilder.create().build();
				System.out.println( "Passo 8 " + httpClient );
				
				HttpResponse response = httpClient.execute( httpPost );

			    System.out.println(response.toString());
				
				System.out.println( "Passo 9 " );
				if( response != null ) {
					System.out.println( "Passo 10 " );
					entity = response.getEntity();
				}
				System.out.println( "Passo 11 " );
				if( entity != null ) {
					System.out.println( "Passo 12 " );
					InputStream is = entity.getContent();
					System.out.println( "Passo 13 " );
					int ch;
					StringBuilder wrk = new StringBuilder();
					System.out.println( "Passo 14 " );

					while( ( ch = is.read() ) >= 0 ) {
						wrk.append( (char) ch );
					}
					System.out.println( "Passo 15 " + wrk.toString() );
					
					return codificaResposta( wrk.toString(), trnXml );
				} else {
					System.out.println( "ERRO - NAO RECEBI RESPOSTA DO SERVIDOR" );
					throw new Exception( "Não recebi resposta do servidor" );
				}

			} catch( Throwable e ) {
				e.printStackTrace();
			}
			
		} catch (Exception e ) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
		return null;
	}
	
	private static DadosRespostaRSData codificaResposta( String resposta, String trnXml ) throws Exception {

		System.out.println("------ INICIO DA RESPOSTA ------");
		System.out.println( resposta );
		System.out.println("------ FIM DA RESPOSTA ------");

		try {
			int posIni = 0;
			int posFim = 0;
			String tagInic = "<SOAP-ENV:Body>";
			String tagFim  = "</SOAP-ENV:Body>";
			
			if( resposta.contains( tagInic ) ) {
				posIni = resposta.indexOf( tagInic );
				posFim = resposta.indexOf( tagFim );
			} 
			
			if( posIni < 0 || posFim < posIni ) {
				throw new Exception( "Resposta de transacao invaida" );
			}
			String trn = resposta.substring( posIni + tagInic.length(), posFim );
			
			DadosRespostaRSData retorno = new DadosRespostaRSData();
			retorno.setTrnEnviRSData( trnXml );
			retorno.setCdMsg( 			getMensagemRetorno( "<ns2:cdMsg>", 			"</ns2:cdMsg>", 		trn ) );
			retorno.setRetornoMsg(		getMensagemRetorno( "<ns2:retornoMsg>", 	"</ns2:retornoMsg>", 	trn ) );
			retorno.setNrCnpjEmpresa(	getMensagemRetorno( "<ns2:nrCNPJEmpresa>", 	"</ns2:nrCNPJEmpresa>", trn ) );
			retorno.setNomeEmpresagado(	getMensagemRetorno( "<ns2:nomeEmpregado>", 	"</ns2:nomeEmpregado>",	trn ) );
			retorno.setTpRetorno(		getMensagemRetorno( "<ns2:tpRetorno>", 		"</ns2:tpRetorno>", 	trn ) );
			retorno.setTxtDescricao(	getMensagemRetorno( "<ns2:txDescricao>", 	"</ns2:txDescricao>", 	trn ) );
			
			System.out.println("cdMsg         " + retorno.getCdMsg());
			System.out.println("retornoMsg    " + retorno.getRetornoMsg());
			System.out.println("nrCNPJEmpresa " + retorno.getNrCnpjEmpresa());
			System.out.println("nomeEmpregado " + retorno.getNomeEmpresagado());
			System.out.println("tpRetorno     " + retorno.getTpRetorno());
			System.out.println("txDescricao   " + retorno.getTxtDescricao());
			
			return retorno;
			
		} catch( Exception e ) {
			throw new Exception( e );
		}
	}
	
	private static String getMensagemRetorno(String tag1, String tag2, String resposta ) {
		int posIni = 0;
		int posFim = 0;
		String tagInic = tag1;
		String tagFim  = tag2;
		
		if( resposta.contains( tagInic ) ) {
			posIni = resposta.indexOf( tagInic );
			posFim = resposta.indexOf( tagFim );
		} 
		
		if( posIni < 0 || posFim < posIni ) {
			System.out.println("Tag Não encontrada " + tag1 + " - " + tag2 );
		} else {
			return resposta.substring( posIni + tagInic.length(), posFim );
		}
		
		return null;
	}

	private static class TransacaoRSDataContentProducer implements ContentProducer {

		private String trnXml;

		public TransacaoRSDataContentProducer( String trnXml ) {
			this.trnXml = trnXml;
		}

		@Override
		public void writeTo( OutputStream outputStream ) throws IOException {

			Writer writer = new OutputStreamWriter( outputStream );
			writer.write( trnXml );
			writer.flush();
		}
	}
	

}
