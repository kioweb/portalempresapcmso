package jUni.bean.wsrsdata;

public class SetorCargo {

	private String dtInicio;
	private String dtSaida;
	private String cdSetor;
	private String nomeSetor;
	private String cdSetorDesenvolvido;
	private String nomeSetorDesenvolvido;
	private String cdCargo;
	private String nomeCargo;
	private String cdCargoDesenvolvido;
	private String cargoDesenvolvido;
	private String cargoCBO;
	private String descSumariaCargo;
	private String descDetalhadaCargo;
	private String cdPosicaoTrabalho;
	private String nomePosicaoTrabalho;
	private String descSumariaPosicaoTrabalho;
	private String descDetalhadaPosicaoTrabalho;

	public String getDtInicio() {
		return dtInicio;
	}

	public void setDtInicio(String dtInicio) {
		this.dtInicio = dtInicio;
	}

	public String getDtSaida() {
		return dtSaida;
	}

	public void setDtSaida(String dtSaida) {
		this.dtSaida = dtSaida;
	}

	public String getCdSetor() {
		return cdSetor;
	}

	public void setCdSetor(String cdSetor) {
		this.cdSetor = cdSetor;
	}

	public String getNomeSetor() {
		return nomeSetor;
	}

	public void setNomeSetor(String nomeSetor) {
		this.nomeSetor = nomeSetor;
	}

	public String getCdSetorDesenvolvido() {
		return cdSetorDesenvolvido;
	}

	public void setCdSetorDesenvolvido(String cdSetorDesenvolvido) {
		this.cdSetorDesenvolvido = cdSetorDesenvolvido;
	}

	public String getNomeSetorDesenvolvido() {
		return nomeSetorDesenvolvido;
	}

	public void setNomeSetorDesenvolvido(String nomeSetorDesenvolvido) {
		this.nomeSetorDesenvolvido = nomeSetorDesenvolvido;
	}

	public String getCdCargo() {
		return cdCargo;
	}

	public void setCdCargo(String cdCargo) {
		this.cdCargo = cdCargo;
	}

	public String getNomeCargo() {
		return nomeCargo;
	}

	public void setNomeCargo(String nomeCargo) {
		this.nomeCargo = nomeCargo;
	}

	public String getCdCargoDesenvolvido() {
		return cdCargoDesenvolvido;
	}

	public void setCdCargoDesenvolvido(String cdCargoDesenvolvido) {
		this.cdCargoDesenvolvido = cdCargoDesenvolvido;
	}

	public String getCargoDesenvolvido() {
		return cargoDesenvolvido;
	}

	public void setCargoDesenvolvido(String cargoDesenvolvido) {
		this.cargoDesenvolvido = cargoDesenvolvido;
	}

	public String getCargoCBO() {
		return cargoCBO;
	}

	public void setCargoCBO(String cargoCBO) {
		this.cargoCBO = cargoCBO;
	}

	public String getDescSumariaCargo() {
		return descSumariaCargo;
	}

	public void setDescSumariaCargo(String descSumariaCargo) {
		this.descSumariaCargo = descSumariaCargo;
	}

	public String getDescDetalhadaCargo() {
		return descDetalhadaCargo;
	}

	public void setDescDetalhadaCargo(String descDetalhadaCargo) {
		this.descDetalhadaCargo = descDetalhadaCargo;
	}

	public String getCdPosicaoTrabalho() {
		return cdPosicaoTrabalho;
	}

	public void setCdPosicaoTrabalho(String cdPosicaoTrabalho) {
		this.cdPosicaoTrabalho = cdPosicaoTrabalho;
	}

	public String getNomePosicaoTrabalho() {
		return nomePosicaoTrabalho;
	}

	public void setNomePosicaoTrabalho(String nomePosicaoTrabalho) {
		this.nomePosicaoTrabalho = nomePosicaoTrabalho;
	}

	public String getDescSumariaPosicaoTrabalho() {
		return descSumariaPosicaoTrabalho;
	}

	public void setDescSumariaPosicaoTrabalho(String descSumariaPosicaoTrabalho) {
		this.descSumariaPosicaoTrabalho = descSumariaPosicaoTrabalho;
	}

	public String getDescDetalhadaPosicaoTrabalho() {
		return descDetalhadaPosicaoTrabalho;
	}

	public void setDescDetalhadaPosicaoTrabalho(String descDetalhadaPosicaoTrabalho) {
		this.descDetalhadaPosicaoTrabalho = descDetalhadaPosicaoTrabalho;
	}

}
