package jUni.bean.dados;

import quatro.util.QtData;

public class DadosSolicitacaoPCMSO {

	private Integer idSolicitacao;
	private Integer idUsuarioSolicitou;
	private Integer idFilial;
	private String dsFilial;
	private String dsUsuarioSolicitou;
	private String cdUnimed;
	private String cdEmpresa;
	private String cdFamilia;
	private String cdDependencia;
	private String cdDigito;
	private String nmBeneficiario;
	private QtData dtSolicitacao;
	private String tpSolicitacao;
	private String dsObservacoes;
	private QtData dtConfirmacao;
	private Integer idUsuarioConfirmacao;

	public Integer getIdFilial() {
		return idFilial;
	}

	public void setIdFilial(Integer idFilial) {
		this.idFilial = idFilial;
	}

	public String getDsFilial() {
		return dsFilial;
	}

	public void setDsFilial(String dsFilial) {
		this.dsFilial = dsFilial;
	}

	public String getDsUsuarioSolicitou() {
		return dsUsuarioSolicitou;
	}

	public void setDsUsuarioSolicitou(String dsUsuarioSolicitou) {
		this.dsUsuarioSolicitou = dsUsuarioSolicitou;
	}

	public Integer getIdSolicitacao() {
		return idSolicitacao;
	}

	public void setIdSolicitacao(Integer idSolicitacao) {
		this.idSolicitacao = idSolicitacao;
	}

	public Integer getIdUsuarioSolicitou() {
		return idUsuarioSolicitou;
	}

	public void setIdUsuarioSolicitou(Integer idUsuarioSolicitou) {
		this.idUsuarioSolicitou = idUsuarioSolicitou;
	}

	public String getCdUnimed() {
		return cdUnimed;
	}

	public void setCdUnimed(String cdUnimed) {
		this.cdUnimed = cdUnimed;
	}

	public String getCdEmpresa() {
		return cdEmpresa;
	}

	public void setCdEmpresa(String cdEmpresa) {
		this.cdEmpresa = cdEmpresa;
	}

	public String getCdFamilia() {
		return cdFamilia;
	}

	public void setCdFamilia(String cdFamilia) {
		this.cdFamilia = cdFamilia;
	}

	public String getCdDependencia() {
		return cdDependencia;
	}

	public void setCdDependencia(String cdDependencia) {
		this.cdDependencia = cdDependencia;
	}

	public String getCdDigito() {
		return cdDigito;
	}

	public void setCdDigito(String cdDigito) {
		this.cdDigito = cdDigito;
	}

	public String getNmBeneficiario() {
		return nmBeneficiario;
	}

	public void setNmBeneficiario(String nmBeneficiario) {
		this.nmBeneficiario = nmBeneficiario;
	}

	public QtData getDtSolicitacao() {
		return dtSolicitacao;
	}

	public void setDtSolicitacao(QtData dtSolicitacao) {
		this.dtSolicitacao = dtSolicitacao;
	}

	public String getTpSolicitacao() {
		return tpSolicitacao;
	}

	public void setTpSolicitacao(String tpSolicitacao) {
		this.tpSolicitacao = tpSolicitacao;
	}

	public String getDsObservacoes() {
		return dsObservacoes;
	}

	public void setDsObservacoes(String dsObservacoes) {
		this.dsObservacoes = dsObservacoes;
	}

	public QtData getDtConfirmacao() {
		return dtConfirmacao;
	}

	public void setDtConfirmacao(QtData dtConfirmacao) {
		this.dtConfirmacao = dtConfirmacao;
	}

	public Integer getIdUsuarioConfirmacao() {
		return idUsuarioConfirmacao;
	}

	public void setIdUsuarioConfirmacao(Integer idUsuarioConfirmacao) {
		this.idUsuarioConfirmacao = idUsuarioConfirmacao;
	}

}
