package jUni.bean.dados;


public class ConfiguracaoBeneficiarios {
	
	private Integer identificacao;
	private Integer existenciaEmpregado;
	private Integer existenciaSetorCargo;
	
	public static String getLinha( ConfiguracaoBeneficiarios config ) {
		StringBuilder str = new StringBuilder();
		str.append(config.getIdentificacao());
		str.append(config.getExistenciaSetorCargo());
		str.append(config.getExistenciaEmpregado());
		return str.toString();
		
	}
	
	public Integer getIdentificacao() {
		return identificacao;
	}
	public void setIdentificacao(Integer identificacao) {
		this.identificacao = identificacao;
	}
	public Integer getExistenciaEmpregado() {
		return existenciaEmpregado;
	}
	public void setExistenciaEmpregado(Integer existenciaEmpregado) {
		this.existenciaEmpregado = existenciaEmpregado;
	}
	public Integer getExistenciaSetorCargo() {
		return existenciaSetorCargo;
	}
	public void setExistenciaSetorCargo(Integer existenciaSetorCargo) {
		this.existenciaSetorCargo = existenciaSetorCargo;
	}
	
}
