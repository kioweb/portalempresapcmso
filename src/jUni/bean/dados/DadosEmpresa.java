package jUni.bean.dados;

import jUni.biblioteca.util.Util;

public class DadosEmpresa {
	
	public static final int ALFANUMERICO_DIREITA = 0;
	public static final int NUMERICO_ESQUERDA = 1;

	public static String getLinha(DadosEmpresa dados) {

		StringBuilder sb = new StringBuilder();
		sb.append(dados.getIdentificacao());

		sb.append(dados.getNrCnpj());
		sb.append(dados.getDsRazaoSocial());
		sb.append(dados.getDsDenominacao());
		sb.append(dados.getDsEndereco());
		sb.append(dados.getDsBairro());
		sb.append(dados.getDsCidade());
		sb.append(dados.getCdCep());
		sb.append(dados.getDsUf());
		sb.append(dados.getNrDDD());
		sb.append(dados.getNrTelefone());
		sb.append(dados.getNrFax());
		sb.append(dados.getDsEmail());
		sb.append(dados.getDsHomePage());
		sb.append(dados.getDsContato());
		sb.append(dados.getNrCNAE());
		sb.append(dados.getIdGrauRisco());
		sb.append(dados.getNrInscricaoEstadual());
		sb.append(dados.getDsPessoa());

		return sb.toString();
	}

	private String identificacao;
	private String nrCnpj;
	private String dsRazaoSocial;
	private String dsDenominacao;
	private String dsEndereco;
	private String dsBairro;
	private String dsCidade;
	private String cdCep;
	private String dsUf;
	private String nrDDD;
	private String nrTelefone;
	private String nrFax;
	private String dsEmail;
	private String dsHomePage;
	private String dsContato;
	private String nrCNAE;
	private String idGrauRisco;
	private String nrInscricaoEstadual;
	private String dsPessoa;

	public String getIdentificacao() {
		return getAjustaRegistro(identificacao, 2, NUMERICO_ESQUERDA);
	}

	public String getNrCnpj() {
		return getAjustaRegistro(nrCnpj, 14, ALFANUMERICO_DIREITA);
	}

	public String getDsRazaoSocial() {
		return getAjustaRegistro(dsRazaoSocial, 50, ALFANUMERICO_DIREITA);
	}

	public String getDsDenominacao() {
		return getAjustaRegistro(dsDenominacao, 30, ALFANUMERICO_DIREITA);
	}

	public String getDsEndereco() {
		return getAjustaRegistro(dsEndereco, 50, ALFANUMERICO_DIREITA);
	}

	public String getDsBairro() {
		return getAjustaRegistro(dsBairro, 30, ALFANUMERICO_DIREITA);
	}

	public String getDsCidade() {
		return getAjustaRegistro(dsCidade, 30, ALFANUMERICO_DIREITA);
	}

	public String getCdCep() {
		return getAjustaRegistro(cdCep, 8, ALFANUMERICO_DIREITA);
	}

	public String getDsUf() {
		return getAjustaRegistro(dsUf, 2, ALFANUMERICO_DIREITA);
	}

	public String getNrDDD() {
		return getAjustaRegistro(nrDDD, 2, NUMERICO_ESQUERDA);
	}

	public String getNrTelefone() {
		return getAjustaRegistro(nrTelefone, 10, ALFANUMERICO_DIREITA);
	}

	public String getNrFax() {
		return getAjustaRegistro(nrFax, 10, ALFANUMERICO_DIREITA);
	}

	public String getDsEmail() {
		return getAjustaRegistro(dsEmail, 30, ALFANUMERICO_DIREITA);
	}

	public String getDsHomePage() {
		return getAjustaRegistro(dsHomePage, 30, ALFANUMERICO_DIREITA);
	}

	public String getDsContato() {
		return getAjustaRegistro(dsContato, 30, ALFANUMERICO_DIREITA);
	}

	public String getNrCNAE() {
		return getAjustaRegistro(nrCNAE, 10, NUMERICO_ESQUERDA);
	}

	public String getIdGrauRisco() {
		return getAjustaRegistro(idGrauRisco, 1, ALFANUMERICO_DIREITA);
	}

	public String getNrInscricaoEstadual() {
		return getAjustaRegistro(nrInscricaoEstadual, 15, NUMERICO_ESQUERDA);
	}

	public String getDsPessoa() {
		return getAjustaRegistro(dsPessoa, 1, NUMERICO_ESQUERDA);
	}

	private String getAjustaRegistro(String registro, Integer tamanho, Integer ladoBrancos ) {
		if (registro == null) {
			registro = "";
		} else {
			if (registro.length() > tamanho) {
				registro = registro.substring(0, tamanho);
			}
		}
		
		return Util.strSpace(registro.trim(), tamanho, ladoBrancos);
	}

	public void setIdentificacao(String identificacao) {
		this.identificacao = identificacao;
	}

	public void setNrCnpj(String nrCnpj) {
		this.nrCnpj = nrCnpj;
	}

	public void setDsRazaoSocial(String dsRazaoSocial) {
		this.dsRazaoSocial = dsRazaoSocial;
	}

	public void setDsDenominacao(String dsDenominacao) {
		this.dsDenominacao = dsDenominacao;
	}

	public void setDsEndereco(String dsEndereco) {
		this.dsEndereco = dsEndereco;
	}

	public void setDsBairro(String dsBairro) {
		this.dsBairro = dsBairro;
	}

	public void setDsCidade(String dsCidade) {
		this.dsCidade = dsCidade;
	}

	public void setCdCep(String cdCep) {
		this.cdCep = cdCep;
	}

	public void setDsUf(String dsUf) {
		this.dsUf = dsUf;
	}

	public void setNrDDD(String nrDDD) {
		this.nrDDD = nrDDD;
	}

	public void setNrTelefone(String nrTelefone) {
		this.nrTelefone = nrTelefone;
	}

	public void setNrFax(String nrFax) {
		this.nrFax = nrFax;
	}

	public void setDsEmail(String dsEmail) {
		this.dsEmail = dsEmail;
	}

	public void setDsHomePage(String dsHomePage) {
		this.dsHomePage = dsHomePage;
	}

	public void setDsContato(String dsContato) {
		this.dsContato = dsContato;
	}

	public void setNrCNAE(String nrCNAE) {
		this.nrCNAE = nrCNAE;
	}

	public void setIdGrauRisco(String idGrauRisco) {
		this.idGrauRisco = idGrauRisco;
	}

	public void setNrInscricaoEstadual(String nrInscricaoEstadual) {
		this.nrInscricaoEstadual = nrInscricaoEstadual;
	}

	public void setDsPessoa(String dsPessoa) {
		this.dsPessoa = dsPessoa;
	}

}
