package jUni.bean.dados;


import jUni.biblioteca.util.Util;

public class DadosExame {

	public static final int ALFANUMERICO_DIREITA = 0;
	public static final int NUMERICO_ESQUERDA = 1;

	private String cdIdentificacao;
	private String nrCnpj;
	private String cdIntegracaoEmpresa;
	private String nrMatricula;
	private String dsNome;
	private String nrCpf;
	private String cdNIT;
	private String dsExame;
	private String cdExame;
	private String dtExame;
	private String tpExigencia;
	private String tpExame;
	private String tpReferencia;
	private String tpInterpretacao;
	private String dsUnimedAtendimento;
	
	public static String getLinha(DadosExame dados) {
		StringBuilder sb = new StringBuilder();
		sb.append(dados.getCdIdentificacao());
		sb.append(dados.getNrCnpj());
		sb.append(dados.getCdIntegracaoEmpresa());
		sb.append(dados.getNrMatricula());
		sb.append(dados.getDsNome());
		sb.append(dados.getNrCpf());
		sb.append(dados.getCdNIT());
		sb.append(dados.getDsExame());
		sb.append(dados.getCdExame());
		sb.append(dados.getDtExame());
		sb.append(dados.getTpExigencia());
		sb.append(dados.getTpExame());
		sb.append(dados.getTpReferencia());
		sb.append(dados.getTpInterpretacao());
		sb.append(dados.getDsUnimedAtendimento());
		
		return sb.toString();
	}

	private static String getAjustaRegistro(String registro, Integer tamanho, Integer ladoBrancos) {
		if (registro == null) {
			registro = " ";
		} else {
			if (registro.length() > tamanho) {
				registro = registro.substring(0, tamanho);
			}
		}

		return Util.strSpace(registro.trim(), tamanho, ladoBrancos);
	}

	public String getCdIdentificacao() {
		return getAjustaRegistro(cdIdentificacao, 2, NUMERICO_ESQUERDA);
	}

	public String getNrCnpj() {
		return getAjustaRegistro(nrCnpj, 14, ALFANUMERICO_DIREITA);
	}

	public String getCdIntegracaoEmpresa() {
		return getAjustaRegistro(cdIntegracaoEmpresa, 101, ALFANUMERICO_DIREITA);
	}

	public String getNrMatricula() {
		return getAjustaRegistro(nrMatricula, 20, ALFANUMERICO_DIREITA);
	}

	public String getDsNome() {
		return getAjustaRegistro(dsNome, 70, ALFANUMERICO_DIREITA);
	}

	public String getNrCpf() {
		return getAjustaRegistro(nrCpf, 11, ALFANUMERICO_DIREITA);
	}

	public String getCdNIT() {
		return getAjustaRegistro(cdNIT, 15, ALFANUMERICO_DIREITA);
	}

	public String getDsExame() {
		return getAjustaRegistro(dsExame, 70, ALFANUMERICO_DIREITA);
	}

	public String getCdExame() {
		return getAjustaRegistro(cdExame, 100, ALFANUMERICO_DIREITA);
	}

	public String getDtExame() {
		return getAjustaRegistro(dtExame, 8, ALFANUMERICO_DIREITA);
	}

	public String getTpExigencia() {
		return getAjustaRegistro(tpExigencia, 1, NUMERICO_ESQUERDA);
	}

	public String getTpExame() {
		return getAjustaRegistro(tpExame, 1, ALFANUMERICO_DIREITA);
	}

	public String getTpReferencia() {
		return getAjustaRegistro(tpReferencia, 1, ALFANUMERICO_DIREITA);
	}

	public String getTpInterpretacao() {
		return getAjustaRegistro(tpInterpretacao, 3, ALFANUMERICO_DIREITA);
	}

	public String getDsUnimedAtendimento() {
		return getAjustaRegistro(dsUnimedAtendimento, 50, ALFANUMERICO_DIREITA);
	}

	public void setCdIdentificacao(String cdIdentificacao) {
		this.cdIdentificacao = cdIdentificacao;
	}

	public void setNrCnpj(String nrCnpj) {
		this.nrCnpj = nrCnpj;
	}

	public void setCdIntegracaoEmpresa(String cdIntegracaoEmpresa) {
		this.cdIntegracaoEmpresa = cdIntegracaoEmpresa;
	}

	public void setNrMatricula(String nrMatricula) {
		this.nrMatricula = nrMatricula;
	}

	public void setDsNome(String dsNome) {
		this.dsNome = dsNome;
	}

	public void setNrCpf(String nrCpf) {
		this.nrCpf = nrCpf;
	}

	public void setCdNIT(String cdNIT) {
		this.cdNIT = cdNIT;
	}

	public void setDsExame(String dsExame) {
		this.dsExame = dsExame;
	}

	public void setCdExame(String cdExame) {
		this.cdExame = cdExame;
	}

	public void setDtExame(String dtExame) {
		this.dtExame = dtExame;
	}

	public void setTpExigencia(String tpExigencia) {
		this.tpExigencia = tpExigencia;
	}

	public void setTpExame(String tpExame) {
		this.tpExame = tpExame;
	}

	public void setTpReferencia(String tpReferencia) {
		this.tpReferencia = tpReferencia;
	}

	public void setTpInterpretacao(String tpInterpretacao) {
		this.tpInterpretacao = tpInterpretacao;
	}

	public void setDsUnimedAtendimento(String dsUnimedAtendimento) {
		this.dsUnimedAtendimento = dsUnimedAtendimento;
	}

}
