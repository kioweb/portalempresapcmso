package jUni.bean.dados;


import jUni.biblioteca.util.Util;

public class DadosAtestados {

	public static final int ALFANUMERICO_DIREITA = 0;
	public static final int NUMERICO_ESQUERDA = 1;

	private String cdIdentificacao;
	private String nrCnpj;
	private String cdIntegracaoEmpresa;
	private String nrMatricula;
	private String dsNome;
	private String nrCpf;
	private String cdNIT;
	private String dtExame;
	private String tpExame;
	private String tpSubExame;
	private String dsMedico;
	private String cdCrm;
	private String tpAptidao;
	private String dsAtividadeComplementar;
	private String dsUnimedAtendimento;
	
	public static String getLinha(DadosAtestados dados) {
		StringBuilder sb = new StringBuilder();
		
		sb.append( dados.getCdIdentificacao() );
		sb.append( dados.getNrCnpj() );
		sb.append( dados.getCdIntegracaoEmpresa() );
		sb.append( dados.getNrMatricula() );
		sb.append( dados.getDsNome() );
		sb.append( dados.getNrCpf() );
		sb.append( dados.getCdNIT() );
		sb.append( dados.getDtExame() );
		sb.append( dados.getTpExame() );
		sb.append( dados.getTpSubExame() );
		sb.append( dados.getDsMedico() );
		sb.append( dados.getCdCrm() );
		sb.append( dados.getTpAptidao() );
		sb.append( dados.getDsAtividadeComplementar() );
		sb.append( dados.getDsUnimedAtendimento() );
		
		return sb.toString();
	}

	private static String getAjustaRegistro(String registro, Integer tamanho, Integer ladoBrancos) {
		if (registro == null) {
			registro = " ";
		} else {
			if (registro.length() > tamanho) {
				registro = registro.substring(0, tamanho);
			}
		}

		return Util.strSpace(registro.trim(), tamanho, ladoBrancos);
	}

	public String getCdIdentificacao() {
		return getAjustaRegistro(cdIdentificacao, 2, NUMERICO_ESQUERDA);
	}

	public String getNrCnpj() {
		return getAjustaRegistro(nrCnpj, 14, ALFANUMERICO_DIREITA);
	}

	public String getCdIntegracaoEmpresa() {
		return getAjustaRegistro(cdIntegracaoEmpresa, 101, ALFANUMERICO_DIREITA);
	}

	public String getNrMatricula() {
		return getAjustaRegistro(nrMatricula, 20, ALFANUMERICO_DIREITA);
	}

	public String getDsNome() {
		return getAjustaRegistro(dsNome, 70, ALFANUMERICO_DIREITA);
	}

	public String getNrCpf() {
		return getAjustaRegistro(nrCpf, 11, ALFANUMERICO_DIREITA);
	}

	public String getCdNIT() {
		return getAjustaRegistro(cdNIT, 15, ALFANUMERICO_DIREITA);
	}

	public String getDtExame() {
		return getAjustaRegistro(dtExame, 8, ALFANUMERICO_DIREITA);
	}

	public String getTpExame() {
		return getAjustaRegistro(tpExame, 1, ALFANUMERICO_DIREITA);
	}
	
	public String getTpSubExame() {
		return getAjustaRegistro( tpSubExame, 200, ALFANUMERICO_DIREITA );
	}
	
	public String getDsMedico() {
		return getAjustaRegistro( dsMedico, 50, ALFANUMERICO_DIREITA );
	}
	
	public String getCdCrm() {
		return getAjustaRegistro( cdCrm, 15, NUMERICO_ESQUERDA );
	}
	
	public String getTpAptidao() {
		return getAjustaRegistro( tpAptidao, 1, ALFANUMERICO_DIREITA );
	}
	
	public String getDsAtividadeComplementar() {
		return getAjustaRegistro( dsAtividadeComplementar, 200, ALFANUMERICO_DIREITA );
	}

	public String getDsUnimedAtendimento() {
		return getAjustaRegistro(dsUnimedAtendimento, 50, ALFANUMERICO_DIREITA);
	}

	public void setCdIdentificacao(String cdIdentificacao) {
		this.cdIdentificacao = cdIdentificacao;
	}

	public void setNrCnpj(String nrCnpj) {
		this.nrCnpj = nrCnpj;
	}

	public void setCdIntegracaoEmpresa(String cdIntegracaoEmpresa) {
		this.cdIntegracaoEmpresa = cdIntegracaoEmpresa;
	}

	public void setNrMatricula(String nrMatricula) {
		this.nrMatricula = nrMatricula;
	}

	public void setDsNome(String dsNome) {
		this.dsNome = dsNome;
	}

	public void setNrCpf(String nrCpf) {
		this.nrCpf = nrCpf;
	}

	public void setCdNIT(String cdNIT) {
		this.cdNIT = cdNIT;
	}

	public void setDtExame(String dtExame) {
		this.dtExame = dtExame;
	}

	public void setTpExame(String tpExame) {
		this.tpExame = tpExame;
	}

	public void setDsUnimedAtendimento(String dsUnimedAtendimento) {
		this.dsUnimedAtendimento = dsUnimedAtendimento;
	}

	public void setTpSubExame(String tpSubExame) {
		this.tpSubExame = tpSubExame;
	}

	public void setDsMedico(String dsMedico) {
		this.dsMedico = dsMedico;
	}

	public void setCdCrm(String cdCrm) {
		this.cdCrm = cdCrm;
	}

	public void setTpAptidao(String tpAptidao) {
		this.tpAptidao = tpAptidao;
	}

	public void setDsAtividadeComplementar(String dsAtividadeComplementar) {
		this.dsAtividadeComplementar = dsAtividadeComplementar;
	}
	
	

}

