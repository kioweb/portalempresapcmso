package jUni.bean.dados;

import jPcmso.persistencia.geral.b.BeneficiarioCbo;
import quatro.util.QtData;

public class DadosListaBeneficiarioCbo {

	private BeneficiarioCbo beneficiarioCbo;
	private QtData dtEnvioRsdata;
	private String cdMsgRetorno;
	private String dsTxtRetorno;
	private String trnEnvioRsdata;

	public BeneficiarioCbo getBeneficiarioCbo() {
		return beneficiarioCbo;
	}

	public void setBeneficiarioCbo(BeneficiarioCbo beneficiarioCbo) {
		this.beneficiarioCbo = beneficiarioCbo;
	}

	public QtData getDtEnvioRsdata() {
		return dtEnvioRsdata;
	}

	public void setDtEnvioRsdata(QtData dtEnvioRsdata) {
		this.dtEnvioRsdata = dtEnvioRsdata;
	}

	public String getCdMsgRetorno() {
		return cdMsgRetorno;
	}

	public void setCdMsgRetorno(String cdMsgRetorno) {
		this.cdMsgRetorno = cdMsgRetorno;
	}

	public String getDsTxtRetorno() {
		return dsTxtRetorno;
	}

	public void setDsTxtRetorno(String dsTxtRetorno) {
		this.dsTxtRetorno = dsTxtRetorno;
	}

	public String getTrnEnvioRsdata() {
		return trnEnvioRsdata;
	}

	public void setTrnEnvioRsdata(String trnEnvioRsdata) {
		this.trnEnvioRsdata = trnEnvioRsdata;
	}

}
