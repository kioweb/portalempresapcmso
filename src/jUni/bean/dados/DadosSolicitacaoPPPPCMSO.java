package jUni.bean.dados;

import quatro.util.QtData;

public class DadosSolicitacaoPPPPCMSO {

	private Integer idSolicitacao;
	private Integer idUsuarioSolicitou;
	private String cdBeneficiarioCartao;
	private String nmBeneficiario;
	private QtData dtSolicitacao;
	private QtData dtAdmissao;
	private QtData dtDemissao;
	private QtData dtNascimento;
	private String nrNitPis;
	private String nrCtps;
	private String nrSerie;
	private String ufCtps;
	private String snAcidentetrabalho;
	private QtData dtAcidenteTrabalho;
	private String nrCat;
	private String nmResponsavel;
	private String nrNitPisResponsavel;
	private QtData dtAdmissaoFuncao;
	private QtData dtFimFuncao;
	private String dsFuncao;
	private String dsCbo;
	private String dsSetor;
	private QtData dtAdmissaoFucaoPPP2;
	private QtData dtFimFincaoFuncaoPPP2;
	private String dsFuncaoFuncaoPPP2;
	private String dsCBOFuncaoPPP2;
	private String dsSetorFuncaoPPP2;
	private QtData dtAdmissaoFucaoPPP3;
	private QtData dtFimFincaoFuncaoPPP3;
	private String dsFuncaoFuncaoPPP3;
	private String dsCBOFuncaoPPP3;
	private String dsSetorFuncaoPPP3;
	private QtData dtAdmissaoFucaoPPP4;
	private QtData dtFimFincaoFuncaoPPP4;
	private String dsFuncaoFuncaoPPP4;
	private String dsCBOFuncaoPPP4;
	private String dsSetorFuncaoPPP4;
	private QtData dtAdmissaoFucaoPPP5;
	private QtData dtFimFincaoFuncaoPPP5;
	private String dsFuncaoFuncaoPPP5;
	private String dsCBOFuncaoPPP5;
	private String dsSetorFuncaoPPP5;
	private QtData dtAdmissaoFucaoPPP6;
	private QtData dtFimFincaoFuncaoPPP6;
	private String dsFuncaoFuncaoPPP6;
	private String dsCBOFuncaoPPP6;
	private String dsSetorFuncaoPPP6;
	private QtData dtAdmissaoFucaoPPP7;
	private QtData dtFimFincaoFuncaoPPP7;
	private String dsFuncaoFuncaoPPP7;
	private String dsCBOFuncaoPPP7;
	private String dsSetorFuncaoPPP7;
	private QtData dtAdmissaoFucaoPPP8;
	private QtData dtFimFincaoFuncaoPPP8;
	private String dsFuncaoFuncaoPPP8;
	private String dsCBOFuncaoPPP8;
	private String dsSetorFuncaoPPP8;
	private QtData dtAdmissaoFucaoPPP9;
	private QtData dtFimFincaoFuncaoPPP9;
	private String dsFuncaoFuncaoPPP9;
	private String dsCBOFuncaoPPP9;
	private String dsSetorFuncaoPPP9;
	private QtData dtAdmissaoFucaoPPP10;
	private QtData dtFimFincaoFuncaoPPP10;
	private String dsFuncaoFuncaoPPP10;
	private String dsCBOFuncaoPPP10;
	private String dsSetorFuncaoPPP10;
	private QtData dtMudancaFuncao;
	private QtData dtMudancaFimFuncao;
	private String dsMudancaFuncao;
	private String dsMudancaCbo;
	private String dsMudancaSetor;
	private String dsObservacoes;
	private QtData dtConfirmacao;
	private Integer idUsuarioConfirmacao;

	public QtData getDtAdmissaoFucaoPPP6() {
		return dtAdmissaoFucaoPPP6;
	}

	public void setDtAdmissaoFucaoPPP6(QtData dtAdmissaoFucaoPPP6) {
		this.dtAdmissaoFucaoPPP6 = dtAdmissaoFucaoPPP6;
	}

	public QtData getDtFimFincaoFuncaoPPP6() {
		return dtFimFincaoFuncaoPPP6;
	}

	public void setDtFimFincaoFuncaoPPP6(QtData dtFimFincaoFuncaoPPP6) {
		this.dtFimFincaoFuncaoPPP6 = dtFimFincaoFuncaoPPP6;
	}

	public String getDsFuncaoFuncaoPPP6() {
		return dsFuncaoFuncaoPPP6;
	}

	public void setDsFuncaoFuncaoPPP6(String dsFuncaoFuncaoPPP6) {
		this.dsFuncaoFuncaoPPP6 = dsFuncaoFuncaoPPP6;
	}

	public String getDsCBOFuncaoPPP6() {
		return dsCBOFuncaoPPP6;
	}

	public void setDsCBOFuncaoPPP6(String dsCBOFuncaoPPP6) {
		this.dsCBOFuncaoPPP6 = dsCBOFuncaoPPP6;
	}

	public String getDsSetorFuncaoPPP6() {
		return dsSetorFuncaoPPP6;
	}

	public void setDsSetorFuncaoPPP6(String dsSetorFuncaoPPP6) {
		this.dsSetorFuncaoPPP6 = dsSetorFuncaoPPP6;
	}

	public QtData getDtAdmissaoFucaoPPP7() {
		return dtAdmissaoFucaoPPP7;
	}

	public void setDtAdmissaoFucaoPPP7(QtData dtAdmissaoFucaoPPP7) {
		this.dtAdmissaoFucaoPPP7 = dtAdmissaoFucaoPPP7;
	}

	public QtData getDtFimFincaoFuncaoPPP7() {
		return dtFimFincaoFuncaoPPP7;
	}

	public void setDtFimFincaoFuncaoPPP7(QtData dtFimFincaoFuncaoPPP7) {
		this.dtFimFincaoFuncaoPPP7 = dtFimFincaoFuncaoPPP7;
	}

	public String getDsFuncaoFuncaoPPP7() {
		return dsFuncaoFuncaoPPP7;
	}

	public void setDsFuncaoFuncaoPPP7(String dsFuncaoFuncaoPPP7) {
		this.dsFuncaoFuncaoPPP7 = dsFuncaoFuncaoPPP7;
	}

	public String getDsCBOFuncaoPPP7() {
		return dsCBOFuncaoPPP7;
	}

	public void setDsCBOFuncaoPPP7(String dsCBOFuncaoPPP7) {
		this.dsCBOFuncaoPPP7 = dsCBOFuncaoPPP7;
	}

	public String getDsSetorFuncaoPPP7() {
		return dsSetorFuncaoPPP7;
	}

	public void setDsSetorFuncaoPPP7(String dsSetorFuncaoPPP7) {
		this.dsSetorFuncaoPPP7 = dsSetorFuncaoPPP7;
	}

	public QtData getDtAdmissaoFucaoPPP8() {
		return dtAdmissaoFucaoPPP8;
	}

	public void setDtAdmissaoFucaoPPP8(QtData dtAdmissaoFucaoPPP8) {
		this.dtAdmissaoFucaoPPP8 = dtAdmissaoFucaoPPP8;
	}

	public QtData getDtFimFincaoFuncaoPPP8() {
		return dtFimFincaoFuncaoPPP8;
	}

	public void setDtFimFincaoFuncaoPPP8(QtData dtFimFincaoFuncaoPPP8) {
		this.dtFimFincaoFuncaoPPP8 = dtFimFincaoFuncaoPPP8;
	}

	public String getDsFuncaoFuncaoPPP8() {
		return dsFuncaoFuncaoPPP8;
	}

	public void setDsFuncaoFuncaoPPP8(String dsFuncaoFuncaoPPP8) {
		this.dsFuncaoFuncaoPPP8 = dsFuncaoFuncaoPPP8;
	}

	public String getDsCBOFuncaoPPP8() {
		return dsCBOFuncaoPPP8;
	}

	public void setDsCBOFuncaoPPP8(String dsCBOFuncaoPPP8) {
		this.dsCBOFuncaoPPP8 = dsCBOFuncaoPPP8;
	}

	public String getDsSetorFuncaoPPP8() {
		return dsSetorFuncaoPPP8;
	}

	public void setDsSetorFuncaoPPP8(String dsSetorFuncaoPPP8) {
		this.dsSetorFuncaoPPP8 = dsSetorFuncaoPPP8;
	}

	public QtData getDtAdmissaoFucaoPPP9() {
		return dtAdmissaoFucaoPPP9;
	}

	public void setDtAdmissaoFucaoPPP9(QtData dtAdmissaoFucaoPPP9) {
		this.dtAdmissaoFucaoPPP9 = dtAdmissaoFucaoPPP9;
	}

	public QtData getDtFimFincaoFuncaoPPP9() {
		return dtFimFincaoFuncaoPPP9;
	}

	public void setDtFimFincaoFuncaoPPP9(QtData dtFimFincaoFuncaoPPP9) {
		this.dtFimFincaoFuncaoPPP9 = dtFimFincaoFuncaoPPP9;
	}

	public String getDsFuncaoFuncaoPPP9() {
		return dsFuncaoFuncaoPPP9;
	}

	public void setDsFuncaoFuncaoPPP9(String dsFuncaoFuncaoPPP9) {
		this.dsFuncaoFuncaoPPP9 = dsFuncaoFuncaoPPP9;
	}

	public String getDsCBOFuncaoPPP9() {
		return dsCBOFuncaoPPP9;
	}

	public void setDsCBOFuncaoPPP9(String dsCBOFuncaoPPP9) {
		this.dsCBOFuncaoPPP9 = dsCBOFuncaoPPP9;
	}

	public String getDsSetorFuncaoPPP9() {
		return dsSetorFuncaoPPP9;
	}

	public void setDsSetorFuncaoPPP9(String dsSetorFuncaoPPP9) {
		this.dsSetorFuncaoPPP9 = dsSetorFuncaoPPP9;
	}

	public QtData getDtAdmissaoFucaoPPP10() {
		return dtAdmissaoFucaoPPP10;
	}

	public void setDtAdmissaoFucaoPPP10(QtData dtAdmissaoFucaoPPP10) {
		this.dtAdmissaoFucaoPPP10 = dtAdmissaoFucaoPPP10;
	}

	public QtData getDtFimFincaoFuncaoPPP10() {
		return dtFimFincaoFuncaoPPP10;
	}

	public void setDtFimFincaoFuncaoPPP10(QtData dtFimFincaoFuncaoPPP10) {
		this.dtFimFincaoFuncaoPPP10 = dtFimFincaoFuncaoPPP10;
	}

	public String getDsFuncaoFuncaoPPP10() {
		return dsFuncaoFuncaoPPP10;
	}

	public void setDsFuncaoFuncaoPPP10(String dsFuncaoFuncaoPPP10) {
		this.dsFuncaoFuncaoPPP10 = dsFuncaoFuncaoPPP10;
	}

	public String getDsCBOFuncaoPPP10() {
		return dsCBOFuncaoPPP10;
	}

	public void setDsCBOFuncaoPPP10(String dsCBOFuncaoPPP10) {
		this.dsCBOFuncaoPPP10 = dsCBOFuncaoPPP10;
	}

	public String getDsSetorFuncaoPPP10() {
		return dsSetorFuncaoPPP10;
	}

	public void setDsSetorFuncaoPPP10(String dsSetorFuncaoPPP10) {
		this.dsSetorFuncaoPPP10 = dsSetorFuncaoPPP10;
	}

	public String getNmBeneficiario() {
		return nmBeneficiario;
	}

	public void setNmBeneficiario(String nmBeneficiario) {
		this.nmBeneficiario = nmBeneficiario;
	}

	public Integer getIdUsuarioSolicitou() {
		return idUsuarioSolicitou;
	}

	public void setIdUsuarioSolicitou(Integer idUsuarioSolicitou) {
		this.idUsuarioSolicitou = idUsuarioSolicitou;
	}

	public String getNmResponsavel() {
		return nmResponsavel;
	}

	public void setNmResponsavel(String nmResponsavel) {
		this.nmResponsavel = nmResponsavel;
	}

	public String getNrCat() {
		return nrCat;
	}

	public void setNrCat(String nrCat) {
		this.nrCat = nrCat;
	}

	public QtData getDtAcidenteTrabalho() {
		return dtAcidenteTrabalho;
	}

	public void setDtAcidenteTrabalho(QtData dtAcidenteTrabalho) {
		this.dtAcidenteTrabalho = dtAcidenteTrabalho;
	}

	public String getSnAcidentetrabalho() {
		return snAcidentetrabalho;
	}

	public void setSnAcidentetrabalho(String snAcidentetrabalho) {
		this.snAcidentetrabalho = snAcidentetrabalho;
	}

	public String getNrCtps() {
		return nrCtps;
	}

	public void setNrCtps(String nrCtps) {
		this.nrCtps = nrCtps;
	}

	public Integer getIdSolicitacao() {
		return idSolicitacao;
	}

	public void setIdSolicitacao(Integer idSolicitacao) {
		this.idSolicitacao = idSolicitacao;
	}

	public String getCdBeneficiarioCartao() {
		return cdBeneficiarioCartao;
	}

	public void setCdBeneficiarioCartao(String cdBeneficiarioCartao) {
		this.cdBeneficiarioCartao = cdBeneficiarioCartao;
	}

	public QtData getDtSolicitacao() {
		return dtSolicitacao;
	}

	public void setDtSolicitacao(QtData dtSolicitacao) {
		this.dtSolicitacao = dtSolicitacao;
	}

	public QtData getDtAdmissao() {
		return dtAdmissao;
	}

	public void setDtAdmissao(QtData dtAdmissao) {
		this.dtAdmissao = dtAdmissao;
	}

	public QtData getDtDemissao() {
		return dtDemissao;
	}

	public void setDtDemissao(QtData dtDemissao) {
		this.dtDemissao = dtDemissao;
	}

	public QtData getDtNascimento() {
		return dtNascimento;
	}

	public void setDtNascimento(QtData dtNascimento) {
		this.dtNascimento = dtNascimento;
	}

	public String getNrNitPis() {
		return nrNitPis;
	}

	public void setNrNitPis(String nrNitPis) {
		this.nrNitPis = nrNitPis;
	}

	public String getNrSerie() {
		return nrSerie;
	}

	public void setNrSerie(String nrSerie) {
		this.nrSerie = nrSerie;
	}

	public String getUfCtps() {
		return ufCtps;
	}

	public void setUfCtps(String ufCtps) {
		this.ufCtps = ufCtps;
	}

	public String getNrNitPisResponsavel() {
		return nrNitPisResponsavel;
	}

	public void setNrNitPisResponsavel(String nrNitPisResponsavel) {
		this.nrNitPisResponsavel = nrNitPisResponsavel;
	}

	public QtData getDtAdmissaoFuncao() {
		return dtAdmissaoFuncao;
	}

	public void setDtAdmissaoFuncao(QtData dtAdmissaoFuncao) {
		this.dtAdmissaoFuncao = dtAdmissaoFuncao;
	}

	public QtData getDtFimFuncao() {
		return dtFimFuncao;
	}

	public void setDtFimFuncao(QtData dtFimFuncao) {
		this.dtFimFuncao = dtFimFuncao;
	}

	public String getDsFuncao() {
		return dsFuncao;
	}

	public void setDsFuncao(String dsFuncao) {
		this.dsFuncao = dsFuncao;
	}

	public String getDsCbo() {
		return dsCbo;
	}

	public void setDsCbo(String dsCbo) {
		this.dsCbo = dsCbo;
	}

	public String getDsSetor() {
		return dsSetor;
	}

	public void setDsSetor(String dsSetor) {
		this.dsSetor = dsSetor;
	}

	public QtData getDtMudancaFuncao() {
		return dtMudancaFuncao;
	}

	public void setDtMudancaFuncao(QtData dtMudancaFuncao) {
		this.dtMudancaFuncao = dtMudancaFuncao;
	}

	public QtData getDtMudancaFimFuncao() {
		return dtMudancaFimFuncao;
	}

	public void setDtMudancaFimFuncao(QtData dtMudancaFimFuncao) {
		this.dtMudancaFimFuncao = dtMudancaFimFuncao;
	}

	public String getDsMudancaFuncao() {
		return dsMudancaFuncao;
	}

	public void setDsMudancaFuncao(String dsMudancaFuncao) {
		this.dsMudancaFuncao = dsMudancaFuncao;
	}

	public String getDsMudancaCbo() {
		return dsMudancaCbo;
	}

	public void setDsMudancaCbo(String dsMudancaCbo) {
		this.dsMudancaCbo = dsMudancaCbo;
	}

	public String getDsMudancaSetor() {
		return dsMudancaSetor;
	}

	public void setDsMudancaSetor(String dsMudancaSetor) {
		this.dsMudancaSetor = dsMudancaSetor;
	}

	public String getDsObservacoes() {
		return dsObservacoes;
	}

	public void setDsObservacoes(String dsObservacoes) {
		this.dsObservacoes = dsObservacoes;
	}

	public QtData getDtConfirmacao() {
		return dtConfirmacao;
	}

	public void setDtConfirmacao(QtData dtConfirmacao) {
		this.dtConfirmacao = dtConfirmacao;
	}

	public Integer getIdUsuarioConfirmacao() {
		return idUsuarioConfirmacao;
	}

	public void setIdUsuarioConfirmacao(Integer idUsuarioConfirmacao) {
		this.idUsuarioConfirmacao = idUsuarioConfirmacao;
	}

	public QtData getDtAdmissaoFucaoPPP2() {
		return dtAdmissaoFucaoPPP2;
	}

	public void setDtAdmissaoFucaoPPP2(QtData dtAdmissaoFucaoPPP2) {
		this.dtAdmissaoFucaoPPP2 = dtAdmissaoFucaoPPP2;
	}

	public QtData getDtFimFincaoFuncaoPPP2() {
		return dtFimFincaoFuncaoPPP2;
	}

	public void setDtFimFincaoFuncaoPPP2(QtData dtFimFincaoFuncaoPPP2) {
		this.dtFimFincaoFuncaoPPP2 = dtFimFincaoFuncaoPPP2;
	}

	public String getDsFuncaoFuncaoPPP2() {
		return dsFuncaoFuncaoPPP2;
	}

	public void setDsFuncaoFuncaoPPP2(String dsFuncaoFuncaoPPP2) {
		this.dsFuncaoFuncaoPPP2 = dsFuncaoFuncaoPPP2;
	}

	public String getDsCBOFuncaoPPP2() {
		return dsCBOFuncaoPPP2;
	}

	public void setDsCBOFuncaoPPP2(String dsCBOFuncaoPPP2) {
		this.dsCBOFuncaoPPP2 = dsCBOFuncaoPPP2;
	}

	public String getDsSetorFuncaoPPP2() {
		return dsSetorFuncaoPPP2;
	}

	public void setDsSetorFuncaoPPP2(String dsSetorFuncaoPPP2) {
		this.dsSetorFuncaoPPP2 = dsSetorFuncaoPPP2;
	}

	public QtData getDtAdmissaoFucaoPPP3() {
		return dtAdmissaoFucaoPPP3;
	}

	public void setDtAdmissaoFucaoPPP3(QtData dtAdmissaoFucaoPPP3) {
		this.dtAdmissaoFucaoPPP3 = dtAdmissaoFucaoPPP3;
	}

	public QtData getDtFimFincaoFuncaoPPP3() {
		return dtFimFincaoFuncaoPPP3;
	}

	public void setDtFimFincaoFuncaoPPP3(QtData dtFimFincaoFuncaoPPP3) {
		this.dtFimFincaoFuncaoPPP3 = dtFimFincaoFuncaoPPP3;
	}

	public String getDsFuncaoFuncaoPPP3() {
		return dsFuncaoFuncaoPPP3;
	}

	public void setDsFuncaoFuncaoPPP3(String dsFuncaoFuncaoPPP3) {
		this.dsFuncaoFuncaoPPP3 = dsFuncaoFuncaoPPP3;
	}

	public String getDsCBOFuncaoPPP3() {
		return dsCBOFuncaoPPP3;
	}

	public void setDsCBOFuncaoPPP3(String dsCBOFuncaoPPP3) {
		this.dsCBOFuncaoPPP3 = dsCBOFuncaoPPP3;
	}

	public String getDsSetorFuncaoPPP3() {
		return dsSetorFuncaoPPP3;
	}

	public void setDsSetorFuncaoPPP3(String dsSetorFuncaoPPP3) {
		this.dsSetorFuncaoPPP3 = dsSetorFuncaoPPP3;
	}

	public QtData getDtAdmissaoFucaoPPP4() {
		return dtAdmissaoFucaoPPP4;
	}

	public void setDtAdmissaoFucaoPPP4(QtData dtAdmissaoFucaoPPP4) {
		this.dtAdmissaoFucaoPPP4 = dtAdmissaoFucaoPPP4;
	}

	public QtData getDtFimFincaoFuncaoPPP4() {
		return dtFimFincaoFuncaoPPP4;
	}

	public void setDtFimFincaoFuncaoPPP4(QtData dtFimFincaoFuncaoPPP4) {
		this.dtFimFincaoFuncaoPPP4 = dtFimFincaoFuncaoPPP4;
	}

	public String getDsFuncaoFuncaoPPP4() {
		return dsFuncaoFuncaoPPP4;
	}

	public void setDsFuncaoFuncaoPPP4(String dsFuncaoFuncaoPPP4) {
		this.dsFuncaoFuncaoPPP4 = dsFuncaoFuncaoPPP4;
	}

	public String getDsCBOFuncaoPPP4() {
		return dsCBOFuncaoPPP4;
	}

	public void setDsCBOFuncaoPPP4(String dsCBOFuncaoPPP4) {
		this.dsCBOFuncaoPPP4 = dsCBOFuncaoPPP4;
	}

	public String getDsSetorFuncaoPPP4() {
		return dsSetorFuncaoPPP4;
	}

	public void setDsSetorFuncaoPPP4(String dsSetorFuncaoPPP4) {
		this.dsSetorFuncaoPPP4 = dsSetorFuncaoPPP4;
	}

	public QtData getDtAdmissaoFucaoPPP5() {
		return dtAdmissaoFucaoPPP5;
	}

	public void setDtAdmissaoFucaoPPP5(QtData dtAdmissaoFucaoPPP5) {
		this.dtAdmissaoFucaoPPP5 = dtAdmissaoFucaoPPP5;
	}

	public QtData getDtFimFincaoFuncaoPPP5() {
		return dtFimFincaoFuncaoPPP5;
	}

	public void setDtFimFincaoFuncaoPPP5(QtData dtFimFincaoFuncaoPPP5) {
		this.dtFimFincaoFuncaoPPP5 = dtFimFincaoFuncaoPPP5;
	}

	public String getDsFuncaoFuncaoPPP5() {
		return dsFuncaoFuncaoPPP5;
	}

	public void setDsFuncaoFuncaoPPP5(String dsFuncaoFuncaoPPP5) {
		this.dsFuncaoFuncaoPPP5 = dsFuncaoFuncaoPPP5;
	}

	public String getDsCBOFuncaoPPP5() {
		return dsCBOFuncaoPPP5;
	}

	public void setDsCBOFuncaoPPP5(String dsCBOFuncaoPPP5) {
		this.dsCBOFuncaoPPP5 = dsCBOFuncaoPPP5;
	}

	public String getDsSetorFuncaoPPP5() {
		return dsSetorFuncaoPPP5;
	}

	public void setDsSetorFuncaoPPP5(String dsSetorFuncaoPPP5) {
		this.dsSetorFuncaoPPP5 = dsSetorFuncaoPPP5;
	}

}
