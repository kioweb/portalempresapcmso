package jUni.bean.dados.manutencao;

import java.util.LinkedList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import jPcmso.negocio.geral.c.CboNegocio;
import jPcmso.persistencia.geral.c.Cbo;
import jPcmso.persistencia.geral.c.CboCaminhador;
import jPcmso.persistencia.geral.c.CboLocalizador;
import jPcmso.persistencia.geral.e.ExameComplementarCbo;
import jUni.util.Util;
import quatro.persistencia.FiltroDeNavegador;
import quatro.sql.ConexaoParaPoolDeConexoes;
import quatro.sql.PoolDeConexoes;

@SessionScoped
@ManagedBean
public class BeanCBOPCMSO {

	private static final String LINK_MNT = "resources/page/cbo/mntCBO.jsf";
	private static final String LINK_LISTA = "resources/page/cbo/listaCBO.jsf";
	private static final String NOME_MNT = "CBO";

	List<Cbo> listagem;

	private Cbo cboSelecionada;
	private ExameComplementarCbo complementarCboSelecionada;
	private String msgAoUsuario;
	private Integer msgExibida = 0;

	private String pesquisaCodigo;
	private String pesquisaDescricao;

	public BeanCBOPCMSO() {
		cboSelecionada = new Cbo();
		complementarCboSelecionada = new ExameComplementarCbo();
		listagem = new LinkedList<>();
		listagem = carregaListagem();
	}

	public void inic() {
		if (msgAoUsuario != null) {
			if (msgExibida > 0) {
				msgAoUsuario = null;
				msgExibida = 0;
			}
			msgExibida++;
		}
	}

	public List<Cbo> carregaListagem() {

		List<Cbo> lista = new LinkedList<>();

		try {
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
			CboCaminhador cbos = null;
			try {
				FiltroDeNavegador filtro = new FiltroDeNavegador();
				filtro.setOrdemDeNavegacao(CboCaminhador.POR_CODIGO);

				if (pesquisaCodigo != null && !pesquisaCodigo.equals("")) {
					filtro.adicionaCondicao("cd_cbo", FiltroDeNavegador.IGUAL, pesquisaCodigo );
				}
				if (pesquisaDescricao != null && !pesquisaDescricao.equals("")) {
					filtro.adicionaCondicao("nm_cbo", FiltroDeNavegador.ILIKE, "%" + pesquisaDescricao + "%");
				}

				cbos = new CboCaminhador(cnx, filtro);
				cbos.ativaCaminhador();

				if (!cbos.isEmpty()) {
					while (!cbos.isEof()) {
						lista.add(cbos.getCbo());
						cbos.proximo();
					}
				}

			} finally {
				cnx.libera();
				cbos.liberaRecursos();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return lista;
	}
	
	public void limpaMensagem() {
		msgAoUsuario = null;
		msgExibida = 0;
	}

	private void abreLista() {
		Util.sendRedirect(LINK_LISTA);
	}

	private void abreMnt() {
		Util.sendRedirect(LINK_MNT);
	}

	public void seleciona(Cbo objeto) {
		cboSelecionada = objeto;
		abreMnt();
	}

	public void pesquisar() {
		listagem = carregaListagem();
		abreLista();
	}

	public void novo() {
		cboSelecionada = new Cbo();
		abreMnt();
	}

	public void limpaFiltro() {
		pesquisaCodigo = null;
		pesquisaDescricao = null;
		listagem = carregaListagem();
		abreLista();
	}

	public void cancelar() {
		pesquisaCodigo = null;
		pesquisaDescricao = null;
		listagem = carregaListagem();
		abreLista();
	}

	public void salvar() {
		try {
			System.out.println("Inserindo novo ");

			CboNegocio cargo = new CboNegocio(cboSelecionada);
			if (cboSelecionada.getCdCbo() != null) {
				if (CboLocalizador.existe(cboSelecionada)) {
					cargo.altera();
					msgExibida = 0;
					msgAoUsuario = NOME_MNT + " alterado com sucesso";
				} else {
					cargo.insere();
					msgAoUsuario = NOME_MNT + " inserido com sucesso";
				}
			} else {
				cargo.insere();
				msgAoUsuario = NOME_MNT + " inserido com sucesso";
			}
			abreLista();
		} catch (Exception e) {
			msgExibida = 0;
			msgAoUsuario = "Ops! " + e.getMessage();
			e.printStackTrace();
		}
	}

	public void exclui(Cbo cargo) {
		try {
			System.out.println("Exluindo");

			CboNegocio cargos = new CboNegocio(cargo);
			cargos.exclui();

			msgAoUsuario = NOME_MNT + " excluído com sucesso";
			abreLista();
		} catch (Exception e) {
			msgExibida = 0;
			msgAoUsuario = "Ops! " + e.getMessage();
			e.printStackTrace();
		}
	}

	public String getMsgAoUsuario() {
		return msgAoUsuario;
	}

	public void setMsgAoUsuario(String msgAoUsuario) {
		this.msgAoUsuario = msgAoUsuario;
	}

	public Integer getMsgExibida() {
		return msgExibida;
	}

	public void setMsgExibida(Integer msgExibida) {
		this.msgExibida = msgExibida;
	}

	public Cbo getCboSelecionada() {
		return cboSelecionada;
	}

	public void setCboSelecionada(Cbo cboSelecionada) {
		this.cboSelecionada = cboSelecionada;
	}

	public ExameComplementarCbo getComplementarCboSelecionada() {
		return complementarCboSelecionada;
	}

	public void setComplementarCboSelecionada(ExameComplementarCbo complementarCboSelecionada) {
		this.complementarCboSelecionada = complementarCboSelecionada;
	}

	public List<Cbo> getListagem() {
		return listagem;
	}

	public void setListagem(List<Cbo> listagem) {
		this.listagem = listagem;
	}

	public String getPesquisaCodigo() {
		return pesquisaCodigo;
	}

	public void setPesquisaCodigo(String pesquisaCodigo) {
		this.pesquisaCodigo = pesquisaCodigo;
	}

	public String getPesquisaDescricao() {
		return pesquisaDescricao;
	}

	public void setPesquisaDescricao(String pesquisaDescricao) {
		this.pesquisaDescricao = pesquisaDescricao;
	}

}
