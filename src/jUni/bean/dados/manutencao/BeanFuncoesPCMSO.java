package jUni.bean.dados.manutencao;

import java.util.LinkedList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import jPcmso.negocio.geral.f.FuncaoNegocio;
import jPcmso.persistencia.geral.f.Funcao;
import jPcmso.persistencia.geral.f.FuncaoCaminhador;
import jPcmso.persistencia.geral.f.FuncaoLocalizador;
import jUni.util.Util;
import quatro.persistencia.FiltroDeNavegador;
import quatro.sql.ConexaoParaPoolDeConexoes;
import quatro.sql.PoolDeConexoes;

@SessionScoped
@ManagedBean
public class BeanFuncoesPCMSO {

	private static final String LINK_MNT = "resources/page/funcao/mntFuncoes.jsf";
	private static final String LINK_LISTA = "resources/page/funcao/listaFuncoes.jsf";
	private static final String NOME_MNT = "Função";

	List<Funcao> listagem;

	private Funcao selecionada;
	private String msgAoUsuario;
	private Integer msgExibida = 0;

	private String pesquisaCodigo;
	private String pesquisaDescricao;

	public BeanFuncoesPCMSO() {
		selecionada = new Funcao();
		listagem = new LinkedList<>();
		listagem = carregaListagem();
	}

	public void inic() {
		if (msgAoUsuario != null) {
			if (msgExibida > 0) {
				msgAoUsuario = null;
				msgExibida = 0;
			}
			msgExibida++;
		}
	}

	public List<Funcao> carregaListagem() {

		List<Funcao> lista = new LinkedList<>();

		try {
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
			FuncaoCaminhador cargos = null;
			try {
				FiltroDeNavegador filtro = new FiltroDeNavegador();
				filtro.setOrdemDeNavegacao(FuncaoCaminhador.POR_ORDEM_DEFAULT);

				if (pesquisaCodigo != null && !pesquisaCodigo.equals("")) {
					filtro.adicionaCondicao("id_funcao", FiltroDeNavegador.IGUAL, new Integer(pesquisaCodigo));
				}
				if (pesquisaDescricao != null && !pesquisaDescricao.equals("")) {
					filtro.adicionaCondicao("ds_funcao", FiltroDeNavegador.ILIKE, "%" + pesquisaDescricao + "%");
				}

				cargos = new FuncaoCaminhador(cnx, filtro);
				cargos.ativaCaminhador();

				if (!cargos.isEmpty()) {
					while (!cargos.isEof()) {
						lista.add(cargos.getFuncao());
						cargos.proximo();
					}
				}

			} finally {
				cnx.libera();
				cargos.liberaRecursos();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return lista;
	}

	public void limpaMensagem() {
		msgAoUsuario = null;
		msgExibida = 0;
	}

	private void abreLista() {
		Util.sendRedirect(LINK_LISTA);
	}

	private void abreMnt() {
		Util.sendRedirect(LINK_MNT);
	}

	public void seleciona(Funcao objeto) {
		selecionada = objeto;
		abreMnt();
	}

	public void pesquisar() {
		listagem = carregaListagem();
		abreLista();
	}

	public void novo() {
		selecionada = new Funcao();
		abreMnt();
	}

	public void limpaFiltro() {
		pesquisaCodigo = null;
		pesquisaDescricao = null;
		listagem = carregaListagem();
		abreLista();
	}

	public void cancelar() {
		pesquisaCodigo = null;
		pesquisaDescricao = null;
		listagem = carregaListagem();
		abreLista();
	}

	public void salvar() {
		try {
			System.out.println("Inserindo novo ");

			FuncaoNegocio cargo = new FuncaoNegocio(selecionada);
			if (selecionada.getIdFuncao() != null) {
				if (FuncaoLocalizador.existe(selecionada)) {
					cargo.altera();
					msgExibida = 0;
					msgAoUsuario = NOME_MNT + " alterado com sucesso";
				} else {
					cargo.insere();
					msgAoUsuario = NOME_MNT + " inserido com sucesso";
				}
			} else {
				cargo.insere();
				msgAoUsuario = NOME_MNT + " inserido com sucesso";
			}
			abreLista();
		} catch (Exception e) {
			msgExibida = 0;
			msgAoUsuario = "Ops! " + e.getMessage();
			e.printStackTrace();
		}
	}

	public void exclui(Funcao cargo) {
		try {
			System.out.println("Exluindo");

			FuncaoNegocio cargos = new FuncaoNegocio(cargo);
			cargos.exclui();

			msgAoUsuario = NOME_MNT + " excluído com sucesso";
			abreLista();
		} catch (Exception e) {
			msgExibida = 0;
			msgAoUsuario = "Ops! " + e.getMessage();
			e.printStackTrace();
		}
	}

	public String getMsgAoUsuario() {
		return msgAoUsuario;
	}

	public void setMsgAoUsuario(String msgAoUsuario) {
		this.msgAoUsuario = msgAoUsuario;
	}

	public Integer getMsgExibida() {
		return msgExibida;
	}

	public void setMsgExibida(Integer msgExibida) {
		this.msgExibida = msgExibida;
	}

	public Funcao getSelecionada() {
		return selecionada;
	}

	public void setSelecionada(Funcao selecionada) {
		this.selecionada = selecionada;
	}

	public List<Funcao> getListagem() {
		return listagem;
	}

	public void setListagem(List<Funcao> listagem) {
		this.listagem = listagem;
	}

	public String getPesquisaCodigo() {
		return pesquisaCodigo;
	}

	public void setPesquisaCodigo(String pesquisaCodigo) {
		this.pesquisaCodigo = pesquisaCodigo;
	}

	public String getPesquisaDescricao() {
		return pesquisaDescricao;
	}

	public void setPesquisaDescricao(String pesquisaDescricao) {
		this.pesquisaDescricao = pesquisaDescricao;
	}

}
