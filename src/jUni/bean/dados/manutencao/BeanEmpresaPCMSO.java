package jUni.bean.dados.manutencao;

import java.util.LinkedList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import jPcmso.negocio.geral.c.CargoNegocio;
import jPcmso.negocio.geral.e.EmpresaNegocio;
import jPcmso.persistencia.geral.c.Cargo;
import jPcmso.persistencia.geral.c.CargoCaminhador;
import jPcmso.persistencia.geral.e.Empresa;
import jPcmso.persistencia.geral.e.EmpresaCaminhador;
import jPcmso.persistencia.geral.e.EmpresaLocalizador;
import jPcmso.persistencia.geral.m.Medico;
import jPcmso.persistencia.geral.m.MedicoLocalizador;
import jUni.persistencia.geral.p.Prestador;
import jUni.persistencia.geral.p.PrestadorLocalizador;
import jUni.util.Util;
import quatro.persistencia.FiltroDeNavegador;
import quatro.sql.ConexaoParaPoolDeConexoes;
import quatro.sql.PoolDeConexoes;
import quatro.util.ParametrosSistema;

@SessionScoped
@ManagedBean
public class BeanEmpresaPCMSO {

	private static final String LINK_MNT = "resources/page/empresa/mntEmpresa.jsf";
	private static final String LINK_LISTA = "resources/page/empresa/listaEmpresas.jsf";
	private static final String NOME_MNT = "Empresa";

	List<Empresa> listagem;
	private Empresa registroSelecionado;
	private String msgAoUsuario;
	private Integer msgExibida = 0;

	private String pesquisaCodigo;
	private String pesquisaDescricao;

	public BeanEmpresaPCMSO() {
		registroSelecionado = new Empresa();
		listagem = new LinkedList<>();
		listagem = carregaListagem();
	}

	public void inic() {
		if (msgAoUsuario != null) {
			if (msgExibida > 0) {
				msgAoUsuario = null;
				msgExibida = 0;
			}
			msgExibida++;
		}
	}
	
	public String getMedicoCoordenador() {
		
		System.out.println("Pesquisando coordenador com "); 
		System.out.println( ParametrosSistema.getInstance().getCodigoDoCliente() );
		System.out.println( registroSelecionado.getCdMedicoCoordenador() );
		
		try {
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
			try {
				Medico prest = MedicoLocalizador.buscaMedico(cnx, registroSelecionado.getCdMedicoCoordenador() );
				if( prest != null ) {
					if( prest.getNmMedico() != null ) {
						return prest.getNmMedico();
					}
				}
			} finally {
				cnx.libera();
			}
		} catch( Exception e ) {
			e.printStackTrace();
			msgAoUsuario = "Ops! " + e.getMessage();
			msgExibida = 0;
		}
		return null;
		
	}

	public List<Empresa> carregaListagem() {

		List<Empresa> lista = new LinkedList<>();

		try {
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
			EmpresaCaminhador cargos = null;
			try {
				FiltroDeNavegador filtro = new FiltroDeNavegador();
				filtro.setOrdemDeNavegacao(CargoCaminhador.POR_ORDEM_DEFAULT);

				filtro.adicionaCondicao("cd_empresa", FiltroDeNavegador.MAIOR_OU_IGUAL, "8000");
				filtro.adicionaCondicao("cd_empresa", FiltroDeNavegador.MENOR, "9000");

				if (pesquisaCodigo != null && !pesquisaCodigo.equals("")) {
					filtro.adicionaCondicao("cd_empresa", FiltroDeNavegador.IGUAL, pesquisaCodigo );
				}
				if (pesquisaDescricao != null && !pesquisaDescricao.equals("")) {
					filtro.adicionaCondicao("ds_empresa", FiltroDeNavegador.ILIKE, "%" + pesquisaDescricao + "%");
				}

				cargos = new EmpresaCaminhador(cnx, filtro);
				cargos.ativaCaminhador();

				if (!cargos.isEmpty()) {
					while (!cargos.isEof()) {
						lista.add(cargos.getEmpresa());
						cargos.proximo();
					}
				}

			} finally {
				cnx.libera();
				cargos.liberaRecursos();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return lista;
	}

	public void limpaMensagem() {
		msgAoUsuario = null;
		msgExibida = 0;
	}

	private void abreLista() {
		Util.sendRedirect(LINK_LISTA);
	}

	private void abreMnt() {
		Util.sendRedirect(LINK_MNT);
	}

	public void seleciona(Empresa cargo) {
		registroSelecionado = cargo;
		abreMnt();
	}

	public void pesquisar() {
		listagem = carregaListagem();
		abreLista();
	}

	public void novo() {
		registroSelecionado = new Empresa();
		abreMnt();
	}

	public void limpaFiltro() {
		pesquisaCodigo = null;
		pesquisaDescricao = null;
		listagem = carregaListagem();
		abreLista();
	}

	public void cancelar() {
		pesquisaCodigo = null;
		pesquisaDescricao = null;
		listagem = carregaListagem();
		abreLista();
	}

	public void salvar() {
		try {
			System.out.println("Inserindo novo ");

			EmpresaNegocio cargo = new EmpresaNegocio(registroSelecionado);
			if (registroSelecionado.getCdEmpresa() != null) {
				if (EmpresaLocalizador.existe(registroSelecionado)) {
					cargo.altera();
					msgExibida = 0;
					msgAoUsuario = NOME_MNT + " alterado com sucesso";
				} else {
					cargo.insere();
					msgAoUsuario = NOME_MNT + " inserido com sucesso";
				}
			} else {
				cargo.insere();
				msgAoUsuario = NOME_MNT + " inserido com sucesso";
			}
			abreLista();
		} catch (Exception e) {
			msgExibida = 0;
			msgAoUsuario = "Ops! " + e.getMessage();
			e.printStackTrace();
		}
	}

	public void exclui(Cargo cargo) {
		try {
			System.out.println("Exluindo");

			CargoNegocio cargos = new CargoNegocio(cargo);
			cargos.exclui();

			msgAoUsuario = NOME_MNT + " excluído com sucesso";
			abreLista();
		} catch (Exception e) {
			msgExibida = 0;
			msgAoUsuario = "Ops! " + e.getMessage();
			e.printStackTrace();
		}
	}

	public String getMsgAoUsuario() {
		return msgAoUsuario;
	}

	public void setMsgAoUsuario(String msgAoUsuario) {
		this.msgAoUsuario = msgAoUsuario;
	}

	public Integer getMsgExibida() {
		return msgExibida;
	}

	public void setMsgExibida(Integer msgExibida) {
		this.msgExibida = msgExibida;
	}

	public Empresa getRegistroSelecionado() {
		return registroSelecionado;
	}

	public void setRegistroSelecionado(Empresa registroSelecionado) {
		this.registroSelecionado = registroSelecionado;
	}

	public String getPesquisaCodigo() {
		return pesquisaCodigo;
	}

	public void setPesquisaCodigo(String pesquisaCodigo) {
		this.pesquisaCodigo = pesquisaCodigo;
	}

	public String getPesquisaDescricao() {
		return pesquisaDescricao;
	}

	public void setPesquisaDescricao(String pesquisaDescricao) {
		this.pesquisaDescricao = pesquisaDescricao;
	}

	public List<Empresa> getListagem() {
		return listagem;
	}

	public void setListagem(List<Empresa> listagem) {
		this.listagem = listagem;
	}
	
	

}
