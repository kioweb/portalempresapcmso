package jUni.bean.dados.manutencao;

import java.util.LinkedList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import jPcmso.negocio.geral.r.RiscoNegocio;
import jPcmso.persistencia.geral.r.Risco;
import jPcmso.persistencia.geral.r.RiscoBeneficiario;
import jPcmso.persistencia.geral.r.RiscoBeneficiarioCaminhador;
import jPcmso.persistencia.geral.r.RiscoCaminhador;
import jPcmso.persistencia.geral.r.RiscoLocalizador;
import jPcmso.persistencia.geral.t.TipoDeRisco;
import jPcmso.persistencia.geral.t.TipoDeRiscoCaminhador;
import jUni.util.Util;
import quatro.persistencia.FiltroDeNavegador;
import quatro.sql.ConexaoParaPoolDeConexoes;
import quatro.sql.PoolDeConexoes;

@SessionScoped
@ManagedBean
public class BeanRiscosPCMSO {

	private static final String LINK_MNT = "resources/page/risco/mntRisco.jsf";
	private static final String LINK_LISTA = "resources/page/risco/listaRiscos.jsf";
	private static final String NOME_MNT = "Risco";

	private String pesquisaCodigo;
	private String pesquisaDescricao;

	private List<Risco> listagem;

	private List<TipoDeRisco> listagemRiscoBeneficiario;

	private Risco selecionada;
	private String msgAoUsuario;
	private Integer msgExibida = 0;

	public BeanRiscosPCMSO() {
		selecionada = new Risco();
		listagem = new LinkedList<>();
		listagemRiscoBeneficiario = new LinkedList<>();
		listagemRiscoBeneficiario = carregaListaRiscosBeneficiarios();
		listagem = carregaListagem();
	}

	public void inic() {
		if (msgAoUsuario != null) {
			if (msgExibida > 0) {
				msgAoUsuario = null;
				msgExibida = 0;
			}
			msgExibida++;
		}
	}

	public List<TipoDeRisco> carregaListaRiscosBeneficiarios() {
		List<TipoDeRisco> lista = new LinkedList<>();

		try {
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
			TipoDeRiscoCaminhador riscos = null;
			try {
				FiltroDeNavegador filtro = new FiltroDeNavegador();
				filtro.setOrdemDeNavegacao(RiscoCaminhador.POR_ORDEM_DEFAULT);

				riscos = new TipoDeRiscoCaminhador(cnx, filtro);
				riscos.ativaCaminhador();

				if (!riscos.isEmpty()) {
					while (!riscos.isEof()) {
						lista.add( riscos.getTipoDeRisco() );
						riscos.proximo();
					}
				}

			} finally {
				cnx.libera();
				riscos.liberaRecursos();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return lista;

	}

	public List<Risco> carregaListagem() {

		List<Risco> lista = new LinkedList<>();

		try {
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
			RiscoCaminhador cargos = null;
			try {
				FiltroDeNavegador filtro = new FiltroDeNavegador();
				filtro.setOrdemDeNavegacao(RiscoCaminhador.POR_ORDEM_DEFAULT);

				if (pesquisaCodigo != null && !pesquisaCodigo.equals("")) {
					filtro.adicionaCondicao("id_Risco", FiltroDeNavegador.IGUAL, new Integer(pesquisaCodigo));
				}
				if (pesquisaDescricao != null && !pesquisaDescricao.equals("")) {
					filtro.adicionaCondicao("ds_Risco", FiltroDeNavegador.ILIKE, "%" + pesquisaDescricao + "%");
				}

				cargos = new RiscoCaminhador(cnx, filtro);
				cargos.ativaCaminhador();

				if (!cargos.isEmpty()) {
					while (!cargos.isEof()) {
						lista.add(cargos.getRisco());
						cargos.proximo();
					}
				}

			} finally {
				cnx.libera();
				cargos.liberaRecursos();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return lista;
	}

	public void limpaMensagem() {
		msgAoUsuario = null;
		msgExibida = 0;
	}

	private void abreLista() {
		Util.sendRedirect(LINK_LISTA);
	}

	private void abreMnt() {
		Util.sendRedirect(LINK_MNT);
	}

	public void seleciona(Risco objeto) {
		selecionada = objeto;
		abreMnt();
	}

	public void pesquisar() {
		listagem = carregaListagem();
		abreLista();
	}

	public void novo() {
		selecionada = new Risco();
		abreMnt();
	}

	public void limpaFiltro() {
		pesquisaCodigo = null;
		pesquisaDescricao = null;
		listagem = carregaListagem();
		abreLista();
	}

	public void cancelar() {
		pesquisaCodigo = null;
		pesquisaDescricao = null;
		listagem = carregaListagem();
		abreLista();
	}

	public void salvar() {
		try {
			System.out.println("Inserindo novo ");

			RiscoNegocio cargo = new RiscoNegocio(selecionada);
			if (selecionada.getIdRisco() != null) {
				if (RiscoLocalizador.existe(selecionada)) {
					cargo.altera();
					msgExibida = 0;
					msgAoUsuario = NOME_MNT + " alterado com sucesso";
				} else {
					cargo.insere();
					msgAoUsuario = NOME_MNT + " inserido com sucesso";
				}
			} else {
				cargo.insere();
				msgAoUsuario = NOME_MNT + " inserido com sucesso";
			}
			abreLista();
		} catch (Exception e) {
			msgExibida = 0;
			msgAoUsuario = "Ops! " + e.getMessage();
			e.printStackTrace();
		}
	}

	public void exclui(Risco cargo) {
		try {
			System.out.println("Exluindo");

			RiscoNegocio cargos = new RiscoNegocio(cargo);
			cargos.exclui();

			msgAoUsuario = NOME_MNT + " excluído com sucesso";
			abreLista();
		} catch (Exception e) {
			msgExibida = 0;
			msgAoUsuario = "Ops! " + e.getMessage();
			e.printStackTrace();
		}
	}

	public List<Risco> getListagem() {
		return listagem;
	}

	public void setListagem(List<Risco> listagem) {
		this.listagem = listagem;
	}

	public Risco getSelecionada() {
		return selecionada;
	}

	public void setSelecionada(Risco selecionada) {
		this.selecionada = selecionada;
	}

	public String getMsgAoUsuario() {
		return msgAoUsuario;
	}

	public void setMsgAoUsuario(String msgAoUsuario) {
		this.msgAoUsuario = msgAoUsuario;
	}

	public Integer getMsgExibida() {
		return msgExibida;
	}

	public void setMsgExibida(Integer msgExibida) {
		this.msgExibida = msgExibida;
	}

	public String getPesquisaCodigo() {
		return pesquisaCodigo;
	}

	public void setPesquisaCodigo(String pesquisaCodigo) {
		this.pesquisaCodigo = pesquisaCodigo;
	}

	public String getPesquisaDescricao() {
		return pesquisaDescricao;
	}

	public void setPesquisaDescricao(String pesquisaDescricao) {
		this.pesquisaDescricao = pesquisaDescricao;
	}

	public List<TipoDeRisco> getListagemRiscoBeneficiario() {
		return listagemRiscoBeneficiario;
	}

	public void setListagemRiscoBeneficiario(List<TipoDeRisco> listagemRiscoBeneficiario) {
		this.listagemRiscoBeneficiario = listagemRiscoBeneficiario;
	}

}
