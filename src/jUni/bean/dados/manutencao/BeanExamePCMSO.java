package jUni.bean.dados.manutencao;

import java.io.FileOutputStream;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import jPcmso.biblioteca.negocio.BibliotecaExame;
import jPcmso.biblioteca.relatorio.QtJrDataSource;
import jPcmso.biblioteca.relatorio.dados.DadosRenovacao;
import jPcmso.negocio.geral.c.CargoNegocio;
import jPcmso.negocio.geral.e.ExameBeneficiarioNegocio;
import jPcmso.negocio.geral.e.ExameOcupacionalNegocio;
import jPcmso.persistencia.geral.b.BeneficiarioCbo;
import jPcmso.persistencia.geral.b.BeneficiarioCboLocalizador;
import jPcmso.persistencia.geral.c.Cargo;
import jPcmso.persistencia.geral.e.Empresa;
import jPcmso.persistencia.geral.e.EmpresaLocalizador;
import jPcmso.persistencia.geral.e.ExameBeneficiario;
import jPcmso.persistencia.geral.e.ExameBeneficiarioLocalizador;
import jPcmso.persistencia.geral.e.ExameComplementar;
import jPcmso.persistencia.geral.e.ExameComplementarCaminhador;
import jPcmso.persistencia.geral.e.ExameOcupacional;
import jPcmso.persistencia.geral.e.ExameOcupacionalCaminhador;
import jPcmso.persistencia.geral.e.ExameOcupacionalLocalizador;
import jPcmso.persistencia.geral.l.Lotacao;
import jPcmso.persistencia.geral.l.LotacaoLocalizador;
import jPcmso.persistencia.geral.m.Medico;
import jPcmso.persistencia.geral.m.MedicoLocalizador;
import jPcmso.persistencia.geral.p.Procedimento;
import jPcmso.persistencia.geral.p.ProcedimentoLocalizador;
import jUni.relatorio.DadosRelatorioJasper;
import jUni.util.Util;
import quatro.negocio.ErroDeNegocio;
import quatro.persistencia.FiltroDeNavegador;
import quatro.sql.Conexao;
import quatro.sql.ConexaoParaPoolDeConexoes;
import quatro.sql.PoolDeConexoes;
import quatro.sql.QtSQLException;
import quatro.sql.Query;
import quatro.util.ParametrosSistema;
import quatro.util.QtData;

@SessionScoped
@ManagedBean
public class BeanExamePCMSO {

	private static final String LINK_MNT = "resources/page/exame/mntExame.jsf";
	private static final String LINK_LISTA = "resources/page/exame/listaExames.jsf";
	private static final String NOME_MNT = "Exame";

	List<ExameOcupacional> listagem;
	List<ExameComplementar> listagemExameComplementar;

	private ExameComplementar exameComplementarSelecionado;

	private ExameOcupacional registroSelecionado;
	private String msgAoUsuario;
	private Integer msgExibida = 0;

	private String pesquisaCodigo;
	private String pesquisaDescricao;

	private String abaDados;
	private String abaComplementares;
	private String abaExamesComplementares;
	private String abaDadosComplementares;

	private String cdBeneficiarioCartaoRel;
	private String dsNomeBeneficiario;
	private String cdMedicoRel;
	private String dsNomeMedico;
	private String tpExameRel;

	private String cdEmpresaRel;
	private String dsNomeEmpresa;
	private String tpExameRelRenovacao;
	private String dtInicial;
	private String dtFinal;

	private Integer idLotacaoRelExame;
	private String dsNomeLotacao;

	private Integer idLotacaoFinalRelExame;
	private String dsNomeLotacaoFinal;

	private String cdEmpresaFinalRel;
	private String dsNomeEmpresaFinal;
	private String cdProcedimento;
	private String dsProcedimento;
	private String cdProcedimentoFinal;
	private String dsProcedimentoFinal;
	private Integer novoMesExame;
	private Integer diasInicial;
	private Integer diasFinal;

	public BeanExamePCMSO() {
		registroSelecionado = new ExameOcupacional();
		listagemExameComplementar = new LinkedList<>();
		listagem = new LinkedList<>();
		listagem = carregaListagem();
		abaDados = "active";
		abaComplementares = "";
		abaExamesComplementares = "active";
		abaDadosComplementares = "";
	}

	public void inic() {
		if (msgAoUsuario != null) {
			if (msgExibida > 0) {
				msgAoUsuario = null;
				msgExibida = 0;
			}
			msgExibida++;
		}
	}

	public void processoNovoExame() {

		System.out.println("Processando Exame..");
		try {
			BeneficiarioCbo beneficiarioCbo = null;
			System.out.println("cdBeneficiarioCartaoRel " + cdBeneficiarioCartaoRel);
			if (!cdBeneficiarioCartaoRel.trim().equals("")) {
				beneficiarioCbo = BeneficiarioCboLocalizador.buscaBeneficiarioCbo(cdBeneficiarioCartaoRel);

				System.out.println("beneficiarioCbo " + beneficiarioCbo);
				if (beneficiarioCbo != null) {
					boolean temrisco = false;
					ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();

					try {
						Query qry = new Query(cnx);
						qry.addSQL("select count(*) from pcmso.riscos_beneficiario where  cd_beneficiario_cartao = '"
								+ beneficiarioCbo.getCdBeneficiarioCartao() + "'");
						qry.executeQuery();
						if (!qry.isEmpty()) {
							if (qry.getInt(1) > 0) {
								temrisco = true;
							}
						}
					} finally {
						cnx.libera();
					}

					System.out.println("temrisco " + temrisco);

					if (!temrisco) {
						System.out.println("não tem risco");
						msgAoUsuario = "Por Favor cadastrar risco para o Beneficiario ";
						msgExibida = 0;
					} else {

						System.out.println("Iniciando impressão");
						ExameOcupacional eo = new ExameOcupacional();

						eo.setCdCbo(beneficiarioCbo.getCdCbo());
						eo.setIdCargo(beneficiarioCbo.getIdCargo());
						eo.setIdFuncao(beneficiarioCbo.getIdFuncao());
						eo.setIdSetor(beneficiarioCbo.getIdSetor());

						eo.setCdBeneficiarioCartao(cdBeneficiarioCartaoRel);
						if (!cdMedicoRel.trim().equals("")) {
							eo.setCdMedicoExaminador(cdMedicoRel);
						}
						eo.setTpExame("A");
						eo.setCdProcedimento("40304361");

						if (tpExameRel == null) {
							tpExameRel = "A";
						}

						if (!tpExameRel.equals("")) {
							eo.setTpExame(tpExameRel);
						}
						try {
							ExameOcupacionalNegocio eon = new ExameOcupacionalNegocio(eo);
							eon.insere();
							BibliotecaExame.geraExamesComplementares(eo);
							System.out.println("Emitindo relatório");
							geraRelatorio(eo.getNrExame());
						} catch (Exception e) {
							e.printStackTrace();
							msgAoUsuario = "Ops! " + e.getMessage();
							msgExibida = 0;
						}
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			msgAoUsuario = "Ops! " + e.getMessage();
			msgExibida = 0;
		}
	}

	private void processoAlerarMesExame(String acao) {
		try {
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
			try {
				if (cdEmpresaRel.trim().equals("")) {
					aviso("Empresa inicial necessita estar Preenchido!!");
					return;
				}
				if (cdEmpresaFinalRel.trim().equals("")) {
					aviso("Empresa final necessita estar Preenchido!!");
					return;
				}
				if (cdProcedimento.trim().equals("")) {
					aviso("Procedimento inicial necessita estar Preenchido!!");
					return;
				}
				if (cdProcedimentoFinal.trim().equals("")) {
					aviso("Procedimento final necessita estar Preenchido!!");
					return;
				}
				if (novoMesExame != null) {
					aviso("Mês necessita estar Preenchido!!");
					return;
				}
				if (diasInicial != null) {
					aviso("Dias após hoje necessita estar Preenchido!!");
					return;
				}
				if (diasFinal.equals("")) {
					aviso("Dias após hoje necessita estar Preenchido!!");
					return;
				}

				gerarArquivoLog(BibliotecaExame.alterarMesExame(cnx, cdEmpresaRel, cdEmpresaFinalRel, cdProcedimento,
						cdProcedimentoFinal, novoMesExame, diasInicial, diasFinal));

				// enviaArquivo( "pcmso/Log_alterarMesExame.txt",
				// InterfaceFileDownload.SALVAR_ABRIR_ARQUIVO, "txt",
				// "Log_alterarMesExame" );

				aviso("Exames alterados");

			} finally {
				cnx.libera();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void gerarArquivoLog(String texto) {
		String pathJasper = (String) ParametrosSistema.getInstance().getParametro("dirPCMSO");
		pathJasper += "/";
		try {
			FileOutputStream fos = new FileOutputStream(pathJasper + "Log_alterarMesExame.txt");
			try {
				fos.write(texto.getBytes());
				fos.flush();
			} finally {
				fos.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void processoRelatorioExames() {
		try {
			System.out.println("EMITINDO EXAMES");
			if (cdEmpresaRel.trim().equals("")) {
				aviso("Empresa necessita estar Preenchido!!");
				return;
			}

			if (!QtData.ehData(dtInicial)) {
				aviso("Data Inicial necessita estar Preenchido!!");
				return;
			}

			if (!QtData.ehData(dtFinal)) {
				aviso("Data Final necessita estar Preenchido!!");
				return;
			}

			QtData dtInicial = new QtData(this.dtInicial);
			QtData dtFinal = new QtData(this.dtFinal);
			Integer idLotacao = null;

			if (idLotacaoRelExame != 0) {
				idLotacao = idLotacaoRelExame;
			}

			Vector<DadosRenovacao> dados = new Vector<DadosRenovacao>();

			geraListaDeExames(dados, cdEmpresaRel, idLotacao, dtInicial, dtFinal);

			QtJrDataSource qt = new QtJrDataSource(dados);

			enviaArquivo("renovacao/relExameFuncionarios", qt);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void geraListaDeExames(Vector<DadosRenovacao> dados, String cdEmpresa, Integer idLotacao, QtData dtInicial,
			QtData dtFinal) {

		try {

			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
			try {

				Query qry = new Query(cnx);

				qry.addSQL("select e.*, m.*, b.cd_beneficiario_cartao, b.ds_nome, p.*, ex.* ");
				qry.addSQL("from  pcmso.empresas e, ");
				qry.addSQL("pcmso.beneficiarios_cbo b, ");
				qry.addSQL("pcmso.exames_beneficiario ex, ");
				qry.addSQL("pcmso.procedimentos p , ");
				qry.addSQL("pcmso.medicos m ");
				qry.addSQL("	where ex.cd_beneficiario_cartao = b.cd_beneficiario_cartao ");
				qry.addSQL("and p.cd_procedimento = ex.cd_procedimento ");
				qry.addSQL("  and b.cd_empresa = e.cd_empresa ");
				qry.addSQL("  and m.cd_medico = e.cd_medico_COORDENADOR ");
				qry.addSQL("	and e.cd_empresa = :prmsCdEmpresa");

				if (dtFinal != null)
					qry.addSQL(" and ( b.dt_exclusao is null or b.dt_exclusao >= :prmsDtExclusao ) ");
				else {
					qry.addSQL(" and b.dt_exclusao is null ");
				}

				if (idLotacao != null) {
					qry.addSQL(" and b.id_lotacao = :prmsIdLotacao ");
				}

				qry.setParameter("prmsCdEmpresa", cdEmpresa);
				if (idLotacao != null) {
					qry.setParameter("prmsIdLotacao", idLotacao);
				}

				if (dtFinal != null) {
					qry.setParameter("prmsDtExclusao", dtFinal);
				}

				qry.executeQuery();

				Empresa e = new Empresa();
				Medico m = new Medico();
				ExameBeneficiario eb = new ExameBeneficiario();

				if (!qry.isEmpty()) {
					while (!qry.isAfterLast()) {

						EmpresaLocalizador.buscaCampos(e, qry);
						MedicoLocalizador.buscaCampos(m, qry);
						ExameBeneficiarioLocalizador.buscaCampos(eb, qry);

						DadosRenovacao dr = BibliotecaExame.buscaUltimaExameComplementaresExame(cnx, eb, dtInicial,
								dtFinal);

						if (dr != null) {
							dr.setEmpresas(e);
							dr.setMedicos(m);
							dr.setDsNome(qry.getString("ds_nome"));
							dr.setCdProcedimento(qry.getString("cd_procedimento"));
							dr.setDsProcedimento(qry.getString("ds_procedimento"));
							dados.add(dr);
						}
						qry.next();
					}
				}

			} finally {
				cnx.libera();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void processoRelatorioAnualXls() {
		try {
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
			try {
				if (cdEmpresaRel.trim().equals("")) {
					aviso("Empresa necessita estar Preenchido!!");
					return;
				}

				if (!QtData.ehData(dtInicial)) {
					aviso("Data Inicial necessita estar Preenchido!!");
					return;
				}

				if (!QtData.ehData(dtFinal)) {
					aviso("Data Final necessita estar Preenchido!!");
					return;
				}

				QtData dtInicial = new QtData(this.dtInicial);
				QtData dtFinal = new QtData(this.dtFinal);

				String pathJasper = (String) ParametrosSistema.getInstance().getParametro("dirRelPCMSO");
				pathJasper += "/";

				Hashtable<String, Object> parametros = new Hashtable<String, Object>();

				parametros.put("CaminhoPath", pathJasper);
				parametros.put("cdEmpresa", cdEmpresaRel);
				parametros.put("dtInicial", dtInicial.getFormatoSQL(true, false));
				parametros.put("dtFinal", dtFinal.getFormatoSQL(true, false));

				parametros.put("dtAssinatura", dtFinal.getCalendar().getTime());

				if (idLotacaoFinalRelExame != null) {
					parametros.put("lotacaoFinal", idLotacaoFinalRelExame);
				} else {
					parametros.put("lo tacaoFinal", "9999");
				}

				if (idLotacaoRelExame != null) {
					parametros.put("lotacaoInicial", idLotacaoRelExame);
				} else {
					parametros.put("lotacaoInicial", "0");
				}

				enviaArquivo("relAnualQuadroNR07Xls", parametros);
			} finally {
				cnx.libera();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void processoRelatorioRenovacao() {
		try {
			String tpRenovacao = "M";

			if (cdEmpresaRel.trim().equals("")) {
				aviso("Empresa necessita estar Preenchido!!");
				return;
			}

			if (!QtData.ehData(dtInicial)) {
				aviso("Data Inicial necessita estar Preenchido!!");
				return;
			}

			if (!QtData.ehData(dtFinal)) {
				aviso("Data Final necessita estar Preenchido!!");
				return;
			}

			System.out.println("VAI IMPRIMIR...");
			QtData dtInicial = new QtData(this.dtInicial);
			QtData dtFinal = new QtData(this.dtFinal);

			if (tpExameRelRenovacao != null && !tpExameRelRenovacao.equals("")) {
				tpRenovacao = tpExameRelRenovacao;
			}

			Vector<DadosRenovacao> dados = new Vector<DadosRenovacao>();

			System.out.println("inicnando vetor... " + dados);

			geraListaDeRenovacao(dados, cdEmpresaRel, dtInicial, dtFinal, tpRenovacao);

			QtJrDataSource qt = new QtJrDataSource(dados);

			enviaArquivo("renovacao/relRenovacoes", qt);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void aviso(String msg) {
		System.out.println("AVISO:::: " + msg);
		msgAoUsuario = msg;
		msgExibida = 0;
	}

	private void geraListaDeRenovacao(Vector<DadosRenovacao> dados, String cdEmpresa, QtData dtInicial, QtData dtFinal,
			String tpRenovacao) {

		System.out.println("Iniciando relatório de renovação");
		try {

			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
			try {

				Query qry = new Query(cnx);

				qry.addSQL("select e.*, m.*, b.cd_beneficiario_cartao, b.ds_nome, p.*, ex.* ");
				qry.addSQL("from  pcmso.empresas e, ");
				qry.addSQL("pcmso.beneficiarios_cbo b, ");
				qry.addSQL("pcmso.exames_beneficiario ex, ");
				qry.addSQL("pcmso.procedimentos p , ");
				qry.addSQL("pcmso.medicos m ");
				qry.addSQL("	where ex.cd_beneficiario_cartao = b.cd_beneficiario_cartao ");
				qry.addSQL("and p.cd_procedimento = ex.cd_procedimento ");
				qry.addSQL("  and b.cd_empresa = e.cd_empresa ");
				qry.addSQL("  and m.cd_medico = e.cd_medico_COORDENADOR ");
				qry.addSQL("	and e.cd_empresa = :prmsCdEmpresa");
				if (dtFinal != null) {
					qry.addSQL(" and ( b.dt_exclusao is null or b.dt_exclusao >= :prmsDtExclusao ) ");
				} else {
					qry.addSQL(" and dt_exclusao is null ");
				}

				qry.setParameter("prmsCdEmpresa", cdEmpresa);
				if (dtFinal != null) {
					qry.setParameter("prmsDtExclusao", dtFinal);
				}

				qry.executeQuery();

				System.out.println(qry.preparaSQL());

				Empresa e = new Empresa();
				Medico m = new Medico();
				ExameBeneficiario eb = new ExameBeneficiario();

				// String cdBeneficiarioCartao = "";
				if (!qry.isEmpty()) {
					while (!qry.isAfterLast()) {

						EmpresaLocalizador.buscaCampos(e, qry);
						MedicoLocalizador.buscaCampos(m, qry);
						ExameBeneficiarioLocalizador.buscaCampos(eb, qry);

						// if( !qry.getString( "cd_Beneficiario_Cartao"
						// ).trim().equalsIgnoreCase(
						// cdBeneficiarioCartao.trim() ) ){
						// cdBeneficiarioCartao = qry.getString(
						// "cd_Beneficiario_Cartao" );
						// DadosRenovacao dr = BibliotecaExame.buscaUltimaExame(
						// cnx, cdBeneficiarioCartao, qry.getString( "ds_nome"
						// ), dtInicial, dtFinal, e , m, tpData );
						// if( dr != null ){
						// dados.add( dr );
						// }
						// }

						// cdBeneficiarioCartao = qry.getString(
						// "cd_Beneficiario_Cartao" );

						DadosRenovacao dr = null;

						if (tpRenovacao.equalsIgnoreCase("I")) {
							dr = BibliotecaExame.buscaExameComplementaresApartirInclusao(cnx, eb, dtInicial, dtFinal);
						} else if (tpRenovacao.equalsIgnoreCase("M")) {
							dr = BibliotecaExame.buscaUltimaExameComplementares(cnx, eb, dtInicial, dtFinal);
						} else if (tpRenovacao.equalsIgnoreCase("S")) {
							String snRenovacaoSemestral = qry.getString("sn_renovacao_semestral");
							if (snRenovacaoSemestral != null && snRenovacaoSemestral.equalsIgnoreCase("S")) {
								dr = BibliotecaExame.buscaExameComplementaresSemestre(cnx, eb, dtInicial, dtFinal);
							}
						}

						if (dr != null) {
							dr.setEmpresas(e);
							dr.setMedicos(m);
							dr.setDsNome(qry.getString("ds_nome"));
							dr.setCdProcedimento(qry.getString("cd_procedimento"));
							dr.setDsProcedimento(qry.getString("ds_procedimento"));
							dados.add(dr);
						}
						qry.next();
					}
				}

			} finally {
				cnx.libera();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void enviaArquivo(String nomeRelatorio, Hashtable<String, Object> parametros) {

		System.out.println("GERANDO RELATÓRIO EMISSAO ASO");
		try {

			String pathJasper = (String) ParametrosSistema.getInstance().getParametro("dirRelPCMSO");

			parametros.put("CaminhoPath", pathJasper);

			DadosRelatorioJasper relatorio = new DadosRelatorioJasper();

			relatorio.setBaseRelatorio(pathJasper + nomeRelatorio + ".jasper");
			relatorio.setParametros(parametros);

			Util.getSession().setAttribute("RelatorioPdf", relatorio);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void enviaArquivo(String nomeRelatorio, QtJrDataSource qt) {

		System.out.println("GERANDO RELATÓRIO EMISSAO ASO");
		try {

			String pathJasper = (String) ParametrosSistema.getInstance().getParametro("dirRelPCMSO");

			Hashtable<String, Object> parametros = new Hashtable<String, Object>();

			parametros.put("CaminhoPath", pathJasper);

			DadosRelatorioJasper relatorio = new DadosRelatorioJasper();

			relatorio.setBaseRelatorio(pathJasper + nomeRelatorio + ".jasper");
			relatorio.setParametros(parametros);

			Util.getSession().setAttribute("RelatorioPdf", relatorio);
			Util.getSession().setAttribute("QtJrDataSource", qt);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void geraRelatorio(String nrExame) {

		System.out.println("GERANDO RELATÓRIO EMISSAO ASO");
		try {
			String nomeRelatorio = "relEmissaoASOFichaClinica";

			String pathJasper = (String) ParametrosSistema.getInstance().getParametro("dirRelPCMSO");

			Hashtable<String, Object> parametros = new Hashtable<String, Object>();

			parametros.put("CaminhoPath", pathJasper);
			parametros.put("nrNota", nrExame);

			DadosRelatorioJasper relatorio = new DadosRelatorioJasper();

			relatorio.setBaseRelatorio(pathJasper + nomeRelatorio + ".jasper");
			relatorio.setParametros(parametros);

			Util.getSession().setAttribute("RelatorioPdf", relatorio);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getNomeBenef(ExameOcupacional exame) {
		if (exame != null) {
			try {

				ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
				try {
					BeneficiarioCbo beneficiarioCbo = BeneficiarioCboLocalizador
							.buscaBeneficiarioCbo(exame.getCdBeneficiarioCartao());
					if (beneficiarioCbo != null) {
						return beneficiarioCbo.getDsNome();
					}
				} finally {
					cnx.libera();
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	
	public String getNomeExaminador(ExameOcupacional exame) {
		
		if (exame != null) {
			try {

				ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
				try {
					Medico medico = MedicoLocalizador.buscaMedico(cnx, exame.getCdMedicoExaminador() );
					if ( medico != null) {
						return medico.getNmMedico();
					}
				} finally {
					cnx.libera();
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	public String getNomeProcedimento(String cdProcedimento) {
		if (cdProcedimento != null) {
			try {

				ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
				try {
					Procedimento proc = ProcedimentoLocalizador.buscaProcedimento(cnx, cdProcedimento);
					if (proc != null) {
						return proc.getDsProcedimento();
					}
				} finally {
					cnx.libera();
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	public List<ExameOcupacional> carregaListagem() {

		List<ExameOcupacional> lista = new LinkedList<>();

		try {
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
			ExameOcupacionalCaminhador cargos = null;
			try {
				FiltroDeNavegador filtro = new FiltroDeNavegador();
				filtro.setOrdemDeNavegacao(ExameOcupacionalCaminhador.POR_ORDEM_DEFAULT);

				if (pesquisaCodigo != null && !pesquisaCodigo.equals("")) {
					filtro.adicionaCondicao("cd_beneficiario_cartao", FiltroDeNavegador.IGUAL, pesquisaCodigo);
				}
				if (pesquisaDescricao != null && !pesquisaDescricao.equals("")) {
					filtro.adicionaCondicao("cd_procedimento", FiltroDeNavegador.ILIKE, "%" + pesquisaDescricao + "%");
				}

				cargos = new ExameOcupacionalCaminhador(cnx, filtro);
				cargos.ativaCaminhador();

				System.out.println(cargos.getClausulaSQL());
				if (!cargos.isEmpty()) {
					while (!cargos.isEof()) {
						lista.add(cargos.getExameOcupacional());
						cargos.proximo();
					}
				}

			} finally {
				cnx.libera();
				cargos.liberaRecursos();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return lista;
	}

	public void selecionaExameComplementar(ExameComplementar exame) {
		exameComplementarSelecionado = exame;
		Util.sendRedirect(LINK_MNT);
		abaExamesComplementares = "";
		abaDadosComplementares = "active";
	}

	public void limpaMensagem() {
		msgAoUsuario = null;
		msgExibida = 0;
	}

	public void selecionaAbaDados() {
		abaDados = "active";
		abaComplementares = "";
		selecionaAbaExamesComplementares();
	}

	public void selecionaAbaComplementares() {
		abaDados = "";
		abaComplementares = "active";
		selecionaAbaExamesComplementares();
	}

	public void selecionaAbaExamesComplementares() {
		abaExamesComplementares = "active";
		abaDadosComplementares = "";
	}

	public void selecionaAbaDadosExamesComplementares() {
		abaExamesComplementares = "";
		abaDadosComplementares = "active";
	}

	private void abreLista() {
		Util.sendRedirect(LINK_LISTA);
		abaDados = "active";
		abaComplementares = "";
		abaExamesComplementares = "active";
		abaDadosComplementares = "";
	}

	private void abreMnt() {
		Util.sendRedirect(LINK_MNT);
		abaDados = "active";
		abaComplementares = "";
		abaExamesComplementares = "active";
		abaDadosComplementares = "";
	}

	public void seleciona(ExameOcupacional cargo) {
		registroSelecionado = cargo;
		listagemExameComplementar = carregaExamesComplementares();
		abreMnt();

	}

	private List<ExameComplementar> carregaExamesComplementares() {
		List<ExameComplementar> lista = new LinkedList<>();
		try {
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
			try {
				FiltroDeNavegador filtro = new FiltroDeNavegador();
				filtro.adicionaCondicao("cd_beneficiario_cartao", FiltroDeNavegador.IGUAL,
						registroSelecionado.getCdBeneficiarioCartao());
				filtro.setOrdemDeNavegacao(ExameComplementarCaminhador.POR_ORDEM_DEFAULT);
				ExameComplementarCaminhador riscos = new ExameComplementarCaminhador(cnx, filtro);
				riscos.ativaCaminhador();
				
				System.out.println(riscos.getClausulaSQL());

				if (!riscos.isEmpty()) {
					while (!riscos.isEof()) {
						lista.add(riscos.getExameComplementar());
						riscos.proximo();
					}
				}

			} finally {
				cnx.libera();
			}
		} catch (Exception e) {
			e.printStackTrace();
			msgAoUsuario = "Ops! " + e.getMessage();
			msgExibida = 0;
		}

		return lista;
	}

	public void pesquisar() {
		listagem = carregaListagem();
		abreLista();
	}

	public void novo() {
		registroSelecionado = new ExameOcupacional();
		abreMnt();
	}

	public void limpaFiltro() {
		pesquisaCodigo = null;
		pesquisaDescricao = null;
		listagem = carregaListagem();
		abreLista();
	}

	public void cancelar() {
		pesquisaCodigo = null;
		pesquisaDescricao = null;
		listagem = carregaListagem();
		abreLista();
	}

	public void salvar() {
		try {
			System.out.println("Inserindo novo ");

			ExameOcupacionalNegocio cargo = new ExameOcupacionalNegocio(registroSelecionado);
			if (registroSelecionado != null) {
				if (ExameOcupacionalLocalizador.existe(registroSelecionado)) {
					cargo.altera();
					msgExibida = 0;
					msgAoUsuario = NOME_MNT + " alterado com sucesso";
				} else {
					cargo.insere();
					msgAoUsuario = NOME_MNT + " inserido com sucesso";
				}
			} else {
				cargo.insere();
				msgAoUsuario = NOME_MNT + " inserido com sucesso";
			}
			abreLista();
		} catch (Exception e) {
			msgExibida = 0;
			msgAoUsuario = "Ops! " + e.getMessage();
			e.printStackTrace();
		}
	}

	public void exclui(Cargo cargo) {
		try {
			System.out.println("Exluindo");

			CargoNegocio cargos = new CargoNegocio(cargo);
			cargos.exclui();

			msgAoUsuario = NOME_MNT + " excluído com sucesso";
			abreLista();
		} catch (Exception e) {
			msgExibida = 0;
			msgAoUsuario = "Ops! " + e.getMessage();
			e.printStackTrace();
		}
	}

	private String getNomeBeneficiario() {

		try {
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
			try {

				BeneficiarioCbo beneficiarioCbo = BeneficiarioCboLocalizador
						.buscaBeneficiarioCbo(cdBeneficiarioCartaoRel);
				if (beneficiarioCbo != null) {
					return beneficiarioCbo.getDsNome();
				} else {
					cdBeneficiarioCartaoRel = null;
					return "Não encontrado";
				}

			} finally {
				cnx.libera();
			}

		} catch (Exception e) {
			e.printStackTrace();
			msgAoUsuario = "Ops! " + e.getMessage();
			msgExibida = 0;
		}
		cdBeneficiarioCartaoRel = null;
		return null;
	}

	private String getNomeMedico() {
		try {
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
			try {
				Medico medico = MedicoLocalizador.buscaMedico(cnx, cdMedicoRel);
				if (medico != null) {
					return medico.getNmMedico();
				} else {
					cdMedicoRel = null;
					return "Não encontrado";
				}

			} finally {
				cnx.libera();
			}

		} catch (Exception e) {
			e.printStackTrace();
			msgAoUsuario = "Ops! " + e.getMessage();
			msgExibida = 0;
		}
		cdMedicoRel = null;
		return null;
	}

	private String getNomeEmpresa() {
		try {
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
			try {
				Empresa empresa = EmpresaLocalizador.buscaEmpresa(cnx, cdEmpresaRel);
				if (empresa != null) {
					return empresa.getDsEmpresa();
				} else {
					dsNomeEmpresa = null;
					return "Não encontrado";
				}

			} finally {
				cnx.libera();
			}

		} catch (Exception e) {
			e.printStackTrace();
			msgAoUsuario = "Ops! " + e.getMessage();
			msgExibida = 0;
		}
		dsNomeEmpresa = null;
		return null;
	}

	private String getNomeEmpresaFinal() {
		try {
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
			try {
				Empresa empresa = EmpresaLocalizador.buscaEmpresa(cnx, cdEmpresaRel);
				if (empresa != null) {
					return empresa.getDsEmpresa();
				} else {
					dsNomeEmpresaFinal = null;
					return "Não encontrado";
				}

			} finally {
				cnx.libera();
			}

		} catch (Exception e) {
			e.printStackTrace();
			msgAoUsuario = "Ops! " + e.getMessage();
			msgExibida = 0;
		}
		dsNomeEmpresaFinal = null;
		return null;
	}

	private String getNomeProcedimento() {
		try {
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
			try {
				Procedimento proc = ProcedimentoLocalizador.buscaProcedimento(cnx, cdProcedimento);
				if (proc != null) {
					return proc.getDsProcedimento();
				} else {
					dsProcedimento = null;
					return "Não encontrado";
				}

			} finally {
				cnx.libera();
			}

		} catch (Exception e) {
			e.printStackTrace();
			msgAoUsuario = "Ops! " + e.getMessage();
			msgExibida = 0;
		}
		dsProcedimento = null;
		return null;
	}

	private String getNomeProcedimentoFinal() {
		try {
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
			try {
				Procedimento proc = ProcedimentoLocalizador.buscaProcedimento(cnx, cdProcedimentoFinal);
				if (proc != null) {
					return proc.getDsProcedimento();
				} else {
					cdProcedimentoFinal = null;
					return "Não encontrado";
				}

			} finally {
				cnx.libera();
			}

		} catch (Exception e) {
			e.printStackTrace();
			msgAoUsuario = "Ops! " + e.getMessage();
			msgExibida = 0;
		}
		cdProcedimentoFinal = null;
		return null;
	}

	private String getNomeLotacao(Integer idLotacao) {
		try {
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
			try {
				Lotacao lotacao = LotacaoLocalizador.buscaLotacao(cnx,
						ParametrosSistema.getInstance().getCodigoDoCliente(), new Integer(cdEmpresaRel), idLotacao);
				if (lotacao != null) {
					return lotacao.getDsLotacao();
				} else {
					dsNomeLotacao = null;
					return "Não encontrado";
				}

			} finally {
				cnx.libera();
			}

		} catch (Exception e) {
			e.printStackTrace();
			msgAoUsuario = "Ops! " + e.getMessage();
			msgExibida = 0;
		}
		dsNomeLotacao = null;
		return null;
	}

	public String getMsgAoUsuario() {
		return msgAoUsuario;
	}

	public void setMsgAoUsuario(String msgAoUsuario) {
		this.msgAoUsuario = msgAoUsuario;
	}

	public Integer getMsgExibida() {
		return msgExibida;
	}

	public void setMsgExibida(Integer msgExibida) {
		this.msgExibida = msgExibida;
	}

	public ExameOcupacional getRegistroSelecionado() {
		return registroSelecionado;
	}

	public void setRegistroSelecionado(ExameOcupacional registroSelecionado) {
		this.registroSelecionado = registroSelecionado;
	}

	public String getPesquisaCodigo() {
		return pesquisaCodigo;
	}

	public void setPesquisaCodigo(String pesquisaCodigo) {
		this.pesquisaCodigo = pesquisaCodigo;
	}

	public String getPesquisaDescricao() {
		return pesquisaDescricao;
	}

	public void setPesquisaDescricao(String pesquisaDescricao) {
		this.pesquisaDescricao = pesquisaDescricao;
	}

	public List<ExameOcupacional> getListagem() {
		return listagem;
	}

	public void setListagem(List<ExameOcupacional> listagem) {
		this.listagem = listagem;
	}

	public List<ExameComplementar> getListagemExameComplementar() {
		return listagemExameComplementar;
	}

	public void setListagemExameComplementar(List<ExameComplementar> listagemExameComplementar) {
		this.listagemExameComplementar = listagemExameComplementar;
	}

	public ExameComplementar getExameComplementarSelecionado() {
		return exameComplementarSelecionado;
	}

	public void setExameComplementarSelecionado(ExameComplementar exameComplementarSelecionado) {
		this.exameComplementarSelecionado = exameComplementarSelecionado;
	}

	public String getAbaDados() {
		return abaDados;
	}

	public void setAbaDados(String abaDados) {
		this.abaDados = abaDados;
	}

	public String getAbaComplementares() {
		return abaComplementares;
	}

	public void setAbaComplementares(String abaComplementares) {
		this.abaComplementares = abaComplementares;
	}

	public String getAbaExamesComplementares() {
		return abaExamesComplementares;
	}

	public void setAbaExamesComplementares(String abaExamesComplementares) {
		this.abaExamesComplementares = abaExamesComplementares;
	}

	public String getAbaDadosComplementares() {
		return abaDadosComplementares;
	}

	public void setAbaDadosComplementares(String abaDadosComplementares) {
		this.abaDadosComplementares = abaDadosComplementares;
	}

	public String getCdBeneficiarioCartaoRel() {
		return cdBeneficiarioCartaoRel;
	}

	public void setCdBeneficiarioCartaoRel(String cdBeneficiarioCartaoRel) {
		this.cdBeneficiarioCartaoRel = cdBeneficiarioCartaoRel;
		if (this.cdBeneficiarioCartaoRel != null && !this.cdBeneficiarioCartaoRel.trim().equals("")) {
			dsNomeBeneficiario = getNomeBeneficiario();
		}
	}

	public String getDsNomeBeneficiario() {
		return dsNomeBeneficiario;
	}

	public void setDsNomeBeneficiario(String dsNomeBeneficiario) {
		this.dsNomeBeneficiario = dsNomeBeneficiario;
	}

	public String getCdMedicoRel() {
		return cdMedicoRel;
	}

	public void setCdMedicoRel(String cdMedicoRel) {
		this.cdMedicoRel = cdMedicoRel;

		if (this.cdMedicoRel != null && !this.cdMedicoRel.trim().equals("")) {
			dsNomeMedico = getNomeMedico();
		}
	}

	public String getDsNomeMedico() {
		return dsNomeMedico;
	}

	public void setDsNomeMedico(String dsNomeMedico) {
		this.dsNomeMedico = dsNomeMedico;
	}

	public String getTpExameRel() {
		return tpExameRel;
	}

	public void setTpExameRel(String tpExameRel) {
		this.tpExameRel = tpExameRel;
	}

	public String getCdEmpresaRel() {
		return cdEmpresaRel;
	}

	public void setCdEmpresaRel(String cdEmpresaRel) {
		this.cdEmpresaRel = cdEmpresaRel;
		if (this.cdEmpresaRel != null && !this.cdEmpresaRel.trim().equals("")) {
			dsNomeEmpresa = getNomeEmpresa();
		}
	}

	public String getTpExameRelRenovacao() {
		return tpExameRelRenovacao;
	}

	public void setTpExameRelRenovacao(String tpExameRelRenovacao) {
		this.tpExameRelRenovacao = tpExameRelRenovacao;
	}

	public String getDtInicial() {
		return dtInicial;
	}

	public void setDtInicial(String dtInicial) {
		this.dtInicial = dtInicial;
	}

	public String getDtFinal() {
		return dtFinal;
	}

	public void setDtFinal(String dtFinal) {
		this.dtFinal = dtFinal;
	}

	public String getDsNomeEmpresa() {
		return dsNomeEmpresa;
	}

	public void setDsNomeEmpresa(String dsNomeEmpresa) {
		this.dsNomeEmpresa = dsNomeEmpresa;
	}

	public Integer getIdLotacaoRelExame() {
		return idLotacaoRelExame;
	}

	public void setIdLotacaoRelExame(Integer idLotacaoRelExame) {
		this.idLotacaoRelExame = idLotacaoRelExame;
		if (this.idLotacaoRelExame != null) {
			dsNomeLotacao = getNomeLotacao(this.idLotacaoRelExame);
		}
	}

	public String getDsNomeLotacao() {
		return dsNomeLotacao;
	}

	public void setDsNomeLotacao(String dsNomeLotacao) {
		this.dsNomeLotacao = dsNomeLotacao;
	}

	public Integer getIdLotacaoFinalRelExame() {
		return idLotacaoFinalRelExame;
	}

	public void setIdLotacaoFinalRelExame(Integer idLotacaoFinalRelExame) {
		this.idLotacaoFinalRelExame = idLotacaoFinalRelExame;
		if (this.idLotacaoFinalRelExame != null) {
			dsNomeLotacaoFinal = getNomeLotacao(this.idLotacaoFinalRelExame);
		}
	}

	public String getDsNomeLotacaoFinal() {
		return dsNomeLotacaoFinal;
	}

	public void setDsNomeLotacaoFinal(String dsNomeLotacaoFinal) {
		this.dsNomeLotacaoFinal = dsNomeLotacaoFinal;
	}

	public String getCdEmpresaFinalRel() {
		return cdEmpresaFinalRel;
	}

	public void setCdEmpresaFinalRel(String cdEmpresaFinalRel) {
		this.cdEmpresaFinalRel = cdEmpresaFinalRel;
		if (this.cdEmpresaFinalRel != null && !this.cdEmpresaFinalRel.trim().equals("")) {
			dsNomeEmpresaFinal = getNomeEmpresaFinal();
		}
	}

	public String getDsNomeEmpresaFinal() {
		return dsNomeEmpresaFinal;
	}

	public void setDsNomeEmpresaFinal(String dsNomeEmpresaFinal) {
		this.dsNomeEmpresaFinal = dsNomeEmpresaFinal;
	}

	public String getCdProcedimento() {
		return cdProcedimento;
	}

	public void setCdProcedimento(String cdProcedimento) {
		this.cdProcedimento = cdProcedimento;
		if (this.cdProcedimento != null) {
			cdProcedimento = getNomeProcedimento();
		}
	}

	public String getDsProcedimento() {
		return dsProcedimento;
	}

	public void setDsProcedimento(String dsProcedimento) {
		this.dsProcedimento = dsProcedimento;
	}

	public String getCdProcedimentoFinal() {
		return cdProcedimentoFinal;
	}

	public void setCdProcedimentoFinal(String cdProcedimentoFinal) {
		this.cdProcedimentoFinal = cdProcedimentoFinal;
		if (this.cdProcedimentoFinal != null) {
			dsProcedimentoFinal = getNomeProcedimentoFinal();
		}
	}

	public String getDsProcedimentoFinal() {
		return dsProcedimentoFinal;
	}

	public void setDsProcedimentoFinal(String dsProcedimentoFinal) {
		this.dsProcedimentoFinal = dsProcedimentoFinal;
	}

	public Integer getNovoMesExame() {
		return novoMesExame;
	}

	public void setNovoMesExame(Integer novoMesExame) {
		this.novoMesExame = novoMesExame;
	}

	public Integer getDiasInicial() {
		return diasInicial;
	}

	public void setDiasInicial(Integer diasInicial) {
		this.diasInicial = diasInicial;
	}

	public Integer getDiasFinal() {
		return diasFinal;
	}

	public void setDiasFinal(Integer diasFinal) {
		this.diasFinal = diasFinal;
	}

}
