package jUni.bean.dados.manutencao;


import java.util.LinkedList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import jPcmso.negocio.geral.t.TipoDeRiscoNegocio;
import jPcmso.persistencia.geral.t.TipoDeRisco;
import jPcmso.persistencia.geral.t.TipoDeRiscoCaminhador;
import jPcmso.persistencia.geral.t.TipoDeRiscoLocalizador;
import jUni.util.Util;
import quatro.persistencia.FiltroDeNavegador;
import quatro.sql.ConexaoParaPoolDeConexoes;
import quatro.sql.PoolDeConexoes;

@SessionScoped
@ManagedBean
public class BeanTipoRiscoPCMSO {
	
	private static final String LINK_MNT = "resources/page/tipoRisco/mntTipoDeRisco.jsf";
	private static final String LINK_LISTA = "resources/page/tipoRisco/listaTipoDeRisco.jsf";
	private static final String NOME_MNT = "Tipo de Setor";

	private String pesquisaCodigo;
	private String pesquisaDescricao;

	List<TipoDeRisco> listagem;

	private TipoDeRisco selecionada;
	private String msgAoUsuario;
	private Integer msgExibida = 0;

	public BeanTipoRiscoPCMSO() {
		selecionada = new TipoDeRisco();
		listagem = new LinkedList<>();
		listagem = carregaListagem();
	}

	public void inic() {
		if (msgAoUsuario != null) {
			if (msgExibida > 0) {
				msgAoUsuario = null;
				msgExibida = 0;
			}
			msgExibida++;
		}
	}

	public List<TipoDeRisco> carregaListagem() {

		List<TipoDeRisco> lista = new LinkedList<>();

		try {
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
			TipoDeRiscoCaminhador cargos = null;
			try {
				FiltroDeNavegador filtro = new FiltroDeNavegador();
				filtro.setOrdemDeNavegacao(TipoDeRiscoCaminhador.POR_ORDEM_DEFAULT);

				if (pesquisaCodigo != null && !pesquisaCodigo.equals("")) {
					filtro.adicionaCondicao("tp_risco", FiltroDeNavegador.IGUAL, pesquisaCodigo );
				}
				if (pesquisaDescricao != null && !pesquisaDescricao.equals("")) {
					filtro.adicionaCondicao("ds_setor", FiltroDeNavegador.ILIKE, "%" + pesquisaDescricao + "%");
				}

				cargos = new TipoDeRiscoCaminhador(cnx, filtro);
				cargos.ativaCaminhador();

				if (!cargos.isEmpty()) {
					while (!cargos.isEof()) {
						lista.add(cargos.getTipoDeRisco());
						cargos.proximo();
					}
				}

			} finally {
				cnx.libera();
				cargos.liberaRecursos();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return lista;
	}

	public void limpaMensagem() {
		msgAoUsuario = null;
		msgExibida = 0;
	}

	private void abreLista() {
		Util.sendRedirect(LINK_LISTA);
	}

	private void abreMnt() {
		Util.sendRedirect(LINK_MNT);
	}

	public void seleciona(TipoDeRisco objeto) {
		selecionada = objeto;
		abreMnt();
	}

	public void pesquisar() {
		listagem = carregaListagem();
		abreLista();
	}

	public void novo() {
		selecionada = new TipoDeRisco();
		abreMnt();
	}

	public void limpaFiltro() {
		pesquisaCodigo = null;
		pesquisaDescricao = null;
		listagem = carregaListagem();
		abreLista();
	}

	public void cancelar() {
		pesquisaCodigo = null;
		pesquisaDescricao = null;
		listagem = carregaListagem();
		abreLista();
	}

	public void salvar() {
		try {
			System.out.println("Inserindo novo ");

			TipoDeRiscoNegocio cargo = new TipoDeRiscoNegocio(selecionada);
			if (selecionada.getTpRisco() != null) {
				if (TipoDeRiscoLocalizador.existe(selecionada)) {
					cargo.altera();
					msgExibida = 0;
					msgAoUsuario = NOME_MNT + " alterado com sucesso";
				} else {
					cargo.insere();
					msgAoUsuario = NOME_MNT + " inserido com sucesso";
				}
			} else {
				cargo.insere();
				msgAoUsuario = NOME_MNT + " inserido com sucesso";
			}
			abreLista();
		} catch (Exception e) {
			msgExibida = 0;
			msgAoUsuario = "Ops! " + e.getMessage();
			e.printStackTrace();
		}
	}

	public void exclui(TipoDeRisco cargo) {
		try {
			System.out.println("Exluindo");

			TipoDeRiscoNegocio cargos = new TipoDeRiscoNegocio(cargo);
			cargos.exclui();

			msgAoUsuario = NOME_MNT + " excluído com sucesso";
			abreLista();
		} catch (Exception e) {
			msgExibida = 0;
			msgAoUsuario = "Ops! " + e.getMessage();
			e.printStackTrace();
		}
	}

	public String getPesquisaCodigo() {
		return pesquisaCodigo;
	}

	public void setPesquisaCodigo(String pesquisaCodigo) {
		this.pesquisaCodigo = pesquisaCodigo;
	}

	public String getPesquisaDescricao() {
		return pesquisaDescricao;
	}

	public void setPesquisaDescricao(String pesquisaDescricao) {
		this.pesquisaDescricao = pesquisaDescricao;
	}

	public List<TipoDeRisco> getListagem() {
		return listagem;
	}

	public void setListagem(List<TipoDeRisco> listagem) {
		this.listagem = listagem;
	}

	public TipoDeRisco getSelecionada() {
		return selecionada;
	}

	public void setSelecionada(TipoDeRisco selecionada) {
		this.selecionada = selecionada;
	}

	public String getMsgAoUsuario() {
		return msgAoUsuario;
	}

	public void setMsgAoUsuario(String msgAoUsuario) {
		this.msgAoUsuario = msgAoUsuario;
	}

	public Integer getMsgExibida() {
		return msgExibida;
	}

	public void setMsgExibida(Integer msgExibida) {
		this.msgExibida = msgExibida;
	}
	
	

}
