package jUni.bean.dados.manutencao;

import java.util.LinkedList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import jPcmso.negocio.geral.s.SetorNegocio;
import jPcmso.persistencia.geral.s.Setor;
import jPcmso.persistencia.geral.s.SetorCaminhador;
import jPcmso.persistencia.geral.s.SetorLocalizador;
import jUni.util.Util;
import quatro.persistencia.FiltroDeNavegador;
import quatro.sql.ConexaoParaPoolDeConexoes;
import quatro.sql.PoolDeConexoes;

@SessionScoped
@ManagedBean
public class BeanSetoresPCMSO {

	private static final String LINK_MNT = "resources/page/setor/mntSetore.jsf";
	private static final String LINK_LISTA = "resources/page/setor/listaSetores.jsf";
	private static final String NOME_MNT = "Setor";

	private String pesquisaCodigo;
	private String pesquisaDescricao;

	List<Setor> listagem;

	private Setor selecionada;
	private String msgAoUsuario;
	private Integer msgExibida = 0;

	public BeanSetoresPCMSO() {
		selecionada = new Setor();
		listagem = new LinkedList<>();
		listagem = carregaListagem();
	}

	public void inic() {
		if (msgAoUsuario != null) {
			if (msgExibida > 0) {
				msgAoUsuario = null;
				msgExibida = 0;
			}
			msgExibida++;
		}
	}

	public List<Setor> carregaListagem() {

		List<Setor> lista = new LinkedList<>();

		try {
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
			SetorCaminhador cargos = null;
			try {
				FiltroDeNavegador filtro = new FiltroDeNavegador();
				filtro.setOrdemDeNavegacao(SetorCaminhador.POR_ORDEM_DEFAULT);

				if (pesquisaCodigo != null && !pesquisaCodigo.equals("")) {
					filtro.adicionaCondicao("id_setor", FiltroDeNavegador.IGUAL, new Integer(pesquisaCodigo));
				}
				if (pesquisaDescricao != null && !pesquisaDescricao.equals("")) {
					filtro.adicionaCondicao("ds_setor", FiltroDeNavegador.ILIKE, "%" + pesquisaDescricao + "%");
				}

				cargos = new SetorCaminhador(cnx, filtro);
				cargos.ativaCaminhador();

				if (!cargos.isEmpty()) {
					while (!cargos.isEof()) {
						lista.add(cargos.getSetor());
						cargos.proximo();
					}
				}

			} finally {
				cnx.libera();
				cargos.liberaRecursos();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return lista;
	}

	public void limpaMensagem() {
		msgAoUsuario = null;
		msgExibida = 0;
	}

	private void abreLista() {
		Util.sendRedirect(LINK_LISTA);
	}

	private void abreMnt() {
		Util.sendRedirect(LINK_MNT);
	}

	public void seleciona(Setor objeto) {
		selecionada = objeto;
		abreMnt();
	}

	public void pesquisar() {
		listagem = carregaListagem();
		abreLista();
	}

	public void novo() {
		selecionada = new Setor();
		abreMnt();
	}

	public void limpaFiltro() {
		pesquisaCodigo = null;
		pesquisaDescricao = null;
		listagem = carregaListagem();
		abreLista();
	}

	public void cancelar() {
		pesquisaCodigo = null;
		pesquisaDescricao = null;
		listagem = carregaListagem();
		abreLista();
	}

	public void salvar() {
		try {
			System.out.println("Inserindo novo ");

			SetorNegocio cargo = new SetorNegocio(selecionada);
			if (selecionada.getIdSetor() != null) {
				if (SetorLocalizador.existe(selecionada)) {
					cargo.altera();
					msgExibida = 0;
					msgAoUsuario = NOME_MNT + " alterado com sucesso";
				} else {
					cargo.insere();
					msgAoUsuario = NOME_MNT + " inserido com sucesso";
				}
			} else {
				cargo.insere();
				msgAoUsuario = NOME_MNT + " inserido com sucesso";
			}
			abreLista();
		} catch (Exception e) {
			msgExibida = 0;
			msgAoUsuario = "Ops! " + e.getMessage();
			e.printStackTrace();
		}
	}

	public void exclui(Setor cargo) {
		try {
			System.out.println("Exluindo");

			SetorNegocio cargos = new SetorNegocio(cargo);
			cargos.exclui();

			msgAoUsuario = NOME_MNT + " excluído com sucesso";
			abreLista();
		} catch (Exception e) {
			msgExibida = 0;
			msgAoUsuario = "Ops! " + e.getMessage();
			e.printStackTrace();
		}
	}

	public String getPesquisaCodigo() {
		return pesquisaCodigo;
	}

	public void setPesquisaCodigo(String pesquisaCodigo) {
		this.pesquisaCodigo = pesquisaCodigo;
	}

	public String getPesquisaDescricao() {
		return pesquisaDescricao;
	}

	public void setPesquisaDescricao(String pesquisaDescricao) {
		this.pesquisaDescricao = pesquisaDescricao;
	}

	public List<Setor> getListagem() {
		return listagem;
	}

	public void setListagem(List<Setor> listagem) {
		this.listagem = listagem;
	}

	public Setor getSelecionada() {
		return selecionada;
	}

	public void setSelecionada(Setor selecionada) {
		this.selecionada = selecionada;
	}

	public String getMsgAoUsuario() {
		return msgAoUsuario;
	}

	public void setMsgAoUsuario(String msgAoUsuario) {
		this.msgAoUsuario = msgAoUsuario;
	}

	public Integer getMsgExibida() {
		return msgExibida;
	}

	public void setMsgExibida(Integer msgExibida) {
		this.msgExibida = msgExibida;
	}

}
