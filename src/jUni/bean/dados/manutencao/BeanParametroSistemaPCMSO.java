package jUni.bean.dados.manutencao;

import java.util.LinkedList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import jUni.controleLogin.Usuario;
import jUni.controleLogin.UsuarioLogado;
import jUni.util.Util;
import quatro.sql.Conexao;
import quatro.sql.ConexaoParaPoolDeConexoes;
import quatro.sql.PoolDeConexoes;
import quatro.sql.Query;
import quatro.util.QtData;

@SessionScoped
@ManagedBean
public class BeanParametroSistemaPCMSO {

	private static final String LINK_MNT = "resources/page/parametros/parametroSistema.jsf";

	List<DadosParametroSistema> listagem;
	private String msgAoUsuario;
	private Integer msgExibida = 0;
	private boolean estadoAtual;
	private Usuario usuarioLogado;

	public BeanParametroSistemaPCMSO() {
		listagem = new LinkedList<>();
		listagem = carregaListagem();
		if (usuarioLogado == null) {
			usuarioLogado = jUni.controleLogin.Util.getDadosUsuarioLogado();
		}
		try {
			estadoAtual = sistemaAtivo();
		} catch (Exception e) {
			estadoAtual = false;
			e.printStackTrace();
		}
	}

	public void inic() {
		if (msgAoUsuario != null) {
			if (msgExibida > 0) {
				msgAoUsuario = null;
				msgExibida = 0;
			}
			msgExibida++;
		}
	}
	
	private boolean sistemaAtivo( Conexao cnx )  {
		
		try {
			Query q = new Query(cnx);
			q.setSQL("select * from pcmso.parametro_pcmso order by id_parametro desc limit 1");
			q.executeQuery();
	
			if (!q.isEmpty()) {
				System.out.println("Sistema ativo? " + q.getBoolean("sn_ativo"));
				return q.getBoolean("sn_ativo");
			}
		} catch ( Exception e ) {
			e.printStackTrace();
		}
		return false;
	}

	private boolean sistemaAtivo() throws Exception {
		try {
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
			return sistemaAtivo( cnx );

		} catch (Exception e) {
			throw new Exception(e);
		}
	}

	public List<DadosParametroSistema> carregaListagem() {

		List<DadosParametroSistema> lista = new LinkedList<>();

		try {
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
			try {
				Query q = new Query(cnx);
				q.setSQL("select * from pcmso.parametro_pcmso order by id_parametro desc limit 10");
				q.executeQuery();

				if (!q.isEmpty()) {
					while (!q.isAfterLast()) {
						DadosParametroSistema dados = new DadosParametroSistema();
						dados.setIdParametro(q.getInteger("id_parametro"));
						dados.setSnAtivo(q.getBoolean("sn_ativo"));
						dados.setDtAlteracao( q.getTimestamp("dt_alteracao") != null ? new QtData(q.getTimestamp("dt_alteracao") ) : null);
						dados.setIdUsuario( q.getInteger("id_usuario") );
						dados.setDsUsuario( q.getString("ds_usuario") );
						lista.add(dados);
						q.next();
					}
				}

			} finally {
				cnx.libera();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return lista;
	}

	public void limpaMensagem() {
		msgAoUsuario = null;
		msgExibida = 0;
	}

	public class DadosParametroSistema {
		private Integer idParametro;
		private Boolean snAtivo;
		private QtData dtAlteracao;
		private String dsUsuario;
		private Integer idUsuario;
		
		public String getDsUsuario() {
			return dsUsuario;
		}

		public void setDsUsuario(String dsUsuario) {
			this.dsUsuario = dsUsuario;
		}

		public Integer getIdUsuario() {
			return idUsuario;
		}

		public void setIdUsuario(Integer idUsuario) {
			this.idUsuario = idUsuario;
		}

		public Integer getIdParametro() {
			return idParametro;
		}

		public void setIdParametro(Integer idParametro) {
			this.idParametro = idParametro;
		}

		public Boolean getSnAtivo() {
			return snAtivo;
		}

		public void setSnAtivo(Boolean snAtivo) {
			this.snAtivo = snAtivo;
		}

		public QtData getDtAlteracao() {
			return dtAlteracao;
		}

		public void setDtAlteracao(QtData dtAlteracao) {
			this.dtAlteracao = dtAlteracao;
		}

	}

	private void abreMnt() {
		Util.sendRedirect(LINK_MNT);
	}

	public void salvarEstado() {
		try {
			System.out.println("Inserindo novo ");
			
			try {
				ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
				try {
					
					Query q = new Query(cnx);
					q.setSQL("insert into pcmso.parametro_pcmso ( sn_ativo, dt_alteracao, id_usuario, ds_usuario ) values ( :prmSnAtivo, now(), :prmIdUsuario, :prmDsUsuario );");
					q.setParameter("prmSnAtivo", !estadoAtual );
					q.setParameter("prmIdUsuario", usuarioLogado.getIdUsuario() );
					q.setParameter("prmDsUsuario", usuarioLogado.getDsNomeUsuario() );
					q.executeUpdate();
					
					listagem = carregaListagem();
					estadoAtual = sistemaAtivo( cnx );

				} finally {
					cnx.libera();
				}

			} catch (Exception e) {
				throw new Exception(e);
			}
			
			abreMnt();
		} catch (Exception e) {
			msgExibida = 0;
			msgAoUsuario = "Ops! " + e.getMessage();
			e.printStackTrace();
		}
	}

	public String getMsgAoUsuario() {
		return msgAoUsuario;
	}

	public void setMsgAoUsuario(String msgAoUsuario) {
		this.msgAoUsuario = msgAoUsuario;
	}

	public Integer getMsgExibida() {
		return msgExibida;
	}

	public void setMsgExibida(Integer msgExibida) {
		this.msgExibida = msgExibida;
	}

	public List<DadosParametroSistema> getListagem() {
		return listagem;
	}

	public void setListagem(List<DadosParametroSistema> listagem) {
		this.listagem = listagem;
	}

	public boolean isEstadoAtual() {
		return estadoAtual;
	}

	public void setEstadoAtual(boolean estadoAtual) {
		this.estadoAtual = estadoAtual;
	}

}
