package jUni.bean.dados.manutencao;


import java.util.LinkedList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import jPcmso.negocio.geral.c.CargoNegocio;
import jPcmso.negocio.geral.m.MedicoNegocio;
import jPcmso.persistencia.geral.c.Cargo;
import jPcmso.persistencia.geral.m.Medico;
import jPcmso.persistencia.geral.m.MedicoCaminhador;
import jPcmso.persistencia.geral.m.MedicoLocalizador;
import jUni.util.Util;
import quatro.persistencia.FiltroDeNavegador;
import quatro.sql.ConexaoParaPoolDeConexoes;
import quatro.sql.PoolDeConexoes;

@SessionScoped
@ManagedBean
public class BeanMedicoPCMSO {

	private static final String LINK_MNT = "resources/page/medico/mntMedico.jsf";
	private static final String LINK_LISTA = "resources/page/medico/listaMedicos.jsf";
	private static final String NOME_MNT = "Medico";

	List<Medico> listagem;
	private Medico registroSelecionado;
	private String msgAoUsuario;
	private Integer msgExibida = 0;

	private String pesquisaCodigo;
	private String pesquisaDescricao;

	public BeanMedicoPCMSO() {
		registroSelecionado = new Medico();
		listagem = new LinkedList<>();
		listagem = carregaListagem();
	}

	public void inic() {
		if (msgAoUsuario != null) {
			if (msgExibida > 0) {
				msgAoUsuario = null;
				msgExibida = 0;
			}
			msgExibida++;
		}
	}
	
	public List<Medico> carregaListagem() {

		List<Medico> lista = new LinkedList<>();

		try {
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
			MedicoCaminhador cargos = null;
			try {
				FiltroDeNavegador filtro = new FiltroDeNavegador();
				filtro.setOrdemDeNavegacao(MedicoCaminhador.POR_ORDEM_DEFAULT);

				if (pesquisaCodigo != null && !pesquisaCodigo.equals("")) {
					filtro.adicionaCondicao("cd_empresa", FiltroDeNavegador.IGUAL, pesquisaCodigo );
				}
				if (pesquisaDescricao != null && !pesquisaDescricao.equals("")) {
					filtro.adicionaCondicao("ds_empresa", FiltroDeNavegador.ILIKE, "%" + pesquisaDescricao + "%");
				}

				cargos = new MedicoCaminhador(cnx, filtro);
				cargos.ativaCaminhador();

				if (!cargos.isEmpty()) {
					while (!cargos.isEof()) {
						lista.add(cargos.getMedico());
						cargos.proximo();
					}
				}

			} finally {
				cnx.libera();
				cargos.liberaRecursos();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return lista;
	}

	public void limpaMensagem() {
		msgAoUsuario = null;
		msgExibida = 0;
	}

	private void abreLista() {
		Util.sendRedirect(LINK_LISTA);
	}

	private void abreMnt() {
		Util.sendRedirect(LINK_MNT);
	}

	public void seleciona(Medico cargo) {
		registroSelecionado = cargo;
		abreMnt();
	}

	public void pesquisar() {
		listagem = carregaListagem();
		abreLista();
	}

	public void novo() {
		registroSelecionado = new Medico();
		abreMnt();
	}

	public void limpaFiltro() {
		pesquisaCodigo = null;
		pesquisaDescricao = null;
		listagem = carregaListagem();
		abreLista();
	}

	public void cancelar() {
		pesquisaCodigo = null;
		pesquisaDescricao = null;
		listagem = carregaListagem();
		abreLista();
	}

	public void salvar() {
		try {
			System.out.println("Inserindo novo ");

			MedicoNegocio cargo = new MedicoNegocio(registroSelecionado);
			if (registroSelecionado.getCdMedico() != null) {
				if (MedicoLocalizador.existe(registroSelecionado)) {
					cargo.altera();
					msgExibida = 0;
					msgAoUsuario = NOME_MNT + " alterado com sucesso";
				} else {
					cargo.insere();
					msgAoUsuario = NOME_MNT + " inserido com sucesso";
				}
			} else {
				cargo.insere();
				msgAoUsuario = NOME_MNT + " inserido com sucesso";
			}
			abreLista();
		} catch (Exception e) {
			msgExibida = 0;
			msgAoUsuario = "Ops! " + e.getMessage();
			e.printStackTrace();
		}
	}

	public void exclui(Cargo cargo) {
		try {
			System.out.println("Exluindo");

			CargoNegocio cargos = new CargoNegocio(cargo);
			cargos.exclui();

			msgAoUsuario = NOME_MNT + " excluído com sucesso";
			abreLista();
		} catch (Exception e) {
			msgExibida = 0;
			msgAoUsuario = "Ops! " + e.getMessage();
			e.printStackTrace();
		}
	}

	public String getMsgAoUsuario() {
		return msgAoUsuario;
	}

	public void setMsgAoUsuario(String msgAoUsuario) {
		this.msgAoUsuario = msgAoUsuario;
	}

	public Integer getMsgExibida() {
		return msgExibida;
	}

	public void setMsgExibida(Integer msgExibida) {
		this.msgExibida = msgExibida;
	}

	public Medico getRegistroSelecionado() {
		return registroSelecionado;
	}

	public void setRegistroSelecionado(Medico registroSelecionado) {
		this.registroSelecionado = registroSelecionado;
	}

	public String getPesquisaCodigo() {
		return pesquisaCodigo;
	}

	public void setPesquisaCodigo(String pesquisaCodigo) {
		this.pesquisaCodigo = pesquisaCodigo;
	}

	public String getPesquisaDescricao() {
		return pesquisaDescricao;
	}

	public void setPesquisaDescricao(String pesquisaDescricao) {
		this.pesquisaDescricao = pesquisaDescricao;
	}

	public List<Medico> getListagem() {
		return listagem;
	}

	public void setListagem(List<Medico> listagem) {
		this.listagem = listagem;
	}

}
