package jUni.bean.dados;

import jUni.biblioteca.util.Util;

public class DadosBeneficiarios {
	
	public static final int ALFANUMERICO_DIREITA = 0;
	public static final int NUMERICO_ESQUERDA = 1;
	
	public static String getLinha(DadosBeneficiarios dados) {
		// TODO Auto-generated constructor stub
		StringBuilder sb = new StringBuilder();
		sb.append(dados.getIdentificacao());
		sb.append(dados.getNrCnpj());
		sb.append(dados.getDsNome());
		sb.append(dados.getDtNascimento());
		sb.append(dados.getDsSexo());
		sb.append(dados.getCdCtps());
		sb.append(dados.getDsSerieCtps());
		sb.append(dados.getDtEmissaoCtps());
		sb.append(dados.getUfCtps());
		sb.append(dados.getNrIdentidade());
		sb.append(dados.getDsOrgaoEmissor());
		sb.append(dados.getDtEmissaoIdentidade());
		sb.append(dados.getUfIdentidade());
		sb.append(dados.getCdNit());
		sb.append(dados.getNrCpf());
		sb.append(dados.getCdMatricula());
		sb.append(dados.getIdVinculo());
		sb.append(dados.getCdBrPhd());
		sb.append(dados.getDsRegimeRevezamento());
		sb.append(dados.getDtAdmissao());
		sb.append(dados.getDtDemissao());
		sb.append(dados.getDsObservacao());
		sb.append(dados.getDsEndereco());
		sb.append(dados.getDsCidade());
		sb.append(dados.getDsBairro());
		sb.append(dados.getDsEstado());
		sb.append(dados.getCdCep());
		sb.append(dados.getDsTelefone());
		sb.append(dados.getVlRemuneracaoMensal());
		sb.append(dados.getNmMae());
		sb.append(dados.getDsFiliacaoPrevidencia());
		sb.append(dados.getDsEstadoCivil());
		sb.append(dados.getIdAposentado());
		sb.append(dados.getCdTituloEleitor());
		sb.append(dados.getCdCNH());
		sb.append(dados.getDtValidadeCNH());
		sb.append(dados.getDtInicioSetor());
		sb.append(dados.getDtSaidaSetor());
		sb.append(dados.getCdSetor());
		sb.append(dados.getNmSetor());
		sb.append(dados.getCdCargo());
		sb.append(dados.getNmCargo());
		sb.append(dados.getDsCargoDesenvolvido());
		sb.append(dados.getCdCBOCargo());
		sb.append(dados.getCdRFID());
		sb.append(dados.getCdBarras());
		sb.append(dados.getDsTurno());
		sb.append(dados.getCdTurnoJornada());
		sb.append(dados.getDsGrupoSanguineo());
		sb.append(dados.getIdDeficiencia());
		sb.append(dados.getCdFuncao());
		sb.append(dados.getCdIntegracaoEmpresa());
		
		return sb.toString();
	}

	private String identificacao;
	private String nrCnpj;
	private String dsNome;
	private String dtNascimento;
	private String dsSexo;
	private String cdCtps;
	private String dsSerieCtps;
	private String dtEmissaoCtps;
	private String ufCtps;
	private String nrIdentidade;
	private String dsOrgaoEmissor;
	private String dtEmissaoIdentidade;
	private String ufIdentidade;
	private String cdNit;
	private String nrCpf;
	private String cdMatricula;
	private Integer idVinculo;
	private String cdBrPhd;
	private String dsRegimeRevezamento;
	private String dtAdmissao;
	private String dtDemissao;
	private String dsObservacao;
	private String dsEndereco;
	private String dsCidade;
	private String dsBairro;
	private String dsEstado;
	private String cdCep;
	private String dsTelefone;
	private String vlRemuneracaoMensal;
	private String nmMae;
	private Integer dsFiliacaoPrevidencia;
	private Integer dsEstadoCivil;
	private Integer idAposentado;
	private String cdTituloEleitor;
	private String cdCNH;
	private String dtValidadeCNH;
	private String dtInicioSetor;
	private String dtSaidaSetor;
	private String cdSetor;
	private String nmSetor;
	private String cdCargo;
	private String nmCargo;
	private String dsCargoDesenvolvido;
	private String cdCBOCargo;
	private String cdRFID;
	private String cdBarras;
	private String dsTurno;
	private String cdTurnoJornada;
	private String dsGrupoSanguineo;
	private Integer idDeficiencia;
	private String cdFuncao;
	private String cdIntegracaoEmpresa;

	public String getIdentificacao() {
		identificacao = "02";
		return identificacao;
	}

	public void setIdentificacao(String identificacao) {
		identificacao = identificacao;
	}

	public String getNrCnpj() {
		return getAjustaRegistro(nrCnpj, 18, NUMERICO_ESQUERDA);
	}

	public void setNrCnpj(String nrCnpj) {
		this.nrCnpj = nrCnpj;
	}

	public String getDsNome() {
		return getAjustaRegistro(dsNome, 50, ALFANUMERICO_DIREITA);
	}

	public void setDsNome(String dsNome) {
		this.dsNome = dsNome;
	}

	public String getDtNascimento() {
		return getAjustaRegistro(dtNascimento, 10, ALFANUMERICO_DIREITA);
	}

	public void setDtNascimento(String dtNascimento) {
		this.dtNascimento = dtNascimento;
	}

	public String getDsSexo() {
		return getAjustaRegistro(dsSexo, 1, ALFANUMERICO_DIREITA);
	}

	public void setDsSexo(String dsSexo) {
		this.dsSexo = dsSexo;
	}

	public String getCdCtps() {
		return getAjustaRegistro(cdCtps, 10, NUMERICO_ESQUERDA);
	}

	public void setCdCtps(String cdCtps) {
		this.cdCtps = cdCtps;
	}

	public String getDsSerieCtps() {
		return getAjustaRegistro(dsSerieCtps, 6, NUMERICO_ESQUERDA);
	}

	public void setDsSerieCtps(String dsSerieCtps) {
		this.dsSerieCtps = dsSerieCtps;
	}

	public String getUfCtps() {
		return getAjustaRegistro(ufCtps, 2, ALFANUMERICO_DIREITA );
	}

	public void setUfCtps(String ufCtps) {
		this.ufCtps = ufCtps;
	}

	public String getNrIdentidade() {
		return getAjustaRegistro(nrIdentidade, 15, ALFANUMERICO_DIREITA );
	}

	public void setNrIdentidade(String nrIdentidade) {
		this.nrIdentidade = nrIdentidade;
	}

	public String getDsOrgaoEmissor() {
		return getAjustaRegistro(dsOrgaoEmissor, 3, NUMERICO_ESQUERDA );
	}

	public void setDsOrgaoEmissor(String dsOrgaoEmissor) {
		this.dsOrgaoEmissor = dsOrgaoEmissor;
	}

	public String getUfIdentidade() {
		return getAjustaRegistro(ufIdentidade, 2, ALFANUMERICO_DIREITA);
	}

	public void setUfIdentidade(String ufIdentidade) {
		this.ufIdentidade = ufIdentidade;
	}

	public String getCdNit() {
		return getAjustaRegistro(cdNit, 15, ALFANUMERICO_DIREITA);
	}

	public void setCdNit(String cdNit) {
		this.cdNit = cdNit;
	}

	public String getNrCpf() {
		return getAjustaRegistro(nrCpf, 15, ALFANUMERICO_DIREITA);
	}

	public void setNrCpf(String nrCpf) {
		this.nrCpf = nrCpf;
	}

	public String getCdMatricula() {
		return getAjustaRegistro(cdMatricula, 20, ALFANUMERICO_DIREITA);
	}

	public void setCdMatricula(String cdMatricula) {
		this.cdMatricula = cdMatricula;
	}

	public Integer getIdVinculo() {
		return idVinculo;
	}

	public void setIdVinculo(Integer idVinculo) {
		this.idVinculo = idVinculo;
	}

	public String getCdBrPhd() {
		return getAjustaRegistro(cdBrPhd, 3, ALFANUMERICO_DIREITA);
	}

	public void setCdBrPhd(String cdBrPhd) {
		this.cdBrPhd = cdBrPhd;
	}

	public String getDsRegimeRevezamento() {
		return getAjustaRegistro(dsRegimeRevezamento, 15, ALFANUMERICO_DIREITA);
	}

	public String getDtEmissaoCtps() {
		return getAjustaRegistro(dtEmissaoCtps, 10, ALFANUMERICO_DIREITA);
	}

	public void setDtEmissaoCtps(String dtEmissaoCtps) {
		this.dtEmissaoCtps = dtEmissaoCtps;
	}

	public String getDtEmissaoIdentidade() {
		return getAjustaRegistro(dtEmissaoIdentidade, 10, ALFANUMERICO_DIREITA);
	}

	public void setDtEmissaoIdentidade(String dtEmissaoIdentidade) {
		this.dtEmissaoIdentidade = dtEmissaoIdentidade;
	}

	public String getDtAdmissao() {
		return getAjustaRegistro(dtAdmissao, 10, ALFANUMERICO_DIREITA);
	}

	public void setDtAdmissao(String dtAdmissao) {
		this.dtAdmissao = dtAdmissao;
	}

	public String getDtDemissao() {
		return getAjustaRegistro(dtDemissao, 10, ALFANUMERICO_DIREITA);
	}

	public void setDtDemissao(String dtDemissao) {
		this.dtDemissao = dtDemissao;
	}

	public void setDsRegimeRevezamento(String dsRegimeRevezamento) {
		this.dsRegimeRevezamento = dsRegimeRevezamento;
	}

	public String getDsObservacao() {
		return getAjustaRegistro(dsObservacao, 100, ALFANUMERICO_DIREITA);
	}

	public void setDsObservacao(String dsObservacao) {
		this.dsObservacao = dsObservacao;
	}

	public String getDsEndereco() {
		return getAjustaRegistro(dsEndereco, 50, ALFANUMERICO_DIREITA);
	}

	public void setDsEndereco(String dsEndereco) {
		this.dsEndereco = dsEndereco;
	}

	public String getDsCidade() {
		return getAjustaRegistro(dsCidade, 30, ALFANUMERICO_DIREITA);
	}

	public void setDsCidade(String dsCidade) {
		this.dsCidade = dsCidade;
	}

	public String getDsBairro() {
		return getAjustaRegistro(dsBairro, 40, ALFANUMERICO_DIREITA);
	}

	public void setDsBairro(String dsBairro) {
		this.dsBairro = dsBairro;
	}

	public String getDsEstado() {
		return getAjustaRegistro(dsEstado, 2, ALFANUMERICO_DIREITA);
	}

	public void setDsEstado(String dsEstado) {
		this.dsEstado = dsEstado;
	}

	public String getCdCep() {
		return getAjustaRegistro(cdCep, 10, ALFANUMERICO_DIREITA);
	}

	public void setCdCep(String cdCep) {
		this.cdCep = cdCep;
	}

	public String getDsTelefone() {
		return getAjustaRegistro(dsTelefone, 15, ALFANUMERICO_DIREITA);
	}

	public void setDsTelefone(String dsTelefone) {
		this.dsTelefone = dsTelefone;
	}

	public String getVlRemuneracaoMensal() {
		return getAjustaRegistro(vlRemuneracaoMensal, 15, ALFANUMERICO_DIREITA);
	}

	public void setVlRemuneracaoMensal(String vlRemuneracaoMensal) {
		this.vlRemuneracaoMensal = vlRemuneracaoMensal;
	}

	public String getNmMae() {
		return getAjustaRegistro(nmMae, 40, ALFANUMERICO_DIREITA);
	}

	public void setNmMae(String nmMae) {
		this.nmMae = nmMae;
	}

	public Integer getDsFiliacaoPrevidencia() {
		return dsFiliacaoPrevidencia;
	}

	public void setDsFiliacaoPrevidencia(Integer dsFiliacaoPrevidencia) {
		this.dsFiliacaoPrevidencia = dsFiliacaoPrevidencia;
	}

	public Integer getDsEstadoCivil() {
		return dsEstadoCivil;
	}

	public void setDsEstadoCivil(Integer dsEstadoCivil) {
		this.dsEstadoCivil = dsEstadoCivil;
	}

	public Integer getIdAposentado() {
		return idAposentado;
	}

	public void setIdAposentado(Integer idAposentado) {
		this.idAposentado = idAposentado;
	}

	public String getCdTituloEleitor() {
		return getAjustaRegistro(cdTituloEleitor, 12, ALFANUMERICO_DIREITA);
	}

	public void setCdTituloEleitor(String cdTituloEleitor) {
		this.cdTituloEleitor = cdTituloEleitor;
	}

	public String getCdCNH() {
		return getAjustaRegistro(cdCNH, 15, ALFANUMERICO_DIREITA);
	}

	public void setCdCNH(String cdCNH) {
		this.cdCNH = cdCNH;
	}

	public String getDtValidadeCNH() {
		return getAjustaRegistro(dtValidadeCNH, 10, ALFANUMERICO_DIREITA);
	}

	public void setDtValidadeCNH(String dtValidadeCNH) {
		this.dtValidadeCNH = dtValidadeCNH;
	}

	public String getDtInicioSetor() {
		return getAjustaRegistro(dtInicioSetor, 10, ALFANUMERICO_DIREITA);
	}

	public void setDtInicioSetor(String dtInicioSetor) {
		this.dtInicioSetor = dtInicioSetor;
	}

	public String getDtSaidaSetor() {
		return getAjustaRegistro(dtSaidaSetor, 10, ALFANUMERICO_DIREITA);
	}

	public void setDtSaidaSetor(String dtSaidaSetor) {
		this.dtSaidaSetor = dtSaidaSetor;
	}

	public String getCdSetor() {
		return getAjustaRegistro(cdSetor, 12, ALFANUMERICO_DIREITA);
	}

	public void setCdSetor(String cdSetor) {
		this.cdSetor = cdSetor;
	}

	public String getNmSetor() {
		return getAjustaRegistro(nmSetor, 100, ALFANUMERICO_DIREITA);
	}

	public void setNmSetor(String nmSetor) {
		this.nmSetor = nmSetor;
	}

	public String getCdCargo() {
		return getAjustaRegistro(cdCargo, 15, ALFANUMERICO_DIREITA);
	}

	public void setCdCargo(String cdCargo) {
		this.cdCargo = cdCargo;
	}

	public String getNmCargo() {
		return getAjustaRegistro(nmCargo, 100, ALFANUMERICO_DIREITA);
	}

	public void setNmCargo(String nmCargo) {
		this.nmCargo = nmCargo;
	}

	public String getDsCargoDesenvolvido() {
		return getAjustaRegistro(dsCargoDesenvolvido, 100, ALFANUMERICO_DIREITA);
	}

	public void setDsCargoDesenvolvido(String dsCargoDesenvolvido) {
		this.dsCargoDesenvolvido = dsCargoDesenvolvido;
	}

	public String getCdCBOCargo() {
		return getAjustaRegistro(cdCBOCargo, 7, ALFANUMERICO_DIREITA);
	}

	public void setCdCBOCargo(String cdCBOCargo) {
		this.cdCBOCargo = cdCBOCargo;
	}

	public String getCdRFID() {
		return getAjustaRegistro(cdRFID, 100, ALFANUMERICO_DIREITA);
	}

	public void setCdRFID(String cdRFID) {
		this.cdRFID = cdRFID;
	}

	public String getCdBarras() {
		return getAjustaRegistro(cdBarras, 100, ALFANUMERICO_DIREITA);
	}

	public void setCdBarras(String cdBarras) {
		this.cdBarras = cdBarras;
	}

	public String getDsTurno() {
		return getAjustaRegistro(dsTurno, 100, ALFANUMERICO_DIREITA);
	}

	public void setDsTurno(String dsTurno) {
		this.dsTurno = dsTurno;
	}

	public String getCdTurnoJornada() {
		return getAjustaRegistro(cdTurnoJornada, 4, NUMERICO_ESQUERDA);
	}

	public void setCdTurnoJornada(String cdTurnoJornada) {
		this.cdTurnoJornada = cdTurnoJornada;
	}

	public String getDsGrupoSanguineo() {
		return getAjustaRegistro(dsGrupoSanguineo, 2, ALFANUMERICO_DIREITA);
	}

	public void setDsGrupoSanguineo(String dsGrupoSanguineo) {
		this.dsGrupoSanguineo = dsGrupoSanguineo;
	}

	public Integer getIdDeficiencia() {
		return idDeficiencia;
	}

	public void setIdDeficiencia(Integer idDeficiencia) {
		this.idDeficiencia = idDeficiencia;
	}

	public String getCdFuncao() {
		return getAjustaRegistro(cdFuncao, 20, ALFANUMERICO_DIREITA);
	}
	
	public void setCdIntegracaoEmpresa(String cdIntegracaoEmpresa) {
		this.cdIntegracaoEmpresa = cdIntegracaoEmpresa;
	}
	
	public String getCdIntegracaoEmpresa() {
		return getAjustaRegistro(cdIntegracaoEmpresa, 100, ALFANUMERICO_DIREITA);
	}

	public void setCdFuncao(String cdFuncao) {
		this.cdFuncao = cdFuncao;
	}

	private static String getAjustaRegistro(String registro, Integer tamanho, Integer ladoBrancos ) {
		if (registro == null) {
			registro = " ";
		} else {
			if (registro.length() > tamanho) {
				registro = registro.substring(0, tamanho);
			}
		}
		
		return Util.strSpace(registro.trim(), tamanho, ladoBrancos);
	}
	
}
