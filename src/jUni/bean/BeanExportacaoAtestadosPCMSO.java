package jUni.bean;


import java.io.IOException;
import java.io.OutputStream;
import java.sql.SQLException;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;

import jPcmso.persistencia.geral.b.BeneficiarioCbo;
import jPcmso.persistencia.geral.b.BeneficiarioCboLocalizador;
import jPcmso.persistencia.geral.e.Empresa;
import jPcmso.persistencia.geral.e.EmpresaLocalizador;
import jPcmso.persistencia.geral.e.ExameComplementar;
import jPcmso.persistencia.geral.e.ExameComplementarCaminhador;
import jPcmso.persistencia.geral.e.ExameOcupacional;
import jPcmso.persistencia.geral.e.ExameOcupacionalLocalizador;
import jPcmso.persistencia.geral.f.FiliaisEmpresa;
import jPcmso.persistencia.geral.f.FiliaisEmpresaLocalizador;
import jPcmso.persistencia.geral.m.Medico;
import jPcmso.persistencia.geral.m.MedicoLocalizador;
import jUni.bean.dados.DadosAtestados;
import jUni.biblioteca.util.Util;
import jUni.persistencia.geral.b.Beneficiario;
import jUni.persistencia.geral.b.BeneficiarioLocalizador;
import quatro.persistencia.FiltroDeNavegador;
import quatro.sql.Conexao;
import quatro.sql.ConexaoParaPoolDeConexoes;
import quatro.sql.PoolDeConexoes;
import quatro.sql.QtSQLException;
import quatro.util.DigitoVerificador;
import quatro.util.MD5;
import quatro.util.QtData;

@SessionScoped
@ManagedBean
public class BeanExportacaoAtestadosPCMSO {

	private String msgAoUsuario;
	private Integer msgExibida = 0;
	private String dtInicial;
	private String dtFinal;

	public BeanExportacaoAtestadosPCMSO()  {
		dtInicial = new QtData().toString();
		dtFinal = new QtData().toString();
	}

	public void inic() {
		if (msgAoUsuario != null) {
			if (msgExibida > 0) {
				msgAoUsuario = null;
				msgExibida = 0;
			}
			msgExibida++;
		}
	}

	public void gerarRelatorio() {
		try {
			executaDownloadArquivoExportacao();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void executaDownloadArquivoExportacao() throws SQLException {

		FacesContext fc = FacesContext.getCurrentInstance();
		try {

			HttpServletResponse response = ((HttpServletResponse) fc.getExternalContext().getResponse());
			fc.responseComplete();
			response.setContentType("text/plain");

			String data = new QtData().toString();

			response.setHeader("Content-Disposition", "attachment;filename=RelatorioAtestadosPCMSO_" + data + ".txt");
			OutputStream respOs = response.getOutputStream();

			respOs.write(preencheArquivo().getBytes());

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private static String getCnpjFilial(Conexao cnx, String empresa, Integer idFilial) throws QtSQLException {
		FiliaisEmpresa filial = FiliaisEmpresaLocalizador.buscaFiliaisEmpresa(cnx, empresa, idFilial );
		if( filial != null ) {
			if( filial.getNrCnpj() != null ) {
				return filial.getNrCnpj();
			}
		}
		return "";
	}

	private String preencheArquivo() {
		StringBuilder sb = new StringBuilder();
		try {

			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
			try {
				
				FiltroDeNavegador filtro = new FiltroDeNavegador();
				
				if( dtInicial != null ) {
					filtro.adicionaCondicao( "dt_exame_complementar", FiltroDeNavegador.MAIOR_OU_IGUAL, dtInicial );
				}
				if( dtFinal != null ) {
					filtro.adicionaCondicao( "dt_exame_complementar", FiltroDeNavegador.MENOR_OU_IGUAL, dtFinal );
				}
				filtro.adicionaCondicao( "cd_procedimento", FiltroDeNavegador.IGUAL, "00001111" );
				
				
				filtro.setOrdemDeNavegacao(ExameComplementarCaminhador.POR_ORDEM_DEFAULT);
				ExameComplementarCaminhador itens = new ExameComplementarCaminhador(cnx, filtro);
				itens.ativaCaminhador();

				if (!itens.isEmpty()) {
					while (!itens.isEof()) {
						ExameComplementar item = itens.getExameComplementar();

						DadosAtestados dados = new DadosAtestados();
						ExameOcupacional exame = ExameOcupacionalLocalizador.buscaExameOcupacional(cnx, item.getNrExame() );
						
						if( verificaEmpresaAtiva( cnx, exame.getCdBeneficiarioCartao() ) ) {
							Beneficiario beneficiario = BeneficiarioLocalizador.buscaporCodigoCartao(cnx, exame.getCdBeneficiarioCartao() );
							
							dados.setCdIdentificacao("01");
							if( beneficiario != null ) {
								dados.setNrCnpj(getCnpjEmpresa(cnx, beneficiario.getCdEmpresa() ) );
								dados.setNrCpf(beneficiario.getCdCpf());
								dados.setDsNome( Util.trocaAcentuacao( beneficiario.getDsNome() ) );
								dados.setCdNIT( getNit(cnx, beneficiario ) );
								dados.setNrMatricula(  beneficiario.getCdBeneficiarioCartao() + DigitoVerificador.calculaDigito(beneficiario.getCdUnimed() + beneficiario.getCdBeneficiarioCartao() ) );
								
							} else {
								dados.setNrCnpj( "" );
								dados.setNrCpf("");
								dados.setDsNome( "" );
								dados.setCdNIT( "" );
								dados.setNrMatricula(  exame.getCdBeneficiarioCartao() + DigitoVerificador.calculaDigito( "0270" + exame.getCdBeneficiarioCartao() ) );
							}
							dados.setCdIntegracaoEmpresa("");
							if( beneficiario != null && beneficiario.getIdFilial() != null ) {
								dados.setCdIntegracaoEmpresa( getCnpjFilial( cnx, beneficiario.getCdEmpresa(), beneficiario.getIdFilial() ) );
							} else {
								BeneficiarioCbo beneficiarioCbo = BeneficiarioCboLocalizador.buscaBeneficiarioCbo(cnx, exame.getCdBeneficiarioCartao() );
								if( beneficiarioCbo != null && beneficiarioCbo.getIdFilial() != null ) {
									dados.setCdIntegracaoEmpresa( getCnpjFilial( cnx, beneficiarioCbo.getCdEmpresa(), beneficiarioCbo.getIdFilial() ) );
								}
							}
							
							if( item.getDtExameComplementar() != null ) {
								String data = "";
								data += Util.strZero( item.getDtExameComplementar().getNrDia(), 2);
								data += Util.strZero(item.getDtExameComplementar().getNrMes(), 2);
								data += item.getDtExameComplementar().getNrAno();
								dados.setDtExame( data );
							} else {
								dados.setDtExame("");
							}
							
							Medico medico = MedicoLocalizador.buscaMedico(cnx, exame.getCdMedicoExaminador());
							
							dados.setTpExame(exame.getTpExame());
							dados.setTpSubExame("");
							if( medico != null ) {
								dados.setDsMedico( Util.trocaAcentuacao( medico.getNmMedico() ) );
							}
							dados.setCdCrm(exame.getCdMedicoExaminador());
							
							if( item.getTpResultado() != null ) {
								if( item.getTpResultado().equals( "N" ) ) {
									dados.setTpAptidao("1");
								} else {
									dados.setTpAptidao("2");
								}
							} else {
								dados.setTpAptidao("1");
							}
							
							dados.setDsAtividadeComplementar( Util.trocaAcentuacao( exame.getDsAtividade() ) );
		
							dados.setDsUnimedAtendimento("");
							sb.append(DadosAtestados.getLinha(dados) + "\n");
						} else {
							System.out.println("EMPRESA INATIVA");
						}

						itens.proximo();
					}
				}
				
			} finally {
				cnx.libera();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return sb.toString();
	}

	private boolean verificaEmpresaAtiva( Conexao cnx, String cdBeneficiarioCartao) {
		try {
			jUni.persistencia.geral.e.Empresa empresa = jUni.persistencia.geral.e.EmpresaLocalizador.buscaEmpresa(cnx, "0270", new Integer(cdBeneficiarioCartao.substring(0,4)) );
			if (empresa != null) {
				if (empresa.getDtExclusao() == null) {
					return true;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	private String getNit(Conexao cnx, Beneficiario beneficiario) throws QtSQLException {
		
		BeneficiarioCbo benefCbo = BeneficiarioCboLocalizador.buscaBeneficiarioCbo(cnx, beneficiario.getCdEmpresa() + beneficiario.getCdFamilia()
						+ beneficiario.getCdDependencia() );
		if (benefCbo != null) {
			return benefCbo.getNrPis();
		} 
		return "";
	}

	private String getCnpjEmpresa(Conexao cnx, String cdEmpresa) {

		try {
			Empresa empresa = EmpresaLocalizador.buscaEmpresa(cnx, cdEmpresa);
			if (empresa != null) {
				
				String retorno = empresa.getNrCpfRepresentante();
				
				if (empresa.getNrCnpj() != null && !empresa.getNrCnpj().trim().equals("")) {
					retorno = empresa.getNrCnpj();
				}
				
				jUni.persistencia.geral.e.Empresa empGeral = jUni.persistencia.geral.e.EmpresaLocalizador.buscaPorCodigosDeEmpresa( cnx,  new Integer( cdEmpresa ) );
				if( empGeral != null && empGeral.getNrCeiSefip() != null && !empGeral.getNrCeiSefip().equals( "" ) ) {
					retorno = empGeral.getNrCeiSefip();
				}
				
				return retorno;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public String getDtInicial() {
		return dtInicial;
	}

	public void setDtInicial(String dtInicial) {
		this.dtInicial = dtInicial;
	}

	public String getDtFinal() {
		return dtFinal;
	}

	public void setDtFinal(String dtFinal) {
		this.dtFinal = dtFinal;
	}

}
