package jUni.bean;


import java.io.IOException;
import java.io.OutputStream;
import java.sql.SQLException;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;

import jPcmso.persistencia.geral.b.BeneficiarioCbo;
import jPcmso.persistencia.geral.b.BeneficiarioCboLocalizador;
import jPcmso.persistencia.geral.e.Empresa;
import jPcmso.persistencia.geral.e.EmpresaLocalizador;
import jPcmso.persistencia.geral.e.ExameComplementar;
import jPcmso.persistencia.geral.e.ExameComplementarCaminhador;
import jPcmso.persistencia.geral.e.ExameOcupacional;
import jPcmso.persistencia.geral.e.ExameOcupacionalLocalizador;
import jPcmso.persistencia.geral.f.FiliaisEmpresa;
import jPcmso.persistencia.geral.f.FiliaisEmpresaLocalizador;
import jPcmso.persistencia.geral.p.Procedimento;
import jPcmso.persistencia.geral.p.ProcedimentoLocalizador;
import jUni.bean.dados.DadosExame;
import jUni.biblioteca.util.Util;
import jUni.persistencia.geral.b.Beneficiario;
import jUni.persistencia.geral.b.BeneficiarioLocalizador;
import quatro.persistencia.FiltroDeNavegador;
import quatro.sql.Conexao;
import quatro.sql.ConexaoParaPoolDeConexoes;
import quatro.sql.PoolDeConexoes;
import quatro.sql.QtSQLException;
import quatro.util.DigitoVerificador;
import quatro.util.QtData;

@SessionScoped
@ManagedBean
public class BeanExportacaoSESMTPCMSO {

	private String msgAoUsuario;
	private Integer msgExibida = 0;
	private String dtInicial;
	private String dtFinal;

	public BeanExportacaoSESMTPCMSO()  {
		dtInicial = new QtData().toString();
		dtFinal = new QtData().toString();
	}

	public void inic() {
		if (msgAoUsuario != null) {
			if (msgExibida > 0) {
				msgAoUsuario = null;
				msgExibida = 0;
			}
			msgExibida++;
		}
	}

	public void gerarRelatorio() {
		try {
			executaDownloadArquivoExportacao();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void executaDownloadArquivoExportacao() throws SQLException {

		FacesContext fc = FacesContext.getCurrentInstance();
		try {

			HttpServletResponse response = ((HttpServletResponse) fc.getExternalContext().getResponse());
			fc.responseComplete();
			response.setContentType("text/plain");

			String data = new QtData().toString();

			response.setHeader("Content-Disposition", "attachment;filename=RelatorioExamesPCMSO_" + data + ".txt");
			OutputStream respOs = response.getOutputStream();

			respOs.write(preencheArquivo().getBytes());

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static String getCnpjFilial(Conexao cnx, String empresa, Integer idFilial) throws QtSQLException {
		FiliaisEmpresa filial = FiliaisEmpresaLocalizador.buscaFiliaisEmpresa(cnx, empresa, idFilial );
		if( filial != null ) {
			if( filial.getNrCnpj() != null ) {
				return filial.getNrCnpj();
			}
		}
		return "";
	}
	
	private String preencheArquivo() {
		StringBuilder sb = new StringBuilder();
		try {

			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
			FiltroDeNavegador filtro = new FiltroDeNavegador();
			
			if( dtInicial != null ) {
				filtro.adicionaCondicao( "dt_exame_complementar", FiltroDeNavegador.MAIOR_OU_IGUAL, dtInicial );
			}
			if( dtFinal != null ) {
				filtro.adicionaCondicao( "dt_exame_complementar", FiltroDeNavegador.MENOR_OU_IGUAL, dtFinal );
			}
			filtro.adicionaCondicao( "cd_procedimento", FiltroDeNavegador.DIFERENTE, "00001111" );
			
			
			filtro.setOrdemDeNavegacao(ExameComplementarCaminhador.POR_ORDEM_DEFAULT);
			ExameComplementarCaminhador itens = new ExameComplementarCaminhador(cnx, filtro);
			itens.ativaCaminhador();

			if (!itens.isEmpty()) {
				while (!itens.isEof()) {
					ExameComplementar item = itens.getExameComplementar();

					DadosExame dados = new DadosExame();
						
					ExameOcupacional exame = ExameOcupacionalLocalizador.buscaExameOcupacional(cnx, item.getNrExame() );
					Beneficiario beneficiario = BeneficiarioLocalizador.buscaporCodigoCartao(cnx, exame.getCdBeneficiarioCartao() );
					
					Procedimento procedimento = ProcedimentoLocalizador.buscaProcedimento(cnx, item.getCdProcedimento() );
					
					dados.setCdIdentificacao("01");
					if( beneficiario != null ) {
						dados.setNrCnpj(getCnpjEmpresa(cnx, beneficiario.getCdEmpresa() ) );
						dados.setNrCpf(beneficiario.getCdCpf());
						dados.setDsNome( Util.trocaAcentuacao( beneficiario.getDsNome() ) );
						dados.setCdNIT( getNit(cnx, beneficiario ) );
						String digito = "";
						if( exame.getCdBeneficiarioCartao() != null) {
							digito = DigitoVerificador.calculaDigito( beneficiario.getCdUnimed() + exame.getCdBeneficiarioCartao() ) + "";
						}
						dados.setNrMatricula(  exame.getCdBeneficiarioCartao() + digito );
					} else {
						dados.setNrCnpj( "" );
						dados.setNrCpf("");
						dados.setDsNome( "" );
						dados.setCdNIT( "" );
						dados.setNrMatricula(  exame.getCdBeneficiarioCartao() + DigitoVerificador.calculaDigito( "0270" + exame.getCdBeneficiarioCartao() ) );
						
					}
					dados.setCdIntegracaoEmpresa("");
					if( beneficiario != null && beneficiario.getIdFilial() != null ) {
						dados.setCdIntegracaoEmpresa( getCnpjFilial( cnx, beneficiario.getCdEmpresa(), beneficiario.getIdFilial() ) );
					} else {
						BeneficiarioCbo beneficiarioCbo = BeneficiarioCboLocalizador.buscaBeneficiarioCbo(cnx, exame.getCdBeneficiarioCartao() );
						if( beneficiarioCbo != null && beneficiarioCbo.getIdFilial() != null ) {
							dados.setCdIntegracaoEmpresa( getCnpjFilial( cnx, beneficiarioCbo.getCdEmpresa(), beneficiarioCbo.getIdFilial() ) );
						}
					}
					
					dados.setDsExame( procedimento != null ? Util.trocaAcentuacao( procedimento.getDsProcedimento() ): "" );
					dados.setCdExame(  item.getCdProcedimento() );
					if( item.getDtExameComplementar() != null ) {
						String data = "";
						data += Util.strZero( item.getDtExameComplementar().getNrDia(), 2);
						data += Util.strZero(item.getDtExameComplementar().getNrMes(), 2);
						data += item.getDtExameComplementar().getNrAno();
						dados.setDtExame( data );
					} else {
						dados.setDtExame("");
					}

					/*
					 * O - Obrigat�rio R - Recomendado
					 */
					dados.setTpExigencia("O");

					/**
					 * A - Admissional P - Peri�dico D - Demissional M -
					 * Mudan�a de Fun��o R - Retorno ao Trabalho S -
					 * Semestral
					 */
					
					dados.setTpExame(exame.getTpExame());

					/**
					 * R - Referencial S - Sequencial
					 */
					dados.setTpReferencia("R");

					/**
					 * N - Normal AE - Alterado Est�vel AAO - Alterado
					 * Agravamento Ocupacional AAN - Alterado Agravamento
					 * N�o Ocupacional
					 */
					
					if( item.getTpResultado() != null ) {
						if( item.getTpResultado().equals( "A" ) ) {
							dados.setTpInterpretacao("AAN");
						} else {
							dados.setTpInterpretacao("N");
						}
					} else {
						dados.setTpInterpretacao("N");
					}
					
					dados.setDsUnimedAtendimento("");
					sb.append(DadosExame.getLinha(dados) + "\n");

					itens.proximo();
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return sb.toString();
	}

	private String getNit(ConexaoParaPoolDeConexoes cnx, Beneficiario beneficiario) throws QtSQLException {
		
		BeneficiarioCbo benefCbo = BeneficiarioCboLocalizador.buscaBeneficiarioCbo(cnx, beneficiario.getCdEmpresa() + beneficiario.getCdFamilia()
						+ beneficiario.getCdDependencia() );
		if (benefCbo != null) {
			return benefCbo.getNrPis();
		} 
		return "";
	}

	private String getCnpjEmpresa(ConexaoParaPoolDeConexoes cnx, String cdEmpresa) {

		try {
			Empresa empresa = EmpresaLocalizador.buscaEmpresa(cnx, cdEmpresa);
			if (empresa != null) {
				
				String retorno = empresa.getNrCpfRepresentante();
				
				if (empresa.getNrCnpj() != null && !empresa.getNrCnpj().trim().equals("")) {
					retorno = empresa.getNrCnpj();
				}
				
				jUni.persistencia.geral.e.Empresa empGeral = jUni.persistencia.geral.e.EmpresaLocalizador.buscaPorCodigosDeEmpresa( cnx,  new Integer( cdEmpresa ) );
				if( empGeral != null && empGeral.getNrCeiSefip() != null && !empGeral.getNrCeiSefip().equals( "" ) ) {
					retorno = empGeral.getNrCeiSefip();
				}
				
				return retorno;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public String getDtInicial() {
		return dtInicial;
	}

	public void setDtInicial(String dtInicial) {
		this.dtInicial = dtInicial;
	}

	public String getDtFinal() {
		return dtFinal;
	}

	public void setDtFinal(String dtFinal) {
		this.dtFinal = dtFinal;
	}

}
