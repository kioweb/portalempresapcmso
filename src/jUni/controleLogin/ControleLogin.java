package jUni.controleLogin;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.servlet.http.HttpSession;

import jUni.util.Util;
import quatro.smartDB.Localizador;
import quatro.sql.ConexaoParaPoolDeConexoes;
import quatro.sql.PoolDeConexoes;
import quatro.sql.QtSQLException;
import quatro.sql.Query;
import quatro.util.MD5;

@SessionScoped
@ManagedBean
public class ControleLogin {

	private Usuario usuario;
	private String nomeUsuario;
	private String senhaUsuario;
	private String tpUsuario;

	private String msgErro;

	private String dsSenhaAtualizacao;
	private String dsSenhaAtualizacaoConfirmacao;

	public void inicControleLogin() {

		// HttpSession sessao = Util.getSession();
		// Usuario user = new Usuario();
		// user = (Usuario) sessao.getAttribute( "UsuarioLogado" );
		//
		//
		// if( user != null ) {
		// Util.redirect( "../home.jsf" );
		// }

	}

	public void sair() {
		System.out.println("INVALIDANDO SESSÃO");
		System.out.println("INVALIDANDO SESSÃO");
		System.out.println("INVALIDANDO SESSÃO");
		System.out.println("INVALIDANDO SESSÃO");
		System.out.println("INVALIDANDO SESSÃO");
		System.out.println("INVALIDANDO SESSÃO");
		HttpSession sessao = Util.getSession();
		sessao.invalidate();
	}

	public void vaiTelaLogin() {
		System.out.println("Login...");
		Util.sendRedirect("/sair.jsp");
	}
	
	public void atualizaSenha() {
		System.out.println("Atualizando senha");

		Usuario result = (Usuario) Util.getSession().getAttribute("UsuarioLogado");
		System.out.println("Usuário da Sessão " + result.getDsNomeUsuario());

		if (dsSenhaAtualizacao != null && dsSenhaAtualizacaoConfirmacao != null) {
			if (!dsSenhaAtualizacao.equals("") && !dsSenhaAtualizacaoConfirmacao.equals("")) {
				if (dsSenhaAtualizacao.equals(dsSenhaAtualizacaoConfirmacao)) {
					if( dsSenhaAtualizacao.length() >= 6 ) {
	
						try {
							
							ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao("jDesk");
							try {
								
								Localizador<Usuario> lcUsu = new Localizador<Usuario>(Usuario.class, cnx);
								lcUsu.setHuman(true);
								lcUsu.setShowCommand(true);

								lcUsu.localizaPorAlternateKey("login", nomeUsuario);

								if (lcUsu.isFound()) {
									Query q = new Query(cnx);
									q.setSQL("update jdesk.usuario set dssenha = :prmInfoSenha");
									q.addSQL("where idusuario = :prmIdUsuario ");
									q.setParameter("prmIdUsuario", result.getIdUsuario() );
									q.setParameter("prmInfoSenha", codificaSenha(dsSenhaAtualizacao, result.getIdUsuario()) );
									q.executeUpdate();
									
									msgErro = "Sua senha foi alterada. Faça o login novamente!";
									Util.sendRedirect("/resources/login/senhaAlterada.jsf");
									sair();
									
								} else {
									msgErro = "Ops! Usuário não encontrado";
								}
								
							} finally {
								cnx.libera();
							}
							
						} catch( Exception e ) {
							e.printStackTrace();
							msgErro = "Ops! " + e.getMessage();
						}
						
					} else {
						msgErro = "Ops! A senha precisa conter pelo menos 6 caracteres.";
					}
					
				} else {
					msgErro = "Ops! As senhas precisam ser iguais.";
				}
			} else {
				msgErro = "Ops! Preencha os dois campos.";
			}
		} else {
			msgErro = "Ops! Preencha os dois campos.";
		}

	}

	public void testaLogin() {

		try {
			tpUsuario = null;
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao("jDesk");

			try {
				Localizador<Usuario> lcUsu = new Localizador<Usuario>(Usuario.class, cnx);
				lcUsu.setHuman(true);
				lcUsu.setShowCommand(true);

				lcUsu.localizaPorAlternateKey("login", nomeUsuario);

				if (lcUsu.isFound()) {

					usuario = lcUsu.getRegistro();
					String infoSenha = codificaSenha(senhaUsuario, usuario.getIdUsuario());

					if (usuario.getDsSenha().equals(infoSenha)) {

						System.out.println("Setando Usuario " + usuario.getDsNomeUsuario());

						Util.getSession().setAttribute("UsuarioLogado", usuario);

						Usuario result = (Usuario) Util.getSession().getAttribute("UsuarioLogado");
						System.out.println("Usuário da Sessão " + result.getDsNomeUsuario());
						
						if (usuario.getSnAlterarSenha().equals("N")) {
							msgErro = null;
							Util.getSession().setAttribute("UsuarioDoSistema", usuario);
							Util.sendRedirect("../page/dashboard.jsf");
						} else {
							msgErro = "Ops! Sua senha precisa ser redefinida!";
							Util.redirect("redefinirSenha.jsf");
						}
					} else {
						msgErro = "Usuário ou senha inválidos";
					}
				} else {
					msgErro = "Usuário ou senha inválidos";
				}
			} finally {
				cnx.libera();
			}
		} catch (QtSQLException e) {
			msgErro = "Falha na comunicação com o  Banco.";
			e.printStackTrace();
		}
	}

	public static String codificaSenha(String senhaUsuario, Integer idUsuario) {

		String infoSenha = MD5.md5(senhaUsuario);

		return infoSenha;
	}

	public String getNomeUsuario() {
		return nomeUsuario;
	}

	public void setNomeUsuario(String nomeUsuario) {
		this.nomeUsuario = nomeUsuario;
	}

	public String getSenhaUsuario() {
		return senhaUsuario;
	}

	public void setSenhaUsuario(String senhaUsuario) {
		this.senhaUsuario = senhaUsuario;
	}

	public String getMsgErro() {
		return msgErro;
	}

	public boolean isErroLogin() {
		return msgErro != null;
	}

	public String getTpUsuario() {
		return tpUsuario;
	}

	public void setTpUsuario(String tpUsuario) {
		this.tpUsuario = tpUsuario;
	}

	public void setMsgErro(String msgErro) {
		this.msgErro = msgErro;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public String getDsSenhaAtualizacao() {
		return dsSenhaAtualizacao;
	}

	public void setDsSenhaAtualizacao(String dsSenhaAtualizacao) {
		this.dsSenhaAtualizacao = dsSenhaAtualizacao;
	}

	public String getDsSenhaAtualizacaoConfirmacao() {
		return dsSenhaAtualizacaoConfirmacao;
	}

	public void setDsSenhaAtualizacaoConfirmacao(String dsSenhaAtualizacaoConfirmacao) {
		this.dsSenhaAtualizacaoConfirmacao = dsSenhaAtualizacaoConfirmacao;
	}

}