package jUni.controleLogin;

import java.sql.Timestamp;

public class DadosUsuariojDesk {

	private Integer idUsuario;
	private String dsNomeUsuario;
	private String dsLogin;
	private String dsSenha;
	private Integer nrValidadeSenha;
	private String dsEmail;
	private String snAdministrador;
	private Timestamp dtInclusao;
	private Timestamp dtExclusao;
	private String snAlterarSenha;
	private Timestamp dtUltimaSenha;
	private String tpUsuario;
	private String cdUnimedPrestador;
	private String cdPrestadorRelacionado;

	public Integer getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario( Integer idUsuario ) {
		this.idUsuario = idUsuario;
	}

	public String getDsNomeUsuario() {
		return dsNomeUsuario;
	}

	public void setDsNomeUsuario( String dsNomeUsuario ) {
		this.dsNomeUsuario = dsNomeUsuario;
	}

	public String getDsLogin() {
		return dsLogin;
	}

	public void setDsLogin( String dsLogin ) {
		this.dsLogin = dsLogin;
	}

	public String getDsSenha() {
		return dsSenha;
	}

	public void setDsSenha( String dsSenha ) {
		this.dsSenha = dsSenha;
	}

	public Integer getNrValidadeSenha() {
		return nrValidadeSenha;
	}

	public void setNrValidadeSenha( Integer nrValidadeSenha ) {
		this.nrValidadeSenha = nrValidadeSenha;
	}

	public String getDsEmail() {
		return dsEmail;
	}

	public void setDsEmail( String dsEmail ) {
		this.dsEmail = dsEmail;
	}

	public String getSnAdministrador() {
		return snAdministrador;
	}

	public void setSnAdministrador( String snAdministrador ) {
		this.snAdministrador = snAdministrador;
	}

	public Timestamp getDtInclusao() {
		return dtInclusao;
	}

	public void setDtInclusao( Timestamp dtInclusao ) {
		this.dtInclusao = dtInclusao;
	}

	public Timestamp getDtExclusao() {
		return dtExclusao;
	}

	public void setDtExclusao( Timestamp dtExclusao ) {
		this.dtExclusao = dtExclusao;
	}

	public String getSnAlterarSenha() {
		return snAlterarSenha;
	}

	public void setSnAlterarSenha( String snAlterarSenha ) {
		this.snAlterarSenha = snAlterarSenha;
	}

	public Timestamp getDtUltimaSenha() {
		return dtUltimaSenha;
	}

	public void setDtUltimaSenha( Timestamp dtUltimaSenha ) {
		this.dtUltimaSenha = dtUltimaSenha;
	}

	public String getTpUsuario() {
		return tpUsuario;
	}

	public void setTpUsuario( String tpUsuario ) {
		this.tpUsuario = tpUsuario;
	}

	public String getCdUnimedPrestador() {
		return cdUnimedPrestador;
	}

	public void setCdUnimedPrestador( String cdUnimedPrestador ) {
		this.cdUnimedPrestador = cdUnimedPrestador;
	}

	public String getCdPrestadorRelacionado() {
		return cdPrestadorRelacionado;
	}

	public void setCdPrestadorRelacionado( String cdPrestadorRelacionado ) {
		this.cdPrestadorRelacionado = cdPrestadorRelacionado;
	}

}
