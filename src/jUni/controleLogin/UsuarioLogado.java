package jUni.controleLogin;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.servlet.http.HttpSession;

import quatro.sql.Conexao;
import quatro.sql.ConexaoParaPoolDeConexoes;
import quatro.sql.PoolDeConexoes;
import quatro.sql.QtSQLException;
import quatro.sql.Query;
import quatro.util.MD5;
import quatro.util.QtData;

import jUni.controleLogin.Util;

@ManagedBean
@SessionScoped
public class UsuarioLogado {

	private Usuario usuarioLogado;
	private Integer idEmpresaRelacionada;
	private String nomeUsuarioLogado;
	private String PUsuarioLogado;
	private String email;
	private String dsLogin;
	private String idUsuarioLogado;
	private String senhaAnterior;
	private String dsSenhaNova;
	private String dsSenhaNova2;
	private boolean erroNaNovaSenha;
	private boolean estadoSistema;
	private boolean senhaRedefinida = false;
	private String msgErro;
	private int nrForcaSenha = 0;
	private boolean podeAlterar = false;
	private String confere = "remove";
	private String confereAntiga = "remove";
	private String tpUsuario = null;

	public void onLoad() {

		if (usuarioLogado == null) {
			usuarioLogado = Util.getDadosUsuarioLogado();
		}
		if (usuarioLogado != null) {
			try {
				setIdEmpresaRelacionada(getEmpresaRelacionada(usuarioLogado.getIdUsuario()));
				estadoSistema = sistemaAtivo();
//				setIdEmpresaRelacionada(8000);
				System.out.println("usuarioLogado.getCdPrestadorRelacionado() " + getIdEmpresaRelacionada() );
				System.out.println("estadoSistema " + estadoSistema );
				
				if( usuarioLogado.getIdUsuario().equals( 293 ) ) {
					estadoSistema = true;
				}
				
				if( getIdEmpresaRelacionada() == null || estadoSistema ) {

					setTpUsuario(usuarioLogado.getTpUsuario());
					setPUsuarioLogado(usuarioLogado.getDsSenha());
					setSenhaRedefinida(false);
					setNomeUsuarioLogado(usuarioLogado.getDsNomeUsuario());
					setIdUsuarioLogado(usuarioLogado.getIdUsuario().toString());
					setEmail(usuarioLogado.getDsEmail());
					setDsLogin(usuarioLogado.getDsLogin());

					jUni.controleLogin.Util.getSession().setAttribute("NomeUsuarioLogado", usuarioLogado.getDsNomeUsuario());
					jUni.controleLogin.Util.getSession().setAttribute("IdUsuarioLogado", usuarioLogado.getIdUsuario());
				} else {
					Util.redirect(Util.getContextPath() + "/resources/login/emManutencao.jsf");
				}
			} catch (Exception e) {
				e.printStackTrace();
				Util.redirect(Util.getContextPath() + "/sair.jsp");
			}

		} else {
			Util.redirect(Util.getContextPath() + "/sair.jsp");
		}

	}

	private boolean sistemaAtivo() throws Exception {
		try {
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
			try {

				Query q = new Query(cnx);
				q.setSQL("select * from pcmso.parametro_pcmso order by id_parametro desc limit 1");
				q.executeQuery();

				System.out.println("OnLoad ativo? " + q.preparaSQL());

				if (!q.isEmpty()) {
					System.out.println("Sistema ativo? " + q.getBoolean("sn_ativo"));
					return q.getBoolean("sn_ativo");
				}

			} finally {
				cnx.libera();
			}

		} catch (Exception e) {
			throw new Exception(e);
		}
		return false;
	}

	private Integer getEmpresaRelacionada(Integer usuario) {
		if (usuario != null) {
			try {
				ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
				try {
					Query q = new Query(cnx);
					q.addSQL(
							"select dspermissao from jdesk.permissoes_especiais_usuario u, jdesk.permissoes_especiais e");
					q.addSQL("where u.idusuario = :prmIdUsuario");
					q.addSQL("and u.sqpermissao = e.sqpermissao");
					q.setParameter("prmIdUsuario", usuario);

					q.executeQuery();

					System.out.println(q.preparaSQL());

					if (!q.isEmpty()) {

						try {
							Integer idPermissao = new Integer(q.getString("dspermissao"));
							return idPermissao;
						} catch (Exception e) {
							e.printStackTrace();
							return null;
						}
					}
				} finally {
					cnx.libera();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	public void inicRedefineSenha() {
		onLoad();

	}

	public void redefinirSenhaNova() throws QtSQLException {

		if (usuarioLogado == null) {
			Util.redirect("index.jsf");
		} else {
			if (dsSenhaNova.equals(dsSenhaNova2)) {

				String infoSenha = codificaSenha(senhaAnterior);

				if (usuarioLogado.getDsSenha().equals(infoSenha)) {

					if (!usuarioLogado.getDsSenha().equals(dsSenhaNova)) {
						String novaSenha = codificaSenha(dsSenhaNova);
						redefineNovaSenha(usuarioLogado, novaSenha);
						setNrForcaSenha(0);
						setConfere("remove");
						setSenhaRedefinida(true);
						setPodeAlterar(false);
						Util.redirect("../index.jsf");
					} else {
						setDsSenhaNova("");
						setDsSenhaNova2("");
						setErroNaNovaSenha(true);
						setSenhaRedefinida(false);
						setPodeAlterar(false);
						msgErro = "Use uma senha difernete da anterior";
					}

				} else {
					setSenhaRedefinida(false);
					setErroNaNovaSenha(false);
					msgErro = "A Senha anterior não confere";
				}
			} else {
				setDsSenhaNova("");
				setDsSenhaNova2("");
				setErroNaNovaSenha(true);
				setSenhaRedefinida(false);
				msgErro = "As senhas não conferem";
			}

		}

	}

	private void redefineNovaSenha(Usuario usr, String novaSenha) throws QtSQLException {

		ConexaoParaPoolDeConexoes cnx = null;

		try {
			cnx = PoolDeConexoes.getConexao("jDesk");

			Query qry = new Query(cnx);

			qry.setSQL("UPDATE jDesk.usuario set snalterarsenha = 'N', dsSenha = '" + novaSenha + "', dtultimasenha = '"
					+ new QtData().getFormatoSQL() + "' WHERE idusuario = " + usr.getIdUsuario());
			qry.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
			Util.enviaMsgErroPorEmail(e, null);
		} finally {
			cnx.close();
		}

	}

	public static int getVerificaSenha(String str) {

		int i = 0;
		Pattern pat = Pattern.compile("[!@#$%*()_+=1234567890ABCDEFGHIJALMNOPQRSTUVXWYZ]");
		Matcher m = pat.matcher(str);
		while (m.find()) {
			i++;
		}
		if (i <= 3) {
			if (str.length() >= 6) {
				i = 4;
			}
		}

		return i;
	}

	public static String codificaSenha(String senhaUsuario) {
		String infoSenha = MD5.md5(senhaUsuario);
		return infoSenha;
	}

	public String getNomeUsuarioLogado() {
		return nomeUsuarioLogado;
	}

	public void setNomeUsuarioLogado(String nomeUsuarioLogado) {
		this.nomeUsuarioLogado = nomeUsuarioLogado;
	}

	public Usuario getUsuarioLogado() {
		return usuarioLogado;
	}

	public void setUsuarioLogado(Usuario usuarioLogado) {
		this.usuarioLogado = usuarioLogado;
	}

	public String getIdUsuarioLogado() {
		return idUsuarioLogado;
	}

	public void setIdUsuarioLogado(String idUsuarioLogado) {
		this.idUsuarioLogado = idUsuarioLogado;
	}

	public String getSenhaAnterior() {
		return senhaAnterior;
	}

	public void setSenhaAnterior(String senhaAnterior) {
		if (usuarioLogado != null) {
			if (senhaAnterior != null) {
				if (!usuarioLogado.getDsSenha().equals(codificaSenha(senhaAnterior))) {
					confereAntiga = "remove";
				} else {
					confereAntiga = "ok";
				}
			}
		}
		this.senhaAnterior = senhaAnterior;
	}

	public String getDsSenhaNova() {
		return dsSenhaNova;
	}

	public void setDsSenhaNova(String dsSenhaNova) {
		if (dsSenhaNova != null) {
			int snh = getVerificaSenha(dsSenhaNova);
			if (snh <= 3) {
				setPodeAlterar(true);
				nrForcaSenha = 1;
			} else if (snh > 3 && snh <= 6) {
				setPodeAlterar(false);
				nrForcaSenha = 2;
			} else if (snh > 6) {
				setPodeAlterar(false);
				nrForcaSenha = 3;
			}
		}
		this.dsSenhaNova = dsSenhaNova;
	}

	public String getDsSenhaNova2() {
		return dsSenhaNova2;
	}

	public void setDsSenhaNova2(String dsSenhaNova2) {
		if (dsSenhaNova != null) {
			if (!dsSenhaNova.equals(dsSenhaNova2)) {
				confere = "remove";
			} else {
				confere = "ok";
			}
		}
		this.dsSenhaNova2 = dsSenhaNova2;
	}

	public String getMsgErro() {
		return msgErro;
	}

	public void setMsgErro(String msgErro) {
		this.msgErro = msgErro;
	}

	public boolean isErroNaNovaSenha() {
		return erroNaNovaSenha;
	}

	public void setErroNaNovaSenha(boolean erroNaNovaSenha) {
		this.erroNaNovaSenha = erroNaNovaSenha;
	}

	public boolean isSenhaRedefinida() {
		return senhaRedefinida;
	}

	public void setSenhaRedefinida(boolean senhaRedefinida) {
		this.senhaRedefinida = senhaRedefinida;
	}

	public int getNrForcaSenha() {
		return nrForcaSenha;
	}

	public void setNrForcaSenha(int nrForcaSenha) {
		this.nrForcaSenha = nrForcaSenha;
	}

	public boolean isPodeAlterar() {
		return podeAlterar;
	}

	public void setPodeAlterar(boolean podeAlterar) {
		this.podeAlterar = podeAlterar;
	}

	public String getConfere() {
		return confere;
	}

	public void setConfere(String confere) {
		this.confere = confere;
	}

	public String getConfereAntiga() {
		return confereAntiga;
	}

	public void setConfereAntiga(String confereAntiga) {
		this.confereAntiga = confereAntiga;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDsLogin() {
		return dsLogin;
	}

	public void setDsLogin(String dsLogin) {
		this.dsLogin = dsLogin;
	}

	public String getTpUsuario() {
		return tpUsuario;
	}

	public void setTpUsuario(String tpUsuario) {
		this.tpUsuario = tpUsuario;
	}

	public String getPUsuarioLogado() {
		return PUsuarioLogado;
	}

	public void setPUsuarioLogado(String pUsuarioLogado) {
		PUsuarioLogado = pUsuarioLogado;
	}

	public Integer getIdEmpresaRelacionada() {
		return idEmpresaRelacionada;
	}

	public void setIdEmpresaRelacionada(Integer idEmpresaRelacionada) {
		this.idEmpresaRelacionada = idEmpresaRelacionada;
	}

	public boolean isEstadoSistema() {
		return estadoSistema;
	}

	public void setEstadoSistema(boolean estadoSistema) {
		this.estadoSistema = estadoSistema;
	}

}