package jUni.controleLogin;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import quatro.sql.ConexaoParaPoolDeConexoes;
import quatro.sql.PoolDeConexoes;
import quatro.sql.QtSQLException;
import quatro.sql.Query;
import quatro.util.MD5;
import quatro.util.ParametrosSistema;
import quatro.util.QtData;

import jUni.persistencia.geral.p.Procedimento;

public class Util {
	
	public static void enviaMsgErroPorEmail( Exception exc, String nmUsuarioLogado, String metodo, String nrTrnPrest ) {
		
		Properties props = new Properties();
		/** Parâmetros de conexão com servidor Gmail */
		props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");

		Session session = Session.getDefaultInstance( props, new javax.mail.Authenticator() {

			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication( "lucionteixeria@gmail.com", "TI@x1ca1" );
			}
		} );

		/** Ativa Debug para sessão */
		session.setDebug( true );

		try {

			Message message = new MimeMessage( session );
			message.setFrom( new InternetAddress( "lucionteixeria@gmail.com" ) ); // Remetente

			Address[] toUser = InternetAddress.parse( "luciont@unimedvaledocai.com.br, jeanc@unimedvaledocai.com.br, sobreavisoti@unimedvaledocai.com.br" );

			message.setRecipients( Message.RecipientType.TO, toUser );
			message.setSubject( "WSD " + ParametrosSistema.getInstance().getNomeDoCliente() );// Assunto
			message.setText( "Foi identificada uma falha no sistema da " + ParametrosSistema.getInstance().getNomeDoCliente()  + " ao " + metodo + ": " + new QtData().getFormatoSQL() + "\n\n" + "Transação: " + nrTrnPrest + "\nUsuário: " + nmUsuarioLogado + "\n " +  mostraErroCompleto( exc ) );
			
			/** Método para enviar a mensagem criada */
			Transport.send( message );

		} catch( MessagingException e ) {
			throw new RuntimeException( e );
		}
	}
	
	public static void enviaNotificacaoDeNovoProcedimento( Procedimento procedimento ) {
		
		Properties props = new Properties();
		/** Parâmetros de conexão com servidor Gmail */
		props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");

		Session session = Session.getDefaultInstance( props, new javax.mail.Authenticator() {

			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication( "lucionteixeria@gmail.com", "TI@x1ca1" );
			}
		} );

		session.setDebug( true );

		try {

			Message message = new MimeMessage( session );
			message.setFrom( new InternetAddress( "lucionteixeria@gmail.com" ) ); // Remetente

			Address[] toUser = InternetAddress.parse( "luciont@unimedvaledocai.com.br" );

			message.setRecipients( Message.RecipientType.TO, toUser );
			message.setSubject( "Interface de Auditoria - Novo Procedimento" );// Assunto
			message.setText( "A Interface de Auditoria inseriu um novo procedimento. Atualize os dados deste procedimento para evitar contratempos \n\n Código: " + procedimento.getCdProcedimento() + "\nVersao: " + procedimento.getCdVersao() + "\nProcedimento: " + procedimento.getCdProcedimento()  );
			
			Transport.send( message );

		} catch( MessagingException e ) {
			throw new RuntimeException( e );
		}
	}
	
	public static String buscaViaCartao( Integer idBenef ) throws QtSQLException {
		
		ConexaoParaPoolDeConexoes cnx = null;
		
		try{
			cnx = PoolDeConexoes.getConexao();
			
			Query qry = new Query( cnx );
			try{
				qry.setSQL( "select nr_via from validades_cartao  where id_beneficiario = :idBenef order by nr_via desc limit 1" );
				qry.setParameter( "idBenef", idBenef );
				
				qry.executeQuery();
				
				if( !qry.isEmpty() ) {
					return qry.getInteger( "nr_via" ).toString();
				} 
			} finally {
				qry.liberaRecursos();
			}
			
		} catch( Exception e ) {
			e.printStackTrace();
		} finally {
			cnx.close();
		}
		
		return null;
		
	}
	
	public static void enviaMsgErroPorEmail( String exc, String nrTrnPrest ) {
		
		Properties props = new Properties();
		/** Parâmetros de conexão com servidor Gmail */
		props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");

		Session session = Session.getDefaultInstance( props, new javax.mail.Authenticator() {

			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication( "lucionteixeria@gmail.com", "TI@x1ca1" );
			}
		} );

		/** Ativa Debug para sessão */
		session.setDebug( true );

		try {

			Message message = new MimeMessage( session );
			message.setFrom( new InternetAddress( "lucionteixeria@gmail.com" ) ); // Remetente

			Address[] toUser = InternetAddress.parse( "luciont@unimedvaledocai.com.br, jeanc@unimedvaledocai.com.br, sobreavisoti@unimedvaledocai.com.br" );

			message.setRecipients( Message.RecipientType.TO, toUser );
			message.setSubject( "WSD " + ParametrosSistema.getInstance().getNomeDoCliente() + " - FALHA DE COMUNICAÇÃO COM O BANCO");// Assunto
			message.setText( "Foi identificada uma falha no sistema da " + ParametrosSistema.getInstance().getNomeDoCliente()  + ": " + new QtData().getFormatoSQL() + "\n\n" + "Transação: " + nrTrnPrest + "\n\n " +  exc );
			
			/** Método para enviar a mensagem criada */
			Transport.send( message );

		} catch( MessagingException e ) {
			throw new RuntimeException( e );
		}
	}
	
	public static void enviaLoadAverage( Double over ) {
		
		Properties props = new Properties();
		/** Parâmetros de conexão com servidor Gmail */
		props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");

		Session session = Session.getDefaultInstance( props, new javax.mail.Authenticator() {

			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication( "lucionteixeria@gmail.com", "TI@x1ca1" );
			}
		} );

		/** Ativa Debug para sessão */
		session.setDebug( true );

		try {

			Message message = new MimeMessage( session );
			message.setFrom( new InternetAddress( "lucionteixeria@gmail.com" ) ); // Remetente

			Address[] toUser = InternetAddress.parse( "luciont@unimedvaledocai.com.br, jeanc@unimedvaledocai.com.br" );

			message.setRecipients( Message.RecipientType.TO, toUser );
			message.setSubject( "WSD " + ParametrosSistema.getInstance().getNomeDoCliente() + " - OverLoad " + over );// Assunto
			message.setText( "O servidor está com overLoad em: " + over );
			
			/** Método para enviar a mensagem criada */
			Transport.send( message );

		} catch( MessagingException e ) {
			throw new RuntimeException( e );
		}
	}
	
	public static void enviaMsgErroPorEmail( Exception exc, String nrTrnPrest ) {
		
		Properties props = new Properties();
		/** Parâmetros de conexão com servidor Gmail */
		props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");

		Session session = Session.getDefaultInstance( props, new javax.mail.Authenticator() {

			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication( "lucionteixeria@gmail.com", "TI@x1ca1" );
			}
		} );

		/** Ativa Debug para sessão */
		session.setDebug( true );

		try {

			Message message = new MimeMessage( session );
			message.setFrom( new InternetAddress( "lucionteixeria@gmail.com" ) ); // Remetente

			Address[] toUser = InternetAddress.parse( "luciont@unimedvaledocai.com.br, jeanc@unimedvaledocai.com.br, sobreavisoti@unimedvaledocai.com.br" );

			message.setRecipients( Message.RecipientType.TO, toUser );
			message.setSubject( "WSD " + ParametrosSistema.getInstance().getNomeDoCliente() + " - FALHA DE COMUNICAÇÃO COM O BANCO");// Assunto
			message.setText( "Foi identificada uma falha no sistema da " + ParametrosSistema.getInstance().getNomeDoCliente()  + ": " + new QtData().getFormatoSQL() + "\n\n" + "Transação: " + nrTrnPrest + "\n\n " +  exc );
			
			/** Método para enviar a mensagem criada */
			Transport.send( message );

		} catch( MessagingException e ) {
			throw new RuntimeException( e );
		}
	}
	
	public static String mostraErroCompleto( Exception e ) {
		
		StringWriter sw = new StringWriter( 512 );
		PrintWriter pw = new PrintWriter( sw );
		
		e.printStackTrace( pw );
		
		return sw.toString();
	}
	
	public static HttpSession getSession() {
		
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		HttpServletRequest request = (HttpServletRequest) context.getRequest();
		HttpSession result = request.getSession();
		
		return result;
	}
	
	public static Usuario getDadosUsuarioLogado() {
		
		HttpSession sessao = getSession();
		Usuario result = (Usuario) sessao.getAttribute( "UsuarioLogado" );
		
		return result;
	}
	
	public static String NomeUsuarioLogado() {
		
		String nome;
		HttpSession sessao = getSession();
		Usuario result = (Usuario) sessao.getAttribute( "UsuarioLogado" );
		nome = result.getDsNomeUsuario();
		return nome;
	}

	public static void redirect( String destino ) {

		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		HttpServletResponse response = (HttpServletResponse) context.getResponse();

		try {
			response.sendRedirect( destino );
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static HttpServletRequest getRequest() {
		
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		HttpServletRequest request = (HttpServletRequest) context.getRequest();
		return request;
	}
	
	public static void sendRedirect( String destino ) {
		
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		
		if( !destino.startsWith( "." ) ) {
			
			if( !destino.startsWith( "/" ) ) {
				destino = "/" + destino;
			}
			
			HttpServletRequest request = (HttpServletRequest) context.getRequest();
			
			destino = request.getContextPath() + destino; 
		}
		
		try {
			context.redirect( destino );
		} catch( IOException e ) {
			e.printStackTrace();
		}
	}
	
	public static String getRequestParameter( String nomeParametro ) {
		
		return getRequest().getParameter( nomeParametro );
	}
	
	public static String getUrlSite() {
		
		ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
		HttpServletRequest request = (HttpServletRequest) ec.getRequest();

		String cp = request.getContextPath();
		String url = request.getRequestURL().toString();
		
		int pos = url.indexOf( cp );
		
		if( pos > 0 ) {
			url = url.substring( 0, pos + cp.length() ) + "/";
		}
		
		return url;
	}

	public static String getContextPath() {
	
		return FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath();
	}
	
	public static String criaHashConfirmacao( String nrSolicitacao, String idOferta, String tpVisita ) {

		String txHash = nrSolicitacao + "Mc_MutuaCompras" + idOferta + "QtInfo2014" + tpVisita;
		txHash = MD5.md5( txHash );
		
		StringBuilder result = new StringBuilder( txHash );
		
		for( int i = 1; i < result.length(); i += 2 ) {
			result.delete( i, i + 1 );
		}
		
		for( int i = 4; i < result.length(); i += 2 ) {
			result.delete( i, i + 1 );
		}
		
		
		return result.toString();
	}
	
	public static String adicionaZero( String valor, int tam ) {

        if ( valor.length() < tam ) {

            StringBuilder vlr = new StringBuilder( tam + 1 );
            vlr.append( valor );

            while ( vlr.length() < tam ) {
                vlr.insert( 0, "0" );
            }

            return vlr.toString();
        }
        return valor;
    }
	
}