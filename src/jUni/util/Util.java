package jUni.util;


import java.io.IOException;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jUni.persistencia.geral.l.Lotacao;
import jUni.persistencia.geral.l.LotacaoLocalizador;
import quatro.sql.ConexaoParaPoolDeConexoes;
import quatro.sql.PoolDeConexoes;
import quatro.util.Cnpj;

public class Util {

	public static HttpSession getSession() {
		
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		HttpServletRequest request = (HttpServletRequest) context.getRequest();
		HttpSession result = request.getSession();

		return result;
	}
	
	public static String getDsLotacao( String cdUnimed, Integer idLotacao, Integer idEmpresa ) {
		String retorno = "";
		
		try {
			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
			try {
				
				Lotacao lotacao = LotacaoLocalizador.buscaLotacao(cnx, cdUnimed, idEmpresa, idLotacao );
				if( lotacao != null ) {
					return lotacao.getDsLotacao();
				} else {
					retorno = "Ops! Lotação não encontrada";
				}
				
			} finally {
				cnx.libera();
			}
		} catch( Exception e ) {
			e.printStackTrace();
			retorno = "Ops " + e.getMessage();
		}
		
		return retorno;
	}

//	public static Usuario getDadosUsuarioLogado() {
//
//		HttpSession sessao = getSession();
//		Usuario result = (Usuario) sessao.getAttribute( "UsuarioLogado" );
//		return result;
//	}
//	
//	public static void setDadosUsuarioLogado(Usuario user) {
//
//		HttpSession sessao = getSession();
//		sessao.setAttribute( "UsuarioLogado", user );
//		
//	}
		

//	public static String NomeUsuarioLogado() {
//
//		String nome;
//		HttpSession sessao = getSession();
////		Usuario result = (Usuario) sessao.getAttribute( "UsuarioLogado" );
////		nome = result.getDsNomeUsuario();
//
////		System.out.println( "Nome:" + nome );
////		return nome;
//	}

	public static void redirect( String destino ) {

		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		HttpServletResponse response = (HttpServletResponse) context.getResponse();

		try {
			response.sendRedirect( destino );
		} catch( IOException e ) {
			e.printStackTrace();
		}
	}

	public static void sendRedirect( String destino ) {

		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();

		if( !destino.startsWith( "." ) ) {

			if( !destino.startsWith( "/" ) ) {
				destino = "/" + destino;
			}

			HttpServletRequest request = (HttpServletRequest) context.getRequest();
			destino = request.getContextPath() + destino;
		}

		try {
			context.redirect( destino );
		} catch( IOException e ) {
			e.printStackTrace();
		}
	}
	
	public static String getContextPath() {
		
		return FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath();
	}
	
	public static String formataCnpj( String cnpj ){
		
		cnpj = cnpj.replaceAll( ".", "" );
		cnpj = cnpj.replaceAll( "/", "" );
		cnpj = cnpj.replaceAll( "-", "" );
		
		return cnpj;	
	}
	
	public static String verificarCaracterValidoASCII( String linha ){
		
		StringBuilder sb = new StringBuilder();
		
		char[] caracteres = linha.toCharArray();
		for( char c : caracteres ){
			if( (int) c >= 32 && (int) c <= 126 || (int) c >= 192 && (int) c <= 220 || (int) c >= 224 && (int) c <= 252 ){
				sb.append( c );
			} else {
				sb.append( " " );
			}
		}
		
		return sb.toString().trim();
	}

}