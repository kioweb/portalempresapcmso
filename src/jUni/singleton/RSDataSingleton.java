package jUni.singleton;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import jPcmso.persistencia.geral.b.BeneficiarioCbo;
import jPcmso.persistencia.geral.b.BeneficiarioCboLocalizador;
import jUni.bean.wsrsdata.TransacaoRSData;
import quatro.sql.ConexaoParaPoolDeConexoes;
import quatro.sql.PoolDeConexoes;
import quatro.sql.Query;
import quatro.util.ParametrosSistema;
import quatro.util.QtData;

@ManagedBean(eager = true)
@ApplicationScoped
public class RSDataSingleton {
	
	private static final RSDataSingleton instance = new RSDataSingleton();
	
	public RSDataSingleton() {
		System.out.println("INICIANDO THREAD ATUALIZAÇÃO RSDATA");
		System.out.println("INICIANDO THREAD ATUALIZAÇÃO RSDATA");
		System.out.println("INICIANDO THREAD ATUALIZAÇÃO RSDATA");
		System.out.println("INICIANDO THREAD ATUALIZAÇÃO RSDATA");
		System.out.println("INICIANDO THREAD ATUALIZAÇÃO RSDATA");
		new Thread() {
		     
		    @Override
		    public void run() {
		    	boolean envia = true;
		    	try {
		    		ParametrosSistema.setNmArquivoParametros("/usr/local/projetos/PortalEmpresaPCMSO/conf/parametros0047.xml");
		    		//ParametrosSistema.setNmArquivoParametros("/home/luciont/projetos/Argous/workspace/PoralEmpresaPCMSO/WebContent/WEB-INF/parametros0047.xml");
		    		
		    		envia = ParametrosSistema.getInstance().getParametro( "producao" ).equals( "true" );
		    		
		    	} catch( Exception e ) {
		    		envia = false;
		    		e.printStackTrace();
		    	}
		    	
		    	while( envia ) {
		    		
		    		QtData dtAlteracao = new QtData();
		    		dtAlteracao.setNrMinutos( dtAlteracao.getNrMinutos() - 30 );
		    		
		    		try {
		    			
		    			ConexaoParaPoolDeConexoes cnx = PoolDeConexoes.getConexao();
		    			try {
		    				Query q = new Query(cnx);
		    				q.setSQL("select * from pcmso.beneficiarios_cbo ");
		    				q.addSQL("where dt_alteracao_maquina >= '" + dtAlteracao.getQtDataAsTimestamp() + "' ");
//		    				q.addSQL("and dt_exclusao is null");
		    				q.addSQL("and ( ( dt_envio_rsdata <= '" + dtAlteracao.getQtDataAsTimestamp() + "' ) or ( dt_envio_rsdata is null ) )");
		    				
//		    				System.out.println(q.preparaSQL());
		    				
		    				q.executeQuery();
		    				
		    				if( !q.isEmpty() ) {
		    					while( !q.isAfterLast() ) {
		    						
		    						BeneficiarioCbo beneficiarioCbo = new BeneficiarioCbo();
		    						BeneficiarioCboLocalizador.buscaCampos(beneficiarioCbo, q);
		    						
	    							TransacaoRSData.enviaRSData(cnx, beneficiarioCbo); 
	    							
		    						q.next();
		    					}
		    				}
		    				
		    			} finally {
		    				cnx.libera();
		    			}
		    			
		    		} catch (Exception e ) {
		    			e.printStackTrace();
		    		}
		    		
		    		try {
						Thread.sleep(10000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
		    	}
		       
		    }
		  }.start();
	}
	
	
	
	public static RSDataSingleton getInstance() {
		if( instance != null ) {
			return instance;
		} else {
			return new RSDataSingleton();
		}
	}

}
